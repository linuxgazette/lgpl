---
title: Wydanie 185
date: 2011-04-01
comments: false
--- 

## Spis treści

* News Bytes
* Linux based SMB NAS System Performance Optimization
* Activae: towards a free and community-based platform for digital assets
    management
* Away Mission for April 2011
* HAL: Part 3 Functions
* synCRONicity: A riff on perceived limitations of cron
* Applying Yum Package Updates to Multiple Servers Using Fabric
* Ubuntu, the cloud OS
* [Dawno temu: Red Hat Linux 3.0.3 - 4.2, 1996-1997](/post/185/silva/)
* Taking the Pain Out of Authentication
* HelpDex
* XKCD
* Doomed by Obscurity