---
title: Wydanie 182
date: 2011-01-01
comments: false
---

## Spis treści

* News Bytes
* Mutt configuration
* Protecting Your Linux Server from Root Password-Guessing Attacks
* Henry's Techno-Musings: Philosophy of Troubleshooting: Sea of Troubles
* Eight Reasons to give E17 a Try
* How I created my perfect programming environment
* HelpDex
* XKCD
* Doomed to Obscurity