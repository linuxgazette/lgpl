---
title: Wydanie 131
date: 2006-10-01
comments: false
--- 

## Spis treści

* Mailbag
* Talkback
* 2-cent Tips
* Apache2, WebDAV, SSL and MySQL: Life In The Fast Lane
* Sharp does it once again: the SL-C3200
* Sharp's latest PDA -- I'll use that term although it really doesn't do it justice -- is a real killer, irresistible to incorrigible Linuxers.
* Ogg, WAV, and MP3
* What is describe in the following may be illegal where you live.
* On Qmail, Forged Mail, and SPF Records
* Here's an object lesson in how not to do e-mail, with the role of global village idiot played by Prof.
* The Geekword Puzzle
* [Przyśpiewki w tonacji Pingwina: KGuitar](/post/131/oregan/)
* It's been quite a while since my last "Songs in The Key of Tux" article, and, to be honest, I had given up on using Linux-based programs for my tablature needs.
* SVN Hackery: Versioning Your Linux Configuration
* The LG Backpage
