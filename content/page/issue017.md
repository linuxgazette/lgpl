---
title: Wydanie 17
date: 1997-05-01
comments: false
--- 

## Spis treści

- The Front Page
- The MailBag
- More 2 Cent Tips
- News Bytes
- The Answer Guy, by James T. Dennis
- Clueless at the Prompt: A Column for New Users, by Mike List
- The Dotfile Generator, by Jesper Pedersen
- Graphics Muse, by Michael J. Hammel
- Kandinsky, by Jeff Hohensee
- Linux Expo 1997, by Jon "maddog" Hall
- Linux Raid Functions, by Jay Painter
- New Release Reviews, by Larry Ayers
- Slackware Linux Installation & Getting Started, by Sean Dreilinger
- Texas Linux Installation Project, by Kendall G. Clark
- The Back Page
