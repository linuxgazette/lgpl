---
title: Wydanie 180
date: 2010-11-01
comments: false
--- 

## Spis treści

* News Bytes
* Away Mission - ApacheCon, QCon, ZendCon and LISA
* Enemy Action
* House, Lies and Sysadmin
* [Instalowanie/Konfigurowanie/Cachowanie Django na serwerze Linux](/post/180/silva/)
* Ten Tips to Harden Your Website
* HelpDex
* Ecol
* XKCD
* Doomed by Obscurity