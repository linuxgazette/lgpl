---
title: Wydanie 86
date: 2003-01-01
comments: false
--- 

## Spis treści

* Skrzynka pocztowa
* Rady za 2 grosze
* The Answer Gang
* Kęs nowości
* An Undeletion Experience
* I Broke the Console Barrier
* HelpDex.
* Ecol.
* EcolNet and the escomposLinux.org project
* Te głupie rzeczy, jakie robimy ze swoimi komputerami.
* Jak przesłać emailem encyklopedię?
* Bezpieczeństwo a Superglobalne zmienne PHP.
* Perl One-Liner of the Month: The Case of the Evil Spambots
* Qubism.
* Programming in Ruby - Part 3
* Debian APT Part 2: Installing Unreleased Software
* Eksploracja TCP/IP przy użyciu TCPdump i Tethereal.
* Czy vmWare jest dobry rozwiązaniem dla użytkowników Linuksa.
* Na zakończenie 