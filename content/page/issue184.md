---
title: Wydanie 184
date: 2011-03-01
comments: false
---

## Spis treści

* News Bytes
* Henry's Techno-Musings: "The Wit and Wisdom of Chairman John"
* HAL: Part 2 My Favourite Aliases
* Book Review: Snip, Burn, Solder, Shred
* The reason why I have switched to zsh - Part 2
* Playing around with IPv6 on Linux and Freenet6
* HelpDex
* XKCD
* Doomed to Obscurity
* The Foolish Things We Do With Our Computers