---
title: Wydanie 1
date: 1995-07-01
comments: false
---

## Spis treści

* [Co to jest Linux Gazette...](/post/1/whatis/)
* [Zaczynamy...](/post/1/offwego/)
* [Czym jest to całe MOTD...](/post/1/motd/)
* [Cała zabawa z aliasami i ich efektywność](/post/1/alias/)
* [Dodawanie różnych funkcji dla późniejszego oszczędzenia czasu](/post/1/functions/)
* [Odtwarzanie plików .AU przy starcie systemu](/post/1/au/)
* [Prosty sposób na montowanie cdrom-u i stacji dyskietek](/post/1/mounting/)
* [Zabezpieczanie systemowych plików konfiguracyjnych](/post/1/configs/)
* [Dostęp do Linuksa spod DOSa!?](/post/1/ext2tool/)
* [Dopieszczanie kernela... to nie takie trudne](/post/1/kernel/)
* [Prosty sposób na przetestowanie kernela](/post/1/testdrive/)
* [Cóż, to wszystko](/post/1/thatsit/)