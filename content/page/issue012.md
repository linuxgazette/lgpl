---
title: Wydanie 12
date: 1996-12-01
comments: false
---

## Spis treści:

* The Front Page
* The MailBag
* More 2 Cent Tips
  * Boot Information Display
  * Console Tricks
  * Firewalling / Masquerading with 2.0.xx
  * FTP and /etc/shells
  * How to Truncate /var/adm/messages
  * HTML, Use of BODY Attributes
  * lowerit Shell Script
  * Removing Users
  * Root and Passwords
  * Talk Daemon and Dynamic Addresses
  * tar Tricks
* News Bytes
  * News in General
  * Software Announcements
* The Adventure of Upgrading to Redhat 4.0, by Randy Appleton
* Features of TCSH Shell, by Jesper Kjær Pedersen
* FEddi HOWTO (English version), by Manuel Soriano
* Graphics Muse, by Michael J. Hammel
* InfoZIP Archive Utilities, by Robert G. "Doc" Savage
* New Release Reviews, by Larry Ayers
  * Slang Applications for Linux
  * Updates to My Previous Reviews
  * The Yard Rescue Disk Package
* Recent Linux Conferences
  * Unix Expo 1996, by Lydia Kinata
  * DECUS in Anaheim, by Phil Hughes
  * Open Systems World/Fed/UNIX, by Gary Moore
* Setting Up the Apache Web Server, by Andy Kahn
* Weekend Mechanic, by John M. Fisk
* The Back Page
  * About This Month's Authors
  * Not Linux