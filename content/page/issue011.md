---
title: Wydanie 11
date: 1996-11-01
comments: false
---

## Spis treści:

* The Front Page
* The MailBag
* More 2 Cent Tips
* News Bytes
* Graphics Muse
* Kill-Ring for Xemacs
* [Linus przeniesie się do USA w 1997r](/post/11/linus/)
* New Release Reviews
* TAPR Statement on Spread Spectrum Technology Development
* TCL/TK Installation
* Weekend Mechanic
* The Back Page