---
title: Wydanie 3
date: 1995-09-01
comments: false
---

## Spis treści:

* Salutations and the MailBag
* Saving WWWeb documents for posterity!
* Some great 2¢ Tips...
* Introducing FVWM!
* FVWM: Getting stuff going...
* FVWM: Adding programs to the Popup Menus
* FVWM: Managing the Virtual Desktop
* FVWM: Quick Tips!
* Product Testimonial: XFMail for Sendmail-less mail!
* Colophon
