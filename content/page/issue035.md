---
title: Wydanie 35
date: 01-12-1998
comments: false
---

## Spis treści

- The Front Page
- The MailBag
-  More 2 Cent Tips
- News Bytes
- The Answer Guy, by James T. Dennis
- Basic Emacs, by Paul Anderson
- Creating A Linux Certification, Part 3, by Dan York
- 1998 Editor's Choice Awards, by Marjorie Richardson
- [Recenzja Klawiatury Happy Hacking](../post/35/dinsel.md), Jeremy Dinsel
- Getting Started with Linux, by Prakash Advani
- The GNOME Project, by Miguel de Icaza
- IMAP on Linux: A Practical Guide, by David Jao
- Linux Installation Primer, Part 4, by Ron Jenkins
- Linux - the Darling of COMDEX 1998?, by Norman M. Jacobowitz
- My Hard Disk's Resurrection, by René Tavera
- New Release Reviews, by Larry Ayers
  - Two Small Personal Databases for Linux
- Product Review: Partition Magic 4.0, by Ray Marshall
- Quick and Dirty RAID Infromation, by Eugene Blanchard
- The Back Page

