---
title: Wydanie 15
date: 1997-03-01
comments: false
---

* The Front Page
* The MailBag
  * Help Wanted -- Article Ideas
  * General Mail
* More 2 Cent Tips
  * Automatic Term Resizing
  * Background Images
  * Changing Directories
  * Colorized Prompts
  * Getting less to View gzipped Files
  * Lowercased Filenames
  * More on Xterm Tittlebar Tip
  * A Quick & Dirty getmail Script
  * Syslog 2c Tip Revised
  * vi/ed Tricks and the .exrc File
* News Bytes
  * News in General
  * Software Announcements
* The Answer Guy, by James T. Dennis
  * fetchmail and POP3 Correction
  * Automated File Transfer over Firewall
  * chown Question
  * Copy from Xterm to TkDesk
  * File System Debugger
  * IP Fragmentation Attack Description
  * Mail Server Problem
  * Mail and Sendmail
  * Mounted vfat File Systems
  * POP3 E-Mail
  * Pseudo Terminal Device Questions
  * root login Bug in Linux
  * Sendmail-8.8.4 and Linux
  * wu-ftpd Problems
* Clueless at the Prompt: A New Column for New Users, by Mike List
* Big Brother Network Monitoring System, by Paul M. Sittler
* Date & Its Switches, by Larry Ayers
* Debian Linux Installation & Getting Started, by Boris D. Beletsky
* Graphics Muse, by Michael J. Hammel
* Learning about Security, by Jay Sprenkle
* Linux & Midi, by Dave Phillips
* New Release Reviews, by Larry Ayers
  * Amaya
  * Slrn & Slrnpull: Sucking Down the News
* Sigrot: BBS Taglines for the Net, by Paul Anderson
* Thoughts on Multi-threading by Andrew L. Sandoval
* Usenix/Uselinux Notes by Arnold Robbins
* What You Can Do with tcpd, by Kelly Spoon
* The Back Page
  * About This Month's Authors
  * Not Linux