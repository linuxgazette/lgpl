---
title: Wydanie 181
date: 2010-12-01
comments: false
--- 

## Spis treści

* Mailbag
* 2-Cent Tips
* News Bytes
* Simple lip-sync animations in Linux
* You don't need proprietary software to make pictures talk
* What Really Matters or The Counting Article
* Linux Sound System
* Linux Sound Journey - OSS to ALSA
* [Prawie, ale nie całkiem Linux...](/post/181/okopnik/)
* Some amusing "does that say what I think it does?" images from the streets
* SQL as a Service
* Home, Sweet Home
* SSH tunnel with home router and DD-WRT
* HelpDex
* Ecol
* XKCD