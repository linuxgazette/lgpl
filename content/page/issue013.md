---
title: Wydanie 13
date: 1997-01-01
comments: false
---


* The Front Page
* The MailBag
  * Help Wanted -- Article Ideas
  * General Mail
* More 2 Cent Tips
  * Another 2cent Tip for LG
  * Console Trick Follow-up
  * GIF Animations
  * How to close and reopen a new /var/adm/messages file
  * How to truncate /var/adm/messages
  * Info-ZIP encryption code
  * Kernel Compile Woes
  * Letter 1 to LJ Editor re Titlebar
  * Letter 2 to LJ Editor re Titlebar
  * PPP redialer script--A Quick Hack
  * TABLE tags in HTML
  * Text File undelete
  * Truncating /var/adm/messages
  * 2c Host Trick
  * Use of TCSH's :e and :r Extensions
  * Various notes on 2c tips, Gazette 12
  * Viewing HOWTO Documents
  * Xaw-XPM .Xresources troubleshooting tip
  * Xterm Titlebar
* News Bytes
  * News in General
  * Software Announcements
* The Answer Guy, by James T. Dennis
  * Dialup Problem
  * File Referencing
  * Combining Modems for More Speed
  * WWW Server
* Comdex '96, by Belinda Frazier & Kevin Pierce
* Filtering Advertisements from Web Pages using IPFWADM, by David Rudder
* Floppy Disk Tips, by Bill Duncan
* Graphics Muse, by Michael J. Hammel
  * History of Portable Network Graphics Format, by Greg Roelofs
* Indexing Texts with Smart, by Hans Paijmans
* Linux Text Editors and A New One, by Oleg L. Machulskiy
* New Release Reviews, by Larry Ayers
  * Two New X Windows Mail Clients
  * Miscellaneous Notes
* Petition to Cancel Filed Against Linux Trademark
* SLEW: Space Low Early Warning, by James T. Dennis
* The Back Page
  * About This Month's Authors
  * Not Linux