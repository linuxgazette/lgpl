---
title: Wydanie 174
date: 2010-05-01
comments: false
--- 

## Spis treści

* Mailbag
* Talkback
* 2-Cent Tips
* News Bytes
* Away Mission - Upcoming in May: Citrix Synergy and Google IO
* Henry's Techno-Musings: My Help System
* Part of my toolkit
* Pixie Chronicles: Part 2 Bootable Linux
* diagnosing problems, even more problems, an excellent solution
* Solving sudokus with GNU Octave (I)
* HTML obfuscation
* Open Hardware and the Penguin: using Linux as an Arduino development platform
* Hey, Researcher! Open Source Tools For You!
* A quick look at what's coming with Fedora 13
* [Zarządzanie hasłami z KeePassX](/post/174/youngman/)
* HelpDex
* XKCD
* Doomed to Obscurity
* The Linux Launderette