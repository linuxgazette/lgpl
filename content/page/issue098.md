---
title: Wydanie 87
date: 2003-02-01
comments: false
--- 

## Spis treści

* Skrzynka pocztowa
* Rady za 2 grosze
* The Answer Gang
* Kęs nowości
* Ecol
* Twórcza paranoja na zakończenie roku 2003
* Python Simplicity
* Odkrywanie matematyki z Sci/Linux
* The Wonderful World of Linux 2.6
* Qubism
