---
title: Wydanie 100
date: 2004-03-01
comments: false
--- 

## Spis treści:

* Skrzynka pocztowa.
* Rady za dwa grosze.
* The Answer Gang.
* Kęs nowości.
* Fvwm i zarządzanie sesjami.
* Help Dex.
* Retrospekcje.
* Ecol.
* Te głupie rzeczy, jakie robimy ze swoimi komputerami.
* [Sztuczki z generatorami w Pythonie](/post/100/pramode/)
* Zestaw narzędzi dla programistów: Profilowanie programów z użyciem gprof.
* Na zakończenie.