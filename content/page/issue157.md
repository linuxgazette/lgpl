---
title: Wydanie 157
date: 2008-12-01
comments: false
--- 

## Spis treści

* Mailbag
* Talkback
* 2-Cent Tips
* News Bytes
* Init Call Mechanism in the Linux Kernel
* Keymap Blues in Ubuntu's Text Console
* [Recenzja książki: CUPS Administrative Guide](/post/157/dokopnik/)
* Away Mission: A Vision for Embedded Linux
* Jetty: The Twelve Year Journey to Market Maturity
* Joey's Notes: Basic Samba Configuration
* HelpDex
* XKCD
* The Linux Launderette