---
title: Wydanie 183
date: 2011-02-01
comments: false
---

## Spis treści

* Mailbag
* Talkback
* 2-Cent Tips
* News Bytes
* Unattended stream recording with MPlayer
* Away Mission - RSA 2011
* Ten Things I Dislike About Linux
* HAL: Part 1 Preliminaries
* Henry's Abstraction Layer is introduced
* Enlightened Distros
* Linux distributions that offer the Enlightenment desktop
* Automating virtual server administration using Puppet
* The reason why I have switched to zsh - Part 1
* HelpDex
* XKCD
* The Linux Launderette
* The Foolish Things We Do With Our Computers