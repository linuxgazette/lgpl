---
title: Wydanie 96
date: 2003-11-01
comments: false
--- 

## Spis treści

* Linux Gazette - narodziny w nowej formie.
* Szkrzynka pocztowa.
* Rady za 2 grosze.
* The Answer Gang.
* Kęs nowości.
* Czy Twoje czcionki w X wyglądają kiepsko? Od teraz już nie.
* Ecol.
* Sendmail AUTH/STARTTLS.
* [Krótka historia Linux Gazette](post/96/history/)
* Jednolinijkowiec Perla miesiąca: Przygody ostrej blondyny.
* Jednolinijkowiec Pythona - satyra.
* Qubizm.
* Moje pierwsze doświadczenia z Apache Axis.
* Pingwin w Andorze.
* Na zakończenie. 