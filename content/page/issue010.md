---
title: Wydanie 10
date: 1996-10-01
comments: false
---

## Spis treści:

* The Front Page
* The MailBag
* More 2 Cent Tips
* News Bytes
* FEddi-COMO (article in Spanish), by Manual Soriano
* [Packet Radio i Linux](/post/10/radio/)
* In Memory of Mark A. Horton
* [Narzędzie mconv2](/post/10/mconv2/)
* NetDay96 and Linux
* Sample Plug-In SMGL Source Template
* Setting Up a Dynamic IP Web Server
* XaoS: A New Fractal Program for Linux
* Xmosaic Development on a Roll
* The Back Page
