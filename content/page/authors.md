---
title: Autorzy
subtitle: Tłumaczenia tworzyli
comments: false
---

- Marcin Niedziela
- Marek Wojtaszek
- Artur Szymański
- Daniel Horecki
- Jacek Masiulaniec
- Mateusz Pohl
- Rafał Bujnowski
- Artur Frydel
- Wiesław Ruszowski
- Krzysztof Kacprzak
- Marek Bugaj
- Jan Pieniądz
- Sebastian Górecki
- Piotr Rogacki
- Marcin Wojtyński
- Damian Rafałowski
- Bartłomiej Świercz
- Grzegorz Ponikierski
- Jarek Kus
- Rajmund Paczkowski
- Marcin Rybak
- Maksymilian Pawlak
- Tomasz Kubacki
- Piotrek Zarzycki
- Maciej Zawadziński
- Sebastian Wilczyński
- Oskar Ostafin
