---
title: Wydanie 94
date: 2003-05-01
comments: false
--- 

## Spis treści

* Kęs Nowości
* Ecol
* From C to Assembly Language
* Odtwarzanie kodowanych płyt DVD
* W okolicach /etc (prosty przewodnik)
* Linux based Radio Timeshifting
* Grupy USENET, email i tunele ssh przez połączenie dial-up
* [Pythonowa stacja pogody](/post/94/weather/)
* Wywiad z SCO