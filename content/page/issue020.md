---
title: Wydanie 20
date: 1997-08-01
comments: false
--- 

## Spis treści

- The Front Page
- The MailBag
- More 2 Cent Tips
- News Bytes
- The Answer Guy, by James T. Dennis
- Cleaning up your /tmp--Revisited, by Guy Geens
- Dealing with System Crackers--Basic Combat Techniques, by Andy Vaught
- Graphics Muse, by Michael J. Hammel
- Including RCS Keyword in LaTeX Documents, by Robert Kiesling
- Interview with Sameer Parekh, by Jim Dennis
- Introduction to RCS, by Håkon Løvdal
- TeX Without Tears!, by Martin Vermeer
- Using a Laptop in Different Environments, by Berd Bavendiek
- Weekend Mechanic, by John M. Fisk
- The Back Page
