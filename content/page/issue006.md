
title:·Wydanie·6
date:·1996-01-01

##·Spis·treści


* Salutations and the MailBag
* An Introduction to Tcl/Tk by Jesper Pedersen
* Still Yet another HACK script by Dean Carpenter
* [Recenzja·Caldera·Network·Desktop](caldera.md)
* FVWM and Audio by Nic Tjirkalli
* Using Strace by Jesper Pedersen
* Connecting Computers via PLIP by James McDuffie
* Using Cron by Brian Freeze
* Knews and crontab tips by Bill Powers
* Emacs tips by Paul Lussier
* ZLess followup and tips by Werner Fleck
* Kscreen followups
* Updating XTerm Titlebar Followups
* Yahoo! Got Sendmail to Queue!!
* Using the Keypad as a VT Switcher
* Colophon