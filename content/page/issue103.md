---
title: Wydanie 103
date: 2004-06-01
comments: false
--- 

## Spis treści

* Skrzynka pocztowa
* Rady za dwa grosze
* The Answer Gang
* Kęs nowości
* Jak Linux zmienia oblicze edukacji w Afryce
* A Bare-Bones Guide to Firewalls
* Firewalling with netfilter/iptables
* Świadomość designu
* Getting a better browsing experience with Mozilla
* Plots, Graphs, and Curves in the World of Linux
* Experiments with Kernel 2.6 on a Hyperthreaded Pentium 4
* Ecol
* Qubism
* [Linus nie jest autorem Linuksa?](/post/103/oregan2/)
* The Foolish Things We Do With Our Computers
* Na zakończenie