---
title: Wydanie 54
date: 2000-06-01
comments: false
--- 

## Spis treści

* The MailBag
* News Bytes
* The Answer Guy
* More 2-Cent Tips
* 0800-LINUX: Creating a Free Linux-only ISP
* HelpDex
* What are Oracle's plans to boost and support the Linux platform?
* General Graphics Interface Project (GGI)
* Creating A Linux Certification Program, Part 9
* CAD Programs for Linux
* First Attempt at Creating a Bootable Live Filesystem on a CDROM
* Introduction to Shell Scripting
* [Specjalne atrybuty metod w Pythonie](/post/54/pramode/)
* [Laptop z preinstalowanym systemem Linux](/post/54/sevenich/)
* Building a Secure Gatway System
* The Back Page