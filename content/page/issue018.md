---
title: Wydanie 18
date: 1997-06-01
comments: false
--- 

## Spis treści

- The Front Page
- The MailBag
- More 2 Cent Tips
- News Bytes
- The Answer Guy, by James T. Dennis
- bash Strng Manipulations, by Jim Dennis
- Brave GNU World, by Michael Stutz
- Building Your Linux Computer Yourself, by Josh Turial
- Cleaning Up Your /tmp, The Safe Way, by Guy Geens
- Clueless at the Prompt: A Column for New Users, by Mike List
- DiskHog: Using Perl and the WWW to Track System Disk Usage, by Ivan Griffin
- dosemu & MIDI: A User's Report, by Dave Phillips
- Graphics Muse, by Michael J. Hammel
- New Release Reviews, by Larry Ayers
- Red Hat Linux: Linux Installation and Getting Started, by Henry Pierce
- SQL Server and Linux: No Ancient Heavenly Connections, But..., by Brian Jepson
- The Weekend Mechanic, by John M. Fisk
- The Back Page
