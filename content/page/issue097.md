---
title: Wydanie 97
date: 2003-12-01
comments: false
--- 

## Spis treści:

* Skrzynka pocztowa.
* Kęs nowości.
* MS Dezerterzy: Dlaczego czasem warto migrować na Linuksa
* MS Dezerterzy: Chyba już nie jesteśmy w Redmond Toto.
* Ecol.
* [Ostatnie wydarzenia oraz działania SSC Inc. dotyczące znaku handlowego](/post/97/events/)
* Używanie modyłu HTML::Template.
* [Przyśpiewki w tonacji Pingwina: Songwrite](/post/97/songwrite/)
* [Używanie DCOP z linii poleceń](/post/97/dcop/)
* Prosta zmiana czasu pulsacji w Linux/RTAI.
* Niech się stanie muzyka.
* Rzut okiem na Jython.
* Poznajemy Mozilla Firebird.