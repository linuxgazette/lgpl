---
title: Wydanie 5
date: 1995-11-01
comments: false
---

## Spis treści


* [Pozdrowienia i Skrzynka Pocztowa](/post/5/mail)
* [Wprowadzenie do DOSEMU](/post/5/dosemu)
* [Wprowadzenie do Garrot](/post/5/garrot)
* [Tworzenie lepszych hack skryptów](/post/5/hack1)
* [Tworzenie jeszcze innego hack skryptu](/post/5/hack2)
* [RCS: Zarządzanie Plikami Konfiguracyjnymi Systemu](/post/5/rcs)
* [Supermount: BARDZO łatwe Montowanie Flopa](/post/5/supermount)
* [Zapisywanie logów z inittab](/post/5/inittab)
* [Więcej zabawy z Setterm](/post/5/setterm)
* [Jeszcze więcej trików w VI](/post/5/vi)
* [Używanie kscreen do czyszczenia Śmieci na Ekranie!](/post/5/kscreen)
* [Interaktywna zmiana paska tytułu xterm!](/post/5/xtitle)
* [Oszczędzanie miejsca z wykorzystaniem dyskietek](/post/5/floppy)
* [Less potrafi więcej!](/post/5/less)
* [Po słowie](/post/5/colophon)