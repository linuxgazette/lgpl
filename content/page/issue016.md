---
title: Wydanie 16
date: 1997-04-01
comments: false
--- 

## Spis treści

* [Skrzynka Pocztowa](/post/16/mailbox/)
* [Więcej rad za dwa grosze](/post/16/2cent/)
* [Trochę Nowości](/post/16/news/)
* [The Answer Guy](/post/16/answer/)
* [Krótkie wprowadzenie do Biblioteki kunf](/post/16/kunf/)
* [Informacje dla początkujących](/post/16/clueless/)
* [CeBit'97, 13-19 Marzec](/post/16/cebit/)
* [Rozwiązanie dynamicznego IP z użyciem Internetowego Konta Geocities](/post/16/dynip/)
* [Graficzne Przemyślenia](/post/16/graphmuse/)
* [LGEI przepytuje Redaktora LG](/post/16/lgei/)
* [Więcej o bezpieczeństwie Linuksa](/post/16/linsec/)
* Przegląd nowych wersji
    - [GV: Alternatywa dla Ghostview](/post/16/gv/)
    - [XEmacs 19.15](/post/16/xemacs/)
* [SuSE Linux - Instalacja i przygotowanie do uruchomienia.](/post/16/suse/)
* [UniForum'97, 12-14 Marzec](/post/16/uniforum/)
* [Weekendowy Mechanik](/post/16/wkndmech/)
* [Na Zakończenie](/post/16/backpage/)
