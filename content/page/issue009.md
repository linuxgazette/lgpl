---
title: Wydanie 9
date: 1996-09-01
comments: false
---

## Spis treści:

* The Front Page
* The MailBag
* More 2 Cent Tips
  * Emacs Control M Trick
  * XTerm Title Trick 2
  * VI Trick -- Commenting Code
  * Newbie Tip on Find
  * Masquerading with SendMail
  * Linux Upgrade
* News Bytes
* Binstats: Finding Unusable Binaries, by Larry Ayers
* The Easy Way to Set Up a Local News Server, by Christophe Blaess
* FileRunner: A New Tk/Tcl File Manager, by Larry Ayers
* Getting Up and Running on StarOffice 3.1, by Dwight W. Johnson
* YODL: A New, Easy-To-Use Text Formatting Language, by Larry Ayers
* The Back Page
