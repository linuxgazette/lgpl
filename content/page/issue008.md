---
title: Wydanie 8
date: 1996-08-01
comments: false
---

## Spis treści:

* Salutations and the MailBag
* More 2 Cent Tips...
* PPPD Tips -n- Tricks, by Baard Johannessen
* The Wily Text Editor, by Gary Capell
* Monitoring a Link with SNMP, by Jason Philbrook
* Using JAVA and Netscape 2+, by Nic Tjirkalli
* Web Surfer!, by Ed Cameron
* TkDesk, FTE, Process Meters, html-helper-mode, and a Whole Lot More!, by Larry Ayers
* Shell Programming, by Geoff Taylor
* The Utility Room, by Jens Wessling
* A Collection of Articles Including:
  * An Introduction to AWK, by Cheng Hian Goh
  * Cheap ANSI Color!, by Jim Valentine
  * DIRED: Distant Relative of GNU 'ls', by Grant B. Gustafson
  * Securing your RM!, by Christophe Blaess
  * TAR'ing over the Net, by Mark A. Bentley
  * Taking Full Advantage of TCSH - precmd, by Ryan
  * Customizing Logins with XDM, by Yann Le Fablec
  * Announcing ZLISTER, by Joe Wulf
* Kernel 2.0 Upgrade, by John M. Fisk
* Colophon
