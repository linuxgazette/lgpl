---
title: Wydanie 87
date: 2003-02-01
comments: false
--- 

## Spis treści

* Skrzynka pocztowa.
* Rady za dwa grosze.
* The Answer Gang.
* Kęs nowości.
* Edytor Ostateczny
* HelpDex.
* Ecol.
* Quick-Start Networking.
* A Keep-Alive Program You Can Run Anywhere.
* Rozpoznawanie mowy w oparciu o Linuksa.
* Perl One-Liner of the Month: The Adventure of the Arbitrary Archives.
* Fun with Simputer and Embedded Linux.
* Qubism
* Yacc/Bison - Parser Generators - Part 1.
* Budowa własnej wersji Debiana na CD dzięki dystrybucji Knoppix.
* Szyfrowanie z użyciem bibliotek OpenSSL.
* Na zakończenie. 