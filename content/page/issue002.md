---
title: Wydanie 2, sierpień 1995
comments: false
---

## Spis treści

* [Pozdrowienia i torba z pocztą](/post/2/welcome/)
* [Teraz, trochę więcej sztuczek z tar'em](/post/2/tar_tricks/)
* [Używasz VI, używaj VIM](/post/2/vim/)
* [Pomocne skrypty dla PPP](/post/2/ppp/)
* [PINE + popclient - poczta dla sendmaila-impaired](/post/2/mail/)
* [Ustawiamy X-y - czyli gdzie co się znajduje?](/post/2/customx/)
* [Ustawiamy X-y z wykorzystaniem vgaset](/post/2/vgaset/)
* [Ustawiamy X-y z wykorzystaniem Xaw3d - to takie proste!](/post/2/xaw3d/)
* [Co zobaczymy w następnym miesiącu?](/post/2/wrapup/)