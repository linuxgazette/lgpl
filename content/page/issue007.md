---
title: Wydanie 7
date: 1996-03-01
comments: false
---

## Spis treści:

* Salutations and the MailBag
* New! Q&A Gopher!
* New! New Programs and Products!
* More 2 Cent Tips...
* Fun with Color ls!
* Colorizing /etc/issue
* Setting up PPP's ip-up and ip-down scripts!
* Two Cent BASH Shell Script Tips
* Customizing the 'ol Login Prompt!
* Review: Caldera Network Desktop Preview II, by Alan Bailward
* CND Desktop Review - Part II, and WebSurfer, by Edward Cameron
* Emacs -- Enhanced Features, by Jesper Pedersen
* Moxfm, Ext2-OS/2, Xvile, Elvis, and the Unclutter Programs!, by Larry Ayers
* The Screen Program, by Borek Lupomesky
* Linuxita: the Minimal Working Set?, by Peter T. Breuer
* Colophon