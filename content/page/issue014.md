---
title: Wydanie 14
date: 1997-02-01
comments: false
---

## Spis treści:

* The Front Page
* The MailBag
* More 2 Cent Tips
* News Bytes
* The Answer Guy
* Clueless at the Prompt: A New Column for New Users
* Cracraft & Lijewski DIRED Programs
* Directory Trees in Outline Format
* Graphics Muse
* [Linus Torvalds otrzyma doroczną nagrodę UniForum](/post/14/linus/)
* Linux Security 101
* New Release Reviews
* Novice Bash Tip -- Edit Command Lines "joe-style"
* Pick an Editor, Any Editor -- VIM
* A Philosophy for Change from DOS to Linux
* Procmail: Automated Mail Handling
* Utilizing the US Robotics Pilot with Linux
* Stronghold: Undocumented Fun
* Usenix/Uselinux in Anaheim
* Weekend Mechanic
* The Back Page