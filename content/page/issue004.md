---
title: Wydanie 4
date: 1995-11-01
comments: false
---

## Spis treści:

* Salutations and the MailBag
* Lots More 2 Cent Tips...
* Useful Entrapment! (letter submitted by Nathan Hand)
* In-depth Guide to XF-Mail and XMailbox Installation
* Configuring XF-Mail and XMailbox
* FVWM: Introduction to Styles!
* Colophon
