---
title: Wydanie 19
date: 1997-07-01
comments: false
--- 

## Spis treści


- The Front Page
- More 2 Cent Tips
- News Bytes
- The Answer Guy, by James T. Dennis
- Adventures in Linux: A Redhat Newbie Boldly Treks Onto the Internet Frontier, by A. Cliff Seruntine
- Atlanta Showcase Report, by Phil Hughes and Todd M. Shrider
- Caldera OpenLinux, by Evan Leibovitch
- Clueless at the Prompt: A Column for New Users, by Mike List
- Graphics Muse, by Michael J. Hammel
- Intranet Hallways Systems Based on Linux, by Justin Seiferth
- Linux and Artificial Intelligence, by John Eikenberry
- Linux: For Programmers Only--NOT!, by Mike List
- New Release Reviews, by Larry Ayers
- Single-User Booting Under Linux, by John Gatewood Ham
- User Groups and Trade Shows: Lessons from the Atlanta Linux Showcase, by Andrew Newton
- Using Python to Generate HTML Pages, by Richie Bielak
- Using SAMBA to Mount Windows, by Jonathon Stroud
- The Back Page
