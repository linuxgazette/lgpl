---
title: Wydanie 99
date: 2004-02-01
comments: false
--- 

## Spis treści

* Skrzynka pocztowa
* Rady za dwa grosze
* The Answer Gang
* Lekka, (prawie) nieszyfrowana zdalna obsługa systemu
* XMLTV
* Zbbudujmy fajną zabawkę Linuksową
* Wstęp do OCaml
