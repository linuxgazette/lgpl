---
title: Dawno temu - Red Hat Linux 3.0.3 - 4.2, 1996 i 1997
date: 2021-04-08
---

* Autor: Anderson Silva
* Tłumaczył: Marcin Niedziela
* Original text: https://linuxgazette.net/185/silva.html

---

Grudzień 1996 roku, właśnie skończyłem swój pierwszy semestr na studiach. W
czasie przerwy świątecznej, postanowiłem wybrać się do domu mojego wujka na Long
Island w stanie Nowy Jork. Mój wujek, doktorant na Uniwersytecie Stony Brook,
pożyczył mi wielka jak cegła książkę, która dostał razem z darmowym systemem
operacyjnym - Red Hat Linux, wersja 3.0.3.

![](/post/185/misc/image07.jpg)

Oryginalna płyta z Red Hat Linux 3.0.3, dodana do książki którą dał mi wujek w
1996 roku.

Luty 2011 roku, jest mroźny zimowy dzień tutaj w Północnej Karolinie, jestem
chory na grypę. Kręcąc się po domu szukałem czegoś do roboty, gdy nagle trafiłem
na coś co jest odpowiedzialne za moją 15-letnią karierę akademicką i aktualną
pracę. Przez głowę przeszła mi myśl: "Ciekawe czy uda mi się zainstalować to na
maszynie wirtualne?".

I o tym właśnie będzie ten nostalgiczny artykuł: podróż do przeszłości Linuksa.
Najpierw próbowałem zainstalować Red Hat Linux 3.0.3 (Picasso) z 1996 roku,
który działa na jądrze 1.2.13. Uruchomienie instalatora wymaga nieco pracy, gdyż
w tamtych czasach nie było jeszcze płyt bootowalnych. Instalacja wymaga trzech
dyskietek, jedna na obraz startowy, dwie na obrazy ramdysku. Udało mi się
załadować wszystkie trzy dyskietki, jednak kernel wysypał się tuż przed
rozpoczęciem instalacji.

![](/post/185/misc/image02.png)

Red Hat Linux 3.0.3: ładowanie systemu.

![](/post/185/misc/image04.png)

Red Hat Linux 3.0.3: błąd instalacji

Niestety, ale nie udało mi się uruchomić wersji 3.0.3 na maszynie wirtualnej
(jak widać powyżej), ale nie zamierzałem się poddawać. Zdecydowałem się
spróbować z kolejną wersją jaką miałem, Red Hat Linux 4.0. Wynik:

Wersja 4.0 była pierwszą oficjalną wersją Red Hat, która była na bootowalnej
płycie CD (chyba, że nie wiem o innych). Udało mi się ją uruchomić i nawet
zainstalować system, ale LILO, program startowy dla systemów Linux używany zanim
pojawił się GRUB, nie chciał się zainstalować... zdecydowałem się zatem spróbować
kolejnej dostępnej wersji.


![](/post/185/misc/image08.png)

Red Hat Linux 4.0 (Colgate, 1996).

![](/post/185/misc/image03.png)

Red Hat Linux 4.0: Wysypał się również.

Nareszcie sukces! Kolejną dostępną wersją był Red Hat Linux 4.2 (Biltmore,
1997). Instalacja była bardzo prosta, poza uruchomieniem serwera X. Kiedy w
końcu wymyśliłem jak go uruchomić w trybie VGA i skonfigurować poprawnie mysz,
mogłem oddać się przyjemności zabawy z systemem. Red Hat Linux korzystał z jądra
w wersji 2.0.30.

![](/post/185/misc/image12.png)

Red Hat Linux 4.2, Biltmore. Manager okien to [FVWM95](http://en.wikipedia.org/wiki/FVWM95).

![](/post/185/misc/image01.png)

Red Hat Linux 4.2: Musiałem użyć panelu sterowania aby podnieść eth0 przez DHCP,
ale zadziałało.

![](/post/185/misc/image00.png)

Red Hat Linux 4.2: Jak widać, nie było wtedy za dużo
dostępnych aplikacji, ale to i tak więcej niż oferował Windows 95. :-)

![](/post/185/misc/image09.png)

Red Hat Linux 4.2: GUI dla managera paczek RPM. Warto przypomnieć, że w 1997
roku nie było jeszcze sieciowych narzędzi aktualizacji takich jak up2date, yum
czy apt-get.

![](/post/185/misc/image06.png)

Red Hat Linux 4.2: Instalowanie aplikacji.

![](/post/185/misc/image11.png)

Red Hat Linux 4.2: Przeglądarka Arena z uruchomioną aktualną wersją strony
Google! :-) W tamtych czasach konieczne było ręczne pobranie przeglądarki
Netscape.

![](/post/185/misc/image05.png)

Red Hat Linux 4.2: Chciałem poprzeglądać internet czymś bardziej zaawansowanym
niż przeglądarka Arena, dlatego poszukałem starej wersji Netscape 4.08 dla jądra
2.0.x. Pamiętaj, że netscape Communicator 4.08 został wydany w listopadzie 1998
roku. W tym czasie był już Red Hat Linux 5.2. Ku mojemu zdziwieniu ta wersja u
mnie działała! Porównaj teraz wygląd przeglądarki Arena (poprzedni zrzut) oraz
Netscape, aby zrozumieć dlaczego Netscape był tak popularny w tamtych czasach.

![](/post/185/misc/image10.png)

Red Hat Linux 4.2: Aktualna wersja Google.com w oknie przeglądarki
Netscape Communicator z 1998 roku.

Chorobowy weekend minionego lutego był całkiem zabawny, gdyż nie poprzestałem na
instalowaniu Red Hat Linux 4.2. Zainstalowałem jeszcze wersje 5.0, 6.1, 7.2, 9
oraz Fedore Core 1 i mam nadzieję opisać swoje doświadczenia w przyszłych
wydaniach Linux Gazette.

