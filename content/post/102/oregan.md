---
title: Przyśpiewki w tonacji Pingwina - więcej o Songwrite
date: 2021-10-15
---

* Autor: Jimmy O'Regan
* Tłumaczył: Maciej Jaśkowski
* Original text: https://linuxgazette.net/102/oregan.html

---

Jako, że miała to być seria artykułów, to aby tak było, potrzeba przynajmniej jeszcze drugiej części. 
Na swoją obronę mam fakt, że nie miałem dostępu do internetu od ostatniego tygodnia listopada -- parę 
dni po tym jak wysłałem swój poprzedni artykuł.

Tymczasem, miałem jednak sporo czasu, żeby lepiej poznać Songwrite. Począwszy od grudnia używam do 
tabulatur wyłącznie tego programu; od tego czasu nie włączyłem Windowsa ani razu. Tak więc pisząc ten 
artykuł mam dwa cele: dokończyć to czego się podjąłem cztery miesiące temu i sprostować nieścisłości 
mojego poprzedniego artykułu.

Songwrite znalazł nowy dom. Można go teraz znaleźć na http://oomadness.nekeme.net/en/songwrite/index.html

## Sprostowania

Na początku najważniejsze -- sprostowania. W swoim poprzednim artykule powiedziałem, że Songwrite nie ma 
opcji zabaw z wajchą (ang. whammy bar dives). To nie do końca jest prawda: Songwrite wspiera downward 
bends (przeciągnięcia w dół), tylko nie do tego stopnia na jaki pozwalałaby wajcha -- Songwrite może 
wspierać przeciągnięcia strun w dół tylko dwóch półtonów. Jeśli spróbujesz przeciągnąć niżej niz ta wartość 
to całkiem możliwe, że zrobi on to za ciebie, jednak przeciągnie tony spowrotem do góry co nie powinno się 
zdarzyć. Szerzej opiszę to trochę później.

Mówiłem również, że Songwrite nie ma opcji zmiany tempa, co jest nie tylko nieprawdą, lecz nie jest również 
tym, co miałem na myśli. Chciałem powiedzieć, że nie można zmienić tempa utworu przy importowaniu plików 
GuitarPro ale, jak się okazuje, można (mimo, że czasem się myli - na przykład na kilku plikach, na których 
testowałem to pisząc ten artykuł, to częściej operacja ta kończy się sukcem). Przy importowaniu plików 
GuitarPro pojawia się jednak pewien problem -- tempo jest za szybkie. Jednak nie jest to wina Songwrite, 
bo pliki MIDI, które konwertowałem przy pomocy GuitarPro są również za szybko odtwarzane.

W końcu, powiedziałem, że Songwrite dopisał ciąg znaków do pliku, na którym pracowałem. Najprawdopodobniej 
zrobiłem to własnoręcznie, bo dalsza praca z Songwrite pokazała, że Songwrite po prostu tego nie robi. 
Utrzymuje on jednak nuty na właściwych liniach pięciolini, tak więc nuty mogą zostać przeciągnięte na właściwe lokalizacje.

Przy używaniu TiMidity z Songwrite trzeba uważać, bo dźwięk "Distortion Guitar" jest o oktawę za niski. 
To jednak nie jest błąd Songwrite, chociaż Songwrite używa Timidity do odtwarzania utworów (można jednak 
użyć playmidi albo innego odtwarza plików MIDI). Również nie jest to błąd Timidity, lecz soundfontów 
(banków zawierających próbki instrumentów midi -- przyp. naczelny). Gitara jest tradycyjnie transkrybowana 
oktawę wyżej niż brzmi (i pewnie dlatego zwykło się pod kluczem wiolinowym pisać 8 ;) przyp. tłum.) i ktokolwiek 
dodał brzmienie "Distortion Guitar" zrobił to trochę jakby na siłę.

## Podstawy tabulatury

Podstawy sposobu notacji już omówiłem, choć warto przypomnieć, że możesz "chodzić" po tabulaturze za pomocą 
strzałek i nie musisz tym samym sięgać po mysz za każdym razem, gdy chcesz wpisać nutę. Większość właściwości 
jest dostępna z klawiatury -- aby dodać hammer-on czy pull-off, wciśnij 'h'; aby dodać slide'y, wciśnij 's' 
(żeby skorzystać z tych efektów musisz określić obiekt, na którym chcesz z nich skorzystać, Songwrite nie 
pozwoli ustawić hammer-on pustce); aby dodać tremolo, 't'; podciągnięcie (ang. bend), 'b'; roll, 'r'; aby 
zrezygnować z efektów: 'n' (od ang. normal przyp. tłum). Możesz również ustawić właściwości nut, klikając LPM 
dwa razy na nutę i wybierając opcję, którą chcesz zmienić albo wybierając 'Nuty-\>Właściwości...' 
(org. Notes-\>Properties...). Możesz również podnieść lub obniżyć wysokość dźwięku, wciskając '+' lub '-' 
odpowiednio lub po prostu naciskając 'Enter', żeby uzyskać otwartą notację (Uwaga: chodzi o 'Enter' na klawiaturze 
numerycznej, drugi 'Enter' oznacza nutę jako akcentowaną (org. strong)).

Gdy ustawiasz podciąg, używając klawiatury czy właściwości nuty, możesz potem wybrać właściwości nuty (ang. 
note properties), aby ustawić "wielkość" podciągu; 0.5 to pół tonu, 1.0 to cały ton; -1.0 daje podciąg w przeciwną 
stronę o cały ton itd. Nie zapomnij, że jeśli używałeś właściwości nuty, aby ustawić podciąg, musisz zamknąć to 
okno i otworzyć je ponownie. Songwrite obsługuje przeciągnięcia tonów pomiędzy -1.0 i 1.0 bez wygięcia w 
przeciwnym kierunku, tak więc uważaj!

## Wstawianie nut

Wstawianie nut jest w Songwrite bardzo proste, jak już mówiłem. Zamiast zmuszać użytkownika do wpisywania 
dźwięków nuta po nucie (ang. linearly), Songwrite pozwala wkleić nutę tam, gdzie jest potrzebna i nie trzeba 
wklepywać całej reszty - można po prostu zostawić puste miejsce.

Gdy wklejamy lub przenosimy nuty za pomocą myszki, czas trwania zaznaczonej nuty determinuje gdzie możesz je 
wkleić. Dla większości kawałków, oznacza to konieczność zmniejszenia czasu trwania nuty do szesnastek przed 
wklejeniem/przeniesiem fragmentu. Jeśli o tym zapomnisz, nie martw się! Songwrite ma możliwości Cofnij/Ponów 
(ang. Undo/Redo).

## Nuty powiązane

Wprowadzanie połączeń między nutami jest tak proste i oczywiste w porównaniu z innymi programami do tabulatur 
jakie używałem: Nie musisz wiązać nut. Jeśli masz jakiś synkopowany riff, który zaczyna się z ósmemki i grany 
potem szesnastkami, to w prosty sposób umieszczasz szesnastkę przed końcem poprzedniego taktu i wstawiasz tam 
ósemkę. Jeśli w akordach używasz hammer-ons lub pull-offs możesz ustawić sobie czas trwania całego akordu i w 
prosty sposób zmieniać nutę w miejscu gdzie trafia się hammer-on. Songwrite wstawi je wtedy do pozostałych 
akordów w momencie eksportowania do Lilypond lub MIDI.

## Strojenie

Songwrite jest domyślnie ustawiony na standardowe strojenie (EADGBE), które odpowiada większości gitarzystów, 
ale ja używam wielu strojeń.

Okno strojeń w Songwrite (dostępne dla każdego instrumentu z menu "właściwości" (ang. Properties), ponad 
pięciolinią) jest łatwe do zrozumienia: wybierz strunę, którą chcesz stroić i przesuń suwak do odpowiedniego 
dźwięku. Jest jednak pewien problem - niektóre dźwięki nie pojawiają się na suwaku! Na przykład jeśli chce 
użyć strojenia Drop-D, chciałbym móc przesunąć suwak do 'D', ale on przeskakuje bezpośrednio z Dis (D#) do Des 
(Db). Żeby to obejść, próbowałem najpierw tworzyć pusty plik, zapisać go, a potem ręcznie edytować, żeby dostać 
odpowiednie strojenie. Ale to było bardzo męczące i zdecydowałem się zmienić źródło i dodać strojenia, których 
często używam.

Ponieważ Songwrite jest napisany w Pythonie, źródło jest zainstalowane standardowo, tak więc nie musiałem 
szukać tarpaczki. Domyślnie Songwrite ma dwa standardy strojenia dostępne z menu 'Partition' -- 'Guitar Standard' 
(standard gitarowy) i 'Guitar DADGAD' (gitara DADGAD). 'DADGAD' wydałowo się być unikalne i jedyne przypadkowo 
znalazłem tam to, czego szukałem: miejsce, gdzie strojenia są zdefiniowane -- tablature.py

```py
guitar_view_type = view.ViewType("tab", _("Guitar"       ), Tablature, \
{ "strings_args": ((64, 0), (59, 0), (55, 0), (50, 1), (45, 1), (40, 1)) }, 24)
view.ViewType("tab", _("Guitar DADGAD"), Tablature, { "strings_args": \
((62, 0), (57, 0), (55, 0), (50, 1), (45, 1), (38, 1)) }, 24)
view.ViewType("tab", _("Bass"         ), Tablature, { "strings_args": \
((43, 0), (38, 0), (33, 1), (28, 1)) }, 33)
view.ViewType("tab", _("Banjo 5G"     ), Tablature, { "strings_args": \
((62, 0), (59, 0), (55, 0), (50, 0), (Banjo5GString, 67, 0)) }, 105)
```

No... nigdy wcześniej nie używałem Pythona, ale to wyglądało wystarczająco prosto, więc zanurzyłem się w tym 
i zrobiłem własne zmiany (po zrobieniu kopi oryginalnego pliku oczywiście), puściłem Songwrite i mogłem się 
cieszyć z moich nowych strojeń.

```py
guitar_view_type = view.ViewType("tab", _("Guitar"       ), Tablature, { \
"strings_args": ((64, 0), (59, 0), (55, 0), (50, 1), (45, 1), (40, 1)) }, 24)
view.ViewType("tab", _("Guitar DADGAD"   ), Tablature, { "strings_args": \
((62, 0), (57, 0), (55, 0), (50, 1), (45, 1), (38, 1)) }, 24)
# Helmet, Metallica, Nirvana, Van Halen etc.
view.ViewType("tab", _("Guitar Dropped D"), Tablature, { "strings_args": \
((64, 0), (59, 0), (55, 0), (50, 1), (45, 1), (38, 1)) }, 24)
# Hendrix, Slayer etc.
view.ViewType("tab", _("Guitar Eb"       ), Tablature, { "strings_args": \
((63, 0), (58, 0), (54, 0), (49, 1), (44, 1), (39, 1)) }, 24)
# My crappy Strat copy. I've set this 'cause the tuning dialogue won't let me
# set the correct tuning on the G string. Set to overdriven guitar by default.
# Slayer's later albums use this tuning.
view.ViewType("tab", _("Guitar C#"       ), Tablature, { "strings_args": \
((61, 0), (56, 0), (52, 0), (47, 1), (42, 1), (37, 1)) }, 29)
view.ViewType("tab", _("Bass"            ), Tablature, { "strings_args": \
((43, 0), (38, 0), (33, 1), (28, 1)) }, 33)
#My brother has a 5 string bass as well as a normal bass.
view.ViewType("tab", _("5 String Bass"   ), Tablature, { "strings_args": \
((43, 0), (38, 0), (33, 1), (28, 1), (23, 1)) }, 33)
#I read about this in Guitar World. Possibly an April issue :)
view.ViewType("tab", _("Bassitar"        ), Tablature, {"strings_args": ((43, 0), \
(64, 0), (59, 0), (38, 0), (55, 0), (50, 1), (33, 1), (45, 1), (50, 1), (28, 1)) }, 33)
view.ViewType("tab", _("Banjo 5G"        ), Tablature, { "strings_args": \
((62, 0), (59, 0), (55, 0), (50, 0), (Banjo5GString, 67, 0)) }, 105)
```

Jedyne fragmenty tego kodu, które tak naprawdę rozumiem, to "Bassitar", "Banjo" itd., które są nazwami 
pojawiającymi się w menu, a to (64, 0) są szczegóły strojenia jedna para na strunę, zaczynając od najniższej; 
numer nuty, który pojawia się w oknie strojenia); i kierunek strojenia (ang. tuning direction) (który zdaje 
się niczego nie zmieniać).

## Teksty

Wstawianie tekstów jest tak proste i logiczne jak wszystko inne - aby dodać teksty, musisz najpierw wpisać 
partię wokalu (ang. vocal). Songwrite dopasowuje potem słowa poniżej do odpowiednich dźwięków. Naciskając 
spację lub Tab przemieszczasz kursor poniżej następnej nuty, podobnie gdy wpiszesz '-', co dzieli słowo. 
'Enter' przenosi cię do następnej linii (choć trzeba wklepać jeszcze podwójny odwrócony ukośnik przed nową 
linią, żeby przy drukowaniu wszystko pojawiało się poprawnie - patrz niżej).
Właśwości utworu

Tak jak większość innych programów tabulaturowych, Songwrite daje możliwość dodania informacji o utworze. 
W przeciwieństwe do innych programów, Songwrite używa Lilyponda i LaTeXa do drukowania. Miłym efektem ubocznym 
tego rozwiąznia jest to, że możesz pisać całe artykuły w Songwrite, ciekawa alternatywa do LaTeXa (co wyjaśnia
dlaczego trzeba w tekście dodawać podwójne odwrócone ukośniki).

## Gnometab

Na końcu swojego poprzedniego artykułu krótko wspomniałem o Gnometab; Pomyślałem sobie, że znowu o nim wspomnę. 
Gnometab jest zupełnie inny niż Songwrite: może być użyty tylko do ułożenia tabulatury - nie ma możliwości 
playbacku, więc jeśli nie wiesz jakich użyć długości dźwięków, nie możesz użyć go efektywnie.

Jeśli jednak chcesz drukować tabulatury na poważnie, Gnometab robi to lepiej niż Lilypond (którego Songwrite 
używa do drukowania); i jeśli już skończysz swoje dzieło używając Songwrite, nietrudno jest zrobić to ponownie 
za pomocą Gnometab niezależnie od twojej wiedzy muzycznej.

## Przykłady

Wykonałem nieco przeróbek przykładów, które towarzyszyły mojemu poprzedniemu artykułowi, ale głównie tam, 
gdzie zapomniałem dodać kawałka, albo żeby coś poprawić. Od tego czasu wpadłem na parę riffów, które zapisałem 
od razu w Songwrite. Możesz je znaleźć tutaj.

## Wnioski

Jak już wspomniałem, nie miałem dostępnu do internetu przez jakiś czas - właściwie to przez 6 miesięcy - 
a ten artykuł wtedy właśnie napisałem. Po napisaniu tego artykułu, udało mi się ponownie uzyskać dostęp do 
sieci i postanowiłem go opublikować.

* Jeśli masz urządzenie Palm Pilot i grasz na gitarze, może zainteresuje Cię [Fretboard](http://www.mennigmann.com/fretboard/), 
aplikacja pokazująca kilka różnych skali i strojeń.
* Jeśli używałeś Songwrite, może cię zainteresować to, że Songwrite używa Pythonowego httplib do otwierania 
plików, więc za pomocą Songwrite możesz otwierać pliki bezpośrednio z internetu.
* [Tablature](http://tablature.sourceforge.net/) jest bazującym na Javie (i przez to niezależnym od platformy) edytorem tabulatur. Jednak posiada 
tylko podstawowe opcje. Ponieważ Songwrite jest napisany w Pythonie, z Tk jako GUI, powinien być uruchamialny 
na każdej platformie, na której możesz uruhomić Javę. 

Następny artykuł w tej serii (a będzie jeszcze jeden, serio!!) będzie zawierał utwory: pokusy oferowane przez 
[Ardour](http://ardour.org/) i [Ecasound](http://www.wakkanet.fi/~kaiv/ecasound/welcome.html), połączone z odkryciem etykiety 
zapisu [co nie jest takie straszne](http://www.magnatune.com/). Dopiero nad tym pracuję, tak więc ciąg dalszy nastąpi!
