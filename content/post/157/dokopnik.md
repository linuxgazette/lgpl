---
title: Recenzja książki - CUPS Administrative Guide
date: 2021-10-15
---

* Autor: Deividson Luiz Okopnik
* Tłumaczył: Marcin Niedziela
* Original text: https://linuxgazette.net/157/dokopnik.html

---

    CAG.RVW 20081130
    ================
    %A Ankur Shah
    %C Birmingham, UK
    %D 2008
    %G ISBN-13: 978-1-84719-258-5
    %I Packt Publishing Ltd.
    %P 225 pages
    %T "CUPS Administrative Guide"
    %O http://packtpub.com

---

"Czy jest tak dużo do administrowania w CUPS, że wymaga to 225-stronicowej
książki?" To było pierwsze pytanie, które przyszło mi do głowy, kiedy
zdecydowałem się przejrzeć tę książkę. Po jej przeczytaniu tej recenzji, 
możesz zauważyć, tak jak ja, że jest więcej niż wystarczająco dużo
materiału.

Książka zaczyna się od szybkiego przeglądu funkcji CUPS i części historii
drukowania Linuksa. Następnie, rozdział 2 pokazuje jak zrobić prostą
instalację CUPS i podstawową konfigurację drukarki.

Rozdział 3 prowadzi Cię przez wszystkie podstawowe kroki zarządzania 
drukarkami, w tym dodawanie i usuwanie drukarek oraz zarządzanie zadaniami 
drukowania za pomocą linii poleceń. Następnie pokazuje, jak korzystać 
z interfejsu WWW do wykonywania tych samych zadań - innymi słowy, podstawowej
administracji CUPS.

Zaczynając od rozdziału 4, książka ta zaczyna błyszczeć: opisuje kilka
zaawansowanych funkcji CUPS - nawet te drobne "szczegóły"! W rozdziale
4, "Zarządzanie wieloma drukarkami jednocześnie", książka wyjaśnia jak takie
zarządzanie jest wykonywane, jakie są klasy drukarek, zalety i wady ich
używania, jak tworzyć konfiguracje, jak konfigurować przy użyciu zarówno
interfejsu wiersza poleceń, jak i interfejsu WWW, kiedy używać a kiedy nie
używać różnych funkcji.

Inne poruszane tematy obejmują zarządzanie serwerem CUPS, szczegóły dotyczące
plików konfiguracyjnych CUPS, konfigurację klientów, w tym klientów Windows z
Samba, limity, monitorowanie serwera, filtrowanie wydruków na użytkownika,
bezpieczeństwo i wiele innych.

Książka wyjaśnia te dodatkowe funkcje z poziomem szczegółowości, który naprawdę
zrobił na mnie wrażenie: tak, naprawdę jest wystarczająco dużo dobrego materiału
na temat administracji CUPS, aby wypełnić 225-stronicową książkę.

Kończąc recenzję, książka oferuje wiele informacji. Więcej niż tylko informacji,
daje Ci ona wiedzę o kimś, kto wydaje się administrować CUPS od dłuższego
czasu. Naprawdę warta polecenia.

Więcej informacji o książce można znaleźć na tej stronie:
http://www.packtpub.com/printing-with-cups-common-unix-printing-system/book.

