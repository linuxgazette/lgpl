---
title: Sztuczki z generatorami w Pythonie
date: 2021-10-15
---

* Autor: Pramode C.E.
* Tłumaczył: Marcin Niedziela
* Original text: https://linuxgazette.net/100/pramode.html

---

Obsługa generatorów w języku programowania Python opisana jest w 
[PEP 255](http://www.python.org/peps/pep-0255.html). Artykuł ten demonstruje 
kilka prostych programów, które wykorzystują tę funkcję do rzeczy, takich jak 
odfiltrowywanie liczb pierwszych, reprezentacji "skończonej" ekspansji serii 
w sposób skończony, stosowanie "akceleratora" Eulera, aby seria zbiegała się 
szybciej itp. Wiele z prezentowanych tu programów zostało zaczerpniętych z 
**test_generators.py**, który jest dostępny w dystrybucji źródłowej Pythona. 
Kilka pomysłów zostało skradzionych z klasyki informatyki, 
[Struktury i Interpretacji Programów Komputerowych](http://mitpress.mit.edu/sicp).


## Czym jest generator

W uproszczeniu, generator jest to funkcją, która może zatrzymać się swoje
wykonanie w dowolnym momencie, zwrócić wartość do funkcji wołającej, 
a następnie wznowić od miejsca, w którym się "zatrzymała" i kontynuować jakby
nic się nie stało. Oto prosty przykład:

```py
from __future__ import generators

def foo():
    print 'hello'
    yield 1
    print 'world'
    yield 2
```

Używam Pythona 2.2 - aby skorzystać z funkcji generatora, na samym początku 
pliku należy umieścić specjalną instrukcję "import". Może ona nie być 
wymagana w późniejszych wersjach Pythona.

Zwróć uwagę na słowo kluczowe *yield*. Funkcja, która zawiera słowo *yeld*
gdziekolwiek w swoim ciele traktowana jest przez interpreter Pythona za 
specjalną - jest traktowana inaczej niż zwykłe funkcje. Zobaczmy jak:

```py
>>> from gen1 import *
>>> a = foo()
>>> print a
<generator object at 0x8158db8>
```
Warto zauważyć, że wywołanie funkcji nie spowodowało jej wykonania. 
Zamiast tego, interpreter Pythona zwrócił nam "obiekt generatora". Jest to 
jedna z konsekwencji użycia słowa `yield` w funkcji. Co można z tym zrobić?

```py
>>> a.next()
hello
1
>>> a.next()
world
2
>>> a.next()
Traceback (most recent call last):
 File "<stdin>" line 1, in ?
StopIteration
```

Wywołanie `a.next()` spowodowało rozpoczęcie wykonywania funkcji - wypisuje 
ona `hello` i zatrzymuje swoje działanie przy słowie `yield`, zwracając 
wartość 1. Funkcja wróciła do obiektu ją wołającego, ale jej stan został
w pełni "wstrzymany". Kolejne wywołanie `a.next` skutkuje ponownym 
uruchomieniem funkcji od miejsca, w którym zatrzymała się wcześniej - wypisuje 
ona `world` i zatrzymuje się po zwróceniu wartości 2. Kolejne wywołanie 
`a.next` skutkuje zakończeniem działania funkcji - ponieważ nasza funkcja 
jest funkcją specjalną generatora, spowoduje to zwrócenie wyjątku StopIteration.

Spróbujmy teraz uruchomić pętlę for na naszym generatorze:

```py
>>> a = foo()
>>> for i in a:
...    print i
...
hello
1
world
2
```

Pętla for powoduje wywołanie `a.next()` i przypisanie zawróconej wartości
do zmiennej `i`, której wartość jest następnie drukowana na ekranie. 
Łańcuch `hello` i `world` są drukowane jako element wykonania funkcji `foo`.
Ciekawe efekty zwraca także wywołanie funkcji `list` na obiekcie generatora -
otrzymamy listę w postaci `[1,2]`. W obu przypadkach (pętla for oraz list),
iteracja zatrzymuje się w momencie wywołania wyjątku StopIteration.

Ciało funkcji generatora nie może zawierać instrukcji "return" w formie 
`return expr` - sam `return` jest dopuszczalny. 
[PEP](http://www.python.org/peps/pep-0255.html) można znaleźć więcej na ten
temat, jak i wiele więcej informacji. Uruchom teraz następujący kod:

```py
from __future__ import generators

def foo(n):
    if (n < 3): yield 1
    else: return
    yield 2
```

Spróbuj uruchomić pętlę for na obiekcie generatora zwróconego przez `foo(10)`
i `foo(1)`, spróbuj wywołać `next()` na tych obiektach.

## Reprezentacja sekwencji nieskończonych

Generatory prezentują kilka zabawnych sposobów na manipulowanie nieskończonymi 
sekwencjami - choć niektórzy mogą kwestionować ich praktyczną użyteczność! 
W naszym przypadku, sama zabawa jest wystarczającym powodem!

```py
from __future__ import generators

def foo():
    i = 0
    while 1:
        yield i
        i = i + 1
```

Kod powyżej to najprostszy możliwy "nieskończony" generator. Spróbuj wywołać 
`next()` na obiekcie generatora zwróconym przez `foo`. Podaj ten obiekt jako 
argument dla pętli - będzie ona drukować kolejno numery. Jeżeli chcesz aby
Python zjadł całą Twoją dostępną pamięć, spróbuj uruchomić `list(foo())`. 
Spróbuj napisać bardziej interesującą funkcję, powiedzmy generator serii 
Fibonacciego.

Oto nieskończona seria na przemian pozytywnych i negatywnych wartości:

```
1 - 1/3 + 1/5 - 1/7 + ...
```

Ta seria zbiega się do PI/4. Napiszemy dla niego generator Pythona.

```py
def pi_series():
    sum = 0
    i = 1.0; j = 1
    while(1):
        sum = sum + j/i
        yield 4*sum
        i = i + 2; j = j * -1
```

Każda instrukcja `yield` zwraca lepsze przybliżenie wartości PI. Przetestuj 
je wywołując `next` na generatorze zwróconym przez wywołanie `pi_series`. 
Zauważamy, że seria nie zbiega się bardzo szybko.

Pomocna może okazać się funkcja, która zwracałaby pierwsze wartości *N*
uzyskane przez generator.

```py
def firstn(g, n):
    for i in range(n):
        yield g.next()
```

Zwróć uwagę, że pierwszy argument tej funkcji jest obiektem generatora. 
Oto co otrzymałem dla `list(firstn(pi_series(), 8))`:

```py
[4.0, 2.666666666666667, 3.4666666666666668, 2.8952380952380956, 
3.3396825396825403, 2.9760461760461765, 
3.2837384837384844, 3.0170718170718178]
```

Możemy zastosować "akcelerator sekwencji" do konwersji serii terminów na 
nową serię, która znacznie szybciej zbiega się z oryginalną wartością. 
Jeden z takich akceleratorów, wynaleziony przez Leonharda Eulera jest 
przedstawiony poniżej:

![Sn+1 - \[(Sn+1 - Sn)\*(Sn+1 - Sn)\]/\[Sn-1 - 2\*Sn +
Sn+1\]](/post/100/misc/euler.jpg)

`(Sn+1)` oznacza *(n+1)tych* wartości, `(Sn-1)` - *(n-1)tych* wartości.

Jeśli *Sn* jest wartością *n-tą* pierwotnej sekwencji to sekwencja przyspieszona 
ma wartości jak pokazano w powyższym równaniu.

Spróbujmy napisać funkcję generatora, która akceptuje obiekt generatora 
i zwraca "przyspieszony" obiekt generatora.

```py
def euler_accelerator(g):
    s0 = g.next() # Sn-1
    s1 = g.next() # Sn
    s2 = g.next() # Sn+1
    while 1:
        yield s2 - (sqr(s2 - s1))/(s0 - 2*s1 + s2)
        s0, s1, s2 = s1, s2, g.next()
```

Oto co dostałem, gdy próbowałem wydrukować kilka pierwszych terminów tej serii:

```py
[3.166666666666667, 3.1333333333333337, 3.1452380952380956, 
3.1396825396825401, 3.1427128427128435, 3.1408813408813416, 
3.1420718170718178, 3.1412548236077655]
```
Zwróć uwagę, że seria zbiega się znacznie szybciej! Oto cały program:

```py
# Euler accelerator, from SICP

from __future__ import generators

def sqr(x):
    return x*x

def firstn(g, n):
    for i in range(n):
        yield g.next()

def pi_series():
    sum = 0
    i = 1.0; j = 1
    while(1):
        sum = sum + j/i
        yield 4*sum
        i = i + 2; j = j * -1

def euler_accelerator(g):
    s0 = g.next() # Sn-1
    s1 = g.next() # Sn
    s2 = g.next() # Sn+1
    while 1:
        yield s2 - (sqr(s2 - s1))/(s0 - 2*s1 + s2)
        s0, s1, s2 = s1, s2, g.next()

if __name__ == '__main__':
    print list(firstn(euler_accelerator(pi_series()), 8))
```


## Sito Eratostenesa

Sito jest ciekawym sposobem na "odfiltrowanie" liczb pierwszych, wymyślonym
przez aleksandryjskiego matematyka Eratostenesa. Działa ono w następujący
sposób: Załóżmy, że chcesz poznać wszystkie liczby pierwsze poniżej, 
powiedzmy, 1000. Najpierw anulujesz z listy 1..1000 wszystkie wielokrotności 2 
(z wyjątkiem 2). Teraz anulujesz wszystkie wielokrotności 3 (z wyjątkiem 3). 
4 zostało już anulowany, ponieważ jest to wielokrotność 2. Teraz zdejmiesz 
wszystkie wielokrotności 5, z wyjątkiem 5. I tak dalej. Ostatecznie, to co 
pozostanie na liście to liczby pierwsze!

Zacznijmy od generatora, który zwraca nam wszystkie liczby całkowite:

```py
def intsfrom(i):
    while 1:
        yield i
        i = i + 1
```

Następnie, piszemy generator, który eliminuje wszystkie wielokrotności
liczby `n` z sekwencji:

```py
def exclude_multiples(n, ints):
    for i in ints:
        if (i % n):
            yield i
```
Wywołanie generatora, powiedzmy, 
`list(firstn(exclude_multiples(2, intsfrom(1)), 5))`, da nam listę `
[1,3,5,7,9]`. Teraz czas na stworzenie sita.

```py
def sieve(ints):
    while 1:
        prime = ints.next()
        yield prime
        ints = exclude_multiples(prime, ints)
```

Oto cały program:

```py
from __future__ import generators

def firstn(g, n):
    for i in range(n):
        yield g.next()

def intsfrom(i):
    while 1:
        yield i
        i = i + 1

def exclude_multiples(n, ints):
    for i in ints:
        if (i % n): yield i

def sieve(ints):
    while 1:
        prime = ints.next()
        yield prime
        ints = exclude_multiples(prime, ints)

if __name__ == '__main__':
    for i in firstn(sieve(intsfrom(2)), 400):
        print i
```


## Generatory zwrotne

Funkcje generatora mogą wywoływać się rekurencyjne. Przyzwyczajenie się do 
tego zajmuje trochę czasu. Spróbujmy przeanalizować sposób działania 
następujących funkcji:

```py
from __future__ import generators

def abc():
    a = deff()
    for i in a:
        yield i
    yield 'abc'

def deff():
    a = ijk()
    for i in a:
        yield i
    yield 'deff'

def ijk():
    for i in (1,2,3):
        yield i
    yield 'ijk'
```

Wywołanie `abc` da obiekt generatora. Wywołanie na nim `next` spowoduje, że 
`abc` rozpocznie wykonywanie. Pierwsza linia w `abc` woła `deff`, która 
zwraca obiekt generatora. Następnie, wołane jest `a.next()`, która jest
pierwszą iteracją pętli for. Powoduje to rozpoczęcie wykonywana w `deff`.
Ciało metody `deff` tworzy obiekt generatora poprzez wywołanie `ijk` 
i wywołanie jego metody `next` jako część pętli for. Powoduje to rozpoczęcie
wykonywania `ijk` i zwrócenie 1, `deff` zwraca 1, `abc` także zwraca 1.
Wywołanie metody `next` (na obiekcie generatora wołanego przez `abc`) przez
kolejne dwa razy powoduje zwrócenie wartości 2 i 3. 
Następne wywołanie spowoduje, że ciąg "ijk" propagował będzie stos połączeń,
ponieważ pętla for w ciele `ijk` została przerwana. 
Następne wywołane `next` spowoduje, że ciało metody `ijk` zostanie zakończone
tak, że pętla for w `deff` wywoła wyjątek StopIteration, co spowoduje 
zakończenie tej pętli i funkcję dającą ciąg "deff". 

Kolejne wywołanie `next` spowoduje, że `abc` zostanie zwrócone do metody 
wywołującej najwyższego poziomu. Końcowe wywołanie `next` (zauważ, że ponownie 
wywołujemy `next` na obiekcie zwróconym przez wywołanie `abc`) spowoduje, że 
metoda wołająca otrzyma wyjątek StopIteration, ponieważ w ciało `abc` również 
zostało wykonane w całości.

Spójrzmy teraz na przykład drzewa binarnego Guido. Klasyczny sposób przejścia
poprzecznego drzewa binarnego jest zakodowany poniżej: 

```py
def inorder(t):
    if t:
        for x in inorder(t.left):
            yield x
        yield t.dat
        for x in inorder(t.right):
            yield x
```

Pomyślmy o wywołaniu porządku w drzewie z tylko jednym węzłem (powiedzmy 
zawierającym ilość danych 50). Wykonanie `for x in inorder(t.left)` można 
zapisać jako:

```py
a = inorder(t.left)
for x in a:
    ....
```

Ponieważ `t.left` ma wartość 0, wywołanie `a.next()` (co robi pętla for) 
skutkuje wyjątkiem StopIteration - co natychmiast kończy pętlę. Następna 
instrukcja w to `yield t.dat` - zwraca 50. Następna instrukcja w pętli 
również kończy się natychmiastowo z powodu wystąpienia StopIteration. 
Wizualizowanie jak ten kod zadziała dla bardziej złożonych struktur
drzewiastych nie powinno nastręczyć wielu problemów. 

Pełny kod:

```py
from  __future__ import generators

class node:
    def __init__(self, dat):
        self.dat = dat
        self.left = 0
        self.right = 0


def inorder(t):
    if t:
        for x in inorder(t.left):
            yield x
        yield t.dat
        for x in inorder(t.right):
            yield x


if __name__ == '__main__':
    h = node(50)
    h.left = node(40) ; h.right = node(60)
    h.left.right = node(45); h.right.left = node(55)

    for x in inorder(h):
        print x
```

## Detektor przejścia przez zero

Zdefiniujmy "sygnał" jako potok dodatnich i ujemnych liczb całkowitych.

```
1 2 -1 -4 3 2 -3 -4 2 3 4 -2 ...
```

Detektor przejścia przez zero zwraca sygnał, który opisuje przejście przez
zero sygnału wejściowego - sygnał wynikowy jest +1 kiedy sygnał wejściowy 
zmienia się z ujemnego na dodatki, -1 gdy sygnał wejściowy zmienia się z
dodatniego na ujemny, a 0 w każdym innym wypadku. Zakładamy, że 0 jest
dodatnie.

Oto detektor przejścia przez zero:

```py
def zerocross(g):
    a = g.next()
    b = g.next()
    while 1:
        yield cross_detect(a, b)
        a, b = b, g.next()
```

Jeżeli sygnał pochodzi z czujnika, szumy doprowadzą do błędnego przejścia
przez zero. Możemy więc pomyśleć o "wygładzeniu" sygnału (przy pomocy 
jakiegoś wyliczenia "średniej kroczącej") i wtedy wykryć przejście przez
zero.

Pełny kod:

```py
from __future__ import generators
import random


def firstn(g, n):
    for i in range(n):
        yield g.next()

def signal():
        while 1:
            yield random.randrange(-5, 5)

def cross_detect(a, b):
    if ((a > 0) and (b < 0)): return -1
    elif ((a < 0) and (b > 0)): return 1
    else: return 0

def zerocross(g):
    a = g.next()
    b = g.next()
    while 1:
        yield cross_detect(a, b)
        a, b = b, g.next()

def smooth(g):
    a = g.next()
    b = g.next()
    while 1:
        yield (a+b)/2.0
        a, b = b, g.next()
```

## Podziękowania

Większość kodu została "pożyczone" z pliku `test_generators.py`, który 
pochodzi z dystrybucji źródłowej Pythona. Podziękowania dla społeczności 
Pythona za wiele godzin przyjemnego czytania kodu, oraz za stworzenie 
najlepszego języka programowania na świecie! Podziękowania dla autorów 
[SICP](http://mitpress.mit.edu/sicp), za udostępnienie w sieci takiego 
klasycznego języka za darmo!

