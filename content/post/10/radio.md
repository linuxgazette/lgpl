---
title: Packet Radio i Linux
date: 2022-02-09
---

* Autor: Phil Hughes
* Tłumaczył: Marcin Niedziela
* Original text: https://linuxgazette.net/issue10/radio.html

---

Tegoroczna konferencja sponsorowana przez ARRL/TAPR, na temat
komunikacji cyfrowej odbyła się w Seattle w dniach 20-22 września. 
Jako że komunikacją cyfrową bardzo się interesuję i byłem 18 mil od
konferencji, zdecydowałem się wziąć w niej udział. Zaproponowałem również, 
że przyniosę kilka magazynów Linux Journal do rozdania. Pod koniec 
konferencji "kilka" urosło do 100.

Prezentowane wystąpienia różniły się od materiału wprowadzającego do 
poważnego spojrzenia na sposób wyrównywania grupowego opóźnienia filtrów IF. 
Wiele referatów i jeden z trzech warsztatów dotyczył systemu zwanego 
Automatic Position Report System. Dla tych, którzy go nie znają, 
odbiornik GPS jest połączony ze stacją pakietową do wysyłania raportów 
o pozycji.

Jednak celem tego artykułu nie jest mówienie o krótkich wystąpieniach (jeśli 
chcesz uzyskać więcej informacji na ten temat, zajrzyj na 
http://www.tapr.org/ ) ale porozmawiaj o słowie na L.

Podczas pierwszych warsztatów (na temat APRS) Keith Sproul zademonstrował 
wersję dla systemu Windows i Mac, ale regularnie powoływał się na fakt, 
że istnieje również wersja dla systemu Linux. Byłem zaskoczony (ponieważ 
nie było wzmianki o wersji na Sun ani żadnej innej wersji podobnej do 
Uniksa), ale byłem teraz pewien, że Linux dość poważnie zinfiltrował 
społeczność radiową.

Słowo „na L” nadal pojawiało się w dyskusjach z ludźmi. Różniło się to 
od komentarzy na temat instalacji Linuksa do tego, jak Linux stał się 
znaczącą częścią sieci krótkiej. Na przykład w prezentacji Barry'ego 
McLarnona i Dennisa Rosenauera na temat sieci bezprzewodowych przy 
użyciu technologii modemu RF WA4DSY 56K Barry powiedział: "Linux jest 
platformą z wyboru". Później, opisując Ottawa MAN (Metropolitan Area 
Network), wskazał, że ich serwer internetowy (hydra.carleton.ca) działa
na Linuksie, a oni, mówiąc o maszynie bramie pakietów, powiedzieli: 
"Nie został on skonwertowany jeszcze na Linuksa".

Na zakończenie ich prezentacji mieliśmy okazję pobawić się siecią 
bezprzewodową ustawioną na sali. Maszyna na końcu łącza 56K 
działała pod kontrolą Linuksa, tak samo jak maszyna 
na drugim końcu tego łącza.

Podsumowując, na konferencji padło wiele dobrego na temat Linuksa.

