---
title: Narzędzie mconv2
date: 2022-02-09
---

* Autor: Nic Tjirkalli
* Tłumaczył: Marcin Niedziela
* Original text: https://linuxgazette.net/issue10/mconv2.html

---

Użytkownicy mysz na złącze **PS/2** mają problem z jednoczesnym współdzieleniem
myszy pomiędzy różne aplikacji, jak *gpm* (zaznaczanie) i *XFree86*. Tak było do
czasu wynalezienia małego narzędzia zwanego **mconv2**. Mconv2 pozwala kilku
programom używać myszy PS/2 w tym samym czasie.

Narzędzie mconv2 wspiera użycie PS/2 w aplikacjach, które nie rozumieją
protokołu PS/2, ale potrafią komunikować się przez protokół szeregowy microsoft
(taki jak *svgalib*). Ten dokument opisuje tylko współdzielenie myszy PS/2 - nie
jej użycie jako myszy typu microsoft - po więcej informacji, odsyłam do pliku
**README** dołączonego do narzędziea mconv2.

Instalowanie i używanie mconv2 jest **BARDZO** proste:

1. **Pobieranie mconv2**

Mvonv2 może być pobrane z różnych stron z aplikacjami na Linuksa. Ja
pobrałem swoją kopię z http://sunsite.unc.edu/pub/Linux/system/Misc/mconv2.tar.gz

2  **Rozpakowywanie**

Mconv2 jest rozpowszechniany jako skompresowane (gz) archiwum tar. Z tego
powodu musi być rozpakowany do odpowiedniego katalogu zanim może
być zainstalowany. Ja skorzystałem z */usr/src* (ale możesz wybrać inny
katalog) jak niżej:

```sh
cp mconv2.tar.gz /usr/src
```

Przejdź do katalogu roboczego i rozpakuj archiwum mconv2, jak niżej:

```sh
cd /usr/src
tar -zxvf mconv2.tar.gz
```

Spowoduje to utworzenie podkatalogu o nazwie *mconv2* zawierającego:

* kod źródłowy mconv2 - *mconv2.c*.
* binarnie skompilowany plik wykonywalny mconv2 - *mconv2*.
* pliki dokumentacji - *mconv2.lsm* i *README*.


3.  **Kompilować, czy nie kompilować mconv2**

Wraz z dokumentacją źródłową i pomocniczą, mconv2 dostarczany jest także w wersji
binarnej. Może ona być używana jako taka, lub można ją skompilować samodzielnie
w następujący sposób:

```sh
gcc -O2 -o mconv2 mconv2.c
```

4.  **Instalacja binarnej wersji mconv2**

Skopiuj dostarczoną lub samodzielnie zbudowaną wersję mconv2 do katalogu z pozostałymi
programami - ja korzystam z  */usr/local/bin*.

```sh
cp mconv2 /usr/local/bin
```

5. **Budowanie urządzenia fifo dla myszy PS/2**

Utwórz urządzenie fifo (pseudo sterownik myszy) dla myszy PS/2:

```sh
mkfifo -m 666 /dev/ps2mouse
```

6. **Korzystanie z mconv2**

mconv2 przed pierwszym użyciem musi posiadać konfigurację wymuszającą użycie
pseudo urządzenia */dev/ps2mouse*. Prawdopodobnie najbezpieczniej jest uruchamiać mconv2
z poziomu pliku */etc/rc.d.rc.local* za pomocą poniższego polecenia:

```sh
echo -n "Runnig mconv2 .... "
/usr/local/bin/mconv2 /dev/psaux -ps2 /dev/ps2mouse &
echo "Done"
```

**UWAGA: powyższe zakłada, że dotychczasowym urządzeniem myszy jest /dev/psaux**

7. **Konfiguracja innych programów**

Jak wspomniano w punkcie powyżej, inne programy wymagające myszy PS/2
muszą być skonfigurowane do używania urządzenia pseudo myszy mconv2 - */dev/ps2mouse*.

Przykłady:

* Aby korzystać z **gpm**, wskaż pseudo urządzenie jak niżej:

```sh
gpm -t ps2 -m /dev/ps2mouse &
```

**UWAGA: opcja -m wskazuje z jakiego urządzenia korzystać**

* Aby korzystać z **XFree86**, zmodyfikuj sekcję **Pointer** w pliku **XF86Config** wskazując
urządzenie **/dev/ps2mouse**, jak niżej:

```sh
Section "Pointer"
    Protocol    "PS/2"
    Device      "/dev/ps2mouse"
```

Ciesz się współnieloną myszą na PS/2. Podziękowania dla Frode'a Fjelda
<frodef@stud.cs.uit.no> za stworzenie mconv2.
