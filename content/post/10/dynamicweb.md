---
tytuł: Konfigurowanie dynamicznego serwera sieciowego IP przez połączenie PPP
data: 2023-05-04
---

- Autor: Henry H. Lu
- Tłumaczył: Marcin Niedziela
- Oryginalny tekst: https://linuxgazette.net/issue10/dynamicweb.html

---

Czy myślałeś o skonfigurowaniu w swoim domu dostępnego dla wszystkich serwera 
WWW jako dodatek do Twojej strony u swojego ISP? Są ku temu powody: Możesz 
pochwalić się światu swoim domowym systemem Linux; nie musisz korzystać z 
jakiejś niechlujnej metody aby poznać swój aktualny adres IP do zdalnych
połączeń; oraz dla frajdy

Po pierwsze, musisz mieć działające połączenie ppp i httpd oraz jakąś STALE
DOSTĘPNĄ stronę internetową do skorzystania z poniższej metody dynamicznego
rozwiązywania IP.

## Opis plików

**web_up:** skrypt powłoki, który uruchamiam, aby zaktualizować stałą stronę 
internetową wyświetlić na niej adres IP po nawiązaniu połączenia.

**web_down:** skrypt powłoki, który uruchamiam przed zamknięciem łącza, do
informowania innych o wyłączeniu.

**update_uppage:** skrypty perla do tworzenia strony up.html ze zaktualizowanym 
adresem IP w locie, wywoływany przez web\_up.

**up.html_source:** poprawiona część pliku up.html

**down.html:** strona internetowa używana, gdy łącze nie działa.

**/etc/add, /etc/last_add:** pliki, w których umieściłem adres IP.

**ip-down, ip-up:** pliki wykonywane, gdy połączenie ppp jest rozłączone lub
połączony. Są one używane do aktualizacji plików /etc/add tutaj.

## Przyjrzyjmy się skryptowi web\_up:

```sh
#!/bin/sh
#check new IP
new_ip()
{
if [ -f /etc/add ]; then
   if [ -f /etc/last_add ]; then
      if /usr/bin/diff /etc/add /etc/last_add >/dev/null ; then
          exit 1 
      else
          return 0 
      fi
   else
      return 0
   fi
else
   exit 1
fi
}

#check whether maroon is connected
try_connect()
{
if ping -c4 -l3 128.101.118.21  2>&1 | grep "0 packets" > /dev/null
then 
   return 1
else
   return 0
fi
}

if try_connect
then
     touch  /var/run/maroon_connected  
else
     rm -f /var/run/maroon_connected
fi

# ftp to update page
if [ -f /var/run/maroon_connected ] && new_ip
then
   # update_uppage is perl scripts, exit status is opposite of shell
   if ( ! /home/honglu/public_html/update_uppage )
   then 
      cd /home/honglu/public_html
      if echo "put up.html /nlhome/m508/luxxx012/dynamic.html" \
          | /usr/bin/ftp maroon  
      then 
         rm -f /etc/last_add
         cp /etc/add  /etc/last_add
         exit 0
      else
         exit 1
      fi
   fi
else
    exit 1
fi
```

## web_up

Przyjrzyjmy się teraz szczegółowo skryptowi web\_up.

Funkcja **new_ip()** służy do sprawdzenia czy mamy nowe IP i
czy nowy adres IP różni się od poprzedniego. `/etc/ppp/ip-up` i
`/etc/ppp/ip-down` aktualizuje adres IP w plikach `/etc/add` i `/etc/last\_add`
abyśmy mogli porównać pliki "add" z "last_add", aby stwierdzić, czy my
trzeba zaktualizować stronę.

Funkcja **try_connect()** służy do sprawdzania, czy stała strona internetowa 
jest osiągalna.

Następna część jest zabawna, użyłem automatycznej funkcji **ftp** do 
aktualizacji strony. Aby to zadziałało, musisz poprawnie skonfigurować plik 
`~/.netrc`; wpisz `man ftp`, aby uzyskać więcej informacji.

**update_page** to proste skrypty Perla do analizowania i tworzenia pliku 
up.html przy użyciu nowego adresu IP z pliku `/etc/add`.

Ostatnią częścią jest aktualizacja `/etc/add` `/etc/last_add`, aby 
odzwierciedlała poprawność stałego adresu IP.

Możesz umieścić „web\_up” w crontab (lub ip-up lub keapalive.sh), aby 
umożliwić automatyczne uruchamianie za każdym razem, gdy komputer łączy się
z siecią.

## Web_down

Web\_down to podobna strona, główna różnica polega na części ftp:

```sh
# ftp to send down.html page 
if [ -f /var/run/maroon_connected ] 
then
      cd /home/honglu/public_html
      if  echo "put down.html /nlhome/m508/luxxx012/dynamic.html" \
        | /usr/bin/ftp maroon  
      then
          rm -f /etc/last_add
      else
          exit 1
      fi
else
      exit 1
fi
```

Podobnie jak web_up wrzucał na ftp plik up.html, tak web_down wrzuca plik
down.html, aby poinformować o braku aktywnego połączenia.

web\_down powinien być uruchamiany przed wyłączeniem komputera. Utworzyłem 
skrypt "shut" do tego celu:

```sh
#!/bin/sh
if web_down
then
   shutdown -h now
else
   echo "can not web_down"
   exit 1
fi
```

Pełny kod źródłowy dostępny jest na mojej stronie domowej: 
http://www.tc.umn.edu/nlhome/g625/luxxx024/
