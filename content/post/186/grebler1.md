---
title: FreeBSD - Wstęp dla linuksowych dewotek
date: 2021-10-15
---

* Autor: Henry Grebler
* Tłumaczył: Marcin Niedziela
* Original text: https://linuxgazette.net/186/grebler1.html

---

## Zastrzeżenia

Zabierzmy stąd szybko te zastrzeżenia. To są moje wrażenia. Nie mam 
specjalnych umiejętności we FreeBSD.

Zacząłem korzystać z FreeBSD w styczniu 2010 roku, trochę ponad rok temu. 
FreeBSD nie był mi zupełnie obcy, gdyż pracowałem na nim kilka lat temu. 
Jednak korzystałem z jedynie niego ułamek tego ile Linuksa.

## Krótka historia (bardzo krótka)

Istnieją 3 główne wersje BSD i kilka drobnych wariacji. Te główne
to FreeBSD, OpenBSD, NetBSD. Nie wiem czym się różnią. Możliwe, że są to 
analogiczne wersje podobnie jak Red Hat, Debiana i SUSE. Z tych trzech głównych
wersji, FreeBSD ma lwią część użytkowników.

NetBSD i FreeBSD zaczynały na jednej bazie kodu około roku 1993. OpenBSD 
powstał później na bazie NetBSD. W pewnym stopniu, kod prekursora FreeBSD
wykorzystywany jest w Microsoft Windows, Darwinie (Mac OS X), Solarisie.

## Wygląd i odczucia

Jak to jest korzystać z FreeBSD na co dzień? Krótko mówiąc jest bardzo podobny do
Linuksa czy też innego Uniksa. 

Czytelnicy Linux Gazette mogą kojarzyć mój HTR (Henry Tak Robi); 
używam HTR aby ukryć różnice pomiędzy różnymi platformami. Nie musiałem za
bardzo zmieniać swojego HTR aby zaznajomić się z FreeBSD.

Użytkownicy Linuksa zauważyć, że niektóre standardowe polecenia nie posiadają
tych samych opcji. Polecenie `ls` we FreeBSD to nie to samo `ls` co w GNU. Dla
przykładu, posiadają różne interpretacje opcji `-T`. We FreeBSD, `-T` wyświetla
pełną informację o czasie, ale nie tak dokładną jak `--full-time`.

Jednakże, we FreeBSD dostępna jest wersja GNU ls pod nazwą `gls`
(`/usr/local/bin/gls`). 

Idąc dalej, jeżeli chcesz czegoś podobnego do Linuksa, FreeBSD posiada zestaw
kompatybilnych binariów Linuksa, które "pozwalają użytkownikom FreeBSD
uruchomić około 90% wszystkich aplikacji na Linux, bez żadnych modyfikacji" 
(http://www.freebsd.org/doc/en\_US.ISO8859-1/books/handbook/linuxemu.html).

Zatem, obok `gls` możesz mieć także `/usr/compat/linux/bin/ls` (to wersja ls za
cenę 1!).

Jaka jest między nimi różnica?

```sh
$ /usr/compat/linux/bin/ls --version
ls (GNU coreutils) 6.12
Copyright (C) 2008 Free Software Foundation, Inc.

$ gls --version
ls (GNU coreutils) 7.5
Copyright (C) 2009 Free Software Foundation, Inc.
```

To prawdopodobnie i tak więcej niż chciałbyś wiedzieć na temat ls (czy innego
polecenia).

Jako ostatnią deskę ratunku, zawsze możesz uruchamiać programy z Linuksa
pod FreeBSD. Jest bardzo duża szansa, że po prostu będzie działać. I nie jest to
emulacja - działa zazwyczaj z natywną prędkością.

## Zauważone różnice

FreeBSD dostarczany jest bez powłoki bash. Jednak, oddając prawdę, `/bin/sh` na
FreeBSD jest czymś dużo więcej niż Bourne shell. Ale to nie bash.

Standardowe rzeczy jakie dostajemy z FreeBSD znajdują się w katalogu `/bin` i
`/usr/bin`. Dodatkowe oprogramowanie zazwyczaj jest instalowane w
`/usr/local/bin`. Możesz zainstalować Basha, który trafi do `/usr/local/bin`.
Większość moich skryptów napisana jest w Bashu; ich pierwsza linia ma postać:

```sh
#! /bin/sh
```

Jednakże, mam kilka skryptów pod bash; te jako pierwsza linia mają:

```sh
#! /bin/bash
```

Pewnie udałoby się znaleźć jakieś inne rozwiązanie, ale wydawało mi się, że
najprościej będzie utworzyć `/bin/bash` jako dowiązanie symboliczne do
`/usr/local/bin`. Pewnie są ludzie, którzy zrobią wielkie oczy, ale to mój
system i moje zasady. 

Polecenie `su` jest nieco inne. FreeBSD posiada `truss` (podobnie jak Solaris)
zamiast strace.


## Dobre rzeczy

Ogromna ilość pakietów. FreeBSD posiada ogromną ilość pakietów. Patrząc na mój
system, oszacowałem około 22 tysiące pakietów (dostępnych, nie mam ich
wszystkich zainstalowanych!). Liczba ta jest prawdopodobnie nieco przesadzona,
ponieważ część z nich to pewnie różne wersje językowe (arabska, chińska,
francuska, niemiecka, węgierska, japońska, polska, portugalska, rosyjska,
ukraińska, wietnamska). Ale nawet bez pakietów językowych, jest tam ponad 21 800
pakietów.

Nie jestem zainteresowany tymi wersjami językowymi, ponieważ czuję
się komfortowo z angielskim. Ale podejrzewam, że jest masa ludzi na świecie,
którzy będą bardzo wdzięczni za możliwość używania systemu w swoim własnym
języku. Takie rzeczy są problematyczne tylko w sytuacji gdy czyjś preferowany
język jest pominięty.

System jest bardzo dobrze udokumentowany, a sama dokumentacja jest dobrej
jakości.

Podoba mi się pomysł, że istnieje Mistrz Wszechświata FreeBSD.
W konsekwencji, nikt nie może zmienić standardu lub frameworka. Indywidualnie, 
każdy użytkownik może nadal dokonywać wszelkich możliwych modyfikacji - jest
nawet do tego zachęcany. W rezultacie, nigdy nie będziesz mieć konfliktów 
dynamicznych bibliotek. 

System tworzony jest w poczuciu dbania o jakość, spełniając oczekiwania, że
będzie po prostu działał. Jeżeli coś u mnie nie działało, wina leżała zawsze po
stronie mojej ignorancji.

## Złe rzeczy

Oto co kiedyś znalazłem na internecie:

> "... jako długoterminowy użytkownik Linuksa, z przykrością muszę 
> stwierdzić, że system portów/pakietów we FreeBSD jest dla mnie mylący."


Zgadzam się. W końcu mam uchwyt na paczki. Wciąż jestem zdezorientowany jeżeli
chodzi o porty. Chętnie przejrzał bym kilka tutoriali na temat tego jak
zarządzać portami. Istnieje masa podobnie nazwanych narzędzi. Których powinienem
używać? Jakie są najlepsze praktyki?

W najprostszym przypadku, paczki - a dokładniej `pkg_add` - jest bardzo podobny
do menadżera yum. Działa jak `yum install` bez `yum list`: sam zajmuje się całą
instalacją, ale czasami trzeba się nico pomodlić aby cały proces przebiegł bez
problemów. Do niedawna był to problem z moim ISP. Teraz jest to problem innej
natury. Jeżeli instaluje openoffice, mogę w ciemno zapełnić cały dysk. Yum
przynajmniej daje mi jakieś pojęcie tego co zostanie zainstalowanie *zanim*
zaakceptuje cały proces.

Jest jeszcze jedna irytująca rzecz. Powiedzmy, że chcesz uruchomić Firefoxa. 
Oczywiście, że chcesz. Żaden problem. Nie możesz wejść na stronę Firefoksa i po
prostu pobrać najnowszą wersję. Do wyboru są wersje na Windows, Mac OS X i
Linux. Zamiast tego, możesz pobrać go jako pakiet. To całkiem duża paczka do
pobrania, ale pkg_add, podobnie jak yum, zajmie się wszystkimi zależnościami.

Dobrze, jesteś zadowolony. Twój Firefox działa, jakby był dostarczony razem z
systemem. Chyba, że zechcesz otworzyć jakąś stronę, powiedzmy YouTube lub inną
stronę, która wymaga rozszerzeń. Proszę, pozwól że będę bardziej dokładny: chodzi
o pluginy, które uruchamiają jakiegoś rodzaju własnościowy kod. Kto stoi za
Flashem? Adobe. Czy źródła są ogólnie dostępne? Nie, proszę pana. Jasne jest, że
jakieś rozwiązanie jest konieczne. Rozwiązanie jest, ale nieco zagmatwane: 

(Za stroną http://www.freebsd.org/doc/handbook/desktop-browsers.html)

    6.2.3 Firefox i Macromedia® Flash Plugin

    Plugin Macromedia® Flash nie jest dostępny dla FreeBSD. Jednakże, 
    istnieje możliwość uruchomienia wersji wtyczki dla Linuksa. Pozwala
    to także na uruchomienie rozszeżeń dla Adobe® Acrobat®, RealPlayer® 
    i innych.

Teraz, zanim zaczniesz się śmiać, pozwól że przedstawię Ci podobny problem
jakie miałem kiedy chciałem uruchomić 64-bitową wersję Firefoksa na CentOS w
pracy (na maszynie 64-bitowej). Firefox działa, ale nie mogłem zmusić żadnego
pluginu aby działał, ponieważ nie było jego 64-bitowej wersji. Nie rozumiem,
dlaczego nie mogłem uruchomić 32-bitowej wersji. 

Przynajmniej byłem w stanie uruchomić Flasha pod FreeBSD. Video i audio działa
bez problemu. Instalacja była skompilowana, ale już wszystko działa. 


## Obojętność

Nie oczekuje, że FreeBSD będzie jak Microsoft Windows czy nawet Ubuntu. System
wymaga od Ciebie abyś wiedział co robić. Jeżeli nie wiesz, możesz poprosić o
pomoc. Oczywiście nie będziesz niańczony jak to robi Microsoft czy Apple.
Osobiście, wole takie podejście.

Domyślnie, FreeBSD korzysta z ufs, który nie jest taki jak ext2 czy ext3. Używa
pojęcia cząstek zamiast partycji. Zatem, aby zainstalować FreeBSD, korzystasz z
narzędzie (np. fdisk) do partycjonowania dysku. Jakąkolwiek partycję przydzielisz 
na FreeBSD, zostanie ona podzielona na kilka cząstek. W moim przypadku,
przydzieliłem partycję 3 na drugim dysku (`/dev/ad1s3`) na FreeBSD:

```
Device Boot    Start       End    Blocks   Id  System
/dev/ad1s3   *     10608     25846   7680456   a5  FreeBSD
```

(Pominąłem pozostałe partycje).

Oto odpowiednik w df:

```
    Filesystem                         Size     Used    Avail Capacity Mounted
    /dev/ad1s3a                        372M     268M      74M  78% /
    /dev/ad1s3e                        310M     117M     168M  41% /tmp
    /dev/ad1s3f                        4.9G     3.5G     1.0G  78% /usr
    /dev/ad1s3d                        558M      51M     462M  10% /var
```

FreeBSD korzysta z terminu "cząstka". Wziął on partycję (/dev/ad1s3) i
poszatkował ją (/dev/ad1s3a, /dev/ad1s3b, itp).

Dobra wiadomość jest jednak taka, że FreeBSD obsługuje ext2. Kiedy zatem skończy
mi się miejsce na mojej partycji z FreeBSD, może zwyczajnie zamontować partycję
Linuksa. Idąc dalej, FreeBSD obsługuje także NFS. Mogę zatem uruchomić moje
komputery z Linuksem i FreeBSD i zamontować ich dyski i pracować na obu bez
większych problemów.

Nie znam względnych zalet ufs i ext2. Wygląda na to, że FreeBSD obsługuje 
wszystkie wersje ext2/ext3/ext4. Zastanawiam się, dlaczego używam 
ext2, a nie ext3.


## Dlaczego wybrałem FreeBSD?

Dobre pytanie. Trudne pytanie.

Chciałem rozwiązać pewien problem. Może nawet i kilka problemów. 

Mój pierwszy problem jest taki, że korzystam ze specyficznego środowiska.
Dlaczego?


Wyobraź sobie, że masz samochód. To pewnie łatwe dla większości ludzi. Dla celów
tego ćwiczenia przyjmijmy, że nie jesteś mechanikiem samochodowym. Nie jesteś
zainteresowany samochodem jako takim. Jesteś bardziej jako moja żona: wsiadasz
do samochodu i jedziesz do celu. Jedyny wybór jaki Cię interesuje to kolor
Twojego TICO. 

Po jakimś czasie przychodzi moment na zmianę samochodu. Ale najnowsze modele nie
dają Ci tej samej kontroli jaką dawał poprzedni. Kierownica? Nie, nie mamy jej w
tym modelu. Mamy nowy gadżet. Siadasz na nim; jak chcesz skręcić, musisz
się powiercić w odpowiednią stronę. Brzmi dziwnie, ale niedługo się
przyzwyczaisz. Nie jest to może rozwiązanie lepsze niż kierownica, ale to
ostatni krzyk mody. Każdy tego chce. Kierownica każdemu już się znudziła.
Hamulce? Tak, ale mamy nowy sposób na zatrzymanie samochodu. Dostaniesz taką
specjalną wkładkę do ucha i...

Tak właśnie odbieram nowe wydania dystrybucji. Zaktualizowałem moją Fedorę 5 do
wersji 10. Nie było z tym żadnych problemów. Ale 10 nie wyglądała już tak jak
bym się tego spodziewał. Wszystko jest inaczej, nazwy dysków się zmieniły. W
Fedorze 5 (której nadal używam) dyski odwołują się do /dev/hda. Fedora 10
postanowiła odwoływać się do /dev/sda. Może jestem naiwny, ale nie spodziewam
się tego typu zmian we FreeBSD.

Każda nowa wersja emacsa wprowadza niekompatybilne zmiany.

Nie uruchamiam komputera po to aby uczyć się nowych sztuczek. Nie tego typu
nowych. Jeden z moich komputerów musi być jak chleb i masło, niezmienny każdego
dnia. Nie chcę tracić dnia na naukę innych osób na idealny edytor tekstu. To
jest tylko edytor! Chciałbym używać tego samego, którego używałem wczoraj. Nie
lepszego, nie gorszego, tego samego. NIE CHCĘ ŻADNYCH NOWYCH FICZERÓW. Jeżeli
już ktoś chce tak robić, niech będzie dostępna wersja emacs-classic i new-emacs.
Implementujcie nowinki do new-emacs i zostawcie w spokoju emacs-classic.

Za każdym razem jak robią go lepszym, zmieniają go na gorsze. To samo dotyczy
pasjansa czy aspella, a ta lista się nie kończy.

I te wszystkie zmiany mają swoją cenę. Kiedyś używałem Slackware na maszynie 
z 16MB RAM i 100 MHz Pentium 1. Chciałem zainstalować Fedore 10 na wolnej
maszynie, ale napotkałem same problemu. Po jakimś czasie sobie z nimi poradziłem
gdy znalazłem informację w dokumentacji, że Fedora nie uruchomi się już na
256MB; potrzebuje minimum 512MB. Dobrze, że znalazłem jakieś stare kości w innym
popsutym komputerze.

Doszedłem do wniosku, że nie lubię pojęcia "wydanie". Szczególnie od kiedy
Fedora zdecydowała się wypuszczać kolejne wydania każdego roku. Nie byłby to
problem gdyby kolejne wydania były kompatybilne wstecz. Ale nie są. Jest takie
milczące założenie, że najnowsze i ostatnie jest najlepsze. Dla wszystkich.

Trochę popytałem. Zaproponowano mi używanie Gentoo lub Arch Linux. Dano mi do
zrozumienia, że są one bliżej idei aktualizacji organicznych. Jeżeli chcesz
wyższej wersji aplikacji X, po prostu ją instalujesz. Odsunięto się od idei
dyskretnych wydań. Jasne, jeżeli chcesz zmienić wersję, dostaniesz najnowszą
wersję. Jednak nie będziesz karany za używanie starego wydania. Chyba, że
coś źle zrozumiałem.

Próbowałem Arch, ale trudno mi było znaleźć informacje. Idea może jest dobra,
ale czułem się trochę wyizolowany.

Czytelnicy LG mogą być świadomi mojego pliku Historia, z zapiskami co robiłem
maszynach, którymi administruje. Oto notka z dnia kiedy zainstalowałem FreeBSD:

> Swoją drogą, dokumentacja jest wspaniała.

I to, prawdopodobnie, mówi wszystko. Pozwolę sobie powiedzieć, że z FreeBSD
napotkałem te same pytania jakie miałem w przypadku Arch. Jednakże we 
FreeBSD udało mi się znaleźć odpowiedzi.

Mam nadzieję, że uda mi się znaleźć naprawdę stare wydanie emacs i go
skompilować. I zostawię go na zawsze, bo używanie swojego oprogramowania jest
mile widziane we FreeBSD.

Nie jest prawdą, że odpuściłem całkiem Linuksa. Ostatnio miałem problemy z
zasilaniem na tej maszynie. Po wymianie zasilacza, nie mogłem uruchomić
komputera. Próbowałem uruchomić komputer z FreeBSD live CD, ale nie miałem
żadnego narzędzia, którym mógłbym przebadać dysk.

Uruchomiłem Fedore live i szybko miałem wszystko pod kontrolą. 

Udało mi się uruchomić KVM/QEMU na FreeBSD. Moim następnym krokiem będzie
zwrtualizowanie mojej maszyny z Fedorą 5. Uruchomiłem już wirtualną maszynę z XP
(której nigdy nie wróciłem odkąd udowodniłem, że mógłbym), Archa oraz ostatnią
wersję FreeBSD. Odpaliłem także w ten sposób pierwszego Linuksa jakiego używałem
- Slackware z 1995 roku.


## Kilka luźnych obserwacji

FreeBSD radzi sobie doskonale z ext2/ext3/ext4, za to moja Fedora 10 ma
problemy z obsługą ufs. Kiedy wszystko działa poprawnie, nie ma się czego
martwić. Problem pojawia się jeżeli trzeba odzyskać jakieś dane, wtedy warto
przemyśleć swoje kroki. Udało mi się dostać do pewnych cząstek systemu ufs z
Fedory 10 - w sumie to tylko jednego.

Podobne uwagi można mieć, być może, do innych mniej popularnych systemów plików:
Reiser, xfs, zfs, LVM. Kiedyś miałem podobne problemy z LVM.

## Podsumowanie

Nie ma systemów idealnych. Mogę mieć tylko nadzieję, że istnieje taka, która
działa racjonalnie dobrze, szczególnie w warunkach dla jakich została stworzona.
Doszedłem do wniosku, że Fedora doszła do takiego etapu, że bardziej skupia się
na tym jak wygląda, zaniedbując element funkcjonalności.

FreeBSD dostarcza mi komfortowe środowisko audio-video. Do innych celów mogę
używać maszyn wirtualnych.

## Referencje

* http://en.wikipedia.org/wiki/Bsd
* http://en.wikipedia.org/wiki/FreeBSD

