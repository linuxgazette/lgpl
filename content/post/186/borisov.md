---
title: PostgreSQL - import dużych tekstów i zawartości dynamicznej
date: 2021-10-15
---

* Autor: Anton Borisov
* Tłumaczył: Marcin Niedziela
* Original text: https://linuxgazette.net/186/borisov.html

---

## Wstęp

PostgreSQL to zaawansowana baza danych. To oprogramowanie typu open source, 
konkurujące z funkcjami z produktami Oracle, nic więc dziwnego, że coraz 
więcej projektów deleguje tak ważne zadania, jak eksploracja danych i obsługa 
danych, do PostgreSQL. Co ważniejsze, projekt architektoniczny tej bazy 
danych jest niezwykle potężny i bardzo blisko zgodny z zasadami KISS, 
więc programowanie wewnętrzne jak i utrzymanie PGSQL sprawia wielką frajdę.

## Zadanie

Jeden z projektów, którym muszę się zająć, jest projekt oparty na 
PostgreSQL i Liferay Portal, który z kolei jest oparty na Tomcacie. 
W rzeczywistości ma architekturę trójwarstwową, w której Tomcat działa 
jako serwer WWW, jest to w zasadzie front-end obsługujący żądania z całego 
świata. Strony internetowe Liferay mogą mieć postać: (a) zwykłego HTML, 
(b) JSP (Java Server Pages), (c) lub mogą być również zaprogramowane jako 
serwlety (portlety). Dwa ostatnie scenariusze wymagają posiadania IDE 
(zintegrowanego środowiska programistycznego) z wdrożonymi powiązaniami 
JSP, portletów i JDBC. Zasadniczo albo JSP, albo portlet zawiera kod, 
który po prostu pobiera aktualne dane SQL z instancji bazy danych (na 
przykład news_portal) i przygotowuje ładną stronę HTML, która pokazuje 
dzisiejszą prognozę pogody lub kursy walut. Jednak możesz być 
zainteresowany wygenerowaniem tej samej strony bez wykonywania czasochłonnych 
czynności, takich jak pobieranie i instalowanie IDE, programowanie nowego 
serwletu i późniejsze wdrażanie. Jak to możliwe Po prostu wykonaj niezbędne 
zapytania SQL, tj. w przestrzeni systemu operacyjnego, gdzie znajdują się 
serwery Tomcat i PostgreSQL. Możesz go zaprogramować w 10 minut - w bashu, 
pythonie lub jakimkolwiek innym języku skryptowym. W moim przypadku generuję 
stronę HTML, która składa się z tysięcy wierszy tekstu i wrzucam ją z 
powrotem do silnika bazy danych Liferay CMS (news_lportal), więc 
zawartość HTML tej strony ma być wyświetlana przez sam Liferay. 
Skonfigurowałem także cron aby odtwarzał ponownie tę stronę 
informacyjną, aby Liferay zawsze pokazywał aktualne wiadomości, stawki itp.

## Manipulowanie danymi z PSQL

Istnieje natywny klient, dostarczany z serwerem PostgreSQL o nazwie psql. 
Choć psql jest tylko aplikacją konsolową, ma zasadniczo te same możliwości, 
co jego odpowiednik w GUI, oparty na GTK PgAdmin. Jeśli nie masz go w 
swoim systemie, uruchom aptitude (dla Debiana):

```
# aptitude install postgresql-client
Reading package lists... Done
Building dependency tree
Reading state information... Done
Reading extended state information
Initializing package states... Done
Reading task descriptions... Done
The following NEW packages will be installed:
  postgresql-client postgresql-client-8.3{a} postgresql-client-common{a} 
0 packages upgraded, 3 newly installed, 0 to remove and 6 not upgraded.
Need to get 2028kB of archives. After unpacking 5276kB will be used.
Do you want to continue? [Y/n/?]
```

Listing 1. Pakiet Postgresql składa się z psql oraz innych narzędzi 
pomocniczych

Zainstaluje psql, a także narzędzia pg_dump, pg_restore i inne.

Oczywiście możesz zainstalować aplikację GUI do wykonywania złożonych 
zadań, takich jak analiza danych:

```
# aptitude install pgadmin3
```

![PgAdmin](https://www.pgadmin.org/static/COMPILED/assets/img/screenshots/pgadmin4-welcome-dark.png)

Rysunek 1. PgAdmin - aplikacja graficzna do obsługi zapytań SQL

Z pomocą psql możesz łatwo uruchomić dowolne zapytanie SQL, jak na poniżej:

```
psql -q -n -h 127.0.0.1 news_lportal -U root -c "select userid, emailaddress from user_"
```

gdzie użyto:

```
adres do połączenia - 127.0.0.1
nazwa bazy danych - news_lportal
użytkownik, który może wykonywać zapytania SQL - root
oraz samo zapytanie SQL - select userid, emailaddress from user_
```

Można także użyć operacji *update*, jak niżej:

```
psql -q -n -h 127.0.0.1 news_lportal -U root -c "update journalarticle set content = '<H1>Hello, World!</H1>' where id_ = 24326"
```

Gdzie ID z numerem 24326 to mój dokument HTML utworzony wcześniej w CMS 
Liferay i zapisany w bazie PostgreSQL - news\_lportal.

W ten sposób można odświeżyć dowolną informację, która przechowywana 
jest w tabeli **journalarticle**. Jedyne, o czym powinieneś pamiętać, 
to prawidłowy identyfikator artykułu.

Jednak w prawdziwym życiu ta sztuczka aktualizacji nie będzie działać 
tak, jak powinna. Przygotowałem skrypt aktualizacyjny (import_table.sh), 
gdzie zawartość *table_news.html* wgrywana jest do PostgreSQL.

```sh
#!/bin/sh

ct=`cat table_news.html`
psql -t -l -q -n -h 127.0.0.1 news_lportal -U root -c "update journalarticle set content = '$ct' where id_ = 24326;"
```

Listing 2. Bardzo prosty skrypt importu (import_table.sh), pierwsza wersja

Argh! Nie udało się - klient PostgreSQL odmówił uruchomienia polecenia 
aktualizacji.

```
$ ./import_table.sh 
./import_table.sh: line 4: /usr/bin/psql: Argument list too long
```
Na pierwszy rzut oka plik *table_news.html* wydaje się być całkiem niezły. 
Ale bliższe przyjrzenie się pokazuje, że plik jest nieco za duży - ma 
rozmiar 400 KB.

```
$ file table_news.html
table_news.html: UTF-8 Unicode text, with very long lines
$ cat table_news.html | wc
    617    2505  408460
```

Czy istnieje mechanizm ładowania do bazy danych dowolnego pliku tekstowego 
o rozmiarze większym niż 2KB? Tak! Na szczęście PostgreSQL ma funkcje 
importu/eksportu, które ułatwiają komunikację z operacjami we/wy na 
plikach. Zadeklarujmy naszą własną procedurę get_text_document_portal(), 
która załaduje do bazy danych dowolny plik tekstowy.

```sql
- Function: get_text_document_portal(character varying) 
-- DROP FUNCTION get_text_document_portal(character varying); 
CREATE OR REPLACE FUNCTION get_text_document_portal(p_filename character varying) 
  RETURNS text AS 
$BODY$ SELECT CAST(pg_read_file(E'liferay_import/' || $1 ,0, 100000000) AS TEXT); 
$BODY$ 
  LANGUAGE sql VOLATILE SECURITY DEFINER 
  COST 100; 
ALTER FUNCTION get_text_document_portal(character varying) OWNER TO postgres; 
```

Listing 3. Nasza nowa procedura wywoła funkcję pg_read_file() i odczyta 
plik tekstowy z dysku

W celu załadowania pliku tekstowego do bazy danych o nazwie news_lportal 
napisałem poniższy skrypt (import_table_2.sh), który przyjmuje nazwę 
pliku - w tym przykładzie **table.text** - jako parametr procedury 
get_text_document_portal() i umieszcza jego zawartość do odpowiedniego 
polaw tabeli journalarticle.

```sh
#!/bin/sh
psql -q -n  -h 127.0.0.1 news_lportal -U root -c "update journalarticle set content = get_text_document_portal('table.text') where id_  = 24326;"
```

Listing 4. Skrypt importu (import_table_2.sh), który uruchamia naszą 
nową procedurę

Wszystko, co musisz zrobić, to zmienić źródłowy plik HTML o nazwie 
table.text i uruchomić import_table_2.sh. Proszę zwrócić uwagę na 
lokalizację, w której ma zostać umieszczony importowany plik - jest 
to podkatalog **liferay_import** w /var/lib/postgresql/8.3/main/.

```
$ ls -l /var/lib/postgresql/8.3/main/
total 48
-rw-------  1 postgres postgres    4 Nov  9 10:20 PG_VERSION
drwx------ 10 postgres postgres 4096 Nov 10 11:16 base
drwx------  2 postgres postgres 4096 Mar  4 16:44 global
drwx------  2 postgres postgres 4096 Dec  3 18:27 liferay_import
drwx------  2 postgres postgres 4096 Nov  9 10:20 pg_clog
drwx------  4 postgres postgres 4096 Nov  9 10:20 pg_multixact
drwx------  2 postgres postgres 4096 Mar  1 13:29 pg_subtrans
drwx------  2 postgres postgres 4096 Nov  9 10:20 pg_tblspc
drwx------  2 postgres postgres 4096 Nov  9 10:20 pg_twophase
drwx------  3 postgres postgres 4096 Mar  4 12:43 pg_xlog
-rw-------  1 postgres postgres  133 Feb 11 22:09 postmaster.opts
-rw-------  1 postgres postgres   53 Feb 11 22:09 postmaster.pid
lrwxrwxrwx  1 root     root       31 Nov  9 10:20 root.crt -> /etc/postgresql-common/root.crt
lrwxrwxrwx  1 root     root       36 Nov  9 10:20 server.crt -> /etc/ssl/certs/ssl-cert-snakeoil.pem
lrwxrwxrwx  1 root     root       38 Nov  9 10:20 server.key -> /etc/ssl/private/ssl-cert-snakeoil.key
```

Listing 5. Informacje o właścicielach plików z pulą pamięci dyskowej PostgreSQL

Właścicielem jest postgres i może być napisany tylko przez tego użytkownika. 
Lub przez konto roota. Oczywiście możesz dodać wpis do tabeli crontab roota, 
ale dobrą praktyką jest - rozdzielanie zadań między różne konta. 
Przypisywanie zadań bazy danych tylko do postgres, a każde inne zadanie 
do na przykład konta tomcat. Jak więc użytkownik tomcat może pisać do 
katalogu liferay_import z bitami dostępu właściciela postgres? 
Tworząc dowiązanie symboliczne nie działa, ale łącze twarde wystarczy!

```
# ln /var/lib/postgresql/8.3/main/liferay_import/table.text /home/tomcat/db/table.text
```
Listing 6. Hardlink pozwala na ominięcie ograniczeń dowiązań symbolicznych

```sh
#!/bin/sh

/home/tomcat/db/prepare_table_news.sh > /home/tomcat/db/table.text
/home/tomcat/db/import_table_2.sh
```

Listing 7. Skrypt (mk_db.sh), który przygotowuje dowolny dokument HTML 
i ładuje go do bazy danych

Brawo! Teraz mogę umieścić wpis w crontab tomcata i co godzinę otrzymywać 
informacje. Odbywa się to z konta tomcat. 

```sh
$ crontab -l
# m h  dom mon dow   command
0 * * * * /home/tomcat/db/mk_db.sh > /dev/null
```

Listing 8. Jeden wpis w crontab tomcata, który powinien być wykonywany 
co godzinę i aktualizować wiadomości

## Podsumowanie

Istnieją różne sposoby dostarczania aktualnych informacji podczas 
pracy z Liferay Portal i technologią portletów. Pierwszy sposób wymaga 
dedykowanego środowiska programistycznego (NetBeans IDE z portletami), 
podczas gdy drugi sposób wymaga jedynie podstawowej znajomości skryptów 
powłoki i umiejętności prawidłowego konstruowania zapytań SQL. Oczywiście, 
lepszym rozwiązaniem jest dobry programista na pełny etat, który ma do 
czynienia ze standardami portletów IDE, JSR168 / JSR268, który może 
zaprogramować dowolną aplikację internetową, zwłaszcza stronę HTML 
z dynamicznie zmienianymi informacjami. Jednak te same wyniki można 
osiągnąć znacznie szybciej - wystarczy polegać na zwykłych narzędziach 
konsoli Linuksa.

## Źródła

* http://www.postgresql.org/
* "Practical PostgreSQL" - Joshua D. Drake, John C. Worsley, O'Reilly Media

