---
title: Caps Lock
date: 2021-10-15
---

* Autor: Anonymous
* Tłumaczył: Marcin Niedziela
* Original text: https://linuxgazette.net/186/anonymous.html

---

Wracamy do Ubuggy w konsoli tekstowej, upewnij się, że jesteś zrelaksowany 
i odpowiednio tolerancyjny.

Po pierwsze, jeśli nie znasz jej nazwy zastrzeżonej, Ubuggy używa głównie 
pseudonimu Ubuntu. Nie mam problemów z używaniem pseudonimu.

Po drugie, załóżmy, że przeszliśmy z pulpitu Ubuntu do trybu tesktowego
za pomocą Ctrl-Alt-F1 lub Ctrl-Alt-F2 lub czymkolwiek. Nie jesteśmy 
w terminalu graficznym.

Po trzecie, zmiany w układzie klawiszy w trybie tekstowym nie mają 
wpływu na pulpit.

Po czwarte, masz uprawnienia superużytkownika; jeżeli nie, możesz 
skończyć czytać.

Z czym tym razem mamy do czynienia? Klawisz Caps Lock. Po celowym 
lub nieumyślnym naciśnięciu go w konsoli tekstowej zauważysz, 
że inne klawisze nie reagują lub reagują w niewłaściwy sposób. 
Dioda Caps Lock nie świeci się.

\[Informacyjnie: używam Ubuntu i wszystkie moje klawisze w terminalu 
wydają się działać dobrze - chociaż dioda LED Caps Lock nie włącza się. - Ben\]

Google, pomocy: problem ma co najmniej 6 lat. Czy został rozwiązany? 
Masz jedną szansę na poprawną odpowiedź, a jeśli ci się nie uda, 
nikt ci nie pomoże.

Okazuje się, że Ubuntu właśnie odziedziczył problem, chociaż można 
powiedzieć, że nawet go pogorszył. Problemem jest:

* http://old.nabble.com/Bug-514212%3A-console-setup%3A-on-UTF-8-console,-caps-lock-is-turned-into-a-shift-lock-td21848145.html
* http://www.mail-archive.com/debian-boot@lists.debian.org/msg106545.html

Tradycyjna akcja przycisku - nazywana Caps_Lock w jądrze - działa tylko 
w ASCII lub dokładniej, w latin1.

Zamierzonym rozwiązaniem jest zastąpienie go CtrlL_Lock, który jest 
niczym innym jak lepkim klawiszem Ctrl, który po ustawieniu, pomaga 
tworzyć wielkie litery w Unicode.

Droga do piekła jest wybrukowana Unicode. Na razie, żadne łaty nie 
działają poprawnie. A może robią to w innej dystrybucji, ale Ubuntu 
ma następujące problemy:

1. Mapa klawiszy trybu tekstowego Ubuntu ma łącznie 127 kombinacji 
na dowolnym zwykłym klawiszu. Założę się, że nie testowali ich 
jeden po drugim.

2. Debian wciąż pracuje nad spuścizną console-tools: 
http://lct.sourceforge.net/

Był to projekt z alternatywnymi loadkeys/dumpkeys, uruchomiony 
pod koniec lat 90-tych i niedługo potem przestał być rozwijany. 
Jego krótkie życie było jednak wystarczająco długie, aby zwabić 
Debiana do kosztownego błędu, z którym nadal nie potrafili sobie 
poradzić. OK, ale Ubuntu to Ubuntu i oni są odpowiedzialni za to, 
co robią, nawet jeśli wzorowali się na Debianie. Więc jeśli zmieniasz
mapę klawiszy w trybie testowym i potem przestaje ona działać, 
to podziękuj console-tools.

Co zatem należy zrobić zanim powstaną dedykowane dla Caps Lock  
poprawki w jądrze i Ubuntu skonwertuje układ klawiatury poprawnie?

Zadawałem sobie to pytanie i nie mogłem znaleźć odpowiedzi. Ale 
potem przyszło mi do głowy, że nikt zazwyczaj nie pisze długich
tekstów tylko wielkimi literami. W przypadku krótkiego tekstu 
o długości połowy linii ekranu można przytrzymać klawisz Shift. 
W przypadku długiego tekstu, jeśli naprawdę zajdzie taka potrzeba, 
wpisz małe litery w edytorze, zamień je na wielkie dzięki wbudowanym
w edytorze funkcjom. Innymi słowy, kto potrzebuje Caps Lock oprócz 
Nigeryjczyków oferujących miliony?

A skąd ten klawisz się wziął? Dziesiątki lat temu w świecie teleksu
i telegrafu nie było małych liter. Maszyny do pisania można by przełączyć 
na wielkie litery. Kiedy ok. 1970 wprowadzono dedykowane maszyny do 
przetwarzania tekstu, miały klawiatury z małymi literami 
i naśladowały maszyny do pisania, więc otrzymały klawisz Caps Lock. 
Ale dlaczego czterdzieści lat później, do cholery, wciąż jest tam 
dodawany?

Rozwiążmy problem bez czekania na idealną łatkę do jądra: rozpracujmy 
Caps Lock.

Krok 1, uruchom

```sh
dumpkeys -1 > kdump.txt
```

W kroku 2, otwieramy kdump.txt w edytorze i zastępujemy wszystkie
wystąpienia CtrlL\_Lock na VoidSymbol. Zapisujemy plik.

Krok 3, uruchom:

```sh
loadkeys -s kdump.txt
```

Zrobione. Oczywiście możesz przypisać mu także inne działanie. 
Dla przykładu, znałem gościa, który przypisał Tab w to miejsce, 
ponieważ klawisz Tab na jego klawiaturze był uszkodzony. 
Jeśli chcesz to przetestować, odpal opisane wyżej instrukcje, 
a następnie uruchom:

```sh
echo "keycode 58 = Tab" | loadkeys
```

lub cokolwiek innego.

Rozwiązanie to jest krótkotrwałe. Po następnym uruchomieniu 
znów uruchomiona zostanie funkcja CtrlL_Lock. Zapisz więć 
mapę klawiszy w formie jakiej używasz i ładuj ją automatycznie
w swoim profilu bash.

Halo, Dell? Zaproponuj firmie Microsoft i innych usunięcie
klawisza Caps Lock z domyślnego układu klawiatury. 

