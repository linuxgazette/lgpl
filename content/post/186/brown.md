---
title: Uruchamianie Encyklopedii Britannica w systemie Linux
date: 2021-10-15
---

* Autor: Silas Brown
* Tłumaczył: Marcin Niedziela
* Original text: https://linuxgazette.net/186/brown.html

---

Encyklopedia Britannica jest encyklopedią komercyjną i dlatego jest płatna, 
ale stare wersje płyt CD i DVD często można tanio kupić tanio na rynku 
wtórnym. Szczególnie interesujące są wydania z 2004 roku, ponieważ ich 
wydawcy udostępnili wtedy [nieoficjalny skrypt do obsługi
Linuksa](http://support.britannica.com/linux/linux.htm) (instalator Windows 
nie działa jeszcze na WINE). Postępując zgodnie z instrukcjami, można 
uruchomić dostęp do podstawowych artykułów i obrazków z encyklopedii, jednak 
można spodziewać się pewnych problemów z wpisami w słowniku Webstera 
i niektórymi multimediami.

Instrukcje mówią, aby pobrać wersję 1.3.1 środowiska Java Runtime Environment,
a skrypt nie chce działać w żadnej innej wersji. Obecnie mamy co najmniej 
wersję 1.6 i znalezienie starej wersji 1.3.1 może być trudne. Jeśli jednak 
edytujesz ich skrypt i wyłączysz sprawdzenie wersji (w momencie pisania
jest to w linii 112 `linux-launch2.0.pl`) zauważysz , że większość rzeczy 
działa na nowszych wersjach.

Musisz także ustawić zmienną środowiskową JAVA_HOME. Jeśli nie jesteś 
pewien, spróbuj ustawić go na `/usr`. Dodałem `$ENV{JAVA_HOME}="/usr";` 
do samego pliku linux-launch2.0.pl, a także usunąłem pytanie 
"Wprowadź lokalizację oprogramowania Britannica" i zastąpiłem je zakodowaną 
lokalizacją, abym mógł go szybciej uruchomić.

## Wielkość czcionek

Niestety okno dialogowe preferencji nie działa, a ręczna edycja pliku 
z ustawieniami jest ograniczona jeśli chodzi o dostosowanie rozmiaru 
czcionki. Może się wydawać, że utknąłeś z domyślnymi czcionkami o małych 
rozmiarach i konieczne jest kopiowanie tekstu w inne miejsce aby można go 
było przeczytać. Jeśli jednak wszystko inne zawiedzie, możesz użyć VNC 
do powiększenia. Aby to ustawić:

```
sudo apt-get install x11vnc xtightvncviewer vnc4server
vncpasswd   # give yourself a password
cat > .vnc/xstartup <<EOF
#!/bin/bash
x11vnc -display :1 -rfbport 5902 -forever -scale 2:nb &
xsetroot -solid grey
exec $HOME/britannica/linux-launch2.0.pl
EOF
chmod +x .vnc/xstartup
```

następnie, aby go uruchomić:

```
vncserver :1 -geometry 700x560 -depth 16  # this is about the minimum dimensions for EB
sleep 2  # allow x11vnc to start
xtightvncviewer :2 -passwd $HOME/.vnc/passwd -geometry 1275x720 # (adjust this for your desktop)
killall Xvnc4
```

`xtightvncviewer` ma zazwyczaj lepszą logikę aktualizacji wyświetlacza 
niż zwykły `xvncviewer`.

Wadą tego rozwiązania jest to, że nie można kopiować tekstu z artykułów 
i wklejać go do innych aplikacji znajdujących się poza serwerem VNC. 
Wydaje się, że jest to ograniczenie `x11vnc`. Jeśli chcesz skopiować tekst, 
musisz uruchomić Encyklopedię Britannica bez powiększenia.

## Wniosek

Odbyło się wiele debat na temat tego, jak Britannica wypada w porównaniu 
z Wikipedią i nie będę się nad tym zbytnio rozwodził. Należy pamiętać, 
że nie można oczekiwać, aby **jakakolwiek** encyklopedia była absolutnie 
doskonała; w końcu wszystkie są tworami **ludzkimi**, tak jak oprogramowanie. 
Wikipedia wydaje się być bardziej aktualna i obejmuje pewne obszary, 
których nie ma w Britannice, ale Britannica wydaje się być bardziej
"dopracowanym produktem": nie będziesz spędzać czasu na przeglądanie 
niedokończonych wpisów lub znajdowaniu artykułów, które wyglądają bardziej 
jak efekt gorącej debaty niż coś, co należy do cichego zaplecza 
tradycyjnej biblioteki. Jeśli czasami znudzi Ci się krzykliwa atmosfera 
wiki, możesz czasami dodać Britannicę do swojej lektury.

