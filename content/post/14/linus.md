---
title: Linus Torvalds otrzyma doroczną nagrodę UniForum
date: 2021-10-15
---

* Autor: Richard Shippee
* Tłumacz: Marcin Niedziela
* Original text: https://linuxgazette.net/issue14/linus.html

---

Piątek, 31 stycznia 1997

Linus Torvalds, uważany za "ojca systemu operacyjnego LINUX",
został wybrany przez Zarząd UniForum do otrzymania Nagrody UniForum.
Nagroda zostanie wręczona Torvaldsowi we czwartek 13 marca,
jako część porannej sesji inauguracyjnej na UniForum '97,
odbywającego się w Moscone Convention Center w San Francisco.

![](/post/14/misc/linus.gif)

Nagroda UniForum, przyznawana corocznie od 1983 roku, trafia do osób
lub grup, których praca znacząco przyczyniła się do rozwoju
systemów otwartych w czasie, lub miały natychmiastowy i pozytywny wpływ na
branżę z długoterminowymi konsekwencjami. Zarząd UniForum
rozważał wielu nominowanych do tegorocznych nagród ale jednogłośnie wybrali
Linusa Torvaldsa za jego przełomową pracę nad jądrem LINUX i za pionierskie
wysiłki w udostępnianiu swojej pracy za niewielką lub żadną cenę.

Linus Torvalds jest twórcą i głównym architektem systemu operacyjnego Linux.
Wiosną 1991 roku, na Uniwersytecie w Helsinkach, sfrustrowany
ceną systemów operacyjnych Unix, Torvalds zaczął pisać kod oprogramowania, które
miały pomoć mu poradzić sobie z niektórymi zadaniami obliczeniowymi na komputerze
386. "Zauważyłem. że to zaczyna być system operacyjny" - mówi. Od tego czasu,
podróżował po całym świecie promując Linuksa. Chociaż rozwijanie
Linuksa było dla niego prawie pełnoetatową pracą, ostatnio przyjął ofertę pracy
w firmie Transmeta w Santa Clara, w Kalifornii. On i Tovi Monni niedawno
świętowali narodziny swojej córeczki, Patricii Mirandy Torvalds.

Zarząd UniForum wybrał również drugiego tegorocznego laureata nagrody: Jamesa
Gosling z JavaSoft, oraz jego zespół programistów, za ich pracę nad Javą.
Gosling odbierze swoją nagrodę podczas środowej sesji Keynote.

Wręczenie nagrody dla Linusa Torvaldsa odbędzie się na otwartej dla wszystkich
konferencji, ale wymaga od uczestników zarejestrowania się na UniForum '97.
Zarejestrowani mogą również odwiedzić wystawę, na której znajdują się stoiska
wielu dostawców LINUX, w tym Comtrol, LINUX International; SSC, wydawcy Linux
Journal; Red Hat Software i Work Group Solutions. Aby zapoznać się z całą
broszurą dotyczącą konferencji i targów UniForum '97 i Targów oraz
zarejestrować się on-line, prosimy odwiedzić stronę
http://www.uniforum97.com/.
