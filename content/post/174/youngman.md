---
title: Zarządzanie hasłami z KeePassX
date: 2021-04-08
---

* Autor: Neil Youngman
* Tłumaczył: Marcin Niedziela
* Original text: https://linuxgazette.net/174/youngman.html

---

## Wstęp

Wielu z naszych czytelników jest obeznana z problemem zarządzania hasłami. W
jaki sposób ogarnąć setki haseł do systemów i stron bez dublowania nazw
użytkowników i samych haseł? Jak generować hasła dla różnych stron z innymi
ustawieniami? Jak bezpiecznie przenosić hasła pomiędzy systemami?

Na rynku jest bardzo wiele różnych rozwiązań spełniających kilka z tych wymagań:
używałem już kwallet, menadżera haseł wbudowanego w Firefoksa i Operę, jednak
wszystkie miały swoje ograniczenia. Wszystkie moje wymagania spełnia jednak
KeePassX](http://www.keepassx.org/), wolny (GPL), wieloplatformowy sejf na
hasła. Zawiera wbudowany generator haseł. KeePassX jest bardzo prosty w użyciu i
stosuje wysoki poziom szyfrowania pliku haseł.

Artykuł ten jest krótkim wstępem do KeePassX oraz demonstracją jak z niego
korzystać.


## Proste zastosowanie jako sejf na hasła

![Obraz 1](/post/174/misc/Fig_1.png)

Gdy po raz pierwszy uruchomisz KeePassX, zobaczysz okno z kilkoma pustymi
panelami. Na tym etapie nie jest możliwe zapisanie haseł. Wszystkie hasła muszą
być dodane w pliku bazy haseł.

Na początek, wejdź w menu "Plik" > "Nowa baza danych" (Ctrl+N) lub wybierz
pierwszą z lewej ikonę na pasku. Aplikacja poprosi o podanie hasła (lub pliku
klucza):

![Obraz 2](/post/174/misc/Fig_2.png)

Przycisk po prawej stronie pola hasła przełącza widoczność hasła, możesz więc
sprawdzić poprawność wprowadzonego hasła.

Kolejnym krokiem po utworzeniu bazy danych jest utworzenie grupy za pomocą
"Edycja" > "Dodaj nową grupę" (Ctrl+G). Poza nazwą grupy możliwe jest wybranie
dla niej ikony, jeżeli domyślna jest nieodpowiednia. Dla celów tego artykułu
utworzyłem grupę "Example".

Gdy grupa jest już dodana, można do niej dopisać hasła poprzez "Edycja" > "Dodaj
nowy wpis" (Ctrl+Y). W oknie dialogowym należy wprowadzić wtedy nazwę
użytkownika, hasło oraz inne szczegóły.

![Obraz 3](/post/174/misc/Fig_3.png)

Wpis może zawierać tytuł, nazwę użytkownika, adres strony internetowej, datę
wygaśnięcia i komentarz. Jeżeli to za mało, można także dołączać zewnętrzne
pliki. Aby sprawdzić poprawność wprowadzonego hasła, należy użyć przycisku z
ikoną oka obok pola z hasłem. Pole "Potwórz" będzie miało czerwone tło, do czasu
aż wprowadzone ponownie hasło będzie odpowiadało temu podanemu w polu "Hasło".

Gdy hasła są już dodane w KeePass, są łatwo dostępne acz w bezpieczny sposób.
Domyślnie, hasła nie są wyświetlane, ale można to zmienić w menu Widok. Z haseł
można korzystać bez ich wyświetlania poprzez opcję "Skopiuj hasło". Można wtedy
wkleić to hasło gdzie potrzeba.


## Korzystanie z generatora haseł

Są trzy sposoby na dostęp do generatora haseł. Wygenerować hasło poprzez menu
"Dodatkowe" > "Generator haseł" lub użyć skrótu Ctrl+P. Jeżeli generujesz hasło
dla wpisu w bazie danych, generator jest dostępny poprzez przycisk "Gen." obok
pola "Powtórz".

![Obraz 4](/post/174/misc/Fig_4.png)

Pole hasło pozwala na dostosowanie opcji generowania haseł, takich jak długość,
czy używać wielkich i małych liter, numerów, spacji, znaków specjalnych. Jest
także zakładka, która pozwala na generowanie haseł łatwych do wymówienia z
użyciem dowolnych zestawów znaków. Zakładka "Własne" nie jest nie pozwala w
łatwy sposób podać zakresu znaków i szybko, a szybkie przejrzenie kodu
źródłowego potwierdza, że nie jest to jeszcze wspierane.

Po wprowadzeniu swoich kryteriów dla haseł, użyj przycisku "Generuj" aby
wygenerować hasło. Jeżeli nie zapisujesz hasła a tylko chcesz je skopiować,
musisz przestawić jego widoczność ikoną z okiem. Gdy edytujesz wpis, kliknięcie
"OK" w oknie generowania uzupełni pole hasło rekordu.


## Międzyplatformowość

[Strona pobierania KeePassX](http://www.keepassx.org/downloads) publikuje kod
źródłowy oraz paczki dla Windows, MacOS X, Ubuntu, OpenSuSe i Fedory. KeePassX
jest także w oficjalnych repozytoriach Debiana.

Bazy haseł generowane na Linuksie lub MacOS mogą być kopiowane na Windowsy i
vice-versa.


## Inne narzędzie

KeePassX oferuje możliwość wyboru algorytmu szyfrowania w AES(Rijndael)
lub Twofish z poziomu "Plik > Ustawienia bazy danych". Liczba rund szyfrowania
także może być dostosowana.

KeePassX potrafi importować pliki
[PwManager](http://sourceforge.net/projects/passwordmanager/) oraz pliki XML
wyeksportowane z kwalletmanager. Potrafi eksportować dane do pliku tekstowego lub
XML.


## Na zakończenie

W okresie pomiędzy napisaniem tego artykułu a jego publikacją, zostałem
powiadomiony, że jedna ze stron internetowych gdzie mam swoje konto została
zaatakowana i powinienem uznać, że nazwa użytkownika i hasło zostały skopiowane.
Hasło to pochodziło z okresu przed rozpoczęciem korzystania z KeePassX
i jak wiele moich starszych kont nie było aktualizowane. W tym czasie,
na stronach o "niskiej wartości", stosowałem praktykę ograniczonej liczby nazw
użytkowników i bardzo prostych haseł. Wszystko to w celu zminimalizowania ilości
danych do zapamiętania. W związku z tym pewna liczba loginów była
zagrożona gdyby atakujący wypróbowali je na innych stronach, z których korzystałem.

Miałem szczęście, że nazwa użytkownika na tej stronie nie była nazwą
użytkownika, której używałem najczęściej, więc liczba witryn narażonych na
bezpośrednie ryzyko była stosunkowo niewielka. Niemniej jednak była to
motywacja do zaktualizowania moich wszystkich haseł, szczególnie tych słabych.

W trakcie aktualizacji tych haseł zauważyłem, że miara "siły hasła"
dostarczona przez KeePassX jest zasadniczo bezużyteczna, ponieważ jest
obliczana poprzez zwykłe pomnożenie długości hasła przez 8. Ciąg "aaaaaaaa"
ma taką samą siłę jak "hr~9kl_p7". Wolałbym brak wskaźnika siły od
mylącego wskaźnika.

Zauważyłem również, że kiedy kopiujesz hasło z KeePassX, wszystkie hasła są
po krótkim czasie usuwane z klippera (schowek w KDE).
