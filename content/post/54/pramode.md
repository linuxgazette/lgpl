---
title: Specjalne atrybuty metod w Pythonie
date: 2021-10-15
---

* Autor: Pramode C E
* Tłumaczył: Marcin Niedziela
* Original text: https://linuxgazette.net/issue54/pramode.html

---

Programiści C++ używają "przeciążania operatorów", aby zastosować wbudowane 
operatory do klas zdefiniowanych przez użytkownika. Klasa liczb zespolonych 
może mieć zatem operator dodawania, który pozwala na użycie dwóch obiektów 
typu "zespolonego" w wyrażeniu arytmetycznym w taki sam sposób, w jaki 
używamy liczb całkowitych lub zmiennoprzecinkowych. Język programowania 
Python zapewnia wiele tych samych funkcji w prosty i elegancki sposób - przy
użyciu specjalnych atrybutów metod. To jest krótkie wprowadzenie do
niektórych metod specjalnych poprzez fragmenty kodu, które napisaliśmy,
próbując przetrawić odniesienie do języka Python. Kod został przetestowany
w Pythonie w wersji 1.5.1


## Klasy i obiekty w Pythonie

Spójrz na definicję klasy w Pythonie:

```py
class foo:
    def __init__(self, n):
        print 'Constructor called'
        self.n = n

    def hello(self):
        print 'Hello world'

    def change(self, n):
        print 'Changing self.n'
        self.n = n

f = foo(10)  # create an instance of class foo with a data field 'n' whose value is 10.
print f.n    # prints 10. Python does not support member access control the way C++ does.
f.m = 20     # you can even add new fields!
f.hello()    # prints 'Hello world'
foo.change(f, 40)
print f.n    # prints 40
```

Metoda `__init__` jest podobna do "konstruktora" w in C++. Jest to
"specjalna metoda", która wołana jest automatycznie zawsze, gdy klasa
jest tworzona. 

Widzimy, że wszystkie funkcje składowe mają parametr o nazwie "self". Kiedy 
wywołujemy f.hello(), w rzeczywistości wywołujemy metodę należącą do klasy 
foo z obiektem "f" jako pierwszym parametrem. Ten parametr jest zwykle 
nazywany "self". Zwróć uwagę, że można nadać mu dowolną inną nazwę, chociaż 
konwencje zachęcają do nadawania jej takiej nazwy. Możliwe jest nawet 
wywołanie **f.hello()** jako **foo.hello(f)**. Programiści C++ znajdą pewien 
związek między "self" a słowem kluczowym "this" (w C++), za pomocą którego 
można uzyskać dostęp do ukrytego pierwszego parametru wywołań funkcji 
składowych.


## Metoda specjalna `__add__`

Rozważ następującą definicję klasy:

```py
class foo:
    def __init__(self, n):
        self.n = n
    def __add__(self, right):
        t = foo(0)
        t.n = self.n + right.n
        return t
```

Utwórzmy następnie dwa obiekty f1 i f2 o typie "foo" i dodajmy je:

```py
f1 = foo(10)  # f1.n is 10
f2 = foo(20)  # f2.n is 20
f3 = f1 + f2
print f3.n    # prints 30
```

Co się stanie gdy Python wywoła f1+f2? Interpreter wywoła `f1.__add__(f2)`. 
Zatem "self" w funkcji `__add__` odwołuje się do f1, a "right" odwołuje 
się do f2.


## Inne możliwości `__add__`

Sprawdźmy inne możliwości metody specjalnej `__add__`:

```py
class foo:
    def __init__(self, n):
        self.n = n
    def __radd__(self, left):
        t = foo(0) 
        t.n = self.n + left.n
        print 'left.n is', left.n
        return t

f1 = foo(10)
f2 = foo(20)
f3 = f1 + f2  # prints 'left.n is 10'
```

Różnica polega na tym, że w tym przypadku f1+f2 jest konwertowane 
do `f2.__radd__(f1)`.


## Jak przedstawiają się obiekty - metoda specjalna `__str__`

```py
class foo:
    def __init__(self, n1, n2):
        self.n1 = n1
        self.n2 = n2

    def __str__(self):
        return 'foo instance:'+'n1='+`self.n1`+','+'n2='+`self.n2`
```

Klasa foo definiuje specjalną metodę `__str__`. Zobaczymy jej działanie po 
uruchomieniu poniższego kodu:

```py
f1 = foo(10,20)
print f1    # prints 'foo instance: n1=10,n2=20'
```

Zachęcam zajrzeć do Python Language Reference aby zobaczyć jak zachowuje się 
podobna funkcja - `__repr__`.


## Testowanie wartości prawdziwych przy pomocy `__nonzero__`

`__nonzero__`  służy do testowana wartości prawdy. Powinna zwrócić 0 lub 1. 
Gdy metoda ta nie jest zdefiniowana, wywoływana jest `__len__`. Jeżeli klasa 
nie definiuje ani `__len__` ani `__nonzero__`, wszystkie jej instancje są 
traktowane jako prawdziwe. Oto jak definiowana jest metoda `__nonzero__`.

Przetestujmy ją:

```py
class foo:
    def __nonzero__(self):
        return 0

class baz:
    def __len__(self):
        return 0

class abc:
    pass


f1 = foo()
f2 = baz()
f3 = abc()

if (not f1): print 'foo: false'  # prints 'foo: false'
if (not f2): print 'baz: false'  # prints 'baz: false'
if (not f3): print 'abc: false'  # does not print anything
```


## Magia `__getitem__`

A gdybyś chciał, ab Twój obiekt zachowywał się jak lista? Cechą wyróżniającą 
listę (lub krotkę, lub to co ogólnie nazywa się typem "sekwencyjnym") jest 
to, że obsługuje indeksowanie. Oznacza to, że możesz napisać `print[i]`. 
Istnieje specjalna metoda o nazwie `__getitem__`, która została 
zaprojektowana do obsługi indeksowania obiektów zdefiniowanych przez 
użytkownika. Oto przykład:

```py
class foo:
    def __init__(self, limit):
        self.limit = limit

    def __getitem__(self, key):
        if ((key > self.limit-1) or (key < -(self.limit-1))):
            raise IndexError
        else:
            return 2*key

f = foo(10)       # f acts like a 20 element array 
print f[0], f[1]  # prints 0, 2
print f[-3]       # prints -6
print f[10]       # generates IndexError
```

Istnieją także inne metody jak `__setitem__`,
`__delitem__`, `__getslice__`, `__setslice__`, `__delslice__`.


## Dostęp do atrybutów dzięki `__getattr__`

`__getattr__(self, name)` jest wywoływane, gdy wyszukiwanie atrybutu nie 
nie jest w stanie znaleźć atrybutu, którego nazwa jest podana jako drugi 
argument. Metoda powinna zgłosić wyjątek AttributeError, albo zwrócić 
obliczoną wartość atrybutu.

```py
class foo:
    def __getattr__(self, name):
        return 'no such attribute'


f = foo()
f.n = 100
print f.n     # prints 100
print f.m     # prints 'no such attribute'
```

Zauważ, że mamy również wbudowaną funkcję getattr(object, name). 
Zatem getattr(f, 'n') zwraca 100, a getattr(f, 'm') zwraca ciąg "no such
attribute". Za pomocą getattr można łatwo zaimplementować takie rzeczy jak 
delegowanie. Oto przykład:

```py
class Boss:
    def __init__(self, delegate):
        self.d = delegate

    def credits(self):
        print 'I am the great boss, I did all that amazing stuff'

    def __getattr__(self, name):
        return getattr(self.d, name)


class Worker:
    def work(self):
        print 'Sigh, I am the worker, and I get to do all the work'


w = Worker()
b = Boss(w)
b.credits()  # prints 'I am the great boss, I did all that amazing stuff'
b.work()     # prints 'Sigh, I am the worker, and I get to do all the work'
```

## Do poczytania

Python dostarczany jest ze wspaniałą dokumentacją, która zawiera 
przewodniki, opis języka i bibliotek. Jeśli jesteś początkującym, powinieneś 
zacząć od wspaniałego 
[przewodnika](http://www.python.org/doc/current/tut/tut.html) 
autorstwa Guido. Następnie przejrzyj 
[Language Reference](http://www.python.org/doc/current/ref/ref.html) 
i [Library Reference](http://www.python.org/doc/current/lib/lib.html). 
Artykuł ten pisany był posiłkując się w/w dokumentacją.


