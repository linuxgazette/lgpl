---
title: Laptop z preinstalowanym systemem Linux
date: 2021-10-15
---

* Autor: Richard Sevenich
* Tłumaczył: ???
* Original text: https://linuxgazette.net/issue54/sevenich.html

---

Ci, którzy używają Linuksa i okazjonalnie podróżują w ramach swojej pracy, 
są często bardzo motywowani do zakupu laptopa. Zrobiłem to we wrześniu 
1999 roku i pomyślałem, że warto podzielić się swoim doświadczeniem. 
Aktualnie, instalacja Linuksa na laptopie staje się coraz łatwiejsza,
jednak w tamtym czasie nie byłem w stanie znaleźć sprzętu raz 
instalowanie Linuksa na laptopie staje się łatwiejsze, ale w tamtym czasie 
był z tym problem. Po kilku poszukiwaniach znalazłem niedrogiego laptopa z 
preinstalowanym Linuksem Nflux, z 
[theLinuxStore](http://www.thelinuxstore.com/) (obecnie część EBIZ). Oto 
reklamowane specyfikacje:

|                |                                         |
| :------------- | --------------------------------------- |
| CPU            | AMD K6-2 3D 300                         |
| Monitor        | 13.3" XVGA (1024x768) TFT Color LCD     |
| RAM            | 64 Mbyte                                |
| GPU            | Neomagic NM2160 128-bit, 2MB RAM        |
| HDD            | 4.3 Gbyte                               |
| Mysz           | touchpad ps/2-compatible                |
| cdrom          | 24X                                     |
| FDD 3.5"       | 1.44                                    |
| Dźwięk         | 16-bit, mikrofon, głośniki              |
| porty          | monitor, szeregowy, irda, usb, etc.     |

Zamówiłem również karty PCMCIA do obsługi modemu i sieci Ethernet oraz 
dodatkową baterię. Kiedy kurz opadł, zmieściłem się w niecałych 1800
dolarach - całkiem rozsądnie. Laptop został zamówiony 30 września, obiecany 
po 5 dniach roboczych i pojawił się w połowie października, 
z mojego doświadczenia wynika, że jest to całkiem normalny poślizg w 
przypadku sprzętu zamawianego przez e-mail. Dostarczona maszyna działała 
dobrze; Byłem bardzo zadowolony z jego możliwości i funkcjonalności. 
Oddzielnie zakupiłem tanią mysz PS/2, woląc ją od touchpada. Bardzo łatwo
udało mi się skonfigurować drukarkę do użytku domowego. Natychmiast byłem
gotowy do pracy, poza dwoma problemami.

* Dostarczony komputer zawierał 32 MB pamięci RAM, zamiast reklamowanych 64 MB.
* Schemat partycji nie był optymalny

Z tą druga wadą poradziłem sobie sam. 

Zadzwoniłem do theLinuxStore z pytaniem o brakujące 32 MB pamięci RAM i po 
pewnym czasie pojawił się układ pamięci RAM, ale był zły (dodając 8MB zamiast 
32 MB). Znów zadzwoniłem, otrzymałem numer RMA i odesłałem RAM. W końcu 
pojawił się kolejny chip, tym razem 128 MB - niestety był niekompatybilny z 
moim komputerem. Zadzwoniłem więc, otrzymałem kolejny numer RMA i odesłałem 
kość. Do trzech razy sztuka - otrzymałem kompatybilny chip, a laptop 
wreszcie był zgodny ze specyfikacją. Było to w połowie marca 2000 r., Około 
6 miesięcy po pierwszym zamówieniu. Część tego czasu była spowodowana moją 
podróżą, ale większość - spowodowana opóźnieniami dostawcy.

Przez cały ten czas mogłem jednak korzystać z maszyny, korzystając z niej
podczas dwóch podróży służbowych. Posiadanie go przy sobie było niezwykle 
przydatne i produktywne. Były to wyjazdy szkoleniowe, na których uczyłem
pisania niskopoziomowych sterowników dla urządzeń dla Linuksa dla 
początkujących. Wieczorami po dniu treningu mogłem sprawdzić różne rzeczy, 
które pojawiały się w ciągu dnia - naprawdę przydatne.

Obsługa moich reklamacji była raczej szybka (dzięki Tiffany Johnson z 
theLinuxStore). Co więcej, z 32MB pamięci RAM, maszyna była wystarczająca 
do moich potrzeb. Gorzej, gdyby maszyna nie nadawała się do użytku - niestety,
to przyszło później. Wyświetlacz panelu zgasł! Laptop był teraz bezużyteczny! 
Zadzwoniłem więc, otrzymałem numer RMA i odesłałem sprzęt. Śledzenie 
przesyłki wskazało, że został odebrany przez theLinuxStore 17 kwietnia. Kilka 
tygodni później zadzwoniłem do theLinuxStore i byłem w stanie ustalić, 
że theLinuxStore odesłał maszynę z powrotem do dostawcy sprzętu, ale nie 
mógł uzyskać żadnych innych informacji. Obecnie jest 23 maja i moje 
zapytania do theLinuxStore o termin zwrotu pozostają bez odpowiedzi. A
komputera potrzebuję na zbliżającą się podróż - mam nadzieję, że się pojawi.

Funkcjonalność laptopa jest naprawdę cudowna, jeśli jej potrzebujesz, 
a ja zaliczam się do tych ludzi. Ta maszyna działała dobrze, ale nie 
została dostarczona zgodnie z opisem i ostatecznie się zepsuła. W moim 
przypadku sprzedawca nie był najlepszym dostawcą sprzętu - więc w procesie 
rozwiązywania problemu może być dodatkowy krok, dodający czas i niepewność 
zwrotów. Werdyktu naprawdę jeszcze nie ma, ale podsumowanie nie jest 
zachęcające:

* maszyna zamówiona 30 września 1999 r
* pełna specyfikacja osiągnięta w połowie marca 2000
* zwrot do LinuxStore w celu naprawy wyświetlacza, 17 kwietnia 2000
* data zwrotu po naprawie nieznana, obecnie brak odpowiedzi

Szczegóły rozwiązania przedstawię w kolejnym wydaniu *Linux Gazette*.

