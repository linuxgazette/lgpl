---
title: Krótka historia Linux Gazette
date: 2022-02-09
---

* Autor: Rick Moen
* Tłumaczył: Marcin Niedziela
* Original text: https://linuxgazette.net/issue96/moen.html

---

*-- Nad tym artykułem LG pracowało kilka osób.*

**Lipiec 1995 (#1)**: Linux Gazette ("LG") zostaje wydane przez jej twórce, Johna F. Misk, studenta kierunku Chirurgii Ogólnej i członka zespołu badaniowego na 
Akademii Medycznej w Vanderbilt. Jego gazeta powstaje, jeszcze za czasów Slackware 2.0, jako "kilka pomysłów jakie wypróbowałem, polubiłem i nad którymi 
pracowałem" wydawane jako część jego strony internetowej znanej jako HomeBoy Web.

Lokalny dostawca internetu w Tennessee, firma CommerceNet wspaniałomyślnie udostępniła miejsce na strone pod adresem http://www.tenn.com/fiskhtml/gazette_toc.html.

Dużą rolę w historii Gazety odegrała kolumna Weekendowy Mechanik, której autorem był John.

**Sierpień 1995 (#2)**: John prezentuje swoją sygnaturke, "Making Linux just a little more fun".

**Wrzesień 1995 (#3)**: Linux Gazette rozpoczyna tradycje prostej, lekkiej graficznie i w wykonaniu stronę. Kolumny "Rady za dwa grosze" i MailBag 
(później jako "Mailbag") mają swoje debiuty, powstają pierwsze wersje FAQ Linux Gazette.

**Październik 1995 (#4)**: Z pomocą Matta Welsha i Grega Hankinsa, raczkujący magazyn Johna został dodany jako oficjalna publikacja Projektu Dokumentacji 
Linuksa. W tym samym czasie powstaje zapis o prawach autorskich, które mówią, że każdy autor ma prawa własności do swoich artykułów, jednak całe wydanie 
istnieje na zasadzie licencji BSD. Pierwszym oficjalnym mirrorem jest strona tworzona przez Donniego Barnesa, Marca Ewinga i Erika Troana z Red Hat Linux, 
Inc. (przemianowana później na Red Hat, Inc.).

**Listopad 1995 (#5)**: Powstają dodatkowe mirrory z inicjatywy Phila Hughesa z SSC, Inc. i Alana Coxa w WB. Inne są w budowie, a po raz pierwszy stają się 
dostępne przez ftp. Michael J. Hammel ma swój debiut w kolumnie "Graphics Muse".

**Styczeń 1996 (#6)**: Rozpoczyna się tradycja zmieszczania numerów Linux Gazette na CD-ROMach wydawanych przez Pacific HighTech, Inc. (zwana później TurboLinux, 
Inc.) jako comiesięczne "Linux Archive Monthly CD-ROM".

**Marzec 1996 (#7)**: John zaprasza do pomocy większą ilość redaktorów i zostaje miło zaskoczony szerokim odezwem.

**Sierpień 1996 (#8)**: SSC, Inc., wydawca Linux Journal, deklaruje się nieodpłatnie prowadzić hosting strony, przeniesienie z Tennessee CommerceNet oraz 
pozwala na przemianowanie go na niekomercyjny magazyn.

**Wrzesień 1996 (#9)**: Linux Gazette przechodzi na otwartą licencję, ordynarnie BSDowska, całkiem podobną do późniejszej OPL. Marjorie L. Richardson staje 
się wydawcą (często pomaga jej Amy Kukuk, a często także inne osoby).

**Październik 1996 (#10)**: Po raz pierwszy pojawia się kolumna John M. Fiska, Weekendowy Mechanik (później wydawana przez Thomasa Adama).

**Styczeń 1997 (#13)**: Jim Dennis wysyła do redaktora list, w którym oferuje swoją pomoc w odpowiadaniu na pytania o bardziej technicznej naturze, wysłane 
do wydawców LG. Marjorie przesyła pytania od czytelników, a potem publikuje odpowiedzi Jima jako kolumnę "Answer Guy", robiąc mu przy tym niemałą niespodzinkę. 
Kolumna ta staje się jedną z najpopularniejszych części Gazety.

**Luty 1997 (#14)**: Pierwsza przetłumaczona edycja (włoska) zostaję wydana przez Francesco De Carlo i inne osoby z LUGBari.

**Marzec 1997 (#15)**: Linux Gazette otrzymuje komercyjnego sponsora, firmę InfoMagic (teraz możesz zamówić do domu CD z gazetą.).

**Październik 1997 (#22)**: Viktorie Navratilova dołącza do redakcji.

**Grudzień 1997 (#23)**: Riley P. Richardson przejmuje pałeczkę redaktora naczelnego, której asystują Marjorie Richardson i Amy Kukuk. Marjorie przejmuję 
dowodzenie w marcu 1998.

**Maj 1998 (#28)**: Dodano wiele nowych mirrorów. Heather Stern przebudowuję poszerzającą się kolumnę "Answer Guy" tak aby zawierała grafikę i była bardzije 
funkcjonalna — oraz staję się jej opiekunką. Jim Dennis, poza swoimi odpowiedziami, zaoferował dział Greetings będacy notką od redakcji.

**Czerwiec 1999 (#42)**: Webmaster Mike Orr przejmuje kontrole nad magazynem jako wydawca, a pomagają mu Marjorie Richardson, Heather Stern i Jim Dennis.

**Październik 1999 (#46)**: Pojawia się ostatnia część "Graphics Muse", Michael J. Hammel przekazuje swoje doświadczenie na swojej [własnej stronie](https://web.archive.org/web/20040117015221/http://www.graphics-muse.org/).

**Luty 2000 (#50)**: Po raz pierwszy zostaje dodana kreskówka Shanea Collingea - HelpDex. Mike opisuję ją jako pierwszą kreskówke odcinkową w LG

**Marzec 2000 (#51)**: SSC rozpoczyna formowanie układu odnośników na stronie do każdego artykułu Gazety. Niedawno (Październik 2003) przykrym trafem, 
wszystkie fora i ich zawartości zostają wyczyszczone podczas reorganizacji strony i giną na zawsze. (Ten przełom ilustruje jeden z problemów z dynamicznym 
połączeniem, który narusza integralność każdego wydania Gazety, jednak przyjacielskie serwery lustrzane pozwalają na szybką odbudowę.)

**Lato 2000 (#54-56)**: Answer Guy zaprasza kilkoro przyjaciów z załogi Linux Gazette oraz kilka innych osób, których odpowiedzi były pomocne, by przyłączyły 
się do pomocy w odpowiadaniu na ogromne ilości pytań. Heather Stern stworzy krok po kroku łatwy w edycji silnik oraz tworzy kilka skryptów, by pomóc 
w utrzymaniu ciągłości odpowiedzi. W lipcu przy pomocy wszystkich ludzi zbiera materiał do kupy, a w sierpniu "The Answer Gang" zaczyna przybierać jako 
taki kształt. Thomas Adam także bardzo pomaga tworząc ogromny, acz wielce pomocny skrypt w Perlu, który zajmuje się obsługą skrzynek pocztowych TAG.

**Wrzesień 2000 (#57)**: Linux Gazette formalnie przechodzi na zasady Otwartej Licencji dla Publikacji (OPL).

**Październik 2000 (#59)**: W odpowiedzi na coraz liczniejsze prośby w sprawie określenia zajęcia poszczególnych osób, określono hierarchię, gdzie Mike Orr 
został redaktorem naczelnym, Heather Stern wydawcą technicznym, Michael "Alex" Williams, Don Marti i Ben Okopnik jako wydawcy współtworzący, a Jim Dennis 
jako główny wydawca współtworzący. Po kilku dobrych wydaniach odchodzi Michael Williams, a do drużyny dochodzi Dan Wilder. Linux Gazette ma już serwery 
lustrzane w ponad 40 krajach na świecie.

**Maj 2001 (#66)**: Thomas Adam odnawia kolumnę Weekendowy Mechanik, którą ostatnio zajmował się jeszcze twórca Gazety John M. Fisk w 1998.

**Październik 2001 (#71)**: Linux Journal wspiera tworzenie opartej na PNuke strony internetowej; wielkim kopem w zad był pomysł, według którego SSC rozważa 
możliwość, czy LG mogłaby być dobrym kandydatem na przeistoczenie się w stronę bazowaną na jakimś niewielkim forum.

**Zima 2001**: Wydawca Phil Hughes z SSC przeprowadza się z Seattle na Costa Rice. Kilka miesięcy później, cała strona SSC (hosting dla ssc.com, 
linuxjournal.com i linuxgazette.com) przenosi sie razem z nim na Costa Rice.

**Wrzesień 2002 (#82)**: Komiks Javiera Malondy (hiszp. / ang.), Ecol, staje się częścią Gazety. Magazyn zyskuje ładne logo.

**Listopad 2002 (#84)**: Mike Orr przerabia techniczną strukturę Linux Gazette by wyeliminować symboliczne odnośniki (problemy w systemach MS-Windows), 
dodaje wersję jednoczęściowe w ASCII i HTML zawierające adresy URL oraz przerabia skrypt tworzący magazyn na [YAML](https://web.archive.org/web/20040117015221/http://www.yaml.org/)
z poprawą błędów generowanych przez [Cheetah](https://web.archive.org/web/20040117015221/http://www.cheetahtemplate.org/). 
Ben Okopnik przedstawia kolumnę Jednolinijkowiec Perla miesiąca — gdyby Sam Spade był Perl-hackerem...

**Jesień 2002 - Zima 2003**: Marketing w obrębie SSC przwiduje dodanie nieco polotu stronom LG. Ich pomysł polega na dodaniu forum www. Stworzono prototyp 
strony, budowanej na podstawie PHPNuke, jednak nie chciał działać; przestał istnieć zanim wprowadzono go do oficjalnego użytku. Członkowie teamu Gazety 
nie zgodzili się na więcej jakichkolwiek zmian. "Oczywiście. To wy *tworzycie* Linux Gazette. Bez was nie będzie niczego."

**Marzec 2003 (#88)**: Mike Orr, wydawca LG, odchodzi z pracy w SSC. Reszta personelu Gazety została poinformowana, a on sam pracował za darmo jeszcze przez 
kilka miesięcy wydając LG. Kilkoro osób zaniepokoiło się, jakoby SSC nie chciało w przyszłości prowadzić hostingu strony; wszelkie niejasności są rozwiewane 
i nie robi się z tego wielkiej sprawy. Wszystkie pytania jakoby zbliżał się koniec, zostały wyjaśnione przez Phila Hughesa, który zapewnił zobowiązanie 
SSC w celu utrzymania istnienie Linux Gazette oraz aby artykuły mogły być bez zmian wydawane jak wcześniej.

**Maj 2003**: Scott (webmaster SSC) napisał do Mikea Orra, że Phil chciałby aby to on przejął opieke nad wydawaniem artykułów, by poznać ducha magazynu gdy 
planowana jest budowa lepszego enginu CMS. Jednakże nie dostał szansy na opublikowanie żadnego numeru; został zwolniony z SSC.

**Czerwiec 2003 (#91)**: Mike podaje krótkie ogłoszenie w wydaniu czerwcowym.

[WorldWatch](https://web.archive.org/web/20040117015221/http://www.cheetahtemplate.org/), chciałby codziennie wydawać najciekawsze artykuły na temat Linuksa, będą zastępczą domeną dla linuxgazette.com. Prawodopodobnie był to resultat 
jeszcze kolejnego wysiłku w kierunku przygotowania magazyn do przejścia na CMS, jednak nie był zbytnio podobny do oryginalnej Gazety, tak więc, poza 
wzmianką w News Bytes, zostało to całkowicie przez nas zignorowane.

**Lipiec 07, 2003**: Mike na liscie mailingowej dla redaktorów ogłasza, że Jeff, webmaster SSC, staje się naszym nowym redaktorem. Został dobrze przywitany. 
Jeff rezygnuje z tytułu redaktora, i ogłasza plany włączenia samoredagującego się ficzera napędzanego przez CMS. W większości redakcja ma pewne wątpliwości, 
a personel uważa, że lepiej pozostawić te plany, gdyż lepszym rozwiązaniem jest pozostawienie publikacji tak jak jest. Jakość magazyny widocznie się obniża; 
niektórzy autorzy zaczynają wycofywać swoje materiały.

**Sierpień 2003 (#93)**: Jeff Tinsler oświadczył w dziale "Na zakończenie", że już więcej nie będzie pisał dla LG. Jednakże sprawy redakcyjne są nadal 
dyskutowane na liscie mailingowej dla redaktorów.

**Wrzesień 2003 (#94)**: Numer wydany w zwykłym stylu bez kilku artykułów ponieważ nie mogliśmy przekroczyć granic czasowych wydania.

**Październik 2003 (#95)**: Heather Stern przesyła swoją porcję materiału (TAG, Tips i Mailbag) do publicznego wydania. Kilka dni później błyskotliwy 
obserwator zauważa temat "Gazette Matters", który opisuje, że SSC chciałby aby redakcja skoncentrowała się na tajemniczym zniknięciu artykułów z Mailbag. 
SSC nie dostaje żadnej odezwy w tej sprawie. Większość serwerów lustrzanych przeprowadza odbudowę uszkodzonej edycji (okazuje się, że listy i inne 
artykuły zostały po cichu usunięte z głównych wydań.

Wciąż niewiadome jest, który program CMS planowany jest do obsługi wydań, dlategoteż redakcja prowadzi narady, według których możliwym rozwiązaniem 
może być:

1. CMS tworzony przez SSC może mieć status jakiegoś krótkiego forum, jednak (nigdy nie będąc periodyczną, redagowaną ani magazynem) nigdy nie będzie 
to prawdziwa Linux Gazette.
2. znajdą jakiś hosting (taki jak SSC), który nie zacznie nagle obracać wszystkiego w niekomunikatywny magazyn oraz dodawać nieautoryzowane dopiski 
do publikowanych tekstów.

Przygotowanie do przenisienia strony na nowy hosting, kurtuazje inżyniera oprogramowania T.R. Fullhart, po których Mike Orr wraca na miejsce redaktora.

**Koniec października 2003**: Obiecana strona oparta na silniku CMS zastępuje poprzednią wersję Gazety pod adresem http://www.linuxgazette.com/ .

Redaktorzy zauważają, że starsze artykuły przerobione na CMS nie zawierają dopisków autorów czy praw autorskich, tak więc zawiadamiają SSC o tym problemie 
i pomagają go rozwiązać. Te listopadowe wydanie zawiera wszystkie dopiski i nie narusza żadnych praw autorskich, a wręcz zostały one wyjątkowo wyjaśnione.

Redaktorzy zbierają całość poprzednich artykułów z paczek Debiana, ponieważ nie były one nigdy przerabiane ani nic do nich nie dodawano, oraz przebudowują 
sieć serwerów lustrzanych.

**28 Pażdziernik 2003**: Rick Moen pisze formalny list do SSC w sprawie LG, aby ten poprawił braki w poprzednich wydaniach oraz aby uaktywnili domenę 
linuxgazette.com dla redakcji Linux Gazette, jeśli nie są zainteresowani dalszym mirrorowaniem nowej strony.

**Listopad 2003 (#96)**: Właśnie je czytasz. Jest to pierwsze wydanie Linux Gazette opublikowane pod nowym adresem: http://linuxgazette.net/.
