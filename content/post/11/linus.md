---
title: Linus przeniesie się do USA w 1997 r.
date: 2022-02-09
---

* Autor: Phil Hughes
* Tłumacz: Marcin Niedziela
* Original text: https://linuxgazette.net/issue11/linus.html

---

Linus Torvalds jest obecnie w trakcie kończenia studiów magisterskich
na Uniwersytecie Helsińskim. Po ukończeniu studiów planuje
przenieść się do Santa Clara w Kalifornii, gdzie w marcu 1997 roku
rozpocznie pracę w firmie Transmeta. Ponieważ Transmeta nie zajmuje się
Linuksem, jest to stanowisko nie związane z Linuksem; jednakże jego
kontrakt zawiera zapis o czasie pracy przewidzianym na kontynuowanie
pracy nad Linuksem.

Po dokładnym rozważeniu, czego chciałby w przyszłości dla siebie
i swojej rodziny, Linus zdecydował się na pracę w świecie biznesu.
Jego przyjaciel, który pracuje w Transmecie i który od dawna jest współtwórcą
Linuksa, porozmawiał z jego kierownictwem i znalazł dla Linusa stanowisko,
które zarówno wykorzystuje jego talenty jak i zainteresowania.

Linus uważa, że połączenie Transmety idzie w parze z jego przyszłymi celami.
Transmeta jest stosunkowo małą firmą. Podczas gdy głównym zadaniem Linusa
nie będzie praca nad Linuksem, to jednak Transmeta dostrzega dużo dobra jakie
przynosi Linux i dlatego pozwoli Linusowi na swobodną kontynuację projektu
Linux.

Tutaj w *Linux Journal* odbieramy to jako szansę na jakieś bliższe spotkanie,
choćby przy piwie. Chociaż nie mamy do siebie aż tak blisko, to jednak
będąc na tym samym wybrzeżu w odległości około dwóch godzin samolotem sprawia,
sprawa nie wydaje się być beznadziejna.

Niektórzy wyrazili obawę, że Linus postawił bardziej na sprawy prywatne, co
może negatywnie wpłynąć na rozwój Linuksa; osobiście myślę jednak, że tak nie jest.
Ci z nas, którzy mięli okazję usiąść i porozmawiać z Linusem wiedzą, że Linusa
już ma swoje życie. Chociaż spędza dużo czasu pracując nad Linuksem, ma też
inne zainteresowania. Celem uczęszczania na Uniwersytet w Helsinkach było zdobycie
dyplomu, a nie tworzenie systemu operacyjnego, w który wszyscy jesteśmy teraz
zaangażowani.

Myślę, że jego decyzja o przejściu do firmy neutralnej wobec Linuksa, mimo że
miał oferty pracy od firm pracujących z Linuksem, będzie zaletą
dla przemysłu linuksowego jako całości. Również fakt, że jest w USA pozwoli
Linusowi łatwiej uczestniczyć w pokazach i innych wydarzeniach, jednocześnie
nadal zachowując życie osobiste. Poza tym, miło będzie móc powiedzieć, że osoba,
która stworzyła nasz system operacyjny ma też normalną pracę.

