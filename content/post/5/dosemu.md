---
title: Wprowadzenie do DOSEMU
date: 2022-02-09
---

* Autor: John M. Fisk
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.nov.html

---

## Uruchamianie programów DOS'a pod Linuksem

Spróbuje w sensowny sposób przedstawić następujące tematy:

* Co to jest i dlaczego go potrzebujesz?
* Które programy działają, a które nie?
* Jak go dostać?
* Jak wszystko bezboleśnie zainstalować i uruchomić,
* ...i wszystko inne

## Co i Dlaczego

Przede wszystkim. *Możesz* uruchomić pod nim **DOOM'a**.

DOSEMU to program użytkowy, który pozwala jądru Linuksa na uruchominie
MS-DOS w czymś w rodzaju "dosowego pudełka". W przybliżeniu to
"wirtualna maszyna" - wydaje mu się, że jest sam w komputerze i że ma
całkowitą kontrolę nad sprzętem i oprogramowaniem (software & hardware)

Z punktu widzenia oprogramowania nie ma to większego znaczenia. Dosowy
program *Terminate* może powiedzieć, że został uruchomiony w wirtualnej
maszynie, a 'msd' i 'mft' mogą produkować ciekawe rezultaty. Ogólnie
programy nie wyglądaja na NIEpracujące ponieważ uruchomione są pod
emulatorem DOS'a.

## Co *Będzie* Działało

Aczkolwiek sam nie używam wielu programów wymagających DOSEMU, miałem
pozytywne rezultaty z tymi, które próbowałem uruchamiać. U Ciebie może
być z tym różnie.

Najlepiej zajrzyj do [EMUsuccess.txt](ftp://tsx-11.mit.edu/pub/linux/ALPHA/dosemu/EMUsuccess.txt).
Lista ta zawiera wiele dosowych programów, które doskonale działają pod
DOSEMU, lub działają całkiem dobrze. Programy te to gry, narzędzia i
"prawdziwe" aplikacje. Chce dodać od siebie, że Windows 3.1 równiez
działa pod DOSEMU. Wymaga co prawda "spaczowania" jądra Linuksa i kilku
innych programów - wszystko opisane w pliku DOSEMU-Win3.1-HOWTO.

Generalnie małe programiki działają. EDIT, 4dos, arj, pkzip i podobne
pracują bez komplikacji. Miałem drobne problemy z CSHOW, a TERMINATE
zgłaszał połączenie 14.4, ale faktycznie **NIE** przekraczał 1200.
Telmate 4.20 był troche lepszy. Zauważcie, że wszystko jest trochę
opóźnione. Mój 486-33 wyglądałby jak 386-40, lub 486-25, zależnie od
programu.

Przy okazji, mój prawdziwy "DOS" to Windows 95 i DOSEMU nie ma z nim
problemu. (też się zdziwiłem - czy nie jest to przypadkiem całkiem nowy
system operacyjny?).

## Gdzie?

Aktualnie (1 listopad 1995) najnowsza stabilna wersja DOSEMU to 0.60.3.
Wymaga conajmniej Linuksa 1.1.43. Najnowszą wersję DOSEMU możesz pobrać
przez ftp:
* [sunsite.unc.edu](ftp://sunsite.unc.edu/pub/Linux/ALPHA/dosemu);
* [tsx-11.mit.edu](ftp://tsx-11.mit.edu/pub/linux/ALPHA/dosemu);
* [dspsun.eas.asu.edu](ftp://dspsun.eas.asu.edu/pub/dosemu).

Twardziele mogą zajrzeć do podkatalogu /Development.

## Setup

**RTFM\!** Nie koniecznie\!\!\! Quickstart.txt (zawarty w pliku
DOSEMU.tgz) jest doskonały i prawdopodobnie Twoim największym
zmartwieniem w setupie. To co bym *sugerował* to zajrzenie do sekcji 2
pliku DOSEMU-HOWTO. Podpowie Ci jak przyspieszyć kompilacje i kilka
innych pomocnych wskazówek.

Jeżeli posiadasz CDROM - zerknij do pliku
./dosemu0.60.4/drivers/cdrom.c. Są tam flagi które musisz ustawić, żeby
umożliwić włączenie programu 'eject' (zawarty) w CDROMie. Jeżeli flaga
nie jest ustawiona na 1, a używasz programu 'eject', DOSEMU nie
rozpozna, że CD zostało zmienione, i napęd może się zablokować.

Poza tym zwróć uwagę na fakt, że jeżeli ustawiasz dostęp DOSEMU do
innych napędów (poniżej), cały czas pracujesz w UNIKSIE. Jeżeli masz
napędy DOS'a zamontowane jako 744 (-rwxr--r--), takie same prawa widzi
również DOSEMU\!. Jeżeli zalogujesz się jako użytkownik
nieuprzywilejowany (nie-root) i spróbujesz uruchomić program który pisze
do plików log, tworzy pliki tymczasowe, lub robi cokolwiek innego - DOS
poinformuje, że "nie można otworzyć pliku" lub inny błąd. Moje własne
montowanie tak wygląda - jest niekonwencjonalne, ale (IMHO),
bezpieczniejsze.


## Inne rzeczy

Ok, reszta jest juz tylko moją paplaniną. Problemy, sukcesy itd moje
własne DOSEMU OONE (Off Of Net Experience).

W pliku 'dosemu.conf' możesz zdefiniowac całą masę parametrów... ilośc
pamięci, EMS, XMS, mysz, porty COM itd. Wszystko jest dobrze
udokumentowane w pliku, oraz przedstawione w przykładach. Jeżeli nie
rozumiesz ustawień, zrób dokładnie to co zrobiłby prawdziwy hacker
Linuksa... próbuj wszystkiego. Możesz też zostawić to w spokoju i mieć
nadzieję, że będzie działać..

Ponieważ DOSEMU przechwytuje kontrolę nad portami COM, odkryjesz
zapewne, że po jego wystartowaniu, a nastepnie próbie przełączenia się
na inny VT i próbie wybrania numeru poprzez minicom lub DIP, modem
będzie zablokowany przez DOSEMU. Możesz albo nie pozwolić DOSEMU do
przeglądania portów, albo uruchamiać programy modemowe *przed* startem
DOSEMU. Jeżeli wykonasz to drugie, zauważysz na ekranie podczas
opuszczania DOSEMU, że nie mógł **on** inicjalizować modemu.

Możliwe jest ustawienie DOSEMU do startowania z pliku obrazu dysku
twardego. Jest to obraz dysku, który DOSEMU używa jako dysku startowego.
Jest to ten dysk na którym znajdują się pliki command.com, msdos.sys,
autoexec.bat i config.sys. Przyjmij, że jest to Twój dysk C:.

To będzie Twoje 'C:'. Możesz użyć 'lredir' do ustawienia innych dysków,
tak by DOSEMU je rozpoznawał. Zrobisz to poprzez nastepujące odwołanie:

```bat
C:\> lredir d: LINUXS\dosd
```

Zamontuje to katalog /dosd jako DOSEMU'owy D:.

Nie znalazłem dobrego rozwiązania na otrzymanie "prawdziwego" C:
(zamontowanego jako /dosc), do stania się DOSEMU'owego C:. Umożliwiłoby
to moim plikom batch'owym i innym programóm z minionej ery DOS'a,
pracować poprawnie. Rzeczywiście mam nastepujące ustawienia w moim
komputerze:

```
    PRAWDZIWY       LINUX           DOSEMU
    -------         -----           ------
    /hdimage        /hdimage        C:
    C:              /dosc           H:
    D:              /dosd           D:
    E:              /dose           E:
    Linux Root      /               F:
    CDROM           /dev/cdrom      G:
```

Jak widzicie jest trochę zakręcony. Ale pozwala mi na dostęp do
**wszystkich** danych na komputerze.

Powinniście zauważyć,że domyślnie DOSEMU nie rozpoznaje więcej dysków
niż C: i D:. Jeżeli spróbujecie wyjść poza to, otrzymacie błąd, że nie
ma innych dysków. Osobiście używam programu **LASTDRV.COM** (dostępny w
[Quarterdeck's](https://web.archive.org/web/20030813200051/http://www.quarterdeck.com/)
QEMM), który pozwala na zobaczenie wszystkich dostępnych pod DOSEMU
dysków.

Poza tym dostępny jest inny program pod nazwą **xdos**. Jest dokładnie
tym o czym myślicie. Choć można usuchomić DOSEMU w xterm lub czymś
podobnym, program xdos uruchamia się we własnym małym pudełeczku z
bardziej "poprawnie" wyglądającymi czcionkami:). Choć zauważyłem, że
programy graficzne mogą mieć problemy pod xdos.

## Podsumowanie

DOSEMU to świetny program. Pozwoli na uruchomienie szeregu ważnych
programów DOSowych pod Linuksem. Niemniej jednak jest *tylko emulatorem
DOS'a*. Oznacza to, że programy **NIE** będą chodziły tak szybko jak pod
czystym DOSem. Choć jest najlepszym aktualnie dostępnym.

Chcę podziękować John'owi M. Frisk'owi za jego wkład w społeczność
Linuksa, oraz załodze DOSEMU za świetny program (i mam nadzieję, że nic
nie pokręciłem w tym artykule). Szczęśliwego Linuksowania\!\!

[Alan Bailward](mailto:ALAN_BAILWARD@MINDLINK.BC.CA) (teraz 
[URLowany](http://mindlink.net/alan_bailward/)!)

\[*Ten artykuł i następny o Garrot'ie zostały świetnie przedstawione
przez Alan'a Bailward'a. Jestem ogromnie wdzięczny za pracę wykonaną nad
tym artykułem. Jeżeli chcesz podziękować za jego pracę lub jeżeli masz
jakieś komentarze lub sugestie, proszę napisz do Alan'a. Dzięki\!
--John*\]



