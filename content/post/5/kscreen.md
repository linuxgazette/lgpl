---
title: Używanie *kscreen* do czyszczenia Śmieci na Ekranie!
date: 2022-02-09
---

* Autor: John M. Fisk
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.nov.html

---

Ok... to jedna z tych rzeczy, która jest moją obsesją porządku...

Za każdym razem grzebiąc w czymś... wiesz... mając na uwadze swój
interes, próbujesz zapisać pracę, nagle...

*Szast prast\!\!\!*

Widzisz na ekranie robaczki?

Arrggghhh...\!\!\! MEGA-WSTRZĄS\! :-(

Ostatnio próbowałem skompilować ostatnią wersję **xfig** z obsługą jpeg
a ten uparcie odrzucał linki z jpeg po 20 minutach kompilacji. Tak więc
próbując odszukać odpowiedzialną za to funkcję, wykonałem "grep
jpeg\_destroy\_compress \* " lub coś w tym rodzaju, szukając błędnej
funkcji i nagle mój cały ekran pokrył się robaczkami. Tak to jest jak
"grepujesz" pliki objektowe...

Nie żartuję.

OK, cóż... chcę wrócić do poprzedniego stanu... po prostu wylogowałem
się, żeby wyczyścić te znaczki i.... Hmmm...

Cały czas widać "robaczki"...

Wstrząs.

ALE\! Chwila\! Ludzie, to jest Linux, a nie jakaś tania imitacja systemu
operacyjnego... (powstrzymamy się od nazwania go, co nie?) i to jest
proste do rozwiązania.

Rozwiązanie jest proste:

``` 
echo -ne "\017"
```

Świetne, nie? :-)

Ten sprytny mały sposób działa nawet gdy na terminalu wszystko jest
popieprzone. Nie ma potrzeby killowania niczego, a napewno nie ma
potrzeby restartu do wyczyszczenia tego bałaganu. Nie będąc w stanie
zobaczyć co dokładnie wpisujesz, trzeba rozwiązać to w inny sposób...

Jak wiele z małych pomocnych poleceń linii komend, i ten sposób jest
idealny do stworzenia małej funkcj, która może być umieszczona w Twoim
\~/.bash\_profile. Edytuj \~/.bash\_profile wszystkich użytkowników
jakich posiadasz i wstaw:

``` 
kscreen() { echo -ne "\017" }
```

Teraz wszystko ustawiłeś\! Za każdym razem, gdy coś się spieprzy, po
prostu wpisz tajemnicze polecenie "kscreen" (od Kleen SCREEN - "cyść
EKRAN") i już masz wyczyszczony ekran\!

W przypadku, gdy myślisz że zapomnisz o tym... dodaj poprostu linię do
\~/.bash\_login lub \~/.bash\_profile:

``` 
echo -ne "\017"
```

i odtąd za każdym razem gdy się logujesz Twój ekran będzie czyszczony, a
w przypadku gdy zapomniałeś tajemniczego polecenia, po prostu wyloguj
się i zaloguj ponownie.

Dalej... weź to w obroty... nawet jeżeli Twoja mama pomyśli, że jesteś
sprytny :-)


