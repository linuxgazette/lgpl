---
title: Tworzenie Lepszych *hack* skryptów
date: 2022-02-09
---

* Autor: John M. Fisk
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.nov.html

---

```
Date: Mon, 16 Oct 1995 08:09:06 CDT  
From: System Administrator \<sysadmin@rjm2.res.lehigh.edu\>  
To: \<fisk@web.tenn.com\>  
Subject: Linux Gazette  
  

Masz tu coś małego. Myślę że będziesz zainteresowany...

    CUT-HERE-----------------------------------------------------------------------
    #!/bin/bash
    #
    #   Ross J. Micheals (1995)
    #   rjm2@lehigh.edu
    #   Dystrybucja dozwolona pod warunkiem informowania mnie o ewentualnych zmianach
    #
    #   Version 1.0
    #   Bazując na pomyśle Linux Gazette Author John M. Fisk (dzięki!)
    #
    #   Program zaprojektowany z myślą o trochę łatwiejszej kontroli wielu
    #   plików konfiguracyjnych (Ok, _bardziej_troche.)
    #
    #
    #   Składnia
    #   ----------------------------------------------------------------
    #   hack *filename*
    #   (Wildcards jeszcze nie testowane!)
    #
    #   gdzie użytkownik to ROOT
    #   i *filename* to Twój standardowy plik tekstowy
    #
    #   Ten program wykonuje
    #   ----------------------------------------------------------------
    #   1. Tworzy /root/links i /root/config_dist jeżeli jeszcze ich nie masz
    #
    #   2. Tworzy kopie filename* i zapamiętuje ją w $DIR_DIST jako
    #      *filename.dist*.  Jeżeli mirror pliku dystrybucyjnego juz istnieje
    #      nic więcej nie robi.
    #
    #   3. Tworzy link symboliczny w $DIR_LINKS do *filename*.  Metodą tą możesz
    #      w prosty sposób odwoływać się do poszególnych plików, a poza tym będziesz miał
    #      przejrzystą listę wszystkich plików konfiguracyjnych, które zmieniłeś!
    #      (Doskonałe do archiwizacji i aktualizacji)
    #
    #   4. Uruchamia edycję *filename*. Myślę, że to jest to.
    #
    ###########################################################################
    #   SEKCJA KASTOMIZACJI                       #
    ###########################################################################
    #
    #   Lokalizacja gdzie chcesz skopiować pliki dystrybucyjne 
    #
    DIR_DIST='/root/config_dist'
    #
    #   Lokalizacja gdzie mają się znajdować linki symboliczne dla wszystkich
    #   plików konfiguracyjnych które zmieniłeś
    #
    DIR_LINKS='/root/links'
    #
    #   Domyślny edytor
    #
    EDITOR='emacs'
    #
    ###########################################################################
    #
    #   Aktualny katalog
    #
    CURR_DIR=`pwd`
    
    #    Upewnij się, że uzytkownik jest root'em
    
    if [ "$LOGNAME" != "root" ]
    then
        echo "hack: user not logged in as root"
        exit 1
    fi
    
    #   Upewnij się, że uzytkownik wywołał program z argumentami
    
    if [ "$1" = "" ]
    then
        echo "hack: filename is missing"
        exit 2
    fi
    
    #   Upewnij się, że filename jest plikiem
    if [ ! -f "$1" ]
    then
        echo "hack: filename is bad or is a directory"
        exit 3
    fi
    BASENAME=`basename $1`
    
    #   Stwórz $DIR_DIST i $DIR_LINKS jeżeli jeszcze nie istnieją
    if [ ! -d $DIR_DIST ]
    then
        echo "hack:" $DIR_DIST "not found"
        echo "hack: creating" $DIR_DIST
        mkdir $DIR_DIST
        chmod 711 $DIR_DIST
    fi
    if [ ! -d $DIR_LINKS ]
    then
        echo "hack:" $DIR_LINKS "not found"
        echo "hack: creating" $DIR_LINKS
        mkdir $DIR_LINKS
        chmod 711 $DIR_LINKS
    fi
    
    #   Stwórz archiwum plików  (na koniec!)
    
    if [ ! -f "$DIR_DIST/$BASENAME.dist" ]
    then
        cp $1 $DIR_DIST/$BASENAME.dist
        echo "hack: creating file" $DIR_DIST/$BASENAME".dist"
    elif [ -f $DIR_DIST/$BASENAME.dist ]
    then
        echo "hack:" $DIR_DIST/$BASENAME".dist already exists"
    else
        echo "hack: fatal error in destination file"
        exit 4
    fi
    
    #
    #   Twórz symlinki
    #
    
    if [ -f $CURR_DIR/$1 ]
    then
        if [ $CURR_DIR = "/" ]
        then
            ln -s "$1" "$DIR_LINKS/$BASENAME"
            echo "hack: linking" $1
        elif [ ! -L $DIR_LINKS/$1 ]
        then
            ln -s "$CURR_DIR/$1" "$DIR_LINKS/$1"
            echo "hack: (c) linking" $CURR_DIR"/"$1
        fi
    elif [ -f $1 ]
    then
        if [ ! -L $DIR_LINKS/$BASENAME ]
        then
            ln -s "$1" "$DIR_LINKS/$BASENAME"
            echo "hack: linking" $1
        fi
    else
        echo "hack: something is fundametally wrong here"
    fi
    
    $EDITOR $1
    exit
    CUT-HERE-----------------------------------------------------------------------

Możesz spokojnie zamieścić to w następnym wydaniu Linux Gazette. Poza
tym pracuję nad małym skrypcikiem do sprawdzania czy używasz wild-card
czy nie, jeżeli chcesz, poczekaj na gotową pracę. Wygląda nieźle.
Dziękuję bardzo za Twój entuzjazm.

[mailto:%20sysadmin@rjm2.res.lehigh.edu](mailto:%20sysadmin@rjm2.res.lehigh.edu)
```

\[*Kilka edycji temu sugerowałem, że rozsądnie jest zapisywać ważne
systemowe pliki i niektóre prawdopodobnie... uh... od tyłu... :-), więc
można by zapisywać swoje oryginalne pliki konfiguracyjne uzywając
skryptu takiego jak "hack". Był to tylko zarys programu, oraz sugestią,
a nie wielkim, rozdmuchanym programem.*

*Coż, kilku ludzików odpisało z ważnymi poprawkami i sugestiami. Chcę im
podziękować:*

* Ross J. Michaels
* Judith Elaine
* Nick LeRoy

*Dlaczego archiwizacja tego całego materiału jest ważna...? Cóż, musicie
mieć crash systemu lub przez przypadek skasować/nadpisać pliki, żeby
stracić GODZINY wytężonej pracy. Wtedy archiwizacja ma znaczenie w
uratowaniu ciężkiej pracy ;-). Poza tym, zarchiwizowanie pozwoli Ci na
grzebanie w systemie... jeżeli coś totalnie zepsujecie, będziecie mogli
wrócić do poprzedniego stanu. Naprawdę warte zapamiętania jest:*

NIGDY NIE HACKUJ TEGO CZEGO NIE MOŻESZ ODHACKOWAĆ\!

Jeżeli coć zmieniasz, zawsze pamiętaj o zrobieniu kompletnego backupu i
przywróceniu do oryginalnego stanu. Ćwicząc, oszczędź Linuksa\! :-)

Podczas wprowadzania w życie planu zapisywania plików konfiguracyjnych w
jakimkolwiek czasie, idealnie jest też zrobić to, gdy planujesz
aktualizację lub reinstalację systemu. Myślę, że będziecie pod wrażeniem
pomysłów zaprezentowanych przez tych ludzi.

*Miejcie na uwadze, że ten i inne prezentowane tu materiały są chronione
prawami autorskimi. Jeżeli modyfikujesz jakikolwiek z tych programów i
pragniesz publicznie go udostepnić, proszę skontaktuj się z autorem.
Trzymcie się mocno\! --John* \]

