---
title: Supermount - *BARDZO łatwe* Montowanie Flopa
date: 2022-02-09
---

* Autor: Daniel Sully
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.nov.html

---


```
Date: Sat, 04 Nov 1995 23:54:50 CST  
From: Daniel Sully \<daniel@joshua.cco.net\>  
To: \<fiskjm@ctrvax.Vanderbilt.Edu\>  
Subject: Neato linux util, and aliases  

Hej tam... Przeczytałem wszystkie wydania Linux Gazette, i myślę że
wykonujesz kawał dobrej roboty\! LG daje wskazówki i małe rady, o
których mowa w książkach O'Reilly, lub których nie ma w LDP, i myślę że
jest bardzo przyciągające...

2)Może będę w stanie mirrorować LG... ale chcę się najpierw dowiedzieć
wszystkiego o obciążeniu Twojego serwera... Jakiego typu masz
sprzęt/przepustowość? Nie chce za bardzo zamulić swojego systemu =) Mam
pełne T1 na 80Mhz DX2 i Linuksie do oferowania stron www...

c) Dla TLG: sprawdź patch'a supermount, dostępny na:

[sunsite.unc.edu:/pub/Linux/kernel/patches/diskdrives/supermount-0.4a.tar.gz](ftp://sunsite.unc.edu/pub/Linux/kernel/patches/diskdrives/supermount-0.4a.tar.gz)

jest to patch na kernela pozwalający dynamicznie montować napęd
dyskietek i cdrom'u..

Musisz jedynie dodać wpis do /etc/fstab, przekompilować kernel, a
nastepnie włożyć dyskietkę do napędu, cd /floppy i zostanie zamontowana
za Ciebie. Możesz nawet wyjąc dysk, a ten automatycznie odmontuje..

I... w nagrodę... dla wszystkich "konwerterów"(?) DOS'a, (większości z
nas)

w \~/.bash\_profile

alias a:='cd /floppya'
alias b:='cd /floppyb'
alias c:='cd /dosc'
alias d:='cd /dosd'
alias e:='cd /cdrom'


świetne, no nie?? Też tak myślę... =)

Dzięki za Twój czas.

Daniel Sully  
daniel@cco.net  
.sig got eaten  
```

## I w odpowiedzi na moje pytanie o montowaniu flopów read/write:

```
Date: Mon, 06 Nov 1995 11:13:53 CST  
From: Daniel Sully \<daniel@joshua.cco.net\>  
To: John M. Fisk \<fiskjm@ctrvax.Vanderbilt.Edu\>  
Subject: RE: Neato linux util, and aliases  
  

Nie, montuje mojego flopa jako:

    / /floppya   supermount  rw,dev=/dev/fd0

w moim pliku fstab... właśnie tak.

Daniel  
daniel@cco.net  

On Mon, 6 Nov 1995, John M. Fisk wrote:

    > Dan, skopiowałem supermount i przekompilowałem mój kernel
    > -- bez większych problemów.  Nie bawiłem się jeszcze tym, ale spoglądając do dokumentacji
    > zauważyłem, że musisz montować urządzenie w trybie tylko do odczytu. Czy było to dla Ciebie w ogóle problemem?
    > CD oczywiście powinno być
    > tylko do odczytu, ale dyskietki często są używane zarówno do odczytu jak i zapisu, przynajmniej
    > ja ich tak używam... przerzucam sporo danych na dyskietki, żeby mieć trochę wolnej przestrzeni
    > na dysku twardym i odruchowo używam cp do i z dyskietki... jak to ominąłeś?
```

## I dla waszytkich tych co są zainteresowani...

Taa\! Po wielu modyfikacjach i kompilacjach kernela, supermount działa
na **FiskHaus** i wygląda na to, że pracuje doskonale\! Jeżeli jesteście
starej gwardii ludźmi konwertera DOS'a którzy wiecznie zapominali
odmontowywać floppy to naprawdę ucieszycie się z tej zakręconej rzeczy.

FYI...Używam kernela w wersji 1.2.13 i supermount-0.4a.tar.gz, którego
można znaleźć na sunsite.unc.edu i mirrorach wylistowanych powyżej.
Jeżeli jesteście zainteresowani, przedstawiam co znalazłem
działającego...

Ale zanim zaczniemy, **MOCNO** nalegam na przeczytanie README, który
dołączony jest do tego programu. Jeżeli jesteście własnymi
administratorami Linuksa, musicie wiedzieć co jest grane. Powiem Wam co
zrobiłem, żeby to pracowało... od Was zależy upewnienie się czy będzie
to działało w waszym systemie.

*Caveat emptor...*

Podstawowe kroki są następujące:

1.  odarchiwizuj pliki i przeczytaj README
2.  zpaczuj kernela
3.  edytuj /etc/fstab
4.  zrestartuj system

Gdy odarchiwizujesz plik, znajdziesz następujące pliki:


* supermount-0.4a-1.2.13.ud.gz
* supermount-0.4a-1.3.30.ud.gz
* supermount-README


Pierwszy plik jest ujednolicony dla kernela 1.2.13, drugi dla kernela
1.3.30, dla wszystkich ludzi z rodzaju "lubiących ryzyko":-).

Rozpakuj (gunzip) ten, który potrzebujesz i przekopiuj (cp) do swojego
katalogu /usr/src. I w duchu "nigdy niepowodzenie nie jest oczywiste..."
idź dalej bez słowa, że potrzebujesz PEŁNYCH źródeł jądra, a nie
prostych nagłówków.

Po wykonaniu tego, zaaplikuj patch'a po prostu wprowadzając:

```sh
patch -p0 < supermount-0.4a-1.2.13.ud
 -OR-
patch -p0 < supermount-0.4a-1.3.30.ud
```

zależnie od wersji kernela z którym ma działać. Powinieneś zobaczyć
przelatujące rutynowe komunikaty. (Upewnij się, że jesteś w katalogu
/usr/src w trakcie wykonywania patchowania, bo w przeciwnym wypadku te
rutynowe komunikaty zamienią się w komunikaty w rodzaju "Hmmm...nie
wygląda, żeby zadziałało")

Szybcy czytacze -- będziecie potrzebować tutaj czytać szybciej, nie
przejmujcie się zawartością.

Zakładając że wszystko przebiegło gładko jesteście gotowi do kompilacji
nowego kernela. Wszystko czego potrzebujecie to zrobienie prostego:

make config && make dep && make clean &&
make zImage (lub cokolwiek innego byś chciał)


Pozwoli ci to przejść przez CAŁY proces w jednym rzucie. Mogłbyś dodać
"make mrproper" na początku tego bałaganu. Więc, podwójny ampersand (&)
umożliwi kontynuację procesu kompilacji tak długo, jak długo nie będzie
błedów. Jeżeli już kompilowałeś jądro przedtem, powinieneś wiedzieć jak
długo będzie trwać.

Och, wcześniej zapomniałem...i tylko dlatego, że nie stracicie
niepotrzebnie czasu:-) Kiedy robicie "make config" przejdźcie
bezpośrednio do linii:

Dynamic mounting of removable media?

to jest opcja która wkompiluje supermount w jądro, dlatego odpowiedz
"y".

Kiedy się skończy kompilacja i będziesz miał gotowy kernel, NIE ZAPOMNIJ
ZRESTARTOWAC LILO\!\!\! Wierzcie mi, nieraz sięgałem po dyskietke
startową, kiedy zapomniałem o tym... ;-) oszczędź sobie tej mordęgi\!

Teraz przyszedł czas na edycje fstab. Jeszcze raz słowo zachęty: NIE
SPIEPRZ TEGO CO DZIAŁA\!

Nie czujesz tego twierdzenia?

Ok, ok... poważnie... zrób backup pliku fstab. Przeczytałeś już 3
doskonałe artykuły publikowane wyżej jak można to zrobić. Wybierz swoją
broń\! A przynajmniej zrób kopię fstab i nazwij go jakoś tak
"fstab.aktualna\_działająca\_wersja\_której\_obiecuję\_nie\_usuwać".

Bądź rozsądny.

Teraz jeszcze raz: czytaj README\! Powie Ci on czego potrzebujesz dodać
do *swojego* systemu. Tutaj widać mój plik fstab:

```
#------------------------------------------------------------------------------
#
#   file:   /etc/fstab
#
# Tu ustawiam różne systemy plików które mogą być montowane podczas startu
#   albo ręcznie. Po pierwsze ustawimy startowy system plików:
#
# device    mount       type    options     dump    fsck
#-------    -----       ----    -------     ----    ----
/dev/hdb8   swap        swap    defaults    0   1
/dev/hdb6   /       ext2    defaults    0   1
/dev/hdb7   /usr/local  ext2    defaults    0   1
none        /proc       proc    defaults    0   0
/dev/hda2   /c      msdos   defaults    0   2
/dev/hdb5   /f      msdos   defaults    0   2
#/dev/hda5  /os2        hpfs    defaults    0   2
#------------------------------------------------------------------------------
#
# Następnie ustawiamy system plików, które mogą być montowane ręcznie. Użyj opcji
#   "noauto" żeby system plików nie był montowany podczas startu systemu.
#
# /dev/fd0H1440 /fd0    ext2    user,noauto 0   0
# /dev/fd0H1440 /a  msdos   user,noauto 0   0
/dev/sbpcd  /cdrom  iso9660 ro,noauto   0   0
#
# Poniżej, spróbujmy użyć programu "supermount" wkompilowanego w kernel
#
/   /a  supermount  rw,fs=msdos,dev=/dev/fd0    0   0
/   /fd0    supermount  rw,fs=ext2,dev=/dev/fd0     0   0

Prawdopodobnie zauważycie wstawienie przeze mnie *małej* regułki
montującej stacje dyskietek dla obu systemu plików msdos i ext2 jako
czytanie i zapis. NIE RÓBCIE TEGO BEZ PRZECZYTANIA PLIKU README\!

...chyba że jesteś kimś, kto przechadza się wokół śliskiego basenu w
gorący dzień z nożycami w ręku... ;-)

Niemniej jednak edytuj swój plik fstab, zrestartuj system i będziesz
gotowy do gry\! Po pierwsze, możesz chcieć sprawdzić i zobaczyć co
"mount" powie o twoich systemach plików. Wprowadź po prostu:

mount

powninieneś zobaczyć coś podobnego do tego:

/dev/hdb6 on / type ext2 (rw)
/dev/hdb7 on /usr/local type ext2 (rw)
none on /proc type proc (rw)
/dev/hda2 on /c type msdos (rw)
/dev/hdb5 on /f type msdos (rw)
/ on /a type supermount (rw,fs=msdos,dev=/dev/fd0)
/ on /fd0 type supermount (rw,fs=ext2,dev=/dev/fd0)

Ostatnie dwie linie wyglądają znajomo\! Jeżeli wszystko jest ustawione w
porządku jesteś mistrzem\!

Teraz możesz dysponować cyklami "mount - umount" za każdym razem gdy
wkładasz dyskietke. Wsadź jednego z tych złych chłopców do środka i
wydaj rozkaz systemowi:

ls -l /fd0

Kiedy to teraz robię... ludzie to demonstracja *na żywo*

Miłe panie...proszę odsunąć dzieci... proszę się nie pchać i nie
przepychać...

Kiedy wydałem to polecenie, otrzymałem:

total 1299
-rw-r--r--   1 root     root       534714 May 31 14:46 mfm-stat-bin.tar.gz
-rw-r--r--   1 root     root        67359 May 30 13:39 xcolorsel.tar.gz
-rw-r--r--   1 root     root       230145 May 30 12:59 xfm-1.3.2.tar.gz
-rw-r--r--   1 root     root       112640 May 31 14:49 xgoups-1.3.tar.gz
-rw-r--r--   1 root     root        64204 Jan 20  1995 xinfo-1.01.tar.gz
-rw-r--r--   1 root     root       297522 May  5  1995 xkeycaps-2.28.tar.gz
-rw-r--r--   1 root     root        11256 May 30 15:25 xvset-0.90.tar.gz

dla jednej z moich archiwalnych dyskietek.

Zauważcie, że *NIE* musiałem nic przedtem montować , ani nic
odmontowywać po zakończeniu. Bez dymu, bez luster, a moje palce ani się
nie poruszyły :-)

Dostałeś punkt zaczepienia... teraz idź i baw się\!
```


\[**Addendum:** Suppermount jest przeznaczony do pracy zarówno z
dyskietkami jak i CDROMami. Niemniej jednak, po wielu walkach z
programem nie byłem w stanie zmusić go do prawidłowej współpracy z
CDROMem. Pracował poprawnie tylko z pierwszym CD który był włożony, ale
następne nośniki nie były rozpoznawane, a *ls* i *cd* nie pracowały
poprawnie po zmianie CD. NIE ZNACZY to, że supermount nie działa, znaczy
to tylko tyle, że *ja* nie mogłem zmusić go do pracy w moim systemie.

Warte dodania jest to, że jeżeli ręcznie odmontuje i ponownie zamontuje
/cdrom, wtedy wszystko działa, ale nie jest to dobry sposób na używanie
tego.

W związku z tym, zabawę z tym zostawiam Tobie. Plik README opisuje co
potrzebujesz zrobić, żeby dodać obsługę CDROM (podpowiedź:
ro,fs=iso9660,dev=/dev/cdrom). Jeżeli uda ci się zmusić CDROM do pracy,
skrobnij mi o tym... Używam karty Creative Labs SB16 i CDROM Creative 2X
IDE dołączonej do karty SB16. Jeżeli uda ci się uruchomić obsługę CDROM
poinformuj mnie o swoim sprzęcie.

Dzięki\! --John\]


