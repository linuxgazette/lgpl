---
title: Less potrafi więcej!
date: 2022-02-09
---

* Autor: John M. Fisk
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.nov.html

---

Moje ostatnie dwa grosze... dla waszej informacji.

Ci z was, którzy jeszcze nie odkryli tego doskonałego małego programu,
naprawdę powinni wziąć **less** w obroty. Ci którzy już to przerabiali,
mogą tu znaleźć trochę różności, które ostatnio odkryłem.

Pewnego dnia grzebałem w artykułach usenetu próbując znaleźć coś, o czym
wiedziałem że to zapisywałem. Po grepowaniu wielu z nich, w końcu
natrafiłem na kilka prawdopodobieństw, i zdecydowałem się na zajrzenie
do nich. Istotny był czas, tak więc spróbowałem manewru:

```sh
less *.news
```

na tych plikach w którcyh szukałem danych. Załadowało to przeglądanie
wiązki plików do przeszukiwania. Więc, co to za trik? Tutaj jest kilka
rzeczy które umożliwia Ci less:

```
:n  przejdź do następnego pliku
:p  przejdź do poprzedniego pliku
:v  edytuj plik używając VI
h   odpal pomoc on-line
```

Poprzez wciskanie dwukropka a następnie "n" lub "p" możesz przechodzić
do poprzedniego lub nastepnego pliku. Chcesz wyedytować plik? Wciśnij
dwukropek, nstępnie "v" i nagle jesteś w VI, gotowy do edycji. Jesteś
ciekaw co możesz zrobić więcej z less... poprostu wciśnij "h" a
zobaczysz ekran pomocy online.

Przeglądanie wieloplikowe jest szczególnie przydatne jeżeli chcesz
obejrzeć kilka plików na raz. Edycja tez jest pomocna, w trakcie
czytania Ty decydujesz o edycji pliku.

Nie ma się może czym podniecać, ale jednak... całkiem sprytne.


