---
title: Zakończenie
date: 2022-02-09
---

* Autor: John M. Fisk
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.nov.html

---

Cóż, gratulacje...zrobiłeś to\!

Dzięki za odwiedziny. Chciałbym jeszcze raz z głębi serca podziękować
wszystkim ludziom, którzy napisali i zaoferowali swoje szczere poparcie,
oraz wszelkiego rodzaju rady, sugestie, pomysły, wskazówki, triki, a
także poprostu za pogawędki o Linuksie. Nauczyłem się wiele od Was i
cenię to bardziej niż możecie to sobie wyobrazić. Szczególnie wdzięczny
jestem tym osobom, którzy napisali z zagranicy, dla których angielski
nie jest "językiem ojczystym". Przyznajmy, że angielski nie jest łatwym
do nauki językiem dlatego cenię wysiłek wielu z Was do pisania.

Jak mówiłem, raz jeszcze dziękuję ludziom, którzy napisali i których
listy i wiadomości zostały zamieszczone w tym miesiącu w LG. Nie jest to
jednoosobowym widowiskiem\! :-) Jeżeli znajdziesz coś pożytecznego tutaj
napisz o tym ludziom i poinformuj ich o tym\!

Na koniec, co jest tu **najnowszą zabawką**...?

Cóż, zacząłem w szkole małe programowanie C++, naprawdę zostałem
zaintrygowany przez programowanie **Tcl/Tk**. Tym z was, którzy nie
mieli jeszcze z tym do czynienia, powiem że jest to doprawdy świetne\!

Bawiłem się kilkoma bardzo fajnymi programami, które powinny się znaleźć
w pudełku z Linuksem..., a są to:

* **ical** jest fajnym i bardzo użytecznym osobistym
    kalendarzem/menedżerem/organizerem. Używałem Iksowego programu
    xcalendar przez chwilę i jest on naprawdę mały, choć adekwatny do
    prostych potrzeb. Poza tym próbowałem naprawdę fantastycznego
    programu **plan-1.4.x** który jest naprawde pełnym możliwości
    programem. Ktokolwiek z Was go używał, wie co mam na myśli. Jest
    świetnym programem.

    Odkryłem że przyciągają mnie te programy, które mają poprostu wiele
    możliwości -- rzeczy których naprawdę używam. Plan był trochę
    "miażdżący" do prostych zastosowań, których potrzebuję w
    planowaniu dnia, podczas gdy xcalendar był zbyt spartański. **Ical**
    był prawdziwą przyjemnością, więc zacząłem często go używać.
    Pozostawię go wam do oceny.

* **addressbook** jest jeszcze innym bardzo fajnym programem Tcl/Tk,
    który jest poza tym "po prostu porządny". Jak mówi nazwa, jest
    książką adresową z wieloma miłymi dodatkami. *Dopiero co* zacząłem
    z nim zabawę i nawet nie miałem czasu do wprowadzenia jeszcze
    wszystkich numerów przyjaciół i rodziny...

    Mimo wszystko, wygląda na doskonały mały program, który powinien
    dostarczyć wiele radości. Myślę o tworzeniu menu kontekstowego z
    kilkoma z tych "aplikacji biurkowych", które mogą być w prosty
    sposób wywoływane. Są to te z kilku programów takich jak **ical,
    addressbook, xpostit i plan**, które naprawdę ułatwiają codzinne
    planowanie zadań. Poinformuję Was jak to działa.

* **xskim** który jest częscią pakietu **skim**. Ci z Was, którzy mają
    system nie podłączony sieci i odbierają pocztę i czytaja grupy
    dyskusyjne poprzez łącze PPP lub SLIP, powinni wziąć ten mały
    program w obroty.

Używałem kilku czytników grup dyskusyjnych (news readers) tj. **xrn,
tin, PINE (który ma możliwość czytania grup dyskusyjnych) i rn**.
Wszystkie działaja bajecznie dobrze, ale są BARDZO wolne z serwerem
NNTP, którego używam w szkole. W akcie desperacji, rozpocząłem
poszukiwania lepszego, lub conajmniej szybszego, czytnika grup
dyskusyjnych i natknąłem się na **skim**.

Ten mały, doskonały program pozwoli Ci na wszystko z Twoimi grupami
dyskusyjnymi czytanymi offline, co jest BARDZO wygodne. Po podstawowym
ustawieniu, które jest świetnie udokumentowane i proste w wykonaniu,
poprostu stwórz listę grup dyskusyjnych, które chcesz subskrybować, a
jak to wykonasz, pobierze wszystkie nagłówki tematów interesujących Cię
grup. Następnie wybierz artykuły, które według Ciebie są interesujące, a
program zbierze tylko te artykuły, a następnie zapisze je na dysku.

W ten sposób możesz zebrać tylko te artykuły, które Cię interesują i
możesz je czytać offline.

Używam interfejsu graficznego **xskim**, Tcl/Tk app, do tego
wszystkiego. Trzeba przyznać że jest to trochę mniej niż "pełne
możliwości", ale pracuje on zupełnie dobrze i jest naprawdę
niezawodny. Teraz mam możliwość przeglądania kilku grup
comp.os.linux.xxxxx\!

Inną miłą rzeczą w **xskim** jest fakt napisania go w Tcl/Tk. Trochę w
nim grzebałem i miałem sporo doskonałej nauki Tcl/Tk z niego.

Wszystkie te programy są do znalezienia, jeżeli dobrze pamiętam...:-) na
sunsite lub jednym z jego mirrorów. Wybaczcie mi, że nie podam URL dla
nich, ale poprostu nie miałem w tym tygodniu czasu na ich znalezienie.

Poza tym, jeżeli ktokolwiek z Was jest zainteresowany **Tcl/Tk**, jest
tam WIELKA ilość naprawdę dobrego stuffu do zabawy z nim. Nie możecie
zaczynać myśleć o tym, co jest innego, dopóki nie odwiedzicie Yahoo i
nie wprowadzicie "Tcl" do wyszukiwarki. Jest naprawdę wart poświęcenia
chwili jeżeli jesteście w ogóle zainteresowani.

Kilka dobrych stron internetowych poświęconych Tcl/Tk:

* [Żródła Tcl/Tk Resources](http://web.cs.ualberta.ca/~wade/Auto/Tcl.html)  
* [Projekt Tcl/Tk Labolatoriów Sun Microsystem](http://www.sunlabs.com/research/tcl/)
* [TCL WWW Info](http://www.sco.com/Technology/tcl/Tcl.html)

Jeżeli na poważnie jestes zainteresowany nauką Tcl/Tk dobrze by było
gdybyś odwiedził stronę domową **Brent Welch's**. Był on Ph.D studentem
w labolatorium **John Osterhout's** i napisał doskonałą książkę
zatytułowaną Practical Programming in Tcl and Tk. Plik postscriptowy
projektu tej książki dostepny jest przez anonimowy FTP dla tych, którzy
mogą być zainteresowani tym tematem.

Zarówno John Osterhout jak i Brent Welch napisali książki o
programowaniu Tcl i Tk. Kupiłem książkę Johna Osterhouta za urodzinowe
pieniądze i cieszę się czytając ją pomiędzy
studiowaniem/programowaniem/wkuwaniem (co generalnie oznacza wtedy, gdy
jestem w łazience...;-)

Są to definitywnie rzeczy do dodania do bożonarodzeniowej listy życzeń\!

Na koniec, tu jest BARDZO fajna strona o Linuksie, na której wszyscy z
Was powinni się zatrzymać:

[Wędrowna Weblista Aplikacji Linuksowych](http://www.xnet.com/~blatura/linapps.shtml)

Chłopaki utrzymujący tę stronę zrobili to o czym ostatnio
myślałem...i zrobili to o wiele lepiej niż ja mógłbym to zrobić.
Zgromadzili wyczerpującą listę programów, które pracują na platformie
Linuksa i umieścili linki (URL) do stron domowych tych programów.
Wszystkie programy skatalogowane są wg typu, tak więc jeżeli interesuje
Cię Grafika, Tekst, Procesy, Dostrajanie, lub cokolwiek innego, po
prostu wybierz interesującą Cię grupę i przejrzyj ją. Programy maja
krótkie opisy, żebyś wiedział co one robią.

Bardzo fajne.

Byłem tam kilka razy i naprawdę świetnie się bawiłem. Sprawdź ten
link. Poza tym, jeżeli jesteś autorem programu, przemyśl dodanie tam
URL-a do swojego programu.

Mam nadzieję, że jesteście zadowoleni z tego wydania LG\! :-)

Do zobaczenia w przyszłym roku!
-- John

