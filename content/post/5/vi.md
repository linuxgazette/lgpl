---
title: Jeszcze *więcej* trików w VI
date: 2022-02-09
---

* Autor: John M. Fisk
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.nov.html

---

```
Date: Sat, 14 Oct 1995 10:35:02 CDT  
From: Jens Wessling \<wessling@emunix.emich.edu\>  
To: \<fisk@web.tenn.com\>  
Subject: Vi Rządzi\!\!  
  

Cześć,

Chcę się podzielić z Tobą pewnymi interesującymi trikami w vi. Jeżeli
wydadzą Ci się interesujące, dodaj je do gazety.

Kiedy pierwszy raz używałem vi, moja reakcja była dokładnie taka sama
jak tych, którzy pierwszy raz sie na niego natknęli. yechhh\!\! To bez
sensu. Po pewnym czasie używałem go coraz częściej. Dokonałem jednej
krótkiej próby użycia emacs'a, spędziłem 45 minut próbując wyjść i
przyrzekłem sobie powrót do mojego starego, poczciwego edytora vi.

Odkryłem jedną prawdę o ludziach używających vi - im dłużej go używają,
tym mniej mają do niego uwag. Jest on teraz bardziej pospolity w
posiadaniu przypadkowych sekwencji escape w innych edytorach niż dla
mnie do próby wprowadzania w linii komend w vi.

Po pierwsze, czy wiedziałeś że, vi posiada plik konfiguracyjny (tak jak
każdy inny program Linuksa)? Możesz pomyśleć, że nazywa się .virc. Nie
prawda. Nazywa się .exrc. Jest tak, dla tych co nie wiedzą, ponieważ vi
jest pełnoekranowym trybem edytora ex (jednoliniowego). Faktem jest, że
w niektórych miejscach, vi i ex odwołują się do tych samych programów,
które poprzez użycie arv\[0\] determinują czy ma się uruchomić w trybie
pełnoekranowym czy nie.

Pewnie zapytacie co można zrobić z tym plikiem .exrc. Coż, powiem Wam.
Przede wszystkim, nie znoszę głupiego domyślnego ustawienia tabulatora
na 8 znaków. Jest za duży do programowania. Po pewnym czasie wszystko
nie mieści się na ekranie. Chcąc zmienić to na bardziej rozsądną
wartość, spróbuj dodać "set tabstop=3". Ustaw taką wartość jaka Ci się
podoba. Możesz poza tym użyć pliku do ustawienia każdego innego
polecenia vi, wyglądu, timeoutu itd. Sprawdź go.

Następny trik z vi jest też dobry. Jeżeli używałeś jednej z nowych
możliwości procesora słów w Windowsie, zwróciłeś może uwagę na "nową"
możliwość. Jest zazwyczaj wykorzystywana do ciągłego sprawdzania pisowni
lub do czegoś podobnego. Sprawdza co wprowadzasz i jeżeli stwierdzi
pomyłkę w pisowni, natychmiast ją poprawia. Cóż, vi miał to od lat. A
tu przedstawiam jak to działa. Spróbuj dodać następującą linię do
swojego pliku .exrc

ab cant can't
ab dont don't
ab HW Hello World

Teraz uruchom ponownie vi i spróbuj wpisać cant dont i HW. Kiedy to
wpisujesz, vi natychmiast zamienia to co wpisałeś, na to co znajduje się
po prawej stronie za "ab".

Może nie wygląda to na użyteczne, ale może być o wiele bardziej
rozbudowane. Spróbuj dodać następujący tekst:

ab homead sMyNameCtrl+vReturn123 MyStreetCtrl+vReturnMy Twn, MyState

Teraz, kiedy wprowadzisz home, na ekranie zobaczysz swój adres. Inną
wygodną rzeczą której używam jest:

ab ee wessling@emunix.emich.edu

W tym momencie tylko dwa klawisze wprowadzaja mój adres e-mail do
dowolnego dokumentu. Nie przejmuj się tym, że ee może zmienić słowo
'week' - nie zrobi tego.

To powinno działać w prawie wszystkich edytorach vi, ale nie gwarantuję
tego. To jest tylko bardzo mały przykład tego co możesz robić z vi. Jest
bardzo prosty w dostrajaniu i posiada spore możliwości.

Przy okazji, jest jeszcze bardzo wiele podobnych trików.

                   jEnS Wessling

--
==========================================================================
==== Jens Wessling             == "A compiler ought to compile the    ====
==== wessling@emunix.emich.edu ==    comments and ignore the code."   ====
====                           ==           M. Minsky(or close to it) ====
==========================================================================
```

## moje dwa grosze...

List Jensa przypomniał mi prawdę, którą mama zawsze mi powtarzała, gdy
byłem dzieciakiem:

"Nie ma co roztrząsać się nad smakiem."

Lubię i używam często VIM i też doszedłem do wniosku, tak jak Jens, że
im więcej go używam, tym bardziej go lubię i szybciej mogę skończyć
zadania. To samo niektórzy mogliby powiedzieć, że jeżeli zaczynałbym z
Emacs lub Jed lub Joe lub...:-) Byłbym zachwycony (albo lepiej...
conajmniej wdzięczny :-), gdyby ktoś, kto wie coś na temat Emacsie
napisał o jakiś pomysłach czy sugestiach używania go. Cały czas jestem w
trakcie nauki Emacs i nie mogę powiedzieć żebym miał już za dużo
wiedzy...

Zamieszczam tu jeszcze kilka małych wskazówek pomocnych użytkownikom...

* Wiecie że używając "/" przeszukujecie plik do początku do końca , a
użycie "?" (w trybie komend) pozwoli przeszukać plik od końca do
początku. Poza tym, wciśnięcie litery "n" powtórzy przeszukiwanie do
następnej szukanej dyrektywy.

Jeżeli na przykład szukałbyś każdego wystąpienia URL
rozpoczynającego się od "sunsite.unc.edu" poprostu wprowadziłbyś:

```
/sunsite.unc.edu
```

w trybie komend a przeszukiwanie zatrzymałoby się na pierwszym
wystąpieniu wprowadzonej dyrektywy. Wciśniecie "n" powtórzy
przeszukiwanie do nastepnego wystąpienia. Wciśniecie "?" przeszuka w
przeciwnym kierunku. Teraz "n" będzie kontynuować szukanie wstecz.

* Chcesz powtórzyć poprzednie polecenie ale jesteś zbyt leniwy, żeby
wprowadzić ją w całości? :-) Hej, nie ma sprawy... możemy to
zrobić... Poprostu wciśnij "." w trybie komend i *czary-mary\!\!*
natychmiast "Twoje-życzenie-jest-poleceniem" zostaje spełnione\!
* Hej, coś napisałeś, wydrukowałeś i zorientowałeś się, że marginesy
były *ciut* za długie, a Twoje zdania zostały przycięte na końcach...?

Szok\! ;-)

Cóż, możesz wrócić do edycji i spróbować przesunąć tekst, LUB
skorzystać z malutkiego programiku **fmt**, który sformatuje tekst
za Ciebie. Ta poręczna aplikacja zformatuje tekst w miłe dla oka 72
znakowe kolumny. Nie justuje tekstu, a jedynie ustawia prawy
margines na około 72 znaki tak, że wydruk wygląda dobrze.

Odkąd używam VIM, mogę podświetlić tekst, który chcę sformatować
używając opcji "v" w trybie komend. Przesuwając kursor podświetlam
tekst który chcę podać do fmt. Teraz, wciskam "\!" wykrzyknik i
wpisuję "fmt" po wykrzykniku.

Wciskam ENTER i PRESTO\!, natychmiastowa zmiana. Bardzo fajne.

Możecie uzyskać ten sam efekt w linii komend: Po prostu wprowadźcie
numery wierszy, które chcecie poddać obróbce. Tak więc, jeżeli
chcecie sformatować linie od 10 do 50, powinniście wprowadzić:

``` 
:10,50! fmt
```

wcisnąć ENTER i zrobione\!

