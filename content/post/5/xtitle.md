---
title: Interaktywna zmiana paska tytułu xterm!
date: 2022-02-09
---

* Autor: John M. Fisk
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.nov.html

---

Jest to rodzaj małej a cieszącej rzeczy...

Wiesz że istnieje wiele *rodzajów* fajnych rzeczy, które możesz zrobić z
xterm... powiększyć, pomniejszyć go, zmienić czcionki, ustawić kolory,
wykonywać polecenia, logować się do konsoli... wszystkie rodzaje fajnych
rzeczy, które mam nadzieję, mając w przyszłości chwilę czasu, w pełni
opiszę. Jedną z tych rzeczy które możesz zrobić jest ustawienie tytułu
(nazwy) xterm używając opcji linii komend "-T". Na przykład...

Podejrzewam że naprawdę zmęczył Cię tak samo nudno wyglądający tytuł
"xterm" gdy za każdym razem odpalasz gościa. Więc... w ataku śmiałości,
decydujesz się na to, że to co tak *naprawdę* chcesz to to, żeby xterm
mówił Ci coś w tym rodzaju "Jakie jest Twoje życzenie, o Panie...?"

No wiesz... coś więcej w celu poprawienia swojej pozycji w życiu... :-)

Żaden problem\!

Po prostu odpal xterma używając linii komend:

```sh
xterm -T "Jakie jest Twoje życzenie, o Panie...?" &
```

i *voila\!*, natychmiastowy hołd\! Kocham taką służalczość... To jest
*TWÓJ* system... rządź nim\! Linux jest na Twoje skinięcie i zawołanie\!

Witam wśród życzliwych despotów...

Hmmm... trochę porywające, co nie? :-)

Jakkolwiek dostaliście punkt zaczepienia. Możesz zmienić tytuł na
cokolwiek. Ale podejrzewam, że chcecie zmieniać tytuł *interaktywnie*?
Albo więcej, chcecie aby pasek tytułowy wyświetlał innego rodzaju
użyteczne informacje oprócz tego jaki to jest program. Tak jest,
prawdopodobnie chcecie zmienić tytuł *po* wystartowaniu xterma lub
chcecie, aby wyświetlał kilka użytecznych informacji. No cóż, zobaczmy
co da sie z tym zrobić.

Ci z Was, którzy mają dostęp do "X Window System User's Guide" wydanego
przez O'Reilly, mogą przeczytać rozdział 9 o dostrajaniu X Window. Jedną
z interesujących opisanych rzeczy jest użycie sekwencji escape do
wymuszenia wyświetlania przez xterm katalogu w którym aktualnie
przebywamy. Jest to miło opisane na stronie 259. Rzecz w tym, że
potrzebujesz używać C Shella żeby to poprawnie działało, ponieważ
używana jest zmienna "cwd" do sprawdzenia, gdzie jesteś. W czasie
grzebania w xterm nie potrafiłem zmusić BASHa do współpracy, ALE,
nauczyłem sie kilku trików, które są swego rodzaju zabawne.

Bez wdawania się w długie dyskusje, co to jest sekwencja escape i jak
jej używać, wystarczy powiedzieć, że możesz jej użyć do interaktywnej
aktualizacji paska tytułu xterma.

Ostrożnie..\!\! *Może* być z tego niezła zabawa.. :-)

Prawdopodobnie najprostszym sposobem zrobienia tego jest dodanie wpisu
do swojego \~/.bash\_profile i zawarcie albo w funkcji albo w aliasie.
Zobaczmy jak to wykonać. Wszystko czego potrzebujesz to dodanie
nastepujących wpisów:

```
    # Użyj tych funkcji do ustawienia tytułu i etykiety do interaktywnego xterma.
    # Zobacz opis w "X Window System User's Guide" na stronie 764
    #
    #Argumenty ustawiające akcję:
    #
    #   0 = zmiana nazwy Okna/Ikony i Tytułu Okna
    #   1 = zmiana nazwy Okna/Ikony
    #   2 = zmiana Tytułu Okna
    #
    # xtitle() ustawia interaktywnie tytuł xterma. Jeżeli wywołasz funkcję bez żadnych argumentów
    # format domyślny będzie "terminal: xxxx date: xxxx"
    #
    xtitle()
    {
        if [ "$*" != "" ]; then
            echo -n "^[]2;$*^G"
        else
            echo -n "^[]2;xterminal:`/usr/bin/tty` date: `date '+%A %B %d, %Y'`^G"
        fi
    }
    
    #
    # xlabel() ustawia nazwę Ona i Ikony xterma
    #
    xlabel() { echo -n "^[]1;$*^G" }
    
    #
    # xtime() pokazuje aktualna date i czas w pasku tytułowym xterm
    #
    xtime()
    {
        echo -n "^[]2;current date: `date '+%A %B %d, %Y  %l:%M %p'`^G"
    }
    
    #
    # teraz, zrobimy proste sortowanie żeby otrzymać bardziej funkcjonalny tytuł xterm
    #
    if [ "$?PROMPT" ]; then
        if [ "$TERM" = "xterm" ]; then
            echo -n "^[]2;xterminal:`/usr/bin/tty` date: `date '+%A %B %d, %Y'`^G"
        fi
    fi
```

Zobaczmy co się tutaj dzieje.

Podstawowy pomysł jest taki, że poprzez wysłanie sekwencji escape do
xterm, możesz zmusić go do wykonania określonej akcji, która w tym
przypadku uaktualnia pasek tytułowy. Sposobem na zrobienie tego jest
użycie polecenia **echo -n** a nastepnie sekwencję escape.

Jeżeli używasz edytora VI, sposobem na umieszczenie znaku specjalnego
(takiego jak ESCAPE lub CTRL-G) jest użycie CTRL-V, który umożliwi ci
wprowadzenie znaku specjalnego lub wprowadzenie wartości dziesiętnej
znaku. Będziesz potrzebował tej możliwości żeby wprowadzić znak escape i
znak CTRL-G. To co wprowadzisz to:

``` 
ESC ] 2 ; tekst CTRL-G
```

Przy okazji musisz zamknąć całe wyrażenie w podwójnych cudzysłowach. Tak
więc, jeżeli wpiszesz to do swojego \~/bash\_profile, lub gdziekolwiek
indziej, wprowadź:

```sh
echo -n "
```

a następnie wciśnij CTRL-V, a potem klawisz ESCAPE. Wstawi to znak
specjalny ESC do pliku, który jest reprezentowany przez "^\[". Teraz
wprowadź resztę danych, aż dojdziesz do końca, gdzie jeszcze raz
wciśniesz CTRL-V, a następnie kombinację klawiszy CTRL-G. Wierzcie mi,
że jest to o WIELE prostsze do zrobienia niż na to wygląda...:-)
Pokombinuj z tym parę razy a zobaczysz jakie to proste. Naprawdę :-)

Spoglądając na powyższy przykład, można znaleźdź kilka wariantów w tym
temacie. Pierwszy, "xtitle" interaktywnie ustawia tytuł, poprzez proste
wywołanie:

```sh
xtitle "Co jest Doktorku?"
```

Jak widzisz, jeżeli wywołujesz go z ciągiem znaków, wtedy wyświetla te
znaki. Jeżeli wywołujesz go bez argumentów, domyślnie wykonuje
nastepujące polecenie:

```sh
    else
        echo -n "[]2;xterminal:`/usr/bin/tty` date: `date '+%A %B %d, %Y'`^G"
```

które na wyjściu poda coś w tym rodzaju:

```sh
xterminal: /dev/ttyp0 date: Saturday November 25, 1995
```

na pasku tytułowym. Zauważ, że używasz "odwróconego apostrofu" lub,
bardziej prawidłowo, znaku **grave** do zamknięcia poleceń
**/usr/bin/tty** i **date**.

Zauważ, że w tym przypadku użyłem polecenia tty do wskazania, którego
TTY używam. Poza tym użyłem polecenia **date** które, jak zwykle w
UN\*X, posiada w linii poleceń mnóstwo opcji, pozwalających uzyskać
wszystkie możliwe efekty. Te dwa polecenia wyświetlają terminal i datę.
Całkiem funkcjonalne, ale nie pisz o tym do mamy...

Ale czekaj\! Jest tutaj paradygmat\!\!

Zobacz, używając podstawowych mozliwości sekwencji escape możesz
wyświetlać każdego rodzaju fajne polecenia. Poprzez używanie poleceń --
zamykając je w odwróconych apstrofach, które wyświetlą wynik danego
polecenia -- masz doskonałą kontrolę nad tym co może być wyświetlane.

Weźmy na przykład funkcję **xtime**. Też używa pomysłu z sekwencją
escape, ale tym razem pokazuje aktualny czas w godzinach i minutach.
Wszystko co daje możliwość wypisania w pojedynczej linii może być
potencjalnie użyte. Czas przymierzyć czapeczkę mądrali i zrobić coś
ciekawego\! :-)

Dalej...rozgryź to\!

Na koniec, jeżeli chcesz aby pasek tytułu był kastomizowany za każdym
razem, gdy odpalasz xterm, zainteresuj sie ostatnią z powyższych
funkcji. To co zauważysz,to znacznik "if" kóry testuje, czy używamy
xterm (X Terminal) czy terminala znakowego. Jeżeli używamy xterm, wysyła
określoną sekwencję escape. Jeszcze raz, nie ma nic o czym można
powiedzieć, że nie można tego skastomizować na cokolwiek byś chciał...

Baw się\!

Aha, przy okazji, dwa ostatnie małe punkciki.

Zauważysz w powyższych komentarzach funkcji wspomnienie o zmianie tytułu
okna, window &, nazwę ikony, albo wszystkie naraz. Zrobisz to przez
zmianę argumentu nastepującego po znaku "ESC \]". Opcje zawierają:

``` 
0 = zmiana nazwy Okna/Ikony i Tytułu Okna
1 = zmiana nazwy Okna/Ikony
2 = zmiana Tytułu Okna
```

Wybrałem tylko zmianę tytułu okna, ale jak widzisz, możesz zmienić nazwę
okna i ikony, tak jak metoda użyta do Tytułu Okna.

I na koniec...

Jeżli zdaża Ci się używać VIM w xterm, zauważysz, że gdy go otwierasz,
wysyła on własną sekwencje escape (nie jesteś jedyny który zna ten
trik...), która wyprowadza nazwę pliku na pasku tytułowym. Gdy
zakończysz edycję, wychodzi tam skąd zacząłeś. Jeżeli chcesz to
wyłączyć, poprostu dodaj do swojego pliku \~/.vimrc:

```sh
set notitle
```

Zapobiegnie to wyświetlaniu na pasku tytułowym.

Dobrej zabawy\!


