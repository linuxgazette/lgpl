---
title: Skrzynka Pocztowa
date: 2022-02-09
---

* Autor: John M. Fisk
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.nov.html

---


Cóż, jak zwykle, poczta przychodząca i wychodząca **FiskHaus**
(najfajniejsza skrzynka linuksowa) była dość świeża. Naprawdę doceniam
pomysły i sugestie, krytykę i raportowanie mojej dziwnej wymowy oraz
błędów w kodzie HTML. Strasznie się starałem zamieścić tu Was
wszystkich, co najmniej krótką notatkę. Tym, którzy prosili o pomoc
również próbowałem odpowiedzieć w najlepszy sposób jaki mogłem.

Miejcie na uwadze... NIE jestem guru Linuksa\!

Linux Gazette narodził się z moich własnych doświadczeń, prób i często
popełnianych błędów, a pewnego rodzaju sugestie i oferty innych są o
wiele inteligentniejsze i bardziej zaawansowane niż moje. Jeżeli chodzi
o mnie, Linux jest doskonałą "nauką życia", która jest tym co powoduje
WIELKĄ ilość zabawy.

Dobra, zaczynam paplać...

Do każdego co coś napisał... Dzięki\!

## Kontynuacja skryptu PPP - Adama Schlesingera

```
Date: Wed, 08 Nov 1995 09:28:37 CST  
Sender: \<adams@Morgan.COM\>  
From: Adam Schlesinger \<adams@Morgan.COM\>  
To: \<fiskjm@ctrvax.Vanderbilt.Edu\>  
Subject: Dzięki\!\!\!\!\!  
  

Cześć,

Dopiero co postawiłem Linuksa i jestem w trakcie prób mojego połączenia
ppp - nie mogło to się stać w bardziej odpowiednim momencie. Jestem
programistą C++ w Unixie - ale modemy/sprzęt itd - przyprawiają mnie o
nerwicę.

Mam jedną uwagę na temat jednego z Twoich skryptów (nie mam na myśli nic
krytycznego) - ale z perspektywy czystego uniksa manipulowanie
dynamicznym adresem IP jest niebezpieczne. Na stałym (standalone) - może
Ci to ujść na sucho, ale to nie jest dobra praktyka.

    127.0.0.1       localhost
    $IP     MyMachine.vanderbilt.edu                MyMachine" > /etc/hosts

Zmieniłbym to na identyfikację linii zawierającej
MyMachine.vanderbilt.edu i zmodyfikowałbym ją dla nowego adresu IP.
Łatwo jest to zrobić w perlu - lub trochę gorszą metodą wyglądałoby to
w następujący sposób:

    grep -v MyMachine.vanderbilt.edu  /etc/hosts > /tmp/etc.hosts
    echo $IP     MyMachine.vanderbilt.edu   >> /tmp/etc.hosts
    cp /etc/hosts /etc/hosts.bak
    cp /tmp/etc.hosts /etc/hosts

Inna sprawa, że właścicielem /etc/hosts powinien być root, a do
generalnego setup'u myślę, że przekazujesz luźne prawa dostępu - albo
uruchamiasz wszystko jako root. Bezpieczniej do tego typu zadań jest
mieć skrypt uruchamiany z prawami root (skrypt setuid).

Dzięki wielkie za Twoją pracę - nie przejmuj się moimi dwoma
groszami:-\>

Dzięki,  
adam  

--
----------------------------------------------------------------------
Adam Schlesinger
email@morgan:                                 adams@morgan.com
email@home:                                 mada@panix.com
work phone:                                              (212)762-2289
----------------------------------------------------------------------
```

Aktualnie, jestem naprawde WDZIĘCZNY Adamowym dwóm groszom... Jego
wskazówka jest dobra i chcę ją tu umieścić jako jeden z punktów. To
prawda, że cały czas grzebię w moim systemie jako **root**. Wprawdzie to
nie jest dobry pomysł i powoli będę pozbywał się tego płaszcza,
szczególnie po jego wypłowieniu od jakiegoś czasu.

Ponieważ używam Linuksa na moim domowym PC, jako pojedynczego systemu i
jestem jedynym używającym go (moja ukochana żona jest bardzo pomocna,
ale niekoniecznie jest zainteresowana nauczeniem się UN\*X'a) pozwalam
sobie na robienie czegoś, czego nie mógłbym robić w innej konfiguracji.

Nie tyczy się to tylko skryptów PPP, ale też wielu innych sugestii,
które podałem. Nie jestem "zorientowany" na bezpieczeństwo,
przynajmniej nie w tym momencie, tak więc od Was zależy bezpieczeństwo
waszego systemu, jeżeli pracujecie w wieloużytkownikowym otoczeniu.

*Caveat emptor* -- John

## Sugestia co do .hushlogin - Jeff Bauer

```
Date: Thu, 09 Nov 1995 22:20:42 CST  
From: Jeff Bauer \<jeff@medsup.com\>  
To: \<fiskjm@ctrvax.Vanderbilt.Edu\>  
Subject: sztuczka z hushlogin  
  

Chcę tylko powiedzieć, że podziwiam Linux Gazette. Mała sugestia co do
Twojej sztuczki z hushlogin.

zmień: touch .hushlogin --\>\>\> touch \~/.hushlogin

Da to pewność stworzenia go w katalogu użytkownika. Możesz poza tym
zaznaczyć, że .hushlogin jest strasznie przydatny do automatycznego
logowania (uucp, slip/ppp), gdzie wylew otwartego tekstu może być
nieprzyjemny. Możliwość ta jest nieodzowna w aplikacjach typu
"pen-based" używanych w Apple Newton.

Z królewskim pozdrowieniem.

<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
<> Jeff Bauer                           Okay, I'm on the Internet.  <>
<> Medical Support Services, Inc.       Now where's the money?      <>
<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>
```

Wielkie dzięki Jeff'owi za sprostowanie konfiguracji\! --John


## Uzupełnienie pułapek i czyszczenia ekranu przy wylogowywaniu według 
GarrettZilla :-)

```
Date: Sat, 04 Nov 1995 23:30:02 CST  
Sender: \<gpn@osf1.gmu.edu\>  
From: GarrettZilla \<gpn@osf1.gmu.edu\>  
To: \<fiskjm@ctrvax.Vanderbilt.Edu\>  
Subject: Użycie pułapki do czyszczenia ekranu przy wyjściu  
  

Chociaż ustawienie pułapki do wyjścia z shella i wyczyszczenia ekranu
jest doskonałą drogą (uzywam tego w pracy, gdzie mam Korn shella),
istnieje prostsze rozwiązanie w bashu: wystarczy wstawić komendę do
pliku .bash\_logout. Jest to plik z poleceniami wykonywanymi w trakcie
opuszczania shella. Możliwośc ta została przekopiowana z C-shella, w
którym ten plik nazywał się .logout.

Choć ustawianie pułapek jest opisane w "jak to zrobić" w shellach Bourna
i Korna - to jeżeli włożysz to w ten podzbiór i zmartwisz się o wsteczna
kompatybilność, powinieneś prawdopodobnie zrobić to tym sposobem.

Zdrówka,  
Garrett  
```

Doceniam notatkę Garretta o używaniu pułapek i plików .logout. Obawiam
się że mam małe doświadczenie w używaniu shelli oprócz BASHa i chętnie
przyjmę uwagi, które pozwolą mi na dowiedzenie się trochę więcej o tym.
BASH jest naprawde świetnym shellem, który ma mnóstwo możliwości.
Zdecydowanie jest on kandydatem na fajny "man bash | col -b \> bash.txt"
trik... --John

A teraz dla wszystkich hardcorowy konwert DOS'a...

## Referencje Jed i Joe według Eric'a Hultin'a i Jeppe'a Sigbrandta

```
Date: Mon, 23 Oct 1995 17:06:21 CDT  
From: Eric Hultin \<ech3@lehigh.edu\>  
To: \<fisk@web.tenn.com\>  
Subject: Linux Gazette  
  

Naprawde lubię Gazetę (nie znałem niektórych opisanych przez Ciebie
sztuczek), robisz dobrą robotę. Chciałbym przedstawić małą wtyczkę do
jednego z moich ulubionych edytorów: jed'a

Dlaczego lubię jed'a? Cóż miło że pytasz, jed jest naprwadę świetnym
edytorem, który koloruje różne edytowane pliki. (Musisz upewnić się czy
definicja USE\_ANSI\_COLORS = 1; istnieje w pliku
/usr/lib/jed/lib/jed.rc) Możesz ustawiać kolorowe kodowanie w pliku.
Moduł c który koloruje kod języka c jest bezcenny, ale edytor koloruje
poza tym inne pliki, takie jak .html, czy .tex zwiększając
przejrzystość. Odkąd wiele programuję i uczę się c przez grzebanie w
kodach źródłowych, IMHO ten edytor był naprawdę pomocny w opanowaniu c,
sprawdź go i zobacz co o nim myślisz. (Zawiera go dystrybucja slackware)

    Eric Hultin  x1373   | "Doctor's mistakes you bury,
    ech3@Lehigh.edu      |  Engineer's mistakes you live with forever."
    Undergraduate MechE  | A.K.A. Atilla => telnet ech3.res.lehigh.edu 6789
    "Frustration has taken it's control"- via Pantera my opinion of ME205
```

```
Date: Sun, 29 Oct 1995 20:10:18 CST  
From: Jeppe Sigbrandt \<jay@elec.gla.ac.uk\>  
To: \<fiskjm@ctrvax.Vanderbilt.Edu\>  
Subject: vi ?? kurcze nie \!\!\!\!\!\!\!\!  
  

Cześć raz jeszcze,

Zmodyfikowałem linię, pamiętam czytałem wcześniej w gazecie:

Jeżeli musisz używać vi, używaj vim. Jeżeli musisz używać vim używaj
jove. Jeżeli musisz używać jove, uzywaj joe.

Joe to jest business. Odkryłem go dzisiaj i prawie zamordowałem vi,
który pewny siebie mógł zrobić wszystko co było dobre. Po 3 sekundach
używania poczułem się szczęśliwy.

mój plik cshrc wygląda teraz następująco:

alias "vi"   joe
alias "vim"  joe
alias "jove" joe
alias "edit" joe


Pozdrawiam,  
jay  

ps: czytaj tego emaila z przymróżeniem oka. Używałem joe przez trzy
sekundy. Ale to było wystarczające dla kogoś kto wychował się na dosowym
edit!!!

wskazówka: Spróbuj uruchomić ekran pomocy. Zobaczysz większość komend
poprzedzonych ctrl+k. Użyj ctrl+k+r do wstawienia pliku. Spyta Cię o
nazwę. Nie musisz wiedzieć wszystkiego. wciśnij tab tab, użyj kursora do
nawigacji w strukturze katalogów i wybierz plik. Nie mogło być prościej
i bardziej intuicyjnie. Więc dlaczego oni używają r do otwarcia i
wstawienia pliku?
```

Naprawdę doceniam tesksty tych chłopaków, ponieważ jest do doskonałe
przypomnienie o preferencjach\!:-) Świetny start *Jehads*, podsycony
nieśmiertelną gorliwością stronników w poszczególnych OS'ach i
programach. Wobec tego.... lubię VIM i jestem zadowolony z tego małego
programu ponieważ używając go mogę wiele rzeczy zrobic szybciej. Dla
Erica i Jeppe'a, Joe i Jed były kartą wstępu...

Świetne myslenie o Linuksie... zaoferowaliście programy do zabawy.
Eksplorujcie i bawcie się\! --John


## Wyjaśnienie do urlget przez Jacka Lunda**

```
Date: Wed, 22 Nov 1995 10:01:12 CST  
Sender: \<zippy@cc.utexas.edu\>  
From: Jack Lund \<j.lund@cc.utexas.edu\>  
To: \<fiskjm@ctrvax.Vanderbilt.Edu\>  
Subject: url\_get i Perl  
  

Witam.

Jeden z Twoich zapalonych czytelników przekonał mnie do Twojego
doskonałego magazynu online, w którym był list o moim skrypcie
url\_get. Jestem naprawdę rad, że wszyscy z niego skorzystali, a
szczególnie miło słyszeć że świetnie pracuje on pod Linuksem.

Chciałem wyjaśnić jedno małe niezrozumienie - url\_get \*pownien\*
pracować równie dobrze pod Perl 4 (specifikacja 4.036) jak i pod Perl 5
(wspominałeś, że jeszcze nie próbowałeś go, bo jeszcze nie uaktualniłeś
do 5.001).

W każdym razie dziękuję bardzo za reklamę i sławę i gratuluję
doskonałego magazynu.

--
Jack Lund                     "The dead have risen from the grave,
Graphics Services              and they're voting REPUBLICAN!!!"
UT Austin Computation Center                         -Bart Simpson
j.lund@cc.utexas.edu     www: http://uts.cc.utexas.edu/~zippy/
```

Wielkie dzięki Jack, że znalazłeś czas na napisanie listu i
sprostowanie\! Mam nadzieję, że teraz gdy LG jest mirrorowane w kilku
różnych lokacjach, połaczenie nie jest tak wolne jak we wrześniu.
Podziękujcie Jack'owi\! -- John


## Wskazówka do xwininfo według Jason Lewis**

```
Date: Fri, 10 Nov 1995 02:18:05 CST  
From: \<krusty@blitzen.canberra.edu.au\>  
To: \<fiskjm@ctrvax.Vanderbilt.Edu\>  
Subject: linux gazette  
  

Cześć,

Chce tylko powiedzeć że według mnie Linux Gazette jest naprawdę cool.
Lubię Twój styl pisania, przywraca siły do życia.:)

Jestem trochę zdziwiony, że znasz program zwany xwininfo.

Jeżeli go uruchomisz, klinij w okno - pokaże Ci się wiele informacji na
temat tego okna. Jedną z interesujących części informacji jest polecenie
geometrii do otrzymania dokładnie takiego okna jak to (doskonałe do
wycinania i wklejania do .fvwmrc lub czegoś w tym rodzaju)

Dzięki za robotę wkładaną w Gazetę.

Jason

\-- krusty@blitzen.canberra.edu.au

Date: Sat, 11 Nov 1995 16:56:49 CST  
From: Jason Lewis 932535 \<krusty@blitzen.canberra.edu.au\>  
To: John M. Fisk \<fiskjm@ctrvax.Vanderbilt.Edu\>  
Subject: RE: linux gazette  
  

John

On Fri, 10 Nov 1995, John M. Fisk wrote:

    >
    Dzięki za notatkę! Taa... trochę się z tym wygłupiłem, masz rację...otrzymałeś interesujące info:-)
    > Nie jestem wystarczająco dobrym "magikiem" do jego pełnego używania, ale to jest rodzaj zabawy!
    >
    > dzięki za sugestie!

Nie martw się, najbardziej interesująca rzecz dla mnie to taka, że mogę
przeskalować okno i jego pozycję tak jak chce. Mogę użyć xwininfo do
"zdjęcia" polecenia geometrii, żeby otrzymać okno w takim a nie innym
miejscu i takiego a nie innego rozmiaru. Nie muszę o tym mysleć. Mogę
poprostu wyciąc i wkleić informację geometryczną prosto do stosownej
linii.

Niemniej jednak, do zobaczenia.

Jason.

 ____________________________________________________________________________
/Jason Lewis - System Administrator for ucnet - krusty@blitzen.canberra.edu.au\
| Be alert. Be vigilant. Behave!                                              |
\krusty@ise.canberra.edu.au __________________________________________________/
```

To jest jeszcze *jedna* poręczna sugestia do tworzenia i modyfikacji
\~/.fvwmrc w trochę łatwiejszy sposób. Otrzymując okna programu w
prawidłowym rozmiarze to jedna z tych rzeczy, która ZNACZNIE upraszcza
życie i tworzy Twój desktop bardziej funkcjonalnym. Próbowałem
wskazówki Jason'a na temat xwininfo i jest naprawdę prosta w użyciu.
Poprostu otwórz w swoim ulubionym edytorze \~/.fvwmrc lub systemowy
.fvwmrc, ustaw okno w pozycji w jakiej chcesz, a następnie uruchom
xwininfo w xterm'ie. Możesz go odpalać wielokrotnie, kliknij na każde
okno, pobierz informacje o jego geometrii (ustawieniu), a następnie
wklej prawidłowe dane to swojego pliku fvwmrc. Bardzo poręczne\! --
John


## Wskazówka dotycząca przeglądania stron w trybie off-line!

```
Date: Thu, 16 Nov 1995 23:12:54 CST  
From: \<toms@worldgate.com\>  
To: John Fisk \<fiskjm@ctrvax.Vanderbilt.Edu\>  
Subject: Linix-Gazette...dwugroszowa wskazówka (?) \!  
  

Jeszcze dwa grosze do Twojej kolekcji. W pośpiechu to wysyłam, jako że
wygląda na to, że większość linuksowców ma podobne urzeczywistnienia
powszechnych problemów. Jednym z łatwiejszych spsobów na zaoszczędzenie
pieniędzy z połączeń 'ppp' jest zapisywanie swoich długich stron
internetowych, a następnie przeglądanie ich w trybie off-line.
(Linux-gazette-październik)

Jeżeli chcesz zapisać nawet duże projekty, zapisuj pliki w trybie
ŹRÓDŁOWYM a wtedy możliwe będzie używanie jakichkolwiek linków w
dalszym przeglądaniu. Żeby to zrobić, użyłem NCSA httpd\_1.3 server
dostępny na
[sunsite.unc/edu](ftp://sunsite.unc.edu/system/Network/infosystems/httpd_1.3.tar.gz)
(Aktualnie, jest on na Infomagic CD, nowsza wersja jest na Sunsite)
Dyrektywy mogą być czytane w edytorze tekstowym (są tam w formacie
źródeł http) Po instalacji, Twoja przeglądarka powinna "zobaczyć"
zapisane pliki html w \<http://localhost/...\>, gdzie możliwość
indeksacji Netscape'a może udostępnić wszystkie pliki w katalogu
DocumentRoot oraz umożiwić dostęp do Twojej strony domowej. Moja strona
domowa jest poprostu edytowaną kopią najlepszych bookmarków, które
znalazłem i przez 'Welcome.html', do \<DocumentRoot\> mogę ładować każdy
dowolny dokument html i następnych twardych linków.(?) Realne jest
uzyskanie 3-megowej strony domowej\!

Jest to jest przykład 'WYŻSZOŚCI' Linuksa. Jako, że mam dostęp do
bogactwa możliwości, mogę wybierać oprogramowanie i sposoby operacji
jakie chcę. Próbowałem używać Eudory do poczty (z WIN) ale nigdy nie
miałem miłych wspomnień. Przynajmniej Linux rozłącza mój modem bez
konieczności wyłączania napięcia lub wyciągania wtyczki.

Myslę, że cenię twórców LINUKSA za operacje początkowego ładowania
(bootstrap operation) Poprostu nauka administarcji 'UNIXa' była
pokręconym zadaniem, wydawnictwo O'Reilly zarobiło kupę pieniędzy na
mnie.

Naprawdę cieszę się z Twojej gazety, upłynie sporo czasu, zanim będę
miał całe oprogramowanie które chcę. . Narazie.

    ----------------------------------------------------------------------------
    |Disclaimer: Opinions expressed are my own, and   | Do the neoLudites have |
    |should be taken with a grain of NaCl, EVEN by me.| a Home Page Yet ?      |
    ----------------------------------------------------------------------------
```

Interesująca sugestia dla wszystkich dobrze zapowiadających się
webmasterów. Zacząłem robić coś podobnego do tego, ale w trochę bardziej
uproszczony sposób: Strony, które chcę mieć zapamiętane zapisuję jedynie
jako źródło a następnie używam Netscape'owego menu do zapisania również
obrazu. **Presto\!** - strona ładuje się momentalnie. Wszystko wstawiłem
do tego samego katalogu i stworzyłem bookmarka do tego katalogu. Teraz,
gdy chcę obejrzeć strony nawet gdy nie jestem "online" po prostu odpalam
Netscape'a, ignoruję komunikaty o błędach i używam bookmarka do
wylistowania katalogu. --John


## Ups\!\! :-( Dzieki za wyłapanie tych pomyłek -- Frank i Boaz Studnitzky

```
Date: Tue, 28 Nov 1995 10:03:01 CST  
From: \<frust@iti.cs.tu-bs.de\>  
To: \<fiskjm@ctrvax.Vanderbilt.Edu\>  
Subject: Zgubione \<a\>  
  

Cześć John\!

W październikowym LinuxGazette brak było \<a\>.

Pozycja w górnej linii.

\[snip\!\]

Cześć, Frank.

Date: Wed, 29 Nov 1995 10:19:09 CST  
Sender: \<s3176015@csc.cs.technion.ac.il\>  
From: Studnitzky Boaz \<s3176015@csc.cs.technion.ac.il\>  
To: \<fiskjm@ctrvax.Vanderbilt.Edu\>  
Subject: mały błąd (zły adres)  
  

Sprawdź link do nic.funet.fi. Aktualnie wskazuje na nic.funet.ni.

B.
```

I na koniec... **wielkie** dzięki dla tych, (i szacuneczek również dla
kilku innych :-)), którzy informowali o moich gafach. Dzięki wszystkim
za wyłapanie tych pomyłek i poinformowanie mnie o tym. Narazie --John


