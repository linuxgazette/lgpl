---
title: Zapisywanie logów z inittab
date: 2022-02-09
---

* Autor: Eric Sorton
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.nov.html

---

Date: Sun, 22 Oct 1995 01:31:27 CDT
From: \<eric@smtp.db.erau.edu\>
To: John M. Fisk \<fiskjm@ctrvax.Vanderbilt.Edu\>
Subject: RE: Linux Gazette

    > Miałem kilka pytań 
    dotyczących programu "init"
    > i porównania stylu SysV do "innego stylu?"
    > (Przepraszam, ale o 5:00 rano nie mogę sobie przypomnieć tego innego stylu)

\[*"inny styl" to BSD :-)
    --John*\]

    >W każdym razie padło kilka pytań o to, oraz o sposób efektywnego używania init.
    >
    >Jeżeli jesteście biegli w tym temacie, to każdy pomysł, sugestia lub instrukcja najlepszego
    > używania tego, byłaby naprawdę cenna!

Znam trochę styl BSD (w szkole administruje SunOS 4.1.?) oraz używałem
Slackware, który jest BSD, przez około rok. Niedawno zainstalowałem
RedHat, który jest SYSV i jak na razie próbuję wyznać się w nim. Chociaż
mogę powiedzieć o różnicach pomiędzy nimi, to nie mogę nic więcej. Nie
jestem wystarczającym specem i nie czułbym się komfortowo rozmawiając w
tym temacie bo nie wiem zbyt dużo na ten temat.

Tutaj jest wskazówka, jeżeli możecie ją gdzieś dopasować... Jest
związana z plikiem inittab. Potrzebuję/chcę wyświetlić plik logu na
VT... Widywałem ludzi robiących to przez syslog i przekierowywujących
sysloga na VT (poproście to wytłumaczę), ale mój plik log nie używał
sysloga. Wymyśliłem taki sposób:

Wstaw nastepującą linię do inittab'a:

    c7:45:respawn:/usr/bin/tail -f /var/adm/log.smb >> /dev/tty7

Tu jest krótkie wyjaśnienie znaczenia poszczególnych pól:

  - c7 to VT (użyj c1 dla VT1, c2 dla VT2, itd...)

  - 45 to poziomy uruchomienia (runlevel), użyj tego jako domyślnych dla
    Slackware, użyj 3 jako domyślnych dla RedHat.

  - respawn ponownie uruchamia jeżeli proces został ubity z
    jakichś przyczyn...

  - /usr/bin/tail to polecenie do uruchomienia, w tym przypadku
    tail -f wyświetla końcówkę pliku i kontunuje wyświetlanie nowych 
    danych z pliku które są do niego dodawane.

  - /var/adm/log.smb to plik który ma być wyświetlany na A.

  - /dev/tty7 to terminal (VT) na którym mają być wyśiwetlane
    informacje powinien być taki sam jak w pierwszym polu.

Mam nadzieję, że jest to jasne. Jest to trochę niedorobione, ale myślę,
że to dobry punkt zaczepienia. Jak chcesz to dodaj ją do gazety, sądzę
że będzie użyteczną wskazówką.

    --
    /-=-=-=-=-=-=-=-=-=-=-<<< ERIC F SORTON >>>-=-=-=-=-=-=-=-=-=-=-\
    |   eric@db.erau.edu -- Embry Riddle Aeronautical University    |
    | Graduate Administrator Aerospace Engineering Computer Systems |
    |   Phone: (904) 226-6752       Office: E&T 208     |
    |-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-|
    |  Certainly the game is rigged.  Don't let that stop you;      |
    |        if you don't bet, you can't win.      --RAH            |
    \-=-=-=-=-=-=<<<  HTTP://ERAU.DB.ERAU.EDU/~ERIC/ >>>=-=-=-=-=-=-/

## Logowanie komunikatów do pliku i na konsole...

Pełna informacja używania **syslog** do logowania błędów systemowych
jest trochę obszerniejsza niż ta którą chcę teraz przedstawić. Niemniej
jednak Eric podjął temat, który prawdopodobnie wart jest zwrócenia
uwagi: jednoczesne zapisywanie komunikatów systemowych do pliku i na
ekran.

Logowanie błędów według stylu BSD, najczęściej używanego w wielu różnych
dystrybucjach Linuksa, wykorzystuje demon **syslog** do nadzorowania
komunikatów systemowych i błędów generowanych przez różne procesy oraz
programy i zapisywania ich do wyspecyfikowanego wyjścia. Interfejs ten
jest dostępny dla wielu różnych procesów takich jak procesy drukowania,
poczty, jądra, autentyfikacji itd. Ci z was, którzy ustawiali PPP, a
także używali pliku README dostarczonego do ppp-2.1.2\[x\], przeczytali
pewnie wskazówki dotyczące ustawiania logowania błędów.

Jeszcze raz, nie jest to rozdmuchana dyskusja o logowaniu błedów... jest
to przedstawienie możliwości demona syslog do zapisywania komunikatów
systemu zarówno do pliku jak i na konsolę, poprzez wprowadzenie wpisu
dla określonego procesu w /etc/syslog.conf.

Używając przykładu z setupu PPP: logowanie błędów PPP dostępne jest po
wpisaniu:

```
    local2.*                                        /var/adm/ppplog
    local2.*                                        /dev/console
```

w pliku /etc/syslog.conf.

Wart podkreślenia jest fakt, że są tam *dwa* wpisy dla "local2.\*"
Pierwszy kieruje logi do - używanego zazwyczaj - pliku administracyjnego
/var/adm/ppplog. Oczywiście możecie podać dowolną nazwę pliku. Następna
linia też zapisuje logi z "local2.\*", ale tym razem wysyła je do
/dev/console -- wasz ekran\! Poprzez zawarcie podwójnego zapisu dla
pliku i konsoli, możecie zapisywać komunikaty na wiele wyjść.

Do zabawniejszej i bardzo pouczającej dyskusji różnych aspektów
administracji systemem, szczerze polecam książkę:

**Essential System Administration, Second Edition by Aeleen Frisch**
Pub: O'Reilly, ISDN: 1-56592-127-5

Aeleen Frisch to doskonała autorka z wielkim, praktycznym
doświadczeniem. Jej styl pisania z wieloma zadaniami dostarcza
informacji zarówno o skonfigurowaniu różnych rzeczy, jak i o tym jak
dana rzecz działa\! Drugie wydanie zawiera poza tym rozdziały poświęcone
Linuksowi, a także innym popularnym, komercyjnym klonom UNIXa.

Jest to na pewno jedna z tych książek, która powinna się znaleźć na
waszej liście gwiazdkowych
zakupów...!

--John
