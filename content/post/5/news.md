---
title: Niusy!
date: 2022-02-09
---

* Autor: John M. Fisk
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.nov.html

---

## Tym razem dla was!

Cóż, dostałem mnóstwo maili, oraz kilka niezłych sugestii, tak więc w
tym miesiącu LG jest "dedykowane" tym z Was, którzy już coś napisali.
Szczególne podziękowania dla:

* Alan Bailward
* Ross J. Michaels
* Judith Elaine
* Nick LeRoy
* Daniel Sully
* Eric Sorton
* Gary Jaffe
* Jens Wessling

Jeszcze raz chciałbym zaprosić wszystkich do przysyłania swoich sugesti,
wskazówek, sztuczek czy pomysłów. To proste... za każdym razem gdy
"klepiesz-się-w-czoło-myśląc-to-jest-super", poświęć drobną chwilę i
zanotuj to.

Poważnie!

Jeśli natknąłeś się na coś, co według Ciebie jest fajne, lub w końcu
zmusiłeś coś do pracy, istnieje szansa, że gdzieś tam jest wielu innych
którzy jadą na tym samym wózku, ale nie mieli jeszcze takiego farta...

Podziel się tym bogactwem.

I nie przejmuj się swoją formą wypowiedzi \[sic\!\], formatem, czy tym
jak dobry jest Twój angielski... (jak widzisz, mój styl pisania jest
cokolwiek luźny :-) po prostu daj o tym znać, a ja będe szczęśliwy, że
mogę to tu zamieścić.

Dzięki!


## Bożonarodzeniowa przerwa w Linux Gazette\!

Tak, w związku z gorączką przedświąteczną, obiecałem swojej żonie, że
zrobię sobie przerwę w Linux Gazette. Naprawdę potrzebuje spędzić troche
czasu z nią i moją rodziną, tak więc prawdopodobnie grudniowa edycja nie
będzie wydana.

Dlatego mam nadzieję, że wszyscy z Was mieli udany Dzień Dziękczynienia,
a poza tym chciałbym wszystkim życzyć Wesołych Świąt i Do Siego Roku\!

## Dwa nowe hosty dla Linux Gazette\!

Chciałbym podziękować **Phil'owi Hughes'owi** z Linux Journal i
**Alan'owi Cox'owi** z ftp.linux.org za łaskawą ofertę mirrorowania
Linux Gazette. Ci z Was, którzy próbowali dostać się do LG z Europy,
pewnie ucieszą się z faktu, że od teraz jest tam przyjazny mirror
niedaleko waszego miejsca zamieszkania :-)

Informacje o LG znajdziecie na tych stronach:

* http://www.ssc.com/
* http://www.linux.org.uk/

Jeżeli pobieracie LG z tych miejsc, zostawcie ludziskom notkę i
poinformujcie ich o tym -- nie zapomnijcie poza tym powiedzieć
dziękuję\! Ci ludzie prowadzą mirror LG całkowicie za darmo dla
społeczności Linuksa. Pamiętajcie... Wasze mamy zawsze wbijały Wam do
głów "proszę & dziękuję..."

## Tak... cały czas pracuję nad możliwością otrzmywania LG poprzez FTP...:-(

Szczerze przepraszam za to, że LG nie jest dostępny przez anonimowego
ftp w tym miesiącu. Wiem, że pomogłoby to wielu z Was i byłoby lepszym
rozwiązaniem. Poza tym, są osoby, które poprostu nie mają dostępu do
WWW, ale mogą korzystać z ftp.

Jeszcze raz dziękuję za waszą cierpliwość. To jest jeden z grudniowych
projektów i ma on wysoki priorytet.

Jak tylko skończę z tym, to wrzucę stosowane info na
comp.os.linux.announce.

Wielu z Was zainteresował **mirroring LG**. Po rozważeniu co będzie
najlepsze do zastosowania, zdecydowałem co nastepuje:

1. Będe kontynuował zarządzanie Linux Gazette na "oficjalnej stronie"
(Tennessee CommerceNet, RedHat Commercial Linux, The Linux Journal,
WWW.Linux.Org). Tak jest, będę odpowiedzialny za rozwiązywanie
wszystkich problemów jakie stworzyłem, oraz za wstrętną wymowę \[sic\].

2. Ponieważ miałem prośby mirrorowania LG z Korei, Czechosłowacji,
Arabii Saudyjskiej, RPA, Zambii, Wenezueli, Szwecji, Wielkiej Brytanii,
Danii, itd..., a także dlatego, że połączenie ze Stanami do niektórych z
tych krajów jest strasznie wolne...

Zdecydowałem się, że LG będzie mogło być mirrorowane przez każdego kto
zechce. Moja jedyna prośba: jeżeli zdecydujesz się to zrobić, napisz mi
o tym i prześlij mi dwie rzeczy:

* adres strony na której umieściłeś LG
* adres email osoby która tym zarządza

To co teraz robię to strona startowa z mirrorami, tak więc każdy z was
kto próbuje dostać się do tych stron z lokacji nieamerykańskich, może
znaleźć stronę najbliżej miejsca zamieszkania. Poza tym jestem
szczęśliwy mając jakikolwiek mirror LG również w Stanach, niemniej
jednak jestem szczególnie zainteresowany w stworzeniu dostępu dla tych
osób, które na stałe nie mieszkają w poblizu Nashwille, Tennessee:-)

Jeszcze raz, jest to najwyższy priorytet i zrobię wszystko co w mojej
mocy aby wszystko było gotowe w okolicach środka - końca grudnia. Wrzucę
info na c.o.l.announce, kiedy te rzeczy będą gotowe do uruchomienia.


## Pracujemy nad Listą Pocztową

Tak, jest to w tym momencie BARDZO nieoficjalna wiadomość, ale kilka
szlachetnych duszyczek zaoferowało próbne stworzenie i uruchomienie
listy pocztowej dla Linux Gazette.

Otrzymałem o to wiele próśb, ale sam nie mam czasu na zrobienie takiego
czegoś (ani technicznej możliwości w tym momencie). Ale jest tam kilku
poważnych Linux-mózgowców, którzy zaoferowali swoje usługi.

Jeżeli coś z tego wyjdzie, wtedy wszyscy będziemy winni chłopakom
wielkie dzięki. Raz jeszcze, to piękny i szlachetny czyn, choć może z
tego nic nie wyjść... te chłopaki mają przecież też inne rzeczy do
roboty, jak na przykład chodzenie do roboty każdego dnia, coby móc
odebrać wypłatę w każdy piątek... tak więc zobaczymy.

Jeśli nic z tego nie wyjdzie, poinformuję Was o tym i zobaczę, czy będe
mógł zrobić coś innego. Trzymajcie kciuki...

Dziękuje za uwagę\!


## Dostępne są prekompilowane binaria do XF-Mail!

Po korespondencji z niektórymi z Was na temat kompilacji XF-Mail,
napisałem do autora XF-Mail i zapytałem go o prekompilowane binaria dla
Linuksa. Gennady dość szybko odpisał i poinformował mnie, że są one już
dostepne poprzez jego serwer ftp:

```
Sender: \<gena@Burka.NetVision.net.il\>  
From: Gennady Sorokopud \<gena@NetVision.net.il\>  
To: John M. Fisk \<fiskjm@ctrvax.Vanderbilt.Edu\>  
Subject: RE: Precompiled binaries for Linux  

Cześć John\!

Oczywiście rozumiem Twój problem i spróbowałem go rozwiązać. Moja strona
[(burka.netvision.net.il/pub/xfmail)](https://web.archive.org/web/20030813200051/ftp://burka.netvision.net.il/pub/xfmail/)
zawiera prekompilowane pakiety na każdą platformę zawierającą Linuksa.
Jeżeli chcesz dystrybuować prekompilowane pakiety na inne FTP, nie ma
sprawy, zrób to\!

Nie ma żadnych limitów na dystrybucję xfmail, jest on całkowicie za
darmo.

On 27-Nov-95 John M. Fisk wrote:

    >Gennady,
    >
    > Cześć!
    >
    > Mam przyjemność zapytać i zaoferować... :-)
    >
    > Po opisaniu XF-Mail w Linux Gazette dostałem kilka listów o nim...większość nawet pozytywnych.
    > Jednakże, kilku ludzi pisało DŁUGIE listy na temat problemów z kompilacją XF-Mail. Nie mogę sobie wyobrazić
    > co niektórzy z nich robią ale wiem, że aktualna wersja libxpm NIE, na podstawie co najmniej jednego czytelnika, 
    > pracuje -- skompilowane dobrze i po tym seg fault 
    > W przypadku, gdy kompilował z poprzednią wersją libxpm, pracowało dobrze.

Tak, widziałem ten problem. ostatnia wersja biblioteki XPM ma pewne
problemy z narzędziem xforms.

\[SNIP\!\]

    --------
     Gennady B. Sorokopud - System programmer at NetVision Israel.
     E-Mail: Gennady Sorokopud 
     Homepage: http://www.netvision.net.il/~gena
    
     This message was sent at 11/27/95 13:35:26 by XF-Mail
```

Coż, ci z Was, którzy mieli problemy ze zmuszeniem XF-Mail do
kompilacji, mogą spróbować binarek dostępnych na burka.netvision.net.il,
które zawierają biblioteki XPM.

Powodzenia\!

Miłej zabawy.

