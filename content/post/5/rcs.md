---
title: RCS - Zarządzanie Systemowymi Plikami Konfiguracyjnymi
date: 2022-02-09
---

* Autor: John M. Fisk
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue01to08/linux\_gazette.nov.html

---

```
Date: Tue, 07 Nov 1995 14:42:15 CST  
From: Nicholas R LeRoy \<nleroy@norland.com\>  
To: \<fiskjm@ctrvax.Vanderbilt.Edu\>  
Subject: Linux Gazette  
  

John..

Kilka chwil temu wysłałem Ci e-mail o Linux Gazette, a teraz przesyłam
inną wskazówkę, która prawdopodobnie warta jest opublikowania. Nie
szukałem jeszcze wśród wszystkich zagadnień, ale nie widzę tego tutaj.
Jeżeli jest, przepraszam...

W lipcowym numerze opisałeś kopiowanie plików do /config\_dist lub
podobne rozwiązania do backup'u. Mechanizm ten jest rzeczywiście
użyteczny, ale myślę, że jest lepsze rozwiązanie: RCS. Zainstaluj
pakiet RCS i każ mu przechować wszystkie poprzednie wersje. Pozwala to
na przechowywanie historii zmian, opisu zmian itp na wszystkich plikach.
Z RCS możesz spytać: Co zmieniłem w moich plikach? Moja stara wersja
sendmail.cf działała OK, a teraz się popsuła. Co zostało zmienione?
Wcześniej rozwiązałem ten problem. Jak? Itd.

Aha, RCS to skrót od Revision Control System (System Kontroli Rewizji)

Przedstawiam tu krótkie podsumowanie używania RCS. Przyjmijmy, że plik
którym chcesz zarządzać to '/katalog/plik'.

1.  Zdobądź i zainstaluj.  
    Prawdopodobnie powinieneś też zrobić: man rcsintro jako wstęp.  
      
2.  Dodanie pliku do kontroli wersji:  
    cd /katalog  
    mkdir RCS (nie jest wymagane, ale rekomendowane.)  
    Polecenie 'ci -u' wstawi plik pod kontrolę wersji.  
    odpowiedz na pytanie opisu pliku dla RCS.  
    Zauważ, że plik ma teraz prawa r--r--r-- (tylko do odczytu).  
      
3.  kontrola pliku:  
    co -l Kontroluje plik (teraz jest zapisywalny)  
    Teraz możesz zmodyfikować plik  
      
4.  Kiedy skończysz, zablokuj go spowrotem  
    ci -u plik (poprosi cie o wprowadzenie opisu zmian)  
    opis\_zmian (Napisz co zrobiłeś z plikem).  
    (plik znowu ma prawa r--r--r--).  
      
5.  Sprawdzenie historii zmian:  
    rlog plik  
      
6.  Sprawdzenie różnic pomiędzy aktualnym, a ostatnio edytowanym:  
    rcsdiff plik  
      

Posiada wiele innych opcji. Przeczytaj man'a. Warto wysilić się na
naukę\!


[mailto:%20nleroy@norland.idcnet.com](mailto:%20nleroy@norland.idcnet.com)

+--------------------------------------+-------------------------------------+
| /`-_     Nicholas R LeRoy            | Linux -- What *nix was meant to be. |
|{     }/  nleroy@norland.idcnet.com   | gcc   -- What C was meant to be.    |
| \ *  /   Norland Corp                +-------------------------------------+
| |___|    W6340 Hackbarth Rd          |  Escape the Gates of Hell with      |
|          Fort Atkinson, WI 53530     |   The choice of a GNU generation... |
+--------------------------------------+-------------------------------------+
| Hey -- These are my own ideas, not my employer's.  Don't blame them...     |
+--------------------------------------+-------------------------------------+
```

Sugestia Nick'a była jednym z tych
"stuknięć-w-czoło-dlaczego-nie-pomyśałem-o-tym" Powinienem używać RCS
do różnorodych programów, nad którymi pracuje w szkole. Używanie RCS do
zarządzania systemowymi plikami konfiguracyjnymi jest doskonałym
pomysłem. Jest to potężne narzędzie do obserwowania różnych ZMIAN
dokonywanych na przestrzeni określonego czasu. Nick ma świętą rację...
RCS naprawdę wart jest poświęcenia na jego naukę\! --John*\]


