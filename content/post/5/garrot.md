---
title: Wprowadzenie do Garrota
date: 2022-02-09
---

* Autor: John M. Fisk
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.nov.html

---

## Co i JAK

Zauważyłeś pewnie, że podczas uruchamiania DOSEMU, zasoby komputera
gwałtownie maleją. Dzieje się tak, gdyż podczas pracy DOSEMU cały czas
je wykorzystuje (wygląda to podobnie jak w DOSie ). Ratunkiem na
zapanowanie nad tym, jest program pod nazwą **GARROT**. GARROT powinien
być dostępny w podkatalogu DOSEMU na Twoim ulubionym serwerze FTP

Co zrobi GARROT:

\[...\] zwiększa wydajność Linuksa poprzez wymuszenie na systemie
operacyjnym działającym w DOSEMU zwolnienia zasobów CPU z powrotem do
Linuksa, jeżeli nic aktualnie nie robi.

GARROT działa jako TSR w sesji DOSEMU, więc instalacja jest prosta,
szczególnie od momentu, gdy pliki \*.tgz są dostarczane jako binaria\!
Wszystko co zrobiłem, to wydzieliłem podkatalog w dysku DOSEMU'owym C: i
wstawiłem następującą linijkę w moim AUTOEXEC.BAT:

```
lh c:?rrot?rrot -8
```

Liczba jest argumentem nazwanym przez autora "stałą garrota".

## "stała garotta"?

Liczbowy argument daje najlepsze zrównoważenie czasu CPU oddanego
DOSEMU. Przypuszczalne ustawienie to około połowa wartości Twojego Linux
BogoMips. Więc jeżeli Twój BogoMips jest w wersji 16.7 (jak mój),
wystartowałbyś GARROTa z argumentem 8 lub 9.

\[UWAGA: Używam kernela 1.3.30 i BogoMips nie jest już wyświetlany
podczas startowania systemu. Jeżeli masz tę samą wersję jądra, a nie
pamiętasz wartości z wcześniejszego kernela, sprawdź w BogoMips
mini-HOWTO (dostępne w podkatalogu linux/docs w szanujących się
serwerach FTP). Zawiera on informacje jaki BogoMips *powinien* być dla
procesorów x86.\]

## Badanie w TOP

Autor GARROTa proponuje sprawdzanie czy "stała garott'a" pracuje czy
nie, poprzez uruchomienie programu 'top'. Wystartuj DOSEMU (bez
garott'a) w jednym z wirtualnych terminali, a w innym terminalu uruchom
'top'. Zwróć uwagę na zajętość procesora (CPU) przez program 'dos' w
aplikacji top. Kiedy patrzysz w terminal z uruchomionym 'top', DOSEMU
jest wolny (nie pracuje), i nie powinien zabierać zbyt dużo czasu CPU
(44.1% w moim przypadku). Teraz wyedytuj AUTOEXEC.BAT i wstaw linię z
garrot. Zamknij DOSEMU i zrestartuj go.

\[Jeszcze jedna uwaga: Zauważyłem, że jeżeli uruchomisz garrot z
prompta dosa w DOSEMU, 'domyśla' się co do prawidłowego levelu (level =
stała)\]

Teraz jeżeli spojrzysz na VT gdzie masz uruchomiony 'top', powinieneś
zobaczyć że zajętość CPU przez program 'dos' *powinna* **spadać** (w
moim przypadku z 1.0 do 0.1). Możesz pobawić się ze "stałą garrot" aby
otrzymać jak najlepsze rezultaty w DOSEMU.

[Alan Bailward](mailto:ALAN_BAILWARD@MINDLINK.BC.CA) (teraz [URLowany](http://mindlink.net/alan_bailward/)\!)

\[*Po napisaniu przez Alan'a powyższego artykułu o DOSEMU, zaraz potem
napisał drugi siostrzany artykuł o programie Garrot. Proszę napiszcie do
niego podziękowania za dobrą robotę. Jestem absolutnie poważny kiedy
mówię, że Linux Gazette nigdy nie był pomyślany jako jednoosobowy twór.
Stworzyłem ją jako sposób do WYMIANY pomysłów i informacji, dlatego
naprawdę doceniam ciężką pracę wykonana przez Alan'a. --John*

