---
title: Oszczędzanie miejsca z wykorzystaniem dyskietek
date: 2022-02-09
---

* Autor: John M. Fisk
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.nov.html

---

Ten pomysł *może* być przydatny dla tych z Was, którym kurczy się
miejsce na twardym dysku.

Nie wiem jak u Was, ale u mnie zawsze wygląda to tak samo, mam dużo
więcej programów, które chciałbym zainstalować i pogrzebać w nich niż
miejsca na dysku dla nich. Aktualnie na wszystkich partycjach Linuksa
mam już upchane 600 MB i cały czas muszę konserwować przestrzeń dyskową
tak aby mieć miejsce na wszystkie programy, którymi się bawię. Ostatnio,
żeby zwolnić nieco miejsca na dysku, zacząłem archiwizować dokumenty na
dyskietkach i odryłem, że sprawuje się to doskonale.

Jeżeli posiadacie jeden z tych wielogigowych napędów nie jesteście
pewnie zainteresowani tym tematem, ale jeżeli macie nieco skurczone
zasoby, ten pomysł może być trochę pomocny.

Podstawą pomysłu jest:

1.  Zachowywać CAŁĄ dokumentację zainstalowanych programów -- te które
    zostały zainstalowane z danej dystrybucji, albo te które
    instalowałeś samodzielnie ze źródeł lub binariów.

2.  Kompresować je. Poprzez użycie opcji "gzip -9" możesz ścisnąć to do
    imponująco małych rozmiarów. To jest Sugestia Oszczędzania Miejsca
    Nr. 1

3.  Teraz, przenieś wszystkie skompresowane dokumenty na dyskietki.
    Przyznajmy się tutaj, że dyskietki nie są tanie, a koszty dysków
    twardych stale sie zmniejszają, więc może to być nie być
    ekonomiczne. Mi się udało dostać 50 dyskietek no-name za nieco ponad
    20 dolarów w jednym z lokalnych sklepów.

    Możesz rozważyć nazewnictwo dyskietki A, B, C itd... nazwij
    dyskietki i zachowuj je alfabetycznie. Zacznij z 26 dyskietkami a
    nastepnie dodaj to co potrzebujesz.

    Swoje dyskietki sformatowałem używając ext2fs. Jest on co prawda
    nieco nieefektywny odkąd ext2 jest dobrym systemem plików, ale
    zajmuje troche miejsca dla siebie. Mimo wszystko, nie zabiera dużo
    miejsca, a chcę mieć możliwość zapisywania plików z długimi nazwami
    bez potrzeby konwertowania plików tekstowych z UNIX -\> DOS. Ext2
    pozwala mi na to.

    Niemniej jednak, możesz przeformatować dyskietki jeżeli chcesz
    używać ext2 lub minix, lub cokolwiek innego, a następnie przenieść
    pliki na dyskietki. Po prostu stwórz katalog na każdy z programów a
    następnie przenieś skompresowane dokumenty do tych katalogów.

4.  Teraz, kiedykolwiek potrzebujesz przeczytać dokumentację dla
    określonego programu, musisz jedynie wyciagnąć dyskietkę, którą
    wcześniej odpowiednio nazwałeś, włożyć do napędu i użyć **zless** do
    obejrzenia pliku. Pozwoli Ci to na przeczytanie pliku bez potrzeby
    odkompresowywania go.

5.  Przy okazji, program **zless** jest prostym trikiem w shellu:

```sh
#!/bin/sh
for args 
    do
    zcat $args | less
done
```

który po prostu wykonuje zcat na pliku, a następnie **less**. Teraz
możesz czytać dokumentację bez potrzeby kopiowania czegokolwiek na swój
dysk twardy i bez potrzeby jego odkompresowywania. Działa to
nadspodziewanie dobrze i jest nawet szybkie. Dla tych z Was, którzy
skonfigurowali program **supermount** jest szczególnie szybkim sposobem,
bo musicie tylko włożyć dyskietkę i juz możecie rozpocząć czytanie.

Świetnie, IIDSSM. (...to znaczy *If I Do Say So Myself...*)

Przyznaje, że jest to jeden z rodzajów "małych wskazówek"... ale jeżeli
da Ci możliwość wciśnięcia *jeszcze jednego* małego programiku na Twój
dysk twardy, to jest tego warty\! I może za miesiąc porozmawiamy o
"dupie Maryni" ;-)

Zna ktoś jeszcze inne sposoby na oszczędzacze miejsca...? (wiem,
wiem...oprócz triku " rm -rf /c/windo...")

Jeszcze tylko jedna mała uwaga o archiwizowaniu...

Może nie jest złym pomysłem w poważnym zastanowieniu się o
archiwizowaniu źródeł programów, które skompilowałeś i zainstalowałeś
samodzielnie. Uzasadnienie dla tego jest proste. Nie wszystkie programy
umożliwiają Ci rozpostarcie źródeł, a następnie wykonać "make" i
skompilować czysto program. Przez większość czasu musisz edytować
Makefile lub Imakefile, a często musisz zrobić więcej niż małe grzebanie
w źródle, żeby program skompilował się bez problemów. Po wykonaniu tej
całej pracy, prawdopodobnie nie jest złym pomysłem zapisać te zmiany.

Raz jeszcze porozmawiajmy o tym tutaj:

1.  Jeżeli kompilacja ma być czysta, trzeba pozbyć się dodatkowych
    śmieci -- zazwyczaj robimy to poprzez "make clean", który pozbywa
    się wszystkich obiektów, plików tymczasowych i innych tego rodzaju.
    Po prostu pamiętaj o zrobieniu tego *po* zrobieniu "make install", w
    przeciwnym wypadku możesz odkryć, że pliki wykonywalne tajemniczo
    znikają...
2.  Nastepnie, napisz szybkie README, w którym zapisz przynajmniej
    polecenia potrzebne do kompilacji. Niemniej jednak prawdopodobnie
    jest troche więcej informacji niż "make", "make install", "make
    install.man". Okazjonalnie istnieją śmieszne polecenia linii komend,
    które są potrzebne, a Tobie oszczędzą trochę czasu na powtórnym
    przeczytaniu oryginalnego README.
3.  Teraz poprostu wykonaj tar i gzip i zmień nazwę na coś w rodzaju
    "program\_name-1.34-MINE.tar.gz", która poinformuje Cię o tym, że
    samodzielnie go skompilowałeś i wykonałeś niezbędne poprawki.

Możesz nie potrzebować robić tego wszystkiego, jeżeli masz podobną ideę
robienia archiwzacji systemu. Jeśli nie, a przewidujesz potrzebę
reinstalacji swojego systemu, wtedy możliwość szybkiej rekompilacji i
reinstalacji systemu ze źródeł może być przydatna.

W każdym razie, pomyśl o tym. ;-)


