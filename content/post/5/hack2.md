---
title: Tworzenie jeszcze innego *hack* skryptu
date: 2022-02-09
---

* Autor: John M. Fisk
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.nov.html

---

```
Date: Mon, 16 Oct 1995 19:47:49 CDT  
From: Judith Elaine \<blondie@cybertia.org.tenn.com\>  
To: \<fisk@web.tenn.com\>  
Subject: linux program date  
  

Drogi John'ie,

Siedzę sobie tutaj, przeglądając sierpniowe i wrześniowe kopie Linux
Gazette, razem ze ściągniętym softem -- 8 mega "starowanego" i
skompresowanego pliku.

Wzięłam sobie do serca ostatniej nocy Twoje sugestie dotyczące pisania
skryptu do archiwizacji i napisałam nie tylko skrypt jak Twój "hack",
który starannie archiwizuje oryginalne pliki, ale również program
zapisujący moje modyfikacje.

    ------------------------------- CUT HERE ------------------------------
    #!/bin/sh
    #
    # /usr/local/bin/savemod  -- in the spirit of safehack
    #
    # Copyright (c) 1995 Judith Elaine
    #
    #
    DIR=/usr/local/modifications
    \cp $1 $DIR/$1.`date +%d%h%y`
    echo " " >> $DIR/$1.`date +%d%h%y`
    echo "#>> "$PWD/$1" copied over on "`date` >> $DIR/$1.`date +%d%h%y`
    echo Made a backup of $1.
    ------------------------------- CUT HERE ------------------------------

Zauważ że używam programu 'date' i substytut komendy wywołującej, lub
coś w tym rodzaju. Jestem przekonana, że spędzając czas z tym i
czytając manuale zrozumiesz ważność daty.

Używam jej tutaj w kilku interesujących (przynajmniej dla mnie -- jak
dziele wszystkie moje dane analizowane w pracy) celach. Po pierwsze,
kopiuje plik który chcę zapisać w "bezpiecznym" katalogu nadając mu tą
samą nazwę PLUS datę kopiowania. W chwili wyszukiwania czasu ostatniej
modyfikcaji za pomocą ls, nie mogę archiwizować wielu wersji pliku,
chyba że dam im unikalne nazwy. Skrypt robi to automatycznie.
(podejrzewam, że jeżeli zmieniasz pliki wiele razy w ciągu dnia,
będziesz pewnie musiał dodać godziny i minuty) Zatem, wywołanie

    # cp h h.`date +%d%h%y`

kopiuje plik h na taki sam z dziesiejszą datą na końcu:

    # ls h*
    h          h.16Oct95

Po drugie używam daty do dodania komentarza na samym końcu pliku, w
którym notuję ścieżkę do oryginalnego pliku ($PWD/$1 może się zmieniać,
ale cały czas ma sens) oraz równocześnie datę backap'u. Przypuszczalnie
mogłabym dodawać linię, do której byłaby dołączona nazwa pliku w liście
zarchiwizowanych plików, tak jak sugerowałeś w wydaniu sierpniowym.

Hmmmm.

Jeszcze o połowie drogi w 8 megach...

Jeszcze raz dziękuje za DOSKONAŁE źródła\!

Narazie,

[mailto:%20blondie@cybertia.org.tenn.com](mailto:%20blondie@cybertia.org.tenn.com)
```

