---
title: Więcej zabawy z Setterm
date: 2022-02-09
---

* Autor: Gary Jaffe
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.nov.html

---

Date: Sun, 05 Nov 1995 21:05:00 CST
Sender: \<gary@shadow.net\>
From: Gary Jaffe \<gjaffe@shadow.net\
To: \<fiskjm@ctrvax.Vanderbilt.Edu\>
Subject: sugestia do setterm

John --


Kocham czytać Twoje wskazówki nt. Linuksa. Szczególnie ucieszył mnie
artykuł o setterm. Sugerowałeś dodanie następującej linii do
`~/.bash\_profile`

```
case "$V_TERMINAL" in
        "/dev/tty1") setterm -background black -foreground white -store;;
        "/dev/tty2") setterm -background black -foreground white -store;;
        "/dev/tty3") setterm -background black -foreground white -store;;
        "/dev/tty4") setterm -background black -foreground white -store;;
        "/dev/tty5") setterm -background black -foreground white -store;;
        "/dev/tty6") setterm -background black -foreground white -store;;
esac
```

Zamiast tego możesz spróbować dodać następujący wpis do pliku
/etc/rc.d/rc.local.

```
setterm -background black -foreground white -store >/dev/tty1
setterm -background black -foreground white -store >/dev/tty2
setterm -background black -foreground white -store >/dev/tty3
setterm -background black -foreground white -store >/dev/tty4
setterm -background black -foreground white -store >/dev/tty5
setterm -background black -foreground white -store >/dev/tty6
```

Ma to przewagę tego typu, że kod nie musi się wykonywać za każdym razem
jak się logujesz. Jeżeli ustawiasz kolor tła na jakikolwiek inny niż
czarny, możesz potrzebować poza tym wyczyścić ekran
następująco:

```
clear >/dev/tty1
```

[mailto:%20gjaffe@shadow.net](mailto:%20gjaffe@shadow.net)

## I jeszcze jedno przemyślenie...

Jest to fajna rzecz w Linuksie...zazwyczaj jest kilka sposobów na
doprowadzenie czegoś do działania i jest fajna zabawa z grzebaniem w
nim\! Pomysł Garego nie tylko pozwala Ci na ustawienie koloru każdego
terminala, ale umożliwia ustawienie tych rzeczy bez konieczności
wcześniejszego zalogowania!

Bardzo Fajoskie!

Poza tym, po trochę większych zabawach z **setterm** odkryłem, że jeżeli
zawrzesz opcję "-bold" uwydatnisz kolor czcionki. Innymi słowy,
aktualnie mam ustawiony VT1 na żółty tekst i niebieskie tło. Jeżeli
wpiszesz po prostu:

```
setterm -foreground yellow -background blue -store
```

"żółty" jest aktualnie raczej brązowy :-( Ażeby rozjaśnić go trochę,
spróbuj dodać:

```
setterm -foreground yellow -bold -background blue -store
```

i będzie wyglądać dużo lepiej! :-)

Weź go w obroty!

