---
title: Prawie, ale nie całkiem Linux...
date: 2021-10-15
---

* Autor: Ben Okopnik
* Tłumaczył: Marcin Niedziela
* Original text: https://linuxgazette.net/181/okopnik.html

---

Prawie, ale nie całkiem zupełnie związane z Linuksem zdjęcia z naszych ulic.
Dzięki i proszę [o więcej](mailto:editor@linuxgazette.net)!

---

![Eh, darmowe paliwo? Na prawdę? Niech żyje Francja!](/post/181/misc/gpl_in_france_-_haute_savoie_province.jpg)  
(Wysłane przez Antona Borisova)

"Tak, jest wolne. Nie, nie jest [za darmo](http://en.wikipedia.org/wiki/Gratis_versus_Libre).
Tak, wciąż pracujemy zarówno nad masową replikacją jak i dostawą WiFi "prosto do zbiornika"...

---

![Linux - Jedzenie za myśli](/post/181/misc/yum.jpg)  (Wysłane przez Andersona Silva)

---

`yum install ocwoce orzechy śmietana ryba wołowina kawa squid java ocet ...`

---

"Nie przejmuj się mną - tylko uzupełniam półki z delikatesami."

![](/post/181/misc/unisys.jpg) "Lata temu byliśmy wielcy, ale teraz zmniejszamy sie 
jak wszyscy inni..."


