---
title: UniForum'97, 12-14 marzec
date: 2022-02-09
---

* Autor: Marjorie L. Richardson
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue16/uniforum.html

---

[Album zdjęć UniForum](http://www.linuxgazette.com/issue16/photos.html)


Moja podróż do San Francisco, mająca na celu odwiedzenie UniForum'97
była bardzo satysfakcjonująca, ponieważ zobaczyłam dwie doskonałe
gwiazdy -- kometę Hale-Boppa i Linusa Torvaldsa. Kometa Hale-Boppa była
widoczna na niebie 12 i 13 marca. Linus widoczny był na przemówieniu 13
marca i na pewno był jaśniejszy od komety.

Prezydent UniForum, Tom Mace, przywitał Linusa, a Douglas Michaels z SCO
wręczył Linusowi nagrodę UniForum za osiągniecia. Sama nagroda jest
piramidą, o której Linus powiedział, że miło było zabaczyć coś
"fizycznego" za swoją pracę. Jak zwykle podziękowania Linusa były
zwięzłe i skromne. Określił siebie jako "pająka w centrum pajęczyny" z
wieloma innymi pracującymi wokół niego. Tove i ich 3 miesięczna
córeczka, Patricia Miranda, towarzyszące Linusowi ztolerowali moje
wepchnięcie się do zdjęcia. Następnie Linus i Tove obeszli halę
wystawową, odwiedzając wszystkich swoich fanów w Pawilonie Linuksa.
Tove zwierzyła się, że była zadowolona z pogody (bez śniegu), ale
dostawa jej mebli była opoźniona z powodu burzy dlatego wróciły
spowrotem do Niemiec. (?)

Mitchell Kertzman z Sybase wygłosił rankiem przemówienie, w którym
zignorował Linuksa jako możliwy czynnik w powaleniu na kolana
Microsoftu. Być może nie słyszał on, że celem Linusa nie jest "światowa
dominacja". Kertzman porównał dzisiejszy przemysł oprogramowania do
przemysłu samochodowego w latach pięćdziesiątych -- jest to
projektowanie produktów, które będą przestarzałe za 3 lata, podczas gdy
klienci oczekują długoterminowej pewności. Dla mnie wygląda to tak, że
konsumenci szukają Linuksa.

Podczas, gdy 7000 ludzi dokonało rezerwacji na UniForum, tylko około 75%
z nich rzeczywiście forum odwiedziło. Możliwe, że poszli na jedną z
wpółzawodniczących wystaw np. na Internet World. W każdym razie,
czasami było tłoczno, podczas gdy innym razem (szczególnie w końcówce
dnia) wystawa świeciła pustkami. Pawilon Linuksa (Linux Pavillion)
zlokalizowany był w prawym tylnym rogu wystawy, jednak wyglądało mi na
to, że większość odwiedzających krążyło wokół niego, żeby sprawdzić ten
"nowobogacki" system operacyjny, który odważa się być darmowy. SSC
rozdawał swoje ulotki i naklejki na zderzaki samochodowe, jak również
koszulki z logo, informatory i nowe kubki z pingwinem. IBM i Lucent
Technologies mieli swoje punkty na środku wystawy, ale zauważyłam, że
wielu ludzi omijało ich, odwiedzając Digitala, żeby sprawdzić Alphe i
nowego Linuksa na Intelowską platformę Digitala Johna "maddoga" Halla.
John pisze dla nas krótki artykuł o tej platformie, który ukaże się w
przyszłym miesiącu.

Wysłuchałem dwóch odczytów: jeden o Electronic Document Interchange i
jeden o szybkim dostępie do Internetu. Oba zostały dobrze przedstawione
i były pełne przydatnych informacji. Szczególnie wywarły na mnie
wrażenie pomysły Jeffa Wilbura o kierunkach dostępu do Internetu, które
będą dostępne w przyszłości (np. modemy kablowe, xDSL, satelity, ISDN),
a poza tym poprosiłam go o artykuł.

Jako, że UniForm'97 było moją pierwszą konferencją w roli Redaktora
Linux Journal, spotkałam wielu ludzi, o których wcześniej tylko
słyszałam, jak Joela Goldberga z InfoMagic (sponsorującym Linux
Gazette), Marka Bolzerna z WGS, Adama Richtera z Yggdrasil i oczywiście
Johna "maddog" Halla z Digitala. John przedstawił mnie Tedowi Cookowi z
BRU, który opowiedział mi o jego planach oddania oprogramowania Bru
Grupie Użytkowników Linuksa w nadzchodzącym Linux Expo i grupom, które
są członkami [G.L.U.E.](http://www.ssc.com/glue/).

W środowy wieczór, Joanne Wagner, jedna z naszych reprezentantów
ogłoszeniowych oraz ja, uczestniczyłyśmy w konferencji/imprezie
prasowej zwołanej przez XiGraphics--darmowe żarcie i napitki, zawsze
mile widziane. Konferencję prasową zwołano w celu poinformowania o
ostaniej zmianie nazwy (z X Inside) i najnowszym wydaniu oprogramowania
Xi's Accelerated X. Prezes i założyciel firmy, Thomas Roell, przedstawił
krótką prezentację, w której opisał kierunki wizji dla Xi Graphics.

W sumie miło spędziłam czas i na konferencji i w San Francisco.
