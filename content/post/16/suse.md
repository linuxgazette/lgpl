---
title: Instalacja dystrybucji SuSE
date: 2022-02-09
---

* Autor: Larry Ayers
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue16/suse.html

---

## Wprowadzenie

Dystrybucja Linuksa SuSE powstała kilka lat temu jako adaptacja
Slackware. Patrick Volkerding ze Slackware najpierw pomagał twórcom
SuSE, ale wkrótce zaczął sam rozwijać tą dystrybucję. Kilka nowych
właściwości miało na celu pomóc początkującym użytkownikom zwiększenie
prawdopodobieństwa udanej instalacji. Wiedząc o przenikaniu się rzeczy w
świecie wolnego oprogramowania, nie byłbym zaskoczony, gdybym dowiedział
się, że niektóre z tych właściwości pojawiły się w najnowszych wydaniach
Slackware.


## Początek instalacji

Kiedy uruchamiasz swój komputer z dysku instalacyjnego, tak naprawdę
uruchamiasz miniaturowy system Linuks specjalnie przygotowany do tego
celu. Pokazuje się kolorowy ekran, za pomocą którego, odpowiadając na
serię pytań, przejdziesz przez proces instalacji.

YAST (Yet Another Set-up Tool - jeszcze jedno narzędzie do setupu)
wygląda jak w Slackware, jako że używa programu Dialog; narzędzie to
włącza skrypt shella do przedstawienia listy pytań i przycisków
wyboru, które pozwalają użytkownikowi na odpowiednie wybory i przejście
przez proces instalacyjny.

Podczas gdy nie ma dystrybucji gwarantującej bezbolesną instalację,
twórcy z GmbH zdołali przewidzieć kilka problemów na jakie mogą
napotkać użytkownicy Linuksa. Jednym z bardziej frustrujących problemów
jest nierozpoznawanie napędu CD-ROM. Kopiowanie pakietów potrzebnych do
instalacji na dysk twardy i instalowanie właśnie z niego jest jakimś
rozwiązaniem, ale jest niewygodne i czasochłonne.

Zamiast dostarczania kilku obrazów dysku, z których jeden prawdopodobnie
obsługuje twój CD-ROM, pojedynczy dysk startowy SuSE zawiera małe,
podstawowe jądro ze wszystkimi sterownikami, dostępnymi w razie potrzeby
jako moduły. Daemon jądra jest procesem tła, co daje pewność że
określony moduł będzie w razie potrzeby załadowany. Pomaga to ominąć
jedną z przeszkód.

Inną powszechną pułapką jest zlekceważenie potrzebnej przestrzeni
dyskowej, w przypadku której instalacja jest samoczynnie przerywana z
powodu braku miejsca. Kiedy to się zdąży, decydujący końcowy krok (jak
np. instalacja LILO) nie jest kończony i start jest uniemożliwiony.
Instalacja bazująca na skrypcie jest z natury sekwencyjna; przeskoczenie
jednego kroku może nie spowodować żadnych perturbacji, ale każda
ewentualność w skrypcie jest ciężka do przewidzenia i jeżeli coś
zostanie opacznie zrozumiane skrypt z reguły przerywa działanie.

W trakcie instalacji SuSE, pozostałe miejsce na partycji jest
wyświetlane na ekranie YAST; podczas wyboru pakietów, możesz próbować
różnych kombinacji cały czas widząc jak dużo miejsca zostało jeszcze na
dysku.

Partycjonowanie i formatowanie dysku jak i tworzenie i włączanie
partycji wymiany są procesami, które nie różnią się
zbytnio od innych dystrybucji. Wszystkie używają tych samych narzędzi do
wykonania tych zadań; procedura przebiega w bardziej lub mniej
standardowy sposób.

Użycie "zależności", które zawierają informacje zawarte w pakietach
oprogramowania dotyczące konieczności instalacji innych pakietów,
rozszerza się na wiele dystrybucji Linuksa. Niestety nie ma
uniwersalnego formatu dla zależności, każda z dystrybucji używa jeszcze
różnych formatów. Format RPM Red Hata, używany w kilku dystrybucjach,
jest potężny i efektywny, ale ma też kilka minusów. Najlepiej działa na
wszystkich systemach "rpmowych", tak więc zależności sprawdzane przez
program RPM są znane tylko dla pakietów RPM. SuSE używa adaptowanego z
Slackware formatu \*.tar.gz, który ma zaletę elastyczności. Zależności
są sprawdzane tylko wtedy, gdy pakiety instalowane są z programu YAST,
zezwalając opcjonalnie (dla zaawansowanych użytkowników) rozpakować
pakiet z innej lokacji, a następnie sprawdzić pliki i konfigurację przed
końcową instalacją. Zależności są bardziej użyteczne podczas wstępnego
ustawiania i w trakcie nowych instalacji. Gdy przez chwilę używałeś
systemu, będziesz miał pomysły jakie biblioteki i programy są dostępne.
Wiele pakietów dla Linuksa zawiera poza tym informacje dotyczące co
potrzebne jest w systemie aby pakiet funkcjonował prawidłowo. Mądrze
jest przeczytać plik rc.config przed uruchomieniem SuSEconfig i
przekazać mu każde zmiany jakie możesz wykonać. Niektóre domyślne akcje
skryptu wybiorą opcje za ciebie, ale w prosty sposób można to wyłączyć
przez edycje pliku.

Użytkownicy obeznani z wyglądem plików startowych Slackware, będą
potrzebowali wykonać kilka regulacji; pliki zazwyczaj znajdujące się w
/etc/rc.d są zamiast tego w /sbin/init.d.


## Po instalacji

YAST jest poza tym używany do rutynowego zarządzania systemem po
instalacji. Mnogość plików koniecznych Linuksowi na uruchomienie i
pracę, może przyprawić o zawrót głowy początkujących użytkowników. YAST
oferuje interfejs menu dla tych plików, włączając w to plik
konfiguracyjny sendmaila, pliki crona (harmonogram), skrypty startowe i
różnorodne pliki do sieci. Zmiany wykonane przez YAST są zapisywane w
pojedynczym pliku rc.config, w katalogu /etc, który może być poza tym
ręcznie edytowany. Te zmiany są następnie zapisywane do różnych
"prawdziwych" plików konfiguracyjnych poprzez skrypt pod nazwą
SuSEconfig. Skrypt ten jest automatycznie uruchamiany przez YAST na
zakończenie sesji YAST; jeżeli /etc/rc.config jest edytowany
bezpośrednio, SuSEconfig musi być uruchomiony ręcznie. Wygląda to na
skomplikowaną procedurę, ale jest o wiele prostsze niż ręczne zmiany w
pojedynczych plikach, dociekanie prawidłowej składni potrzebnej przy ich
edycji i zmienianie rzeczywiście tego co chcemy.

Kiedy już uruchomisz SuSE, dobrym pomysłem jest zainstalowanie źródeł
jądra Linuksa (dostępny na płycie CD-ROM, jako opcjonalny pakiet, który
może być zainstalowany podczas wstępnego setupu). SuSE instaluje ogólne
jądro i możesz prawdopodobnie potrzebować tylko kilku z towarzyszących
modułów. Jest to doskonała okazja do zapoznania się z mechanizmem
kompilowania programu źródłowego, a skończysz na małym dostrajaniu jądra
tylko z tymi rzeczami, które chcesz.

Kompilator GCC wraz z towarzyszącymi mu narzędziami musi być
zainstalowany w przypadku kompilowania jądra; narzędzia te są potrzebne
w systemie Linuks nawet wtedy, gdy nie jesteś programistą. YAST
sprawdzając zależności testuje czy wszystkie wymagane narzędzia
kompilacji są zainstalowane.

Dla początkującego użytkownika, kompilacja jądra może wydawać się
skomplikowana, ale jest to dość intuicyjny proces. Dostępne są trzy
interfejsy do początkowego kroku konfiguracji. Pierwszy (i najstarszy)
jest skryptem konsolowym wywoływanym przez polecenie `make config`.
Skrypt ten zadaje serie pytań a odpowiedzi zapisuje do pliku, który jest
później używany przez kompilator. Musisz wiedzieć kilka rzeczy o swoim
sprzęcie, takich jak typ dysku twardego czy CD-ROMu. Jeżeli chcesz mieć
obsługę dźwięku, będziesz musiał znać IRQ używany przez kartę dźwiękową,
jak również kilka innych parametrów, które uzyskasz z instrukcji karty
lub jako wynik polecenia DOS `msd`.

Dwa następne interfejsy to menuconfig i xconfig. Pierwszy używa
zmodyfikowanej wersji programu Dialog wspomnianego wyżej, który
uruchamiany jest w konsoli lub xtermie a przypomina on narzędzie
konfiguracji YAST. Xconfig to wersja oparta na Tk, stworzona do
działania w sesji X Window. Wszystkie trzy wykonują te same zadania;
dwa ostatnie pozwalają ci na wybór bez zbyt wielu wprowadzeń. Źródła
jądra są dobrze udokumentowane. Plik README w katalogu głównym źródeł
zawiera wystarczające informacje do prawidłowej budowy jądra.

## Przygotowanie do uruchomienia Xów

Prawidłowa konfiguracja Systemu X Window (dokładniej XFree86, który jest
zawarty w SuSE i wielu innych dystrybucjach Linuksa) może być przeszkodą
nie do pokonania. Jest wiele różnych monitorów i kart graficznych, tak
więc każda instalacja X musi być indywidualna. Trudność ta została nieco
złagodzona z wydaniem XFree86 3.2, który jest zawarty w najnowszych
wydaniach SuSE. Narzędzie konfiguracyjne oparte na Dialogu może być
teraz używane w miejsce poprzedniego xf86config. Oba bazują na skryptach
shella podobnie do tych używanych przy konfiguracji jądra Linuksa.
Niemniej jednak, w dalszym ciągu powinieneś znać odświeżanie poziome i
pionowe swojego monitora, jak również typ chipsetu zainstalowanego w
twojej karcie graficznej. Pomocne może być rozpoczęcie ustawień w
niskich wartościach, np. najpierw sprawdź funkcjonalność Xów w niskich
rozdzielczościach zanim przystąpisz do ustawiania pełnych możliwości
swojej karty graficznej.

Twórcy SuSE wzięli pod uwagę kilka bolączek w konfiguracji różnych
menedżerów okien. Gdy pierwszy raz uruchamiasz Xy, wiele aplikacji,
które wybrałeś do zainstalowania będzie dostępnych z menu okna
głównego. Inne opcje menu pozwalają na zmianę tła okna. Dostarczonych
jest wiele dobrze zaprojektowanych ikon. Daje to nowym użytkownikom
lekkie poczucie ulgi. Po uruchomieniu Linuksa i Xów, jest tam
wystarczająco dużo rzeczy, żeby poznać jak pracuje system bez potrzeby
dostrajania środowiska w celu stworzenia tolerowanego wyglądu\!


## Późniejsza aktualizacja

Chwilę po zakończeniu instalacji, nawet większość najnowszych dostępnych
dystrybucji zaczyna się starzeć. Jest to powolny proces, ale będziesz
ewentualnie czuł potrzebę aktualizacji niektórych części systemu.
Niektóre dystrybucje najlepiej pracują, gdy używany jest ich własny
format aktualizacji plików, ale SuSE świetnie działa używając
standardowego formatu \*.tar.gz. W tej materii SuSE podąża za Slackware.
Najważniejsze pakiety będą kompilowane i instalowane ze spakowanego 
formatu używanego przez większość twórców. Jest kilka
pakietów nie mieszczących się w tym schemacie; w tym przypadku może być
użyty RPM. Nie będzie działać sprawdzanie zależności, ale RPM umożliwi
ci wykorzystanie wiedzy bardziej zaawansowanych użytkowników i ich
umiejętności konfiguracji.

