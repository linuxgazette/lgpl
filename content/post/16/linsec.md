---
title: Więcej o bezpieczeństwie Linuksa
date: 2022-02-09
---

* Autor: Andrew Berkheimer
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue16/security.html

---

Macie tutaj jeszcze jeden artykuł o bezpieczeństwie w Linuksie. Trochę
nowości do zabawy dla wszystkich, wzmocnione niektóre kluczowe punkty i
wyjaśnienie niektórych rzeczy, które były trochę przeinaczone w
poprzednim artykule. Zauważcie, że jest on przeznaczony dla troszkę
mniej doświadczonej publiki, bardziej doświadczeni użytkownicy
prawdopodobnie będą się nudzić w tym czasie. Tak więc twój system wstał
i pracuje, jest podłączony do Internetu, możliwe że działa na nim serwer
FTP lub jakiś inny serwis. Ale słyszałeś wstrętne opowieści ludzi,
których komputery hackowano bez widocznego powodu, dlatego jesteś
lekko poddenerwowany. Chcesz zabezpieczyć swój system przed intruzami z
zewnątrz, ale gdzie zacząć? W przeciwieństwie do popularnego
przekonania, zabezpieczenie swojego systemu może być w rzeczywistości
proste i pouczające. Więc nadszedł czas rozpoczęcia\!

Przede wszystkim, dowiaduj się\! Jay wspominał o czytaniu CERT, ale
twierdziłbym, że to nie wystarczy. CERT nie publikuje informacji dopóki
nie zweryfikuje, że jest to problem i większość znanych dystrybutorów
dostarczy łatek rozwiązujących problem. Często może to trwać miesiącami
zanim odkryta dziura będzie zakomunikowana przez CERT. Jest kilka
dobrych list pocztowych takich jak bugtraq, linux-security, czy
linux-alert, które polecam subskrybować (informacje o subsrybcji
znajdują się na końcu tego artykułu), gdzie o dziurach w
bezpieczeństwie systemu często się dyskutuje i znajduje się je na długi
czas przed tym zanim CERT zacznie o tym rozmawiać - Crakerzy znają te
miejsca, tak więc ty też powinieneś je znać.

Teraz coś o prawdziwych zabezpieczeniach. Pierwsza sprawa to samodzielne
zabezpieczenie się przed atakami nieznanych osób, które mogą natknąć się
na twój system i testować swoje umiejętności włamywacza. Jedna z
pierwszych rzeczy do sprawdzenia, to nieużywane a uruchomione deamony w
twoim systemie. Naprawdę brak jest przyczyny pracującego nfsd, jeżeli
nie udostępniasz nikomu NFS. Są dwa miejsca które musisz sprawdzić: plik
konfiguracyjny super serwera inet (zazwyczaj /etc/inetd.conf) i skrypty
startowe systemu (znajdujące się w /etc/rc.d, /etc/rc2.d, lub podobnym
katalogu).

W pliku inetd.conf zakomentuj znakiem hash (\#) linie z usługami,
których tak naprawdę nie potrzebujesz udostępniać... usługi zaczynające
się na r\* (rlogin, rshd, rexecd itp.) do dobre kandydatury, tak samo
jak inne typowo nieużywane rzeczy takie jak echo, daytime, czy chargen.
Dla większości ludzi, pozostawienie telnetd, ftpd i może pop3d powinno
wystarczyć w tym momencie. Może fingerd też powinien zostać, choć
uważaj, finger może podać wiele informacji o twoim komputerze, które
mogą być użyteczne dla potencjalnego crackera. Kiedy skończysz
edytowanie inetd.conf zrestartuj inetd poleceniem "killall -HUP inetd",
aby inetd mógł odświeżyć konfiguracje.

W twoich skryptach startowych możesz zobaczyć odwołania do takich rzeczy
jak portmap, ypserv, rpc.mount czy rpc.nfsd. Jeśli nie masz serwerów NFS
lub NIS, nie potrzebujesz tych usług i nie powinny być one
uruchomione... w wielu wypadkach wersje tych programów mają pewne
złośliwe podatności na bezpieczeństwo. Poza tym zainteresuj się
sendmailem (jeżeli nie odbierasz bezpośrednio poczty, nie potrzebujesz
go mieć uruchomionego) i httpd (potrzebny tylko wtedy gdy masz własny
serwer www).

Tak więc wykonałeś ciężką pracę przy wyłączaniu wszystkich
niepotrzebnych serwerów... czas na dodanie/aktualizację oprogramowania.
Przede wszystkim, upewnij się, że masz uruchomioną najnowszą wersję
NetKit, który zawiera większość typowych dla Linuksa serwerów
sieciowych, takich jak telnetd, fingerd itp. Jego aktualna wersja to
0.09 i jest dostępna na
ftp://ftp.uk.linux.org/pub/linux/Networking/base. W najnowszej wersji
usunięto kilka znanych dziur w bezpieczeństwie wykrytych w poprzednich
wersji.

Generalnie, musisz sprawdzać i łatać wszystko co jest uruchomione:
sprawdź http://www.sendmail.org, czy nie ma aktualizacji sendmaila (w
dzisiejszych czasach nowa wersja może się pokazać w każdym momencie i
prawie zawsze usuwa problemy z bezpieczeństwem), http://www.apache.org
do aktualizacji serwera Apache itp.

Niemniej jednak, cały czas pozostaje problem ze snifferami haseł
przechwytującymi twoje hasło, gdy łączysz się przez telnet z sieci
zewnętrznej. Telnet, FTP, POP i kilka innych standardowych protokołów
używa czystego tekstu do transmisji twojego hasła. Jest kilka sposobów
na ominięcie tego wykorzystując zewnętrzne oprogramowanie. Opiszę tu
OPIE i ssh.

Po pierwsze OPIE, znany też jako One-time Passwords in Everything
("jednorazowe hasła we wszystkim" - przyp. tłum.). Pakiet stworzony
przez Labolatorium Marynarki Wojennej USA (US Naval Research Labs) i
aktualnie zarządzane przez The Inner Net. Pomysłem w jednorazowych
hasłach jest to, że logując się do systemu z zewnątrz, system przywita
cię ekranem takim jak ten poniższy:

``` 
stroke login: andy
otp-md5 271 st6747
Response:
```

Zamiast wprowadzenia swojego hasła w trakcie połączenia, uruchamiasz
program do generacji klucza na swojej lokalnej maszynie z parametrami
podanymi na ekranie logowania (w tym przypadku będące otp-md5 271 st6747).
Swoje hasło wprowadzasz do lokalnego programu (gdzie nie może być
podsłuchane), a generator klucza tworzy unikalne hasło, z którym się
logujesz. Te unikalne, jednorazowe hasło będzie działało tylko jeden
raz, tak więc nawet jeżeli ktoś je przechwyci programem sniffującym, nic
mu to nie da. Pakiet OPIE dostępny jest na:
ftp://ftp.nrl.navy.mil/pub/security/opie/.

Jest też inny popularny pakiet - ssh. Pakiet ssh zastępuje te diabelskie
programy rlogin, rexecd, rshd, itp. na sshd, który ma tą samą
funkcjonalność, ale szyfruje całą komunikację, powodując bardzo ciężkim
do odczytania przez programy sniffujące. Więcej informacji na temat tego
pakietu można znaleźć na http://www.cs.hut.fi/ssh/.

Ponadto jest wiele innych bardziej zawiłych i skomplikowanych metod
przeznaczonych do rozległych sieci, laboratoriów itp. (takie jak
Kerberos V itp.).

To było zabezpieczenie się przed zewnętrznymi crackerami, ale cały czas
musisz martwić się o innych użytkowników w twoim systemie (lub nawet o
zewnętrznych crackerów, jeżeli mają dostęp do shella w twoim systemie).
Zapewne usłyszysz wiele o dziurach typu "buffer overflow" (przepełnienie
bufora - przyp. tłum.). Jest to określony czas, gdy binarka nie sprawdza
czy jej dane są wkładane do odpowiedniego bufora znakowego w pamięci.
Odpowiednio napisany program może wykorzystać to i nadpisać inną część
pamięci, co skutkuje tym, że jest uruchamiany inny program. Normalnie
nie jest to problemem dopóty, dopóki nie ustawisz na programie bitu
setuid roota. Gdy program z bitem setiud roota będzie uruchomiony z
prawami roota, wtedy każdy program uruchomiony przez niego też będzie
pracował na prawach roota. Tak więc, jeżeli przepełnienie bufora jest
używane do uruchamiania /bin/sh jako root, wtedy jakiś koleś nagle
dostaje shella roota i robi to o co poprosi.

Istnieją poza tym programy, które coś robią, a które mogą być użyte
przez crackera, jeżeli zdarzy się, że pracują na koncie roota. Poprzez
małe oszustwo, może być bardzo dobrą możliwością dla niego z otrzymaniem
shella roota. Podstawowa sprawa: ustawianie bitu setuid roota na
programy nie jest najlepszym rozwiązaniem, zmniejsz do niezbędnego
minimum liczbę bitu setuid roota w swoim systemie.

Nie możesz zbyt dużo zrobić przed samodzielnym zabezpieczeniem się przed
przepełnieniem bufora, ale trzymaj rękę na pulsie sprawdzając informacje
o nowych dziurach w bezpieczeństwie i od razu je łataj. Jeżeli masz
jakieś doświadczenie programistyczne, prawdopodobnie zechcesz zajrzeć do
rzeczywistego kodu źródłowego i samemu sprawdzić możliwość przepełnienia
bufora: możesz po prostu znaleźć coś o czym nikt jeszcze nie wie.

Poza tym ważny punkt: bardzo rzadko powinieneś ufać binariom, które
pobierasz z nieznanych, niezaufanych źródeł, szczególnie jeżeli
zamierzasz uruchamiać je jako root. Tak rozprzestrzenia się "wirus"
Bliss, związany z przepełnieniem bufora w niektórych powszechnych grach
z setuid roota. W każdym systemie Unix, root jest wszechmocnym
użytkownikiem, więc o ile normalne wirusy nie mogą istnieć w Unixie,
ponieważ zwykli użytkownicy zasadniczo nie mogą modyfikować programów
systemowych, to program taki jak Bliss został zaprojektowany do próby
znanych przepełnień bufora w celu otrzymania przywilejów użytkownika
root i możliwości modyfikacji programów, których właścicielem jest root.

Jako ostatnie przypomnienie, jest kilka punktów, w których nie mogę
pomóc ale mogę podeprzeć. Jeżeli myślisz, że zostałeś zaatakowany,
natychmiast odłącz się od sieci, przeanalizuj swoje logi, zamień każdy z
programów, który podejrzewasz o "zhackowanie", a nawet przeinstaluj swój
system (po zarchiwizowaniu wszystkich ważnych danych). I zawsze pamiętaj
o tworzeniu trudnych do odgadnięcia haseł, a także o regularnej ich
zmianie. Oprócz tego, nie muszę przypominać o tym, żebyś TRZYMAŁ RĘKĘ NA
PULSIE. Jest wiele dobrych książek o tematyce bezpieczeństwa
komputerowego, szczególnie polecałbym \_Computer Security Basics\_
wydawnictwa O'Reilly and Associates tym, którzy są początkujący w
kwestii bezpieczeństwa. Zapiszcie się na jakieś bardziej popularne listy
pocztowe traktujące o bezpieczeństwie. Istnieje poza tym Linux Security
FAQ dostępny on-line na http://www.aoy.net/Linux/Security/, który jest
dobrym źródłem informacji. Ostatnia rada: nigdy nie myśl, że twój system
jest "perfekcyjnie zabezpieczony" - tym sposobem zapraszasz do
włamywania się.

Aha, co do tych list pocztowych, o których wcześniej wspominałem.
Informacje o linux-alert i linux-security można znaleźć, jak
wspominałem, na stronie WWW Linux Security
(http://www.aoy.net/Linux/Security/). Informacje o bugtraq można znaleźć
na http://www.geek-girl.com/bugtraq/index.html. Jest poza tym wiele
innych tematów, które traktują o bezpieczeństwie, konfiguracji firewalli
i innych pakietach filtrujących, zabezpieczaniu IP spoofing i wiele
więcej fajnych informacji o kontroli dostępu do usług sieciowych, a
także wiele innych miejsc, ale to już temat na inną dyskusję.


**Podsumowanie źródeł wykorzystanych w artykule**

* netkit: ftp://ftp.uk.linux.org/pub/linux/Networking/base/
* sendmail: http://www.sendmail.org/
* apache: http://www.apache.org/
* opie: ftp://ftp.nrl.navy.mil/pub/security/opie/
* ssh: http://www.cs.hut.fi/ssh/
* linux security www: http://www.aoy.net/Linux/Security/
* info o liście linux-alert : http://www.aoy.net/Linux/Security/LinuxAlertList.html
* informacje o liście linux-security : http://www.aoy.net/Linux/Security/LinuxSecurityList.html
* informacje o liście bugtraq: http://www.geek-girl.com/bugtraq/

