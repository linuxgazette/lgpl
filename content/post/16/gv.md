---
title: GV - Alternatywa dla Ghostview
date: 2022-02-09
---

* Autor: Larry Ayers
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue16/gv.html

---

Mam wrażenie, że większość użytkowników Linuksa próbowało więcej niż
jednej dystrybucji. Ja próbowałem kilku i po konfiguracji nowych
instalacji dostosowanych do moich potrzeb i wiedzy, przypomniałem sobie,
że Linux to przecież... Linux\! Dystrybucje w łatwy sposób
przeprowadzają instalację i zarządzają pakietami, ale gdy uruchomisz
Linuksa różnice nie są naprawdę widoczne.

To co odkryłem interesującego w dystrybucjach w tych dniach to wybór
pakietów oprogramowania, które się w nich znajdują. Pomyślałbyś, że
wszystkie dystrybucje oferują to samo oprogramowanie; mimo wszystko,
większość darmowych programów z Internetu jest dostępna dla wszystkich
dystrybucji. Jest grupa wspólnych aplikacji, które dostarczają prawie
wszystkie dystrybucje, użyteczne i dobre pakiety takie jak XV, XFree86,
i Ghostscript. Ale jest kilka sprzeczności, kiedy zagłębisz się w
mniejsze, mniej podstawowe, mniej potrzebne pakiety. Każda z
dystrybucji, którą probowałem, zawierała oprogramowanie, które nie było
zawarte w innych dystrybucjach.

Ostatnio używałem dystrybucji Debiana. Podczas instalacji pakietów
natknąłęm się na coś, co nazywało się "GV", a wyglądało na jakiś rodzaj
przeglądarki Postscriptowej. Zainstalowałem go i przeczytałem, że ta
przeglądarka została zaprojektowana na bazie Ghostview, ale jest
prostsza w używaniu. W przeciwieństwie do Ghostview, GV może poza tym
otwierać pliki PDF.

W związku z faktem, że większość monitorów komputerowych jest szersza w
poziomie niż w pionie, nie jest możliwe czytanie naraz standardowych
stron dokumentu i zobaczenie całej długości strony. GV załatwia to
poprzez pokazywanie strony w okienku podglądu z lewej strony strony i
podświetleniu widocznej części. Klikając lewym przyciskiem myszy w
obrębie wyświetlanej strony i przeciągając mysz, łagodnie przewija się
stronę w dół i w górę, podczas gdy miniaturowy schemat w oknie pokazuje
ci gdzie aktualnie znajdujesz się na stronie. Jeżeli twoje okno jest
zbyt wąskie do wyświetlenia całej szerokości dokumentu, możesz również
przewijać ją w prawo i w lewo.

Poniżej znajduje się zrzut ekranu GV wyświetlającego stronę z załączoną
dokumentacją Postscriptu:

![GV Window](/post/16/misc/gv.gif)

Jedną z opcjonalnych możliwości GV (może być przełączany w menu) jest
aliasowanie czcionek. Gdy ta opcja jest włączona, czcionka jest ładnie
wyświetlana.

Ghostview był domyślną przeglądarką plików Postscriptowych. Odkryłem, że
jest niewygodny w używaniu; tak to wygląda, gdy mam dostosowane
powiększenie do wyraźnego druku, okno jest tak duże, że mam kłopot z
nawigacją po całym dokumencie. GV radzi sobie z tym problemem (co ma
znaczenie dla każdego posiadacza monitora mniejszego niż 21"\!) w
intuicyjny sposób.

GV jest dobrym przykładem ruchu w darmowym oprogramowaniu. Kilka lat
temu, Timothy Thieson napisał program Ghostview; był to dobry program w
tamtych czasach, ale jest statyczny w dzisiejszych. Poza tym, pisząc
kawałek darmowego programu nie wymaga się jego nowych wersji i
aktualizacji na zawsze\! Ale kod źródłowy był cały czas dostępny i
Johannes Plass w końcu go adoptował, a GV jest tego rezultatem.
Następnie program zwrócił uwagę Helmuta Geyera, który to stworzył z
niego pakiet dla Debiana, oddając tym samym program nowej grupie
użytkowników. Twórcy oprogramowania nie muszą na nowo wynajdować koła,
ponieważ gdzieś tam istnieje prawdopodobnie kod źródłowy, który umożliwi
stworzenie na jego podstawie dowolnej aplikacji.

## Dostępność GV

Źródła GV można pobrać z [niemieckiego FTP](ftp://iphthf.physik.uni-mainz.de/pub/gv/unix/gv_2_7_b5.tar.gz).
Wierzę że ustawienie widget Xaw3d jest wymagane w celu kompilacji ze
źródeł. Wersję dla Debiana można pobrać z [głównej strony tej dystrybucji](ftp://ftp.debian.org/rex-fixed/binary-i386/text/gv_2.7b5-3.deb)
lub jednego z jej mirrorów.

