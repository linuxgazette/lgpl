---
title: Witaj w Weekendowym Mechaniku Linuksa!
date: 2022-02-09
---

* Autor: Marjorie L. Richardson
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue16/wkndmech.html

---

![](/post/16/misc/wkndMech.gif_)

## Czas zostac... *Weekendowym Mechanikiem Linuksa*!

![](/post/16/misc/mechanic.gif)

Dotrwałeś wreszcie do weekendu, wszystkie sprawy nagle nieco zwolniły tempo.
Zwlekłeś się z łóżka, ogoliłeś się i wziąłeś prysznic, bo dziś sobota, wypiłeś 
potrzebną dawkę kofeiny (twoja ulubiona alkaloid). Czas teraz odpalić 
Linuksa i trochę pokombinować!

## Witaj w kwietniowym Mechaniku Weekendowym!

![](/post/16/misc/attndant.gif)

Hej, właź do środka!

Dzięki za odwiedziny! Co robiliście?

Nie wiem jak wy, ale życie w gospodarstwie domowym Fiska ostatnimi czasy
było troszkę zajęte. Miałem doskonały semestr na [MTSU](http://www.mtsu.edu/)
bawiąc się z moją klasą, która, jak było do przewidzenia, zaczęła coraz
głośniej szaleć. I wszyscy razem zaczynamy się "dostrajać..." :-)

Przepraszam, że artykuły i tym podobne w tej edycji są trochę krótkie i
pośpieszne. Miałem kilka godzin czasu przed wyjazdem do rodziny i
zobaczę co będę mógł napisać. W notatniku miałem pełno pomysłów o tym o
czym chciałbym napisać. Co przypomina mi...

Czy mam wygłosić kazanie na mocy notatnika...

Powiedzcie, *nie...?\!* :-)

Cóż, wszyscy usiądźcie wygodnie, weźcie głęboki oddech i zaczynamy\!

Tak na poważnie, przekonałem się, że trzymanie notatnika, gazety, lub po
prostu notowanie pomysłów, które mi przysyłacie oraz krótkich uwag, jest
jak *szczotkowanie i czesanie: dobre do higieny*. Umysłowej higieny.
Pomaga to zapobiec "Łysemu Programiście", rodzaju wyrywania sobie włosów
z głowy próbując przypomnieć sobie dokładne polecenie jakiegoś
niejasnego narzędzia systemowego lub naprawiając coś w pliku
konfiguracyjnym, w którym zrobiłeś jakąś *maleńką zmianę...*

Posiadając notatki informujące co zrobiłeś w jakimś pliku
konfiguracyjnym; posiadając wydruki na kartce dokumentacji/README
jakichś narzędzi; lub po prostu posiadając polecenie w linii komend,
nabazgrane z tyłu koperty po rachunku telefonicznym i przepisane
następnie do swojego notatnika, może NAPRAWDĘ oszczędzić nerwy pewnego
dnia.

I żebyś nie myślał, że jestem bardziej obsesyjny i zapominalski niż
naprawdę jestem... mam rzeczywiście mały stosik kartek na półce obok
komputera, gdzie mam wszystkie te notatki i bazgroły. Nie są zbyt dobrze
zorganizowane, jest tam sporo niepotrzebnych rzeczy, a niektóre z nich
są totalnie nieczytelne lub niezrozumiałe (prawdopodobnie zapisywane w
momencie wątpliwego przebłysku o 2:00 nad ranem...). Mimo wszystko,
notatki te są wielce przydatne od czasu do czasu, do zerknięcia do nich
teraz i w przyszłości.

Poza tym zauważyłem, że trzymając bardziej lub mniej dokładne notatki o
instalacji (które sporządzałem dość często przez ostatnie kilka lat)
dają bardzo przydatne informacje kiedy siadałem do szkicowania nowej
instalacji. Wypracowałem swój własny schemat partycji, który był
użyteczny dla mnie, zaprojektowałem własny schemat archiwizacji i
aktualizacji itd., bazując na tych notatkach.

Poza tym, jak wspomniałem wyżej, jest naprawdę użyteczne trzymanie
wydruków różnych README, manów itd. Podczas, gdy doceniam
wszechstronność dokumentacji on-line -- info, man, HTML itd., nic nie
pobije posiadania książeczki, którą możesz czytać bez potrzeby czekania,
aż Netscape skończy zjadać całą twoją mapę kolorów, zanim się
załaduje... :-)

*(Wiem, wiem... byłeś tam, zrobiłeś to, dostałeś t-shirt... :-)*

A tak na poważnie, posiadanie wydruku z wszystkimi zapiskami i
zaznaczeniami jest naprawdę przydatne. Jeżeli trzymasz to wszystko w
jakimś rodzaju notatniku, folderze plików lub czymkolwiek innym,
prawdopodobnie oszczędzisz sobie w przyszłości pewnego rodzaju
rozdrażnienia.

Po prostu pomyśl...

Niemniej jednak, właśnie skończyłem. Bez dalszej wrzawy...

**Wciąż z przedstawieniem\!**

Mam nadzieję, że było zabawnie\!

## Więcej pomysłów o tapetach...

Po publikacji w kolumnie WM w lutym, **Irek Koziol** napisał o pomysłach
tapetowania, o których wspomniałem:

> Date: Wed, 12 Feb 1997 15:28:28 -0600  
> From: Irek Koziol \<cft-inc@worldnet.att.net\>  
> Subject: Tapeta X Window  
> 
> Używałem:
> 
> ``` 
>   xv -quit -root -max  image.gif
> ```
> 
> (Jeśli powiększony obraz jest celem do wypełnienia całego ekranu).
> 
> Czy mógłbyś to skomentować i dodać to do LG?
> 
> Pozdrawiam serdecznie, George.

No cóż, zobaczmy co możemy na ten temat powiedzieć...

Po pierwsze, wszędobylski program xv **Johna Bradleya** jest swojego
rodzaju "Scyzorykiem Armii Szwajcarskiej" dóbr graficznych. Posiada on,
jak wszystkie dobre programy UNIXa, dziwne opcje linii poleceń, z
którymi zapoznanie się, może zająć sporo czasu. Na szczęście, dla tych
z was którzy nie dysponują czasem na zapoznanie się z nimi, oraz
ograniczając zakres dyskusji, opcje można zawęzić do niezbędnego
minimum.

Spróbuj tak dla zabawy wystartować X i wprowadzić coś takiego:

```sh
xv -help
```

Następnie się cofnij...

Kiedy to robisz, xv wypluwa coś takiego:

    Usage:
    xv [-] [-/+24] [-/+2xlimit] [-/+4x3] [-/+8] [-/+acrop] [-aspect w:h] [-best24]
       [-bg color] [-black color] [-bw width] [-/+cecmap] [-cegeometry geom]
       [-/+cemap] [-cgamma rval gval bval] [-cgeometry geom] [-/+clear] [-/+close]
       [-/+cmap] [-cmtgeometry geom] [-/+cmtmap] [-crop x y w h] [-cursor char#]
       [-DEBUG level] [-dir directory] [-display disp] [-/+dither] [-drift dx dy]
       [-expand exp | hexp:vexp] [-fg color] [-/+fixed] [-flist fname]
       [-gamma val] [-geometry geom] [-grabdelay seconds] [-gsdev str]
       [-gsgeom geom] [-gsres int] [-help] [-/+hflip] [-hi color] [-/+hist]
       [-/+hsv] [-icgeometry geom] [-/+iconic] [-igeometry geom] [-/+imap]
       [-/+lbrowse] [-lo color] [-/+loadclear] [-/+max] [-/+maxpect] [-mfn font]
       [-/+mono] [-name str] [-ncols #] [-/+ninstall] [-/+nodecor] [-/+nofreecols]
       [-/+nolimits] [-/+nopos] [-/+noqcheck] [-/+noresetroot] [-/+norm]
       [-/+nostat] [-/+owncmap] [-/+perfect] [-/+poll] [-preset #] [-quick24]
       [-/+quit] [-/+random] [-/+raw] [-rbg color] [-rfg color] [-/+rgb] [-RM]
       [-rmode #] [-/+root] [-rotate deg] [-/+rv] [-/+rw] [-slow24] [-/+smooth]
       [-/+stdcmap] [-tgeometry geom] [-/+vflip] [-/+viewonly] [-visual type]
       [-/+vsdisable] [-vsgeometry geom] [-/+vsmap] [-/+vsperfect] [-wait seconds]
       [-white color] [-/+wloop] [filename ...]

Imponujące... prawda?

Ups\! Jest tam kto\!\! Nie opuszczaj mnie jeszcze...

Nie taki diabeł straszny jak go malują. Zaufaj mi... :-)

Podstawowe opcje linii poleceń, których będziesz potrzebował do
zrobienia głównego okna tapety mogą być ograniczone do następujących
opcji:

``` 
-root
-rmode [0-9]
-max
-maxpect
-quit
```

Teraz możesz robić więcej fantazyjnych rzeczy, a powyższe opcje na pewno
pozwolą ci na to. Tak więc spójrzmy szybko co każda z nich oznacza.

* `-root` Wyświetla obrazek w głównym oknie zamiast w oddzielnym oknie. 
    To jak jest wyświetlane zależy od ustawienia opcji `-rmode` (domyślnie 
    jest ustawiona na 0).
* `-rmode` \[0-9\] Określa jak xv ma wyświetlać obraz w głównym oknie, 
    jeśli podano opcję `-root`. Aktualnie istnieje 10 różnych trybów, które są 
    oznaczane poprzez użycie liczb od 0 do 9. Żeby zobaczyć listę tych trybów, 
    możesz dodać argument -1 do opcji `-rmode`, a xv troszkę ponarzeka 
    i wyświetli informacje dotyczące *prawdziwych* opcji:

    ```sh
        xv -root -rmode -1 ~/images/wallpaper/forest.gif
        xv: unknown root mode '-1'.  Valid modes are:
            0: tiling
            1: integer tiling
            2: mirrored tiling
            3: integer mirrored tiling
            4: centered tiling
            5: centered on a solid background
            6: centered on a 'warp' background
            7: centered on a 'brick' background
            8: symmetrical tiling
            9: symmetrical mirrored tiling
    ```

    Piękne, no nie?

    To jest właśnie to co nazywamy *zimną krwią*. Możesz nie tylko
    określić swój ulubiony obrazek ożywiający twój X Window, ale
    również możesz robić z nim dużo więcej.

    Cóż, wiem co teraz myślicie... "*Skąd mam widzieć co każda z opcji
    oznacza...?!*"

    Miło że pytasz.

    Najprostszym sposobem zorientowania się co do znaczenia każdej opcji
    jest uruchomienie xv, zaznaczenie pliku do wyświetlenia, a następnie
    użyć opcji menu **Root** do zaznaczenia różnych typów wyświetlania
    głównego okna:

    ![XV Image](/post/16/misc/xv.gif)
    Opcja menu Root wyświetli ten sam listing jak na powyższym rysunku.
    Możesz użyć przeglądarki plików w celu zlokalizowania pliku z którym
    chcesz coś robić, a następnie wybierać różne opcje z menu, żeby
    zobaczyć co te opcje dają. Kiedy wybierzesz odpowiednia dla siebie
    opcje, zapisz sobie co ona oznacza. Na przykład, jeżeli podoba ci
    się efekt "integer mirrored tiling", mógłbyś użyć coś w tym rodzaju:

    ```sh
    xv -rmode 3 -quit ~/images/wallpaper/forest.gif
    ```

    xv ustawi tapetę w twoim głównym oknie jako obraz forest.gif
    używając odbicia kafelkowego (?).

    A myślałeś że to będzie ciężka sprawa... :-)

    Ostatnia uwaga: jeżeli używasz opcji `-rmode`, nie musisz podawać opcji
    `-root`, ponieważ jest on domyślny dla `-rmode`.

* `-max` Inną opcją, do której Irek zrobił aluzję, była opcja `-max`. 
    To co robi ta opcja, to rozciągnięcie
    obrazka aż do wypełnienia głównego okna, bez względu na jego
    oryginalny wygląd. Tak więc na przykład, jeżeli miałeś obrazek w
    rozdzielczości 920x740 i uruchomiłeś Xy w 1024x768, użycie tej opcji
    skutkuje rozciągnięciem obrazka, tak by był widoczny w
    rozdzielczości 1024x768. Teraz, w zależności od oryginalnego
    obrazka, przypuszczam, że mógłby on wyglądać nieco zabawnie, ale
    przynajmniej widać go w całym oknie.

* `-maxpect`
    Opcja ta jest troszkę podobna do powyższej, ale tym razem zachowuje
    oryginalny wygląd obrazka. Zakładając, że użyjesz tego samego
    obrazka jak wymieniony wyżej w rozdzielczości 920x740, używając
    opcji `-maxpect` przeskalujesz obrazek, ale
    utrzymasz jego proporcje szerokości i długości. W tym przypadku,
    możliwe, że wysokość obrazka byłaby w rozdzielczości 768, podczas
    gdy jego szerokość byłaby trochę mniejsza niż 1024.

* `-quit`
    Ahhh... *to* jest magiczne słowo które mówi, "Sezamie otwórz
    się\!"... "proszę..."

    Opcja ta powoduje, że xv wyświetla pierwszy obrazek podany w linii
    poleceń, a następnie spokojnie wychodzi, gdy jest kończony. Taką
    zwrotkę możesz dodać do swojego skryptu, lub pliku startowego i mieć
    tapetę w oknie, która jest w pokojowy sposób kończona w chwili, gdy
    się kończy.

Zobacz, że to nie było takie złe, jak się wydawało. Zatem łącząc to
wszystko razem: podejrzewam, że miałeś katalog w swoim katalogu domowym
nazywający się "/images/wallpaper/" do którego wrzuciłeś całą swoją
kolekcje tapet. Chcesz użyć obrazka forest.gif i mieć go w formie
kafelków. Proste jak drut:

```sh
xv -rmode 1 -quit ~/images/wallpaper/forest.gif
```

*Viola!*, natychmiastowe zadośćuczynienie\! :-)

A teraz, możesz łatwo zrobić to z jakiegokolwiek xterm czy rxvt. Możesz
tego dokonać z emacsa lub vi, jeśli wiesz jak uruchamiać polecenia
shella...

*(pssssss...\! Hej koleś... tak, ty. Jeżeli używasz vi, spróbuj coś
takiego:*

```
:!xv -rmode 1 -quit ~/images/wallpaper/forest.gif
```

a zostaniesz nagrodzony).

Wygodniejszym sposobem do zrobienia tego jest wstawienie polecenia do
swojego pliku startowego. Ostatnio zacząłem używać FVWM-95, tak więc w
moim pliku `~/.fvwm2rc95` wyglądać to będzie tak: 

```
~/.fvwm2rc95 file under the "InitFunction" heading:

    AddToFunc "InitFunction" "I" Module FvwmAuto 200
    +                        "I" Module FvwmButtons
    +                        "I" Module FvwmTaskBar
    +                        "I" Exec syslogtk -geometry +2+2 &
    +                        "I" Exec rxvt -ls -sb -sl 400 -fn 9x15 -geometry 80x32 &
    +                        "I" Exec /usr/X11/bin/xv -rmode 1 -quit ~/forest.gif &
```

Inne menedżery okien będą miały własne pliki inicjujące, które będą
potrzebne do dostrojenia. RTFM.

Apropos RTFM, istnieje *obszerny* manual, który John Bradley dostarcza z
xv. "Wszystko Co Chcesz Wiedzieć o XV, Ale Boisz Się Spytać...". W moim
Slackware z roku 1996, dokumentacja instaluje się w /usr/doc/xv, a plik
z dokumentacją to **xvdocs.ps**. Jest to WIELKI plik postscriptowy
opisujący szczegółowo program i wszystkie jego opcje. Jeżeli przede
wszystkim używasz xv jest to plik, który musisz przeczytać. Do jego
odczytania możesz użyć jednej z przeglądarek plików postscript, takich
jak **ghostscript** lub mojego aktualnego ulubieńca **MGV**.

Tutaj jest jeszcze kilka innych przemyśleń w temacie "tapetowania"...

**Zmniejszanie ilości kolorów w obrazie.**

Jeżeli jeszcze tego nie zauważyłeś, jedną z bardziej upierdliwych rzeczy
w X jest wybitne "nadużywanie mapy kolorów". Programy takie jak Netscape
notorycznie alokują wiele pozycji, uniemożliwiając innym programom
załadowanie kolorów, LUB zmuszając do instalowania ich własnych map
kolorów. Kiedy tak się dzieje kończy się to na migrenie i
psychodelicznym kolorowym mruganiu podczas przechodzenia z jednego okna
do drugiego.

Jedynym sposobem zapobieżenia temu jest użycie obrazków z małą ilością
kolorów. By określić jak dużo kolorów jest w użyciu, załaduj obrazek i
zobacz status, który xv pokaże w oknie kontroli. Inną opcją i jedną z
najprostszych w użyciu w linii poleceń, jest użycie programu **xli**:

```sh
xli -ident forest.gif
forest.gif is a 256x256 GIF89a image with 32 colors
```

Aby ograniczyć ilość kolorów, użyj funkcji XV Save i jeżeli zapisujesz
obrazek w formacie GIF, możesz wybrać opcję "Reduced Color". Możesz
także użyć doskonałego pakietu narzędzi graficznych **ImageMagick**:
użyj programu "convert" z opcją `-colors`, aby
określić maksymalną żądaną ilość kolorów. Użycie:

```sh
convert -colors 24 forest.gif forest_rc.gif
```

jest sposobem na wykonanie tego. Jeżeli znasz narzędzie **NetPBM** to
jestem pewien, że możesz robić podobne rzeczy.

**Dodawanie tapety do swojego ulubionego przycisku lub menu.**

Masz *kolekcję* ulubionych obrazków i nie możesz się zdecydować, który
najbardziej lubisz? Czy zmieniasz swoją tapetę częściej niż skarpetki?
Zrób to sam: dodaj to wszystko do swojego ulubionego przycisku lub menu
i miej to dostępne na każde zawołanie\!

Na przykład, jeżeli używasz FVWM-95 i modułu FvwmButtons, mógłbyś dodać
coś w tym rodzaju:

```
*FvwmButtons   forest  gif.xpm     Exec "" xv -rmode -1 -quit ~/wallpaper/forest.gif &
*FvwmButtons    clouds  gif.xpm     Exec "" xv -rmode -1 -quit ~/wallpaper/clouds.gif &
*FvwmButtons    trees   gif.xpm     Exec "" xv -rmode -1 -quit ~/wallpaper/trees.gif &
*FvwmButtons    space   gif.xpm     Exec "" xv -rmode -1 -quit ~/wallpaper/space.gif &
*FvwmButtons    GTO     gif.xpm     Exec "" xv -rmode -1 -quit ~/wallpaper/GTO.gif &
```

i tak dalej.

Teraz możesz zmienić główne okno po prostu przez kliknięcie przycisku\!
Możesz też zrobić coś w tym rodzaju poprzez menu. Po prostu stwórz swoje
własne podmenu i dodaj je do swojego obecnego menu.

Poza tym, nawet jeżeli nie używasz menedżera okien, który jest
dostarczany ze swoimi przyciskami, (tak jak OpenWindows), możesz użyć
programów takich jak **tkgoodstuff** czy **tycoon** jako dodatków i
stworzyć równie wspaniałe przyciski. Programy te można znaleźć na każdym
dobrym FTP Linuksa, lub poprzez wyszukiwarki Alta-Vista lub Yahoo.

I co o tym myślicie? Coś wam to pomoże? Grzebanie w całym tym
oprogramowaniu może zająć SPORO czasu, lecz w tym ponurym, deszczowym
popołudniu kwietniowej soboty, po prostu powiedzcie małżonkom, że
będziecie zajęci przez cały dzień, troszkę "tapetując..."

Bawcie się\!

John

---

## Tapeta z *xlock*...!?

A i owszem... :-)

Od czasu gdy zajęliśmy się tematem tapet, myślałem o tym, co mógłbym
jeszcze pożytecznego wrzucić.

W rzeczywistości jest kilka różnorodnych sposobów zmiany nudnego wyglądu
głównego okna. A jeśli cały czas używasz tej odrażającej, czarno białej
krzyżówki podczas startu X...

Jesteśmy tu aby Ci pomóc! Trzymaj się!

Ze wszystkich gryzmołów, które tworzyłem ponad kilka miesięcy w tym
temacie, wygląda na to, że są CO NAJMNIEJ trzy podstawowe rzeczy, które
możesz robić z tapetą w swoim oknie:

Kolor lub kolor+tekstura

Obrazki

Animacje

W prosty sposób możesz sprawdzić kolory lub kolory+tekstury, poprzez
użycie programu **xsetroot**. Użyj opcji `-solid` z nazwą koloru, żeby 
ustawić kolor głównego okna z jakąś wartością. Poza tym, spróbuj użyć opcji
`-mod \[x\] \[y\]`, która da Ci efekt "kraciastej tekstury". 
Musisz podać wartość x i y dla wzoru, która mieści się w przedziale 0 - 16. 
Możesz poza tym określić kolor tła i przedniego planu używając 
poszczególnych opcji `-fg` i `-bg`.

Szczegółowo rozmawialiśmy o używaniu obrazka w oknie, używając programu
takiego jak **xv**. Zerknij na poprzedni artykuł w tej kolumnie, gdzie
znajdziesz szczegóły. FWIW, *(For What's It Worth - O Ile Jest To
Warte)* możesz poza tym użyć **xsetroot** z opcją `-bitmap [filename]` 
do użycia czarno-białej bitmapy obrazka.

W końcu, możesz używać animacji w swoim oknie. Jest wiele rodzajów
fajnych programików umożliwiających takie rzeczy. Moim ulubieńcem jest
program **xearth**, choć zabawiam się także programami **xfishtank** i
**xantfarm**. Powinieneś je znaleźć na najbliższym FTP Linuksa lub na
świątecznym CD, którą twój małżonek niechętnie Ci kupuje... :-)

A tu jest *jeszcze jedna* sugestia, której prawdopodobnie nie
próbowaliście...

Czy wiecie, że można używać programu **xlock** jako tapety?

Mówię poważnie...spróbujcie tego\!

Program xlock posiada prawie tak wiele opcji linii poleceń jak xv.
Jeżeli więc, wymówisz tajne hasło...

```
xlock --help
xlock:  bad command line option "--help"

usage:  xlock [-help] [-resources] [-display displayname] [-name resourcename]
    [-/+mono] [-/+nolock] [-/+remote] [-/+allowroot] [-/+enablesaver]
    [-/+allowaccess] [-/+grabmouse] [-/+echokeys] [-/+usefirst] [-/+v]
    [-/+inwindow] [-/+inroot] [-/+timeelapsed] [-/+install] [-delay usecs]
    [-batchcount num] [-cycles num] [-saturation value] [-nice level]
    [-timeout seconds] [-lockdelay seconds] [-font fontname] [-bg color]
    [-fg color] [-username string] [-password string] [-info string]
    [-validate string] [-invalid string] [-geometry geom] [-/+use3d]
    [-delta3d value] [-right3d color] [-left3d color] [-program programname]
    [-messagesfile filename] [-messagefile filename] [-message string]
    [-mfont fontname] [-imagefile filename] [-gridsize] [-neighbors] [-mode ant
    | bat | blot | bouboule | bounce | braid | bug | clock | demon | eyes
    | flag | flame | forest | galaxy | geometry | grav | helix | hop | hyper
    | image | kaleid | laser | life | life1d | life3d | lissie | marquee | maze
    | mountain | nose | petal | puzzle | pyro | qix | rock | rotor | shape
    | slip | sphere | spiral | spline | swarm | swirl | triangle | wator
    | world | worm | blank | random]

Type xlock -help for a full description.
```

Niesamowite...

(...a jeżeli dziwi Was, dlaczego nie spróbowałem sugerowanej opcji 
`xlock -help`, wyjaśniam, że przyczyną jest jej zwięzłość. Sam spróbuj 
otrzymać PEŁNY opis!)

Opcje, które powinny Cie zainteresować to `-inroot` oraz `-mode [name]`. 
Żeby zainstalować swój ulubiony galaxy, pyro, blot, rock,
rotor, swarm lub cokolwiek innego w swoim oknie, po prostu zrób coś
takiego:

```sh
xlock -inroot -mode swarm &
```

Teraz cofnij się trochę i ciesz się z widowiska. Oczywiście może Cię
trochę oszołomić oglądanie niektórych z nich, ale jest to rodzaj
zabawnego oglądania motylków i roju tych małych robaczków na całym
ekranie. Dodaj kilka z nich do swojego przycisku lub menu a wzbudzisz
zazdrość wszystkich swoich sąsiadów. Ludzie pomyślą, że jesteś
niesamowity... Może dostaniesz awans... Miła dziewczynka/chłopczyk
mieszkający w sąsiedztwie opowie wszystkim swoim przyjaciołom, że jesteś
*jej/jego idolem...* Może pozbędziesz się kompleksów... Kto wie? Warto
spróbować... :-)

No i co myślisz? Masz jakieś inne pomysły czy sugestie? Jeśli tak,
skrobnij mi co nie co, a ja będę rad opublikować to w następnym
miesiącu. Kto wie, może będziemy musieli napisać mini-HOWTO w temacie
tapet X Window... :-)

---

## Pomysły zapisywania logów systemowych...

Kilka miesięcy temu ktoś zaatakował mojego domowego Linuksa za pomocą
Satana (wolny komputer połączony z INTERNETEM poprzez PPP), zaraz
po tym jak uzyskałem połączenie z siecią. Idiota nie otrzymał informacji
ponieważ mój sendmail był skonfigurowany na zewnętrzne kolejkowanie
poczty. Nie zagłębiając się w szczegóły, wystarczy powiedzieć, że po
nerwach i wykonaniu kilku telefonów i wysłaniu maili z żądaniem
wyjaśnień, winowajca pozostał anonimowy.

Po tym incydencie, zacząłem zastanawiać się jak otrzymać podgląd mojego
systemu w rodzaju uruchomionych procesów, prób logowania się,
debugowania komunikatów itp. Jedno z rozwiązań, które zostało niedawno
dostarczone przez czytelnika, obejmowało zrzut WSZYSTKICH logów
systemowych do nieużywanego VT, poprzez dodanie pewnego rodzaju formułki
do pliku /etc/syslog.conf:

```
    *.*                /dev/tty9
```

Bez wdawania się w szczegóły, wspomnę tylko, że wysyła ona wszystkie
logi na VT numer 9.

Trochę później przyszło mi do głowy, że mógłbym poza tym zrzucać tą
informację do pliku, a następnie uruchomić na pliku polecenie **tail**,
żeby przez cały czas widzieć pojawiające się informacje. Pod Xami jest
to prostsze w wykonaniu poprzez odpalenie xterm lub rxvt, a następnie
polecenia tail na pliku logów systemowych. Aby tak zrobić, mógłbyś:

1.  Ustawić syslogd tak, aby wrzucał WSZYSTKIE informacje do pliku przez
dodanie następującej linii do swojego /etc/syslog.conf:

    ```
        *.*                /dev/tty9
        *.*             /var/adm/syslog
    ```

    Stworzy ci to plik z logami ze wszystkich interfejsów i wszystkich
    poziomów.

2.  Wystartować xterm lub rxvt i uruchomić proces tail na pliku z
    logami. Oczywiście musisz posiadać prawo do czytania tego pliku,
    żeby móc zrobić to tak:

    ```
    rxvt -sb -sl 200 -e tail -n 50 -f /var/adm/syslog &
    ```

    Wolę używać rxvt, bo zabiera on mniej pamięci niż xterm. Opcja
    `-sb` daje mi bardzo przyjemny pasek przewijania; `-sl 200` 
    zapamiętuje w danym momencie 200 linii; a opcja `-e` informuje
    rxvt, żeby uruchamiał wszystko podane po nim polecenia.

    Po zrobieniu tego, możesz zmniejszyć rozmiar okna używając małych
    czcionek. W zależności jak rxvt został skompilowany, możesz
    interaktywnie zmienić rozmiar czcionki używając kombinacji klawiszy
    `ALT-<`> (lub `ALT->`) -- w wersji 2.18 rxvt skutkuje to
    zmniejszeniem czcionek. Poza tym możesz określić rodzaj użytej
    czcionki, wywołując rxvt z opcją `-fn`.
    Używanie sześcio lub siedmio punktowych czcionek da Ci w efekcie
    małe, ale w dalszym ciągu czytelne okienko.

Jeżeli teraz uruchomisz drugą instancje rxvt, a w nim **top**,
zorientujesz się, że da Ci to podgląd tego co się dzieje w twoim
systemie. U mnie wygląda to mniej więcej tak:

![Rxvt with tail and top](/post/16/misc/syslog.gif)

Oczywiście, jest WIELE bardziej eleganckich sposobów, niż uruchamianie
kilku rxvt z top i tail. Niemniej jednak jest to BARDZO łatwe w
ustawieniu, jak również bardzo wygodne, jeżeli dodasz taką formułkę do
pliku konfiguracyjnego swojego menedżera okien lub pod przycisk lub
menu.

Bawiłem się poza tym w pisanie małego skryptu tcl/tk, co do którego
niektórzy z was mogą być zainteresowani. Skrypt **syslogtk** jest BARDZO
prostym programikiem, który umożliwia ci łatwe oglądanie każdego pliku z
logami zlokalizowanego w /var/adm. W trakcie startu, dodaje on menu do
każdego dającego się odczytać, zwykłego pliku w katalogu /var/adm, które
to menu umożliwia następnie możliwość oglądania tego pliku. Poza tym,
automatycznie ładuje plik /var/adm/syslog. Dodałem kilka przycisków do
skalowania tekstowego okna, przechodzenia do początku i końca pliku i
aktualizacji logów (było z tym trochę problemów kiedy odkryłem, że
proces **tail** zawiesza się po skończeniu procesu pppd. Jakieś pomysły
dlaczego tak się dzieje...?)

Używałem tego już trochę czasu i naprawdę to lubię -- szczególnie od
kiedy umożliwia mi on szybkie podejrzenie statusu rzeczy takich jak mail
czy zadań drukowania. Poniżej zamieszczam zrzut ekranu w normalnej i
zmaksymalizowanej postaci:

![syslogtk image](/post/16/misc/syslogtk.gif)

Zminimalizowany program **syslogtk**.

![syslogtk (max) image](/post/16/misc/syslogtk_max.gif)

Zmaksymalizowany program **syslogtk**.

Przepraszam, że nie mam więcej czasu na dyskusję o tym prostym
narzędziu. Jeżeli jesteście nim zainteresowani, źródła są tutaj
dostępne. Możecie zapisać następujący link na dysku, lub po prostu
załadować całość w przeglądarce i zapisać go jako plik tekstowy:

[źródło tcl syslogtk](/post/16/misc/syslogtk)

Jak zwykle rozprowadzam go BEZ ŻADNEJ GWARANCJI: jeżeli coś nawali,
dopasuj do siebie oba kawałki... :-)

Mam nadzieję, że jak będę miał trochę więcej czasu, to napiszę prosty
przewodnik ustawiania i używania zapisów logów systemowych za pomocą
doskonałego pakietu **syslogd**. Przez ten czas jesteście zdani na
siebie. BTW, syslogtk pisałem pod tcl/tk w wersji 7.6/4.2 -- nie ma w
nim zbyt wiele fantazji, tak więc będzie działał dobrze zarówno w
starszych jak i nowszych wersjach. Zerknijcie na początek skryptu, gdzie
znajdują się punkty, które możecie ustawić według waszych potrzeb,
szczególnie do pliku, który ładuje się podczas startu programu. Kod
programu nie jest w tym momencie strasznie rozbudowany, tak więc jeżeli
nie może czegoś znaleźć, po prostu jęknie ze dwa razy i nic nie zrobi....

Cóż, to powinno wystarczyć!

Mam nadzieję, że dobrze się bawicie. Jeśli macie jakieś pomysły czy
sugestie, skrobnijcie mi o tym, LUB, jeszcze lepiej, napiszcie
redaktorowi LG (Marjorie Richardson z SSC) list lub artykulik\!

Trzymajcie się,

---

Tak jest, jeżeli kiedyś zdołam skończyć moją szkołę, to mogę ominąć
tytuł Calc III i Inżyniera Programisty.... :-(

Zobaczymy.

Inną nowością jest to, że planuję wybrać się jeszcze raz w tym roku na
**Linux Expo 1997**\!

Czas na drogową podróż\!\! :-)

Jestem trochę tym zdenerwowany, ponieważ lista prelegentów wygląda jak
spęd "kto jest kim w społeczności Linuksa". Odczyty wyglądają
interesująco i jeżeli będzie takie jak na zeszłorocznym Expo, powinno
być SPORO ZABAWY. Jeżeli ktoś z was jeszcze o tym nie słyszał, a jest w
zasięgu jazdy, lotu Uniwersytetu Stanowego Północnej Karoliny, to powiem
tylko...

JEDŹ!

Wszelkiego rodzaju informacje dostępne są pod adresem [Linux Expo](http://www.linuxexpo.org/).
Wiem, że sporo pracy włożyli w to ludzie z [RedHat Software, Inc.](http://www.redhat.com/).
Przejrzyj stronę i zapoznaj się z prelegentami, wystawcami, wydarzeniami,
odczytami itd.

Kilkoro z nas z Middle Tenn State Univ planuje podróż samochodem.
Będziemy chodzić razem z opiekunem z imiennymi identyfikatorami jak
reszta z Was... Jeśli zdarzy wam się zobaczyć wizytówkę z napisem:

* Brad Curtis
* Steven Edwards (aka "Maverick")
* John Hoover
* lub swojej sympatii...

Podejdźcie, przedstawcie się i przybijcie piątkę\! Będziemy zachwyceni
możliwością pogadania z Wami. Jeżeli będę miał szansę, wezmę ze sobą
Cannona i spróbuję pstryknąć parę fotek. Jeżeli dostanę w swoje ręce
skaner, będę mógł nawet wstawić kilka z nich do kolumny w następnym
miesiącu (oczywiście za pozwoleniem ludzi z Expo).

Tak więc, mam nadzieję, że uda nam się spotkać\!

Trzymajcie się, Wesołego Linuksowania i Wszystkiego Najlepszego,

