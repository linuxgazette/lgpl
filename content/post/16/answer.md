---
title: The Answer Guy
date: 2022-02-09
---

* Autor: John M. Fisk
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue16/answer.html

---

## Korekcja URL do SATANa

**From: Richard White,
[whiter@digex.net](mailto:whiter@digex.net)  
W wydaniu 14 Linux Journal, powołałeś się na
ftp.cs.perdue.edu...(szeroki uśmiech) Taki adres nie istnieje. Myślę że
przypuszczalnie miało być ftp.cs.purdue.edu.**

**-- Richard D. White, Business Connectivity Technical Support**

![](/post/16/misc/ans.gif)
Racja\! Tak właśnie miało być. A z innej beczki -- znalazłeś jakieś
użyteczne informacje?

![](/post/16/misc/ques.gif)
**Tak. Bardzo. Dopiero co pobrałem SATANa i kilka innych narzędzi
bezpieczeństwa. Pracuje w obsłudze klienta przy dzierżawieniu łącz i
czasami pomagam klientowi konfigurować jego firewall. Poznawanie dziur w
systemie i zatykanie ich jest bardzo cenną wiedzą.**

![](/post/16/misc/ans.gif)
Czy próbowałeś cops (wczesny pakiet audytu bazujący na hoście Dana
Farmera) lub Tiger (Texas A\&M University)?
Udało ci się uruchomić tripwire? Ja (i większość społeczności Linuksa,
która go próbowała) miałem małe problemy z Tripwire. Kilka miesięcy temu
miałem z nim małe zamieszanie -- zapomniałem o nim. Więc ostatnio
musiałem sciągnąć i zbudować go od nowa.

Budując go natknąłem się na te same problemy -- i te same problemy z
README.linux. Sam odkryłem, że ktoś -- ktokolwiek -- powinien
przygotować poprawne ustawienie pacza, który pozwoli użytkownikowi
Linuksa na jego prostą kompilacje z minimalnym wysiłkiem.

Nie jestem programistą (choć wykonuję "roboty internetowe") tak więc
naprawdę nie czułem się wykwalifikowany do zrobienia tego. Niemniej
jednak nigdy nie byłem zdolny do inspiracji wysiłkiem innych osób zanim
zrobiłem to sam.

Tworzenie ustawień pacza pociągneło za sobą naukę używania CVS (version
control system - system kontroli wersji). Myślę nad napisaniem artukułu
na temat używania CVS do kontroli lokalnych zmian w drzewie pobranych
źródeł i odzwierciedlania zmian, tak aby można się dzielić swoją pracę
z innymi w sieci.

Naturalnie podałem używanie tripwire jako jeden z przykładów --
prawdopodobnie pgp jako inny. Poza tym planuje zaimportować źródła
mojego jądra na CVS.

Jeżeli jesteś zainteresowany mógłbyś pobrać mój pacz i poinformować mnie
czy działa. Jest tego w sumie około 150 linii tekstu, które według mnie
wyglądają na działające, używając standardowego programu 'paczowania'
Larrego Walla.

-- Jim

-----

## EDI w Linuksie

**From: Adam Morrisom,
[adam@morrison.iserv.net](mailto:adam@morrison.iserv.net)  
Dopiero co dostałem zgodę od kierownictwa na instalację naszej pierwszej
maszyny Linuksowej, dokładniej na nie-tak-mocnym RS/6000. Na razie
pracuje bez zarzutu (dokładnie tak jak oczekiwałem). I nagle Linuks
staje się możliwym rozwiązaniem wszelkiego rodzaju problemów, które mamy
(oni kochają metke z ceną (?)). Teraz muszę zaimplementować EDIego i
byłem naprawdę zdumiony tym, czy jest jakiekolwiek oprogramowanie
dostępne pod Linuksa. Nie byłem w stanie znaleźć niczego, na mapach
oprogramowania, sunsite lub czymkolwiek innym. Jakiekolwiek wskazówki
lub kontakt do ludzi byłyby naprawdę cenne.**

**Adam,**

![](/post/16/misc/ans.gif)
Z pewnością wstawiasz dobre hasło do konkursu Jima "ciężki orzech do
zgryzienia".

Słyszałem o EDI (electronic data interchange - elektroniczna wymiana
danych) i o ile sobie przypominam jest on specyfikacją formatu danych do
elektronicznego handlu (przeważanie z świecie mainframeów, gdzie X.25
dominuje nad TCP/IP).

Jednak nie słyszałem o żadnym projekcie lub produkcie dostępnym pod
Linuksa.

Masz tu kilka stron internetowych, które wykopałem, a które mogą ci
pomóc:

* [Oprogramowanie St. Paul - UNIX](http://www.spedi.com/products/unix.html)
* [Bookmarki Shada](http://www.marin.cc.ca.us/~shadow/bookmark.html)
* [Premenos Technology Corporation](http://www.premenos.com/)
* [Więcej o Elektronicznej Wymianie Danych (EDI)](http://www.teren.com/edi.html)
* RFC Archives -- RFC1767 (link nie działa 2-Apr-2001: artykuł usunięty)
* [TSI International](ftp://ds.internic.net/rfc/rfc1767.txt)
* [1994 EDI-L (Tematy Elektronicznej Wymiany Danych)](http://www.tsisoft.com/pages/hotlinks.htm)
* [Archiwum Listy Mailingowej, Re: PC based EDI](http://www.ima.com/mlarchive/lists/edi-l.1994/0241.html)
* [Uniforum: 1995 Index](http://www.uniforum.org/news/html/publications/ufm//1995-Indes.html#anchor5495158)

Mam nadzieje, że ci to pomoże. W zasadzie wygląda na to, że nie są to
"niepółkowe" pakiety EDI dla jakiejkolwiek platformy. Powodzenia.

Jedno co możesz zrobić, to skontaktować się z wydawcą lub autorem twoich
aplikacji EDI i zobaczyć czy coś dla ciebie mogą zrobić.

-- Jim

-----

## ![](/post/16/misc/ques.gif) zmodem

**Odpowidaczu pomóż\! Nie mogę pobierać z sieci\! Oto moja historia: -
uruchamiam Linux v2.0.0. Używam minicom v1.71. Nie ruszałem mojego ftp
od czasu instalacji, więc miałby on domyślną konfigurację.**

![](/post/16/misc/ans.gif)
Niecierpie debugować problemów z liniami szeregowymi.

Tutaj jest podstawowa litania do rozwiązania problemów modemowych:

Co się dzieje na niskich prędkościach? Jakiego IRQ używa ta linia
szeregowa? Jaki rodzaj UART jest zainstalowany? Jakie jest ustawienie
kontroli przepływu (flow control)? Czy kabel ma żyły dla wszystkich
sygnałów? Jak jest skonfigurowany modem (sprzęt i polecenia startowe)?

minicom 1.71 jest troszke stary. Mam tutaj 1.75 -- a na sunsite może być
nawet nowsza wersja.

Nawiasem mówiąc -- prawdopodobnie powinieneś zaktualizować wersje jądra
do 2.0.29.

Twój problem może nie być powiązany z żadnym z tych czynników - ale
auktualizacja nie zaboli.

Po pierwsze sprawdziłbym konfiguracje dla ciągu inicjującego i kontroli
przepływu. Spróbuj zainicjować poleceniem:

```
AT&C1&D2
```

...(które pamiętam od lat obsługując PCAnywhere, jak również z mojej
aktualnej konfiguracji). Ustawi to modem dla DCD (device carrier detect)
i flow control. Nie pamietem, które jest którym i co robią inne numery
-- zerknij do manuala jeżeli jesteś ciekaw).

Następnie upewnij się, czy jest włączona opcja "Hardware Flow Control" w
"Serial port setup" ("on").

Kiedy mam problemy z liniami szeregowymi i modemami znajduje je do
otrzymania cyfrowego ekwiwalentu "drugiej opcji." -- Czy uruchamiasz
jakieś inne oprogramowanie komunikacyjne na tym systemie (pppd, uucp/cu,
mgetty -- wdzwanianie, seyon)? Czy działają niezawodnie w trakcie
transferu danych (wstawia linię przed załadowaniem)?

Sugerowałbym ściągnięcie kopi C-Kermit z Columbia Univerity
[kermit.columbia.edu](mailto:kermit.columbia.edu)
. Bez obrażania Miguela van Smoorenburga, ale minicom sprawiał też
problemy w moim systemie. C-Kermit nie jest jakimś pełnoobrazowym,
okienkowym programem, ale wykonuje solidną robotę "rozmawiając" z
modemem. Jego możliwości skryptowe są także znacznie bardziej
zaawansowane niż minicomowy 'runscript' -- i posiada właściwości, które
trzeba wkładać do 'minicoma' za pomoca skryptu (na przykład).

Czy masz inne konto na innym systemie (BBS lub ISP)? Czy twój transfer
plików działa OK do lub z tamtąd? Problem może być z twoim ISP zamiast z
twoją końcówką.

Co się dzieje jeżeli próbujesz innego protokołu -- takiego jak kermit?
Kermit często charakteryzuje się jako "wolne" porównując z zmodem -- ale
głównie dlatego, że jest on domyślnie dostrojony dla bardzo
zaszumionych, zawodnych połączeń, co było powszechne w czasach gdy
powstawał (prawie 20 lat temu).

Po sprawdzeniu z użyciem innego programu komunikacyjnego, zerknąłbym
trochę głębiej. Używając polecenia:

``` 
stty -a < /dev/modem
```

... i

``` 
setserial -a /dev/modem
```

(oba polecenia zakładają, że masz dowiązanie "modem" do odpowiedniego
`/dev/ttyS*` w swoim systemie).

Upewnij się, że twój stty raportuje crtscts (dla flow control).
Nastepnie upewnij się że kabel pomiędzy komputerem a modemem ma
wszystkie połaczenia dla tych pinów.

Sprawdź dwa razy czy nie masz żadnych konfliktów IRQ. Są one o tyle
podstępne, że mogą nie pokazać konfliktów dopóki port nie jest
podniesiony (załadowany).

Sprawdź dodatkowo czy masz "szybki" (high speed) UART (16550AFN) na tym
porcie.

Następnie sprawdziłbym konfigurację modemu. Możesz zobaczyć niektóre z
ustawień poleceniem AT\&V (które to w większości modemów Hayesa wyrzuci
na ekran dane konfiguracyjne i wartości S-register). Zerknij na string
inicjujący, który używasz w Minicom, i zobacz w instrukcji do modemu
rekomendowany string inicjujący przeznaczony dla podobnego
oprogramowania.

Po sprawdzeniu tego wszystkiego, zamknąłbym system i uruchomił DOSa
(jeżeli nie posiadasz kopii DOSa, weź pod uwagę pobranie kopii Open Dos
Caldera. Nie jestem pewien co do jego licencji -- ale czytałem, że
możemy się z nim zapoznawać przez 90 dni). Razem z kopią DOSa możesz
potrzebować poza tym Telix, Qmodem, Procomm lub innego pakietu
komunikacyjnego. Wiele z nich jest w wersji shareware -- Telix jest moim
ulubieńcem.

(Uwaga: nie jestem zwolennikiem używania tych pakietów bez respektowania
ich licencji. Jeżeli zdecydujesz się na kontynuację użytkowania Telixa
lub OpenDOSa -- nawet do sporadycznych sesji "rozwiązywania problemów",
prosze przeczytaj i przestrzegaj postanowień licencyjnych i
rejestracyjnych. Tak, mam pełną legalną kopie Telixa (DOS i Windows)).

W jakimkolwiek przypadku lubie sprawdzać z czystego starego DOSa,
ponieważ stary program ładujący jest naprawde minimalny. Mógłbyś
spróbować zbudować jądro Linuksa bez obsługi TCP/IP i pozbawić go
wszystkich sterowników urządzeń oprócz obsługi portu szeregowego i
konsoli i wystartować system w trybie jednoużytkownikowym (single user
mode)... czy cały czas jest zamknięty

Ten pomysł umożliwi sprawdzenie czy jakieś inne urządzenia czy
możliwości sprzętowe są w konflikcie.

## ![](/post/16/misc/ques.gif) Posiadam internetowe konto shellowe "Best", sądze że to iris, ale nie
znam jego wersji.

![](/post/16/misc/ans.gif)
Zgaduję, że masz na myśli, że twoje konto jest na best.com, i że jest
ono na Irix (SGI). (Co jest interesujące -- jakbym miał zgadywać
Sun/Solaris -- ale co ja wiem).

Uwaga: Irix i Solaris nie są znane z doskonałej obsługi linii
szeregowych. Są one dotychczas przystosowane do obsługi ethernetu TCP/IP
-- przyjmując, że większość stron będzie używać serwerów terminali (małe
urządzenia dedykowane, które konwertują szeregowe połączenia na sesje
telent). W rezultacie, słyszałem, że kopie rz/sz dostarczane z nimi
powinny rutynowo być wymienione na najnowsze źródła z Internetu.

![misc/ques.gif) **mam modem usrobotics sportster 28.8**

![](/post/16/misc/ans.gif)
Wewnętrzny czy zewnętrzny?

Osobiście nie lubie serii Sportster. Ich Courier sa fajne. Narazie
używam Practical Peripherals -- ale moim następnym modemem będzie
prawdopodobnie Zyxel.

##  ![](/post/16/misc/ques.gif) Wprowadzam `sz <filename>`. Działa dobrze do momentu 
około 40k, a następnie dostaje kilka różnych komunikatów błędu:

``` 
BAD CRC:0
```

**czasami po następnej próbie pobierania (zazwyczaj po jednej lub dwóch)
znowu ten sam błąd LUB**

``` 
GARBAGE COUNT EXCEEDED:0
```

**a następnie time-out.**

**AARRGH\! Co się do licha dzieje? możesz wysłać do mnie prywatnego
emaila, jako że prawdopodobnie jest to naprawdę zwyczajny problem, a ja
poprostu nie patrzyłem w dobrym kierunku\!**

![](/post/16/misc/ans.gif)

Moje przypuszczenie byłoby takie, że nie masz "szybkiego" UART. Lub twój
flow control nie jest prawidłowo ustawiony.

Przypuszczam, że przyczyną jest to, że 40K jest rozsądną ilością
pobieranych danych dla modemu i bufora podczas systemowej zmiany
kontekstu (context switch). Przepełnienie bufora (w 16450 -- starszy,
wolny UART) mogło poprostu przerwać transfer na pierwszej zmianie
kontekstu.

Co do 16550 UART -- UART ma szesnasto bajtowy bufor FIFO. To
wystarczające dla UART do zmiany stanu na połączonych liniach (niższy
CTR -- czyść odbiór -- linia) i wystarczające do zapamiętania
przychodzących danych podczas odpowiedzi innego systemu (zatrzymanie
wysyłania).

Przy 28 Kbps nadchodzące do bufora 16450 (jeden bajt\!), nadawca będzie
nadawał wiele bitów przed otrzymaniem komunikatu (które twój system
odrzuca).

Piszę o tym w Linux Gazette \*ponieważ\* jest to powszechny problem.
Większość z nas w prawdziwym świecie używa modemu -- nie mamy T1 czy
ISDN (mam aktualnie Tracell WebRamp, ale jeszcze go nie używam). Więc
cały czas walczymy z tymi problemami.

Mam nadzieje że USB (IEEE 1394 "Firewire") rzeczywiście wystartuje w
przyszłym roku. Mijają już dwa lata od czasu rozpoczęcia prac.

Czy ktokolwiek z was pracuje z płytą z USB pod Linuksem?

Dla tych którzy mało wiedzą o "Firewire":

* [USAR Systems -- Fireware Info](http://www.usar.com/indact/standard/firewf.htm)
* [The IEEE-1394 High Performance Serial Bus -- Adaptec's FAQ](http://www.adaptec.com/firewire/1394main.html)
* [IEEE 1394 Trade Assoc. -- Firewire, USB, serial bus](http://www.1394ta.org/)

Jeżeli masz jakiekolwiek nowości Linuksowe w tym temacie -- wyślij
pocztę do [tag@starshine.org](mailto:tag@starshine.org).

--Jim

---

## ![](/post/16/misc/ques.gif) Uruchamianie połączenia z Interentem w Linuksem

**From:Ricardo Romero
[rromero@netfriendly.com](https://web.archive.org/web/20030813200108/mailto:rromero@netfriendly.com)**

**Cześć, nazywam się Ricardo Ribeiro Romero i mieszkam w Brazylii,
próbuje uruchomić INTERNET z poziomu Linuksa, ale nie uruchamia się,
możesz mi pomóc?**

**Tks,  
Romero, Ricardo**

![](/post/16/misc/ans.gif)
Może to wyglądać mało przyjacielsko Romero, ale chciałbym zasugerować,
że może potrzebujesz zgłosić się po pomoc do lokalnego konsultanta lub
specjalisty komputerowego.

Pytania do publikacji -- szczególnie darmowa publikacja, która jest w
całości obsługiwana przez wysiłek pisarzy-ochotników i hojnych
sponsorów SSC, musi być dość specyficzna i sensownie uzasadniona.

Każda rozsądna dystrybucja Linuksa zawiera wszystkie narzędzia potrzebne
ci do połączenia się z Interentem jako klient i wszystkie narzędzia,
które większość ludzi kiedykolwiek by potrzebowała chcąc zostać
dostarczycielami usług (service provider).

Z twojej wiadomości nie wynika jasno, czy próbujesz ustawić swój system
jako serwer/provider czy jako klient, czy oba naraz.

Jest wiele dobrych książek traktujących o Sieci w Linuksie (która jest w
dużej mierze taka sama jak sieć pod innego rodzaju Unixami). Moim
osobistym faworytem byłby Linux Documentation Project's Network
Administrator's Guide (w skrócie LDP NAG). Jest dostępny w wersji
elektronicznej (jako text, postscript, TeX lub HTML) i prawdopodobnie na
jakiejkolwiek płycie CD, którą byś kupił. Możesz poza tym kupić
profesjonalnie wydaną i wydrukowaną kopię od O'Reilly & Associates
(między innymi).

Poza tym O'Reilly publikuje książkę nazywającą się jakoś tak: "Getting
Connecting: Establishing a Presence on the Internet" (byłaby to książką
"Pig") autorstwa Kevina Dowda). Jeżeli próbujesz samodzielnie ustawić
sieć jako ISP lub jeżeli chcesz mieć dedykowane połączenie z siecią
(powiedzmy dla twojego biura) wtedy prawdopodobnie to jest to czego
chcesz.

Osobiście zalecam, żeby większość małych firm i prywatnych ludzi unikało
"dedykowanych" lub "stałych/całoczasowych" połączeń do Internetu. O
wiele mniej kosztowna jest konfiguracja UUCP do poczty i grup
dyskusyjnych -- i znalezienie wirtalnego hostingu i/lub zewnętrznych
lokacji do obsługi stron internetowych i innych usług. Może to być
uzupełnieniem wybierania PPP na żądanie (używając skryptu lub diald)
dostarczając dostępu do sieci -- poprzez modem lub ISDN.

Jedną z większych korzyści ISDN jest mniejsze opóźnienie. Połączenie
modemowe potrzebuje od około 30 sekund do 1 minuty na wybieranie,
dzwonienie, połączenie i negocjację połączenia. ISDN może to zrobić w 3
sekundy. Będziesz o wiele mniej niechętny do odłożenia słuchawki lub
odblokowania linii telefonicznej jeżeli wiesz, że możesz uzyskać
połaczenie za około 3 sekundy.

W dodatku zmniejszenie wydatku działania twojej sieci jako rozłączonej
sieci uwolni cię od sporych zmartwień o bezpieczeństwo, które jest
związane ze stałym połączeniem sieciowym. Oczywiście -- twoje
połączenie PPP jest bezpośrednie (ludzie mogą się wstecznie połączyć z
tobą i próbować wrzucać explioty na te same usługi jakie ja mam przy
stałym połączeniu z siecią). Niemniej jednak będziesz obserwował jakieś
dodatkowe ładowania czy anomalie -- a cała twoja sieć jest znacznie
mniej atrakcyjna dla crackerów.

(Ludzie, którzy łączą się z Internetem poprzez PPP, naprawdę mogą być
pewni 1 godzinnego bezpieczeństwa ich komputerów. Może kiedyś nabazgram
o tym jakiś artykuł). Romero,

Wracając do twojego pytania. Spróbuj proszę poczytać o tych połaczeniach
i/lub weź pod uwagę wynajęcie lokalnego konsultanta. Nie wiem nic na
temat systemów telefonicznych w Brazylii -- mam pewne wątpliwości czy
jakiś ISP bedzie działał dalej niż 200 mil w głąb lądu od wybrzeża
Pacyfiku.

--Jim

---

## ![](/post/16/misc/ques.gif) Respawning too Fast

**From: Igor Markov
[imarkov@math.ucla.edu](https://web.archive.org/web/20030813200108/mailto:imarkov@math.ucla.edu)**

**Moje pytanie związane jest z niesławnym komunikatem otrzymywanym z
init: "Respawning too fast". Ten komunikat pojawia się w moim
/var/log/messages co 5 minut (oczywiście\!) dla xdm. Przypuszczam, że
jest to dla "The Answer Guy": init: Id "x" respawning too fast: disabled
for 5 minutes. Niemniej jednak, xdm działa (widzę go na wyjściu
polecenia ps i nie mam problemów z jego używaniem).**

![]/post/16/(misc/ans.gif)
Nie pokazujesz odpowiednich linii w twoim /etc/inittab, ale powinna ona
wyglądać jakoś tak:

\# Run xdm in runlevel 5 (and 4 for me) x:45:respawn:/usr/bin/X11/xdm
-nodaemon

(Uwaga: uruchamiam xdm w 4 i 5, które są nieużywane -- ale 4 jest moim
zwykłym poziomem -- z 12 VC, xdm w VC13 -- dostępny poprzez prawy alt +
F1 -- i wyjście sysloga na VC15, VC14 jest używany do polecenia
otwierającego stray lub do przekierowania wyjścia pesky z procesów
pracujących w tle).

Przypuszczam, że nie masz opcji -nodamon w swoim pliku. (Spróbuj ją
dodać).

Jeżeli się pomyliłem, wtedy rozwiązanie problemu będzie bardziej zawiłe.
Sprawdź u producenta twojej dystrybucji Linuksa czy nie ma on jakiejś
łaty.

Użytkownicy Red Hat mogą zerknąć tutaj: [http://www.redhat.com/support/docs/errata.html](http://www.redhat.com/support/docs/errata.html)

...żeby zobaczyć co zostało poprawione od czasu wypalenia swojej płyty
CD.

Poza tym możesz potrzebować zerknąć do swojego pliku xdm-config
(/etc/X11/xdm/xdm-config -- jeśli jestes farciarzem -- w przeciwnym
wypadku może to być w .... /usr/X11R6/....????).

Najlepszym wprowadzeniem do xdm, które kiedykowliek znalazłem było:
\_The\_Shell\_Hacker's\_Guide\_to\_X\_and\_Motif\_ Johna Wiley'a & Sons.

## ![](/post/16/misc/ques.gif)

**Wygląda na to, że init próbuje "spawnować" drugi xdm. Nie mogłem potwierdzić ani
zaprzeczyć tej hipotezie... (egrep xdm /etc/\* /etc/\*/\* nie pokazał
nic obiecującego). Dziękuję**

![](/post/16/misc/ans.gif)
"Respawning too fast" oznacza, że program kończy działanie (raczej
natychmiast) i w związku z tym init sądzi, że musi być z nim spory
problem. Na przykład jeżeli getty jest "respawningowany", to może tak
być, bo usiłuje uchwycić brakującą linię szeregową (jak w przypadku,
gdy nie skonfigurowałeś sterownika serial w swoim kernelu i zapomniałeś
załadować modułu -- lub coś w tym rodzaju).

Jeżeli xdm ładuje się i rozwidla deamona (jest tak domyślnie) wtedy
wygląda on jak exit/failure dla init. Opcja -nodaemon wymusi na xdm
start z terminala, z którego init go wystartuje (nie jest to próba
uruchomienia w "tle" jak to działoby się, gdybyś uruchomił go z linii
poleceń).

Fakt, że twoja kopia działa to sugeruje -- ale, gdy wylogujesz się z
sesji xdm init mógł potrzebować do pięciu minut dla decyzji ponownej
próby uruchomienia xdm (chyba, że twoja konfiguracja wylogowywania się
z xdm wykonuje respawning lub coś innego dziwnego).

--Jim

---

## ![](/post/16/misc/ques.gif)Probelmy z mapowaniem klawiatury

**From: Gilbert R. Payson
[g.payson@edina.xnc.com](https://web.archive.org/web/20030813200108/mailto:g.payson@edina.xnc.com)  
Cześć. Mam trzy (dobra, cztery) maszyny linuksowe w Niemczech. Mój
problem jest nastepujący: W XWindows mapowanie klawiatury (keyboard
mapping) jest prawie doskonałe. Ale mam z tym kilka problemów:**

**Nie działa @. Przenosi mnie do ostatnio edytowanej linii (jak
"strzałka w górę"). Jak mogę to naprawić?**

**dzięki\! -gil**

![](/post/16/misc/ans.gif)
Myślę, że powinieneś zapoznać się z poleceniem xmodmap. Poza tym zerknij
do następujących dokumentów HOW-TO:

* [Keyboard HOWTO](http://sunsite.unc.edu/LDP/HOWTO/Keyboard-HOWTO.html)
* [Key Setup mini-HOWTO](http://sunsite.unc.edu/LDP/HOWTO/mini/Key-Setup)

--Jim

---

## ![](/post/16/misc/ques.gif) Prędkość modemu

**From: Scott
Atwood[mailto:atwood@cs.stanford.edu](https://web.archive.org/web/20030813200108/mailto:atwood@cs.stanford.edu)  
Chciałbym skomentować pytanie z kolumny "The Answer Gui" w 13 wydaniu Linux
Gazette, dotyczące łączenia modemów w celu zwiększenia prędkości. To
pytanie odzwierciedla powszechnie błędne przekonanie zrównywania
szerokości pasma z jego szybkością. Opóźnienie jest o wiele bardziej
istotnym pomiarem dostrzegalnej prędkości, szczególnie w interaktywnych
aplikacjach, takich jak sesje telnetu, czy przeglądanie stron
internetowych. Łączenie modemów zwiększy przepustowość łącza, ale
opóźnienie pozostanie takie samo. Więcej kompletnych informacji w tym
temacie można znaleźć tutaj: [http://rescomp.stanford.edu/\~cheshire/rants/Latency.html](http://rescomp.stanford.edu/~cheshire/rants/Latency.html)  
anessay Stuarta Cheshirea, autora Bolo.**

### ![](/post/16/misc/ans.gif)
W końcu zabrałem się za czytanie twojego artykułu. Był bardzo
interesujący.

Myślałem o ostrzeżeniu czytelników, że podwojenie przepustowości ich
łącza zwiększyłoby tylko transfer -- ale być może przeoczyłem to.

--Jim

---

## ![](/post/16/misc/ques.gif)Duplikowanie Zainstalowanych Dysków Twardych w Linuksie

**Zainstalowałem Slackware na moim PC i jestem naprawdę
usatysfakcjonowany. Chcę zduplikować dyski twarde z zainstalowanym
Linuksem: Czy moge użyć mojego pierwszego dysku jako źródła i skopiować
całą jego zawartość na drugi, sformatowany dysk twardy? Jeżeli wstawię
ten dysk do innego PC, czy Linuks wystartuje bez problemów?**

![](/post/16/mics/ans.gif)
Możesz poprostu użyć polecenia 'dd' ("disk dump" lub "data dump").
Będzie to działało, jeżeli dwa dyski są identyczne i nie mają
uszkodzonych sektorów (bad sectors).

Wiele lat temu powiedziałbym, że byłbyś idiotą biorąc to pod uwagę.
Teraz przekonywałbym odwrotnie w o wiele łagodniejszym języku.

Różnica jest taka, że nowoczesne napędy -- IDE i SCSI są zdolne do
autotranslacji, tak więc BIOS i często sterowniki dysków Unix/Linuks nie
muszą znać prawdziwej geometri dysków. W dzisiejszych czasach, większość
napędów ma poza tym zapasowe sektory na każdej ścieżce -- podczas
niskopoziomowego formatowania zapasowe sektory są mapowane do użycia za
jakiekolwiek błędne sektory na danej ścieżce. Używając tego schematu
(który normalnie jest całkowicie przezroczysty dla maszyny -- wszystko
jest w elektronice napędu) rzadko można zobaczyć jakieś złe sektory w
napędzie (dopóki wszystkie zapasowe sektory w danej ścieżce nie zostaną
zużyte).

Cóż to jest wykonanie techniczne.

Niemniej jednak powiedziałbym, że bezpieczniej będzie jak spędzisz
trochę więcej czasu i "zrobisz to dobrze".

Użyj fdisk do zrobienia partycji na nowym napędzie; ustaw jego partyjce
tak aby był on pierwszym dyskiem. Możesz to zrobic bez potrzeby
"kładzenia" systemu. Osobiście wolę iść za radą i restartować system po
zapisaniu nowej tablicy partycji -- ale to jest prawdopodobnie
pozostałość z przyzwyczajeń nabytych w trakcie długoletniej pracy z
DOS i OS/2.

Następnie wklep `mke2fs -c /dev/hdbX` (gdzie X jest numerem partycji dla
każdej z nowych partycji).

Następnie zrób:

``` 
mount /dev/hdbX /mnt/tmp
find . -mount | cpio -pvum  /mnt/tmp
```

... dla każdej z nich.

Teraz jest juz prawie skończone. Jedynym problemem jest to, że mapa
bootowania lilo (w twoim istniejącym dysku) prawdopodobnie nie pasuje do
konfiguracji lilo na nowym dysku.

Najbardziej niezawodnym sposobem poradzenia sobie z tym jest włożenie
nowego dysku do komputera -- wystartuj system z dyskietki ratunkowej
używając root=/dev/hdaX (w prompcie lilo startującego z dyskietki) i
edytuj /etc/lilo.conf. Następnie wydaj polecenie lilo i zrestartuj
system.

To wszystko. To jest siedem kroków (z czego 3 pierwsze powtarzane są dla
każdego systemu plików na napędzie(ach). Czas potrzebny na to jest
pomniejszony o czas niezbędny na otwarcie obudowy komputera i zmianie
jumperów w nowym napędzie (który, według mojej wiedzy, jest dużo gorszy
dla IDE niż większości SCSI).

Dlaczego to jest lepsze? Coż wiąże się to ze uszkodzonymi blokami (bad
blocks) i małymi różnicami w geometrii. Poza tym zapewnia defragmentacje
nowej kopii. Inne niż to -- czuję, że to lepszy sposób.

--Jim

---

## ![](/post/16/misc/ques.gif)Używanie Linuksa jako Firewalla

**From: Tim Gray
[timgray@lambdanet.com](mailto:timgray@lambdanet.com)  
Cześć, mam mały problem, który mógł tutaj dotknąć także innych...
próbuję zrobić z Linuksa coś w rodzaju "firewalla" dla komputera z
Windowsem 95 mojej żony (nie byłem w stanie jeszcze jej "przełączyć").
Zainstalowałem płyty kompatybilne z ne2000 w każdym komputerze,
połączyłem je, wszystko zainstalowałem według instrukcji
administracji sieciowej Linuksa. Problem, który mam to otrzymywanie
pakietów wychodzących do Internetu przez linie modemową, kiedy ta nie
jest połączona. Potrzebuje sposobu, żeby Linuks automatycznie odpalał
moje połączenie dial-up kiedy zobaczy, że odległy komputer chce go użyć
i miał możliwość "zabijania" połączenia po określonym czasie
bezczynności.**

**Dziękuje. Tim**

![](/post/16/misc/ans.gif)
Pytanie odnosi się do serwera "Proxy" -- który jest tylko częścią
pewnych architektur firewalli.

Wydaje się, że próbujesz ustawić "dzwonienie na żądanie maskaradowanego
hosta proxy." (o ile dobrze rozumiem).

Pierwsze narzędzie, którego potrzebujesz nazywa się 'diald' -- ('deamon
dial').

Najnowsze wersje, o których wiem, znajdują się tu:

[ftp://sunsite.unc.edu/pub/Linux/system/network/serial/](ftp://sunsite.unc.edu/pub/Linux/system/network/serial/)

... a plik nazywa się:

```
diald-0.16.tar.gz
```

Dopiero co skonfigurowałem go (dosłownie w momencie, gdy ten list
lądował w mojej skrzynce pocztowej). Była nadspodziewanie łatwa.

Po prostu edytowałem plik 'make' (zmieniłem LIBDIR, BINDIR, i inne
katalogi wskazujące na /usr/local...) zrobiłem make i make install.
Następnie stworzyłem plik nazywający się /etc/diald.conf z tylko jedną
dyrektywą w nim: 'lock'. Zrobiłem to, więc mogę chętniej obsługiwać
wiele konfiguracji diald -- jak obecnie wyjaśnię:

Stworzyłem katalog /etc/diald/ i wstawiłem tam plik wyglądający mniej
więcej tak:

    device /dev/modem
    connect "chat -f /etc/ppp/connect"
    speed 38400
    modem
    defaultroute
    crtscts
    redial-timeout 120
    connect-timeout 120
    mode ppp
    dynamic
    local 192.168.1.1
    remote 192.168.1.2
    include /usr/lib/diald/standard.filter

Oczywiście twój plik może się różnić w kilku punktach. Parametr -f
powinien wskazywać na skrypt startujący, którego używasz do ręcznego
łączenia się z Internetem. Możesz zmienić linię "device" -- choć
naprawdę polecam konsekwentną konfiguracje wszystkich twoich pakietów do
używania /dev/modem (który jest poprostu dowiązaniem symbolicznym do
prawdziwego portu w systemie).

Aktualnie mam diald, pppd (ręczne), uucp, kermit, minicom i mgetty,
wszystkie dzielą ten modem i używają tego samego pliku lock.

Lokalne i zdalne adresy są pozornie arbitralne -- używam adresów
wymienionych w RFC1918 (nee 1597), które rezerwują kilka adresów, które
nie są "prawdziwymi" adresami sieci Internet. *(Chodzi o adresy
"nierutowalne" np. te z klasy A 10.x.x.x - przyp. tłum.)*

Nastepnie dodałem następujące dwie linie do mojego /etc/rc.local:

``` 
modprobe slip
/usr/local/sbin/diald -f /etc/diald/rahul
```

(Gdzie plik rahul jest tym, który pokazałem wyżej i odnosi się do
jednego z moich providerów PPP).

Jeżeli masz niezawodne wyjście telefoniczne na żądanie -- następnym
krokiem jest ustawienie routingu z komputera twojej żony do Internetu.

Rekomendowałbym uruchomienie połaczenia ppp ręcznie, a następnie
zrobienie wszystkich rzeczy związanych z routingiem/maskaradą/proxym
oraz testowanie konfiguracji w trybie on-line.

--Jim

