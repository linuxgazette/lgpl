---
title: Krótkie Wprowadzenie do Biblioteki **kunf**
date: 2022-02-09
---

* Autor: Marc Welz
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue16/library.html

---

## Dlaczego?

Biblioteka kunf jest próbą ujednolicenia sposobu dostępu do danych
konfiguracyjnych. Aktualnie większość dużych aplikacji posiada własne
pliki konfiguracyjne - te pliki mają prawdopodobnie różną składnię i nie
mają dobrze wyspecyfikowanych funkcji. Z drugiej strony, małe programy i
skrypty nie mają w ogóle plików konfiguracyjnych - wartości zaszyte są w
samym programie, które czasami są osiągalne z linii poleceń lub poprzez
zmienne środowiskowe systemu. Cała konfiguracja wygląda nieco mało
optymalnie - może troche odstraszać początkującego użytkownika.

Biblioteka kunf usiłuje to zmienić - próbuje zarządzać danymi
konfiguracyjnymi w imieniu programu lub skryptu. Zamiast każdej
implementacji własnego parsera, aplikacja wywołuje gotową funkcje
biblioteczną (w przypadku skryptów shellowych wywoływałaby program
narzędziowy), którą następnie wprowadza do danych konfiguracyjnych.

Każdy kawałek danych konfiguracyjnych posiada rozpoznawalną nazwę (coś w
rodzaju ścieżki). Tworzy w ten sposób niezależne powiązania z danymi w
pliku konfiguracyjnym. Kiedy aplikacja żąda jakiejś danej, biblioteka
ogląda wartość i (opcjonalnie) dokonuje tłumaczenie wartości. Następnie
wartość jest zwracana do wywołującego kodu.

Dostęp taki powinien mieć przewagę, która jest konsekwentnym sposobem
dostępu do plików konfiguracyjnych - dane dla różnych aplikacji mogą byc
modyfikowane tymi samymi narzędziami, a skala ekonomiczna powinnna
umożliwiać tworzenie bardziej wyrafinowanych narzędzi do zarządzania,
które byłyby wykonalne dla pojedynczej aplikacji. Początkujący
użytkownicy nie musieliby uczyć się o lokalizacji plików.

## Jak?

Gdy [pobierzesz](http://sar8.ee.uct.ac.za/linux/kunf/download.html),
rozpakujesz (`tar -xzvf filename`) i zainstalujesz (`make ; make
install` powinno wystarczyć) bibliotekę, powinieneś mieć możliwość
używania shella i interfejsu C bez większych problemów:

W skryptach shellowych możesz używać narzędzia `kunfenv` do umieszczenia
określonego kawałka danych konfiguracyjnych w zmiennej środowiskowej. Na
przykład, szablon pliku konfiguracyjnego zawiera hasło dla zmiennej
nntpserver, która jest przechowywana jako `news:nntp:nntpserver`. Skrypt
może mieć dostęp do tej informacji z nastepującym wyrażeniem:

```sh
#!/bin/bash
# określ rezultat wywołania do kunfenv
eval `kunfenv news:nntp:nntpserver`
# Teraz mamy zmienną jako news_nntp_nntpserver
echo "My nntpserver is $news_nntp_nntpserver"
```

Programy w C mogą mieć dostęp do tej samej danej tak jak w poniższym
kodzie:

```c
#include <kunf.h>
  ...
  char *str;
  kunfig_open(NULL,KUNFIG_OPEN_STANDARD);
  str=kunfig_findvalue(3,"news","nntp","nntpserver");
  printf("My nntpserver is %s\n",str);
  kunfig_close();
```

Nie zapomnij skompilować programu z dyrektywą `-lkunf`.

Edytor pliku konfiguracyjnego może być używany do modyfikacji wartości
hasła `news:nntp:nntpserver`. Po prostu wywołaj edytor poleceniem
`kunfedit`, przejdź na dół do nntpserver (wybierz **news** ... ),
zmodyfikuj wartość (wciśnij klawisz escape do przesunięcia pola) i
zapisz zmiany (wciśnij escape kilka razy - program zapyta cię czy chcesz
zapisać plik).

## Więcej?

Istnieje [strona internetowa](http://sar8.ee.uct.ac.za/linux/kunf/),
która zawiera więcej informacji o tej bibliotece. Poza tym możesz pobrać
pakiet bezpośrednio z [ftp](ftp://sar8.ee.uct.ac.za/linux/kunf/).
Biblioteka jest wydana na licencji GNU. Możesz skontaktować się z jej
autorem: [trudny-do-spamowania-email](http://sar8.ee.uct.ac.za/linux/kunf/address.gif).

