---
title: CeBit'97, 13-19 Marzec
date: 2022-02-09
---

* Autor: Belinda Frazier
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue16/cebit.html

---

[Zdjęcia z CeBitu](http://www.linuxgazette.com/issue16/photo2.html)

---

CeBit jest największym na świecie targiem komputerowym, łączącym
wystawców i odwiedzających z wielu różnych krajów. Jeżeli wyobrazisz
sobie krajobraz z 27 halami wystawowymi i nawet większą ilością budynków
pomocniczych ze sklepami i restauracjami, a do tego dodasz 650.000 ludzi
odwiedzających CeBit przez ponad 7 dni, uświadomisz sobie czym jest
CeBIT'97. CeBIT odbywał się w Messegelande \[umlaut nad a\] w Hanowerze,
Niemcy, w dniach 13-19 marca 1997.

Były to moje pierwsze odwiedziny na CeBit i moim celem było zobaczenie
sprzedawców Linuksa i możliwość rozmowy o Linuksie ze sprzedawcami,
których oprogramowanie już pracuje pod innymi platformami Unixa.
Chciałam poza tym zobaczyć jak taka wielka wystawa komputerowa będzie
się kontrastować z największym targiem komputerowym w USA - Comdex -
organizowanym w Las Vegas, który odwiedzałam przez 7 lat.

Mój pierwszy przystanek był w Hall 11, gdzie wizytowałam Calderę Inc.
Stoisko Caldery było łatwo rozpoznawalne jako stoisko z Linuksem z
powodu siedzącego na jednym z monitorów pingwina "Tux" (właściwie,
wypchany Tux). Za każdym razem, gdy je odwiedzałam, przy stoisku Caldery
było zawsze tłoczno.

Zwiedzający byli zainteresowaniu OpenLinux Caldery i uzyskiwali
informacje na temat Linuksa i produktów Linuksa. Rozdanie 1500 *Linux
Journal Buyer's Guides* przez Caldere i pokrewne stoiska wydawały się
poza tym hitem podczas CeBitu. Poza tym Caldera dostarczała informacji
na temat OpenDOS 7.01, który jest darmową aplikacją do niekomercyjnego i
edukacyjnego używania. Personel stoiska Caldery rozmawiał o ostatnich
zapowiedziach, takich jak nadchodzącego wydania oprogramowania Netscape
i StarOffice 3.1 dla OpenLinux.

Niemiecka stacja telewizyjna, Bayerischer Rundfunk, sfilmowała krótki
pokaz Linuksa na stoisku Caldery. Reżyser Jurgend Plate rozgrzewał się
kilka chwil podczas, gdy ekipa filmowa ustawiała sprzęt. Przed
rozpoczęciem filmowania i moim przedstawieniu się jako Zastępca Wydawcy
Linux Journal, Jurgend ryknął na mnie, że LJ był "das beste Magazin auf
der Welt\!". Sebastian Hetze z LunetIX powiedział mi, że Jurgend Plate
ekscytował się Linuksem od lat i dlatego jego entuzjazm o Linuksie był
prawdziwy.

Mój drugi linuksowy przystanek był na wielkim stoisku Star Office w Hall
2, gdzie demonstrowano wiele różnych portów StarOffice na OpenLinux.

Na trzecim linuksowym przystanku, wielkim stoisku Software AG, był
drogowskaz z napisem Datenbanktechnologie i drugi wskazujący na dół
"ADABAS & LINUX". Na stacji roboczej z Caldera OpenLinux Base dumnie
siedział Tux. Nathan Guinn wręczył mi darmową kopię bazy danych SQL
ADABAS LunetIX w wersji jednoużytkownikowej, którą dałam później
redaktorowi Linux Journal.

Czwartą firmą związana z Linuksem była NAG Ltd, która między innymi
produktami, dostarczała informacji o jej kompilatorze pod Linuksa -
Linux Fortran 90 Compiler

Inne firmy takie jak LST Software GmBH czy LunetIX miały reprezentantów
na pokazie, w większości związanych ze stoiskiem Caldery.

Było tam kilka reportaży prasowych o tematyce Linuksa. W specjalnej
sekcji gazetowej CeBit nazywającej się "COMPUTER & KOMMUNIKATION" był
pełen artykuł zatytułowany "Linux schultert Microsoft-Anwendungen",
który relacjonował możliwości uruchamiania Aplikacji Microsoftu pod
Linuksem używając Windows Binary Application Interface (WABI).

W sumie, CeBit był pouczającym, tłocznym pokazem. Następnym razem
powinnam spróbować odwiedzić CeBit bez kuli, którą miałam jako rezultat
zwichnięcia kostki w nodze. Poza tym powinnam wspomnieć o kolorowych
pokazach i przedstawieniach w niektórych stoiskach, zawierających
historie muzyczne (D2-Musical) z "Princess Digital, the Queen of the
World", akrobacje w VIAG Interkom i wiele wystepów kabaretowych, które
dodają zabawy, kolorów i rozrywki podczas CeBitu.

