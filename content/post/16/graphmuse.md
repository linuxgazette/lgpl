---
title: Graficzne przemyslenia
date: 2022-02-09
---

* Autor: John M. Fisk
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue16/gm.html

---

![](/post/16/misc/gm3.gif)


**muse:**

1.  *v;* rozmyślanie o czymś
2.  *n;* \[ fr. Każda z dziewięciu boskich sióstr, opiekunek sztuk
    pięknych i nauki w mitologi greckiej \]: źródło inspiracji

Witaj w dziale Graficzne Przemyślenia! Dlaczego "muse"? Cóż, pomijając
"siostrzany" aspekt, powyższa definicja dobrze opisuje moje własne
zainteresowania grafiką komputerową: pozwala mi na głębokie przemyślenia
i jest codziennym źródłem inspiracji.

Ta kolumna poświęcona jest używaniu, tworzeniu, dystrybucji i dyskusji 
o graficznych narzędziach w systemie Linuks.

> Przykro mi z powodu braków w Przemyśleniach z tego miesiąca. Moja praca 
> spowodowała brak czasu. Poza tym, nie jestem do końca pewien swojej wiedzy 
> o RenderMan Shaders, o którym czuję, że mógłbym zrobić dobry temat w tym 
> miesiącu. Tak więc umieszczę drugą część w 3 seriach w jednym miesiącu. 
> *Będę* robił następne dwa artykuły, potrzebuje tylko trochę więcej czasu. 
> Poza tym przez cały czas będę robił przegląd HF-Lab. Nie jestem pewien czy 
> wskazówki POV-Ray zrobię samodzielnie. Zobaczę czy mogę porozmawiać z kimś 
> z listy pocztowej IRTC-L o napisaniu na ten temat. Nie używałem POV-Ray 3.0 
> nawet przez chwilę. Moja uwaga skupiona była na BMRT.

> Tak więc wszystko co tu się znajduje to kilka zapowiedzi pozyskanych 
> z różnych grup dyskusyjnych i informacje, które były przekazane 
> mi bezpośrednio.


## Graphics Mews

Zastrzeżenie: Przed dalszą dyskusją, powinienem zaznaczyć, że
każdy z niusów umieszczonych w tej sekcji jest tym czym jest - niusem.
Trafiłem na nie na pewnych listach pocztowych, grupach dyskusyjnych lub
poprzez otrzymanie maila od kogoś. Nie mam potrzeby podpisywania się pod
tymi produktami (niektóre z nich mogą być komercyjne), po prostu
informuję was o tym co usłyszałem o nich w zeszłym miesiącu.

### Gifmap - nawigator obrazków

Gifmap jest pakietem, który pozwala na tworzenie kolekcji obrazu
dostępnego w sieci. Rekursywnie przechodzi przez drzewo katalogów,
budując strony HTML, mapy plików graficznych i map graficznych
klient/serwer, co pozwala użytkownikowi na nawigowanie kolekcji miniatur
obrazków (coś podobnego do Visual Schnauzer w xv) i zaznaczanie obrazków
do przeglądania za pomocą kliknięcia myszką. Gifmap dostępny jest na
[ftp://ftp.wizards.dupont.com/pub/ImageMagick/gifmap](ftp://ftp.wizards.dupont.com/pub/ImageMagick/gifmap)
lub na swojej stronie domowej
[http://www.cyberramp.net/\~bfriesen/gifmap/](http://www.cyberramp.net/~bfriesen/gifmap/).

Witryna Gifmap zawiera pewne przykładowe strony, które możesz przejrzeć
żeby zapoznać się z tym co Gifmap może zrobić. Poza tym zawiera
dokumentację do gifmap. Gifmap jest napisany w PERLu i jest kompatybilny
z wersją PERL 4 i 5. Gifmap używa pakietu ImageMagic i dlatego wymagana
jest wcześniejsza instalacja ImageMagic. Wersja rekomendowana ImageMagic
to 3.8.0 lub nowsza.

### Odtwarzacz plików MPEG v0.2

Krótka zapowiedź tego pakietu została opublikowana na
[comp.os.linux.announce](https://web.archive.org/web/20030813200108/news:comp.os.linux.announce)
informując, że program może pracować na maszynach klasy Pentium 60 z 32
MB. Nie wiem dlaczego nie miałby pracować na innych systemach, ale tak
brzmi informacja. 

Odtwarzacz ten obsługuje kodowanie MPEG 1,2 i 3 oraz pliki Wave, a także 
używa pthreads (w związku z tym potrzebuje libpthread.so). Sprawdź
[http://adam.kaist.ac.kr/\~jwj95/](http://adam.kaist.ac.kr/~jwj95/)
lub [ftp://sunsite.unc.edu/pub/Linux/apps/sound/splay-0.2.tar.gz](ftp://sunsite.unc.edu/pub/Linux/apps/sound/splay-0.2.tar.gz).

### Microform opublikował pakiet VARKON

VARKON jest wysokopoziomowym narzędziem projektowym dla CAD i
aplikacji inżynierskich, stworzonym przez Microform ze Szwecji. Pierwszy
raz był on raportowany w ostatnich Przemyśleniach Graficznych. Microform
wprowadził nową wersję pakietu 1.14F i dodał nowe demo aplikacji. Nowa
wersja dostępna jest na: [http://www.microform.se/](http://www.microform.se/).

### MpegTV Player 1.0

MpegTV Player 1.0 jest oprogramowaniem do odtwarzania w czasie rzeczywistym 
plików MPEG z synchronizacją audio.

To większe wydanie ma wiele udoskonaleń w stosunku do wcześniejszych wersji, 
włączając w to większą stabilność, lepszą jakość obrazu, lepszą korekcję 
błędów, poprawiony graficzny interfejs użytkownika i nowe właściwości.

* Wysokie osiągi na Pentium (25 klatek na sekundę na P6-200)
* Obsługa wyświetlania 8, 16 i 24 bitowego koloru.
* Wysoka jakość dźwięku i wideo.
* Szybki swobodny dostęp
* Przechwytywanie klatek
* Wykorzystanie na wieloprocesorowej architekturze
* Dobra obsługa błędów
* Praca w środowisku strumieni wideo i audio
* Graficzny interfejs podobny do VCR (używając biblioteki Xforms)
* Graficzny interfejs może być dostrajany
* Player może być kontrolowany przez jakąkolwiek aplikację poprzez proste API

MpegTV jest aplikacją płatną, ale darmowe demo dostępne jest na
[http://www.mpegtv.com/download.html](http://www.mpegtv.com/download.html).
Więcej informacji można uzyskać na
[http://www.mpegtv.com/player.html](http://www.mpegtv.com/player.html).

### Sterownik skanera GS4500 został zaktualizowany do wersji 2.0

Sterowniki skanera GS4500 jest sterownikiem urządzenia (ładowalny
moduł) dla skanerów Genius GS4500 i GS4500A (a także prawdopodobnie
GS4000). Wersja 2.0 zawiera wiele poprawek w obsłudze GS4500A. Poza tym
usunięto w niej wiele poważnych błędów. Tak więc wszyscy posiadacze
jądra wersji 2.0.x powinni zauktualizować sterownik. (jeżeli cały czas
używasz kernela 1.2.x zostań przy wersji 1.4\!)

Ponadto zawarto w nim zmodyfikowaną wersję xscan. Tak jak sugeruje
nazwa narzędzie to pozwala skanować z poziomu X11 na skanerach GS4500.
Sterownik można pobrać stąd: 
[http://swt-www.informatik.uni-hamburg.de/\~1willamo/linux.html](http://swt-www.informatik.uni-hamburg.de/~1willamo/linux.html).
Niedługo powinien być poza tym dostępny na Sunsite w tsx-11

### Jeszcze jedna publikacja ImageMagick - 3.8.3.

Brak słów z na temat tej wersji. Miło widzieć, że coś się w tym
dobrym narzędziu dzieje. Jestem trochę zaskoczony czy miesięczne wersje
są naprawdę potrzebne.

### Czy wiedziałeś że...?

John Bradley posiada teraz oficjalna stronę programu xv pod adresem:
[http://www.trilon.com/xv/](http://www.trilon.com/xv/).
Nie jest jeszcze doskonała, zawiera źródła i linki do paczy, ale
prawdopodobnie niedługo to się zmieni.


## Musings

W tym miesiącu nie ma Przemyśleń. Choć będę miał kilka rzeczy w przyszłym 
miesiącu. Obiecuję.

## Zasoby

Następujące linki, dają punkt wyjścia w znalezieniu obszernych
informacji o grafice komputerowej i ogólnie multimediach w systemie
Linuks. Jeżeli masz jakieś specyficzne informacje dla mnie, dodam je na
moje inne strony lub możesz skontaktować się z webmasterami innych
witryn internetowych. Z pewnością dodam tu inne ogólne opisy, ale
specyficzne informacje o aplikacji lub miejscu są dostępne tutaj, a nie
wymienione w tej sekcji:

[Linux Graphics mini-Howto](http://www.csn.net/~mjhammel/linux-graphics-howto.html)  
[Graficzne Narzędzia UNIXa](http://www.csn.net/~mjhammel/povray/povray.html)  
[Multimedia w Linuksie](http://www.digiserve.com/ar/linux-snd/)

Niektóre z List Mailingowych i Grup Dyskusyjnych, które mam na oku i
skąd pobieram wiele informacji do tej kolumny:

[Lista Pocztowa Użytkowników i Projektantów Gimpa](http://www.xcf.berkeley.edu/~gimp/)  
[Lista dyskusyjna IRTC-L](http://www.povvray.org/irtc)  
[comp.graphics.rendering.raytracing](news:comp.graphics.rendering.raytracing)  
[comp.graphics.rendering.renderman](news:comp.graphics.rendering.renderman)  
[comp.os.linux.announce](news:comp.os.linux.announce)  

