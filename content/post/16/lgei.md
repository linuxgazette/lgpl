---
title: Wywiad LGEI z Redaktorem *Linux Gazette*
date: 2022-02-09
---

* Autor: Francesco De Carlo
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue16/lgei.html

---

**Francesco**: Kiedy i dlaczego SSC zdecydowało się publikować *Linux
Gazette* w obecnej formie? Oryginalnie, *LG* było redagowane przez Johna
M. Fiska, jedynie jako dodatkowa działalność.

**Margie**: Latem 1996 roku, John Fisk zadecydował, że nie nie jest już
w stanie utrzymywać *Linux Gazette* w formie na jaką gazeta zasługiwała.
*LG* stał się bardzo popularny, a czytelnicy chcieli aby pojawiał się w
comiesięcznym cyklu. Pomiędzy szkołą a pracą John poprostu nie miał
czasu na robienie tego, tak więc rozpoczął poszukiwania kogoś, kto
mógłby tym się zająć. My odpowiedzieliśmy, a on zaakceptował nas jako
odpowiednich ludzi do kontunuowania wydawania *LG*.

**Margie**: SSC odpowiedziało Johnowi, ponieważ zawsze czuliśmy, że
*Linux Gazette* był szanowany i cenny dla społeczności Linuksa. Nie
chcieliśmy patrzeć na to, że ktoś przejmie *LG* i zrobi z niej
komercyjną gazetę. Obiecaliśmy Johnowi, że *LG* pozostanie darmowe i
tak właśnie jest.

**Francesco**: Jaki rodzaj powiązań ma *LG* z jej "wielkim bratem"
*Linux Journal*? Jakieś wymiany artykułów, pisarzy, ...?

**Margie**: Tak, *Linux Gazette* i *Linux Journal* mają wiele wspólnego.
Od 1 lutego tego roku jestem Redaktorem *Linux Journal* i *Linux
Gazette*. Każdego miesiąca wykorzystujemy artykuły z *LG* w *Linux
Journal* i sporadycznie będę wykorzystywał artykuły z *LJ* w *LG* --
zazwyczaj te o konferencjach i innych okolicznościowych wydarzeniach
dotyczących Linuksa. Tak samo mam autorów piszących dla obu magazynów, w
szczególności współpracowników kolumn w *LG*: Larry Ayers, John Fisk i
Michael Hammel. "Odpowiadacz" *Linux Gazette* - Jim Dennis - zakończył
wywiad z Stronghold's Sameer Parekh, który ukaże się w sierpniowym
wydaniu *Linux Journal*.

**Francesco**: Czy autorzy piszą dla *LG* kontaktując sie przez ciebie,
czy też wysyłają artykuły do ciebie? To znaczy: czy przygotowujesz listę
tematów, które pojawią się w nastepnym wydaniu *LG*, czy też użytkownicy
mogą wysyłać tobie jakiekolwiek artykuły na jakiekolwiek tematy?

**Margie**: *LG* jest zarządzane bardzo przypadkowo; autorzy mogą
przysyłać do mnie artykuły na jakikolwiek temat, a ja je umieszczę.
Cokolwiek przychodzi w trakcie miesiąca jest przeznaczane do następnego
wydania. Wszystko co jest związane z Linuksem. Poza tym nie poprawiam
artykułów; są one umieszczane w takiej formie w jakiej przysłał je
autor.

**Francesco**: Czy sam produkujesz *LG*? Czy też masz prawdziwe "biuro
redakcyjne" z prawdziwymi "redaktorami" i "reporterami"? Jeżeli tak, jak
ono działa?

**Margie**: Nie mam prawdziwych redaktorów czy reporterów do pomocy.
Jestem zależny od zewnętrznych autorów społeczności Linuksa
współpracujących w tworzeniu gazety, a wspaniałą rzeczą jest to, że
oni to robią. Są miesiące, gdy mam więcej materiałów niż w innych
miesiącach (styczeń był naprawde napakowany), ale nigdy nie było za
mało. Sporo pomocy związanej z grafiką i HTML otrzymałem od webmastera
SSC - Michael Montoure. Od początku tego miesiąca mam nową asystentkę -
Amy Kukuk, która pomoże mi w tworzeniu kolumny *Trochę Nowości*, a może
i innych.

**Francesco**: Jakie są twoje plany na niedaleką przyszłość? Wprowadzisz
nową szatę graficzną *LG*, nowe artykuły, czy coś więcej?

**Margie**: Zamierzam kontynuować comiesięczne wydawanie *Linux Gazette*
i promowanie jej gdziekolwiek będę mógł. Czuję, że to jest nawet
bardziej cenne dla każdego z nowych i doświadczonych użytkowników
Linuksa.

**Margie**: Nasza szata graficzna okresowo się zmienia. W marcowym
wydaniu usunęliśmy spiralę, która powodowała sporo błędów. Michael jest
pomysłowy i przeważnie dodajemy coś co z nim wymyśliliśmy.

**Margie**: Mamy dwie nowe kolumny, które będą pojawiały sie regularnie:
"Odpowiadacz" Jima Dennisa i "Podpowiedzi dla laików: kolumna dla nowych
użytkowników" Mikea Lista. Obie kolumny są dobre dla nowych, szukających
pomocy użytkowników.

**Margie**: *Linux Gazette* jest darmowy dla czytelników, ale nie jest
darmowym dla SSC. W celu ponoszenia kosztów publikacji, *LG* rozpoczął
akceptować sponsorów. Małym podziękowaniem dla tych sponsorów będzie
umieszczenie ich na stronie głównej. Naszym pierwszym sponsorem jest
InfoMagic -- nasze podziękowania dla niego za pomoc.

**Francesco**: Co myślisz o naszym LGEI? Czy jest dobrym pomysłem i,
przede wszystkim, czy może pomóc włoskim użytkownikom Linuksa lepiej
zrozumieć ten OS?

**Margie**: Myślę że LGEI jest wspaniały\! Jest świetnym sposobem na
rozwój Linuksa w całych Włoszech. Z naszymi stałymi kolumnami i
artykułami, a także z wszystkimi radami i trikami przysyłanymi do nas,
czuję że LGEI jest nieocenionym źródłem dla Włoskich użytkowników
Linuksa, tak samo jak nasza angielska wersja jest cenna dla użytkowników
ze świata.

