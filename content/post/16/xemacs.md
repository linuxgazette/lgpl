---
title: XEmacs 19.15
date: 2022-02-09
---

* Autor: Larry Ayers
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue16/new_xemacs.html

---

Twórcy XEmacs, niezależnie od odgałęzienia GNU Emacs, wydali nową wersję
tego wszechstronnego edytora. Wersja 19.15 jest ostatnią z serii 19.xx;
w przyszłości cała uwaga będzie skupiona na serii 20.xx, która rozwijała
się równolegle z wersją 19.

Obok usunięcia wielu błedów, dużo zmian w tej wersji obejmuje
aktualizacje wielu zewnętrznych pakietów, które są załączane do edytora.
Są one całkiem spore, ważące ponad osiemnaście megabajtów, spakowane.

Wśród nowych właściwości można znaleźć:

* Rejestracja pakietu TM, który umożliwia czytanie i zapisywanie poczty i grup 
    dyskusyjnych w MIME.
* Aktualizacja wersji Gnus, W3, Vm, CC-Mode, Python-mode i Hyperbole.
* Rejestracja pakietu Auctex TeX/LateX.
* Narzędzie Custom, które próbuje standaryzować dostrajanie pakietu.
* Wiele aktualizacji dokumentacji.
* Nowa wersja hm--menu html, które teraz ma plik Info.
* Nowa wersja time.el, która podaje czas, ładowanie systemu i status
    poczty w trybie liniowym.
* Zamiana Angeftp i dired na EFS, który jest połączeniem ich obu.
* Nowy tryb Message używany przez wiele różnych pakietów poczty i grup
    dyskusyjnych.
* Wiele ulepszeń w konfiguracji i kompilacji ze źródeł.
* Aktualizacja trybu Viper (emulacja vi).
* Rozszerzenie i usunięcie błędów w wielu innych pakietach.

Członkowie zespołu XEmacs zmienili się w tym wydaniu; były opiekun Chuck
Thompson oddał pałeczkę Steveowi Baurowi. Innymi opiekunami są teraz
Martin Buchholz i Kyle Jones (autor pakietu pocztowego VM), wspólnie z
Bobem Weinerem, Chuckiem Thompsonem, Benem Wingiem i Billem Perrym
pomagając przy projekcie.

Interesujące jest to, jak projektanci różnych zewnętrznych pakietów i
samego XEmacsa próbowali zarządzać równolegle z projektowaniem Gnu
Emacs. Wiele rozszerzeń, nawet tych początkowo pisanych z myślą o
XEmacs, posiada wsparcie dla wbudowanych rozszerzeń w Gnu Emacs. Zespół
XEmacs próbuje rejestrować nowe właściwości i usuwane błędy w Gnu Emacs
do swojej wersji; ciekawi mnie czy ta przeciwność jest prawdziwa?

## Instalacja

Pakiety binarne dla wersji 19.15 dostępne są na [FTP XEmacsa](ftp://ftp.xemacs.org/xemacs),
ale jest kilka przyczyn dlaczego własna kompilacja może być
korzystniejsza. XEmacs używa skryptu konfiguracyjnego abyś mogł
dostosować makefile do swojego komputera. Jest tam wiele możliwości
ustawiania parametrów, które mogą być zachowane w skrypcie w zależności
od twoich potrzeb. Edytor wspomaga formaty JPEG, GIF, XPM i PNG. Obsługa
każdego z nich może być wyłączana. Jeżeli nie planujesz uruchamiania
przeglądarki W3, lub nie zamierzasz używać możliwości MIME w VM czy Gnus
(połączone z TM), dobrym pomysłem może być wyłączenie ich. Obsługa
dźwięku jest jeszcze innym zagadnieniem, którego nie wszyscy będą
potrzebować. Te opcjonalne właściwości nie są istotne jeżeli masz dużo
pamięci i mocną maszynę, ale nie są one naprawdę niezbędne i mogą być
wyłączane jeżeli zasoby maszyny do używania ich są niewystarczające.
Pasek zadań (a nawet obsługa X-Windows) może być wyłączona poprzez
skrypt konfiguracyjny, jeśli chcesz mieć mniej pamięciożerny plik
wykonywalny.

Będziesz potrzebował około 80 mb wolnej przestrzeni dyskowej do
kompilacji ze źródeł; na szczęście wiele z tego może być później
poprawione.

Niezaprzeczalnym faktem jest to, że instalacja XEmacs zabiera ładny
kawałek przestrzeni dyskowej. Nowy skrypt shellowy pod nazwą
*gzip-el.sh* dostarczany jest z wersją 19.15, który używa narzędzia
**find** do rekursywnego badania różnych podkatalogów LISP, kompresując
wszystkie pliki `*\*.el*`, które mają odpowiedniki skompilowanego pliku
`*\*.elc*`. Potrafi to zaoszczędzić około czternastu megabajtów\!

Jeżeli nie masz zamiaru modyfikować czy czytać tych plików Lisp `*\*.el*`,
możesz je wszystkie po prostu usunąć, ale może to być nieroztropne.
Czasami jedyna dokumentacja dla trybu lub funkcji jest schowana w jednym
z tych plików; inne mogą być modyfikowane w zależności od twoich
preferencji. Lepszą alternatywą jest stać się **rootem** i władając
*rm*, pozbywać się tylko tych pakietów Lisp, o których myślisz, że nigdy
ci nie będą potrzebne. Spróbuj choć oszczędzić katalog `/lisp/prim`, gdzie
znajdują się zasadnicze pliki rdzenia aplikacji. Nie wiem ile razy
usuwałem katalogi Energize, VMS i MH-E przy poprzednich instalacjach;
jestem pewien, że będę je w przyszłości usuwał jeszcze raz. Obiecującą
właściwością wersji 20.1 (będzie następnym większym wydaniem), jest
odseparowanie niektórych z tych pakietów od głównej dystrybucji. Pozwoli
to uzyskać rdzeń XEmacsa w oddzielnej formie, pozwalając użytkownikowi
na zdecydowanie, które rozszerzenia pobierać w zależności od jego czy
jej upodobań.

## Dostrajanie

Każdy kto używał przez dłuższy czas XEmacs, szczególnie do pisania kodu,
zapewne miał życzenie ustawienia kolorowego zaznaczania składni, które
jest miłe zarówno dla oka jak i pod względem funkcjonalności. W XEmacsie
"wygląd" jest połączeniem specyficznej czcionki i koloru do określonej
kategorii tekstu. Istnieje wiele tych definicji; każdy tryb zmierza do
posiadania kilku własnych jak i dzielonych z systemem wyglądów. Może
zająć trochę czasu ustawianie tego w swoim pliku `*\~/.emacs*`,
szczególnie jeżeli używasz czarnego tła, w którym to przypadku wiele
domyślnych kolorów nie zapewnia dostatecznego kontrastu. XEmacs 19.14
umożliwia modyfikację wyglądu poprzez polecenie *edit-faces*. Narzędzie
to działa dobrze, dodając zmiany do twojego pliku `\~/.emacs`. Niestety
format zapamiętywany przez to narzędzie jest szczególnie trudny do
czytania jeśli kiedykolwiek chciałbyś ręcznie dokonać pojedynczych
zmian; linie są bardzo długie, a składnia jest ciężka.

Per Abrahamsen, opiekun Auctexa (jeszcze jeden z dołączonych pakietów),
napisał pakiet Custom do prostego dostrajania XEmacsa i jego wielu
dodatków. Po wprowadzeniu *esc-x customize*, bufor jest widoczny z menu,
do wprowadzania nie tylko wyglądu ale też innych definiowalnych
zmiennych. Te wprowadzenia są podzielone na pakiety; wybranie jednego
skutkuje wyświetleniem podmenu. Pierwszą kategorią jest po prostu
"Emacs", która pozwala na globalne zmiany ustawień. Przy zarządzaniu
pakietami zawartymi w buforze Customize, programista musi zawrzeć kod w
LIPS. Większość dużych pakietów takich jak Gnus, czytnik poczty VM, W3
czy EFS (następca AngeFTP) zostały przystosowane w ten właśnie sposób.

Mądrze jest zrobić kopię pliku `.emacs` lub `.xemacs-options`, które
stworzyłeś przed użyciem takich auto-dostarajających narzędzi. Te
kuszące menu "Options" z wszystkimi jego wyborami radośnie nadpisze
twój plik `.xemacs-options`, jeżeli wybierzesz opcję "Save Options".
Pamiętaj, możesz zawsze wyciąć-i-wkleić dane z wygenerowanego pliku do
swojego prawdziwego pliku konfiguracyjnego, a następnie przesunąć go z
powrotem. Pakiet **Custom** jest bardziej "przebaczalny": dodaje on
swoje rezultaty na koniec twojego pliku `.emacs`. Zauważyłem, że często
gdy pakiet XEmacs, taki jak **Custom** lub **W3** dodaje coś do twojego
pliku init, wstawi na dole kilka linii przed zapisywaniem tych linii.
Jeżeli spojrzysz z ciekawości do tego pliku, żeby zobaczyć jakie
zostały zrobione zmiany, przewiń na koniec pliku; jest łatwo pominąć
dodatek jeżeli ten ukryje się na dole wśród zbędnych pustych linii,
które XEmacs dodał na koniec pliku.

Jedną z technik która jest użyteczna przy optymalizacji XEmacs, Fvm2 lub
jakiegokolwiek złożonego kawałka oprogramowania Linuksa jest
przyjmowanie różnych tożsamości. Po prostu stwórz nowego użytkownika (z
`adduser` lub zamiennikiem) i zaloguj się na nowe konto. Tym sposobem
masz czyste pole i możesz żywiołowo modyfikować, wycinać i wklejać,
wiedząc, że w każdej chwili możesz wrócić no swojego normalnego konta
jeżeli coś pokręcisz. Przykładowy plik `.emacs`, który znajduje się w
podkatalogu `/etc` dystrybucji XEmacs może być dobrym punktem wyjścia,
szczególnie jeżeli jesteś początkujący w edytorach typu Emacs.

## Rozmaitości

Dla użytkowników którzy uruchamiają XEmacs w odcieniach szarości lub z
limitowanym kolorem, zespół XEmacs dołączył ikony paska narzędzi, które
są raczej proste. Podejrzewam, że większość użytkowników XEmacs wyłącza
pasek narzędzi (polecenia z klawiatury są szybsze), ale jeżeli
chcielibyście zamienić ikony, które są pięknie zaprojektowane, kolorowe
i bardzo stylowe, [FTP AfterStep](ftp://afterstep.foo.net/pub/AfterStep/mods)
ma ich ustawienia w pliku **NeXT.XEmacs.tar.gz**. Mogą być one wrzucone
bezpośrednio do katalogu \[XEmacs-root\]/etc/toolbar, nadpisując stare
pliki. Tutaj znajduje się zbiorczy zrzut ekranu:

![Baricon](/post/16/misc/baricon.gif)

---

Dokumentacja XEmacsa jest obszerna, ale jest w nim zawartych tak wiele
nieznanych trybów i właściwości, że udokumentowanie wszystkiego dodałoby
megabajty w dystrybucji (plus rzeczy robione przez ochotników!).
Będziesz zaskoczony tym co możesz znaleźć w katalogach plików Lisp
podczas ich przeglądania. Jako przykład podam to na co natrafiłem
pewnego dnia w katalogu /lisp/modes, a co nazywało się `xpm-mode.el`. Z
ciekawości, załadowałem ten plik do XEmacsa i zobaczyłem, że działa on w
trybie kolorowania przy bezpośredniej edycji plików ikon xpm. Jest to
interesujący tryb, ale wcześniej o nim nie słyszałem; został dodany do
XEmacsa przez Joego Rumseya i Richa Williamsa w 1995 roku. Tutaj jest
przykładowe okno:

![Xpm-Mode Window](/post/16/misc/xpm_mode.gif)

Wszystkie nieznane tryby i pakiety schowane są w podkatalogach *lisp*;
wyszukiwanie różnymi słowami kluczowymi ujawni niektóre interesujące
pliki.

## Wnioski

Byłem posiadaczem ostatniego stadium XEmacsa wersji beta i jestem pod
wrażeniem ilością pracy włożonej w połączenie tego dużego, złożonego
pakietu. Projektanci i beta testerzy zasłużyli na uznanie ich wysiłków.

Jeżeli chciałbyś wypróbować pakiet, źródła są aktualnie dostępne na
[stronie domowej XEmacs](ftp://ftp.xemacs.org/pub/xemacs).
Ta strona będzie prawdopodobnie obciążona przez pierwsze tygodnie po
wydaniu nowej wersji; jeżeli nie możesz się zalogować, zostanie
wyświetlona lista mirrorów. Jeżeli nie będziesz raczej pobierał całego
zarchiwizowanego pliku, po prostu poczekaj kilka tygodni, a jestem
pewien, że program ukaże się w wielu różnych dystrybucjach na FTP.

