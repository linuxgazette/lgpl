---
title: Rozwiązanie dynamicznego IP używając Internetowego Konta Geocities
date: 2022-02-09
---

* Autor: Henry H Lu
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue16/dyn.html

---

Od czasu, gdy opublikowałem artykuł "Ustawianie Dynamicznego IP Sewera
Web" w 10 wydaniu Linux Gazette, straciłem wszystkie darmowe szkolne
konta webowe. Ponieważ potrzebuje stałej strony internetowej połączonej
z linuksowym dynamicznym serwerem webowym, byłem szczęśliwy, gdy
znalazłem darmowe konto webowe na Geocities, które może być używane z
niewielkimi zmianami konfiguracyjnymi. Darmowe konto webowe Geocities z
2 MB przestrzenia dyskową i darmowym adresem email można znaleźć na:
[http://www.geocities.com/](https://web.archive.org/web/20030813200108/http://www.geocities.com/).

Stronę internetową na Geocities można aktualizować poprzez ftp. Niemniej
jednak, procedura przesyłu poprzez ftp na Geocities wymaga aby pliki
\*.html były przesyłane w trybie ascii, a \*.jpg, \*.tgz w trybie
binarnym (binary mode). Odkryłem, że gdy używam nieprawidłowego trybu
przesyłu, strona internetowa nie może być zauktualizowana. Aktualizacja
strony (lub jej nadpisanie) trwała około 10 minut po przesłaniu plików
na ftp, tak więc musisz cierpliwie czekać na rezultaty na twoim koncie
Geocities.

Poniżej znajduje się wycinek ze skryptów w plikach web\_up, web\_down:

web\_up:

```sh
if echo -e "ascii\\ncd /pub/homestead\\nput up.html dynamic.html" \\

| /usr/bin/ftp -v geocities
```

web\_down:

```sh
if echo -e "ascii\\ncd /pub/homestead\\nput down.html dynamic.html" \\

| /usr/bin/ftp -v geocities
```

Pliki źródłowe takie jak ppp-up cz ppp-down też są aktualizowane do
odzwierciedlenia zmiany.

Następujące zdania mogą być dodane do pliku /etc/ppp/ppp-up, w celu
użycia emaila do poinformowania nas o aktualnym adresie IP swojej
maszyny linuksowej:

```sh
mail -s "$4" fasta@geocities.com \< /etc/add
```

Na koniec, chociaż nie jest to tak wygodne jak typowe unixowe konto
shellowe do aktualizacji strony internetowej używając darmowego konta na
Geocities, to daje nam dobry "mostek" do naszego dynamicznego serwera
webowego w domu przy zerowym koszcie. Jeżeli chcesz bliżej zapoznać się
z ideą dynamiczego serwera webowego, przeczytaj mój oryginalny artykuł w
wydaniu 10 i sprawdź moją nową stronę internetową do aktualizacji kodu
źródłowego.

