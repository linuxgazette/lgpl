---
title: Informacje dla początkujących 
date: 2022-02-09
---

* Autor: Mike List
* Tłumaczył: Artur Szymański
* Original text: hhttps://linuxgazette.net/issue16/clueless.html

---

**Witaj w 3 odcinku Podpowiedzi dla laików: Kolumny dla Nowych
Użytkowników.**

**Dzięki za zachęcające e-maile. W odpowiedzi na kilka próśb,
przedstawiam trochę informacji które wam pomogą.**

-----

## Wielozadaniowość

Jeżeli jesteś obeznany z innymi systemami "okienkowymi", może zdajesz
sobie sprawę z idei wielozadaniowości. Używanie pojedynczego komputera
do jednoczesnego uruchamiania kilku aplikacji jest naprawdę pożądaną
cechą systemu operacyjnego.

Jest to dość oczywiste w przypadku wykonania tego w środowisku
"okienkowym", ale w przypadku trybu tekstowego (shell) już takie
oczywiste nie jest. Istnieje tutaj kilka szczegółów.

Gdy uruchamiasz program w shell, możesz go zatrzymać przez
wprowadzenie:

```
Ctrl-Z
```

Po czym wrócisz do shella. Następnie wpisz:

```sh
bg
```

który uruchomi ten program lub zadanie w tle ((b)ack(g)round) i pozwoli
ci na uruchomienie innego zadania podczas, gdy kompiluje się kernel, bez
potrzeby zmiany na inny VC. Prawdopodobnie wiesz, że możesz zmienić VC
używając

```
Alt-F2
```

do F6. Każdy z nich może być używany w sposób, który opisałem do tego
stopnia, że możesz zapchać system jeżeli nie będziesz zbytnio uważny.
Jeżeli jesteś naprawdę pobudliwy mógłbyś nawet zapomnieć o wszystkim co
uruchomiłeś. Spokojnie, możesz wszystko znaleźć poprzez wprowadzenie
polecenia:

```sh
jobs
```

które pokaże wszystkie uruchomione zadania w tle, tak jak polecenie:

```sh
ps
```

pokazujące wszystkie procesy, które używają twojego cennego CPU i
pamięci.

## Mount

Kiedy uruchamiasz Linuksa, twój system plików, albo raczej twoje dyski
twarde muszą być zamontowane, tak żeby system plików mógł czytać i
działać. Twój napęd dyskietek, napęd taśmowy lub CDROM nie mogą być
automatycznie montowane, tak więc możesz potrzebować narzędzia
montującego. Na przykład:

```sh
mount -t ext2 /dev/fd0 /mnt  
```

lub

```sh
mount -t msdos /dev/fd0 /mnt
```

zamontuje twój napęd dyskietek jako odpowiednik dosowego a: do katalogu
/mnt, z którego masz dostęp do plików na dyskietce. W pierwszym
przykładzie, katalog /mnt może być czytany w systemie plików ext2,
podczas gdy w drugim przypadku dyskietka czytana jest w formacie msdos.
Żeby obejrzeć pliki na dyskietce, która teraz jest jako /mnt możesz
wprowadzić polecenie:

```sh
cd /mnt
```

następnie,

```sh
ls
```

lub

```sh
less nazwa_pliku
```

W podobny sposób możesz montować swoje inne napędy dyskietek, taśmy,
CDROM czy innego rodzaju napędy "odczyt-zapis". Urządzenia te mogą być
odmontowane poprzez użycie polecenia:

```sh
umount /dev/fd0  lub /dev/cokolwiekzamontowano
```

## Oszczędzanie czasu....

Jest tu kilka wskazówek, które spowodują, że twoje życie z Linuksem
stanie się trochę łatwiejsze.

Kiedy pierwszy raz logujesz się do systemu, są tam jakieś polecenia
które używają opcjonalnych przełączników, o których możesz nie wiedzieć
lub nie być ich pewnym. Wprowadziłeś może polecenia, których nie
pamiętasz po wciśnięciu Enter. Żeby wprowadzić je jeszcze raz, bez
wprowadzania całego polecenia, wciśnij po prostu "strzałka górna", która
wróci do poprzedniego polecenia, tak więc możesz wrócić na miejsce
zbrodni i zmienić błędne lub zapomniane znaki. W przypadku, gdy kilka
razy wciśniesz enter, możesz wrócić tym sposobem do kilku poprzednich
poleceń, które wprowadzałeś.

Aby wrócić do katalogu, który przed chwila odwiedziłeś lub do skanowania
podkatalogów, możesz użyć:

```sh
cd -
```

Przejście ze swojego katalogu domowego /home do katalogu głównego:

```sh
cd /
```

Obejrzenie najwyższego poziomu każdego katalogu, na przykład:

```sh
cd usr
```

a następnie:

```sh
ls
```

Jeżeli nie znalazłeś tego czego szukałeś, po prostu użyj:

```sh
cd -
```

a znajdziesz się znowu w katalogu /. Niestety możesz zagłębić się tylko
w jeden poziom, ale i tak jest to użyteczne w momencie, gdy instalujesz
pakiety źródłowe i chcesz sprawdzić zawartość każdego podfolderu.
Czasami, przynajmniej na początku, możesz nie wiedzieć jak zatrzymać
uruchomiony program lub proces, który powoli pożera pamięć lub CPU.
Możesz wprowadzić:

```sh
ps  -a
```

żeby otrzymać listę wszystkich uruchomionych procesów, zanotuj numer pid
(Process ID) i wprowadź:

```sh
kill pid
```

na przykład

```sh
kill 2395
```

Ale jest łatwiejszy sposób. Poprzeglądaj LSM (Linux Software Map) w
poszukiwaniu narzędzi, rzeczywiście fajnym jest die-1.1. Możesz go
rozpakować do katalogu, lub użyć installpkg, dopkg, lub jakiegokolwiek
instalatora pakietów. Następnie znajdź katalog /die-1.1 i przejdź do
niego. Zawiera on kilka plików, plik źródłowy,

```sh
die11.c
```

i plik z dokumentacją

```sh
die.doc
```

Zakładając, że zaistalowałeś kompilator GCC, poprostu wprowadź:

```sh
gcc -o die die11.c
```

wciśnij enter i presto, skompilowałeś narzędzie zwane die. Teraz użyj mv
do przeniesienia programu do katalogu w swojej ścieżce, i jeżeli chcesz,
przenieś die.doc do /usr/doc lub innego katalogu, gdzie może on
przebywać ze swoimi kolegami tekstowej pomocy (ale nie do katalogu man,
bo czepną się go bez litości). Następnym razem będąc w rozterce jak
ustrzelić proces, po prostu wprowadź:

```sh
die nazwa_polecenia
```

a ten go zabije. Aby dowiedzieć się więcej o die, wprowadź:

```sh
die
```

bez argumentów a ten wyświetli ci podsumowanie poleceń których możesz użyć.


## Zastrzeżenie

Prawdopodobnie zauważyliście, że ta kolumna nie ma tak wielu rozdziałów
jak dwie poprzednie, przypuszczalnie odkąd Linuks jest naprawdę łatwym
systemem do nauki moja krzywa wiedzy nie jest tak stroma, lub może
faktem jest, że spędziłem szalone chwile próbując instalować DECvt220 na
porcie szeregowym, a ten cały czas odmawiał współpracy.

Sądzę, że popełniłem błąd mówiąc, że popełniłem błąd w poleceniu mkdir w
DOS. Kilku ludzi przysłało mi maile, że mkdir -md, rmdir -rd i kilka
innych jest synonimami w poleceniach Linuksa. Jeden człowieczek
powiedział mi, że zrobił link symboliczny do kilku komend DOSowych, tak
więc może używać ich bez potrzeby uczenia się nowych, ale podobnych,
poleceń. Chore, ale prostolinijne.

Następnym Razem - Dajcie mi znać na maila: 
[mailto:troll@net-link.net](mailto:troll@net-link.net) co chcielibyście 
tutaj zobaczyć i poproście o to, w przeciwnym wypadku po prostu napiszę 
o tym co mi sprawiało kłopoty i jak to rozwiązałem.

