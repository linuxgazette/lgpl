---
title: Na zakończenie
date: 2022-03-09
---

* Autor: John M. Fisk
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue16/lg_backpage16.html

---

## Autorzy tego miesiąca

### Larry Ayers

Larry Ayers wraz ze swoją rodziną mieszka na małej farmie w północnym
stanie Missouri; jest stolarzem, skrzypkiem i generalnie
majstrem-od-wszystkiego.

### John M. Fisk

John Fisk jest najbardziej wybitną postacią, pierwszym redaktorem *Linux
Gazette*. Po trzech latach rezydentury na chirurgi ogólnej Zepołu
Naukowego na Vanderbilt University Medical Center, John zdecydował
"odwiesić stetoskop" i kontynuować karierę w Medical Information
Management. Aktualnie jest studentem Middle Tennessee State University i
ma nadzieje uzyskać dyplom Nauk Komputerowych przed tym, zanim wstąpi w
szeregi członków Medical Informatics. W wolnym czasie, razem ze swoją
żoną, Faith wędrują po pięknych górach Great Smoky Mountains w
Tennessee. Został zapalonym fanatykiem Linuksa od czasu zainstalowania
swojego pierwszego Slackware 2.0.0 półtora roku temu.

### Michael J. Hammel

Michael J. Hammel jest inżynierem oprogramowania "z doskoku", zajmującym
się wszystkim, poczynając na przesyłaniu danych, poprzez projektanta
GUI, aż do systemów kabli -- całość bazująca na Uniksie. Jego
zainteresowania pozakomputerowe to wyścigi "5K/10K", narciarstwo,
tajskie żarcie i ogrodnictwo. Zasugerował nam, że jeśli ktoś chciałby go
bliżej poznać, powinien odwiedzić jego stronę domową
http://www.csn.net/\~mjhammel. Można tam znaleźć więcej informacji niż
może sie wydawać.

### Mike List

Mike List jest ojcem czwórki nastolatków, muzykiem, zajmuje się
drukarkami (nie laserowymi), a ostatnio poprawia "technofobów", którzy
siedzą w komputerach od kwietnia 1996, od lipca używając Linuksa.

### Henry H. Lu

Henry H. Lu posiada M.S. biofizyki Uniwersytetu Minessoty i B.S. fizyki
Uniwersytetu Nankai. Aktualnie pracuje na kontrakcie przy analizie
bioinformatycznej bazy HIV w Los Alamos National Lab w Nowym Meksyku w
USA i projektuje aplikacje i narzędzia systemowe w Javie/HTML, C/C++,
perlu i shellu (środowisko Solaris) na domowym komputerze z Linuksem lub
logując się zdalnie ze stacją roboczą w labolatorium. Dla zabawy, lubi
rozgryzać niektóre programy systemowe i sieciowe, używa Linuksa w nauce
on-line w uniwersyteckich kursach (systemy operacyjne, programowanie
systemowe, sieci) i pisze w Javie/HTML do mojej własnej strony
internetowej.

### Marc Welz

Marc mieszka w Cape Town w RPA. Uważa, że jest to jedno z
najpiękniejszych miast na świecie. Powinien pracować w swoim MS, ale
czułby się roztargniony przez Table Mountain, Linuksa lub cokolwiek
innego.



## Bez Linuksa

Dziękuję wszystkim naszym autorom, nie tylko tym wymienionym wyżej, ale
także tym, którzy uraczyli nas swoimi wskazówkami, sztuczkami oraz
sugestiami. Dziękuję również opiekunom naszych mirrorów.

Bardzo pomocna w tym miesiącu była Amy Kukok, która zajęła się
organizajcą działów Trochę Nowości, Dwugroszowe Sztuczki i Odpowiedzi
Mądrali. Każdego miesiąca będę jej przekazywał więcej materiałów.

Miałem wielką przyjemność oglądać ponownie film "Gwiezdne Wojny". Filmy
o kosmosie najlepiej ogląda się w kinie. Byłem zdumiony tym, że pamiętam
każdą scenę z czasu, gdy oglądałem go po raz pierwszy. Miło było widzieć
jak wiele dzieci cieszyło się sagą, oglądając ją pierwszy raz na dużym
ekranie. Riley i ja mieliśmy sporo zabawy z tym, kto pierwszy rozpozna
kolejną scenę (trącając się łokciami). Myślę, że zrobili kawał dobrej
roboty wstawiając nowe sceny, nie zakłucając przy tym ogólnego
widowiska. Cały czas dziwie się, jak ludzie z Tatooie utrzymywali czyste
ulice ze stadem dinozaurów?

Miłej zabawy!

