---
title: News Bytes
date: 2022-02-09
---

* Autor: John M. Fisk
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue16/lg_bytes16.html

---


## Nowości Ogólnie

### Forum Sprzętowe w Dallas, Texas

Czytelnicy z Dallas, stanu Texas mogą być zainteresowani dwoma forami
dotyczącymi zakupom sprzętu, którego może nie być w innych miejscach.
Pierwsze z nich to spotkanie Północnoteksańskiej Grupy Użytkowników PC
(North Texas PC Users Group). W tym miesiącu spotkanie odbywa się w
Infomart w Dallas (I-35E at Oak Lawn). Spotkanie odbywa się w pierwszą
sobotę miesiąca i otwarte jest od 8:00 rano. Wielu poważanych lokalnych
sprzedawców prezentuje programy i sprzęt jaki ma do sprzedania (kilka
miesięcy temu przestrzeń wystawowa została przeniesiona z suteryny do
większego pokoju, ponieważ w suterynie zabrakło miejsca). Ceny na
wystawie są generalnie mniejsze w stosunku do tych, które obowiązują w
sklepach tych sprzedawców i dają oni także gwarancję i pomoc. Zadzwoń do
NCTPCUG na numer ? aby dowiedzieć się, gdzie odbędzie się następne
spotkanie. I zatrzymaj się przy stoisku lokalnej Grupy Użytkowników
Linuksa i powiedz im cześć, lub poproś ich o darmowe zainstalowanie
Linuksa na nowozakupionej maszynie.

Inne miejsce jest naprawde unikalne. Wyprzedaż Pierwszej Soboty jest
comiesięcznym pchlim targiem odbywającym się (niespodzianka) w pierwszą
sobotę każdego miesiąca. Odbywa sie pod mostem ulicy Ross Street.
Przejdź do końca Pearl Ave. Trzymając się lewej strony Ross, podążaj za
tłumem. Handel oficjalnie rozpoczyna się o 6:00 rano, ale wcześniej też
możesz przyjść. Tak samo jak wyżej, wielu sprzedawców ma własne stoiska
i oferuje te same usługi i gwarancje.

Choć te targi mogą być nie najlepszym miejscem dla początkujących,
uświadomieni kupcy mogą wrócić z każdego z nich ze sprzętem kupionym z
naprawdę znaczącym rabatem.

\-Matthew Mucker

Bedford, Texas


### COMDEX/Wiosna '97

Odwiedź

Pawilon Linuksa na COMDEX/Wiosna '97

Linux International (LI) będzie wynajmował Pawilon Linuksa na
COMDEX/Wiosna '97, który odbywa się w dniach 2-5 czerwca w Atlancie, GA.

7\. i 8. czerwca, w następny weekend po COMDEX/Wiosna '97, LI i
Entuzjaści Linuksa z Atlanty (Atlanta Linux Enthusiasts - ALE) w
porozumieniu z COMDEX, będą gospodarzami Atlanta Linux Showcase. Pokaz
ten zaprezentuje sprzedawców sprzętu, oprogramowania i usług
linuksowych, jak również konferencji na różne tematy związane z
Linuksem. Uczestnicy COMDEX będą wpuszczani na pokaz za darmo, a
rejestracja na ALS umożliwi darmowy wstęp na wystawę COMDEX.

Niektórzy ze sprzedawców obecnych na pokazie:

* Red Hat Software, Inc.
* Caldera, Inc.
* Linux Journal (Specialized Systems Consultants, Inc.)
* Linux Hardware Solutions
* Digital Equipment Corporation

Atlanta Linux Showcase odbędzie się w Inforum w centrum Atlanty, GA,
kilka bloków dalej od Georgia World Congress Center, miejsca
COMDEX/Wiosna '97. Impreza będzie otwarta od 9 rano do 3 popołudniu w
niedzielę, 8 czerwca. Konferencje będą prowadzone równolegle.

Adres Inforum: 250 Williams St., Atlanta, GA.

Więcej informacji o Atlanta Linux Showcase można znaleźć po tym adresem
internetowym:
[http:www.ale.org/showcase](https://web.archive.org/web/20030813200108/http://www.ale.org/showcase)

Informacje na temat COMDEX/Wiosna '97 dostępne pod
[http://www.comdex.com/comdex/owa/event\_home?v\_event\_id=26](https://web.archive.org/web/20030813200108/http://www.comdex.com/comdex/owa/event_home?v_event_id=26)



### Zapowiedź Sympozjum IT Horizon '97

Komunikact The Fisher Center for Information Technology and Management,
Walter A. Hass School of Business , UC Berkley:

Sympozjum IT Horizon '97 , Warsztaty i Rozwiązania

"Od NC do Network Enterprise:

Thin Clients, Robust Servers, Universal Access"

9-11 czerwca

Red Lion Hotel, San Jose, CA

Wysyłanie zgłoszeń do 4 kwietnia do Deborah Murray,
Director-Professional Training, UniForum Association, 2901 Tasman Drive,
Suite 205, Santa Clara, CA 95054 -LUB- E-mail do
[dmurray@uniforum.org](https://web.archive.org/web/20030813200108/mailto:dmurray@uniforum.org)



### Nowości o Linusie

Linus Torvalds odebrał "Lifetime Achievement Award" za prace nad
Linuksem.

Linus (jak zawsze) zaznaczył, że przyjmuje nagrodę, ale tak naprawdę
należy się ona całej społeczności deweloperów Linuksa.

Nagroda, która jest wręczana corocznie od 1983, jest uznaniem dla
indywidulanych osób i grup, których praca znacząco przyczynia się dla
otwartych systemów, lub mieli pozytywny wpływ na prace z długookresową
implikacją.

Doceniając pomysły innych, którzy to odebrali, James Gosling też dostał
nagrodę za pracę nad Javą. Linus był w dobrym towarzystwie.

Zdjęcie nagrody można obejrzeć pod adresem:

[http://daily.comdex.com/events/uf97/photos3.htm](http://daily.comdex.com/events/uf97/photos3.htm)



### Aktualizacja HOWTO

Opublikowano ważną aktualizacje Komercyjnego HOWTO Linuksa, listy
komercyjnych produktów dla Linuksa. Nowa wersja zawiera nowe kategorie,
opis wielu pakietów oprogramowania i aktualizację istniejących
materiałów.

Lista dostępna na ich głównej stronie:
[http://www.cyrius.com/tbm/Commercial-HOWTO](http://www.cyrius.com/tbm/Commercial-HOWTO)
i na mirrorach LDP na całym świecie. 



## Zapowiedzi oprogramowania


### Komunikat o Decision PCCOM8

Informacja o dostępności sterownika pod Linuksa dla karty Decision
PCCOM8

Signum Support, firma specjalizująca się w darmowym wspomaganiu
oprogramowania i Linuksie, dostała od MYDATA Automation AB, szweckiej
firmy sektora robotyki, implementacje do napisania linuksowego
sterownika dla wieloportowej karty Decision PCCOM8. Sterownik został
napisany przez Christera Winigela (wingel@signum.se) i Mikaela Cardella
(mc@signum.se). Pytania odnośnie sterownika proszę wysyłać do
pccom8@signum.se



### Komunikat o Shuttle Connection (EPST)

Signum Support, firma specjalizująca się w darmowym wspomaganiu
oprogramowania i Linuksie, dostała od MYDATA Automation AB, szweckiej
firmy sektora robotyki, implementacje do napisania linuksowego
sterownika dla interfejsu portu równoległego SCSI. Sterownik ten dla
Shuttle Connection został napisany przez Christera Weinigela
[wingel@signum.se](https://web.archive.org/web/20030813200108/mailto:wingel@signum.se)
z Signum Support

Sterownik można znaleźć na: ftp://ftp.signum.se/pub/epst/epst-0.9.diff

Zmiany zostały dokonane w stosunku do kernela w wersji 2.0.29. Sterownik
ten (prawdopodobnie) cały czas zawiera błąd i powinien być brany pod
uwagę jako program wersji ALPHA.

Proszę zauważyć, że istnieją dwa niekompatybilne urządzenia, oba
nazywają się \`Shuttle Connection'. W celu znalezienia posiadanego
modelu, spójrz na "sticker" na tyle urządzenia, gdzie powinien być napis
'EPSA' albo 'EPST'.

Ten sterownik działa z modelem EPST; jeżeli jesteś właścicielem modelu
EPSA, zerknij na http://www.torque.net/epsa.html
gdzie będziesz mógł znaleźć sterownik dla tego urządzenia.

Jakiekolwiek pytania na ten temat można wysłać do mailto:epst@signum.se


### Nowa wersja mtools

Infomujemy o nowej wersji mtools, kolekcji narzędzi do dostępu do dysków
MS-DOS z Unixa bez ich montowania.

Mtools-3.3 rozwiązało typowy model, który sprawiał polecenie
nieużywalnym.

Mtools obsługuje długie nazwy plików Win'95, dyski OS/2 Xdf i dyski 2m
(pamięć do 1992k wysoko upakowanego dysku 3 1/2). Naprawdę godną uwagi
nową właściwością (ponad 3.1) jest obsługa FAT32. Poza tym jest tam
mpartition, prosty programik partycjonujący do ustawiania napędów Zip i
Jaz na maszynach non-PC (SunOs, Solaris i HP/UX).

Aktualna wersja mtool dostępna jest w następujących miejscach:

* http://linux.wauug.org/pub/knaff/mtools
* http://www.club.innet.lu/\~year3160/mtools

a niedługo na:

* ftp://prep.ai.mit.edu/pub/gnu/mtools-3.3.src.tar.gz
* ftp://pub/Linux/utils/disk-management/mtools-3.3.src.tar.gz
* ftp://tsx-11.mit.edu/pub/linux/sources/usr.bin/mtools-3.3.src.tar.gz

Lista mailingowa mtools znajduje się pod adresem: mtools@linux.wauug.org.
W celu subskrybowania jej, wyślij wiadomość zawierającą w treści listu
tekst 'subscribe mtools' na adres majordomo@linux.wauug.org.


### Komunikat o gv 2.9.4

gv 2.9.4 jest już dostępny. gv pozwala przeglądać i otwierać dokumenty
PostScript i PDF pod X-ami poprzez dostarczenie interfejsu użytkownika
do interpretera ghostscript. Można go znaleźć na:
http://wwwthep.physik.uni-mainz.de/\~plass/gv/
albo na anonimowym ftp: ftp://thep.physik.uni-mainz.de/pub/gv/
Prosze zauważyć, że gv wywodzi się z ghostview 1.5 Tim'a Theisen'a.

gv na pewno pracuje z

* Linux (gcc 2.7.2.1)
* OpenVMS AXP (DECC 5.2,DECC 5.0)

Poza tym szczęśliwi użytkownicy podali jeszcze

* Solaris
* FreeBSD
* NetBSD
* Digital UNIX
* SunOS
* HP/UX
* Irix
* OSF/1

gv wymaga Xaw3d widget set Kaleba Keithley'a. Użytkownicy VMS znajdą
wszystko co potrzebują do instalacji widget set w miejscach wymienionych
wyżej.

Dla użytkowników Unixa pracujących w systemie nie wyposażonym w widget
set, strona http://wwwthep.physik.uni-mainz.de/\~plass/gv/Xaw3d.html
może pomóc w jego instalacji.


### SafePassage Web Proxy

Oakland, CA -- C2Net Software, Inc., i UK Web, Ltd., poinformowały o
wydaniu wersji 1.0 nowego produktu, "SafePassage Web Proxy." Produkt
ten, wyprodukowany poza granicami Stanów Zjednoczonych, pozwala na w
pełni silne, nie limitowane szyfrowanie w jakiejkolwiek standardowej
przeglądarce internetowej.

SafePassage jest ulepszonym "eksportem" przeglądarki i jest dodatkowym
produktem pracującym z każdą standardową przeglądarką internetową.
Pełniąc rolę pośrednika, lub proxy, przechwytuje słabo szyfrowane
połączenia na jej wyjściu i transferuje je na w pełni silne
szyfrowanie. "Słabe połączenie nigdy nie opuści twojego PC," wyjaśnia
Parekh, "otrzymuje połączenie nieszyfrowane, a następnie ponownie
zaszyfrowuje z wykorzystaniem silnego klucza szyfrującego."

SafePassage udostępnia bezpieczne połączenie używając silnego
szyfrowania dla każdej przeglądarki obsługującej standardowy tunel SSL,
możliwość normalnie używana przez oprogramowanie firewall. Aktualnie
działa na Windows 3.1, Windows 95 i Windows NT.

Wersje SafePassage można pobrać z serwerów w Wielkiej Brytanii:
http://stronghold.ukweb.com/safepassage
Nie jest na razie dostępna do dystrubucji w Stanach Zjednoczonych i
Kanadzie, ale krajowa wersja będzie dostępna w niedalekiej przyszłości.
Pojedyncza licencja kosztuje 49$, koszt licencji grupowej zaczyna się od
995$ dla 50 użytkowników.

### Informacja o Turbo Vision 0.3

Turbo Vision (lub w skrócie TV) jest biblioteką, która dostarcza
szkielet aplikacji. Z TV możesz w krótkim czasie napisać zorientowany
obiektowo ładny interfejs znakowy użytkownika.

TV dostepne jest w C++ i Pascalu i jest produktem Borland International.
Został zaprojektowany do uruchamiania w systemie MS-DOS, ale dziś jest
dostępny na wiele platform (utrzymywany przez niezależnych programistów)

Ten port bazuje na wersji Borland 2.0 z poprawkami.

Główne zmiany wersji 0.2 w stosunku do 0.3

* Dodano obsługę systemu operacyjnego FreeBSD
* Dodano obsługę kolorowania składni
* Poprawiono zdarzenie evMouseAuto.
* Wyeliminowano niektóre błędy.

Gdzie można pobrać bibliotekę

* ftp://sunsite.unc.edu/incoming/Linux/
* ftp://ftp.cdrom.com/pub/FreeBSD/incoming/

Jeżeli nie chcesz czekać, aż plik pojawi się w tych katalogach, możesz
pobrać jej kopie stąd: ftp://ftp.cdrom.com/pub/FreeBSD/incoming/tvision-0.3.tar.gz


### Informacja o wydaniu TeamWave Workplace 1.0

TeamWave Software Ltd. z przyjemnością informuje o wydaniu TeamWave
Workplace 1.0, internetowego produktu pozwalającego na wspólną pracę z
kolegami w czasie rzeczywistym lub asynchronicznym, używających
platformy Macintosha, Windowsa lub Unixa.

Sprawdź nas na http://www.teamwave.com/

### Wydanie Samby SMB File Server

Poinformowano o wydaniu Samba SMB File Server. Serwer zawiera obsługę
dla nazw plików w formacie zachodnioeuropejskim, umożliwiając
użytkownikom produktów Microsoft Windows(tm) z Europy Zachodniej na
zapisywanie plików w narodowym języku w ich serwerze plików UNIXa.

Choć jest to nowa drugorzędna wersja, poprawiono wiele błędów w stosunku
do poprzedniej.

Nowa wersja dostępna jest w skompresowanym formacie

ftp://samba.anu.edu.au/pub/samba/samba-1.9.16p11.tar.gz

i powinna być wkrótce dostępna na wszystkich mirrorach na całym świecie.
Więcej informacji uzyskasz na stronie domowej Samby:

http://samba.canberra.edu.au/pub/samba

### Informacja o UNIPEN-related Software Package

UPTOOLS3

Jest to komunikat o nowym wydaniu UNIPEN-related software package
(pracujący doskonale także pod Linuksem):

To oprogramowanie UNIXowe jest przeznaczone głównie dla badaczy w
rozpoznawaniu ręcznego pisma on-line. Pozwala na hierarchiczne odręczne
adnotacje w trybie on-line pochodzące z XY digitizerów lub piór
elektronicznych. Oprogramowanie nie\_jest\_przeznaczone do pracy w
trybie off-line (np. skanowanie). Celem tego oprogramowania jest
symulacja używania plików w formacie UNIPEN do badania rozpoznawania
pisma ręcznego on-line. Jest to ten sam format danych jaki jest używany
w projekcie benchmarka rozpoznawczego UNIPEN http://hwr.nici.kun.nl/unipen/

* upview-An - program X-Windows do szybkiej wizualizacji plików UNIPEN.
* upread-A - program do transformacji lub ekstraktowania danych z
jakiegokolwiek pliku UNIPEN.
* upworks-A - duży program używający Tcl/Tk na X-Windows do
przeglądania plików UNIPEN i edycji lub wprowadzania .SEGMENTS.
Można przeglądać seryjny czas znaczących sygnałów. Posiada wiele
opcji do zmiany graficznych atrybutów (takich jak kolor segmentów).
* uni2animgif-A - program do transformacji jakiegokolwiek pliku
UNIFORM do animowanego GIFa
* unipen2eps-A - program do transformacji danych z pliku UNIPEN do
obudowanego PostScriptu (encapsuled PostScript)

Wstęp do UPTOOLS3 można znaleźć na: http://hwr.nici.kun.nl/unipen/uptools3

Nowe oprogramowanie dostępne jest na ftp: ftp://ftp.nici.kun.nl:pub/UNIPEN/tools/uptools3.tar.gz


### Komunikat o Ghostscript System 0.2.0

Display Ghostscript System (System Wyświetlania Ghostscript) jest
darmową implementacją Display PostScript(tm) System. Display PostScript
System dostarcza niezależny sprzętowo model obrazkowy do wyświetlania
informacji na ekranie. Model obrazkowy używa języka PostScript, który
posiada wielkie możliwości graficzne i zwalnia programistów ze
szczegółów specyfikacji wyświetlania takich jak rozdzielczość ekranu
czy kolory.

Display Gosthscript System ma wkomponowany interpreter PostScript
(Ghostscript), bibliotekę Client i translator pswrap.

Display Ghostscript System używa architektury client/server. Aplikacja
jest połączona z biblioteką Client, która komunikuje się z interpreterem
PostScript rezydującym na serwerze. Aplikacja wykorzystuje procedury i
struktury danych w bibliotece Client, która jest niezależna od
aktualnego interpretera PostScript.

Translator pswrap pozwala ci wziąć zwyczajny program języka PostScript i
opakować go z interfejsem funkcji C pozwlając w ten sposób twoim
aplikacjom na bezpośrednie je wywoływanie. Programy pswrap są generalnie
bardziej wydajne niż takie same programy PostScript oparte wyłącznie na
procedurach biblioteki Client.

Plik dystrybucyjny umieszczono na ftp://ftp.gnustep.org/pub/gnustep

Program wymaga gcc 2.7.2.1 lub wyższej.

Plik '.tar' jest skompresowany w GNU gzip. Gzip można pobrać poprzez
anonimowy ftp w jakiejkolwiek lokalizacji GNU.

Informacja o FTP via email - wyślij email na ftpmail@decwrl.dec.com
bez tematu i z dwoma liniami w treści listu - jedna 'help', druga
'quit'.

Najnowsze snapshoty (niekoniecznie testowane) biblioteki będą
umieszczane na ftp://alpha.gnu.ai.mit.edu/gnu/gnustep


### Jest już dostępna wtyczka GA NExS Spreadsheet

X Engineering Software Systems (XESS Corp.) informuje o dostępności
wtyczki algorytmu genetycznego (genetic algorithm -GA) dla arkusza
kalkulacyjnego NExS. Ludzie zainteresowani tą wtyczką mogą pobrać kod
źródłowy i instrukcje PostScript z www.xess.com.
30-dniowa darmowa wersja Arkusza kalkulacyjnego NExS i nowy API
conNExions-BETA mogą być pobrane również dla platform HP/UX, AIX,
Digital UNIX, SunOS, Solaris i Linux.

Algorytm genetyczny (GA) rozwiązuje problemy optymalizacyjne poprzez
modelowanie potencjalnych rozwiązań jako chromosomów, które mogą
wzajemnie się rozmnażać do produkcji lepszych rozwiązań poprzez
wymuszenie naturalnej selekcji.

Wtyczka GA dostarcza jedną nową funkcję NExS: @GENALG(...) która
optymalizuje funkcje "fitness" oddziałując na grupę zmiennych 1/0 w
arkuszu. Jakakolwiek funkcja NExS lub kombinacja funkcji może być
używana do specyfikacji funkcji "fitness".

Wtyczka GA oddziałuje wzajemnie z arkuszem kalkulacyjnym NExS poprzez
conNExions-BETA API. Kod źródłowy dla wtyczki jest dostępny do
modyfikacji i ulepszeń.


### Informacja o MkLinux DR2.1

Z przyjemnością informujemy o wydaniu MkLinux DR2.1. DR2.1 zawiera
wsparcie dla Power Macintosh 601/NuBus 601/PCI bus i 604/PCI bus w
systemach: Power Macintosh 6100, 7100 i 8100; 7200; 7500, 7600, 8200,
8500 i 9500. (Wsparcie dla systemów bazujących na 603 jest w
przygotowaniu ale jeszcze nie jest dostępna. DR2.1 na razie nie
obsługuje Powerbook lub większości Performas.)

DR2.1 jest naszym trzecim wydaniem deweloperskim MkLinux i pierwszym
Wydaniem zawartym w naszym Wydaniu Referencyjnym,opublikowanym przez
Prime Time Freeware (PTF). Wydanie Referencyjne MkLinux zawiera
360-stronicową książke i 2 CDROMy: Apple MkLinux DR2.1 disc i PTF's
Reference disc, z wieloma interesującymi i użytecznymi materiałami
referencyjnymi.. (Każdy z CDROM jest poza tym sprzedawany oddzielnie.)

Wydanie Referencyjne MkLinux jest dostepne poprzez zamówienie pocztowe z
PTF i innych sprzedawców a poza tym można je dostać w wielu księgarniach
technicznych jako indywidulane dyski. Skontaktuj się z Prime Time
Freeware na info@ptf.com albo odwiedź ich stronę WWW: www.ptf.com.

MkLinux dostępny jest zarówno na CDROM jak i przez anonimowy ftp:
ftp://ftp.mklinux.apple.com/
oraz na różnych mirrorach. (Cierpliwości z mirrorami; mogą potrzebować
jakiegoś czasu aby umieścić DR2.1 do pobierania\!)

W związku z wydaniem DR2.1, DR2 nie będzie już więcej dostępne i
wspomagane. Zatrzymamy informacje "Help and Support" dla DR2 na naszych
stronach, ale samo DR2 będzie usunięte z naszego serwera FTP.

Odwiedź stronę internetową: http://www.mklinux.apple.com/DR2.1
w celu dalszych informacji na temat tego wydania. Wszystkie pliki Readme
z Dystrybucji DR2.1, zawierające Release Notes (Readme First) i Opis
Instalacji (Jak Instalować MkLinux) są sklonowane na naszych stronach
WWW.


### Metro-X 3.1.5 już w sprzedaży

Metro Link dostarcza już Metro-X 3.1.5. Jest to zauktualizowana wersja
Metro-X 3.1.2 dla Linuksa, która jest komercyjnym serwerem X używanym
zamiast XFree86. Zawiera różne poprawki i obsługę dodatkowych kart:

* Diamond Stealth 64 Graphics 2200
* Diamond Stealth 64 Video VRAM V1.xx (TI 3026 DAC)
* Diamond Stealth 64 Video VRAM V3.xx (IBM DAC)
* ELSA WINNER 1000TRIO/V (TRIO64V+)
* ELSA Winner 2000AVI
* ELSA Winner 2000PRO/X (TI 3026 DAC)
* Number Nine I-128 series 2
* Toshiba Tecra 720CDT (CHIPS 65550)

Kompletna lista obsługiwanych kart dostępna jest pod: http://www.metrolink.com/products/metrox/cardlist.html

Więcej informacji w kompletnym opisie produktu: http://www.metrolink.com/products/metrox/ess.html

CENY DLA WERSJI LINUKSOWYCH:

Nowy zakup: $99

Aktualizacja wersji wcześniejszej: $69

KONTAKT: Metro Link, Inc. http://www.metrolink.com/ i sales@metrolink.com.

