---
title: Więcej rad za dwa grosze!
date: 2022-02-09
---

* Autor: John M. Fisk
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue16/lg_tips16.html

---

Wysyłaj swoje wskazówki i sztuczki w Linuksie na gazette@ssc.com

## Jak zrobić "zwrotny" FTP

Date:Sat Mar 30 14:23:24 (PST)  
From:Phil Hughes, phil@ssc.com

Wiele firm posiada firewall pomiędzy Internetem a wewnętrznymi
systemami. Jest to dobre zabezpieczenie i ma sens. Jedną z powszechnych
technik filtrowania jest uniemożliwienie dostępu poprzez firewall z
zewnątrz, ale zezwolenie wewnętrznym użytkownikom na dostęp do
większości serwisów na zewnątrz sieci.

Kiedy jestem w domu, potrzebuje rutynowo przesyłać pliki pomiędzy domem
a pracą. Ale w związku z firewallem mogę użyć ftp z pracy do domu, ale
na odwrót już nie. Oznacza to, że potrzebuje ustanowić interaktywne
połączenie (używając ssh) z mojego domu do pracy a nastepnie
zainicjować ftp z pracy do domu.

Jak narazie jest dobrze. To co nazywam "domem" składa się z różnych
lokalizacji, wszystkie połączone dialupem z wykorzystaniem jednego z
czterech ISP (providerów internetowych). Cała czwórka używa dynamicznych
adresów IP, co oznacza, że za każdym razem, gdy się łącze mam różne
adresy IP dla mojego domowego systemu. Nawet jeżeli ISP zna mój aktualny
adres IP, to serwer nazw w pracy tego nie wie.

Rozwiązaniem jest wprowadzenie adresu IP mojego domowego komputera do
polecenia ftp w pracy. Po pierwsze muszę odnaleźć swój adres IP. Aby to
zrobić, wykonuję polecenie ifconfig w moim domowym systemie:

```sh
$ /sbin/ifconfig

lo        Link encap:Local Loopback
          inet addr:127.0.0.1  Bcast:127.255.255.255  Mask:255.0.0.0
          UP BROADCAST LOOPBACK RUNNING  MTU:2000  Metric:1
          RX packets:19 errors:0 dropped:0 overruns:0
          TX packets:19 errors:0 dropped:0 overruns:0

eth0      Link encap:10Mbps Ethernet  HWaddr 02:60:8C:8F:A2:08
          inet addr:198.186.207.131  Bcast:198.186.207.255  Mask:255.255.255.0
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:969719 errors:0 dropped:0 overruns:0
          TX packets:971132 errors:0 dropped:0 overruns:0
          Interrupt:9 Base address:0x280 Memory:d8000-da000

ppp0      Link encap:Point-Point Protocol
          inet addr:206.125.79.118  P-t-P:204.157.220.30  Mask:255.255.255.0
          UP POINTOPOINT RUNNING  MTU:296  Metric:1
          RX packets:5434 errors:0 dropped:0 overruns:0
          TX packets:5545 errors:0 dropped:0 overruns:0
$
```

Adres internetowy dla interfejsu ppp0 (206.125.79.118) jest tym czego
potrzebuje. Teraz , na moim systemie w pracy wprowadzam:

```sh
$ ftp 206.125.79.118
```

a następnie ftp prosi mnie o login i hasło. Wprowadzam mój standardowy
login i hasło dla mojego domowego systemu i już jestem w ftp.

-----

## Sprawdzanie czy jesteś rootem

Date: Sun Mar 23 23:20:51 1997 (PST)  
From: Kevin Lyda, kevin@faxint.com

W marcowym wydaniu gazety Raul Miller sugerował, że najbardziej
przenośnym sposobem testowania czy jest się rootem jest \[ -w / \]. To
nie będzie działać, jeżeli twój główny system plików jest ustawiony na
"tylko do odczytu"; \[ -w /var \] będzie lepszą metodą.

kevin

-----


## XV vs. Xli

Date: Wed Mar 5 16:32:49 1997(PST)  
From: Michael Hammel, mjhammel@emass.com

Do czasu twojego stwierdzenia, ze tylko xv może wyświetlić obrazek w tle
nie znałem się na Xli (nawet go nie widziałem). xv pozwala z linni
poleceń na sporą kontrolę. W pracy, na moim komputerze, używam
następującego polecenia do wstawienia obrazka jako tła zajmującego
całą przestrzeń ekranu):

```sh
xv -root -max -quit /export/home/mjhammel/lib/images/emass3.tga
```

Obrazek wielkości 601x339 jest wyświetlany w oknie 1152x900. Gdy
oryginalny obrazek ma 24bpp, powiększenie ma bardzo dokładne szczegóły.

Michael J. Hammel

-----

## Skrypty Shella Bash

Date: Thu Mar 20 12:22;34 1997(PST)  
From: Paul Sephton, paul@inet.co.ca

Kilka lat cieszę się z powodu owocnej publikacji Linux Gazette.
Ostatnio, jeden z moich użytkowników przypadkowo wprowadził rm \*\>bak,
i odrazu zauważyłem, że coś było nie w porządku, bo usłyszałem krzyk
pochodzący z biura.

Próbując upewnić się, że ta katastrofalna sytuacja nie zdarzy się
jeszcze raz, a szczególnie w obawie o moje bębenki w uszach, spędziłem
kilka dni ćwicząc moje umiejętności w pisaniu skryptów bash i myślę, że
wykombinowałem przyzwoity mechanizm do zarządzania wersjami archiwów.

Mój stosunek odnośnie normalnych rzeczy jak aliasowanie rm lub coś w tym
stylu, jest taki, że nie zabezpieczy cię to od innych programów, które
odlinkują pliki. (Muszę jeszcze napisać program w C w stulu rm do
odlinkowania pliku:)

Podczas pisania ustawień trzech skryptów, zaświtało mi, że pomimo
istnienia niektórych dość kompleksowych narzędzi, które przeprowadzają
te same funkcje sortujące, społeczność Linuksa może być zainteresowana
tym jak ja to zrobiłem.

Choć nie jest to nic więcej jak kreatywne ćwiczenie w korzystaniu z
polecenia 'find' i tolerowanie zwyczajowych limitów restrykcji na jeden
system plików, zawarłem te trzy skrypty do przejrzenia i możliwego
włączenia ich do gazety według twojego uznania.

Nie wahaj się skontaktować ze mną, jeżeli potrzebujesz więcej
informacji.

Pozdrawiam i wielkie dzięki za gazete.  
Paul Sephton

-----

## Skrypt Shella Basha nr. 1

```sh
#!/bin/sh
if [ -z "$SAFEDEL" ];then
SAFEDEL=/u/safedel
fi

NDAYS=5                        # Wymaż pliki po 2 dniach
MAXVER=6                       # Rozpocznij nadpisywanie wersji od tego momentu
BINDIR=$SAFEDEL/bin            # Katalogi z programami
DATADIR=$SAFEDEL/data          # Gdzie prowadzą linki
LOGFILE=$BINDIR/safedel.log    # Komunikaty kieruj tutaj
ERRLOG=$BINDIR/safedel.err     # Błedy kieruj tutaj
DIRLIST=$BINDIR/safedel.dirs   # Lista znalezionych katalogów
LOCKFILE=$BINDIR/safedel.lock  # Zablokuj plik uniemożliwijąc powtórne wprowadzanie

# Przetwórz plik $1 poprzez stworzenie dowiązania symbolicznego w
# katalogu danych i wstaw plik do indeksu
process-file()
{
SRC=`dirname $1`
FNAME=`basename $1`
VERSION=0
if [ ! -d $DATADIR$SRC ]; then
mkdir -p $DATADIR$SRC
#  OWNER=`find -name $SRC -printf "%u"`
#  chown $OWNER $FNAME:$VERSION
fi
cd $DATADIR$SRC
while [ -f $FNAME:$VERSION ]; do
VERSION=$[ $VERSION + 1 ]
done
if ! ln $1 $FNAME:$VERSION 2>> $LOGFILE; then
echo "Could not link file $FNAME:$VERSION" >> $LOGFILE
return
fi
echo -e "Linked $FNAME:$VERSION \t \tin $SRC" >> $LOGFILE
return
}

# Wymaż plik
erase-file()
{
echo "Unlinking $1 $2" >> $LOGFILE
rm -f $1
FN=`echo $1 | cut -f 1 -d ':'`
if ! { echo "$ERASED" | grep "$FN" - } ; then
ERASED="$ERASED $FN"
fi
return
}
# Potrzebujemy numeru wersji do śledzenia każdego innego, tak żeby nastepny
# tworzony plik otrzymywał większy numer.
reorganise()
{
if [ -z $1 ]; then
return
fi
FN=$1
FILE_LIST=`ls $FN:* | sort -n -t: -k2`
if [ "$FILE_LIST" = ":*" ]; then
echo "All [$FN:*] files erased" >> $LOGFILE
return
fi
echo -e "File list to be moved is:\n$FILE_LIST" >>$LOGFILE
VERSION=0
for FNAME in $FILE_LIST; do
if [ "$FNAME" != "$FN:$VERSION" ]; then
  echo "Moving $FNAME $FN:$VERSION" >>$LOGFILE
  mv $FNAME "$FN:$VERSION"
  VERSION=$[ $VERSION + 1 ]
fi
done
}

# Główny kod skryptu rozpoczyna sie tutaj...

cd $BINDIR
if [ -f $LOCKFILE ]; then
exit 0
fi
touch $LOCKFILE
date >> $LOGFILE
cat $DIRLIST |
(
while read SRC ; do
if [ `echo $SRC | cut -b 1` != "#" ]; then
  echo "Finding files in $SRC" >> $LOGFILE
  echo "Point 1 ($SRC)"
  for FNAME in `find $SRC -type f -xdev -links 1 -print`; do
    process-file $FNAME
  done
fi
done
ERASED=""
echo "Point 2"
for FNAME in `find $DATADIR -type f -links 1 -ctime $NDAYS -print`; do
erase-file $FNAME "(older than $NDAYS days)"
done
echo "Point 3"
for FNAME in `find $DATADIR -type f -name "*:$MAXVER" -print`; do
FN=`echo $FNAME | cut -f 1 -d ':'`
erase-file $FN:0 "Too many versions (VERSION > $MAXVER)"
done
echo "Point 4"
for FNAME in "$ERASED"; do
reorganise $FNAME
done
) 2> $ERRLOG > /dev/null
rm -f $LOCKFILE
```

-----

## Skrypt Shella Bash nr. 2

```sh
#!/bin/sh
CURRDIR=`pwd`/
if [ -z $SAFEDEL ]; then
SAFEDEL=/u/safedel
fi

DATADIR=$SAFEDEL/data
BINDIR=$SAFEDEL/bin
cd $DATADIR$CURRDIR
if [ -z "$1" ]; then
echo
echo "Restores files unintentionally deleted"
echo
echo "Useage <salvage <filename>[:version] [dest]> from within the directory"
echo "       in which the file was deleted."
echo
echo "The following is a list of your backed up files and their versions:"
echo "  Salvageable Files:"
find . -xdev -type f -maxdepth 1 -links 1 -printf "%P\n" | column 
echo "  Files Currently in Use:"
find . -xdev -type f -maxdepth 1 -not -links 1 -printf "%P\n" | column 
else
FN=`echo "$1:end" | cut -f 1 -d ':'`
VER=`echo "$1:end" | cut -f 2 -d ':'`
EXIST=`find $CURRDIR -name "$FN"`
#  echo "[$EXIST]"
if [ -n "$EXIST" ]; then
echo "Incorrect file specification: File(s) are not deleted. ($FN)"
exit 0
fi
if [ "$VER" = "end" -o "$VER" = "*" ]; then
VER=""
fi
FILE_LIST=`find . -name "$FN:*" -printf "%f "`
FLIST=""
#  echo "FILE_LIST is $FILE_LIST"
for FNAME in $FILE_LIST; do
FN=`echo "$FNAME:end" | cut -f 1 -d ':'`
FOUND=0
#    echo "Looking for [$FN] in [$FLIST]"
for F in $FLIST; do
  if [ "$F" = "$FN" ]; then
    FOUND=1
  fi
done
if [ "$FOUND" = "0" ]; then
  FLIST="$FLIST $FN"
fi
done
#  echo "FLIST is $FLIST"
for FNAME in $FLIST; do
if [ -z "$VER" ]; then
  VERSION=0
  NEXTVER=1
  while [ -f $FNAME:$NEXTVER ]; do
    VERSION=$NEXTVER
    NEXTVER=$[ $NEXTVER + 1 ]
  done
else
  VERSION=$VER
fi

if [ ! -f $FNAME:$VERSION ]; then 
  echo "File $FNAME:$VERSION not found"
  exit 0
fi

if [ -z "$2" ]; then
  DEST=$CURRDIR$FNAME
else
  DEST=$CURRDIR$2
fi

if ln $FNAME:$VERSION $DEST 2> /dev/null; then
  echo "File $FNAME:$VERSION successfully recovered"
else
  echo "Cannot link $FNAME:$VERSION to $DEST"
fi
done
fi
```


-----

## Skrypt Shella Basha nr. 3

```sh
#!/bin/sh

if [ -z $SAFEDEL ]; then
SAFEDEL=/u/safedel
fi

BINDIR=$SAFEDEL/bin            # Katalog programów
DATADIR=$SAFEDEL/data          # Gdzie prowadzą dowiązania

# Erase a file
reorganise()
{
if [ -z $1 ]; then
return
fi
FN=`echo "$1:end" | cut -f 1 -d ':'`
FILE_LIST=`ls $FN:* | sort -n -t: -k2`
if [ "$FILE_LIST" = ":*" ]; then
echo "All [$FN:*] files erased" 
return
fi
#  echo -e "File list to be moved is:\n$FILE_LIST" 
VERSION=0
for FNAME in $FILE_LIST; do
if [ "$FNAME" != "$FN:$VERSION" ]; then
  echo "Moving $FNAME $FN:$VERSION" 
  mv $FNAME "$FN:$VERSION"
  VERSION=$[ $VERSION + 1 ]
fi
done
}

# Główny kod skryptu zaczyna się tutaj...

CURRDIR=`pwd`/

echo "Safedel: Purging extra versions in $CURRDIR"

cd $BINDIR
find $DATADIR$CURRDIR -type f -maxdepth 1 -links 1 -exec rm {} \;
for FNAME in `find $DATADIR$CURRDIR -type f -maxdepth 1 -print`; do
reorganise $FNAME
done
```

