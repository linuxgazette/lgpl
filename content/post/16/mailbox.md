---
title: Skrzynka pocztowa
date: 2022-02-09
---

* Autor: John M. Fisk
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue16/lg_mail16.html

---

Pisz do gazety na adres gazette@ssc.com


## Potrzebna pomoc -- pomysły artykułów

Date: Tue, 25 Mar 1997 16:32:30 -0600  
Subject: **świetnie**  
From: Francisco Benavides,
mailto:txmfrbg@txm.ericsson.se

Praca wykonywana przez LG jest świetna\! Co do moich pomysłów, weź pod
uwagę to, że większość znanych aplikacji przeznaczonych dla PC
(bazujących na DOS lub Windog) nie ma dedykowanych im sekcji. Dla tych
z nas (jak ja), którzy pragną niedługo przesiąść się na Linuksa, jako
dobrego systememu plików, zdolnym do uruchamiania programów dosowych bez
potrzeby rozróżniania ich pod kątem pracy w Linuksie/Uniksie.

-- Narazie/Francisco :)

-----

Date: Wed, 05 Mar 97 11:24:23 -0500  
Subject: **Prośba**  
From: Bill R. Williams, brw@etsu-tn.edu

W rzeczywistości jest to bardziej prośba niż "List do Redaktora";
niemniej jednak, możesz użyć jej jak/jeżeli ci pasuje.

Fundamentalnym elementem bezpieczeństwa jest użycie haseł "shadow".
Linux (i niektóre komercyjne un\*x\!) niekoniecznie domyślnie włączają
tą możliwość. (od dawna już używam Slackware i nie instaluje się on ze
skonfigurowanym Shadow Password Suite (SPS))

Uważam, że SPS jest absolutnie niezbędny w jakimkolwiek systemie un\*x
(Linuks), który udostępniany jest dla innych użytkowników. Innymi słowy:
nie, nie potrzebuje tego w moim domowym Linuksie, ponieważ system nie
jest dołączony do sieci i jedynie ja jeden go używam. Choć są w nim
gorsze rzeczy niż instalacja SPS, jest to zadanie, którego naprawdę się
boje. Doprowadza mnie do nerwów.

Tak więc zadaję tu pytanie tym z was, którzy mają pojęcie o różnych
dystrybucjach Linuksa: Czy jest jakaś dystrybucja pozwalająca na
instalacje Linuksa z SPS ze wszystkimi stosownymi narzędziami i innymi
wbudowanymi pakietami oprócz SPS? (takie jak sudo czy wu-ftpd.)

Pytanie związane z tematem, które nie jest takie oczywiste: czy używając
jakiejś dystrybucji -- Red Hat, Debian lub jakiejkolwiek innej --
istnieje jakieś niebezpieczeństwo z wprowadzaniem pakietów, które mogą
nie być częścią danej dystrybucji? Ponieważ nie mam doświadczenia z
czymkolwiek innym niż Slackware, nie wiem co jest wymagane w pakietach
oprogramowania używanego przez innych dostawców; Jakkolwiek zdaję sobie
sprawę, że niektórzy producenci mają narzędzia, które mogą "śledzić"
poziomy różnych komponentów. Jeżeli zainstaluje jakiś pakiet
oprogramowania, który mógł nie być częścią "zainstalowanej" dystrybucji,
jakie jest prawdopodobieństwo, że będe mógł śledzić oryginalnie
zainstalowany pakiet? Przypuśćmy, że chcę zainstalować 'Doom' z mojego
starego CD-ROMu z Slackwarem na dystrybucje Linuksa "Miranda v0.01". Czy
będe miał z tym problemy, gdy zaktualizuje moją "Mirande" do nowej
wersji? (Linus miał nową "Mirande" w styczniu\! Zauważ, że mogłoby się
to wydarzyć.;-)

I zupełnie na inny temat... Gdzie znajdę narzędzie dla XFree, które może
być użyte do PRAWIDŁOWEJ konfiguracji ustawień monitora\! Próbowałem.
Naprawdę. Za każdym razem gdy natknę się na wzmiankę w tym temacie,
czytam go, ale nieważne jak bardzo próbuje nie mogę pomieścić w swej
łepetynie o co chodzi. Dostarczony serwer może bez problemu
"rozpracować" typ karty graficznej, ale następne rzeczy związane z
monitorem i odświerzaniem i HZ i KHz i przepustowością i zegarem i... to
właśnie tam się kompletnie gubię. Potrzebuję czegoś, co pozowliłoby mi
poprostu wprowadzić wszystkie parametry dostępne w dokumentacji mojego
monitora, a program przełożyłby to na odpowiednie linie w pliku
XF86Config, w taki sposób, że startując X nie będę miał wykrzywionego
obrazu i nie będę się denerwował, że spalę kineskop. (\*westchnięcie\*)

Mam nowe Xy (v3.4?) z graficznym narzędziem do ustawiania. Trochę
lepiej. Ale w \*dalszym\* ciągu są tam tryby, które strasznie wyglądają.
"...dotknij w jednym miejscu, a bąbelki polecą w innym"

Komentarze, artykuły i/lub sugestie dotyczące wszystkiego co napisałem
od ludzi z "Linux Journal" i czytelników, spotkają się z moja wielką
wdzięcznością\!

Bill R. Williams

-----

Date: Sat, 01 Mar 1997 20:49:13 -0800  
Subject: **Głębia kolorów X Window... problem z Linear Addressing.**  
From: Nicky Wilson, benson@znet.com

Po zabawach z plikiem xf86config w celu zmuszenia Xów do wyświetlania 16
bitowego koloru przerażony przeczytałem, że z moim aktualnym sprzętem
(16 MB RAM i Cirrus Logic GL-5426) 16 bitowy kolor jest
\*niemożliwy\*... nie w związku z "niezdolnym" sprzętem, ale w związku
z pewnym limitem X Windows... problem z linear addressing. Wygląda na
to, że aby mieć 16 bitowy kolor pod Xami, musi on mieć włączony linear
addressing, który tylko wtedy pracuje gdy system ma \*nie więcej niż 14
MB RAM\*.

Coż dzielą mnie tylko 2 megabajty od 16 bitowego koloru który miałem w
Win95. Nie mogę nawet wyłączyć tych dwóch mega ("zdowngradować" mój
system, żeby pracował pod Linuksem\!) w związku z tym, że mam jedną 16
megową kość pamięci.

Musi być jakiś sposób. Miałem nadzieję pracować z moimi programami
graficznymi pod Linuksem, ale 256 kolorów po prostu nie pozwala mi na
to.

Czy ktokolwiek w Linux Gazette zna rozwiązanie? Słyszałem o stworzeniu
jakiejś dwu megowej "dziury pamięci" (?), lub o programie, który ogłupia
system w taki sposób, że ten myśli, że ma mniej RAMu niż jest w
rzeczywistości. Jakies pomysły? (zastanawiam się czy twórcy X-ów pracuja
nad tym problemem?)

Dzięki za każdą wskazówkę.  
Wasz przyjaciel,  
Nicky

-----

Date: Sat, 15 Mar 1997 03:41:04 GMT  
Subject: **Karta dzwiękowa pod Linuksem**  
From: L Hatch, tn00607@ibm.net

Po rekompilacji mojego krenela zabrałem się za zmuszenie do pracy mojej
karty dźwiękowej pod Linuksem... jedyny problem to taki, że muszę
najpierw bootować dosa, żeby ustawić kartę... karta jest ustawiana
programowo za pośrednictwem mojego autoexec.bat... jest to ESS
Audiodrive... jakieś sugestie?

Mam również inne pytanie... Chce razem połaczyć dwie maszyny, używając
modemowego połączenia dialup (wdzwanianego)... chce mieć możliwość
dzwonienia ze standardowego oprogramowania komunikacyjnego pracującego
pod dos, win, win95, itp i przełączać terminal pod kontrolę osoby na
drugim końcu w taki sposób, żeby mogła ona używać shella Linuksa w jej
programie komunikacyjnym... zarządzać tym pod dosem poprzez uzyskanie
połaczenia mdm i wykonanując ctty com2: w linii poleceń przełączyć
kontrolę na tę osobę... otrzymałaby C:\> i miałaby możliwość
wprowadzania poleceń i otrzymywania wyjścia w jej programie
komunikacyjnym... jakieś sugestie jak to zrobić pod Linuksem? Dzięki.

-----

Date: Mon, 17 Mar 1997 15:11:45 -0800  
Subject: **Głupie pytanie**  
From: Steve Arnold, sarnold@rain.org

Cześć: przeszukałem już twoją witrynę w poszukiwaniu odpowiedzi, ale w
związku z tym, że jej nie znalazłem, spytam się więc bezpośrednio:

Co to jest u diabła za pusty ekran uruchamiany domyślnie pod konsolą
(jak się nazywa, gdzie on startuje, itd)?

W starym RedHat 2.1 (kernel 1.2.13) było to wyłączane po starcie Xów,
ale w nowym Redhat 4.0 (kernel 2.0.28) cały czas wyskakuje pod Xami,
nawet gdy działa xlock lub coś podobnego.

Jaką binarkę wyłączyć i co przełaczyć aby zablokować konsolową pustą
stronę pod Xami?

Z góry dziękuje, Steve Arnold

-----

Date: Sun, 23 Mar 1997 11:55:35 -0500 (EST)  
Subject: **Linuksowe pytanie**  
From: Peter Pereira Stamford, stamford@bme.unc.edu

Cześć, jestem czytelnikiem gazety i mam pytanie, które może również
zainteresować inne osoby. Jest to mix problemu ze sprzętem i
oprogramowaniem. Zanim wysłałem tego maila, przejrzałem pobierznie
wszystkie spisy treści gazety i HOWTO Linuksa. Nie znalazłem żadnej
odpowiedzi w tych dwóch miejscach. Jeżeli jest to powszechne pytanie, a
ja przegapiłem odpowiedź, proszę o wybaczenie mi.

Wiele z systemów może zarządzać kilkoma małymi monitorami. Zamiast
kupowania nowego, większego, droższego monitora, można połaczyć dwa
monitory w jeden duży ekran.

Ponieważ mam dodatkowy monitor i karte graficzną, próbuję zainstalować
drugi monitor, aby w efekcie otrzymać ten duży ekran. Nie próbuję
wyświetlać tego samego obrazu na obu monitorach jednocześnie. O ile
rozumiem, MetroX (dołączony do mojego redhata) pozwala mi posiadać Xy
podzielone na wiele wirtualnych ekranów (wybacz mi brak oficjalnego
technicznego terminu) i oglądanie dwóch wirtualnych ekranów Xów jeden
koło drugiego na oddzielnych monitorach (jestem pewien, że inne serwery
X robia to samo). Tak więc mogę mieć otwarte różne aplikacje w każdym z
wirtualnych ekranów, unikając w ten sposób bałaganu. (próbuję być
dokładny, ponieważ próbowałem wcześniej uzyskać informację i zostałem
niezrozumiany).

Posiadam dodatkowy monitor i karte graficzną które chętnie wykorzystam
do tego celu. Ale aktualnie, gdy mam zainstalowane obie karty graficzne
nie mogę wystartować systemu. Powiedziano mi, że dzieje się tak ponieważ
ROM BIOS akceptuje tylko jeden video BIOS, wymagając wyłączenia
drugiego. Moje karty nie mają tej opcji (myślę, że nie). Inni
powiedzieli mi, że jest to ustawiane na płycie głównej.

Konfiguracja programowa Metro-X wygląda na łatwą i intuicyjną, ale jak
mam ustawić sprzęt? Może wyjaśnienie w X86Free byłoby dobre, ale mój
problem jest z ustawieniem sprzętu.

Czy moglibyście pomóc? Jeżeli potrzebuję innej karty, jaką możecie mi
zaproponować?

Dziękuje za jakąkolwiek pomoc, Peter.


## Poczta Ogólna

Date: Tue, 04 Mar 1997 20:02:22 -0500  
Subject: **niedziałające wydanie 14**  
From: Pinwu Xu, pxu@perigee.net

Cześć,  
To prawda, że issue14.html było uszkodzone. Można to jednak rozwiązać
korzystając z edytora Netscape (lub zapamiętaj/drukuj bezpośrednio z
edytora). U mnie to działa.

Dzięki za doskonałą robote.  
\-- Pinwu Xu

-----

Date: Wed, 05 Mar 1997 18:39:17 -0800  
Subject: **dzięki**  
From: arne, asnow@cdepot.net

Chce tylko powiedzieć "dziękuję", za twoją pracę włożoną w Linux
Gazette. Jestem nowym użytkownikiem Linuksa i znalazłem nieocenione
artykuły pisane z myślą o niedoświadczonych użytkownikach. Dzięki raz
jeszcze.

Arne, Rocky Road Ranch

-----

Date: Sun, 09 Mar 1997 16:08:06 -0500  
Subject: **Uwielbiam ten serwis**  
From: Thomas L. Gossard, tgossard@ix.netcom.com

Używam Linuksa od około 2 lat i byłem klientem "Linux Journal" przez
około rok. Ciebie wolę czytać nawet bardziej. Uwielbiam sekcje "rady za
dwa grosze", gdzie są świetne wskazówki i pomysły. Jeżeli byś sprzedawał
gazetę jako magazyn w kiosku lub jako prenumerate, byłbym zapalonym
kupcem. Faktem jest, że mam link do gazety na szczycie moich bookmarków.
Trzymaj tak dalej.

Thomas L. Gossard

-----

Date: Sun, 09 Mar 1997 01:19:54 -0600  
Subject: **Netscape**  
From: Anthony Scott, ascott@Interaccess.com

Mógłbyś mi powiedzieć, gdzie mogę znaleść Netscape dla Linuksa... Ile
kosztuje?

dzięki, tony (Możesz ściągnąć go za darmo ze strony domowej Netscape.
--Red.)

-----

Date: Sat, 8 Mar 1997 18:25:28  
Subject: **Dzięki**  
From: Lance A. DeVooght, devooght@flash.net

Chcę przekazać swą wdzięczność za całą Twoją ciężką pracę w tworzeniu
NAJLEPSZEGO magazynu online\! Poza tym, gratuluje renomowanego sponsora,
Infomagic. Zapewniam, że nie zapomnę o nim, gdy następnym razem będe
kupował oprogramowanie. I na koniec, jestem pod dużym wrażeniem
doskonałych pisarzy, których tu zgromadziłeś.

Twój dłużnik,  
Lance DeVooght

-----

Date: Thu, 20 Mar 1997 11:01:23 +0100 (GMT+0100)  
Subject: **Dobra nie-fikcyjna książka\! The Cuckoo's egg**  
From: Tomas Brostroem, tbc@rcc.se

Fajna książka, którą powinni zainteresować się wszyscy fani Linuksa.
"The cuckoo's egg" Clifforda Stolla.

W najgorszym razie traktuje o bezpieczeństwie komputerowym.

IMHO najlepsza nie-fikcyjna książka jaką kiedykolwiek czytałem.

Pozdrawiam, Tomas

