---
title: Instalowanie/Konfigurowanie/Cacheownie Django na serwerze Linux
date: 2021-10-15
---

* Autor: Anderson Silva
* Tłumaczył: Marcin Niedziela
* Original text: https://linuxgazette.net/180/silva.html

---

W dzisiejszym świecie, tworzenie stron internetowych jest bardzo się zmienia.
Firmy chcą maksymalizować przychody z produkcji, minimalizując przy tym nakłady
na dewelopment i jego czas. Niewielkie zespoły deweloperskie coraz częściej
stają się normą. [Django](http://www.djangoproject.com/ "Django"): popularny
freamework napisany w [Pythonie](http://www.python.org/ "Python") odwołuje się
czystych i pragmatycznych do zasad RWAD (Rapid Web Application Development - 
szybkie tworzenie aplikacji webowych) oraz DRY (Don't Repeat Yourself - nie 
powtarzaj się) 

Artykuł ten nie ma na celu nauczyć jak programować w Pythonie, ani jak używać
frameworka Django. Jego zadaniem jest pokazać jak przenosić aplikacje napisane z
użyciem Django na działające środowiska wykorzystujące Apache lub Lighttpd.

Nauczymy się jak w kilku prostych krokach poprawić wydajność aplikacji w Django
poprzez użycie cachowania. Artykuł ten zakłada, że korzystasz z Fedory jako
serwera aplikacji, jednak wszystkie wspomniane w artykule pakiety są dostępne
dla wielu systemów operacyjnych, włączając w to [Extra Packages for
Enterprise Linux repository](http://fedoraproject.org/wiki/EPEL), co oznacza, że
instrukcje te powinny działać także na serwerach Red Hat Enterprise Linux 
czy CentOS.


## Czego potrzebujesz

Na początek potrzebujemy Django:

```sh
$ yum install Django
```

Jeśli chcesz uruchamiać Django z Apache, potrzebny jest 
[mod\_wsgi](http://code.google.com/p/modwsgi/):

```sh
$ yum install httpd mod_wsgi
```

Jeżeli chcesz uruchamiać Django z Lighttpd:

```sh
$ yum install lighttpd lighttpd-fastcgi python-flup
```

Aby przyśpieszyć działanie aplikacji w Django, potrzebujemy memcached:

```sh
$ yum install memcached python-memcached
```

## Tworzenie nowego projektu Django

1. Tworzenie nowego środowiska deweloperskiego:

```sh
$ mkdir -p $LOCATION_TO_YOUR_DEV_AREA 
$ cd $LOCATION_TO_YOUR_DEV_AREA
```

2. Uruchomienie nowego projektu w Django. Poniższe polecenie tworzy podstawową
   strukturę katalogów projektu.

```sh
$ django-admin startproject my_app
```

3. Uruchom serwer deweloperski Django na porcie 8080 (lub innym, jak Ci
   wygodniej).

Uwaga: Serwer deweloperski służy tylko do testów aplikacji. Nie używaj go dla
aplikacji produkcyjnych! 

```sh
$ python manage.py runserver 8080
```

4. Uruchom swój projekt Django z Apache i `mod_wsgi` poprzez aktywowanie
   `mod_wsgi`. *Zwróć uwagę*, że Twój projekt musi znajdować się w lokalizacji
   przyjaznej SELinux (nie korzystaj z katalogu domowego!) i dostępnej do odczytu
   przez użytkownika apache. W Fedorze, `mod_wsgi` jest dodawany automatyczne
   podczas instalacji do pliku `/etc/httpd/conf.d/wsgi.conf`. Moduł zostanie
   automatycznie załadowany po zrestartowaniu apache.

5. Utwórz host wirtualny poprzez utworzenie nowego pliku w 
`/etc/httpd/conf.d/myapp.conf`.

```
WSGIScriptAlias / /path/to/myapp/apache/django.wsgi

DocumentRoot /var/www/html/
ServerName your_domain_name
ErrorLog logs/my_app-error.log
CustomLog logs/my_app-access_log common
```

6. W kroku 5 ukorzyliśmy skrypt, a teraz musimy utworzyć plik wsgi, który do
   niego się odwołuje.

```py
import os
import sys

import django.core.handlers.wsgi

# Append our project path to the system library path
sys.path.append('/path/to/')

# Sets the settins module so Django will work properly
os.environ['DJANGO_SETTINGS_MODULE'] = 'myapp.settings'

# sets application (the default wsgi app) to the Django handler
application = django.core.handlers.wsgi.WSGIHandler()
```

## Uruchamianie projektu Django pod Lighthttpd z fastcgi

Pierwszą czynnością jest uruchomienie serwera FastCGI.

```sh
./manage.py runfcgi method=prefork socket=/var/www/myapp.sock pidfile=django_myapp.pid
```

Następnie należy w pliku `lighttpd.conf` aktywować serwer FastCGI.

```
server.document-root = "/var/www/django/"
fastcgi.server = (
    "/my_app.fcgi" => (
        "main" => (
            # Use host / port instead of socket for TCP fastcgi
            # "host" => "127.0.0.1",
            # "port" => 3033,
            "socket" => "/var/www/my_app.sock",
            "check-local" => "disable",
        )
    ),
)
alias.url = (
    "/media/" => "/var/www/django/media/",
)
url.rewrite-once = (
    "^(/media.*)$" => "$1",
    "^/favicon\.ico$" => "/media/favicon.ico",
    "^(/.*)$" => "/my_app.fcgi$1",
)
```

## Konfiguracja cachowania w Django

Django posiada wiele różnych silników cachowania, włączają bazy danych, pamięć,
system plików i bardzo popularny memcached. Zgodnie z http://www.danga.com/memcached/, 
memcached jest "wydajnym, zdecentralizowanym systemem cachowania,
zaprojektowanym do użycia w celu przyśpieszenia dynamicznych aplikacji webowych
poprzez łagodzenie obciążenia bazy  danych. Używany jest przez bardzo
uczęszczane strony takie jak [Slashdot](http://www.slashdot.org/) i 
[Wikipedia](http://www.wikipedia.com/). Czyni to z niego idealnego kandydata do
cachowania Twojej super aplikacji webowej.

Na początek, instalujemy memcached.

```sh
$ yum install memcached
```

Następnie, instalujemy dowiązania pythona dla memcached.

```sh
$ yum install python-memcached
```

Kolejny krok to sprawdzenie czy memcached działa poprawnie. 

```sh
$ /etc/init.d/memcached status 
memcached (pid 6771) is running...
```

Jeżeli nie działa, możesz uruchomić go ręcznie.

```sh
$ /sbin/service memcached start
```

Aby upewnić się, że będzie on automatycznie uruchamiany przy każdym starcie
maszyny: 

```sh
$ /sbin/chkconfig --level 35 memcached on
```

Sprawdziłeś już czy memcached jest uruchomiony, czas więc powiedzieć aplikacjom
w Django aby z niego korzystały jako swój silnik cachowania. Można to zrobić
poprzez dodanie zmiennej `CACHE_BACKEND` w pliku `settings.py`.

```sh
CACHE_BACKEND = 'memcached://127.0.0.1:11211/'
```

Format jest postaci "backend://host:port/" lub "backend:///path", zależnie od
wybranego backendu. Jako, że używany memcached, mamy możliwość uruchomić kilka 
instancji na różnych serwerach i współdzielić cache pomiędzy kilkoma maszynami.
Jeśli chcesz to zrobić, to wystarczy dodać kombinację "serwer:port" do
`CACHE_BACKEND` oddzielone od siebie średnikiem. W poniższym przykładzie
dzielimy cache pomiędzy trzy różne serwery memcached:

```sh
CACHE_BACKEND = 'memcached://127.0.0.1:11211;192.168.0.10:11211;192.168.0.11/'
```

Więcej informacji na temat różnych sposobów cachowania dla Django możesz
znaleźć na stronie z [oficjalną dokumentacją](http://www.djangoproject.com/documentation/cache/).

Na koniec, jeżeli chcesz uruchomić swoją aplikację na produkcji, zawsze możesz
napisać swój własny [skrypt startowy](http://code.djangoproject.com/wiki/InitdScriptForLinux "service script")
dla Django, aby aplikacja uruchamiała się automatycznie przy starcie systemu.

Oryginał tego artykułu został opublikowany 5 czerwca 2008 roku w magazynie Red
Hat i zrewidowany na potrzeby listopadowego wydania Linux Gazette z 2010 roku.



