---
title: Cóż, to wszystko!
date: 2021-04-08
---


* Autor: John M. Fisk
* Tłumaczenie: Marcin Niedziela
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.html

---

Mam nadzieję, że podobał wam się pierwszy numer <b>Linux Gazette</b>. Starałem się aby wszystkie
informacje tu zawarte były aktualne i dokładne. Jeśli są tu jakieś błędy, proszę dać mi o tym
znać. Jeśli macie jakieś sugestie, komentarze lub krytykę dajcie mi znać.

Jeśli znajdę jakieś interesujące rzeczy, mam nadzieję opisze je w następnej edycji.

Masz jakieś świetne pomysły! Wyślij swoje [komentarze, krytykę, sugestie i pomysły](mailto:fiskjm@ctrvax.vanderbilt.edu).

