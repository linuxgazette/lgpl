---
title: Dostęp do Linuksa spod DOSa!?
date: 2021-04-08
---

* Autor: John M. Fisk
* Tłumaczenie: Marcin Niedziela
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.html

---

Wielu ludzi używających Linuksa korzystało do niedawna z innych systemów
operacyjnych, jak DOS czy Windoze. W rzeczywistości, wielu z nich nadal
wykorzystuje wielosystemowość. Istnieje wiele różnorakiego
oprogramowania, na które możemy wydać wiele pieniędzy, a które
zwyczajnie może nie chcieć się uruchomić lub może nie działać jak
powinno. Faktem jest, że nie istnieje system operacyjny, pod którym
działać będzie wszystko, co chciałbyś uruchomić (szczególnie w
oprogramowaniu Billa Gate\$ i jego firmy)

Na szczęście, Linux posiada wsparcie systemu plików FAT, dające Ci
dostęp do partycji DOS. Pozwala na zapis i odczyt na systemie plików
DOS. Pozwala także na na emulowanie (naśladowanie) obu środowisk, DOSa
(**DOSEMU**) i Windows 3.1 (**WINE**), co pozwala Ci na uruchamianie
programów dla DOSa i Win 3.1 bezpośrednio z Linuksa. Jeśli jesteś
zainteresowany takimi rozwiązaniami przejrzyj HOWTO i inne informacje
znajdujące się na stronach [Projektu Dokumentacji Linuksa](http://sunsite.unc.edu/mdw/linux.html).\

Znajdziesz tam mały, poręczny program,
[ext2tool.zip](ftp://ftp.cc.gatech.edu/pub/linux/system/Filesystems/ext2/ext2tool.zip),
napisany przez **Claus'a Tondering**, który pozwala Ci na dostęp do
partycji Linuksowej z poziomu DOS'a. Nie jest może bardzo rozbudowany,
ale jest wystarczająco użyteczny. Obsługuje polecenia: **ls, cd, cp**
i **cat**, a także posiada niewielkie narzędzie, które pomaga w
identyfikacji numerów partycji Linuksowej -- potrzebujesz tego by
ustawić zmienną E2CWD.

By go zainstalować, zwyczajnie rozpakuj archiwum zip do jakiegoś
katalogu, ja rozpakowałem go do katalogu \\ext2tool wraz ze wszystkimi
pomocami w plikach README. Jedyną rzeczą jaką musisz zrobić jest
ustawienie zmiennej E2CWD:

```
[wycinek pliku README]

Zanim użyjesz programu ext2, musisz ustawić strodowisko zmiennych E2CWD,
korzystając z poniższego przykładu:

    SET E2CWD=129:5
or
    SET E2CWD=129:5:234

Te trzy liczby po znaku równości mają następujące znaczenie:

Pierwsza liczba (129 w powyższym przykładzie) identyfikuje, na którym
dysku istnieje system plików ext2. Liczba użyta jako drugi parametrer wskazuje
biosdisk() DOS'a. Standardowe wartości to:

      0 dla dyskietki A:
      1 dla dyskietki B:
    128 dla pierwszego dysku twardego
    129 dla drugiego dyski twardego

(Twój BIOS może używać innych wartości, w szczególności, jeśli posiadasz
dysk SCSI.)

Druga liczba (5 w powyższym przykładzie) jest liczbą partycji ulokowaną
na każdym systemie plików ext2. Liczba ta, jest zazwyczaj częścią pliku
wskazującego dysk pod Linuksem. Jeśli, dla przykładu, używasz dysku
/dev/hdb5, numerem partycji bedzie 5. Program E2PART pozwala Ci na
identyfikacje numerów partycji.

Trzecia liczba jest liczbą katalogu znajdującego się w Twoim aktualnym
katalogu roboczym. Jeżeli nie wiesz co tam wpisać, nie martw się. Zwyczajnie
pomiń ten krok.
```

Jeżeli mój Linux jest na pierwszym dysku, na partycji /dev/hda6
wystarczy, że wpisze:

```sh
SET E2CWD=128:6
```

w moim pliku `c:\\autoexec.bat` i wszystko działa znakomicie.

To właściwie dużo prostsze niż na to wygląda, a jeśli już ustawisz te
rzeczy będą one działały całkiem dobrze. By to *działało pod Linuksem*,
będziesz musiał przeładować system, a potem skopiować to *z* DOS'a,
ponieważ **ext2tool** nie pozwala na zapis plików. Ale przecież, kto
chce spędzać większość czasu w DOSie... ;-)

