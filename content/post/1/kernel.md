---
title: Dopieszczanie kernela... to nie takie trudne!
date: 2021-04-08
---

* Autor: John M. Fisk
* Tłumaczenie: Marcin Niedziela
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.html

---

Dla niektórych ludzi, idea kompilowania kernela, przyprawia ich o o
gęsią skórke.

Dobra wiadomość... tak naprawdę to nie jest takie trudne.

Powody są proste: ludzie, którzy przeznaczają swój czas na opracowanie
kernela, wykonują kawał dobrej roboty, by wszystko działało jak należy.
Aktualnie jest to technicznie całkiem proste. Jest jednak, niestety,

**Mała uwaga**: musisz wiedzieć co robisz!

Zobaczmy, co to znaczy, że *wiesz co robisz*. Sedno sprawy leży w
uruchomieniu procedury " make config ". Uruchomi ona program, dzięki
któremu w prosty sposób będziesz mógł wybrać to, co chcesz wkompilować
we własne jądro. Opcje zawierają:

* emulacje koprocesora
* wsparcie dla sterowników dysków twardych
* wspierane systemy plików
* ustawienia sieciowe
* wsparcie dla cdromu, myszy, karty dźwiękowej

i tak dalej. Program jest na tyle 'inteligentny', że dostarcza wiele
znośnych ustawień domyślnych. Rzecz w tym, *aby skompilować we własnym
jądrze, tylko to co jest CI potrzebne w TWOIM systemie*.

Jeśli posiadasz myszkę Logitech, kartę dźwiękową GUS i korzystasz z
połączenia dial-up PPP, wtedy nie jest potrzebne Ci wsparcie dla NFS,
kart Ethernet. Jeśli zaznaczysz jakąś niepotrzebną opcje, nie sprawi to,
że kernel nie będzie działał... będzie po prostu zawierał sterowniki,
których nie będzie wykorzystywał (kernel będzie miał większą objętość i
będzie zajmował więcej pamięci), lub może się zdarzyć, że nie będzie
posiadał jakiejś opcji, której naprawdę potrzebujesz -- jak na przykład
wsparcia dla Twojej myszki.

Skąd więc brać odpowiednią wiedze?

Najlepszym rozwiązaniem na początek będzie lektura **Kernel-HOWTO**
autorstwa **Brian'a Ward'a**, które możesz znaleźć na stronie [Projektu
Dokumentacji Linuksa w sekcji
HOWTO](https://web.archive.org/web/20030313032239/http://sunsite.unc.edu/mdw/linux.html#howtos)
, który pomoże Ci, krok po kroku w całym procesie kompilacji i wyjaśni
do czego służy każda z opcji. Pomocne może być także wyszukiwanie
informacji i rad na [Linux sunsite
kernel](https://web.archive.org/web/20030313032239/ftp://ftp.cc.gatech.edu/pub/linux/kernel/%22).
Kilku programistów pisze programy specjalizujące się w asystowaniu
podczas procesu kompilacji kernela.

Nie zapomnij o wydrukowanych pozycjach. **Matt Welsh** i **Lar Kaufman**
napisali znakomitą książkę, którą *każdy* powinien mieć na swojej półce,
pod tytułem Uruchamianie Linuksa \[Running Linux\] (Publikacja
wydawnictwa O'Reilly). Za niewielką cenę otrzymujemy ponad 500 stron
instrukcji, porad, trików i wygodnych w użyciu HOWTO'sów. Jeśli jej nie
masz, zdobądź ją.

Gdy już odrobiłeś swoją prace domową, część techniczna jest już całkiem
prosta, następnie:

1. Ściągnij aktualną kopię źródeł kernela z [The Linux Kernel Archives](https://web.archive.org/web/20030313032239/ftp://ftp.cc.gatech.edu/pub/linux/kernel/v1.2/).\
2. Rozpakuj je do katalogu /usr/src.
3. Utwórz łącze symboliczne z /linux-1.2.x do /linux - np., ln -s /linux-1.2.x linux
4. Teraz, wejdź do katalogu /usr/src/linux i już jesteś gotów do kompilacji.

W tym momencie wystarczy wpisać:

```sh
make mrproper ; make config ; make dep ; make clean ; make zImage
```

To wszystko. Zobaczysz jak linie skryptu na ekranie uruchamiają
kreatora, który za pomocą serii pytań pozwoli Ci na konfigurację kernela
- wykonujesz właśnie **make config**. Odpowiedz na pytania, a kiedy
skończysz, wszystko będzie już gotowe do kompilacji.

Jak długo to zajmuje?

* pentium/133 -- zdążysz pójść na łazienki, ale niech nie zajmie Ci to długo.
* pentium/66 -- połóż się wygodnie i wypij sobie Cole.
* 486DX2/66 -- połóż się wygodnie, wypij Cole i weź jakąś gazetę do poczytania.
* 486SX/25 -- połóż sie wygodnie, poczytaj gazetę, obejrzyj powtórkę *Cheers*.
* 386DX40 -- połóż się i wypij Cole, przeczytaj gazetę, obejrzyj powtórkę *Cheers*, weź kąpiel.
* 386SX-16 -- połóż się, wypij Cole, przeczytaj gazetę, obejrzyj powtórkę *Cheers*, weź kąpiel, odwiedź rodziców.
* 286 -- hehe, zapomnij... *ten piesek nie zaszczeka*

Kiedy kompilacja się zakończy i wykonasz wszystkie pozostałe kroki,
możesz zacząć podziwiać swój lśniący kernel w katalogu:

/usr/src/linux/arch/i386/boot Będzie on nosił nazwę `zImage` i zazwyczaj
znajduje się na dole wyświetlanego katalogu. No i proszę! Zrobiłeś to!

Teraz pewnie będziesz chciał go wypróbować, prawda? Na początek kilka
**wskazówek!**

**Rada \#1**: zanim cokolwiek zrobisz, musisz być całkowicie pewien, ze
masz sprawną dyskietkę startową;

**Rada \#2**: upewnij się, że ta dyskietka na pewno działa!

**Rada \#3**: zrób kopie aktualnie działającego obrazu kernela
(zazwyczaj, /vmlinuz) i umieść go w bezpiecznym miejscu.

Teraz zmień nazwę aktualnego pliku kernela -- np.,
`mv vmlinuz vmlinuz.bak`, a następnie skopiuj nowy plik kernela, zImage
do głównej partycji -- np. `cp zImage /vmlinuz`.

Możesz też ustawić prawa dostępu `chmod 400 vmlinuz` dla większego
bezpieczeństwa i **nie zapomnij odpalić LILO** -- komenda /sbin/lilo.

Jak tylko to zrobisz, jesteś już gotowy do restartu komputera. Ok,
lecimy... wpisz shutdown -r now i trzymaj kciuki aby wszystko działało!

**Nie zadziałało??!!! Wrrrrr...!!** Ale nie bój się, da się to naprawić.
Jeśli system z jakiegoś powodu nie wczytuje nowego kernela, zresetuj
komputer i uruchom go z dyskietki startowej. Teraz skopiuj stary plik
kernela z powrotem do katalogu głównego root i **przeładuj LILO** --
komenda /sbin/lilo.

Pamiętaj, aby zawsze trzymać stary plik kernela w bezpiecznym miejscu
aby móc w razie problemów uruchomić z niego system. Lecz nie przestawaj
czytać...... mamy kilka pomysłów w kolejnej sekcji pt. *prosty sposób na
przetestowanie kernela*.

