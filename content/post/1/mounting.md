---
title: Prosty sposób na montowanie cdrom-u i napędu dyskietek
date: 2021-04-08
---

* Autor: John M. Fisk
* Tłumaczenie: Marcin Niedziela
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.html

---

OK, doszliśmy już tutaj... Jak już zaczęliśmy bawić się z całym tym
procesem uruchamiania, nadszedł czas, by zahaczyć o coś innego.

Plik **/etc/fstab**.

Oto kilka pomocnych sugestii.

Na początek jednak, mała uwaga dla tych wszystkich, którzy uaktualnili
sobie jądro do wersji 1.2.x i znaleźli jakiś irytujący błąd podczas
uruchamiania... wiecie, czegoś brakuje Waszemu plikowi /etc/fstab... oto
jak sobie z tym poradzić.

Problem w tym, że **mount** szuka teraz dwóch oddzielnych rzeczy, pliku
/etc/fstab -- piątego (fs freq) i szóstego (fs passno) pola -- które nie
są standardowo wbudowane w dystrybucję **Slackware**. Więc, jeśli już
zainstalujesz wszystko prawidłowo, *wciąż* będziesz informowany o
błędzie. Czas to naprawić.

Strony podręcznika man dla **fstab** właściwie bardzo dobrze opisują
poruszany problem. To, co najważniejsze, aby uwolnić się od tego błędu,
to należy wiedzieć, że te dwa dodatkowe pola zostały umieszczone dla
każdego wejścia systemu plików. **Piąte pole** (fs freq) musi wykonywać
informacje zawarte w programie **dump** (?) -- wyszczególnia on, które
systemy plików muszą być **dump'nięte**.(?)... Nie jestem pewien, ale
doczytałem się, że jest bezpiecznie ustawić wartość "0" i pozwolić mu
działać.

**Szóste pole** (fs passno) jest warte zainteresowania. Zawiera wartość
liczbową używaną przez program **fsck** w czasie uruchamiania, by
określić rozkaz, w którym systemy plików są sprawdzane. Reguły są
następujące:

**Główne systemy plików otrzymują wartość 1\
Pozostałe systemy plików otrzymują wartość 2\
Systemy plików, które nie potrzebują być sprawdzone -- np. /proc czy
/cdrom -- otrzymują wartość 0**

Tak więc, Twój `fstab` może wyglądać następująco:

```sh
# entries for file system table "fstab".  File systems are entered in a format:
#
# device:   mount:      fs-type:    options:    dump:   fsck:
# _______   ______      _______     ________    ____    ____
#
/dev/hda6   /       ext2        defaults    0   1
/dev/hdb7   swap        swap        defaults    0   0
/dev/hda2   /dosc       msdos       defaults    0   2
/dev/hdb5   /os2        msdos       defaults    0   2
/dev/hdb6   /dosf       msdos       defaults    0   2
none        /proc       proc        defaults    0   0
#
# These last five are for the CDROM and floppy drives A and B.  The fs-type
# is indicated and the 'noauto' keeps init from trying to automount the fs
# at bootup.  The option 'user' *should* allow normal non-root users to mount
# the fs.
#
/dev/sbpcd  /cdrom      iso9660     ro,noauto   0   0
/dev/fd0    /a      msdos       user,noauto 0   0
/dev/fd1    /b      msdos       user,noauto 0   0
/dev/fd0    /fd0        ext2        user,noauto 0   0
/dev/fd1    /fd1        ext2        user,noauto 0   0
```

O co w tym wszystkim chodzi?

Kiedy uruchamiasz swój system, z pliku /etc/rc.d/rc.S uruchamiane jest
polecenie:

```sh
/sbin/mount -avt nonfs
```

przynajmniej tak to wygląda w dystrybucji Slackware vanilla, Twój może
być inny. Komenda **mount** z flagą **-a** oznacza automatyczne
montowanie wszystkich systemów plików zawartych w pliku /etc/fstab,
*poza tymi, które są oznaczone opcją noauto*.

Jak widzisz na powyższym przykładzie, jest tam kilka systemów plików
automatycznie montowanych podczas uruchamiania systemu, włączając w to
partycje Linux native (/dev/hda6), Linux swap (/dev/hdb7), partycje DOS
(/dev/hda2 i /dev/hdb6), OS/2 (partycja FAT, system plików " msdos ")
(/dev/hdb5) i partycja /proc.

Jak widzisz ostatnie linie wpisu w pliku fstab, oznaczone są jako "
noauto ".

**Oto jak ułatwić sobie życie**

Dodanie opcji " noauto " nie pozwala poleceniu **mount** montować cdromu
lub napędów dyskietek podczas startu systemu -- tak jakby nic się tam
nie znajdowało. Jednak, jeśli mam zamiar zamontować jeden z nich, muszę
zazwyczaj wpisać:

```sh
mount -r -t iso9660 /dev/sbpcd /cdrom
```

Można prościej wstukując:

```sh
mount /cdrom
```

Mount zajrzy do pliku fstab i jeśli znajdzie odpowiedni wpisz dla
/cdrom, zamontuje go z podanymi opcjami. **Małe wyjaśnienie**: Wpisując
/a lub /fd0 dla urządzenia /dev/fd0 i /b lub /fd1 dla /dev/fd1 mogę
zamontować dyskietkę formatu DOS lub ext2:

**mount /a** dla dyskietek DOS, lub\
**mount /fd0** dla dyskietek ext2

Dodając opcję " user " pozwala to na montowanie systemów plików przez
*zwykłych* użytkowników.

Gładko, prawda? **Linux** jest wspaniały!

