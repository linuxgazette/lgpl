---
title: Odtwarzanie plików .AU przy starcie systemu
date: 2021-04-08
---

* Autor: John M. Fisk
* Tłumaczenie: Marcin Niedziela
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.html

---

Ci z Was, którzy posiadają kartę dźwiękową i mają nieprzyjemność
skłaniania się w kierunku *Windozy*, mogą sobie ustawić kilka tych
irytujących *odgłosów* z plików .wav wydobywających się z głośników.

Na szczęście, możesz zrobić dużo więcej niż tylko odtworzenie
`ding.wav`. Linux jest po to, by Ci to umożliwić. Na początek,
potrzebujesz jakiegoś przyzwoitego pliku .au -- poszukaj w podkatalogu
**/pub/multimedia** na
[sunsite.unc.edu](https://web.archive.org/web/20030313032239/ftp://sunsite.unc.edu/pub/multimedia/sun_sounds/),
gdzie znajdziesz wiele wspaniałych, użytecznych plików dźwiękowych .au.

(Jeśli potrzebujesz jakiejś wskazówki, są tam wspaniałe klipy kilku
tekstów z Monty Python'a -- wypróbuj choćby pliki spam.au oraz
lumberjack.au -- większość to klasyki z Three Stooges w podkatalogu
[/series/three\_stooges](https://web.archive.org/web/20030313032239/ftp://sunsite.unc.edu/pub/multimedia/sun_sounds/series/three_stooges/)

Kiedy już zdobędziesz kilka dobrych plików .au, możesz zapewnić sobie
wiele ciekawych wrażeń przez dodanie czegoś takiego jak poniżej do pliku
/etc/rc.d/rc.local

```sh
cat /usr/local/sound/spam.au > /dev/audio &
```

Jeśli dodasz to na końcu pliku, po wszystkich innych wykonywalnych
czynnościach, wtedy będziesz mógł delektować się dźwiękiem "Spam, Spam,
Wonderful Spam" w czterech częściach harmonicznych (...serio!) w czasie
wyświetlania ekranu logowania do systemu. Jedna uwaga -- nie zapomnij
dodać " & " na końcu polecenia, w przeciwnym razie będziesz zmuszony
odsłuchać CAŁY plik dźwiękowy, zanim będziesz mógł cokolwiek zrobić
innego, w tym wypadku zalogować się.
