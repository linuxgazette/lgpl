---
title: Co to jest Linux Gazette...
date: 2021-04-08
---

* Autor: John M. Fisk
* Tłumaczenie: Marcin Niedziela
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.html

---


Pierwsze słowa, przemyślenia i inne rzeczy...

Jakiś rok temu, po tym jak połączyłem się pierwszy raz z Internetem modemem o prędkości  2400,
zacząłem interesować się wolnodostępnymi, UN*X-podobnymi, systemami operacyjnymi, które można było
uruchamiać na komputerach klasy PC i386 i lepszych. Wydawało się to dla mnie bardzo dobre...

Tak więc... po spędzeniu na modemie wielu poranków, ORAZ zużyciu 45 dyskietek,
udało mi się wreszcie 'dorwać' kopię **Slackware'a 2.0.0**. Spędziłem całe popołudnie
przerzucając dyskietkę po dyskietce w moim pececie i czytając informacje wyświetlane na ekranie.
W końcu udało mi się wszystko zainstalować, zresetowałem komputer, i **wreszcie...**

## LINUX!

Wpadłem! To było oszałamiające. W tym czasie byłem całkiem zielony w tych sprawach i
myślałem, że to 120MB, ukryty wirus... W końcu, zacząłem nieco 'chwytać' i system zaczynał jakoś
działać! Teraz to dopiero wsiąkłem, czytałem wszystko co tylko wpadło mi w ręce i co udało mi się znaleźć:

* comp.os.linux.*
* The Linux User's Guide' (Linux Documentation Project (LDP))
* Linux Installation and Getting Started' (LDP)
* the Network Administrator's Guide' (LDP)
* różne opracowania FAQ's i HOWTO
* "Running Linux" autorstwa Matt'a Welsh i Lar'a Kaufman  (wydawnictwa O'Reilly)
* "Linux - Unleashing the Workstation in Your PC" autorstwa Stephana Strobel i Thomasa Uhl (wydawnictwa Springer-Verlag)
* "X Window System User's Guide" autorstwa Valerie Querica i Tim'a O'Reilly  (O'Reilly)
* "A Student's Guide to UNIX" autorstwa Harley'a Hahn, Essential System Administration autorstwa AEleen Frisch,
* "Shell Programming" autorstwa Kamran'a Husain.

Krążyłem wokół tych pozycji, dopóki nie zostały  poprawione lub obalone. Linux *wreszcie zaczął mieć dla mnie sens*.

Jeśli jesteś w tym samym miejscu... nie poddawaj się! Rezultat jest tego warty! Używanie Linuksa
jest czymś, co można porównać do odrabiania pracy domowej. Musisz dużo czytać i uczyć się, zanim
to wszystko będzie miało dla Ciebie jakiś sens.

Jest WIELU ludzi, którzy są chętni do pomocy i angażują WIELE czasu, pisząc dokumentację, która
pozwala na naukę Linuksa.

Doskonałe oprogramowanie bez wiedzy jest zazwyczaj bezużyteczne.

To co chciałem przedstawić w **Linux Gazette**, to kilka pomysłów, które wypróbowałem i polubiłem
lub takie, które do czegoś mi się przydały. Należy jednak pamiętać:

**To nie jest poradnik techniczny i nie chce być uważany przez Was za kogoś w stylu Guru...**

Mam nadzieje, że ta gazeta sprawi, iż będziecie używać Linuksa, jako pewnego rodzaju zabawy, będzie to
przyjemne i proste. Niech ten zbiór pomysłów, jakie z wielu źródeł sobie bezwstydnie przywłaszczyłem,
szczerze mówiąc nie jestem pewien skąd niektóre się wzięły, będzie pewnego rodzaju hołdem dla ich
autorów.

Dzięki.

