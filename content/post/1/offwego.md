---
title: Zaczynamy...
date: 2021-04-08
---

* Autor: John M. Fisk
* Tłumaczenie: Marcin Niedziela
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.html

---

To co jest możliwe do zrobienia jest albo łatwe albo skomplikowane. Według ogólnej opinii,
wszystko daje się wykonać, jeśli tylko poświęcisz temu trochę czasu...

No tak... Twoje połączenie czasem pozostaje w kilku minutowej bezczynności?! Jeśl korzystasz z
połączenia PPP, wypróbuj poniższy skrypt:

```sh
#!/bin/sh
#program:	hose-em
#purpose:	keeps a dial-up line from timing out while you're
#		reading Web stuff or doing your laundry (OK... let's be
#		responsible)
#
# We'll use the gateway server as your host.
HOST=000.00.00.000	# the dotted-quad address of your gateway which
            # you can get by typing "ifconfig"
SLEEP=8m		# how long we'll wait between pings
############################################################################
#
# Set up a while loop that pings the host as long as the PID file is exists.
# But first, make sure thet the connection is still up.
#
if [ -e "/var/run/ppp0.pid" ]; then
    echo "
    * * *    PPP-UP is running   * * *
     "
else
    echo "
    * * * 	 PPP0 interface is not up!  * * *
     "
    exit 1
fi
    while [ -e "/var/run/ppp0.pid" ]	# Now, set up the while loop.
do						# As long as the PID file exists
    ping -c 1 $HOST &gt; /dev/null		# we'll ping the host once every 8 min.
    sleep $SLEEP
done
exit 0
```

zauważ, że użyto `ping -c 1`  -- to zapobiegnie nieskończonemu pingowaniu. Ustaw zmienną
`SLEEP=` na tyle, ile potrzebujesz. I to wszystko!

Nie zapomnij wykonać `chmod 711 hose-em` kiedy skończysz.

Teraz możesz już surfować w spokoju. Kiedy uruchomisz odnośnik, pętla while zakończy się, a
program ping zakończy działanie.

