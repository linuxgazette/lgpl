---
title: Czym jest to całe MOTD i <em>co zjada mi moje issue...!
date: 2021-04-08
---

* Autor: John M. Fisk
* Tłumaczenie: Marcin Niedziela
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.html

---

Za każdym razem gdy logujesz się do Linuksa widzisz ten sam tekst:

```
Welcome to Linux 1.x.x.
Login:

Linux 1.x.x. (POSIX)
```

...i w końcu odkrywasz `/etc/issue</b>` i `/etc/motd`. Pozmieniałeś sobie nawet te
durne teksty na *coś* pożytecznego, np. informację o wersji kernela... Resetujesz..
**i ZNOWU to samo!!**. Wrrrrr!

By to zmienić, musisz edytować jeden z tych 'paskudnych' plików konfiguracyjnych w katalogu
/etc/rc.d, a dokładnie /etc/rc.d/rc.s, zobaczmy:

```
# Setup the /etc/issue and /etc/motd to reflect the current kernel level:
# THESE WIPE ANY CHANGES YOU MAKE TO /ETC/ISSUE AND /ETC/MOTD WITH EACH
# BOOT. COMMENT THEM OUT IF YOU WANT TO MAKE CUSTOM VERSIONS.
echo &gt; /etc/issue
echo Welcome to Linux `/bin/uname -a | /bin/cut -d\  -f3`. &gt;&gt; /etc/issue
echo &gt;&gt; /etc/issue
echo "`/bin/uname -a | /bin/cut -d\  -f1,3`. (POSIX)." &gt; /etc/motd
```

Jeżeli przyjrzysz się dokładnie, znajdziesz tam linijki zaczynające się od echo i wskazujących
na plik /etc/issue, które nadpisują go za każdym razem gdy resetujesz system. To samo dzieje się
z plikiem /etc/motd. Wystarczy teraz tylko ustawić na początku tych linii znaczek "#",
co zostanie odczytane jako komentarz, a system po prostu będzie ignorował te linie. Możesz już
teraz wyedytować pliki `/etc/issue` i `/etc/motd` będąc pewnym, że jeśli przeładujesz system...
zostaną one prawidłowo wyświetlone!

BTW... /etc/issue jest pierwszą wiadomością, jaka jest wyświetlana na ekranie logowania, a
/etc/motd (Message Of The Day - Wiadomość dnia) jest wyświetlana tylko po prawidłowym zalogowaniu się.

