---
title: Cała zabawa z aliasami i ich efekty...
date: 2021-04-08
---

* Autor: John M. Fisk
* Tłumaczenie: Marcin Niedziela
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.html

---

Jeśli już mówimy o poprawianiu... Jeśli używasz shell'a bash (instalowanego domyślnie z
systemem), wtedy masz dostęp do małego, acz wspaniałego pliku konfiguracyjnego ~/.bash_profile.

**Wyjaśnienie**: ten "zawijasty" znaczek ("tylda") wskazuje tylko Twój katalog domowy.
Powinien wskazywać ten sam katalog, który w środowisku oznaczony jest jako $HOME.

Jeśli przejrzysz strony podręcznika man dla **bash**, dowiesz się, że jest jeszcze kilka
plików konfiguracyjnych, które są wczytywane w tym samym czasie po uruchamianiu:

* /etc/profile -- systemowy plik inicjalizacyjny, wykonywany po zalogowaniu;
* ~/.bash_profile -- osobisty plik inicjalizacyjny, wykonywany po zalogowaniu;
* ~/.bashrc -- własny, interaktywny plik inicjalizacyjny shella.

Oznacza to, że możesz ustawić własną konfigurację przy uruchamianiu, za każdym razem, gdy
logujesz się do systemu. I tu właśnie pojawiają się aliasy.

Aliasy są substytutami rozkazu, jeśli chcesz, może on być wykorzystany w pewnych powłokach
takich jak bash. Składnia jest następująca:

```sh
alias skrót=`komenda -opcje`
```

Zauważ użycie \` po znaku równości (apostrof wsteczny znajduje się pod znakiem tyldy w lewej
górnej części klawiatury).

Teraz... czas ułatwić sobie życie. Jeżeli ustawisz aliasy skrótowe, możesz zwiększyć szybkość
wprowadzania komend z klawiatury. Oto kilka przykładów:

```sh
alias c='clear'
alias d='ls -lF'
alias dir='ls -lF'
alias ftp='ftp -v'
alias lpr='lpr -h'
alias rm='rm -i'
alias startx='startx &gt;/dev/null'
alias x='startx &gt;/dev/null'
```

Widzisz, że takie ustawienia dużo pomagają. Zamiast ciągłego wpisywania ls -1F do wyświetlenia
zawartości katalogu wpisujesz po prostu " d ". Również, jeśli uruchamiasz  X Window
*i jesteś zmęczony oglądaniem tych wszystkich, zaśmiecających ekran w czasie uruchamiania
informacji, kiedy zamkniesz X-y*, dodaj alias, który wyśle to wszystko do /dev/null, czyszcząc
Twój ekran. Przyjemne z pożytecznym!

Uważajmy na jedną rzecz -- dla bezpieczeństwa. Tak długo jak jesteś zalogowany jako root,
prawdopodobnie za każdym razem zwracasz pilną uwagę na to co robisz z bardzo ważnymi plikami systemowymi.
Jednak może się zdarzyć, że po godzinach/dniach/tygodniach pracy z plikami, możesz coś niechcący
potraktować komendą " rm ". UN*X potrafi być nieco wredny, a Linux nie jest inny.
Jeśli coś zginie... to zginie na zawsze. Oczywiście istnieją programy wspomagające bezpieczne usuwanie, ale
świeżo zainstalowany Linux Vanilla pozwoli na usunięcie wszystkiego, co mu tylko każesz.

Dodajmy więc alias dla polecenia " rm " -- będzie ono zawsze uruchamiane z opcją -i,
która sprawi, że system zawsze będzie pytał Cię o potwierdzenie przed skasowaniem czegokolwiek. No tak,
to może być nieco uciążliwe, jednak jeśli przyda się to chociaż raz, będziesz zadowolony, że to zrobiłeś.
Oczywiście możesz to odwrócić wpisując, dla przykładu: " rm -f usun_mnie.txt ", a Linux
szczęśliwie usunie plik bez żadnego potwierdzania.


