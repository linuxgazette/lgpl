---
title: Prosty sposób na przetestowanie kernela
date: 2021-04-08
---

* Autor: John M. Fisk
* Tłumaczenie: Marcin Niedziela
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.html

---

Teraz, gdy masz już działający i samodzielnie skompilowany kernel,
nadszedł czas by wypróbować go i zobaczyć jak działa. Nadpisanie starego
działającego kernela jest... cóż, może trochę zuchwałe, chyba, że
naprawdę wiesz co robisz lub jesteś prawdziwym poszukiwaczem wrażeń.
Jeśli jesteś którymś z powyższych, prawdopodobnie możesz przestać czytać
tą sekcje.

Reszta z Was, niech czyta dalej.

Istnieją dwa sposoby aby w łatwo wypróbować nasz nowy kernel:

-   stworzyć z niego dyskietkę startową;
-   ustawić go jako opcja w ustawieniach LILO.

Dla pewności, istnieją bardziej wyszukane sposoby na przetestowanie
nowego jajka... ale to nie jest przewodnik dla profesjonalistów, to co
robimy ma być łatwe do wykonania. I każda z tych dwóch metod jakie
przedstawiłem, powinna dać Ci szansę na uruchomienie nowego kernela bez
sprawiania sobie wielu problemów.

By stworzyć użyteczny dysk startowy, zwyczajnie skopiuj na dyskietkę
plik zImage z katalogu /usr/src/linux/arch/i386/boot/. Wpisz:

```sh
cp zImage /dev/fd0
```

by skopiować go na dyskitce w pierwszym napędzie. Pamiętaj, że to usunie
wszystkie dane na twojej dyskietce. Stanie się to bez żadnego
ostrzeżenia, wiec bądź pewien, że nie ma na niej żadnych ważnych dla
Ciebie plików. Kopiowanie kernel na dyskietkę, na której masz 327 stron
doktoratu w formacie LaTeX prawdopodobnie nie będzie dobrym pomysłem.

Jak już skończysz, by ją wypróbować, po prostu zresetuj komputer
poleceniem **shutdown -r now** i usiądź wygodnie. Nie spuszczaj oczu z
ekranu, wypatrując jakichkolwiek blędow podczas uruchamiania. Istnieje
pewne narzędzie, które powinieneś posiadać, jeśli bawisz sie z kernelem
-- **rdev**, które pozwala Ci na ustawienie opcji uruchamiania dla
kernela. Pozwala ono na ustawienie np. z którego dysku ma być
uruchamiana partycja root i partycja swap, flagi partycji root
(odczyt/zapis lub tylko do odczytu), uruchamiany tryb video, rozmiar
ramdisk'u i kilka innych użytecznych rzeczy. Prawdopodobnie **nie**
będziesz musiał dopisywać nowego kernel do właściwej partycji. Jeśli
jednak, możesz to zrobić poleceniem:

```sh
rdev /dev/fd0 /dev/hda6
```

dla przykładu, jeśli Twoja partycja główna, znajduje się na /dev/hda6
skorzystasz zapewne z polecenia:

```sh
rdev -R /dev/fd0 1
```

Ustawi to partycje główną jako **tylko do odczytu** w czasie
uruchamiania, dlatego też system plików może zostać sprawdzony zanim
zostanie zamontowany. Jeśli zapomnisz podstawowych poleceń programu,
wpisz `rdev -h`, co wyświetli krótki ekran pomocy.

Teraz, gdy jesteś już pewien, że Twój nowy kernel działa -- zamontuj
swój CDROM lub otwórz połączenie PPP -- możesz pójść dalej, zrób kopię
zapasową swojego nowego, zaufanego kernela, umieść go w bezpiecznym
miejscu i skopiuj plik zImage do katalogu głównego. **Nie zapomnij
przeładować LILO** -- zwyczajnie wpisz w linni polecen `/sbin/lilo`.
Jeśli tego nie zrobisz, a jako boorloadera używasz LILO, prawdopodobnie
nie będziesz w stanie uruchomić systemu, nawet jeśli wszystko inne
zrobiłeś prawidłowo.

Drugim sposobem, jest dodanie wpisu do pliku lilo.conf, co da Ci
możliwość, za każdym razem gdy uruchamiasz komputer, na wybranie starej
wersji kernela (zazwyczaj /vmlinuz w katalogu głównym) lub nowego,
którego właśnie skompilowałeś.

Musisz edytowac swój plik **/etc/lilo.conf**. Może on wyglądać mniej
więcej tak:

```sh
# LILO configuration file
#
# Start LILO global section
append="sbpcd=0x230,SoundBlaster"   # this is for my SoundBlaster CDROM
boot = /dev/hda6            # name of device where LILO installs itself
#compact                    # faster, but won't work on my system.
delay = 5               # wait time
vga = normal                # force sane state
ramdisk = 0                 # paranoia setting
# End LILO global section
#
# My normal Linux set-up -- boot from /vmlinuz
#
image = /vmlinuz            # normal kernel in root directory
  root = /dev/hda6          # my root partition
  label = Linux             # boot time label
  read-only                 # Non-UMSDOS filesystems should be mounted read-only for checking
#
# Stanza to allow test driving of new kernel images
#
image = /usr/src/linux/arch/i386/boot/zImage
  root = /dev/hda6
  label = zImage            # what I'll call the test kernel
  read-only             # Same as above
# Linux bootable partition config ends
```

Można powiedzieć, że skonfigurowaliśmy już LILO, ale jak mogłoby się
wydawać to jeszcze nie koniec pracy. Plik ten jest jedynym jaki *z całą
pewnością* powinieneś zachować na dyskietce, jako kopię zapasową zanim
przejdziemy dalej.

Zwróć uwagę na te 5 ostatnich linijek powyższego przykładu. Musisz dodać
kilka zwrotek dla nowego testowego kernela. Wpis image określa, gdzie
znajduje się plik kernela. W tym wypadku, wskazuje katalog, w którym
znajduje się nasz nowy kernel. Całkiem ładnie, prawda?

Drugim, wymaganym wpisem jest *label*, którego LILO użyje pytając Cię,
co ma uruchomić. Pozostałe wpisy, jak root czy vga mogą nie być użyte.
Jeśli nie masz za wiele czasu i nie chcesz za wiele pozmieniać,
wystarczy, że wpiszesz tylko ścieżkę do nowego kernela oraz jego nazwę
(label), a wszystko powinno dobrze działać. Na koniec, nie zapomnij
przeładować LILO -- wpisz /sbin/lilo w linii komend, a na ekranie pokaże
się coś takiego:

```
Adding Linux *
Adding zImage
```

Kiedy już zresetujesz komputer, podczas uruchamiania LILO, wciśnij i
przytrzymaj klawisz Shift, co spowoduje wyświetlenie listy dostępnych
kerneli, możliwych do uruchomienia. Wciskając klawisz Tab możesz
przełączać się pomiędzy opcjami. No... przejdź dalej i odpal zImage i,
jeśli wszystko poszło tak, jak planowałeś, Voila! Nowy kernel. Używając
którejś z tych metod, możesz testować nowy kernel bez zakłócania swojej
dotychczasowej konfiguracji. Jeśli coś nie będzie działało jak należy,
zawsze możesz użyć dyskietki startowej lub skonfigurować LILO, by
uruchamiało stary kernel.

