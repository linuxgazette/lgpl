---
title: Dodawanie różnych funkcji dla późniejszego oszczędzenia czasu
date: 2021-04-08
---

* Autor: John M. Fisk
* Tłumaczenie: Marcin Niedziela
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.html

---

Dodamy teraz kilka pomocnych aliasów, sprawiających duża frajdę i
będących wielce pomocnymi... Dodajemy funkcję do pliku
\~/.bash\_profile.

No tak, ale czym jest funkcja -- to kilka linii kodu, który przejmuje
dane wejściowe i zwraca je jako wynik swojego działania. Możesz ustawić
sobie te same rzeczy, z tym, że w nico łatwiejszy sposób.

W shell'u **bash**, funkcje działają jako skrypty, definiowane na
przykład tak:

```sh
Say_hello()
{
    echo "Witaj Świecie"
}
```

Teraz, gdy wywołasz funkcję Say\_hello, otrzymasz wynik jej działania,
czyli wyświetlony zostanie tekst " Witaj Świecie ". Trochę amatorskie,
prawda? Cóż, nie tak szybko. Możesz użyć tego w swoim pliku
\~/.bash\_profile, by łatwo ustawić wiele użytecznych funkcji. Ciekawy
jesteś jakich? Jeśli często używasz polecenia **tar**, wtedy pomocne
może być dodanie czegoś takiego:

```sh
# Now, some handy functions...
#
tarc () { tar -cvzf $1.tar.gz $1 }
tart () { tar -tvzf $1 | less }
tarx () { tar -xvzf $1 $2 $3 $4 $5 $6 }
popmail () { popclient -3 -v -u myname -p mypassword -o /root/mail/mail-in any.where.edu }
zless () { zcat $* | less }
z () { zcat $* | less }
```

do Twojego pliku `~/.bash\_profile`. Teraz, jeśli będziesz zmuszony
wpisać `tar -xvzf jakas_bardzo_dluga_nazwa_pliku-3.1415926535-bin-Motif-static.tar.gz`
wystarczy, że wpiszesz `tarx jakas_bardz` i wciśniesz klawisz TAB, który
dokończy nazwę pliku (to, że UN\*X obsługuje 256-znakowe nazwy plików,
*nie* znaczy, że musisz *używać* tych wszystkich 256 znaków). Funkcja
zajmie się resztą i na pewno przyda się w wielu innych, gorszych
sytuacjach, gdy będziesz musiał wpisać bardzo długie polecenie.

Dla krótkich funkcji możesz łatwo ustawić aliasy. Ktoś bardziej znający
UN\*X mógłby tu podać lepsze lub bardziej efektowne sposoby. Faktem
jest, że podane przeze mnie działają i pozwalają na zaoszczędzenie WIELE
czasu, jeśli WIĘKSZOŚĆ swojej pracy przed komputerem spędzasz przy
klawiaturze.

Jeszcze jedna uwaga... możesz użyć tych pomysłów, by zaoszczędzić nieco
przestrzeni dyskowej. Pytasz jak?

Jeśli kompresujesz pliki dokumentacji poleceniem **gzip -9**
(prawdopodobnie nie chcesz tego robić ze swoim plikiem /etc/inittab),
wtedy do łatwego przeglądania dobrze jest napisać dla przykładu funkcję
lub alias: " z ", przedstawianą powyżej. **zcat** pozwoli na
przeglądanie skompresowanego pliku umieszczając go w **less** (jeśli
wolisz **more**). Na moim komputerze, opcja ta jest ustawiona w
szczególności dla największych plików.

