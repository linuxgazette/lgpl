---
title: Zabezpieczanie systemowych plików konfiguracyjnych
date: 2021-04-08
---

* Autor: John M. Fisk
* Tłumaczenie: Marcin Niedziela
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.html

---

Spodziewam się, że *większość ludzi* używających Linuksa, w swojej
opinii, lepiej lub gorzej administrują swoją stacją roboczą. Tak, fajnie
jest być root'em, ale to może również być niezłym *kopem w tyłek*.

**Oto następne dwie rady, by ułatwić sobie życie.**

Jeśli używasz Linuksa już od dłuższego czasu, prawdopodobnie wprowadzasz
już pewnie **zmiany w działaniu** systemu. Jeśli jeszcze tego nie
zrobiłeś, to powinieneś stworzyć w /root jakiś podkatalog i umieścić tam
kopie WSZYSTKICH systemowych plików konfiguracyjnych zainstalowanych w
Twojej dystrybucji.

Utwórz więc, podkatalog /config\_dist i zacznij umieszczać w nim różne
pliki:

* /etc/inittab
* /etc/profile
* /etc/rc.d/rc.\*
* /etc/passwd
* /usr/X11R6/lib/X11/XF86Config

wtedy, w przypadku padnięcia systemu, gdy coś sknociłeś, będziesz miał
kopie zapasową pliku, by móc zastąpić jego oryginalną wersję.

Bądź przygotowany.

Jeszcze jedna mała sugestia...

Po tym jak stworzysz podkatalog dla oryginalnych plików
konfiguracyjnych, utwórz inny podkatalog o nazwie **/links**. Teraz, do
wszystkich zagnieżdżonych plików konfiguracyjnych, *stwórz łącza
symboliczne w katalogu /links*.

Jeśli potrzebujesz kontrolować ustawienia PPP, stwórz łącze do
/etc/ppp/options lub /usr/lib/ppp/pppon:

```sh
cd ~/links
ln -s /etc/ppp/options ppp_options
ln -s /usr/lib/ppp/pppon pppon
```

Teraz, gdy wejdziesz do katalogu /links, znajdziesz tam dowiązanie
symboliczne do swojego pliku konfiguracyjnego. Jeśli będzie potrzeba
dokonać w nim pewnych zmian, możesz to w łatwy sposób zrobić z każdego
miejsca w systemie wpisując **vi \~/links/ppp\_options**. Mając
wszystkie pliki konfiguracyjne zlinkowane w jednym katalogu wprowadzanie
zmian jest bardzo proste. Kilka opisów zawartości plików:

* /etc/printcap -- gdy musisz wprowadzić zmiany w pliku printcap
* /var/spool/lpd/filter\_myprinter -- gdy chcesz utworzyć filtr wejścia
* /etc/rc.d/rc.\* -- całkiem sporo ilość ustawień
* /usr/lib/ppp/ppp\* -- ibid.
* /usr/X11R6/lib/X11/fvwm/system.fvwmrc -- całość rzeczy jakie możesz zrobić w FVWM.

Mała rada. Za każdym razem, gdy dokonujesz jakiś czynności pamiętaj o
tych zasadzach:

1. twórz kopie oryginalnych plików nazywając je: `oryginalna_nazwa_pliku.dist`
2. kopiuj oryginalna\_nazwa\_pliku.dist do swojego katalogu /config\_dist\
3. wszystkie zmiany rób dla własnej wygody
4. twórz dowiązania symboliczne do nowych plików konfiguracyjnych w katalogu \~/links\
5. ciesz się tym co zdziałałeś lub naprawiaj to co zepsułeś

Zaraz... po co bawić się tym wszystkim i robić to ręcznie jeśli można
napisać prosty skrypt, który BARDZO ułatwi nam życie.

**Mała rada**: jeśli nie lubisz robić czegoś na piechotę, pomyśl
poważnie o pisaniu skryptów shell'owych, automatyzujących niektóre
zadania. Pamiętaj: nie używasz DOS'a, **to jest Linux**. Spróbuj czegoś
takiego:

```sh
#!/bin/sh
#
# program:  hack
# author:   johnMfisk   7 july 1995
# purpose:  tworzy kobie i dowiazania symboliczne do ważnych plików systemowych.
#
CURR_DIR=`pwd`

cp $1 "/root/config_dist/$1.dist"
ln -s "$CURR_DIR/$1" "/root/links/$1"
vi $1
exit
```

Gdy skończysz, nie zapomnij wpisac `chmod 700 hack`. Teraz, za
każdym razem, gdy będziesz potrzebował nowych plików konfiguracyjnych,
wystarczy wpisać:

**hack important\_sys\_file.cfg**

Program hack stworzy kopię zapasową w katalgu /config\_dist z dopisem "
.dist ", potem stworzy dowiązania symboliczne do plików i umieści je w
katalogu /links; i ostatecznie, uruchomi **vi** (lub jakikolwiek inny
edytor, który wolisz), byś mógł dokonać jakichkolwiek zmian. To tylko
prosty przykład, a możesz z niego łatwo korzystać, testując pliki
znajdujące się w w Twoim katalogu /config\_dist, nie nadpisując ich
jeżeli już istnieją lub uzyskiwać praw do ich kopiowania.

