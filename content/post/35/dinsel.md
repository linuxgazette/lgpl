---
title: Recenzja Klawiatury Happy Hacking
date: 2023-02-22
---

* Autor: Jeremy Dinsel
* Tłumaczył: Marcin Niedziela
* Original text: https://linuxgazette.net/35/dinsel.html

---

- Producent: PFU America Inc.
- E-mail: hhkb-support@pfuca.com
- URL: http://www.pfuca.com/
- Cena: $139 z jednym przewodem, $30 za dodatkowy przewód
- Recenzent: Jeremy Dinsel

Klawiatura Happy Hacking to ładna i niewielka klawiatura zaprojektowana
specjalnie z myślą o programistach. Jej niewielki rozmiar sprawia, że
wydaje się wręcz urocza, zupełne przeciwieństwo do standardowych
klawiatur, jakie dostarczane były z komputerami IBM.

![](./gx/3147f1.jpg)

Nawiązując do PFU America, projekt klawiatury sprawia, że programistom
łatwiej i efektywniej jest dosięgnąć każdego jej klawisza. Twierdzą,
że mniej klawiszy zwiększa efektywność poprzez wykluczenie konieczności
sięgania palcami do skrajnych klawiszy.

## Instalacja

Klawiatura dotarła do mnie w malutkim opakowaniu już w kilka dni po tym,
jak zgodziłem się na napisanie tej recenzji. W pudelku znajdziemy
klawiaturę oraz trzy kable (ze złączami PS2, Macintosh oraz dla komputerów
Sun) oraz podręcznik użytkownika i kartę gwarancyjną.

Firma PFU America niedawno zmieniała opakowanie oraz obniżyła cenę. Klawiatura
wysyłana jest teraz z jednym kablem (na podstawie wyboru klienta), a pozostałe
kable dostępne są za 35 dolarów każdy. Kable są drogie, ponieważ są ręcznie
robione poprzez pracowników PFU America.

Instrukcja jest bardzo prosta, chyba prawie każdy wie jak 
należy poprawnie podłączyć klawiaturę. Jednakże, biorąc pod uwagę mnogość
różnych przewodów, dobrze wiedzieć, że dokumentacja jest dostępna, gdyby
była potrzebna.

Po wyłączeniu komputera odłączyłem moja stara klawiaturę, aby szybko
podłączyć moja nowa Happy Hacking. Myślałem, że będzie szybko, ale
musiałem jeszcze poszukać przejściówki PS/2 do AT.

## Życie to seria poprawek

Klawiatura jest bardzo niewielka, zawiera tylko 60 klawiszy. Klawisz
funkcyjny pozwala na stosowanie kombinacji z innymi klawiszami; w rezultacie,
niewygodne ustawienie palców jest czasem konieczne. W pierwszych dniach
korzystania z klawiatury czułem się jak w grze twister, kiedy to chciałem
dosięgnąć czerwonej kropki przeciskając sie pomiędzy dwóch innych graczy,
cały czas trzymając stopy na pomarańczowej i niebieskiej kropce po drugiej
stronie maty. Po dwóch tygodniach nadal zdarzało się, że wracałem do nawyków
ze starej klawiatury.

Na początku nie działał również klawisz backspace, jednak okazało się, że
to głównie moja wina. Będąc leniwym i podekscytowanym testowaniem nowej
klawiatury nie przeczytałem całej instrukcji do ostatniej (trzeciej) strony,
gdzie znajduje się tabela i towarzyszący jej rysunek, które pokazują jak
zaprogramować klawiaturę za pomocą przełącznika suwakowego. W końcu udało mi
się przełączyć przełącznik, a wtedy klawisz backspace działał w oczekiwany sposób.

Ponieważ używam Linuksa, zanim jeszcze powstał Windows 95 (przestałem
używać produktów MS na długo przed tym), nie brakowało mi dodatkowych
klawiszy "Windows" znajdujących się na większości klawiatur PC. Musiałem jednak
przyzwyczaić się do pracy na konsoli z nową klawiaturą. Przełączanie się z X na
konsole wymaga kombinacji czterech palców i klawiszy ('ctrl-alt-fn-f*',
gdzie fn to klawisz funkcyjny), natomiast poruszanie się po konsolach wymaga
kombinacji trzy palców/klawiszy ('alt-fn-klawisz strzałkowy').

W edytorach innych niż **vi** gdzie do poruszania kursorem korzystać trzeba
z klawiszy strzałek, klawiatura Happy Hacking Keyboard wymaga, aby użytkownik
przyzwyczaił się do znajdowania położenia strzałek i pamiętania o trafieniu w
klawisz funkcyjny. W moim przypadku nauczenie się tego zajęło mi mniej niż
tydzień (komicznie wyglądają próby błądzenia po klawiszach osób, które nigdy
nie korzystały z tej klawiatury).

Inaczej niż na laptopach, wielkość i kształt klawiszy jest taki sam jak w
innych klawiaturach PC, co sprawia, że łatwiej jest się przestawić. Nigdy
nie zdarza mi nie trafić w dany klawisz i nie mam problemów z pisaniem
na innych klawiaturach (niewyposażonych w klawiaturę Happy Hacking).
Zacząłem jednak ostatnio marudzić na temat tego, jak dziwne są inne klawiatury.

## Happy Hacking

Podczas gdy klawiatura nie wyleczyła mnie z mojej sarkastycznej natury,
to klawisz escape jest o wiele łatwiejszy do wciśnięcia, ponieważ znajduje
się bezpośrednio na lewo od klawisza "1". W vi mogę szybko przełączyć się
z trybu wstawiania, ponieważ nigdy nie muszę patrzeć w dół, aby zlokalizować
klawisz escape lub zmieniać położenie palców; dzięki temu poruszanie się po
vi stało się jeszcze łatwiejsze.

W przypadku programowania XEmacs klawisz Control znajduje się w poprawnym
w miejscu, bezpośrednio na lewo od klawisza "A". Dzięki temu można go łatwo używać
bez żadnych dziwnych ruchów lub odrywania palców od głównego rzędu
(Tak, nauczyłem się pisać na maszynie, zanim nauczyłem się programować).

Oba te klawisze, Escape i Control, pozwoliły mi na szybkie wykonywanie poleceń
bez konieczności zmiany położenia palców. To ma tę zaletę, że zmniejsza
frustrację związaną z próbą powrotu do rzędu klawiszy głównych po każdym
poleceniu - moje palce nigdy nie znajdują się w dziwnych miejscach, jak to miało
miejsce na typowej klawiaturze PC.

## Niezadowolony gracz

Jako półetatowy gracz (Linux Quake), jestem przyzwyczajony do używania
klawiatury do wszystkich ruchów, takich jak obroty i bieganie. W przypadku tej
klawiatury musiałbym ciągle trzymać wciśnięty klawisz funkcyjny (aby wybrać
klawisze strzałek) lub nauczyć się, jak używać myszy. W przeciwnym razie trzymanie
klawisza funkcyjnego (dwa klawisze od klawiszy strzałek) i próbowanie
manipulowania strzałkami może zwiększyć prawdopodobieństwo rozwoju zespołu cieśni
nadgarstka.

Po kilku grach w Quake'a myślę, że będę się dobrze czuł z tym dziwacznym
ułożeniem palców. Używanie klawiatury do programowania w XEmacs pomogło
w dostosowaniu się do świata gier.

## Wsparcie techniczne i dokumentacja on-line

Dokumentacja produktu dostępna jest on-line. Mimo, że nie korzystałem
z ich wsparcia poprzez email, to warto wiedzieć, że jest to mozliwe -
gdy kontaktowałem się z PFU America, odpisywali na każdą wiadomość
bardzo szybko. Dodatkowo wszystkie informacje potrzebne do instalacji
i podłączenia klawiatury są dostępne on-line. Zawartość dołączonej
do produktu dokumentacji jest dostępna w całości w sieci.

## Na zakończenie

Dla kogoś, kto nie miał do czynienia z tą klawiaturą, trudno uwierzyć
we wszystko, co o Happy Hacking Keyboard mówi sam producent.
W rzeczywistości byłem sceptycznie nastawiony do uwag, które słyszałem,
zanim sam zostałem użytkownikiem Happy Hacking Keyboard. Teraz, po miesiącu
od położenia na niej palców nie mogę sobie wyobrazić używania żadnej
innej klawiatury. Zastanawiam się, czy PFU America produkuje torbę Happy Hacking.
