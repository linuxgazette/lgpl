---
title: Przyśpiewki w tonacji Pingwina -  KGuitar
date: 2021-10-15
---

* Autor: Jimmy O'Regan
* Tłumaczył: Marcin Niedziela
* Original text: https://linuxgazette.net/131/oregan.html

---

Minęło sporo czasu od mojego ostatniego artykułu - "Przyśpiewki w tonacji
pingwina". Od tego czasu, szczerze mówiąc, przestałem używać linuksowych
programów do wyświetlania tabulatur.

Głównym problemem jest to, że jako gitarzysta metalowy, *muszę* mieć 
możliwość oznaczania [mutowania](http://en.wikipedia.org/wiki/Palm_muting) 
w moich tabulaturach — w przeszłości dzieliłem się z innymi swoimi
tabulaturami, jednak brakowało im informacji kiedy należy mutować struny.
Kończyło się tym, że musiałem im pokazywać jak grać poszczególne riffy, zatem
istnienie tabulatury stało się niepotrzebne.

Całe szczęście, udało mi się w końcu pobawić z 
[KGuitar](http://kguitar.sourceforge.net/), który wspiera oznaczenia mutowania.

## KGuitar

Oprócz wyciszania, KGuitar posiada wiele innych wspaniałych funkcji. Aplikacja
potrafi importować pliki Guitar Pro, aż do wersji 4; posiada wizualną
podstrunnicę, dzięki czemu możesz zobaczyć jak dany akord będzie wyglądał na 
gryfie gitary; analizator akordów (jak w Guitar Pro), który daje Ci możliwość
analizator akordów (jak w Guitar Pro), który daje Ci możliwość wyboru 
palcowania na podstawie nazwy akordu oraz podaje nazwę akordu na podstawie
wyglądu akordu, oraz co wydaje się być unikalne dla KGuitar: możliwość
wystukiwania rytmu taktu za pomocą myszy lub klawiatury.

![Virtual fretboard](/post/131/misc/fretboard.png)

### Wirtualny gryf w KGuitar
Jeśli kiedykolwiek korzystałeś z edytora tablatury działającego pod Windows, 
KGuitar wyda Ci się znajomy. Większość edycji odbywa się za pomocą klawiatury: 
za pomocą klawiszy strzałek przechodzisz do sekcji tabulatury, którą chcesz 
edytować, i wpisujesz numer progu nuty, którą chcesz dodać.

## Konstruktor akordów

Konstruktor akordów jest jedną z najciekawszych funkcji w KGuitar. 
Pozwala on na skonstruowanie akordu poprzez wybranie nuty głównej i edycję 
typu akordu za pomocą trzech list znajdujących się w lewym górnym rogu okna. 
Następnie wyświetlana jest listę ikon możliwych palcowań w dolnej części okna.

Aby uruchomić Konstruktor Akordów, wybierz "Nuta->Wstaw->Akord", lub
wciśnij **Shift-C**:

![Konstruktor Akordów](/post/131/misc/chord-constructor.png)

Dla tych, którzy nie znają teorii muzyki, pozwala na jej podrobienie:
po prostu kliknij na odpowiednie pasmo w okienku akordów na środku okna,
a po prawej stronie pojawi się lista możliwych nazw akordów. Możesz następnie 
użyć jej do znalezienia alternatywnych palcowań: możesz po prostu
znaleźć łatwiejszy sposób na granie swoich utworów!

### Rhythm Constructor

Rhythm Constructor to ciekawy pomysł i całkiem przydatne narzędzie dla tych
gitarzystów, którzy nie potrafią czytać muzyki z nut. Pozwala łatwo wprowadzić
nuty dla taktu (lub kilku taktów), potem wystarczy wybrać Note > Insert Rhythm 
(ewentualnie Shift+R), a następnie wystukaj rytm za pomocą myszki; wciśnij
przycisk Tap, a gdy skończysz - Quantize.

![Konstruktor rytmów](/post/131/misc/rhythm-constructor.png)

Wypróbowałem go używając [prostego przykładowego pliku](/post/131/misc/rhythm1.kg)
([MIDI](/post/131/misc/rhythm1.mid). [Wynik](/post/131/misc/rhythm2.kg) ([MIDI](/post/131/misc/rhythm2.mid)
był inny, ale winne jest tu moje klikanie myszką, nie KGuitar. Jestem pewien, 
że przy odrobinie wprawy, może to być całkiem użytecznym narzędziem.

### Import z TuxGuitar

[TuxGuitar](http://www.tuxguitar.com.ar) jest programem tabulaturowym, napisanym
w Javie, który może importować pliki z Guitar Pro 5. Mimo, że nie posiada
tylu opcji co Guitar Pro, świetnie nadaje się do importowania jego plików
(choć, napotkałem pewien problem, patrz poniżej).

Twierdzi również, że jest w stanie eksportować pliki w formatach Guitar Pro 
3 i 4, chociaż każda próba zaimportowania przeze mnie tych plików do KGuitar,
powodowała wyświetlenie poniższego na ekran:

```
kguitar: WARNING: readDelphiString - first word doesn't match second byte
kguitar: WARNING: Chord INT1=-1, not 257
kguitar: WARNING: Chord INT2=-1, not 0
kguitar: WARNING: Chord INT4=-1, not 0
kguitar: WARNING: Chord BYTE5=255, not 0
```

Napotkałem też dziwny błąd podczas importowania tablatury jednego z moich
ćwiczeń: wersja TuxGuitar [GP4](/post/131/misc/exercise.gp4) lub
[KGuitar](/post/131/misc/exercise.kg).

![TuxGuitar bug](/post/131/misc/tuxguitar-bug.png)

Dla tych, którzy nie potrafią czytać tabulator czy nut wyjaśniam, że tabulatura
nie pasuje do zapisu nutowego. Powinno to wyglądać w ten sposób (wyeksportowane
do ascii przy pomocy KGuitar i lekko poprawione)

```
E|--------------------------|-------------------------|
B|--------------------------|-------------------------|
G|--------------------------|-------------------------|
D|--------------------------|-------------------------|
A|-------7-----------10-----|------7-----------10-----|
D|-0-7-8---8-7-0-7-8----8-7-|0-7-8---8-7-0-7-8----8-7-|
```

Można by pomyśleć, że jest to problem z czcionką; że 0
z linią przez nią i 8 wyglądają podobnie, ale odtwarzanie w TuxGuitar
podąża za linią tablatury i gra D (0) zamiast A# (8).

Kiedy uda mi się skonfigurować Javę w moim komputerze, to w kolejnych 
artykułach przyjrzę się bliżej TuxGuitar 
(i [DGuitar](http://dguitar.sourceforge.net), opartej na Javie przeglądarce 
Guitar Pro). Do tego czasu, polecam wypróbowanie [kilku plików](/post/131/misc/gp5.tar.bz) 
z TuxGuitar. Niektóre z nich są dość skomplikowane, więc powinny dać dobry 
przykład tego co potrafi TuxGuitar.

