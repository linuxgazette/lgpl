---
title: Sztuczka z ZLess
---

* Autor: Werner Fleck
* Tłumaczył: Marcin Niedziela
* Original text: https://linuxgazette.net/issue01to08/lg_issue6.html

---

Cześć!

Po przeczytaniu twoich sztuczek z less w "Saving with a Floppy Fibrary" oraz w 
"Less is just a Whole Lot More!" w listopadowym Linux Gazette, chciałem
podzielić się z Tobą kilkoma swoimi sztuczkami.

Jeżeli szukasz informacji w kilku plikach, jest na to prostszy sposób niż użycie
grepa i potem listowanie wyników z użyciem lees: użyj less to grepowania plików.
Uruchom less z listą plików, np: `less \*` i wpisz `/\* wyrazenie regularne`.
less poszuka wtedy wyrażenie we wszystkich plikach, wciskaj `n` aby przejść do
kolejnego wyniku.

Jest także lepszy sposób na przeglądanie skompresowanych plików, niż używanie 
`zcat \*.gz | less` czy `zless \*.gz` --użyj less i jego opcji strumieni.

Na początek, potrzebujemy poniższego skryptu w `/usr/local/bin/lesspipe`:

```sh
#!/bin/sh

case $1 in
  *.gz|*.tgz)   CMD='gzip -d -c -q';;
  *.Z)          CMD='uncompress -c';;
  *.zip)        CMD='unzip -c -qq';;
  *.zoo)        CMD='zoo xqp';;
  *.arc)        CMD='arc xpn';;
  *)            unset CMD;;
esac

if [ -n "$CMD" ]; then
  $CMD $1 2> /dev/null
fi
```

i oznacz go jako wykonywalny: `chmod +x /usr/local/bin/lesspipe`. Następnie
ustaw zmienną środowiskową: `setenv LESSOPEN "lesspipe %s "` dla c-shela lub 
`LESSOPEN "lesspipe %s" export LESSOPEN` dla bash. Teraz, gdy będziesz chciał
podejrzeć plik file.gz, less przekieruje jego zawartość do gzip, a ty możesz
robić cokolwiek chcesz zupełnie jak z nieskompresowanymi plikami, np: użyj `:p`
lub `:n` aby zmienić pliki lub `/` ` \*` aby je przeszukiwać.

Werner

P.S. Linux Gazette jest świetna\!

    --
    Werner Fleck                    *
    Phone: +49 40 2 54 59 170       *     Werner.Fleck@hamburg.netsurf.de
    Fax:   +49 40 2 54 59 199       *              Fleck@prompt.de         
    Home:  +49 4104 8 08 86         * 

\[Ponownie, mam wielki dług wdzięczności dla wasz wszystkich, którzy przesyłają
mi sugestie i wyjaśnienia. Dzięki Werner za ten jasno wytłumaczony pomysł. To
jest znakomita metoda obsługiwania skompresowanych plików - podobna do pomysłu
nadesłanego przez Paula w sekcji o Emacsie -- John\]
