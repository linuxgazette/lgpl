# Recenzja Caldera Network Desktop

Autor: Edward Cameron
Tłumaczył: Marcin Niedziela
Oryginalny tekst: https://linuxgazette.net/issue01to08/lg_issue6.html#caldera

---

Wielu ludzi pisało i pytało o recenzje różnych dystrybucji Linuksa: 
Slackware, RedHat, Debian, Yggdrasil, Linux-FT, i innych. 
Ponieważ, szczerze mówiąc, nie mam czasu, miejsca na dysku, ani
technicznej wiedzy aby to zrobić, chciałem podziękować Edwardowi Cameronowi
za łaskawą ofertę zrecenzowania jednego z tych wspaniałych produktów: 
Caldera Network Desktop firmy Caldera Corporation.

Ed bardzo uprzejmie przesłał kompletny zestaw dokumentów HTML, które można 
znaleźć w całości pod powyższymi linkami. **Recenzja CND Review** zawiera
w komplecie kilka bardzo ładnych zrzutów ekranu różnych części
pulpitu, które dadzą Ci pewne wyobrażenie o produkcie. Strona **Web Surfing** 
zawiera kilka interesujących odnośników do wtyczek Netscape'a.
**E-Mail i Linux** zawiera kilka przydatnych sugestii dotyczących konfiguracji
**popclient** i **PINE** do obsługi poczty elektronicznej pod Linuksem.

Jeśli masz uwagi, sugestie, pomysły lub po prostu chcesz porozmawiać o
tym programie, nie wahaj się proszę i napisz do Eda! Można się z nim 
skontaktować pod adresem email: ecame119@ecity.net

Jeśli masz doświadczenie w konfiguracji lub korzystania z innej dystrybucji 
Linuksa, napisz do mnie o tym. Jedynym zastrzeżeniem jest to, że poważne 
OS-bashing będzie uważane za nie-nie. (wszystkie słowne utarczki będą kierowane 
na grupę comp.os.linux.advocacy :-)

Jeżeli ktoś ma jakieś doświadczenia z używania jakiejkolwiek dystrybucji
Linuksa, proszę o informacje.

Bawcie się dobrze! --John
