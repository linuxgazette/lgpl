---
title: Sztuczki z knews i crontab
---

* Autor: Bill Powers
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue01to08/lg_issue6.html

---

Na początek pozwól, że pochwalę Twoje pomysły. Znalazłem tu wiele użytecznych
informacji o rzeczach, za które warto się zabrać i trochę o nich poczytać.
Szczególnie lubię pomysł z czyszczeniem ekranu przy wylogowaniu za pomocą 
`trap /usr/bin/clear EXIT`. Barto eleganckie rozwiązanie.

Pomyślałem, że dorzucę się kilka sowich pomysłów. Kiedyś bardzo długo szukałem
klienta grup dyskusyjnych z pełnym wsparciem dla wątków, który działał będzie
poprzez połączenie szeregowe. Coś co będzie działało tak samo dobrze jak program
tin poprzez sieć. Już myślałem, że nigdy nic takiego nie znajdę, gdy nagle
natrafiłem na "knews". Można go znaleźć na zasobach sunsite w podkatalogu
system/Printing, jest łatwy do konfiguracji, a po dodaniu kilku wpisów w
.Xdefaults, jest dość szybki. Jeśli znajdziesz czas aby go wypróbować, dodaj
poniższe ustawienia:

```
Knews.*.retrieveDescriptions:   false
Knews.*.readActiveFile: false
Knews.*.fillNewsrcFile: true
Knews.*.threadEmAll:    true
```

Jedną z fajniejszych rzeczy w Linuksie jest to, że zawsze znajdzie się więcej niż
jeden sposób wykonanie jakiegoś zadania. Chciałem sprawdzać pocztę co kilka
minut, gdy jestem połączony z internetem, ale zastosowałem zupełnie inne
podejście niż ty. Dodałem poniższą linijkę do mojego skryptu połączenia z
internetem:

```
crontab ~/.crontab 
```

Następnie dodałem poniższą linijkę do pliku .crontab w moim katalogu domowym:

```
*/5 * * * * /root/mailman > /dev/console 2>&1
```

`/root/mailman` jest kolejnym jednolinijkowcem odpalającym program popclinet do
pobierania poczty do katalogu mail użytkownika. Trzymam go w `/root` ponieważ
zawiera on moje hasło i nazwę użytkownika. `/root` ma _bardzo_ restrykcyjne
uprawnienia. Przekierowanie wyjścia jest bardzo ważne, gdyż inaczej crontab
wysłałby maila do użytkownika root za każdym razem, gdy coś pójdzie nie tak.
Jeżeli korzystasz z xbiff lub xmailbox (którego używam od czasu, gdy
przeczytałem o nim w twojej gazecie), możesz przekierować wyjście do `/dev/null`
zamiast `/dev/console`. 

Jeden ważny aspekt schematu sprawdzania poczty w crontab: aby wyłączyć go kiedy
zamykasz połączenie ppp, musisz dodać poniższą linie do skryptu ip-down:

```sh
crontab -d $USER
```

Domyślam się, że składanie Gazety to ciężka praca. Chciałbym abyś pamiętał, że
jest ktoś, kto to bardzo docenia. 

[Cóż, muszę przyznać, że nie często zmieniam przyzwyczajenia. Kilka miesięcy
temu zacząłem używać skim/xskim praktycznie codziennie do pobierania newsów.
Nie próbowałem **knews**, ale bardzo dziękuję Billowi za tę sugestię, jak
również te dotyczące crontaba! -- John]

