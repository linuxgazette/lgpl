---
title: PINE + popclient - poczta dla sendmaila-impaired
date: 2022-02-09
---

* Autor: John M. Fisk
* Tłumaczenie: Marcin Niedziela
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.aug.html

---

Jeśli korzystasz z komputera, który nie jest podłączony do sieci -
jednak masz dostęp do serwera poczty SMTP (ang. Simple Mail Transfer
Protocol - Prosty Protokół Przesyłania Poczty), który jest w stanie
współpracować z POP (ang. Post Office Protocol - Protokół Pocztowy),
możliwe jest wtedy wysyłanie i otrzymywanie poczty **bez konieczności
konfigurowania sendmaila**.

Jeśli na samą myśl o konfigurowaniu sendmaila dostajesz bólu brzucha i
już bardziej wolałbyś zjeść zgnite jajko...

**czytaj dalej.**

Istnieje możliwość ustawienia **PINE** tak, aby był naszym klientem
SMTP, a jego zadaniem będzie wysyłanie poczty do serwera SMTP, a
**popclient** zostanie użyty do obierania przychodzącej poczty z serwera
poczty POP. Całe piękno tego typu rozwiązania polega na tym, że nie
musisz bawić się w konfigurację sendmaila, by odbierać pocztę spoza
Twojego komputera. Jeśli nie wymagasz zbyt wiele od emaila, to jest to
warte uwagi. By to zrobić, prawdopodobnie będziesz musiał zrobić parę
innych rzeczy.

Na początek, będziesz potrzebować kopii źródełek do programu PINE, które
musisz samodzielnie skompilować.

To wcale nie jest trudne. SZCZERZE.

Zaufaj mi.

Zdobądź najświeższe źródła PINE -- powinna to być wersja 3.91, która nie
zajmuje więcej niż jakieś 2MB przestrzeni dyskowej. Jego kopia zazwyczaj
znajduje się w katalogu /pub/Linux/system/Mail/readers na Twoim
ulubionym serwerze lustrzanym sunsite.unc.edu. Możesz też pobrać go ze
strony producenta. Oto zawartość pliku "brochure.txt" będącego częścią
pakietu pine:

> Pine, Pico, i serwer UW IMAP są wolnodostępne, jednak posiadają prawa
> autorskie. Ostatnia wersja, zawierająca kod źródłowy, znajduje się w
> Internecie pod adresem "ftp.cac.washington.edu" w pliku
> "pine/pine.tar.Z" (dostęp za pośrednictwem anonimowego serwera FTP).

Jak już go zdobędziesz, rozpakuj go za pomocą swojego ulubionego
oprogramowania i zacznij z nim prace. Trochę do poczytania w celu
poznania więcej szczegółów znajdziesz w podakatalogu /doc, a dokładnie w
pliku "tech-notes.txt". Szczegółowo opisuje on podstawy PINE, zarys
imponującej listy możliwości, a także opisuje to czego potrzebujesz aby
go zainstalować. Ale nie pękaj, obiecuję, że to będzie łatwe.

Głównym powodem, wg którego powinieneś samodzielnie kompilować PINE,
jest możliwość rozwiązania prostego problemu: pola "From:". Widzisz,
większość programów pocztowych takich jak elm, pine, xmail, xmh, mailx
są na tyle "inteligentne", że same domyślają się kim jesteś i gdzie
jesteś. Jeśli dla swojego komputera ustawisz zmienna hostname, to
programy te automatycznie przejmą te dane i dodadzą je do Twojego
loginu.

Dla przykładu: z wielkim wstydem to mówię, ale większość pracy na
LINUKSIE wykonuje jako root. Wiecie przecież... to daję władzę. No ale,
kiedy wysyłam pocztę z mojego komputera korzystając z pine moje pole
FROM: jest automatycznie ustawione na coś w tym stylu:

``` 
From: root@FiskHaus.vanderbilt.edu
```

Nie byłoby w tym nic złego, chyba, że dla kogoś kto chciałby wysłać do
mnie ODPOWIEDZ. No będzie miał mały problem:

Podany adres nie istnieje. Mój "prawdziwy" adres wygląda wygląda nieco
inaczej, a mianowicie coś jakby *dialup0032@ctrvax.Vanderbilt.Edu*.

Sposobem na poprawienie tego, jest przekompilowanie PINE. No wiesz, PINE
pozwala Ci na zmodyfikowanie pola "From: " jeśli jest to ujęte w opcjach
kompilacji. Zazwyczaj, nie jest to niewskazaną cechą, jednak pozwala
zamaskowanie się kogoś jako my i przeprowadzenie antyspołecznego ataku.
Jednakże, jeśli czytasz ten tekst, na pewno nie dasz się wprowadzić w
maliny.

Oto jak w prosty sposób przeprowadzić poprawną kompilację PINE:

1. rozpakuj archiwum  
  
2. jeśli to wymagane, edytuj plik /pine3.91/pine/makefile.lnx.
Zawiera on wszystkie opcje kompilacji, takie jak na przykład opcje
profilaktyczne czy śledzenia błędów. Jeśli jesteś jednym z tych
zarośniętych bujnymi czuprynami, brodatych hakerów... pobaw się nim.
Dla reszty z nas, nie należy za wiele tam zmieniać.  
  
3. edytuj plik /pine3.91/pine/osdep/os-lnx.h -- to bardzo ważny plik.
Zawiera on definicje większości możliwych do ustawienia opcji. Mimo
możliwości ustawienia większości opcji za pomocą menu Opcji, istnieje
kilka rzeczy, które mogą być ustawione tylko przed kompilacją, a
modyfikacja pola From: jest jedną z nich. Wystarczy że odkomentujesz
jedną z definicji. Oto jak ona wygląda:

```c
#define ALLOW_CHANGING_FROM
```

Mądre chłopaki z Uniwersytetu Waszyngtona przygotowali dobrze oznaczony
nagłówek pliku, który wyjaśnia co oznacza większość z tych opcji. Jest
to zapewne najważniejsza rzecz z tych, jakie tu robimy.  
  
4. jak już pozmieniasz te dwa pliki, jesteś gotowy do kompilacji
(widzisz *mówiłem*, że to będzie proste). Przejdź teraz do katalogu
głównego, czyli do /pine3.91, gdzie znajdziesz skrypt *build*. Aby
wykonać kompilację, najprościej wpisz te polecenia:

```sh
build clean
build lnx
```

to wszystko. Kiedy na ekranie skończą mrugać różne komunikaty, w
katalogu /pine3.91/bin znajdziesz kilka świeżutkich lśniących programów,
w których oczywiście będzie także **pine**. Możesz go przetestować, a
potem skopiować go do katalogu /usr/local/bin/ -- lub gdziekolwiek
instalujesz swoje programy.  
  
5. Ustaw teraz właściwie pole "From:", uruchom pine, a potem wciśnij
klawisz "s", by przejść do menu Setup. Wejdź w sekcję "Config". Poniżej
znajduje się przykład najprostszej konfiguracji:

Moja konfiguracja wygląda mniej więcej tak:

```
    personal-name          = John M. Fisk 
    user-domain            = vanderbilt.edu
    smtp-server            = ctrvax.vanderbilt.edu
    nntp-server            = news.vanderbilt.edu
    inbox-path             = /root/mail/mail-in
    folder-collections     = 
    news-collections       = 
    default-fcc            = 
    postponed-folder       = 
    read-message-folder    = mail-in       
    signature-file         = 
    global-address-book    = 
    address-book           = 
```

Bardzo ważnym wpisem jest zmienna smtp-server. Jeśli będzie ona
wskazywać na nieprawidłowy adres IP lub nazwę hosta serwera poczty
SMTP, to PINE zwyczajnie będzie kierował pocztę na niewłaściwy serwer.

Inne ważne wpisy znajdują się na dole menu konfiguracji i wyglądają
mniej więcej tak:

```
    initial-keystroke-list = 
    default-composer-hdrs  = 
    customized-hdrs        = From:  John M. Fisk <fiskjm@ctrvax.vanderbilt.edu>    
    saved-msg-name-rule    =
```

Pozwala to na ustawienie własnych nagłówków dla właściwego adresu email.
Teraz właściwa wartość pola From: jest dodawana do wychodzącej poczty i
wszystkie ODPOWIEDZI będą zawierały właściwy adres email. Zauważ także
coś innego. W powyższej konfiguracji ustawiłem zmienna nntp-server.
Jeśli zrobisz to i Ty, będziesz w stanie używać PINE jako programu do
czytania grup dyskusyjnych (Usenet). Jak na razie działa to całkiem
dobrze. Jeśli masz dostęp do serwera newsów NNTP **możesz czuć się
wspaniale\!**

Widzisz więc, nie było tak źle\!

Ustawienie klienta POP jest jeszcze prostsze\! Aby to zrobić, musisz
zdobyć kopię programu **popclient**, którą znajdziesz w ulubionych
archiwach ftp. Na serwerze lustrzanym sunsite.unc.edu zazwyczaj znajduję
się on w katalogu:

```sh
/pub/Linux/system/Mail/pop/
```

Umieść ten program w katalogu /bin, na który wskazuje ścieżka PATH i już
jesteś gotowy do pobierania poczty.

Istnieje jeden mały problem z popclientem, który może Ci sprawić pewne
kłopoty jeśli będziesz z niego korzystał. Program będzie bez probelmu
pobierał Twoją pocztę z serwera **ale musisz mu dokładnie powiedzieć,
gdzie ta poczta ma być przechowywana**. Jeśli tego nie zrobisz to po
ściągnięciu poczty, będzie on próbował dostarczyć ją do osób *o takiej
samej nazwie loginu*, przynajmniej tak jest w wersji, której ja używam.
Jest to w porządku, jeśli twój login na serwerze poczty POP i Tówj login
w Linuksie są takie same, ale jeśli nie są, wtedy popclient nie będzie
mógł nigdzie go znaleźć i umieści pocztę w koszu. Problem w tym, że
popclient jest na tyle głupi, że potrafi wtedy usunąć Twoją pocztę z
serwera.

Kurde, co jest?\! Nie ma poczty?\!

Co za niefart... zjadło pocztę.

Jak to łatwo naprawić?

Popclient posiada pewną liczbę poleceń jakich możesz użyć w linii
komend, albo wpisać je do pliku który używany jest do konfiguracji. Na
moim komputerze, zastosowałem go z czymś takim:

```sh
popclient -3 -v -u myname -p mypassword -o /root/mail/mail-in ctrvax.vanderbilt.edu
```

Podanie popclientowi opcji (-3) mówi mu aby korzystał z protokołu POP3,
(-v) nakazuje mu pokazywanie na ekranie wszelkich informacji, tak więc
cały czas wiem co się dzieje, (-u) wskazuje mój login, (-p) moje hasło,
(-o) nakazuje umieścić pocztę w podanym ścieżką /root/mail/mail-in
pliku, a na koniec, ściąga całą pocztę z serwera pocztowego
ctrvax.vanderbilt.edu.

Wyświadcz sobie przysługę... pozwól aby LINUX wykonał część pracy za
Ciebie. Zamiast wpisywania tego wszystkiego i marnowania czasu, możesz
napisać sobie i zastosować alias lub funkcję. Wypróbuj coś takiego:

```sh
popmail() { popclient -3 -v blah.blah.blah... }
```

wpisując to do swojego pliku \~/.bash\_profile. Potem, aby zastosować
popclienta z tymi wszystkimi opcjami, wystarczy, że wpiszesz `popmail`,
a program uruchomi się, ściągając całą Twoją pocztę.

Zanim zakończymy, powinieneś rozważyć limity tego typu systemu
pocztowego i zdecydować czy chcesz go z nimi instalować:

* MUSISZ mieć dostęp do serwera poczty SMTP i POP.
* MOŻESZ używać PINE jako JEDYNEGO agenta poczty.
* Pocztę możesz wysyłać TYLKO przez serwer SMTP.

Powstrzyma Cię to przed używaniem do poczty innych programów niż pine,
takich jak elm lub mailx.

Jak widać to nie takie trudne.

To WSZYSTKO staje się jednak zbędnego jeśli pracujesz w X i używasz
jakiegoś przyjaznego użytkownikowi programu do poczty, takiego jak
choćby xmail, xmh, xelm czy mumail. Mają one graficzne interfejsy
użytkownika, więc w łatwy sposób pozwalają Ci na pobieranie swojej
poczty. Nie oferują jednak tylu opcji co program PINE ale do szybkiego
odbierania poczty są bardzo poręczne.

Również, jeśli korzystasz z przeglądarki www, a łączysz się za pomocą
SLIP lub PPP, nie będziesz w stanie wykorzystać funkcji *mailto:*

Suma sumarum, nie potrzebujesz wypasionego w opcje programu. Nie
zapomnij, że odpowiednie ustawienia mogą być wykorzystane do
"podrasowania" programu w celu szybkiego odbierania poczty, bez
konieczności konfiguracji smail, sendmail czy też sendmail+IDA.

## UWAGA: Oto krzykliwe zaświadczenie produktu...

Wypróbuj sendmail+IDA.

Korzystając z kilku dokumentacji dostarczonych razem z dystrybucją
sendmail+IDA i książki Network Administrator's Guide, byłem w stanie
ustawić i uruchomić wszystko w czasie popołudniowej przerwy w pracy. Nie
mam wygórowanych potrzeb przy korzystaniu z poczty, tak więc
konfiguracja była całkiem prosta. Wszystkie znane mi programy pocztowe
dobrze współpracują z sendmail+IDA, a jako że używam uniwersyteckiego
serwera nazw DNS mogłem skorzystać z sendmail.bind.no.

Powodzenia! Jeśli wszystko Ci się uda... gratulacje\! Daj mi o tym
znać!

