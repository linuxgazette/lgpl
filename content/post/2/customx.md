---
title: Ustawiamy X czyli gdzie co się znajduje?
date: 2022-02-09
---

* Autor: John M. Fisk
* Tłumaczenie: Marcin Niedziela
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.aug.html

---

Człowieku, powiem Ci coś na temat X...

**Są OGROMNE**

Sprawienie, aby X Window działało na Twoim komputerze tak jak chcesz,
będzie wymagało od Ciebie wielu nieciekawych doświadczeń. Dla
wszystkich nowych użytkowników LINUKSA, będzie to coś nowego. To jak
odkrycie czegoś nowego. Tak więc, bez pośpiechu.

Dopieszczenie X zajmie Ci trochę czasu. Jednak im więcej czasu na to
poświęcisz, tym lepiej. Raz jeszcze, przedstawię kilka prostych
założeń:

* masz uruchomione X Window
* używasz wersji XFree86 3.1.x
* jako swojego menadżera okien używasz fvwm
* jesteś gotowy na mały tuning

JEDYNYM powodem do wysunięcia tych założeń było to, że opierałem się na
swoim aktualnej instalacji. Tak więc, wszelkie sugestie jakie
przedstawie, wzięły się z mojego własnego eksperymentowania na systemie.
Jeśli używasz starszej wersji programów niż ja, przedstawione przeze
mnie rozwiązania prawdopodobnie będą działały, jednak jeśli nie,
będziesz musiał poradzić sobie sam.

Jakby nie było, zaczynamy...

## Tiaaaak...więc gdzie *to wszystko* się znajduje...

Oto, do czego służy kilka plików konfiguracyjnych:

* /usr/X11R6/bin/xinit  
    Skrypt, który jest odpowiedzialny za uruchamianie X Window. Zagląda
    on do skryptu .xinit w katalogu domowym użytkownika i jest on
    zbiorem instrukcji o tym jak uruchomić dane programy. Jeśli nic nie
    znajdzie, uruchomi xterm w uruchomionym menadżerze okien
* /usr/X11R6/lib/X11/xinit/xinitrc.fvwm --\> xinitrc  
    Skrypt, który uruchamia pozostałe rzeczy, wczytuje dane źródłowe
    zmiennych użytkownika i systemu z plików .Xresources i .Xdefaults,
    wczytuje mapę klawiszy z pliku .Xmodmap, włącza okno główne z miłym
    dla oka cieniowanym błękitem (xsetroot -solid SteelBlue) oraz
    uruchamia menadżera fvwm. Pozwala on systemowi na inicjalizację X i
    może być używany razem ze startx.
* \~/.xinitrc  
    Skrypt, który znajduje się w katalogu domowym użytkownika. Jeśli
    ustawiony, używany jest przez xinit lub startx do uruchomienia
    programów -- takich jak xterm, xclock, xman, etc. -- i/lub menadżera
    okien. Plik ten pozwala każdemu użytkownikowi na zmianę konfiguracji
    X.
* /usr/X11R6/lib/X11/xinit/.Xresources  
    Plik tekstowy, który ustala *źródła* zmiennych klienta X. Plik ten
    może być użyty z programami takimi jak xinit czy startx do
    ustawienia danych dla X jeżeli nie zostały one oddzielnie ustawione
    przez użytkownika.
* \~/.Xresources  
    Plik tekstowy, znajdujący się w katalogu domowym użytkownika
    zawierający wpisy danych X *dla tego użytkownika*. Wraz z plikiem
    znajdującym się powyżej wczytywane są przez program **xrdb** (X
    Resource DataBase).
* /usr/X11R6/lib/X11/XF86Config  
    Plik tekstowy unikany i nielubiany przez wszystkich amatorów X oraz
    powód wielu frustracji. Używany jest przez serwer X do ustawienia
    wymaganych parametrów konfiguracyjnych.
* /usr/X11R6/bin/startx  
    Skrypt, który zajmuje się... według samego siebie:

> \# Jest to tylko prosta implementacja dużo bardziej prymitywnego
> interfejsu xinit. \# Przegląda on pliki .xinitrc i .xserverrc danego
> użytkownika, a potem systemowe \# xinitrc i xserverrc, a gdy ich nie
> znajdzie uruchamia sie z domyślnymi ustawieniami. \# Systemowy xinitrc
> powinien, między innymi sprawdzać plik .Xresources i zastosować go, \#
> uruchamiać menadżera okien, wyświetlać zegar i kilka konsoli xterm. \#
> \# Nasi administratorzy są SILNIE nakłaniani do napisania lepszej
> wersji.

* \~/.Xmodmap  
    Plik tekstowy używany przez **xmodmap** do ustawienia mapowania
    klawiszy pod X. O programie xmodmap w kilku słowach:

> Program xmodmap jest wykorzystywany do edycji i wyświetlania
> modyfikatorów mapy klawiatury i tablicy klawiszy, które wykorzystywane
> są przez aplikacje do konwersji własnych kodów klawiszy na
> wyświetlane. Zazwyczaj jest on uruchamiany z poziomu skryptu sesji
> użytkownika do konfiguracji klawiatury według własnego gustu.

Z polskiego na nasze, oznacza to, że możesz sobie trochę przemapować
klawiaturę jeśli masz taką potrzebę.

/usr/X11R6/lib/X11/fvwm/system.fvwmrc

Plik tekstowy używany przez menadżera okien **fvwm** do ustawienia
własnych opcji konfiguracyjnych. Niezły kąsek dla dociekliwych.

\~/.fvwmrc

to samo co *ibid.* tyle, że w katalogu użytkownika.

Katalog w LINUX FSSTND przechowujący *app-default*, czyli domyślne
aplikacje klienta X, używany do ustawienia domyślnych parametrów dla
tych programów.

Na pewno coś pominąłem, jednak są to najważniejsze pliki i katalogi
używane do konfiguracji X.

Tak więc, do czego nam to potrzebne? Istnieją prawdopodobnie dwie rzeczy
jakie chciałbyś zrobić. Po pierwsze, ZRóB KOPIE WSZYSTKIEGO\!\! Zrób
kopię wszystkich tych plików konfiguracyjnych, które otrzymałeś razem z
dystrybucją.

Dlaczego?

Jeśli coś naknocisz, możesz to naprawić. Bez kopii zapasowej, jesteś
stracony. Nie ma nic bardziej denerwującego niz przeglądanie 40
dyskietek w poszukiwaniu odpowiedniej wersji, odpowiedniego archiwum,
odpowiedniego pliku, w którym jest standardowa konfiguracja do
naprawienia czegoś, co przed chwilą w akcie głupoty pochrzaniłeś, nawet
nie pamiętając co zrobiłeś. Dla porównania podobne problemy sprawia
odszyfrowanie błędu windozy w module X0-FF3445a3. Kojarzysz o co mi
chodzi?

Możesz dać sobie spokój. Nie uda Ci się to.

Po drugie, kup sobie jakieś notesik albo zeszyt i zapisuj wszystko co
robiłeś z systemem. Niektóre z lepszych pomysłów na tuningowanie systemu
powstały za pomocą długopisu i papieru. Serio.

Nio, wystarczy już tego. Czas ZROBIĆ coś konkretnego.

