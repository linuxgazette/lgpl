---
title: Co zobaczymy w nastęnym miesiącu?
date: 2022-02-09
---

* Autor: John M. Fisk
* Tłumaczenie: Marcin Niedziela
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.aug.html

---

Cóż, jak zawsze, będzie DUŻO do napisania, choć większość artykułów już
mam na ukończeniu. Ostatnio dużo czasu spędzam z X, a jest tam wiele do
upiększenia, ułatwienia i sprawienia aby ich używanie było dużo
ciekawsze. Z podstawowymi rzeczami można sobie w całkiem prosty sposób
poradzić.

**Dzięki wszystkim, którzy pomagają mi w pisaniu.** Poważnie.

Więc czego możemy się spodziewać w następnym wydaniu? Cóż, dostałem
wiele próśb i pomysłów dotyczących następnej edycji. Więccc... w
następnym miesiącu zajmiemy się GŁóWNIE CAŁOŚCIĄ X, a dokładnie:

* konfiguracja fvwm -- gdzie zacząć
* konfiguracja fvwm -- dodawanie wyskakującego menu
* konfiguracja fvwm -- tworzenie przycisków do aplikacji
* konfiguracja fvwm -- moc Styli
* konfiguracja fvwm -- uruchamianie w init
* konfiguracja fvwm -- tworzenie efekciarskiego pulpitu
* przejażdżka z xterm -- czcionki, menu, dodawanie przycisków, geometria
* narzędzia do konfiguracji XFree86
* wymagane programy konfiguracyjne: xcolorsel, xkeycaps, xbmbrowser, pixmap
* Gra miesiąca dla X: fly8 -- budzący strach symulator lotu\!
* Aplikacja miesiąca dla X: xarchie -- moc archie + ftp

Istnieje tyle różnych ciekawych rzeczy i będzie mi miło jeśli wyślecie
mi swoje pomysły, sugestie, krytykę lub to co dotychczas. Masz już swój
działający program PINE + klienta poczty. Pisz do mnie.

Mam nadzieję, że Ci się podobało\!

## Jeszcze jedno OSTATNIE spostrzeżenie...

Chciałbym podziękować firmie [Tennessee CommerceNet](http://www.tenn.com/),
która zajęła się hostingiem Linux Gazette i zajęła się konfiguracją
serwera ftp. Chciałbym dodawać do każdej miesięcznej edycji Linux
Gazette spakowaną wersję html wraz z obrazkami abyście mogli sobie ją
łatwo ściągnąć. Będzie to nieco wygodniejsze dla Was rozwiązanie.
Rozważam również wydawanie wersji tylko tekstowej.

Jeśli kogoś to interesuje to niech da mi znać, inaczej nadal będę
wydawał ją tylko jako wersję online.

I jeśli KTOKOLWIEK porusza temat plików PostScriptowych...

Nie.

Bądźcie poważni. Dajcie spokój albo zdobądźcie przyzwoitą przeglądarkę.
:-)

**Jeszcze tutaj wrócę...**

