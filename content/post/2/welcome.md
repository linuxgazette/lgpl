---
title: Pozdrowienia i skrzynka pocztowa
---

* Autor: John M. Fisk
* Tłumaczenie: Marcin Niedziela
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.aug.html

---

Witam w sierpniowym wydaniu **Linux Gazette**. W gazecie usiłuję zebrać
do kupy wybrane rady, triki i pomysły, **które sprawiają, że używanie
Linuksa jest bardziej ciekawe**.

W tym wydaniu planuje, w nieco zabawny sposób, omówić kilka rzeczy,
wymaganych do uruchomienia systemu X Window na Twoim Linuksie. Nie wierz
w te wszystkie bzdury jakie są rozgłaszane na temat X-ów. Konfiguracja
może napotkać kilka przeszkód ale będzie je można w dość prosty sposób
rozwiązać. Poza tym możesz trochę poeksperymentować, co pozwoli Ci na
dobranie własnych ustawień tak, aby wszystko działało tak jak sobie tego
życzysz.

## Chciałbym także podziękować kilku osobom, które zachęciły mnie do 
kontynuacji pisania tej Gazety.

### **Craig Goodrich** \<craig@ms.ljl.com\> napisał poniższe słowa:


> Dwa słowa uznania. Ta gazeta i Keystroke-HOWTO są bez wątpienia dwoma
> najbardziej wartościowymi i przyjaznymi dla niedoświadczonego
> użytkownika publikacjami jakie widziałem.
>
> Pomyłs do następnego wydania -- jeśli posiadasz CDROM na sbpcd,
> sugerowałbym Ci uaktualnienie do 1.2.9 lub .10; kod cdromu jest
> znacznie bardziej sprytny (chyba, że masz TEACa, jak ja), zajrzyj do
> pliku sbpcd.h, by dowiedzieć się o możliwych efektach (otwieranie i
> zamykanie, niezależnie od tego czy jest zamontowany czy nie lub opcja
> detekcji). Potem przekompiluj \[patrz wydanie 1\].
>
> To czego potrzebujesz, to dobry edytor tekstu. Ja zacząłem pisać w
> 1967, a w późnych latach 70-tych stałem się już fachowcem od DEC TECO,
> jednak teraz z niego wyrosłem -- nawet stary PE był bardziej użyteczny
> i intuicyjny niż EMACS lub atawistyczny vi. Od pierwszego uruchomienia
> nie polubiłem tez crispa, byłem fanatykiem BRIEFa, jednak i on stał
> się z czasem denerwujący. Rozpakowałem Xemacs (po tym jak ściągnąłem
> całe te 14 mega -- spakowane\! -- z prędkością 14.4 przez ppp) mało
> nie wykitowałem, gdy wyskoczyło 'segmentation fault'.
>
> Jednakże, mógłbym ewentualnie czasem coś napisać na ten temat i dodać
> to do gazety, jeśli tylko uda mi się przekonać moją żonę, że
> kernel-hacking i zabawa z Xconfig jest rzeczywiście dużo ważniejsza
> niż sprzątanie garażu czy niańczenie dziecka...
>
> Jeszcze raz dzięki--
>
> Craig


Przysłał on również bardzo ciekawy URL, w którym możemy przeczytać o
zbliżających się planach współpracy Caldery i Novell'a' w celu
przeniesienia programu **WordPerfect 6.0** na Linuksa. Więcej możecie
dowiedzieć się na stronie
[http://www.caldera.com/WPLinuxRelease.html](https://web.archive.org/web/20030813195736/http://www.caldera.com/WPLinuxRelease.html)

### Brad Greger napisał:


> Chciałbym tylko powiedzieć, że jestem bardzo wdzięczny za wszystkie te
> wspaniałe rady i za śmiech (bardzo humorystycznie napisane artykuły).
> Opisałeś bardzo wiele rzeczy, które denerwowały mnie w Linuksie, a
> teraz stały się łatwe do zrobienia. Pomagasz nowym użytkownikom
> Linuksa (czyli mnie) na zaoszczędzenie wielu godzin, gdyż nie muszą
> dochodzić do tego samodzielnie.
>
> Proszę, niech Linux Gazette trwa dalej\!
>
> Dzięki, Brad \<begreger@artsci.wustl.edu bgreger@thalamus.wustl.edu\>


### Cimarron Taylor napisał kilka swoich sugestii:

>> Właśnie przeczytałem Twoją gazetę. Zagłębiłem się w kategorii
>> wielosystemowości. Używam Linuksa, Windows NT, Windows i DOSa, by móc
>> podołać temu wszytkimu, czym się zajmuje na moim laptopie. Oto kilka
>> rzeczy, które według mnie powinna zawierać instalacja Linuksa:
>
 > - fips i loadlin  
 >   bez tego nigdy nie mógłbym używać linuksa jako głównego systemu. Na
 >   prawdę nie lubię koncepcji przedkładania bootloaderow DOSa czy NT
 >   nad LILO, ponieważ mam tam nadal wiele rzeczy, do których potrzebuje
 >   DOSa i NT i nie chciałbym ryzykować utraty tych danych.
 > - linux netscape  
 >   Kupiłem Windows NT ale nadal używam Linuksa, by wygenerować sobie
 >   dobre postscripty dla drukowanych dokumentów.
 > - kermit  
 >   być może kwestia gustu ale działa to całkiem dobrze ze wszystkimi
 >   stronami z jakimi się łącze.
 > - emacs  
 >   nie używałbym Linuksa bez niego.
>
>> Jedną rzeczą jaka mnie najbardziej dręczy w Linuksie jest problem w
>> współdzieleniu danych na komputerze wielosystemowym. Linuks nie
>> potrafił czytać lub zapisywać danych na systemie plików Windows NT i
>> nie rozpoznaje nazw plików na drugorzędnej partycji FAT. Podobnie
>> Windows NT nie potrafi zobaczyć partycji Linuksa. Oznacza to, że nie
>> mogę efektownie używać wszystkich zbiorów znajdujących się na moim
>> komputerze.
>>
>> Jednakże, będę czytał Twoją gazetę.
>>
>> Cimarron Taylor  
>> cimarron@acgeas.com


### Hans Zoebelein \<zocki@web.tenn.com\> przesłał mi tą wartościową wiadomość
na zachętę.


> Cześć John,
>
> dzięki za te pokrzepiające strony. Jest to jedna z najlepszych
> publikacji dla LINUKSA jakie kiedykolwiek widziałem w sieci.
>
> Pozdrawiam  
> Hans


Przepraszam, że nie mogę zamieścić tu wszystkich Waszych listów.
Odpisałem do każdego, kto do mnie napisał, chociaż nasz lokalny serwer
poczty wysłał gdzieś wszystko dalej. Przepraszam więc tych, którzy
napisali do mnie z Niemiec i Włoch. Przejrzę je, jeśli uda mi się
wszystko naprawić.

Jakby nie było, mam nadzieje, że jesteście zadowoleni!

