---
title: Używasz VI, używaj VIM!
date: 2022-02-09
---

* Autor: John M. Fisk
* Tłumaczenie: Marcin Niedziela
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.aug.html

---

OK, wiem, żę biorę na siebie ryzyko, że coś sknoce, jednak pozwólcie mi,
że przedstawię Wam rozwiniętą wersję edytora vi, który jak sądzę jest
najużyteczniejszym aktualnie narzędziem tego typu. Mowa oczywiście o
VIM, standardowym edytorem dystrybucji Slackware.

Są takie trzy rzeczy, które lubię w tym edytorze:

* **Pokazuje** tryb w jakim aktualnie jesteś.
* Posiada łatwo dostępną funkcje pomocy on-line.
* Całkiem fajnie działa pod X w połączeniu z xterm.

Aktualnie używam go pod X-ami i w tej chwili w lewym dolnym rogu
pokazuje mi "-- INSERT --", który przypomina mi, że używam tryby
wpisywania. Pokazuje to samo, gdy jestem w trybie NADPISYWANIA. Dlaczego
jest to takie fajne?

Dlatego, że NIENAWIDZE odtwarzania tego piskliwego sygnału dźwiękowego
(beep) tylko dlatego, że nie wiem czy jestem w trakcie wpisywania,
nadpisywania lub jakiegokolwiek innego trybu. Całe szczęście używając
vim wiesz na czym stoisz.

Po drugie, posiada łatwy w użyciu ekran pomocy, który można wywołać
przez wpisanie ":h " w trybie komend. Pomoce posegregowane są według
kategorii -- poruszanie się w edytorze, wyszukiwanie, wpisywanie tekstu,
usuwanie tekstu, ustawianie opcji edytora, praca wielookienkowa-- jeśli
potrzebujesz więcej informacji, Twoja dystrybucja dostarcza Ci dużo
więcej pomocy w plikach dokumentacji, ktorą zazwyczaj można znaleźć w
katalogu /usr/doc/vim. Przejrzyj ją jeśli masz ochotę.

Ostatecznie, vim łatwo uruchamia się w xterm pod X-ami. Dostaniesz nawet
dość fajne pożegnanie gdy skończysz pracę z vimem -- gdy to zrobisz,
sprawdź tytuł na pasku zadań xterm...

Zdaję sobie sprawę, że istnieje WIELE ładniejszych i bardziej
przyjaznych dla użytkownika edytorów dla systemu X i uwierzcie mi, ja
też z nich korzystam. xcoral-2.4 i xwpe są aktualnie moimi ulubionymi.
Jest to jak we wszystkim... tylko kwestia gustu i sposobu użycia. Jeśli
musisz się zając edycją dużych ilośći tekstu, to emacs lub vi\[m\] jest
najlepszym wyjściem z sytuacji.

Dlaczego?

Zabawne, że pytasz. Na początku mojej zabawy z vi. *Na prawdę* wszystko
jest o wiele szybsze w wykonaniu, jeśli nie odrywasz rąk od klawiatury,
niż bawić się w klikanie myszką. Warunkiem tego jest jednak to, że
musisz nauczyć się skomplikowanej klawiszologii. Jednak, gdy raz
zapamiętasz podstawowe komendy, nauka pozostałych przyjdzie już łatwo.

Zatem, co takiego poręcznego znalazłem? Wyprubuj to...

W trybie poleceń wpisz "v", co spowoduje przesunięcie kursora do
pozycji, z której będziesz mógł go przesuwać standardowymi ruchami --
"j" w dół, "k" w górę, "h" w lewo, "l" w prawo. Jeśli dotkniesz
klawiszy, zobaczysz, że ustawione jest to całkiem naturalnie. Możesz
także użyć "G" by przejść do końca pliku. Kiedy już wszystko załapiesz,
otwórz sobie do edycji jakiś plik i zastosuj na nim standardowe komendy
-- "y" by zaznaczyć, "d" by usunąć, "c" by zmienić tekst i tak dalej.

Istnieje także pomocna funkcja, która gdziekolwiek w dokumencie pozwala
na wstawienie "mark". Dla przykładu, jeśli pracujesz sekcja po sekcji
nad hipotetycznym magazynem o Linuksie... no wiesz, coś jak "Linux
Gazette". Po skończeniu jednej sekcji, zaczynasz następną, a potem
dodajesz nazwę zakotwiczenia, którą chcesz dodać w spisie treści.

Znajdując sie w aktualnym miejscu edycji wchodzisz w tryb komend i
wpisujesz "m a" która *odznacza* tą pozycję jako tag "a". Teraz,
przechodzisz na górę strony, wpisujesz swoje informacje i wciskasz "'a"
co wklei tekst zaznaczony wcześniej jako "a".

Fajne, prawda..?

Jednakże jest dużo do nauczenia się. Częste zaglądanie do pomocy sprawi,
że będzie to o WIELE prostsze. Jak już wcześniej wspomniałem, możesz go
równie dobrze używać pod X. Możesz go uruchomić z xterm, bądź
przydzielić mu własny xterm wpisując:

```sh
xterm -e vi [options] filename &
```

uruchomi to xterm. Opcja "-e" *uruchomi* edytor vi. Kiedy skończysz
edycję i wyjdziesz z edytora, xterm zostanie zamknięty. Łatwe i
przyjemne.

Także, jeśli potrzebujesz wycinać lub wklejać tekst możesz zrobić to na
kilka sposobów. Uruchom edytor vim i użyj opcji ":split
/home/me/anyfile.doc", by wczytać drugi plik przy podzielonym ekranie
lub inaczej, jeśli pracujesz w środowisku X, uruchom vim w oddzielnym
oknie. Tekst wycinasz i wklejasz tak jak zawsze -- zazwyczaj, musisz
użyć klawisza 1 by podświetlić tekst, przesunąć kursor spowrotem na
xterm z uruchomionym vim, a potem wcisnąć 2 by go wkleić.

Wszystkie komendy do poruszania się pomiędzy podzielonym ekranem, zmiana
wielkości ekranu, poruszanie się pomiędzy buforami i wszystko inne
zawarte jest w pliku pomocy.

Bardzo fajne.

Na zakończenie. Bardzo często używałem vi lub vim. W swoim systemie, mam
stworzone dowiązanie symboliczne **vim** --\> **vi** (a także, **less**
--\> **view**). Możesz łatwo testować swój ulubiony klon vi przez proste
dowiązania symboliczne dla najlepszego dla Ciebie klona vi:

```sh
ln -sf /usr/bin/elvis /usr/bin/vi
ln -sf /usr/bin/vim /usr/bin/vi
ln -sf /usr/bin/nvi /usr/bin/vi
```

Według ogólnej opinni lepiej jest wpisywać VIM, gdyż jego pełna nazwa
brzmi "Vi iMproved" (Ulepszony Vi).

