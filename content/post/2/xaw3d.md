---
title: Ustawiamy X z wykorzystaniem Xaw3d
date: 2022-02-09
---

* Autor: John M. Fisk
* Tłumaczenie: Marcin Niedziela
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.aug.html

---

Wszystko co można skonfigurować lub "podrasować" w XFree86 jest bardzo
PROSTE, jednak pewne problemy może sprawiać instalacja bibliotek
**libXaw3d**. Jestem jednak *pewien*, że wszyscy już się z tym uporali,
ponieważ na stronie 54 **lipcowego** wydania miesięcznika [Linux
Journal](https://web.archive.org/web/20030813195736/http://www.ssc.com/)
zostało to dokładnie opisane w artykule **Marka Komarinskiego**
"Instalacja bibliotek Xaw3d". Jestem pewien, że nie ma już potrze...

Chwileczkę\!

Co\!\! Nie czytasz LINUX JOURNAL\!\!??

**SKNERA\!\!**

Nie bądź taki\! To wcale nie takie drogie\!\! Ci faceci z firmy
[Specialized Systems Consultants, Inc
(SSC)](https://web.archive.org/web/20030813195736/http://www.ssc.com/)
robią naprawdę kawał porządnej roboty umieszczając tam wspaniałe
informacje przydatne społeczności LINUKSA.

Poważnie mówiąc, naprawdę potrzebujesz tego magazynu. Roczną prenumeratę
możesz mieć już za 22 dolce, a co ważniejsze, w ten sposób wspierasz
całą tą ciężką pracę ludzi z SSC odpowiedzialnych za jego wydawanie. W
lipcowym wydaniu można znaleźć artykuły na temat:

 * Konfiguracja X11* jak bez nerwów skonfigurować Xfree86
 * Instalacja bibliotek Xaw3D
 * Przyjazny przewodnik HTML
 * xfm 1.3* menadżer plików i nie tylko
 * przegląd produktów dla Linuksa w tym Metro-X i Motif
 * Gry, Muzyka, inne zabawy opisane w prosty sposób dla początkujących

To naprawdę dobry magazyn i w 100% wart jest swojej ceny. Nie czekaj
tylko zadzwoń pod numer (206) 782-7733 lub wyślij maila z prośbą o
prenumeratę na adres
[subs@ssc.com](https://web.archive.org/web/20030813195736/mailto:subs@ssc.com).
Jeżeli tak BARDZO, BARDZO MOCNO nie lubisz załatwiać tego przez maila,a
także jakikolwiek inny elektroniczny sposób jest dla Ciebie wyzwaniem,
to wystarczy, że wyślesz swoją prośbę na adres:

``` 
        Linux Journal
        P.O. Box 85867
        Seattle, WA 98145-1867
```

#### WYJAŚNIENIE* Nie, ja nie pracuje dla nich i nie dają mi pieniędzy za pisanie tych słów.

Jestem pod wielkim uznaniem pracy jaką wykonują Ci ludzie i zasługują na
Wasze wsparcie. Z drugiej strony, Ty zasługujesz na owoce ich ciężkiej
pracy. Te artykuły są bardzo pomocne i dadzą Ci wyraźny obraz na to co
dzieje się w społeczności LINUKSA.

Wróćmy jednak do tematu.

Ustawianie bibliotek Xaw3D jest jak bryza, która sprawia, że czujesz się
jak nowo narodzony. Szczerze. No więc *co to* właściwie jest? Biblioteki
Xaw3D są częścią bibliotek X Window Athena Widgets. Widgets są to bloki
graficzne, lub jak wolisz, graficzne komponenty klientów X. Widgets to
menu, przyciski, okienka, sidebary, belki tytułowe, a także to wszystko
co łączy ze sobą te graficzne komponenty programu. Oryginalne Athena
widgets, autorstwa chłopaków z MIT, są powiązane z Xaw. Są to nowe
trójwymiarowe widgets zaprojektowane tak, aby stworzyć w X
trójwymiarowe środowisko podobne do Motif.

Całe to piękno zawiera się w tym, że nowe biblioteki libXaw3D są gotowe
do przekopiowania i podmienienia starszych wersji biblioteki. Instalacja
to prościzna. Zaufaj mi.

Tak więc co musisz zrobić? Oto moje rady:

1. Zdobądź swoją własną kopię bibliotek Xaw3D. Jednym z najlepszym
miejscem do ich zdobyci jest jeden z mirrorów sunsite.unc.edu, na
przykład 
[GA Tech's sunsite archive](ftp://ftp.cc.gatech.edu/pub/linux/libs/X/),
w podkatalogu /pub/linux/libs/X. Użycie prekompilowanych binarek,
które powinny się tam właśnie znajdować sprawia, że cały proces
instalacji jest bardzo prosty.

2. Archiwum potraktuj TARem. W tym momencie, prawdopodobnie chciałbyś
najpierw do niego zajrzeć -- wypróbuj `tar -tvf Xaw3D_X11R6.tar.gz`,
nazwa pliku może być inna -- upewnij się czy jest on zarchiwizowany
z absolutną ścieżką dostępu. Jeśli tak jest, prawdopodobnie wolisz
rozpakować go z poziomu katalogu głównego root (/) tak, że wszystko
zostanie umieszczone tam, gdzie powinno się znajdować. Biblioteki te
zazwyczaj "mieszkają" sobie w katalogu /usr/X11R6/lib. Jeśli
znajdują się tam już jakieś pliki, możesz bezpiecznie rozpakować
pliki gdziekolwiek chcesz, a potem przekopiować je do katalogu
/usr/X11R6/lib. JEDNAK NIE ZAPOMNIJ O NAJWAŻNIEJSZYM...

3. Zrób sobie kopie aktualnego pliku libXaw.so.6.0. Kopię umieść gdzieś
w bezpiecznym miejscu, np /usr/local/backup. Chodzi o to, że jeśli
coś pójdzie nie tak, będziesz mógł łatwo przeinstalować bibliotekę.

4. Gdy już umieścisz pliku w katalogu /usr/X11R6/lib powinieneś tam
znaleźć następujące pliki:

* libXaw3d.so.6.0
* libXaw3d.sa
* libXawed.a

Musisz teraz zmienić nazwę pliku libXaw3d.so.6.0 na libXaw.so.6.0.
Najprostszym na to sposobem jest kopiowanie (kiedy już masz kopie
starego pliku... czy na pewno MASZ kopie starego pliku?) przez
wydanie polecenia `cp libXaw3d.so.6.0 libXaw.so.6.0`.

5. Na koniec, uruchom program **ldconfig**. Zrobisz to przez wydanie
polecenia `ldconfig` w linii poleceń. Program ten automatycznie
uaktualni dowiązania między aktualnymi plikami bibliotek które
znajdzie w pliku **ld.so**. Jak widzisz, dla programów które
dynamicznie odwołują się do bibliotek libXaw, ld.so będzie szukał
pliku nazwanego libXaw.so.6, a NIE libXaw.so.6.0. Tak więc, program
ldconfig stworzy odpowiednie odwołania tak aby ld.so mógł odnaleźć
odpowiednie pliki.

Nadąrzasz jak dotąd? Już prawie skończyliśmy\!

Ostatnią rzeczą jaką się zajmiemy jest dodanie potrzebnych danych w celu
poprawienia wyglądu Xaw3D. Jest to także bardzo pomocne. Zwyczajnie
dodaj poniższe wpisy do swojego pliku konfiguracyjnego .Xresources:

```
    ! Good Xaw3d Defaults 
    !
    *Form.background:                       #c0c0c0
    *TransientShell*Dialog.background:      #c0c0c0
    *Command.background:            #c0c0c0
    *Menubutton.background:         #c0c0c0
    *ScrollbarBackground:           #c0c0c0
    *Scrollbar*background:          #c0c0c0
    *Scrollbar*width:               15
    *Scrollbar*height:              15
    *Scrollbar*shadowWidth:         2
    *Scrollbar*cursorName:          top_left_arrow
    *Scrollbar*pushThumb:           false
    *shapeStyle:                    Rectangle
    *beNiceToColormap:              False
    *shadowWidth:                   2
    *SmeBSB*shadowWidth:            2
    *highlightThickness:            0
    *topShadowContrast:             20
    *bottomShadowContrast:          55
```

W razie czego, możesz także spróbować pobawić się z poniższymi opcjami,
które szerzej zostały opisane w jednym z artykułów Linux Journal:

```
    *background:                            LightSkyBlue
    *Form.background:                       grey90
    *TransientShell*Dialog.background:      bisque3
    *Command.background:                    gray80
    *Menubutton.background:                 gray80
    *ScrollbarBackground:                   gray70
    *Scrollbar*width:                       16
    *Scrollbar*height:                      16
    *Scrollbar*shadowWidth:                 2
    *Scrollbar*cursorName:                  top_left_arrow
    ! You can change the above to all kinds of icons, including gumby.
    ! Find out what icons are available by looking in
    ! /usr/include/X11/cursorfont.h* be sure to strip off the XC_
    *Scrollbar*pushThumb:                   false
    *ShapeStyle:                            rectangle
    *beNiceToColormap:                      false
    *shadow*Width:                          2
    ! The above defines all shadowwidths to 2.  
    *Label*shadowWidth:                     2
    ! The above overrides the definition from a few lines above for Label
    ! widgets.
    *SmeBSB*shadowWidth:                    2
    *highlightThickness:                    0
    *topShadowContrast:                     20
    *bottomShadowContrast:                  40
    ! The above two lines define how the shadows appear on the top and
    ! bottom of teh widgets.  The higher the number, the lighter it is.  This
    ! particular setting makes the top and left sides darker than the
    ! bottom and right sides.
```

Możesz już przetestować swoje ustawienia. Upewnij się, że powyższe wpisy
znajdują się w Twoim .Xresources lub .Xdefaults -- oba są wczytywane
podczas uruchamiania przez xrdb -- i odpal X-y. Jeśli ustawiłeś xterms
by zawierał sidebary, wtedy powinieneś zobaczyć nieporównywalne różnice.
Spróbuj odpalić coś innego. Niech to będzie **xedit** lub coś co
korzysta z bibliotek libXaw, a na pewno zauważysz wielkie różnice.

Gratulacje\! Twoje X-y zostały właśnie podrasowane.

