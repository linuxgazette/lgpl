---
title: Pomocne skrypty dla PPP
date: 2022-02-09
---

* Autor: John M. Fisk
* Tłumaczenie: Marcin Niedziela
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.aug.html

---

Ok, zanim przejdziemy dalej, zakładam, że spełniasz poniższe zasady:

* masz dostęp do połączenia PPP
* jesteś w stanie uruchomić PPP i z niego korzystać
* Twoje połączenie jest w miarę niezawodne

Jeśli to prawda -- gratulacje\! Ustawienie stałego i niezawodnego
połączenia PPP może doprowadzić niejednego do niewielkiej frustracji.
Jeśli masz z tym jakieś problemy, istnieją dwa bardzo dobre miejsca
gdzie możesz znaleźć pomoc -- plik **README.linux** dostarczony razem z
programem ppp-2.1.2 (znajdziesz go w katalogu /usr/lib/ppp); oraz w
**Przewodniku Administratora Sieci**, będącego częścią Projektu
Dokumentacji Linuksa. Przewodnik Admina Sieci możesz znaleźć na
większości Linuksowych serwerów FTP, zazwyczaj w katalogu /doc/LDP.

Jeśli już dowiedziałeś się jak nawiązać i utrzymać połączenie, czas
sprawić aby było ono bardziej wygodne i użyteczne. Uruchomienie mojego
połączenia wiązało się z wydanie polecenia:

```sh
pppd connect 'chat -v "" ATZ OK ATDT*70,123-1234 CONNECT "" name:
myname word: mypassword annex: ppp' crtscts -detach debug modem
defaultroute noipdefault /dev/cua1 38400
```

Jeśli udało Ci sie przez to przejść -- jesteś wspaniały. Nadszedł czas
aby zmusić Linuksa do pracy i zrobić to *WIELE* prostszym.

Na początek, zacznij od umieszczenia wszystkich opcji pppd tam gdzie
powinny być* w pliku /etc/ppp/options. Jest to zwykły plik tekstowy,
który musi być czytelny dla każdego, kto z niego korzysta. Zawiera
pojedyncze linie opisujące używane opcje. Dla przykładu:

```sh
-detach
crtscts
modem
defaultroute
noipdefault
debug
```

Wcześniej, dodawałem opcję *debug* tylko po to by zobaczyć co się
dzieje. Na wyjściu programu, zwykły tekst był konwertowany na prymitywną
formę New Guinea ISO-8956.03-glyph czego pozbyłem się przy użyciu
skryptu AI PERL 5.001.

Aby to wszystko było nieco prostsze w użyciu spróbowałem własnoręcznie
napisanego skryptu. Chciałbym wyjaśnić, że nie jestem bardzo dobrym
programistą, przynajmniej nie w tej chwili. Jednakże, skrypt ten działa
i jak na moje oko działa dobrze. Aktualnie używam LINUKSA na zwykłym
pececie z połączeniem PPP z miejscowym uniwersytetem, które pozwala mi
na korzystanie z serwera newsów NNTP, poczty przez SMTP, a także serwera
poczty POP3.

Dopóki uniwerek posiada dla połączeń pięć linii i dysponuje dynamicznymi
adresami IP, napisałem kilka prostych skryptów robiących poszczególne
rzeczy:


 * zezwala użytkownikowi na uruchomienie połączenia
 * próbuje połączenia z każdym z pięciu numerów dial up
 * jeśli te wszystkie linie są zajęte, odczekuje zdefiniowany przez
    użytkownika czas do następnej próby połączenia
 * dla raz ustalonego połączenia automatycznie uaktualnia wpisy w pliku
    /etc/hosts dodając mój aktualny dynamiczny adres IP (nienawidzę
    robić tego ciągle ręcznie)
 * pozwala użytkownikowi na sprawdzenie poczty za pośrednictwem serwera
    POP
 * uruchamia w tle mały program, który wysyła pakiety ping do bramki
    serwera, by uniknąć połączenia w czasie bezczynności
 * uruchamia w tle mały program, który co jakiś czas sprawdza pocztę
    przez cały czas trwania połączenia PPP.

Aby to wszystko ustawić po prostu stworzyłem plik /etc/ppp/options,
umieściłem skrypty shellowe w katalogu /usr/lib/ppp (pozwoli to mieć
nad wszystkim kontrolę), utworzyłem dowiązania symboliczne do skryptów w
katalogu /root/bin, który jest moją ścieżką do programów wykonywalnych,
oraz stworzyłem serię tekstowych plików zawierających wszystkie
informacje potrzebne do tego, aby dial-up mógł z tym wszystkim się
*dogadać*. Jak wiesz, dogadanie sprowadza się do tego, że wczytywane są
poszczególne linie komend z pliku, który sprawia, że napisanie skryptu
było dużo prostsze. Nazwałem je vandy1, vandy2, vandy3, etc. i wtedy
skorzystałem z shella do włączania ich po kolei.

Łatwiej będzie, jeśli Ci je pokaże niż będe o nich gadał, usunąłem
zawarte w nich nazwę-hasło-numer. Prawdopodobnie będziesz musiał je
poprawić dla własnych potrzeb, a to może Ci pomóc na początek:

 * [ppp](/post/2/misc/ppp): skrypt utrzymujący połączenie
 * [pppon](/post/2/misc/pppon): uruchom ten skrypt po uzyskaniu połączenia by 
 uaktualnić wpis w /etc/hosts, sprawdzić pocztę i uruchomić w tle program 
 pingujący i sprawdzający pocztę
 * [ppp-up](/post/2/misc/ppp-up): okresowo pinguje host bramki by zapobiec bezczynności 
 połączenia PPP
 * [popcheck](/post/2/misc/popcheck): okresowo sprawdza serwer poczty POP
 * [vandy1](/post/2/misc/vandy1): przykład pliku z opcjami do rozpoczęcia poszczególnych połączeń
 * [pppoff](/post/2/misc/pppoff): uruchom ten skrypt gdy będziesz chciał zamknąć połączenie.

Zobaczmy co jeszcze można sobie ułatwić...

Jeśli korzystasz z X, to spróbuj czegoś takiego: możesz uruchomić skrypt
**ppp** w oddzielnym oknie terminala xterm. Wystarczy wpisać:

```sh
xterm -ls -C -e ppp &
```

Należałoby wyjaśnić dwie ważne rzeczy. Pierwsza, dodanie opcji "-C"
wyświetli wszystkie wiadomości, które normalnie idą do konsoli, w oknie
xterm. Korzystam z tego, ponieważ ustawiłem sobie plik /etc/syslog.conf,
by wysyłał komunikaty błędów, np. dla PPP, do /dev/console, tak więc
widzę wszystko co się dzieje. Normalnie xterm nie wyświetla tych
informacji. Możesz je zobaczyć jeśli uruchomisz **xkonsolę**, jednak ja
wolę sobie patrzeć na ustanowione połączenie.

Druga, dodanie opcji "-e" zmusza xterm do uruchomienia programu -- w tym
przypadku jest to mój skrypt shellowy **ppp**. Chciałbyś sprawić, by
było to jeszcze bardziej fantastyczne? Wiedziałem że tak...

Po co wpisywać to wszystko samemu, skoro LINUX może to zrobić za Ciebie.
Wystarczy wykonać trzy proste rzeczy, by cały ten proces był jeszcze
PROSTSZY:

Dopisz do pliku /etc/profile lub swojego własnego \~/.bash\_profile
funkcję definiującą xppp:

```sh
xppp() { xterm -ls -g 80x24+360+120 -C -e ppp & }
```

Przez ustawienie geometrii, zmuszam xterm do otwarcia tam gdzie mam na
to ochotę. Na moim monitorze zostanie to wyświetlone z czcionką na całej
szerokości okna xterm. Teraz, jedyne co wpisuje to "xppp" i *voila*, oto
mój program uruchomił się w swoim oknie xterm. Kiedy połączenie zostanie
nawiązane, zostanę o tym powiadomiony. Ależ ślicznie.

Ale to jeszcze nie wszystko. Jeśli korzystasz z **fvwm** to możesz
jeszcze dodać to do menu lub ustawić jako przycisk na belce GoodStuff.

Aby dodać to do menu, wystarczy edytować plik system.fvwmrc lub swój
własny \~/.fvwmrc, do którego dopisujemy:

```sh
Exec "xppp"     exec xterm -ls -g 80x24+360+120 -C -e ppp &
```

w którejkolwiek z sekcji menu. Dla przykładu możesz dodać to w sekcji
Programy.

Dodając poniższy wpis, otrzymasz przycisk na belce GoodStuff:

```sh
*GoodStuff xppp  modem.xpm      Exec "xppp" xterm -ls -g 80x24+360+120 -C -e ppp &
```

W tym przykładzie, jako plik ikony przycisku posłużyłem się plikiem
modem.xpm, który spowoduje uruchomienie xterm'a oraz uruchomi ppp tak
jakbym wpisał to w linii komend. Teraz przy uruchamianiu odpal moduł
GoodStuff i już\! Kiedy tylko będziesz chciał, wciśnij przycisk, a
odpali się program xppp.

To już kompletna rewelacja...

Kiedy połączenie zostanie nawiązane i uruchomione, zrób ikone z xterm'a
z uruchomionym ppp i uruchom program "pppon". Uaktualni to plik
/etc/hosts, sprawdzi pocztę, uruchomi programy w tle, pogłaszcze kota i
zrobi pranie ;-). LINUX jest wspaniały.

Jestem zainteresowany wszelkimi Waszymi sugestiami, krytyką lub
komentarzami na temat tych programów. Jednym z moich pomysłów jest
napisanie prostego programu, który zrobiłby to wszystko automatycznie, a
swoje opcje określał na podstawie wpisów z pliku \~/.xppprc. Wtedy,
uruchamiając jeden program w ciągu sekundy mielibyśmy już wszystko
gotowe do pracy.

Musze o tym kiedyś pomyśleć...

Tym niemniej, [dajcie znać](mailto:fiskjm@ctrvax.vanderbilt.edu)
jak Wam to wyszło, nie wyszło i co o tym sądzicie\!

