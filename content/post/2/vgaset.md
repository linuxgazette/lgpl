---
title: Ustawiamy X z wykorzystaniem vgaset
date: 2022-02-09
---

* Autor: John M. Fisk
* Tłumaczenie: Marcin Niedziela
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.aug.html

---

Istnieje takie małe narzędzie, które śmiało można umieścić w kategorii
*muszę-to-mieć*: **vgaset**. Co to robi? Przyda Ci się do podkręcenia
geometrii wyświetlania Twojego X Window. Zazwyczaj, gdy znalazłeś wpis
modeline który działa, nie ważne że jest trochę koślawy i skrzywiony po
lewej stronie vgaset potrafi interaktywnie pomóc Ci tak, że nie będziesz
już musiał przejmować się ustawieniami monitora, by ustawić wielkość i
ułożenie wyświetlanego obrazu. Jeśli Twój monitor nie posiada
regulatorów obrazu, vgaset okaże się dla Ciebie jeszcze bardziej
przydatne jak będziesz wiedział jak tego użyć.

Tak więc, OK. Co powinieneś wiedzieć? Vgaset wymaga informacji o trybie
pracy jakiego używasz. Oto co powinieneś znać:

* **częstotliwość odświerzania obrazu** z jakiej korzystasz;
* **minimalną i maksymalną pionową i poziomą częstotliwość** jaką wspiera 
Twój monitor;
* wielkość w pikselach **pulpitu lub wirtualnego pulpitu** jakiego używasz.

Nie jest to bardzo trudne. Moje aktualne ustawienia wyglądają tak:

```
        Section "Monitor"
        Identifier "RandomMonitor-0"
        VendorName "Unknown"
        ModelName "Unknown"
        BandWidth 70                     
        HorizSync 31.5* 48.5
        VertRefresh 50* 100
        Modeline "800x600" 49.87 792 808 928 1032   768 768 772 785 +hsync
        Modeline "1024x768" 64.98 1024 1032 1176 1344  768 768 774 785 -hsync
        Modeline "640x480" 31.50 712 752 798 896  503  511  514  544
        EndSection
```

Z instrukcji do mojego monitora wiem, że minimalna i maksymalna pozioma
częstotliwość to 31.5* 48.5 KHz, a minimalna i maksymalna częstotliwość
pionowa to 50* 100 Hz. Korzystam z wirtualnego pulpitu w rozdzielczości
"800x600" o częstotliwości odświerzania 49.87Hz.

Teraz, aby używać vgaset, uruchamiam xterm z podaniem poniższych opcji:

```sh
vgaset -d49.87 -x31.5 -X48.5 -y50 -Y100 -v 1152 900
```

**WAŻNE:** Zadzieranie z ustawieniami monitora może nieodwracalnie go
uszkodzić, dlatego ZAMIN UŻYJESZ VGASET PRZECZYTAJ JEGO PODRĘCZNIK
UŻYTKOWNIKA.

Tylko nie mów, że Cię nie ostrzegałem. Jednak jeśli będziesz trzymał się
wskazówek, vgaset może być bardzo pomocne. Jak raz go uruchomisz,
będziesz mógł bez problemów zmieniać ustawienia wyświetlania w X
Window.

Istnieją bardzo proste polecenia, które pozwalają Ci na zmianę ustawień:

``` 
    Polecenia:
        
    l* zmniejsz lewy margines
    L* zwiększ lewy margines
    r* zmniejsz prawy margines
    R* zwiększ prawy margines
    h* zmniejsz wielkość poziomą
    H* zwiększ wielkość poziomą
    t* zmniejsz górny margines
    T* zwiększ górny margines
    b* zmniejsz dolny margines
    B* zwiększ dolny margines
    v* zmniejsz wielkośĆ pionową
    V* zwiększ wielkość pionową
    x* zmniejsz rozdzielczość osi X
    X* zwiększ rozdzielczość osi X
    y* zmniejsz rozdzielczość osi Y
    Y* zwiększ rozdzielczość osi Y
    ?* resetuje podane wartości
    CR* wyświetla ustawienia.
    q* wyjście
    EOT* wyjście
```

Tak więc, jeśli twój ekran jest za szeroki/wąski użyj komendy x lub X,
by zmienić rozdzielczość poziomą. Podobnie, skorzystaj z y lub Y, by
zmienić rozdzielczość pionową. Użyj L, R, l, r, by ustawić odpowiednio
pozycję marginesów tak, aby obraz był wycentrowany.

Kiedy już ustawisz rozmiar oraz pozycję obrazu, przyjrzyj się pozostałym
wartościom. Na moim komputerze to wygląda jakoś tak:

```
    Horizontal frequency: 48.3 kHz, vertical frequency: 58.3 Hz
    Horizontal sync  2.41 us, vertical sync 82.78 us
    Horizontal retrace  4.81 us, vertical retrace 786.36 us
    Top margin increase
     50     792 808 928 1032        791 791 795 830
```

Jest tam WIELE informacji jednak Ty powinieneś skupić się na ostatniej
linii. Wykorzystaj poniższy wpis w swojej konfiguracji:

```
modeline "800x600" 49.87  792 808 928 1032  791 791 795 830
```

w pliku XF86Config w sekcji "Monitor". Dodaj go jako pierwszy wpis w
podsekcji "Display", by był on wykorzystywany przez X jako główny tryb.

Niezłe co?

OK, jak dotąd udało nam się jedynie dokonać małych zmian. WIELKA MI
RZECZ...nie? Cóż, teraz dopiero damy naszym Xom kopa...\!

