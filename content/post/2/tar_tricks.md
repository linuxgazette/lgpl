---
title: Więcej sztuczek z tar
date: 2022-02-09
---

* Autor: John M. Fisk
* Tłumaczenie: Marcin Niedziela
* Original text: https://linuxgazette.net/issue01to08/linux_gazette.aug.html

---

OK, dlaczego zdecydowałem się na ten temat..?

Cóż, czy kiedyś chociaż raz spojrzałeś na strony podręcznika man dla
polecenia `tar` -- ma on tysiące linii tekstu i jest jednym z
najdłuższych ze wszystkich dostępnych dla UN\*Xa. Przypomina on swoim
charakterem te wszystkie tajemnicze mity na temat *UNIKSA...*

Mianowicie to, że będzie wiele do powiedzenia na ten temat.

Pokłony dla MS Windows... Dobra, dobra\! Wiem, że wszyscy chcielibyście
spalić Billa G. i jego Złe Imperium ma popiół... jednak wstrzymajcie
pochodnie przynajmniej na sekundę... Czy kiedykolwiek zauważyłeś, że gdy
usuwasz coś pod Windows atakowany jesteś masą pytań, czy aby na pewno
chcesz to zrobić? Zazwyczaj są to teksty typu: "Cześć, jestem Twoim
nowym Systemem Operacyjnym Windows2005 i mam zamiar wyczyścić dysk
twardy". W UN\*Xach nie ma czegoś takiego i nie zareaguje on w ogóle
nawet jeśli bezmyślnie próbujesz wpisać

```sh
su
cd / ; rm -rf *
```

tylko dlatego, że Twój mądraliński kolega Jasiek, chciał Ci pomóc w
pozbyciu się jakiś niepotrzenych plików.

Jednak się zgadzam...

W rzeczywistości Tar jest całkiem imponującą bestią i prawdopodobnie
jest on także kwintesencją programów użytkowych dla uniksa. Zbawienne
jest to, że do jego używania trzeba nauczyć się kilku podstawowych
rzeczy. Jednakże, nudząc sie w deszczowe sobotnie popołudnie, kiedy już
poprzeglądasz wszystko co masz w szafie, warto będzie zapoznać się z
manualem dla tar - jest tam wiele jego ciekawych i ukrytych możliwości.

Pozwólcie, że wyjaśnię.

Miłą rzeczą związaną tar jest dla nas możliwość " kasetowych kopii " w
celu archiwizacji naszych danych. Wystarczy dodać opcję " M " w linni
poleceń i wszystko ustawione. Więc jeśli chcesz wykonać kopię zapasową
swojego katalogu /home/me możesz użyć polecenia:

```sh
cd /
tar cvMf /dev/fd0 /home/me
```

Jeśli teraz masz ochotę na wykorzystanie absolutnych ścieżek dostępu --
nie baw się we wpisywanie " / " -- dodaj opcję "P".  
Jeśli chcesz zachować prawa dostępu do plików -- dodaj opcję "p".  
Jeśli chcesz nadać nazwę woluminowi -- dodaj opcję "-V &
quotJakaś\_krórka\_nazwa "

Teraz musisz trochę postukać w klawiaturę ale rezultat będzie pomyślny:

```sh
cd /
tar -cvpMP -V "Home Directory Backup" -f /dev/fd0 /home/me
```

Wystarczy wpisać takie polecenie dla jakiegoś katalogu na dysku. Kiedy
skończysz, będziesz mógł zapisać całą swoją prace na dyskietce a potem
wystarczy wpisać:

```sh
tar -tvf /dev/fd0
```

Powinieneś zobaczyć wolumin swojego archiwum. Jeśli wszystko będzie się
za szybko przewijać, to w zależności od upodobań, dodaj do polecenia "
|more " lub " |less ", by przeglądać zawartość stronami.

Teraz coś trochę bardziej zabawnego... i kończymy z tym na dzisiaj.
Szczerze.

Przypuśćmy, że chciałbyś od czasu do czasu zrobić kopię zapasową
określonych plików. Możesz to zrobić przez *dodawanie* po kolei każdego
pliku przy użyciu opcji "-A". Jednak to bardzo dużo roboty. Zamiast
tego, pozwól aby tar sam czytał listę plików, z których zrobi kopie
zapasową. Użyj swojego ulubionego edytora tekstu, by stworzyć listę
plików do kopii zapasowej, wpisuj po jednym pliku w każdej linii, a
potem wydaj polecenie:

```sh
tar -cvpPM -V "Moja kopia plikow" -T "./lista.plików" -f /dev/fd0
```

a wszystkie pliki z Twojej listy znajdą się w zarchiwizowanej kopii
zapasowej. Fajosko.

No dobra, ale do czego to się może nam przydać? Jeśli jesteś swoim
własnym adminem, a podejrzewam, że większość z Was nimi jest, to pewnie
przez te kilka miesięcy zabawy z systemem dostosowałeś go do swoich
potrzeb. Wystarczy jeden wredny program z pozoru niegroźna pomyłka, a
ten stuningowany przez Ciebie system może stać się historią.

Jeśli nadal zasilasz szeregi marzących o napędzie taśmowym, to dobrym
posunięciem będzie kupno kilku dyskietek i przeznaczyć je na
przechowywanie kopi zapasowych plików, przynajmniej dla najważniejszych
plików. Dla przykładu, jeśli narobiłeś sobie bałaganu w X Window to
pewnie nie raz Ci zdechnie system... Stwórz sobie listę plików
konfiguracyjnych:

```
/usr/X11R6/lib/X11/XF86Config
/usr/X11R6/lib/X11/xinit/xinit.fvwm
/usr/X11R6/lib/X11/app-defaults/XTerm
...
/usr/X11R6/lib/X11/fvwm/system.fvwmrc
/home/me/.Xresources
/home/me/.fvwm
```

czyli coś takiego. Nazwij ją jakkolwiek chcesz. Zamiast przemęczać się i
wpisując po kolei te pliki, powiedz programowi tar gdzie masz ich listę.
Możesz w ten sposób łatwo tworzyć kopie wszystkich zmiennych plików
systemu jakie używasz. Mam tu na myśli X Window, drukowanie,
konfigurację SLIP lub PPP, pocztę, i dużo innych. Jeśli zrobisz własne
kopie, będziesz mógł wykorzystać je kiedy tylko przyjdzie taka potrzeba.

Oczywiście możesz zostawić wszystko tak jak jest, i robić wszystko
ręcznie. Twój wybór.

Może jeszcze jedna wskazówka:

Niech program "find" zrobi za Ciebie całą brudną robotę. Jak? Powiedzmy,
że chcesz zrobić kopie wszystkich plików w katalogu /usr/lib/ppp. Masz
tam wszystkie swoje połączenia z kilu tygodni. Człowieku, te połączenia
z siecią są jak Twoja **krew**\! By zaoszczędzić sobie ciężkiej pracy
wpisz cos takiego:

```sh
find /usr/lib/ppp > backup.ppp.list
```

i już to masz. Wszystkie nazwy plików znajdą się w pliku bacup.ppp.list.
Jeśli jesteś kawał cwaniaka, pewnie napiszesz sobie skrypt, który będzie
to robił za Ciebie. Mam dla Ciebie małą radę -- jeśli jeszcze nie
nauczyłeś się skryptować, **ZRóB TO\!**

Skrypty Shellowe mogą uczynić Twoje życie milion razy łatwiejszym. Co
prawda nie pójdą za Ciebie w piątek na randkę z jakąś ostrą laską, czy
też posprzątają pokoju tak jak to robi mama (szkoda :))... ale potrafią
przejąć na siebie nudne, rutynowe i męczące zadania. Zdobądź na ten
temat jakąś dobrą książkę i ucz się.

Ale za bardzo oddalam się od tematu. Wróćmy do tar.

Czas teraz na parę *uwag*. Po pierwsze, jeśli stworzyłeś wielotomowe
archiwum, możesz przejrzeć jego zawartość korzystając z opcji "-w",
jednak co ważne, nie będziesz w stanie tego zrobić z plikiem
potraktowanym gzipem, jeśli używasz GNU tar (który oczywiście jest dużo
lepszy niż Brand X tar). Tar zwyczajnie nie będzie w stanie przejrzeć
lub kompresować wielotomowego archiwum. Myślisz sobie pewnie, co z
tego...

Błąd.

Ja pomęczyłem się trochę przy robieniu 5MB kopii plików dźwiękowych .au.
Tak więc, skorzystałem z naszej sztuczki z **find** i zrobiłem listę
plików, wydałem odpowiednie zaklęcie i pozostało mi tylko patrzeć jak
tar mięli po /dev/fd0. Nie przeszkadzało mi to, aż do czasu zmiany 6
dyskietki na 7. Przy 8 dyskietce przerwałem proces wciskając Ctrl-c.
Ciekawe, prawda? Nie jestem może wybitnym naukowcem, ale jak dla mnie
wymagane 8 dyskietek by zapisać 5MB plików jest nieco absurdalne.

Tak więc moją uwagę przykuł **afio**. Afio jest jednym z tych małych
programów użytkowych, które bardzo ułatwiają życie. Nie będę za bardzo
tutaj przedstawiał wszystkich jego zalet (mówimy tu przecież o tar,
prawda...), więc powiem tylko, że udało mi się łatwo zmieścić moje 5MB
plików dźwiękowych na czterech dyskietkach, a nawet zostało jeszcze
trochę miejsca. Jeśli zdobędziesz archiwum, przejrzyj sobie plik README.
Znajdziesz tam kilka ciekawych informacji o tym jak to wszystko działa.
Zasadniczo wystarczy wstukać:

```sh
find . -print | afio ivs 1440k -F /dev/fd0
```

by zarchiwizować pliki z aktualnego katalogu na dyskietce umieszczonej w
pierwszym napędzie dysków. Zasadniczo Aifo działa w formie skryptu
shellowego i wymaga źródła do plików. Możesz to robić za pomocą
polecenia cat by stworzyć listę plików, którą wykorzysta afio. Dodając
"Z" w linii jako opcja, stworzysz skompresowane archiwum.

Miłym akcentem używania **afio** z opcją kompresji jest to, że
przechowuje on archiwa, więc jeśli plik archowum zostanie uszkodzony --
to może się zdarzyć każdemu -- to resztę plików będzie można bez
problemów odzyskać. Jeśli używając tar i gzip wykonasz jakiś błąd, to
masz pecha kolego. To już *wszystko*.

No dobrze.

W tej chwili, pewnie juz jesteście trochę zmęczeniu, więc zakończymy
tutaj. Zapewne na swoim ulubionym serwerze z aplikacjami dla Linuksa,
znajdziesz dużo więcej poręcznych programów i skryptów do wykonywania
kopii zapasowych. Nie mogę o nich nic powiedzieć, gdyż żadnego z nich
nie używałem.

Znalazłeś coś ciekawego? Daj mi o tym znać\!

