---
title: Pythonowa Stacja Pogody
date: 2021-10-15
---

* Autor: Phil Hughes
* Tłumaczył: Marcin Dec
* Original text: https://linuxgazette.net/issue94/hughes.html

---

Omawiany program to prosty interfejs, umożliwiający stworzenie strony WWW z danymi ze stacji obserwacji pogody z całego świata zebranymi 
przez system Metar. Nie nazwałbym tego ekscytującym, ale to naprawdę działa.

Zamiast opisywać co będzie efektem programu, zapraszam [tutaj](https://web.archive.org/web/20040612114116/http://pictures.fylz.com/weather.html). 
Powinniście zobaczyć co najmniej dwa a najprawdopodobniej pięć raportów pogodowych z okolic Costa Rica. A zatem, program zawiera listę pięciu 
stacji pogody i wyświetla informacje pobrane z ich raportów.

## Struktura Systemu

Sercem tego systemu jest pakiet Pymetar, dostępny tutaj. Jest to program napisany w Pythonie, który pobiera dane systemu Metar opisanego
[tutaj](https://web.archive.org/web/20040612114116/http://www.noaa.gov/). Pymetar jest narzędziem obsługiwanym z linii poleceń, ale to 
właśnie on wykonuje całą czarną robotę.

Moim celem jest udostępnienie tych informacji na stronie WWW. Nie chciałem aby to zadanie stało się wielkim projektem programistycznym, 
ale chciałem, żeby implementacja miała sens. Najprostszym podejściem byłoby uruchomienie programu jako skryptu CGI na każde żądanie 
(żądanie strony www - przyp. tłum.). Jednakże byłoby to bardzo nieefektywne, ponieważ program musiałby pobierać dane (z systemu Metar - 
przyp. tłum.) za każdym żądaniem CGI. Co więcej, oznaczałoby to, że użytkownik musiałby czekać na wykonanie wszystkich żądań.

Stwierdziłem, że najlepszym rozwiązaniem będzie ustawienie crona tak, aby pobierał dane oraz generował pogodową stronę www. Wówczas każde 
żądanie strony www będzie wymagało jedynie wyświetlenia statycznej strony. Ponieważ dane o pogodzie nie zmieniają się zbyt często, takie 
rozwiązanie oferuje wyświetlanie informacji w miarę "na bieżąco".

## Implementacja

Na początek kod:

```py
#!/usr/bin/env python

import sys
import time
sys.path.insert(0, "/home/fyl/pymetar-0.5")
import pymetar

def stations(args):
	for arg in map(lambda x: x.strip(), args):
		try:
		weather = pymetar.MetarReport(arg)
	except IOError, msg:
	# odkomentuj poniższą linię i wytnij linię z pass aby zobaczyć błędy
	# sys.stderr.write("Problem accessing the weather server: %s\n" % msg)
	pass
	else:
	if weather.valid:
		print "<h3>"
		print weather.getStationName()
		print " ( Lat: %s, Long: %s, Alt: %s m)" % \
	weather.getStationPosition()
		print "</h3>"
		print "<table border=\"2\">"
		print "<tr><td>Updated</td><td> %s</td</tr>" % \
	weather.getTime()
	if weather.getWindDirection() is not None:
		print "<tr><td>Wind direction</td><td> %s°</td></tr>" % \
	weather.getWindDirection()
	if weather.getWindSpeed() is not None:
		print "<tr><td>Wind speed</td><td> %6.1f m/s</td></tr>" % \
	weather.getWindSpeed()
	if weather.getTemperatureCelsius() is not None:
		print "<tr><td>Temperature</td><td> %.1f°C (%.1f°F)</td></tr>" % \
	(weather.getTemperatureCelsius(), \
	weather.getTemperatureFahrenheit())
	if weather.getDewPointCelsius() is not None:
		print "<tr><td>Dew point</td><td> %.1f°C
	(%.1f°F)</td></tr>" % \
	(weather.getDewPointCelsius(), \
	weather.getDewPointFahrenheit())
	if weather.getHumidity() is not None:
		print "<tr><td>Humidity</td><td> %.0f%%</td></tr>" % \
	weather.getHumidity()
	if weather.getVisibilityKilometers() is not None:
		print "<tr><td>Visibility</td><td> %.1f Km</td></tr>" % \
	weather.getVisibilityKilometers()
	if weather.getPressure() is not None:
		print "<tr><td>Pressure</td><td> %.0f hPa</td></tr>" % \
	weather.getPressure()
	if weather.getWeather() is not None:
		print "<tr><td>Weather</td><td> %s</td></tr>" % \
	weather.getWeather()
	if weather.getSkyConditions() is not None:
		print "<tr><td>Sky conditions</td><td> %s</td></tr>" % \
	weather.getSkyConditions()
		print "</table>"
	else:
		print "Either %s is not a valid station ID, " % arg
		print "the NOAA server is down or parsing is severely broken."


print "<html>"
print "<head>"
print "<title>Costa Rica weather from PlazaCR.com</title>"
print "</head>"
print "<body>"
print "<h1>Costa Rica weather from PlazaCR.com</h1>"
print "<p>Latest reports as of %s CST" % time.ctime()
gm = time.gmtime()
print "(%d.%02d.%02d %02d%02d UTC)" % (gm[0], gm[1], gm[2], gm[3], gm[4])
print '<p><a href="images/costa_rica.gif" target="_blank">Costa Rica map</a>'

stations(["MROC", "MRLM", "MRCH", "MRLB", "MRPV"])

print "</body>"
print "</html>"
```

Postanowiłem po prostu zaimportować kod pymetar.py do wrappera generującego stronę WWW. Aby to osiągnąć, dodałem katalog Pymetara do 
ścieżki przeszukiwanej przez Pythona.

Następnie zdefiniowałem funkcję **stations**, która za pomocą kodu Pymetara pobiera dane ze stacji pogodowych a następnie formatuje je w 
kod HTMLa. Wygląda to nieco brzydko ponieważ są to po prostu instrukcje print, tworzące kod HTMLa, z kilkoma instrukcjami if 
sprawdzającymi czy rzeczywiście otrzymaliścmy odpowiednie dane. Ważne jest to, że do tej funkcji przekazuje się listę nazw stacji, a otrzymujemy "ciało" strony WWW.

Na koniec, bodajże ostatnie 15 linii kodu zajmuje się tworzeniem szkieletu strony HTMLa oraz wywołuje funkcję stations aby wygenerować 
główną część strony.

## Testowanie i Instalacja

Dzięki zastosowanej konstrukcji programu, jego testowanie jest bardzo proste. Nie ma żadnych zależności opartych na WWW, a więc 
można go po prostu uruchomić z linii poleceń.

Nazwałem ten program wcr, a więc polecenie ./wcr uruchomi program i wyświeli stronę HTMLa na standardowym wyjściu. Jeśli wszystko poszło 
dobrze, uruchom program ponownie, przekierowując jego wyjście do pliku. Na przykład,

`./wcr > /tmp/weather.html`

Możesz teraz wskazać ten plik przeglądarce internetowej i sprawdzić czy wyświetla stronę tak jak chciałeś. Jeśli nie, możesz wprowadzić 
zmiany do wcr i kontynuować testowanie.

Gdy juz jesteś zadowolony z wyglądu strony, skopiuj kod programu na swój serwer WWW i ustaw zadanie crona tak aby go uruchamiał. 
Normalnie, polecenie crontab -e umożliwi ci edycję twojego wpisu crontaba.

Ja postanowiłem uruchamiac program dwa razy w ciągu godziny, 5 oraz 35 minut "po". Wpis crontaba musi uruchamiać program i zapisywać 
jego wyjście do pliku dostępnego przez serwer WWW. Mój wpis wygląda tak:

`5,35 * * * * /home/fyl/pymetar-0.5/bin/wcr > /var/www/htdocs/weather.html`

Cztery gwiazdki wskazują cronowi ze 5 i 35 minut dotyczy każdej godziny każdego dnia. Następne pole to nazwa programu do uruchamiania. 
Na koniec, operator przekierowania (>) oraz lokalizacja, w której plik HTMLa jest zapisywany.

Zakładając, że ustawiłeś odpowiednie uprawnienia -- a więc program może zapisywać do pliku oraz serwer WWW może z niego czytać -- to 
wszystko co było do zrobienia. Po prostu wskaż w przeglądarce wygenerowany plik HTMLa i masz stronkę z pogodą.

## Wniosek

Jeśli jesteś perfekcjonistą, prawdopodobnie będziesz potrzebował bardziej wymyślnego rozwiązania. Dlaczego? Cóż, mogą wystąpić momenty, 
gdy zawartość pliku z kodem HTMLa będzie niewłaściwa. Gdy cron nawali zawartość tego pliku może zostać obcięta. Wówcza program uruchamia 
się ponownie i tworzy nowy plik.

Z powodu sposobu w jaki program działa, czas tworzenia tego pliku to nie jest po prostu krótki czas wykonania pewnego kodu Pythona, 
program wysyła bowiem zapytania do kilku stacji obserwacji pogody i musi poczekać na odpowiedź. Z tymi pięcioma wybranymi stacjami 
zauważyłem czas opóźnienia od jednej do dziesięciu sekund. Jeżeli wyświetlanie na stronie WWW złych danych przez maksymalnie 10 sekund 
co pół godziny jest do zaakceptowania, to wszystko jest w porządku. Jeśli nie, zapisz wyjście do pliku tymczasowego i przenieś do 
właściwego pliku gdy wszystko będzie gotowe. Wciąż nie jest to perfekcyjne rozwiązanie, ale jest całkiem blisko.

Natomiast my, zwykli śmiertelnicy, mamy szybką i brzydkawą stonę z danymi o pogodzie. Miłej zabawy.
