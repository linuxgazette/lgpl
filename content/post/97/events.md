---
title: Ostatnie wydarzenia oraz działania SSC Inc. dotyczące znaku handlowego
date: 2021-10-15
---

* Autor: Rick Moen
* Tłumaczył: Artur Szymański
* Original text: https://linuxgazette.net/issue97/moen.html

---

Cofając się do 28 października 2003 roku: Cała załoga Linux Gazette (jednogłośnie) zdecydowała się na przeniesienie magazynu pod nowy adres, 
po raz pierwszy od czasu, gdy w 1996 John M. Fisk (twórca LG) przekazał go pod kuratelę SSC Inc. i wydawcę Phila Hughesa.

Zaistniało kilka przyczyn takiego obrotu sprawy; jedną z dwóch największych było potajemne usunięcie przez SSC materiałów (w tym całych artykułów) 
z archiwalnych wydań oraz totalne olanie naszych pytań o taki stan rzeczy, gdy zorientowaliśmy się w braku tych materiałów. Ostatni raz próbowaliśmy 
dowiedzieć się tego 26 października: Gdy po 48 godzinach nie doczekaliśmy się odpowiedzi SSC, zdecydowaliśmy się odejść.

We wtorek, 28 października napisaliśmy do Hughesa bardzo uprzejmy list, w którym dziękowaliśmy mu za siedem lat współpracy i prosiliśmy go o 
wsparcie w naszym przejściu. Poprosiliśmy go również o rozwiązanie sprawy pogwałcenia przez SSC (przypuszczalnie nieumyślnego) dotychczasowych 
praw autorskich do powtórnie opublikowanych starych artykułów. Mamy grudzień, a te naruszenia w dalszym ciągu istnieją np., zamienione prawa 
(copyright) autorów artykułów na prawa firmy SSC (nieprawdziwie wskazujące na własność SSC) dwóch poniższych artykułów (stopka):

http://www.linuxgazette.com/node/view/58
http://www.linuxgazette.com/node/view/61

Nasz list do Pana Hughesa wysłany 28 października o odejściu załogi można przeczytać pod poniższym adresem. Polecamy go przeczytać:

http://lwn.net/Articles/57161/

Artykuł ten był naszym stanowiskiem, gdy publikowaliśmy w listopadowym (numer 96) wydaniu artykuł [Krótka historia Linux Gazette](../96/history.md). 
Ten artykuł ma na celu zaktualizowanie wiedzy czytelników LG z rozwojem sytuacji od tamtego czasu.

Pierwsza i zarazem najważniejsza przyczyna odejścia Linux Gazette z ogródka SSC to: Pan Hughes i jego webmaster wyraźnie dawali nam do 
zrozumienia, że zamierzają zrobić z LG tylko i wyłącznie dynamiczną stronę, bez comiesięcznych wydań i bez redaktorów. W efekcie tego LG nie 
byłoby magazynem, a strona http://www.linuxgazette.com/ stałaby się jeszcze jednym forum dyskusyjno-niusowym w rodzaju Slashdot.

W istocie oznaczałoby to koniec Linux Gazette w całej znanej formie. LG już zaczęła się psuć poczynając od lata, ponieważ kilku autorów przestało 
współpracowac w tworzeniu artykułów w proteście przeciwko eliminacji redakcji, a jakość pozostałych artykułów wyraźnie spadała w dół. A zatem załoga 
miała do wyboru albo trwać w takim marazmie, albo przenieść magazyn.

Wielu komentatorów czuło, że SSC ma moralne prawo do tworzenia Gazety, ponieważ jej założyciel John Fisk przekazał ją w 1996 Panu Hughesowi i jego 
firmie. **Niemniej jednak 28 października nasze pytanie nie dotyczyło tego czy SSC ma prawo wydawać Gazetę, ale czy ma prawo do jej zabijania pomimo 
sprzeciwu zespołu LG.**

Powyższe jest ważnym stwierdzeniem, ponieważ w momencie opublikowania numeru listopadowego sytuacja momentalnie uległa zmianie: Po ogłoszeniu 28 
października, że wydawanie magazynu będzie kontynuowane gdzie indziej, SSC najwidoczniej zmieniła politykę i zdecydowała się w dalszym ciągu publikować 
poszczególne numery -- włączając do składu magazynu swoich pracowników. Nie wiedzieliśmy o tym zanim bez ostrzeżenia nie zauważyliśmy niespodziewanego 
ukazania się drugiego magazynu Linux Gazette (na linuxgazette.com).

I choć zespół ubolewa nad tym zamieszaniem, to nie był nasz pomysł, a gwałtowna zmiana polityki SSC była poza naszą wiedzą i kontrolą. Robimy co w 
naszej mocy aby rozwiązać ten problem. Widzimy też, że choć SSC znowu wydaje comiesięczne numery, to nie pisnęła nawet słówka redaktorom o tym fakcie.

Ponadto robimy wszystko co możemy aby uniknąć wojny z SSC w tym temacie oraz aby nie było niepotrzebnych działań i komentarzy. Niestety to nastawienie 
nie jest odwzajemniane:

1. Dokładnie tego samego dnia, gdy SSC otrzymało naszą notkę o odejściu, firma nagle wyasygnowała 300 dolarów i złożyła podanie w Biurze Patentowym 
USA (USA Patent and Trademark Office - USPTO) o rejestrację nazwy "Linux Gazette" jako własnej marki (opisane poniżej). Tym samym SSC udowadnia, że 
używała komercyjnie tej marki od 1 sierpnia 1996 roku.

2. Działając wstecz, SSC zaczęła dodawać symbol "TM" (Trade Mark - znak handlowy - przyp. tłum.) do naszej nazwy magazynu na swoich stronach. "TM" 
jest nienormowanym symbolem używanym do zapewnienia handlowej tożsamości marki dla sprzedawanych dóbr. To samo odnosi się do komercyjnych usług co 
technicznie nazywa się service mark (znak usługowy - przyp. tłum.) i może być publicznie przedstawiane jako symbol "SM" (a nie "TM").

3. 3 grudnia 2003 otrzymaliśmy list od Phila Hughesa z SSC, z kopią do naszego opiekuna domeny, twierdząc iż "Linux Gazette" jest znakiem handlowym 
SSC a zatem posiadanie i uzywanie jej w domenie linuxgazette.net narusza prawa handlowe SSC. Główną inetncją SSC jest przejęcie naszej domeny zgodnie 
z wezwaniem ICANN Uniform Domain-Name Dispute-Resolution Policy (UDRP), bez udowadaniania tego w sądzie.

Nasza próba oszacowania i odpowiedzi na ten atak na naszą obecność w Internecie — powstała dokładnie w momencie zamykania numeru — jest jedną z 
przyczyn opóźnienia tego numeru, za co oczywiście przepraszamy. (drugą przyczyną była chęć zmiany loga i wyglądu strony a to wymagało aktualizacji 
kilku szablonów i skryptów).

Przedstawiam tutaj krótkie objaśnienie w nawiązaniu do prawa znaku handlowego. (nie jestem prawnikiem tak więc ludzie chcący podejmować decyzje 
biznesowe powinni skonsultować się z odpowiednimi ekspertami, a nie opierać się na mojej analizie).

Prawo dotyczące znaków handlowych tyczy się każdego komercyjnego "znaku firmowego", który wprowadzasz na rynek. W przeciwieństwie do popularnych 
przesądów, ty jako właściciel znaku handlowego nie możesz innym uniemożliwić jego używania w identyfikowaniu swojego produktu w ogólnym rozumieniu — 
ale możesz zakazać innym oferowania konkurencyjnych, komercyjnych dóbr lub usług (w tej samej branży) używających twojego znaku firmowego do zmylenia 
swoich klientów, którzy mogą mysleć o nich jako o Twoim produkcie.

Przykładowo wywieszając wizytówkę "Viking Network Consulting[SM]", ustaliłbym identyfikację swojego znaku firmowego zgodnego z moją profesją: z 
prawnego punktu widzenia, nazwa mojej firmy byłaby rozpoznawana jako znak usługowy. Chciałbym zatem ukrócić sprzedaż usługi "Viking Firewall Installation" 
przez konkurencje, ponieważ moi klienci mogliby prawdopodobnie myśleć, że produkuję lub dostarczam takie usługi.

Zauważcie, że zasięg znaku handlowego czy usługowego, jeśli naprawdę jest waszą własnością, rozciąga się wyłacznie na (1) używanie go przez innych 
w komercyjnych rozwiązaniach, (2) branże, w której działasz oraz (3) ten sam obszar geograficzny. Nie mogłbym zakazać różnych Viking Network Consulting 
działających w Trondheim, ani Viking Senior Computer Volunteers oferujących wolontariat dla emerytów.

Zabezpieczenie na inne rejony geograficzne ważne jest wtedy, gdy zapłacisz USPTO 300 dolarów za każde 10 lat, oznaczające że używałeś znaku w handlu 
w swoim polu biznesowym przed innymi zgłaszającymi. Na to właśnie powołała się SSC w swoim zgłoszeniu do USPTO. Jeśli urzędnik nadał takie prawa, taka 
rejestracja upoważnia potem właściciela do wstawiania "®" (symbol prawny) do znaku firmowego zamiast kulawych symboli "TM" czy "SM".

I tym sposobem wracamy do pytania kto jest "właścicielem" nazwy Linux Gazette. Słuszne pytanie. Pytanie o prawa autorskie jest jasne: Prawa do wszystkich 
materiałów należą do każdego ze współpracowników i są publikowane na podstawie licencji OPL 1.0 — takie było wcześniejsze założenie, bo LG zawsze było 
kategorycznie wolnym magazynem. Cytując założyciela LG, Johna Fiska, z numeru 8 (sierpień 1996), który pragnął wyjaśnić motywy przekazania LG (wolnego
magazynu) do SSC, firmy komercyjnej:

    Cóż, po rozmowie o tym z Philem Hughesem, zdecydowałem sie na przekazanie Linux Gazette pod opiekę Linux Journal. Myślę, że Gazeta dowiodła, że jest 
    to dobry pomysł — to wolnodostępna i otwarta na wszystkich publikacja online, doskonale dzieląca się z innymi informacjami i pomysłami. Istnieje wiele 
    rzeczy, które mogłyby być z nią zrobionych i jestem doprawdy podekscytowany tym, że Gazeta kontynuuje swoje tradycje.

    Wiedzcie również o tym, że Linux Gazette była, jest i będzie całkowicie wolną publikacją. Nie mogę tego bardziej zaakcentować: wiem, że pasją wielu 
    ludzi jest trzymanie Linuksa z daleka od komercji. Nie mogę się do końca z tym zgodzić, bo w moim odczuciu komercja i wolność mogą ze sobą żyć w 
    zgodzie i w rzeczywistości wzajemnie się uzupełniać. Jak mówiłem, jestem naprawdę zadowolony z tego, że Gazeta była dla wszystkich dostępna za darmo 
    i że będzie to kontynuowane. 

Zgadzamy się z tym, że SSC miała moralne prawo do LG od Johna M. Fiska i przez siedem lat dobrze wypełniała rolę jej opiekuna. Nasz punkt widzenia zmienił 
się w 2003, gdy SSC zapowiedziała plan (w istocie) zabójstwa magazynu w takiej formie oraz gdy ukradkiem skasowała z linuxgazette.com poprzednie wydania 
i ich mirrory.

Rozważmy, dla porównania, hipotetyczną sytuację gdy Linus Torvalds ogłasza, że zamierza skończyć z projektem kernela Linuksa. Sytuacja jest prawie 
analogiczna: zespołowy wkład w pracę kierowany jest przez jedną grupę, ale prawa autorskie pozostają w rękach każdego współpracownika, a materiał jest 
wydawany z licencją open-source. Byłoby nam przykro widząc odejście Linusa, ale wątpie abyśmy tracili zbyt dużo czasu zanim byśmy zaczęli rozwijać kernel 
w innym miejscu. Gdyby później poszedł po rozum do głowy i powrócił do pracy nad kernelem, sytuacja byłaby trudna (tak jak teraz to jest z dwoma Linux 
Gazette), ale nic byśmy nie powiedzieli mając nagle inny kernel.

Idąc dalej tropem praw moralnych, istnieje kilka prawnych uwarunkowań:

Ostatnie prawne żądania SSC co do zawładnięcia nazwą "Linux Gazette" uderza w nasze zasady i jest bezczelne: (1) Nie widzimy żadnego oznaczenia, że 
Fisk nadał SSC komercyjne prawa do Linux Gazette. Jeśli już to Fisk miał raczej przeciwne intencje. (2) W dodatku twierdzenia SSC, że komercyjnie używała 
nazwy od sierpnia 1996 wyglądają, według naszej wiedzy, niepoważnie. Możemy doszukać się, że nikt nie oferował komercyjnego wsparcia pod ta nazwą, ani SSC
ani nikt inny. I na koniec, (3) próba komercyjnego używania znaku handlowego jest taktyką silnej ręki — jest to doprawdy oburzające: istotą prawa znaku 
handlowego jest to, że niekomercyjne jego używanie nie łamie praw znaku handlowego. Koniec, kropka.

Poznając ośmioletnią historię Gazety można powiedzieć, że żadne znaki na niebie i ziemi nie wskazują na to aby można rozsądnie stwierdzić, że kiedykowliek 
było komercyjne wsparacie, czy to w 1996 czy dzisiaj. Mamy nadzieję wytknąć to w naszej odpowiedzi do SSC nawiązując do ich listu o przejęciu domeny zgodnie 
z ICANN UDRP, przyjęcie przez SSC finansowego dowodu dla LG od innych firm Linuksowych (cytując Hughesa jako jego uzasadnienie do praw do zanku handlowego) 
nie stwarza oferty komercyjnego wsparcia; robi to za darmo. Myślimy, że można to pokazać każdemu, włączając w to sędziego.

Chcielibyśmy zaznaczyć, że w dalszym ciągu nie jest naszym celem atakowanie SSC. Tak naprawdę 5 listopada rozwarzaliśmy jedną z kilku inicjatyw, w których 
dwie Gazety mogły by ze sobą żyć w zgodzie. (przedruk z dyskusji na IRC):

```
Frank: btw, czy ktoś jeszcze lituje sie nad Philem? *G*
JimD: formalnie tak. Tak było przez kilka dni.
Frank: dobra, ja też....
		 Czuję się okropnie jak on się musi czuć... ale nie czuję 
		 zbytniej przykrości aby do niego w tej sytuacji dzwonić...
JimD: nie będę mu współczuł gdy to współczucie odrzuca.
Rick: jimd: popieram i przypomina mi to o pewnym fakcie z tym związanym:
		Gdy kogoś zawstydziłeś, dobrze jest jeśli zaproponujesz mu 
		jakieś wyjście z sytuacji zamiast przystawiać go do muru. 
		Czułbym się winny pisząc list spierając się z ludźmi.
JimD: Nie widzę dla nas dobrego sposobu zaoferowania Philowi *innego* wyjścia 
	        z tego wszystkiego. Zaoferowaliśmy kilka.
Rick: To jest argument.
		Miałem na to pogląd który spróbuję teraz przedstawić.
		Zakładając że LG.com operowała z CMS i tym się zadowoliła.
		Może robić ze stroną co tylko chce. Wtedy współpraca z tą stroną
		jest taka, że jednymi z różnych źródeł kandydatów do newslettera LG 
		(przez OPL) są comiesięczne publikacje z LG.net, z oryginalnym 
		pochodzeniem z LG.net, ale mirrorowane na kilka innych miejsc w tym LG.com.
		Jeśli Phil nie zgodziłby się na zawartość w dowolnym wydaniu,
		miałby opcję nie mirrorowania całego wydania.
		Oczywiście inne mirrory też stosowałyby sie do tych praw.
		Korzyści: zachowanie twarzy, nie potrzeba transferować żadnej domeny.
		Phil dostaje zabawki webowe, my comiesięczny magazyn, który wtedy jest 
		tylko jeden. Nikt nie musi nic zmieniać. Nikt nie jest cenzurowany.
		Poza tym redaktorzy dostają kontrolę nad edycją magazynu, SSC dostaje
		kontrolę edycji nad rzeczami webowymi (inne niż magazyn).
		SSC pozostawia swoje linki do mirrorów magazynu z klauzulą
		"SSC Inc. niekoniecznie aprobuje blablabla."
		SSC może dalej twierdzić, że ma prawa do nazwy, ale nie próbuje jej 
		zarejestrować. Redaktorzy mogą ignorować żądania co do znaku handlowego 
		dowodząc że znak handlowy pozwala ci go używać TYLKO w zastosowaniach 
		komercyjnych, co zmyla klientów co do tego, że takie dobra są 
		produkowane przez właściciela marki.
      (Zobacz:  Lanham Act, US Code — łatwo je wyguglać).
JimD: Nie sądzę aby Phil myślał nad taką opcją.
Rick: też prawda. Może za jakiś czas.
JimD: Taa.
```

Wprawdzie obecnie SSC nie jest w nastroju do pracy z nami, a tylko do pracy na nas, np. rozpoczynając niespodziewany atak na nasze miejsce w Internecie. 
Niemniej jednak nasze drzwi sa otwarte, a my dalej traktujemy SSC jako naszego naturalnego przyjaciela i sprzymierzeńca w długim okresie czasu. Mamy 
nadzieję, że wreszcie sobie przypomną, pomimo urażenia ich pychy (ale nie ich interesów handlowych), iż spowodowali że instytucje społeczności Linuksa 
wyszły en masse (franc. tłum - przyp. tłum.).

Czytelników zapewniamy, że staramy się zapobiec zabraniu naszej domeny Internetowej i podejmiemy inne niezbędne kroki aby ochronić naszą nazwę. Jesteśmy 
Linux Gazette i będziemy robić to najlepiej jak potrafimy. Nasi czytelnicy na to zasługują.

## Sugerowana lektura

* [Dobry artykuł autorstwa Cory Doctorów z EFF](https://web.archive.org/web/20040117020533/http://www.openp2p.com/pub/a/p2p/2003/08/14/trademarks.html), 
wyjaśniający prawo znaków handlowych, szczegółowo opisując kruczki prawne.
* [FAQ About.com o znakach handlowych](https://web.archive.org/web/20040117020533/http://inventors.about.com/library/bl/toc/bltmfaq.htm)
* [Ogólny przegląd prawa znaków handlowych](https://web.archive.org/web/20040117020533/http://www.law.cornell.edu/topics/trademark.html)
* Bitlaw posiada doskonałe materiały:
** http://www.bitlaw.com/trademark/
** http://www.bitlaw.com/trademark/infringe.html
** http://www.bitlaw.com/trademark/dilution.html
** http://trademark.blog.us/blog/2003/10/01.html 

* [FAQ ChillingEffects.org EEF o znakach handlowych](https://web.archive.org/web/20040117020533/http://www.chillingeffects.org/trademark/faq.cgi) 
(szczególnie popatrzcie na "Jakie są limity prawa znaków handlowych?" (ang. "What are the limits of trademark rights?")). 

   
