---
title: Przyśpiewki w tonacji Pingwina - Songwrite
date: 2021-10-15
---

* Autor: Jimmy O'Regan
* Tłumaczył: Marcin Niedziela i Artur Szymański
* Original text: https://linuxgazette.net/issue97/oregan.html

---

Witam wszstkich w Piosenkach w tonacji pingwina. W kilku następnych wydaniach
postaram się przedstawć tylko kilka, z dużej ilość, Linuksowych pakietów dla
muzyków. Temat tej serii artykułów kryje w sobie tabulatury gitarowe (ten
artykuł), komponowanie muzyki, nagrywanie dźwięku w domowym stdio nagraniowym
dzięki oprogramowaniu takiemu jak Ardour i Ecasound oraz tworzenie
elektronicznej muzyki z kilkoma różnymi wstawkami.

Jeśli masz jakieś pytania lub sugestie, możesz do mnie majlować bez oporu,
jednak proszę miej na uwadze to, że korzystam z połączenia dial-up i płacę za
każdą minutę połączenia - tak więc nie wysyłaj mi plików dźwiękowych bez
wcześniejszego uprzedzenia mnie o tym. Jeśli uważasz, że nie opisałem czegoś w
wystarczająco wyczerpujący sposób chętnie jeszcze raz to opiszę - wszystko w tej
serii artykułów jest otwarte na sugestie, włączając w to także nazwę
(zdecydowałem się na Tux# (w oryginale tytuł zawiera słowo Tux, ale
przetłumaczyłem to jako Pingwin - przyp. tłum.), ale może to dezorientować fanów
Mono/.Net).

## Pisanie piosenek

W tym samym stopniu w jakim jestem fanatykiem komputerów, jestem także
gitarzystą. Po niewilkim doświadczeniu w kilku zespołach, zdecydowałem się uczyć
teorii muzyki; przynajmniej do takiego stopnia aby być w stanie pisać moje
własne tabulatury. Kolega pokazał mi jak działa GuitarPro dla Windows, którego
to systemu nadal potrzebuję aby uzyskać połączeie z Internetem (Winmodem), a
używając GuitarPro nauczyłem się wystarczająco dużo, by pisać samodzielnie swoje
własne tabulatury.

Nie byłem jednak tym ustatysfakcjonowany, dlategoteż zacząłem szukać programu
bazującego na Linuksie. Na początku męczyłem się z KGuitar, jednak nie moglem
sobie poradzić z kompilacją, a jedyne bianrki jakie udało mi się znaleźć nie
miały obsługi MIDI; nadal się uczę, tak więc muszę przesłuchać wszystko co
wpisałem, by się upewnić, że jest w porządku.

![Songwrite screenshot](/post/97/misc/songwrite-screen.png)

Widziałem Songwrite kilka razy, jednak były to tylko zrzuty ekranu dostępne na
stronie domowej programu; Songwrite bazuje na GTablature, jednak autor nie
docenił zmian API GNOME 2, przepisał GTablature z użyciem Tk oraz zmienił jego
nazwę na Songwrite. Jak dla mnie Tk jest brzydki i próbuje go unikać gdy to
możliwe, ale zostałem skuszony do wyprubowania Songwrite po zobaczeniu w jakim
stopniu program wydano w zeszłym roku.

## Tabulatura gitarowa

Tabulatura gitarowa to mieszanina gitarzystów rockowych; wielu z nich jest
samoukami lub też otrzymują pewne instrukcje, jednak nie portafią czytać muzyki.
Tablatura pokazuje wkład palców na strunach, co sprawia że piosenka jest
łatwiejsza do zrozumienia dla gitarzysty. Większość tabulatur dostępnych w
Internecie pokazuje palcowanie w postaci tekstu ASCII. Dla przykładu E minor
wygląda tak:

```
e|-----------------0--------|
B|--------------0-----0-----|
G|-----------0-----------0--|
D|--------2-----------------|
A|-----2--------------------|
E|--0-----------------------|
```

Aczkolwiek w ostatnich latach pojawiło się kilka programów, które pozwalają
średnio zaawansowanym gitarzystom na odtwarzanie piosenek na komputerze,
usuwając niejsności zwykłego tekstu, oraz spełniają zapotrzebowania gitarzystów,
którzy nie umieją czytąc nut, w celu stworzenie własnego CD. Było tylko kwestią
czasu zanim gitarzyści-użytkownicy Linuksa poczują własne potrzeby i stworzą
własne oprogramowanie.

![E-minor arpeggio](/post/97/misc/songwrite-screen2.png)

Songwrite to jeden z kilku programów tabowych dostępnych na platformę Linuksa i
mimo wczesnej wersji (używam wersji 0.12) jest dość znośny. Niektóre jego
możliwości:

* Import/export MIDI Drukowania (używa Lilypond) Import plików GuitarPro 3/4
* Wielościeżkowość Import/export ASCII Format plików XML Wsparcie dla Songbook
* (Śpiewnik) 

Aczkolwiek Songwrite posiada ograniczenia; może na przykład obsługiwać tylko
rytmy x/4 i x/8 (choć w praktyce nie jest to wielkim problemem), nie posiada
obsługi harmonicznej (ang. harmonic - przyp. tłum.) i choć obsługuje string
bends, nie wspomaga releasing the bend czy whammy bar type bends. Jednak
Songwrite jest w trakcie rozwijania i ważne nowe ficzery są dodawane w każdym
wydaniu, tak więc jest kwestią czasu zanim stanie się godnym przeciwnikiem dla
oprogramowania pod Windowsa; być może nawet go prześcignie - żadne z tych
programów nie ma obsługi niestandardowej harmonijności; ale interfejs Songwrite
oznacza, iż nie musi ono przejść jakiś wielkich badań aby dodać taką obsługę.

![Note properties](/post/97/misc/songwrite-screen4.png)

## Wsparcie GuitarPro

GuitarPro to najpopularniejsze oprogramowanie tabowe oparte na systemie Windows,
a kilka witryn takich jak MySongBook.com zostało skonfigurowanych tak aby ludzie
mogli wymieniać się swoimi ulubionymi tabami oraz z innymi programami
Windowsowymi obsługującymi ten format. Zatem obsługa formatu GuitarPro jest
bardzo ważna dla każdego, kto pragnie przejść z Windowsa lub uzyskać dostęp do
tysięcy dostępnych w tym formacie tabów.

Mając wcześniejsze próby z beta-wersją KGuitar, nie miałem większej nadziei na
importowanie formatu GuitarPro przez Songwrite. Jest z tym kilka problemów; nie
może obsługiwać niestandardowych note durations (choć obsługuje triplets),
rozpoznawanie powtórzonych bars i linked notes is hit and miss, nie rozpoznaje
zmian tempa i ma problemy z importowaniem skomplikowanych tabów. Jednak braki
niektórych z tych mozliwości nie są problemem, gdy dochodzi do importu plików
GuitarPro; chociaż posiada on ograniczoną obsługę niestandardowych podpisów
czasowych to możliwe jest bezproblemowe odtwarzanie prostych plików.

## Zaczynamy

Gdy po raz pierwszy uruchamiasz Songwrite, zobaczysz pusty ekran. Wprowadzanie
nut jest tak łatwe jak w innych programach; klikasz w linię reprezentującą daną
strunę i wprowadzasz numer progu w gitarze. Aby odtworzyć, poprostu wciśnij
spację.

Wprowadzanie akordów też jest łatwe; a jako bonus w stosunku do innych
programów, w Songwrite można kopiować i wklejać kombinacje nut i akordów zamiast
poszczególnych taktów. Poprostu zaznaczasz obszar, który chcesz skopiować, a
środkowym przyciskiem go wklejasz. Pozwala ci to również na wstawianie
fragmentów akordów do innych grup strun.

Inną unikalną cecha Songwrite jest zdolność do wprowadzania nut w każdym punkcie
taktu; w innych programach tabowych musiałbyś wprowadzić pauzy przed następna
nutą. Jest to pomocne, gdy transkrybujesz muzykę i nie jesteś pewien, który
akord lub nuta jest odtwarzana, albo gdy muzyka zaczyna się późniejszym takcie -
z tą możliwością, leadfills i harmonczność wokalna może być szybciej
wprowadzana.

![Selecting an area of music](/post/97/misc/songwrite-screen3.png)

## Rzeczy na które należy zwrócić uwage

Wklejając, kliknij na najwyższą strunę używaną w akordzie. Jeśli pragniesz
przykładowo wstawić akord G5 jako C5, to powinieneś kliknąć na strunę G. Jeśli
klikniesz na niższą, Songwrite doda dodatkowe struny do twojego progu.

```
e|-----------|
B|-----------|
G|--------5--|
D|--5-----5--|
A|--5-----3--|
E|--3--------|
    G5    C5
```

Jeśli próbujesz zrobić coś, czego nie obsługuje Songwrite, może wyczyścić część
twojego pliku. Miałem dodać dziwne rytmy do plików myśląc, że zmiana zostanie
poprostu odrzucona. Jeśli otrzymasz komunikat o błędzie, użyj funkcji Undo
(cofnij) i sprawdź czy cokolwiek się zmieniło.

## Współzawodnictwo

Songwrite ma na dzień dzisiejszy tylko dwóch poważnych konkurentów (choć
istnieje kilka programów pomagających w przygotowywaniu prostych tabów w ASCII)
- KGuitar oraz Gnometab. KGuitar jest prawdopodobnie najlepszym rozwiązaniem dla
    początkujących gitarzystów; do większości ostatnich wersji dodano możliwość
    klikania rytmu - nastepnie KGuitar próbuje wyliczyć note duration i time
    signature. Nie mogę co prawda potwierdzić czy to dobrze działa, ponieważ w
    dalszym ciagu nie udało mi się go skompilować. Ale w wersji którą
    wypróbowałem, moge powiedzeć że narzędzie do akordów jest cudowne.

Nie próbowałem Gnometab tak więc nie moge go zrecenzować; chociaż musze
przyznać, że posiada on najlepiej wyglądający interfejs spośród programów które
widziałem.

## Przykłady

Dla nieco bardziej zainsteresowanych czytelników, których interesuje używanie
Songwrite, załączyłem tarpaczkę plików GuitarPro. Kilka z nich nie można
zaimportować, ale mam zaufanie do deweloperów Songwrite więc załączam je do tego
artykułu jako "próbka przyszłości". Załączam też na razie pliki midi, więc
będziecie wiedzieli jak powinno to brzmieć.
[examples.tar.gz](/post/97/misc/examples.tar.gz) Odnośniki

* [Songwrite](https://web.archive.org/web/20040618232141/http://oomadness.tuxfamily.org/en/songwrite/)
* [MySongBook.com](https://web.archive.org/web/20040618232141/http://www.mysongbook.com/)
* [KGuitar](https://web.archive.org/web/20040618232141/http://kguitar.sourceforge.net/)
* [Gnometab](https://web.archive.org/web/20040618232141/http://www.solutionm.com/gnometab/gnometab.html)
