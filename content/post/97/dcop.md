---
title: Używanie DCOP z linii poleceń
date: 2021-10-15
---

* Autor: Jimmy O'Regan
* Tłumaczył: Marcin Dec
* Original text: https://linuxgazette.net/issue97/oregan2.html

---

## Wprowadzenie

DCOP to mechanizm IPC/RPC środowiska KDE. Jeśli zrozumiałeś to zdanie to możesz
spokojnie opuścić następny paragraf.

DCOP to skrót od Protokół Komunikacji Aplikacji Biurka (Desktop COmmunications
Protocol), IPC to skrót od Komunikacja Międzyprocesowa (Inter-Process
Communication); natomiast RPC to Zdalne Wywoływanie Procedur (Remote Procedure
Call).W jednym zdaniu, mechanizmy te służą do komunikacji między dwoma
programami, niezależnie od tego czy są uruchomione na tej samej maszynie czy
komunikują się przez sieć. W takim rozumieniu, DCOP jest podobny do technologii
OLE Automation Microsoftu: zapewnia deweloperom łatwy sposób dostępu do funkcji
dostępnych w danej aplikacji.

KDE miało być początkowo systemem komponentów opartym na CORBIE (GNOME był
początkowo takim projektem, ale stał się projektem desktopowym w wyniku
niezadowolenia z licencji na jakiej używana była biblioteka Qt), jednak
stwierdzono, że CORBA jest zbyt skomplikowana i stworzono dwa prostsze systemy;
KParts, dla osadzanych komponentów; oraz DCOP.

Aplikacje korzystające z DCOP w czasie uruchamiania rejestrują siebie oraz swoje
funkcje w demonie DCOP. Demon ten udostępnia, na żądanie, listę uruchomionych
procesów oraz funkcje przez nie udostępniane.Wykorzystuje to szereg aplikacji,
takie jak przeglądarka DCOP, czy też demon translacji z DCOP do XMLRPC; ale ten,
któremu chcę się przyjrzeć to 'dcop', który umożliwia dostęp do DCOP poprzez
powłokę.

W przeciwieństwie do innych systemów, takich jak Bonobo w GNOME lub Automation
Microsoftu, DCOP nie udostępnia mechanizmu uruchamiania; DCOP nasłuchuje tylko
na działające procesy. Uruchamianie aplikacji należy do użytkownika.

Artykuł ten opisuje jak używać DCOP, aby kontrolowac aplikacje KDE za pomocą
linii poleceń; na końcu strony umieszczone są odnośniki dla tych, którzy
chcieliby dokładniej poznać tę technologię.

## Używanie DCOP z linii poleceń

Dostęp z linii poleceń do systemu DCOP zapewnia program 'dcop'. Składnia jego
wywołania jest następująca:

`dcop [application] [object] [function] [arguments ...]`

Aby zobaczyć, które aplikacje są dostępne, uruchom 'dcop' bez argumentów

```
$ dcop
khelpcenter
kwin
kicker
kword-4354
kded
knotify
kio_uiserver
kalarmd
kcookiejar
konsole-4300
korgac
klauncher
kdesktop
klipper
ksmserver
```

Spójrzmy na klippera, usługę schowka dla KDE:

```
$ dcop klipper
qt
klipper
```

Możemy się domyślać, że obiekt którego tutaj szukamy to klipper; domyślny obiekt
(czyli ten, którym chcemy kontrolować aplikację) ma zazwyczaj tą samą nazwę jak
aplikacja, czasami z koncówką "Iface", lub jest zaznaczony jako "default":

```
$dcop klipper klipper
QCStringList interfaces()
QCStringList functions()
QString getClipboardContents()
void setClipboardContents(QString s)
void clearClipboardContents()
int newInstance()
void quitProcess()
```

Myślę, że najbardziej insteresującymi funkcjami są tutaj setClipboardContents
oraz getClipboardContents. Tak naprawdę to użyłem setClipboardContenst w okienku
konsoli aby dodać podane powyżej wyjście powłoki:

`dcop klipper klipper setClipboardContents "$(dcop klipper klipper)"`

Szczerze mówiąc, nie jest to najlepszy przykład, dużo szybciej można to osiągnąć
myszką niż wpisując to ręcznie, ale można ten przykład zrobić bardziej
użytecznym jeżeli użyjemy aliasu, np.

`alias klip="dcop klipper klipper setClipboardContents"`

Zawsze myślałem, że dostęp do "post-it notes" z powłoki byłby bardzo użyteczny -
DCOP to umożliwia.

```
$dcop knotes KNotesIface
QCStringList interfaces()
QCStringList functions()
int newNote(QString name,QString text)
int newNoteFromClipboard(QString name)
ASYNC showNote(int noteId)
ASYNC hideNote(int noteId)
ASYNC killNote(int noteId)
QMap notes()
ASYNC setName(int noteId,QString newName)
ASYNC setText(int noteId,QString newText)
QString text(int noteId)
ASYNC sync(QString app)
bool isNew(QString app,int noteId)
bool isModified(QString app,int noteId)
```

A zatem dodajmy notatkę::

`dcop knotes KNotesIface newNote "A note" "Stuff I want to keep track of"`

Mam nadzieje, że wszystko jest tu jasne, żółta notatka pojawi się na pulpicie.

Zobaczmy jakie mamy notatki:

```
$dcop knotes KNotesIface notes
1->A note
```

Po dodaniu jeszcze kilku bardziej bezsensownych notatek, mógłbym chcieć mieć je
w pliku tekstowym, a więc mogę napisać skrypt aby zrealizować ten głupawy
pomysł:

```sh
#!/bin/bash
n=$(dcop knotes KNotesIface notes|awk -F- '{print $1}'|tail -1)
dcop knotes KNotesIface notes > $1
echo >> $1
for ((i=1;i<=$n;i++));do echo -n "$i: "; dcop knotes KNotesIface text $i;echo;done >> $1
```

## Podsumowanie

DCOP to potężny mechanizm zarządzania aplikacjami KDE. Im więcej deweloperów
pozna jego użyteczność, tym szybciej osiągniemy miłe dla oka połączenie desktopu
ze skryptami.

## Odnośniki

* [Programowanie z użyciem DCOP](https://web.archive.org/web/20040618232122/http://developer.kde.org/documentation/library/kdeqt/dcop.%20html)
* [Rozdział z KDE 2.0 Development](https://web.archive.org/web/20040618232122/http://developer.kde.org/documentation/books/kde-2.0-development/ch13.html)
