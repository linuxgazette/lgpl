---
title: Linus nie jest autorem Linuksa?
date: 2021-10-15
---

* Autor: Jimmy O'Regan
* Tłumaczył: Damian Rafałowski
* Original text: https://linuxgazette.net/103/oregan2.html

---

Firma [Alexis de Tocqueville Institution](https://web.archive.org/web/20040618232103/http://www.adti.net/)
opublikowała raport bazujący na podstawie mającej się ukazać książki Kena
Browna, pt. *"Samizdat: And Other Issues Regarding the 'Source' Of Open Source
Code"*, w której zakwestionowano fakt, że Linus Torvalds stworzył Linuksa.

Linus w wywiadzie dla
[LinuxWorld](https://web.archive.org/web/20040618232103/http://www.linuxworld.com/story/44851.htm)
z charakterystycznym dla siebie poczuciem humoru odpowiedział: "OK, przyznaję.
Byłem po prostu podstawionym gościem, prawdziwymi twórcami Linuksa byli Zębowa
Księżniczka oraz Święty Mikołaj". Dodał również, że będzie musiał rozważyć
powrót do wybranej profesji: "Badanie nad dobieraniem się w pary traszki
zwyczajnej".

W opublikowanym
[artykule](https://web.archive.org/web/20040618232103/http://news.yahoo.com/news?tmpl=story&u=/040514/234/71e7q.html)
autor próbuje dowieść, iż Linus nie jest autorem Linuksa; występuje zdanie
"Sprawozdanie Browna powstało na podstawie szczegółowych wywiadów z więcej niż
dwoma tuzinami ekspertów, takich jak Richard Stallman, Dennis Ritchie i Andrew
Tanenbaum". Jednak na Newsforge pojawił się
[artykuł](https://web.archive.org/web/20040618232103/http://trends.newsforge.com/trends/04/05/24/2145237.shtml?tid=2&tid=82&tid=94),
w którym stwierdzono że: "Większa cześć źródeł Browna to strony prywatne ludzi,
których nie uważa się za ekspertów w świecie Uniksa, Linuksa, GNU lub innych
powiązanych ze sprawą tematów, jednakże na stronach tych mówi się generalnie o
historii Uniksa, a cytaty w sposób rażący wybrane są z kontekstu w wywiadach,
których Brown nie przeprowadzał ani nie brał w nich udziału.

Ken Brow bezpośrednio przeprowadził wywiad z Andrew Tanenbaumem. Jak tylko
raport ujrzał światło dzienne, Tanenbaum napisał do
[Slashdot](https://web.archive.org/web/20040618232103/http://developers.slashdot.org/article.pl?sid=04/05/20/1240256&tid=106)
aby udostępnił
[link](https://web.archive.org/web/20040618232103/http://www.cs.vu.nl/~ast/brown/)
z jego prywatnej strony, w którym w szczegółowy sposób opisuje sprawę związaną z
wywiadem.

Na stronie tej, Tanenbaum mówi, że bardzo szybko zaczął być podejrzliwy co do
Browna i jego motywów. Brown, mówi Tanenbaum, był wymijający w odpowiedziach o
przesłanki jakie kierowały nim aby przeprowadzić wywiad, nie chciał wyjawić kto
jest fundatorem (Wired
[spekulował](https://web.archive.org/web/20040618232103/http://www.wired.com/news/linux/0,1411,52973,00.html),
że jednym z głównych sponsorów AdTI jest Microsoft). Tanenbaum przekonał się
również, że Brown nic nie wie o historii Uniksa. Później w wywiadzie, Brown
przedstawił przyczyny, które spowodowały jego obecność, zadając pytania takie
jak: "Czy on nie wykorzystał fragmentów systemu MINIX bez zezwolenia?". Chociaż
Tanenbaum próbował wytłumaczyć rzeczywisty wpływ Miniksa na Linuksa, artykuł na
Newsforge mówi, że wiele raportów bazuje na twierdzeniu, że Linux zawiera
skradziony kod Miniksa.

W późniejszym czasie Tanenbaum udostępnił [inną stronę](https://web.archive.org/web/20040618232103/http://www.cs.vu.nl/~ast/brown/codecomparison/),
na której znajdują się rezultaty porównania kodu Miniksa i wczesnych wersji
Linuksa, przeprowadzonego przez Alexeya Toptygina na zlecenie Browna. Rezultaty
są oczywiste: W kodzie znajdują się jedynie cztery podobne bloki, jeden z nich
bazuje na standardzie ANSI C, drugi na standardzie POSIX, a ostatnie dwa w
kodzie odpowiedzialnym za dostęp do systemu plików miniksa - fragment kodu,
który musi być podobny aby pracował prawidłowo.

Poprzednie raporty będące dziełem AdTI były bardzo podobne w wrogości jaką w
nich prezentowano do ruchu open source. W raporcie stworzonego również przez
Kena Browna, pt. [Outsourcing and the global IP "devaluation"](https://web.archive.org/web/20040618232103/http://www.adti.net/outop.htm)
znajdujemy wiele mówiącą uwagę "Wolny kod źródłowy, również opisywany jako
darmowy kod źródłowy, jest bombą neutronową dla IP". W 2002 roku ZDnet
publikując
[artykuł](https://web.archive.org/web/20040618232103/http://zdnet.com.com/2100-1104-929669.html)
o innym raporcie AdTI, pt. "Opening the Open Source Debate" (przyp. tłum.
"Początek debaty o otwartym kodzie") zacytował zdanie z raportu "Terroryści
próbujący włamywać się lub zakłócać komputery znajdujące się w U.S. mogą mieć
ułatwione zadanie jeśli, jak proponują niektóre grupy, próby rządu federalnego
na przestawienie się na oprogramowanie typu open source dojdą do skutku.

Ten świeży zarzut, będący tematem artykułu, wystosowany przez SCO, spowodował,
że Linus wprowadził pewne [nowe wytyczne](https://web.archive.org/web/20040618232103/http://www.newsfactor.com/story.xhtml?story_title=Torvalds-Sets-New-Rules-for-Linux-Contributors&story_id=24181&category=entcmpt),
powodujące akceptację kodu do projektu: Certyfikat pochodzenia, który wymaga aby
każdy współtworzący kod, oświadczył iż jest autorem wprowadzanego do projektu
kodu.
