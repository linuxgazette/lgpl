# Linux Gazette PL

---

[Archiwum](content/page/archive.md) | [Autorzy](content/page/authors.md)

---

Początki projektu tłumaczenia internetowego miesięcznika Linux Gazette
siegają roku 2003. W pierwszych latach tłumaczenia powstawały w ramach podstron
serwisu Linux.pl. Głownym założeniem było dostarczenie kompletnych tłumaczeń
poszczegolnych wydań gazety. Z biegiem czasu okazało się, że trudno jest
osiągnąć taki stan rzeczy, wiec zdecydowano się na tłumaczenie w pierwszej
kolejności artykułów tematycznych, a w dalszej kolejności kolumn stałych.

Stare tłumaczenia zaginęły, jednak aby uszanować ilość pracy jaką wielu moich
kolegów wniosło to tego projektu, zamieszczam tutaj to, co udało mi się odzyskać
z archive.org.

"The Linux Gazette... making Linux just a little more fun!"

