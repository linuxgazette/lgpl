#!/bin/sh
# Converts linuxgazette html pages to md

if [ -z "$1" ]; then echo "Missing file as an argument"; exit 0; fi

filename=$1
name="${filename%.*}"

pandoc "${filename}" -f html -t gfm -o "${name}".md

#rm -rf "${filename}"
#rm -rf ${name}.tmp
