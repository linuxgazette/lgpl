
# 2-Cent Tips


### 2-Cent Tip: resizing partition together with its filesystem using parted

****

Mulyadi Santosa \[mulyadi.santosa at gmail.com\]

  
**Wed, 17 Nov 2010 22:45:19 +0700**

Possibly, there are many tools out there that might do the same thing.
However, I find parted relatively easy to use when dealing with
partition resizing. The command is "resize".

AFAIK, it only resize VFAT, ext2 and ext3. Other filesystem might need
external tools, but I haven't checked it thoroughly.

\-- regards,

Mulyadi Santosa Freelance Linux trainer and consultant

blog: the-hydra.blogspot.com training: mulyaditraining.blogspot.com

-----


### 2-Cent Tip: Making space while installing

****

Ben Okopnik \[ben at linuxgazette.net\]

  
**Tue, 30 Nov 2010 18:30:23 -0500**

    Date: Sun, 28 Nov 2010 11:22:04 +0100
    From: Jacopo Sadoleto <jacopo.sadoleto@gmail.com>
    To: TAG <tag@lists.linuxgazette.net>
    To: ben at linuxgazette.net
    Subject: 2-Cent Tip

Ben I would like to submit a tip, it it fails miserably off the mark,
please feel free to bin-it...

Sometimes upgrading an Linux distro to a recent version, yields an "No
space left on device" (usually /var); For those not willing to use a
tool to create, resize, grow a partition, a simple artifice will do the
trick:

    CTRL-ALT-F2
    # mv /var /home/user/? <== or wherever space suffice
    # cd /
    # ls -s /home/user/var .
    CTRL-ALT-F7


-----
