
# XKCD

**By [Randall Munroe](../authors/munroe.html)**


[![\[cartoon\]](misc/xkcd/guest_week_bill_amend_foxtrot.png
"Guest comic by Bill Amend of FoxTrot, an inspiration to all us nerdy-physics-majors-turned-cartoonists, of which there are an oddly large number.
")  
Click here to see the full-sized
image](misc/xkcd/guest_week_bill_amend_foxtrot.png)


More XKCD cartoons can be found [here](http://xkcd.com).
