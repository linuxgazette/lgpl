# Mailbag

### This month's answers created by:

**\[ Anderson Silva, Amit Kumar Saha, Ben Okopnik, S. Parthasarathy,
Karl-Heinz Herrmann, René Pfeiffer, Mulyadi Santosa, Neil Youngman
\]**  
...and you, our readers\!  

-----

# Our Mailbag

-----

### short: gnash works.

****

Ben Okopnik \[ben at linuxgazette.net\]

  
**Tue, 9 Nov 2010 14:23:47 -0500**

\----- Forwarded message from "s. keeling" \<keeling at nucleus.com\>
-----

Hey B. I looked into the "Flash Problems" link. Useful information,
thanks.

\[I'm back using my sandbox machine as possible kernel bug is burning up
my AMD 64 HP Pavilion. Running Sidux--\>aptosid on a Compaq Evo
PIV-2.4GHz\] This works:

    i   browser-plugin-gnash                 - GNU
    Shockwave Flash (SWF) player - Plugin for 
    i   gnash                                - GNU
    Shockwave Flash (SWF) player              
    i A gnash-common                         - GNU
    Shockwave Flash (SWF) player - Common file

I've no trouble reading LG, including cartoons. Thanks for helping me
figure this out.

It does tend to spiral out of control on bad flash (WaPo), but it's more
usable than Adobe's has been (MB-3 menu actually gives the user
control\!\!\! Woohoo\!). ![:-)](../gx/smile.png)

\-- Any technology distinguishable from magic is insufficiently
advanced. - -

\----- End forwarded message -----

-----


### IPTables + Squid problem

****

Deividson Okopnik \[deivid.okop at gmail.com\]

  
**Tue, 9 Nov 2010 12:43:04 -0300**

Hello everyone

My old internet server crashed and i had to setup a new one, and im
having several problems.

My squid configuration is pretty simple, I basically allow everything:

    Allow manager localhost     
    Deny manager    
    Allow purge localhost   
    Deny purge  
    Allow localhost     
    Allow all

And my iptables config file is empty (exists but is empty -
/etc/network/iptables)

And msn doesnt work on the machines behind the proxy, and noone can
atach files to emails - im guessing no ports are being forwarded (cause
of the empty iptables file) but if I put content in it, eg.:

    #!/bin/bash
    iptables -F

and do an sudo iptables-apply, it gives me an "line 2 failed - failed" -
what am I doing wrong? btw im on ubuntu 10.04, and allowing any outgoing
connection to any port would be good enough, later Ill think about the
security.

**\[ <span id="mb-iptables_squid_problem"></span> [Thread continues here
(4 messages/8.47kB)](misc/lg/iptables_squid_problem.html) \]**

-----


### Unlogo - adblock for video.

****

Jimmy O'Regan \[joregan at gmail.com\]

  
**Mon, 11 Oct 2010 14:13:52 +0100**

"Unlogo is an FFMPEG AVFilter that attempts to block out corporate logos
using OpenCV 2.1 and an awesome 'plugin' framework for FFMPEG developed
by Michael Droese." <http://unlogo.org/pages/about> Code:
<http://code.google.com/p/unlogo/>

    -- 
    <Leftmost> jimregan, that's because deep inside you, you are evil.
    <Leftmost> Also not-so-deep inside you.

-----


### \[hardabud@romandie.com: Linux Comic\]

****

Ben Okopnik \[ben at linuxgazette.net\]

  
**Tue, 5 Oct 2010 11:42:35 -0400**

\----- Forwarded message from H?rda Bud \<hardabud at romandie.com\>
-----

    Date: Mon, 04 Oct 2010 10:25:47 +0200
    From: H?rda Bud <hardabud@romandie.com>
    To: TAG <tag@lists.linuxgazette.net>
    To: editor at linuxgazette.net
    Subject: Linux Comic

Hello,

A comic you might be intrested in: "Jolly Roger - a history about
copyright in cyberspace"

From the historical background of "intellectual property" to ACTA,
through Free Software, Napster and much more. Told in a simple and funny
way, this comic (under Creative Commons) is available in downloadable
CBZ and EPUB formats or to be read directly on the site:
<http://www.hardabud.com/jollyroger/>

Hope you enjoy it Best regards,

Anders hardabud.com

\----- End forwarded message -----

    -- 
    * Ben Okopnik * Editor-in-Chief, Linux Gazette * http://LinuxGazette.NET *

-----


### Cameron Sinclair on open-source architecture

****

Ben Okopnik \[ben at linuxgazette.net\]

  
**Wed, 24 Nov 2010 09:39:18 -0500**

Another stunning TED presentation (I have to ration myself on those; I
can only stand so many meteor impacts on my heart and soul in a week.)
This one is about improving global living standards, and open source as
an inevitably required part of it.

<http://www.ted.com/talks/cameron_sinclair_on_open_source_architecture.html>

    -- 
    * Ben Okopnik * Editor-in-Chief, Linux Gazette * http://LinuxGazette.NET *

-----


### An ssh puzzle

****

Neil Youngman \[ny at youngman.org.uk\]

  
**Fri, 12 Nov 2010 15:57:20 +0000**

I have a glitch that's stumped both me and my sysadmin.

I wanted to know which hosts a particular file that needed updating was
on and I thought this simple loop would answer that question.

```
$ for n in \`seq 1 25\`; do ssh host$n ls -l /path/to/file; done
```

This worked for the first 4 hosts, ten it got to the 5th host where it
displayed the banner and hung. Trying it directly as a simple command;

```
$ ssh host5 ls -l /path/to/file
```

also hung after displaying the banner.

```
$ ssh host5
```

allowed me to log in normally and

```
$ ls -l /path/to/file
```

showed that the file did not exist on host5.

Next I tried

```
$ ssh echo foo
```

which worked normally, so I tried

```
$ ls -l /bin/sh
```

which also worked normally.

```
$ ssh host5 strace ls -l /path/to/file
```

didn't hang either and I couldn't see anything that struck me as odd in
the output

```
$ ssh 'host5 ls -l /path/to/file 2\> /dev/null'
```

did not hand ang modifying it to

```
$ ssh 'host5 ls -l /path/to/file 2\>&1'
```

allowed me to see the error message.

The problem seems to be related in some way to the handling of standard
error, but I can't think of anything that would cause it to behave like
that. I have a workaround that gets me the information I need, but I
would like to resolve the underlying problem. Do any of the gang have
any idea what could cause this sort of behaviour?

Neil Youngman

**\[ <span id="mb-an_ssh_puzzle"></span> [Thread continues here (2
messages/2.73kB)](misc/lg/an_ssh_puzzle.html) \]**

-----


### GPG public key

****

Dr. Parthasarathy S \[drpartha at gmail.com\]

  
**Sat, 20 Nov 2010 09:14:07 +0530**

I have a question about importing public keys using GPG/Kgpg (under
Linux). I get a public key file (it is an ascii text file usually). I
would like to check the fingerprint of the key, \*\*without importing
the key into my keyring\*\*. Is there a way out using GPG/Kgpg (under
Linux) ? Someone give me a clue please.

Many thanks,

partha

    -- 
    ---------------------------------------------------------------------------------------------
    Dr. S. Parthasarathy                    |   mailto:drpartha at gmail.com
    Algologic Research & Solutions    |
    78 Sancharpuri Colony                 |
    Bowenpally  P.O                          |   Phone: + 91 - 40 - 2775 1650
    Secunderabad 500 011 - INDIA     |
    WWW-URL: http://algolog.tripod.com/nupartha.htm
    GPG Public key :: http://algolog.tripod.com/publikey.htm
    ---------------------------------------------------------------------------------------------

-----


### Talkback:180/grebler.html

****

Neil Youngman \[ny at youngman.org.uk\]

  
**Tue, 2 Nov 2010 15:02:41 +0000**

Some time ago I came across PDFTK, but I've never really had a use for
it. it sounds as though it should do what you need, but a quick scan of
the man page doesn't tell me whether it honours restrictions in the PDF.

<http://www.pdflabs.com/tools/pdftk-the-pdf-toolkit/>

Neil Youngman

**\[ <span id="mb-talkback_180_grebler"></span> [Thread continues here
(2 messages/2.49kB)](misc/lg/talkback_180_grebler.html) \]**

-----


### This is just.. sick :)

****

Ben Okopnik \[ben at linuxgazette.net\]

  
**Wed, 17 Nov 2010 23:06:33 -0500**

<http://trouble-maker.sourceforge.net/>

App randomly breaks your system so you can learn how to fix it. Um,
billed as "not for production systems".

    -- 
    * Ben Okopnik * Editor-in-Chief, Linux Gazette * http://LinuxGazette.NET *

**\[ <span id="mb-this_is_just_sick"></span> [Thread continues here (3
messages/2.30kB)](misc/lg/this_is_just_sick.html) \]**

-----


### Google: Making Websites Run Faster by 2x

****

Ben Okopnik \[ben at linuxgazette.net\]

  
**Wed, 3 Nov 2010 15:05:09 -0400**

Rather interesting new bit from Google: on-the-fly page/site
optimization. Wonder what the magic trick is... I'll have to look into
it as soon as I have some Spare Time. \[hollow laugh\]

\----- Forwarded message from Diana Wilson \<dianawilson at google.com\>
-----

    Date: Wed, 3 Nov 2010 09:03:53 -0700
    From: Diana Wilson <dianawilson@google.com>
    To: TAG <tag@lists.linuxgazette.net>
    To: undisclosed-recipients: ;
    Subject: Developer News: Making Websites Run Faster by 2x

Hi, We just?launched a new open-source Apache module called
mod\_pagespeed that any webmasters can use to quickly and automatically
optimize their sites. (It's like Page Speed, but makes the changes
automatically.) It can reduce page load times by up to 50% -- in other
words, essentially speeding up websites by roughly 2x. This is one of
our more significant "speeding up the web" contributions.?

Webmasters know that if it's not fast, it doesn't matter how pretty it
is -- people won't use it. At Google, we're obsessed with speed--we
measure it, pick it apart, think about it constantly. It's even baked
into our quarterly goals. Mod\_pagespeed comes on the heels of a bunch
of other speed pushes we've already told you about, from Google Instant
to the Images redesign to SPDY to Fast Flip to ultra-high-speed
broadband... etc.

Let me know if you have any questions or want a deep dive briefing.
Regards, Di -- Diana Wilson Developer Global Communications and Public
Affairs Manager direct: 650-253-6280 cell: 916-768-2202 dianawilson at
google.com ----- End forwarded message -----

    -- 
    * Ben Okopnik * Editor-in-Chief, Linux Gazette * http://LinuxGazette.NET *

**\[ <span id="mb-google__making_websites_run_faster_by_2x"></span>
[Thread continues here (3
messages/4.15kB)](misc/lg/google__making_websites_run_faster_by_2x.html)
\]**

-----


### CGI (no, the other kind of CGI.)

****

Ben Okopnik \[ben at linuxgazette.net\]

  
**Wed, 17 Nov 2010 15:59:55 -0500**

Just gotta share this with you folks:

[http://gizmodo.com/5691747/prepare-for-t\[...\]-meltingly-realistic-cgi-youve-ever-seen](http://gizmodo.com/5691747/prepare-for-the-most-mind-meltingly-realistic-cgi-youve-ever-seen)

100% computer-generated.

We now live in The Matrix. ![:)](../gx/smile.png)

    -- 
    * Ben Okopnik * Editor-in-Chief, Linux Gazette * http://LinuxGazette.NET *

**\[ <span id="mb-cgi_no_the_other_kind_of_cgi"></span> [Thread
continues here (3
messages/2.79kB)](misc/lg/cgi_no_the_other_kind_of_cgi.html) \]**

-----


### Accented characters

****

Dr. Parthasarathy S \[drpartha at gmail.com\]

  
**Tue, 16 Nov 2010 09:46:12 +0530**

I do send and receive mails frequently in French. I use Thunderbird, or
Google mail web interface.

How do I add diacritical marks (accent marks) in the text ?

Can someone guide me please ? I find both scim and ibus clumsy. I was
thinking of something simpler like a plugin for gedit or nedit where we
can just go ahead and type. Since French has very few characters with
accent marks and only about a handful of accent marks, I guess it would
be much easier that way. Or is there an easier way ? I remember, long
ago, when I was a student, I used to escape and type the Hex value of
the ASCII code of these characters.

partha

PS: Why do people become so lazy as they grow up ? ;-)

    -- 
    ---------------------------------------------------------------------------------------------
    Dr. S. Parthasarathy                    |   mailto:drpartha at gmail.com
    Algologic Research & Solutions    |
    78 Sancharpuri Colony                 |
    Bowenpally  P.O                          |   Phone: + 91 - 40 - 2775 1650
    Secunderabad 500 011 - INDIA     |
    WWW-URL: http://algolog.tripod.com/nupartha.htm
    GPG Public key :: http://algolog.tripod.com/publikey.htm
    ---------------------------------------------------------------------------------------------

**\[ <span id="mb-accented_characters"></span> [Thread continues here
(10 messages/13.17kB)](misc/lg/accented_characters.html) \]**

-----


### Poetic licence -- BSD licence, in verse

****

Jimmy O'Regan \[joregan at gmail.com\]

  
**Fri, 8 Oct 2010 15:18:34 +0100**

<http://www.gerv.net/writings/poetic-licence/bsd.html>

    -- 
    <Leftmost> jimregan, that's because deep inside you, you are evil.
    <Leftmost> Also not-so-deep inside you.
