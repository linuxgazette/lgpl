# Almost-but-not-quite Linux...

**By [Ben Okopnik](../authors/okopnik.html)**

Almost-but-not-quite-Linux images from the streets. Thanks, folks - and
please [keep them coming](mailto:editor@linuxgazette.net)\!

  

![Um... Free gas? REALLY? Vive la
France\!](misc/okopnik/gpl_in_france_-_haute_savoie_province.jpg)  
<span class="small">(Sent in by Anton Borisov)</span>  
"Yes, it's Free. No, it's not
[free](http://en.wikipedia.org/wiki/Gratis_versus_Libre). Yes, we're
still working on both the mass replication and the WiFi-based
"straight-into-the-tank" delivery..."

![Linux - Food for Thought](misc/okopnik/yum.jpg)  
<span class="small">(Sent in by Anderson Silva)</span>  

    yum install ale fruit nut cream halibut beef kaffeine sabayon squid java vinagre coriander boxes baskets ...

"Don't mind me - I'm just restocking the deli shelves."

![Retrench or die](misc/okopnik/unisys.jpg)  
"Years ago, we used to be a big and powerful company, but now we're
downsizing like everyone else..."

  
