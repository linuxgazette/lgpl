# News Bytes

**By [Deividson Luiz Okopnik](../authors/dokopnik.html) and [Howard
Dyckoff](../authors/dyckoff.html)**

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><img src="../gx/bytes.gif" alt="News Bytes" /></td>
<td><h3 id="contents">Contents:</h3>
<ul>
<li><a href="lg_bytes.html#general">News in General</a></li>
<li><a href="lg_bytes.html#Events">Conferences and Events</a></li>
<li><a href="lg_bytes.html#distro">Distro News</a></li>
<li><a href="lg_bytes.html#commercial">Software and Product News</a></li>
</ul></td>
</tr>
</tbody>
</table>

**Selected and Edited by [Deividson
Okopnik](mailto:bytes@linuxgazette.net)**

Please submit your News Bytes items in **plain text**; other formats may
be rejected without reading. \[You have been warned\!\] A one- or
two-paragraph summary plus a URL has a much higher chance of being
published than an entire press release. Submit items to
<bytes@linuxgazette.net>. Deividson can also be reached via
[twitter](http://www.twitter.com/deivid_okop).

-----

## News in General

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)The Yocto Project: New Embedded Linux Workgroup

The Linux Foundation in October announced it is expanding its technical
work in the embedded space with the continuation of existing CELF work
as well as the formation of another new workgroup focused on the
embedded space, the Yocto Project.

The Yocto Project provides open source, high quality tools to help
companies make custom Linux-based systems for embedded products,
regardless of hardware architecture. The open source Yocto Project
brings together the elements needed to make the normally difficult
embedded Linux development process easier.

The Yocto Project is launching its version 0.9 with initial versions of
common build tools.

Participation in the workgroup is completely open and anyone can join
the development effort. The Linux Foundation invites contributors and
encourages developers and others to participate at the project website:
<http://www.yoctoproject.org>.

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)Linux Foundation and Consumer Electronics Linux Forum to Merge

The Linux Foundation and the Consumer Electronics Linux Forum (CELF), a
nonprofit international open source software development community
focused on embedded Linux, have announced they will merge organizations,
resulting in the CE Linux Forum becoming a technical workgroup at the
Linux Foundation. As part of this merge, the Linux Foundation will
expand its technical programs in the embedded computing space.

The use of Linux in embedded products has skyrocketed in recent years,
with Linux now being used in consumer electronic devices of all kinds.
CELF and the Linux Foundation believe that by combining resources they
can more efficiently enable the adoption of Linux in the Consumer
Electronics (CE) industry. Given the broad overlap in members between
the Linux Foundation and CELF, the similarity in the goals of both
organizations, and the large increase of embedded participants coming to
Linux in recent years, this aligning of resources will strengthen each
organization and ultimately help the organizations' members achieve
their missions: growing the embedded Linux market.

"CELF and the Linux Foundation have co-located technical events in the
spring over the last couple of years to exchange technical information.
We have noticed an increasing number of technical areas that both
organizations are interested in," said Nobuhiro Asai, chair of board of
directors of the Consumer Electronics Linux Forum. "This merger is a
natural transition to accelerate the use of Linux in consumer
electronics and strengthen the involvement of CE-related companies
within the Linux developer community."

CELF will become an official workgroup of the Linux Foundation. Members
of CELF who are not already Linux Foundation members can be
grandfathered into the organization at the Silver membership level.
Organized to promote the use of Linux in digital CE products, CELF was
established in 2003 by eight major consumer electronics companies:
Panasonic Corporation (Matsushita Electric Industrial Co., Ltd.), Sony
Corporation, Hitachi, Ltd., NEC Corporation, Royal Philips Electronics,
Samsung Electronics Co., Ltd., Sharp Corporation, and Toshiba
Corporation.

The Linux Foundation will take over the management of CELF's technical
events and web infrastructure.

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)VMware Unveils Strategy for Next Era of IT

At the conclusion of its seventh annual VMworld conference in San
Francisco, VMware outlined its vision for IT as a Service as the next
big thing and demonstrated new virtualization and cloud computing
technologies (see product details in the Product section.)

Paul Maritz, President and CEO of VMware, told the 17,000 attendees at
VMworld 2010 that a tipping point had been reached in 2009 when the
number of VMs exceeded the number of physical hosts. This means that
modern OSes no longer control the hardware and that the new universal
HAL is the virtualization hypervisor. "This layer is the new IT
infrastructure," Maritz said; "we think this is the new stack that is
emerging for the cloud era."

Key highlights made during the event include:

  - VMware announced a broad strategy and a set of new products to help
    businesses and governments move beyond "IT as a Cost Center" to a
    more business-centric "IT as a Service" model.
  - VMware announced VMware vCloud Director to enable a new model for
    delivering and consuming IT services across hybrid clouds, the
    VMware vShield product family to tackle cloud security challenges,
    and VMware vCloud Datacenter Services designed to help customers
    build secure, interoperable enterprise-class clouds at leading
    service providers.
  - VMware announced its cloud application platform, VMware vFabric, an
    application development framework and platform of services to
    optimize the use of cloud infrastructure.
  - VMware announced VMware View 4.5, its solution for delivering
    virtual desktops as a managed service. The new View can help
    organizations evolve their legacy desktop computing environments to
    an application and data delivery model.
  - VMware announced that it had acquired TriCipher, a leader in secure
    access management and enterprise identity federation for cloud
    hosted Software as a Service (SaaS) applications.

For the first time, the VMworld labs were powered by cloud technology
based on VMware vSphere with distributed data centers located in San
Francisco, California; Ashburn, Virginia; and Miami, Florida. More than
15,300 labs in total and 145,000 virtual machines were deployed during
the conference. This is over 4,000 virtual machines deployed and
un-deployed every hour.

More information from VMworld 2010, visit:
<http://www.vmworld.com/community/buzz/>.

Keynote replays are available at:
<http://www.vmworld.com/community/conferences/2010/generalsessions/>.

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)Next-Generation Storage Can Double Analytics Processing Speed

At the Supercomputing 2010 conference in New Orleans, IBM unveiled
details of a new storage architecture design that will convert terabytes
of pure information into actionable insights twice as fast as previously
possible. Ideally suited for cloud computing applications and
data-intensive workloads such as digital media, data mining and
financial analytics, this new architecture can shave hours off of
complex computations.

"Businesses are literally running into walls, unable to keep up with the
vast amounts of data generated on a daily basis," said Prasenjit Sarkar,
Master Inventor, Storage Analytics and Resiliency, IBM Research,
Almaden. "We constantly research and develop the industry's most
advanced storage technologies to solve the world's biggest data
problems. This new way of storage partitioning is another step forward
on this path as it gives businesses faster time-to-insight without
concern for traditional storage limitations."

Created at IBM Research, Almaden, the new General Parallel File
System-Shared Nothing Cluster (GPFS-SNC) architecture is designed to
provide higher availability through advanced clustering technologies,
dynamic file system management and advanced data replication techniques.
By "sharing nothing," new levels of availability, performance and
scaling are achievable. GPFS-SNC is a distributed computing architecture
in which each node is self-sufficient; tasks are then divided up between
these independent computers and no one waits on the other.

For instance, large financial institutions run complex algorithms to
analyze risk based on petabytes of data. With billions of files spread
across multiple computing platforms and stored across the world, these
mission-critical calculations require significant IT resource and cost
because of their complexity. Using a GPFS-SNC design provides a common
file system and namespace across disparate computing platforms.

For more information about IBM Research, please visit
<http://www.ibm.com/research>.

-----


## Conferences and Events

  - **DreamForce 2010 / SalesForce Expo**  
    December 6-9, Moscone Center, San Francisco, CA  
    <http://www.salesforce.com/dreamforce/DF10/home/>.

<!-- end list -->

  - **CARTES & IDentification 2010**  
    December 7-9, Nord Centre, Paris, France  
    <http://www.cartes.com/ExposiumCms/do/exhibition/>.

<!-- end list -->

  - **MacWorld Expo and Conference**  
    January 25-29, Moscone Center, San Francisco, CA  
    <http://www.macworldexpo.com/>.

<!-- end list -->

  - **Enterprise Connect (formerly VoiceCon)**  
    February 28-Mar 3, 2011, Orlando, FL  
    <http://www.enterpriseconnect.com/orlando/?_mc=CNZMVR07>.

<!-- end list -->

  - **FAST '11 USENIX Conference on File and Storage Technologies**  
    Sponsored by USENIX in cooperation with ACM SIGOPS  
    February 15-18, 2011, San Jose, CA  
    <http://www.usenix.org/events/fast11/>.

<!-- end list -->

  - **NSDI '11 USENIX Symposium on Networked Systems Design and
    Implementation**  
    Sponsored by USENIX with ACM SIGCOMM and SIGOPS  
    March 30-April 1, 2011, Boston, MA  
    <http://www.usenix.org/events/nsdi11/>.

-----


## Distro News

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)Ultimate Edition 2.8

Ultimate Edition 2.8, the Ubuntu based gamers' Linux Distro, was
released, featuring many improvements. 27 games come pre-installed in
this version, including:

  - Urbanterror;
  - Armagetronad;
  - Gunroar;
  - Hedgewars;
  - Kobo Deluxe;
  - Pingus;
  - etc.

A new repository filled to the brim with games has also been pre-added
to allow the end user to install additional games if desired.

More information can be found here:
<http://ultimateedition.info/ultimate-edition/ultimate-edition-2-8/>.

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)openSUSE Medical Team Releases Stable Version 0.0.6

openSUSE Medical is a distribution for medical professionals such as
doctors and clinical researchers. This is a stable release, and is
available for download at
<http://susegallery.com/a/NETBqB/opensuse-medicalos11332bitkde4>.

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)VortexBox 1.6 released

VortexBox is a distro that aims at easy creation of music-servers (or
jukeboxes).

Now with Fedora 14, 4K sector driver support and support for USB 2 and
192/24 USB DACs are available, making VortexBox an easy to use auto
CD/NAS ripping solution.

More information here:
<http://vortexbox.org/2010/11/vortexbox-1-6-released/>.

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)BackTrack 4 R2 "Nemesis"

Offensive Security has announced the release of BackTrack 4, featuring a
new kernel, better desktop responsiveness, improved hardware support,
broader wireless card support, a streamlined work environment, and USB
3.0 support.

More information can be found on the Release Page
(<http://www.backtrack-linux.org/backtrack/backtrack-4-r2-download/>) or
in the BackTrackWiki (<http://www.backtrack-linux.org/wiki/>).


## Software and Product News

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)VMworld Conferences Strengthen End User Computing Strategy

In Copenhagen, at last month's VMworld 2010 Europe, VMware advanced its
vision of end user computing with new products to help organizations
evolve their legacy desktop computing environments to a user-centric
application and data delivery model.

Introduced at VMworld 2010 in San Francisco, IT as a Service is the
transformation of IT to a more business-centric approach, focusing on
outcomes such as operational efficiency, competitiveness and rapid
response. This means IT shifts from producing IT services to optimizing
production and consumption of those services in ways consistent with
business requirements, changing the role of IT from a cost center to a
center of strategic value.

"Desktop computing is rapidly moving to a world focused on connecting
end users to critical data and applications they need via any device at
the edge of the cloud," said Vittorio Viarengo, vice president, End User
Computing products, VMware. "Enterprises today are seeking a path
forward from a decades-old PC-centric architecture to a model that
enables users to be more productive while more effectively managing the
growing portfolio of devices, data and applications. "

The VMware End User Computing strategy delivers solutions for
enterprises to evolve their PC-centric environments to a cloud computing
architecture to deliver applications and data to end users. Announced in
early September at VMworld 2010 in San Francisco, VMware View 4.5,
VMware ThinApp 4.6, VMware vShield Endpoint, Zimbra Appliance and
VMware's Desktop Infrastructure Service provide a new way of addressing
two fundamental client computing challenges: enabling secure data access
to a mobile workforce and managing the diversity of data, applications
and client devices needed by businesses.

Also in early September, VMware previewed and demonstrated Project
Horizon, a cloud-based management service that securely extends
enterprise identities into the cloud to provide new methods for
provisioning and managing applications and data based on the user, not
the device or underlying operating system.

VMware continues to build on this model by introducing VMware View 4.5,
a complete virtual desktop solution that can enable enterprises to
improve security, lower operating costs, and simplify desktop
administration by establishing an architecture that is delivered to
users across the broadest set of devices. Siemens will use VMware View
4.5 to establish a hosted desktop environment to provide secure,
real-time access to enterprise desktops from any device while reducing
the ongoing costs of desktop management.

Outlined in a Reference Architecture Brief announced at VMworld 2010 in
San Francisco, and a white paper commissioned by VMware and published by
Enterprises Management Associates, VMware View allows enterprises to
deploy a secure and stateless virtual desktop at a datacenter
infrastructure cost under $242 per user, more than 60 percent lower than
previously published architectures.

Available today, the full reference architecture includes detailed
validation for a deployment of 1,000 desktops. Download the reference
architecture titled VMware Reference Architecture for Stateless Virtual
Desktops with VMware View 4.5.

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)VMware Introduces Cloud App Platform for IT as a Service

At VMworld 2010, VMware introduced its cloud application platform
strategy and solutions, enabling developers to build and run modern
applications that intelligently share information with underlying
infrastructure to maximize application performance, quality of service
and infrastructure utilization.

VMware vFabric cloud application platform combines the market-leading
Spring Java development framework with platform services including
lightweight application server, global data management, cloud-ready
messaging, dynamic load balancing and application performance
management. Applications built on VMware vFabric provide performance and
portability across heterogeneous cloud environments.

Principles that have defined today's most demanding consumer
applications - built-in scalability, new data models, distributed
infrastructures - are influencing the production of new internal
customer enterprise applications. These modern applications need to
support dynamic user interactions, low-latency data access and virtual
infrastructure all while meeting the security and compliance demands of
the enterprise. VMware vFabric is optimized for cloud computing's
dynamic architectures, unlike traditional middleware that requires
complete stack control.

"IT is undergoing a transformation: applications are changing,
infrastructure is changing, and organizations are looking for a pathway
to harness the promise of the cloud," said Rachel Chalmers, director for
Infrastructure at the 451 Group. "Application platforms of today have
markedly different requirements than those we have relied upon in the
past. VMware vFabric is evolving to meet the needs of today's
organizations."

An open solution, VMware vFabric will initially target the 2.5 million
users that develop Spring Java applications. VMware vFabric will deliver
modern applications to market faster and with less complexity.

VMware vFabric Integrated Application Services is a collection of
cloud-scale, integrated services, including:

  - Lightweight Application Server: tc Server, an enterprise version of
    Apache Tomcat, is optimized for Spring and VMware vSphere and can be
    instantantly provisioned.
  - Data Management Services: GemFire eliminates database bottlenecks by
    providing real-time access to globally distributed data.
  - Cloud-Ready Messaging Service: RabbitMQ facilitates communications
    between applications inside and outside the datacenter.
  - Dynamic Load Balancer: ERS, an enterprise version Apache web server,
    helps ensure optimal performance by distributing and balancing
    application load.
  - Application Performance Management: Hyperic enables proactive
    performance management across physical, virtual and cloud
    environments.

Additional Resources

Spring and the VMware vFabric family of products are available today for
download. For additional information about VMware's cloud application
platform, please visit
<http://www.springsource.com/products/cloud-application-platform>.

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)Zimbra Desktop 2.0 combines user collaboration with offline access

Available now, Zimbra Desktop 2.0 extends the power of Software as a
Service (SaaS)-based collaboration to the desktop with an unparalleled
end user experience that works across many different platforms. Zimbra
Desktop 2.0 is designed to give Zimbra Collaboration Suite users
customizable offline access to their email, contacts, calendar and
document management in one central place. Moreover, Zimbra Desktop 2.0
operates consistently across all platforms (Windows, Mac, and Linux) and
provides a unified offline solution for businesses with multiple
operating systems for end users.

Additionally, Desktop 2.0 brings a new manageability and integration to
SaaS-based applications by enabling users and administrators to download
Zimlets directly to their desktop or VDI client. Zimlets seamlessly
integrate Zimbra's collaboration experience with third-party, SaaS-based
data sources to create new "mash-up" user interfaces within a user's
email and collaboration environment.

The new Social Zimlet in Desktop 2.0 can integrate updates from Twitter,
Facebook, Digg and other social networks. With more than 100 Zimlets
available for download, features such as translation, CRM, photos, maps
and online meeting management can be easily added to the desktop client.
Additional information on VMware Zimbra Desktop 2.0 can be found at
<http://www.vmware.com/files/pdf/vmworld/vmware-zimbra-desktop2.0-en.pdf>.

Zimbra Desktop 2.0 is available worldwide as a free-of-charge download
in 12 languages, including: Danish, Dutch, French, German, Italian,
Polish, Russian, Spanish, Swedish, and Turkish.
