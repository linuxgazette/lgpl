# What Really Matters or The Counting Article

**By [Henry Grebler](../authors/grebler.html)**


> Now it happened that Kanga had felt rather motherly that
> morning, and Wanting to Count Things - like Roo's vests, and
> how many pieces of soap there were left, and the two clean
> spots in Tigger's feeder.
> -- A. A. Milne, The House at Pooh Corner


I'm responsible for a farm of Linux and Unix machines. I'm still coming
to terms with all the computers in my domain. There's the usual panoply
of servers: DNS, DHCP, email, web servers, and file servers, plus
computers which provide the infrastructure for the organisation:
accounts, HR, administration, etc. The list goes on.

Seemingly rare these days, my desktop runs Linux. From my homebase, I
ssh into one server or another as I go about my daily duties.

I also have a laptop, but is sits to one side on my desk. Its main
purpose is to play music while I wage an endless war with the backup
system. Put tapes in; take tapes out; curse the tape drive, the jukebox,
and the rotten software. Rinse and repeat.

My desktop is set up exactly as I like it. The entire range of my HAL
(Henry Abstraction Layer) is available. On most of the servers on the
internal network, this is also true. Typically they mount my home
directory from the file server, the home directory which is repository
to the things that matter to me.

<table>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>As I reread this article, I wondered what is in my HAL these days. Quite educational. Here's the summary:</p>
<pre><code>    170 aliases     alias | wc
     56 envars      env | wc
     93 local vars      set | grep &#39;^[^ ]*=&#39; | wc
     65 functions       set | fgrep &#39; ()&#39; | wc
    159 shell scripts   files ~/scripts/*.sh | wc
    964 help files      ls ~/help*/help_* | grep -v &quot;~$&quot; | wc</code></pre>
<p>The lesson for me is that many of these have passed their use-by date. I haven't used them in yonks and I can't even remember what they were for. I think it might be time for a big clean up.</p>
<p>Also, to be perfectly honest, some of the help files are repeated in different directories. When I used:</p>
<pre><code>    ls ~/help*/help_* | grep -v &quot;~$&quot; | \
        awk -F / &#39;{print $NF}&#39; | sort -u | wc</code></pre>
<p>the number shown was only 812.</p></td>
</tr>
</tbody>
</table>

It's a bit like being at home. There are vastly more clothes than I
usually wear. But they're in a cupboard somewhere and easily accessible.
When the seasons change, so does my wardrobe.

There are also several servers which do not have access to the file
server. These are typically Internet-facing servers like the external
DNS, our Internet web presence, FTP. Also, the way the organisation is
structured, there are some semi-autonomous islands, groups of computers
that have an independent file server.

Arguably, I should install a mirror of my home directory on the file
servers for the islands. Perhaps in the fullness of time I will. In the
meantime, I have been forced to focus on what's important.

If you live in a GUI world, I doubt that what follows will interest you.

My desktop runs a windowing system (OLVWM, for what it's worth). But
apart from the almost mandatory browser (Firefox), the GUI for the
backup system and the occasional XPDF, the purpose of the windowing
system is to run oodles of xterms. How many in an oodle? I'm glad you
asked.

``` 
psa | grep xterm | wc
    177    5298   34702
```

I guess I'm running 177 xterms at the moment. Some are local to my
desktop. Just to check:

``` 
psa | grep -w ssh | wc
        44     574    4260
```

That's 44 ssh sessions to remote machines. There should be many more,
but many of the servers time me out. So, many of the xterms were sshed
into a server and are now sitting back at the bash prompt on my desktop.

A crude count can be obtained thus:

``` 
psa | grep xterm | grep unix | wc
        82    2501   16251
```

That suggests another 38 have been used to get onto another server and
have been timed out. Sometimes I have more than one xterm to a single
server. (The reason the command works is that most of our Unix/Linux
servers have "unix" as the first part of their name. It is but a crude
approximation.)

I've tried to live with what's available on any particular machine. And,
at a pinch, I can do it. But I find myself thinking too much about
**how** to do something, and not enough about **what** I want to do
next.

When you plan a holiday, you don't usually expect to take your entire
wardrobe. Typically, you will only be gone a few weeks; with a bit of
luck, the weather at your destination will be a single season. For me,
the hope is that it will be summer: I only need to pack underwear,
shorts and T shirts.

But whatever you decide to do, you are forced to focus on what's
important. Is it worth the extra effort to carry a second pair of shoes?
Do I really need a dressing gown? (No\!)

After several months, it turns out that what I really need when I visit
these alien machines is just four things. I've put these four things in
a file; and I've dedicated one of my xterms to displaying the file.
Here's the part that's relevant:

```
------------------------------------------------------------------------
        PS1='\[\033[1m\]\h:\w-\t-bash\$\[\033[0m\]\n\$ '
dth () 
{ 
    case $1 in 
        +*)
            lines=-`echo $1 | cut -c2-`;
            shift
        ;;
    esac;
    ls -lat "$@" | head $lines
}
        alias p=pushd
        alias bdx='builtin dirs |tr " " "\012" |awk "{print NR-1, \$0}"'
------------------------------------------------------------------------
```

When I visit a limited machine, after starting bash if necessary, I can
add this small part of my HAL with a simple triple-click-drag, move the
mouse, middle-button.

It's a curious collection of commands. If you'd asked me six months ago
to list the 4 most important components in my HAL, I might have guessed
one or two of these, at most.

So here's what they do and why they are so important.

### The prompt

Here is the standard prompt on one of our machines:

``` 
[root@venus ~]#
```

What does it tell me? Very little.

Here is the prompt after I have set PS1:

``` 
venus:~-21:33:16-bash#
#
```

First of all, it is bold. It constantly fascinates me that others seem
not to care about this. If I issue a command that produces an
indeterminate amount of output (like ls), with the speed of computers
and communications these days, the output appears in a twinkling. All I
can see is a screenfull of stuff. Where does the output of the last
command start? What about the command before that?

Because my prompt is bold and deliberately quite long, it's almost as if
I had ruled a line before each command. The last command (or any before
it) are easy for the eye to locate. If there were half-way decent
colours available, I might be tempted to use colours.

Did I mention that the prompt is quite long? If you look closely, you'll
notice that it's actually a two-line prompt. The commands are typed on
the second line, starting at the third character from the left of the
screen.

I choose not to have the prompt tell me which user I am running as. It's
either henry or root, and the \# means root; otherwise it would be $ and
mean henry.

It has been pointed out to me that it is redundant to have "bash" in the
prompt, and I agree. Eventually, I will fix that; but so far it has not
caused me sufficient inconvenience to bother.

I prefer "venus:" to "@venus". Look what happens if I cd to somewhere
else:

``` 
p /var/log
venus:/var/log-21:45:40-bash#
```

With my mouse, I can select the prompt before the hyphen and use it in
another xterm (one on my desktop) as part of the scp to transfer files
between the server and my desktop. For example:

``` 
scp -p venus:/var/log/syslog /tmp
```

It's not just a matter of less typing, though I welcome that; it
increases accuracy. Across machines, as in scp, you don't get file
completion.

The last part of the prompt is the time. Apart from constantly providing
me with the time, it also provides automatic (albeit crude) timing for
commands. How long did that last copy take? Look at the time before it
started, estimate how long it took to type the command, subtract from
the time in the prompt when it finished.

Where it is often valuable is in telling me when a command finished. In
this case, the indicated time is totally accurate. Why is that
important?

Well, first you have to understand that I am constantly being
interrupted all day long. That comes with the territory. So I want to
check, did I copy the relevant file before I sent the email? No. Well
that explains why that didn't work. I'd better do it again.

Perhaps not of all this is important to others. Perhaps none of it is.
It sure is important to me. That prompt has evolved over the last 20
years.

### The Function

What's more important: underwear or shorts? Food or drink? It's probably
a silly question; if you need both, they're both important.

So, also, the function is every bit as important as the prompt.

This function is my most used command. What does it do? In essence it
lists the 10 most recent entries in a directory.

When I list a directory (think /etc or /var/log), the **last** thing I
want to know is how many files are there. I'm not interested in anything
that has been there for a long time. That's just dross and distraction.

What I want to know is what has changed recently?

If all I wanted out of dth was what I have described in the summary, I
could have got by with an alias (almost). And for many years I did. But,
since it's my most used command, I got fancier.

Sometimes I want to see the first 15 entries. I can enter

``` 
dth +15
```

and it will list the first 15 entries \*and\* set the current default to
15. The next time I enter dth without a plus-arg, I'll get 15 entries
instead of 10. Of course, I can set it back with

``` 
dth +10 yada yada
```

There's one other fringe benefit. On some systems, by default, \`ls' is
aliased to \`ls --color=tty'.

I'm not sure exactly why, but when I use dth I don't see those annoying
and largely unreadable colours; I get standard black and white. Of
particular joy to me, I don't ever have to see blinking red.

\[ Piping the output of 'ls' into anything else makes 'ls' drop the
highlighting, etc. - since it's possible that those control characters
could "confuse" the downstream program. Clever gadget, that 'ls'. -- Ben
\]

In my opinion, those few lines pack a powerful punch.

### The aliases

I may as well discuss them together since they complement each other.

Instead of "cd" I prefer "pushd". And I prefer it even more if I can get
away with typing just "p".

For those not familiar, I can do no better than quote bash's help:

```
------------------------------------------------------------------------
    builtin help pushd
pushd: pushd [dir | +N | -N] [-n]
    Adds a directory to the top of the directory stack, or rotates
    the stack, making the new top of the stack the current working
    directory.  With no arguments, exchanges the top two directories.
    
    +N  Rotates the stack so that the Nth directory (counting
        from the left of the list shown by `dirs', starting with
        zero) is at the top.
    
    -N  Rotates the stack so that the Nth directory (counting
        from the right of the list shown by `dirs', starting with
        zero) is at the top.
    
    -n  suppress the normal change of directory when adding directories
        to the stack, so only the stack is manipulated.
    
    dir adds DIR to the directory stack at the top, making it the
        new current working directory.
    
    You can see the directory stack with the `dirs' command.
------------------------------------------------------------------------
```

I visit a server and usually land in my home directory if I log in as
me, root's home directory (usually /root) if I log in as root. Typically
I visit several directories.

``` 
p /etc
...
p /var/log
```

If all I'm doing is bouncing between those two directories, I can just
use "p" without args. Then I visit several more directories. Pretty soon
I have a large stack. As the man said, "You can see the directory stack
with the \`dirs' command".

Well, actually, \*I\* can't. Long ago, I defined a function "dirs" which
lists only the directories in the current directory. So I have to use:

``` code
------------------------------------------------------------------------
    builtin dirs
/p6/home/henryg/Mail/footy /p6/home/henryg/Mail/bv /p6/home/henryg/Mail/personal /p6/home/henryg/Mail/IBM /p6/home/henryg/Mail/lg /p6/home/henryg/Mail/dmo /p6/home/henryg/Mail/theatre /p6/home/henryg/Mail/spam /p6/home/henryg/Mail/mark /p6/home/henryg/Mail/tsf /p6/home/henryg/Mail/Savicky /p6/home/henryg/Mail/sav /p6/home/henryg/Mail/mh_etc /p6/home/henryg/Mail/jen
------------------------------------------------------------------------
```

You see what's wrong with that picture? First it's one l-o-n-g line. And
second, are you really going to count your way to the directory you
want?

Enter "bdx", which displays the list and the number needed to get to a
particular item:

```
------------------------------------------------------------------------
    bdx
0 /p6/home/henryg/Mail/footy
1 /p6/home/henryg/Mail/bv
2 /p6/home/henryg/Mail/personal
3 /p6/home/henryg/Mail/IBM
4 /p6/home/henryg/Mail/lg
5 /p6/home/henryg/Mail/dmo
6 /p6/home/henryg/Mail/theatre
7 /p6/home/henryg/Mail/spam
8 /p6/home/henryg/Mail/mark
9 /p6/home/henryg/Mail/tsf
10 /p6/home/henryg/Mail/Stephen
11 /p6/home/henryg/Mail/sav
12 /p6/home/henryg/Mail/mh_etc
13 /p6/home/henryg/Mail/jen
------------------------------------------------------------------------
```

If I want to get to /p6/home/henryg/Mail/spam, I just need to enter

``` 
p +7
```

\[Don't reach for you emails just yet.\]

``` 
... one learns a thing best by teaching it.
    -- John Barth, "The Sot-Weed Factor"
```

And so it has been for me. Perhaps I wrote bdx long before bash got
smarter. I'd hate to think that I went to all the trouble when I could
simply have done

``` 
alias bdx='builtin dirs -v'
```

However, in writing this article (claiming to be a teacher of sorts) I
have learnt more about the subject.

### Conclusion

(Not in the sense of "drawing a conclusion", more in the vein of "coming
to the end") So there you have it. This is the way I pack my bags if I
have to travel ultra-light; and these are the reasons why.