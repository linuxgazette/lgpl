# Selecting a Linux Distribution

Autor: Phil Hughes

---

Any current Linux distribution most likely contains the software needed
to do your job, including kernel and drivers, libraries, utilities and
applications programs. Still, one of the most common questions I hear is
"which distribution should I get?" This question is answered by an
assortment of people, each proclaiming their favorite distribution is
better than all the rest.

My new theory is that most people favor the first distribution they
successfully installed. Or, if they had problems with the first, they
favor the next distribution they install which addresses the problems of
the first.

Let's use me as an example. SLS was my first Linux installation.
Unfortunately, SLS had a few bugs--in both the installation and the
running system. This, of course, isn't a surprise since this
installation took place five years ago.

Now, about this time, Patrick Volkerding came along and created
Slackware. Pat took the SLS distribution and fixed some problems. The
result looked the same as SLS and worked the same, but without bugs. To
this day, I find Slackware the easiest distribution to install.

I have, however, progressed beyond installation problems and found some
serious shortcomings in Slackware which have been addressed by other
distributions. Before I get into specifics, here is a rough estimate of
the number of times I have installed various distributions, in order of
first installation. I give you this information to help you understand
the basis of my opinions.

- 100+ SLS/Slackware
- 5 MCC (a small distribution done for university students)
- 5 Yggdrasil
- 20 Red Hat
- 10 Caldera
- 20 Debian
- 5 S.u.S.E.

That said, here is my blow-by-blow analysis of what is right and wrong
with each distribution. Note that this is my personal opinion--your
mileage will vary.

## SLS/Slackware/MCC

All these distributions are easy to install and understand. They were
all designed to install from floppy disk, and packages were in
floppy-sized chunks. At one time, I could successfully install Slackware
without even having a monitor on the computer.

There are, however, costs associated with this simplicity. Software is
saved in compressed tar files. There is no information within the
distribution that shows how files interrelate, no dependencies and no
good path for upgrades. Not a problem if you just want to try something,
but for a multi-computer shop with long-term plans, this initial
simplicity can have unforeseen costs in the long run.

## Yggdrasil

Yggdrasil offered the most promise with a GUI-based configuration.
Unfortunately, development stopped (or at least vanished from the public
eye), and it no longer offers anything vaguely current.

## Red Hat

When I first looked at Marc Ewing's creation, I was impressed. It had
some GUI-based configuration tools and showed a lot of promise. Over the
years, Red Hat has continued to evolve and is easy to install and
configure. Red Hat introduced the RPM packaging system that offers
dependencies to help ensure loaded applications work with each other and
updating is easy. RPMs also offer pre- and post-install and remove
scripts which appear to be underutilized.

Version 4.2 has proven to be quite stable. The current release is 5.0,
and a 5.1 release with bug fixes is expected to again produce a stable
product.

The install sequence is streamlined to make it easy to do a standard
install. I see two things missing that, while making the install appear
easier, detract from what is actually needed:

1. The ability to save the desired configuration to floppy disk during
    the installation process (something that both Caldera and S.u.S.E.
    offer) would simplify subsequent installations on the same or other
    machines.
3. The ability to create a boot floppy disk during installation.

Red Hat has evolved into the most \`\`retailed'' distribution. First it
was in books by O'Reilly, then MacMillan and now IDG Books Worldwide. It
also appears to have a large retail shrink-wrap distribution in the U.S.

Versions of Red Hat are available for Digital Alpha and SunSPARC, as
well as Intel.

## Caldera

The Caldera distribution was assembled by the Linux Support Team (LST)
in Germany--now a part of Caldera. Caldera, like Red Hat, uses the RPM
package format. Installation is similar to Red Hat with the addition of
the configuration save/restore option.

Caldera is different from other distributions at this time in that it
offers a series of systems including various commercial packages such as
a secure web server and an office suite. Caldera is also the most
"commercial feeling" as far as packaging and presentation.

One complaint I received from a reviewer of my original version of this
article is that you cannot perform an upgrade. That is, you must save
your configuration files and then re-install.

## Debian

Debian is one of the oldest distributions, but because development is
strictly by a team of volunteers, it has tended to evolve more slowly.
Since development is performed by a geographically diverse group, the
ability to manage and integrate upgrades is of primary importance. To
that end, you can always upgrade a system by pointing it at an FTP site
and instructing it to get the latest versions of all the packages
currently installed. In some cases, a service needs to be stopped. (For
example, to upgrade **sendmail**, you would need to stop it, replace the
program and then restart it.) This is all done automatically.

Debian deviates from the common RPM packaging format (although it can
install RPMs) by using its own .deb format. The .deb format is the most
versatile and includes dependency checking as well as pre- and
post-install and remove scripts. This is why the sendmail example in the
previous paragraph can be handled automatically.

The most difficult thing about Debian is the initial installation. Or,
put another way, fear of **dselect**, the installer program. The design
of dselect is old, and while it made sense when there were only 50-100
packages in a Linux install, it is out of control now that there are
around 1000. A replacement for dselect is being developed and will be
available in Debian 2.1.

Versions of Debian (with limited applications/utilities) are available
for Digital Alpha and M68k.

## S.u.S.E.

S.u.S.E. is a German distribution with an installation "look and
feel" similar to Caldera. It also uses the RPM package format and
offers a save/restore configuration option during installation.

Two things make S.u.S.E. stand out from the others. First, XFree86
support tends to be better than other distributions because S.u.S.E.
works closely with the XFree86 team. Second, there are more applications
and utility programs in this distribution. A full installation takes
over 2GB of disk space.

YAST, the install/administration tool, can handle .deb and .tgz packages
as well as RPMs. Also, upgrades are quite easy and can be performed by
putting in a new CD or pointing YAST at the files and telling it to
perform the upgrade.

## Which Do I Choose?

It depends. I have one system running Caldera, three running Red Hat (a
PC, a Digital Alpha and a SunSPARC), two running Slackware, one running
S.u.S.E. (a laptop) and quite a few running Debian. (Yes, I personally
own too many computers.)

Further, there are problems with all the distributions--not the same
problems, but problems nevertheless. As a result, I don't see a perfect
answer--yet. This is not to say they don't work--just that each has its
inconsistencies and limitations. They all suffer from the lack of a
common administration tool.

At USENIX in 1997, Caldera announced a project called COAS (Caldera Open
Administration System). The discussion at the conference showed there
were more concepts to consider and a lot of implementation work before
COAS could offer a uniform installation system that would meet the needs
of the majority of Linux users.

Today, for a general-purpose system I tend to install Debian. I do,
however, install other systems for other purposes. For example, I have
S.u.S.E. on a new laptop because the volume of software included makes a
more impressive demo system.

A better question is, "which one should you choose?" The answer is
still, "it depends." Here are some hints to help you along the way:

  - If everyone you know is running a particular distribution and you
    are a newcomer, use the same one they do.
  - If you like to roll your own--that is, you expect to compile and
    install everything yourself--Slackware is probably for you.
  - If you want to "go with the crowd" today, install Red Hat.
  - If you want "everything", install S.u.S.E.
  - If you need the most "commercial" looking product or you are a
    VAR (value-added reseller), pick Caldera.
  - If the politics of free software is important to you and/or you want
    to get involved in development of a distribution, pick Debian.
  - If you have a bunch of systems you need to interconnect and upgrade,
    pick Debian or hope Caldera gets COAS completed.

## Conclusion

There is my input. Ask any other Linux user, and you will probably get a
different opinion from mine. If you are not sure you have the right
answer, there are some things you can do to make it possible to change
distributions in the future with minimal impact.

  - Make /home a separate file system. Then, if you change
    distributions, you don't have to save and restore your files. This
    also means you could have multiple distributions on one computer and
    share /home between them.
  - Select hardware supported by most distributions.
  - If you need to add applications that don't come with the Linux
    distribution, try to get ones that come with source code so you can
    upgrade them and port them to different distributions.
  - Start with a Linux archive CD set (such as InfoMagic's Developer's
    Resource). That will give you at least three distributions
    (Slackware, Debian and Red Hat) with which to play.

Good luck and happy Linuxing.

