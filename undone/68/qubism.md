# Qubism

#### By [Jon "Sir Flakey" Harsem](mailto:sirflakey@core.org.au)

-----

![qb-stats.jpg](misc/qubism/qb-stats.jpg)  
![qb-mram.jpg](misc/qubism/qb-mram.jpg)  
![qb-kiddies.jpg](misc/qubism/qb-kiddies.jpg)  
![qb-getetch.jpg](misc/qubism/qb-getetch.jpg)  
![qb-emotionchip.jpg](misc/qubism/qb-emotionchip.jpg)  
