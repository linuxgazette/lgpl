
# HelpDex

#### By [Shane Collinge](mailto:shane_collinge@yahoo.com)

-----

![jennysknee.jpg](misc/collinge/jennysknee.jpg)  
![allclean.jpg](misc/collinge/allclean.jpg)  
![ruckus.jpg](misc/collinge/ruckus.jpg)  
![explode.jpg](misc/collinge/explode.jpg)  
![lead.jpg](misc/collinge/lead.jpg)  
![psyched.jpg](misc/collinge/psyched.jpg)  

More HelpDex cartoons are at <http://www.shanecollinge.com/Linux>.
