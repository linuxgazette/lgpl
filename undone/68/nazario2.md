# Choosing Good Passwords

#### By [Jose Nazario](mailto:jose@cwru.edu)

-----

Right now I'm running Crack on some people, and I'm doing a lot of
thinking about passwords and how to generate good ones. Specifically,
what sorts of things I can do to get better ones. I was recently asked
by a friend about any ideas on passwords and about sharing them at our
local event "LinuxDay". I'll take some time now and discuss passwords
with you now. Passwords provide our most major defense against
unauthorized use of our systems, so let's keep them good, even in the
presence of crypto, firewalls, and rabid dogs.

OK, so this is how I generate passwords for myself: I reach over, grab
the nearest issue of ["Nature"](http://www.nature.com/), open it up to a
genetics article and point to some gene construct name and use that. No
lie. It's how I chose good passwords. Random, complex, easy to generate.
Granted, a lot of dictionaries have now included gene names, but these
are construct names, which differ from gene names. So, instead of
something like "Brc1" it's more like "pRSET5a::20STa::6xHis". You can
shove the latter in any cracking program and it will not fall out
quickly, I can almost garauntee it.

The trick is this: users dislike complex passwords. They're difficult to
remember, they'll cry. And they're right. To overcome that, they'll
either write it down on some post-it note on their monitor or change it
to something silly, like "LucyDoll".

Most importanly, a password should roll off the fingers. It should be
typed quickly and efficiently, and of course corectly. For that matter,
I sometimes will type it ten times quickly to generate the rythm of it,
and that works.

Quickly, a diversion to the Crack 5.0a documentation, this is ripped
from the appendix. It deals with password security and security in
general, and is some good wisdom:

> At the bottom line, the password "fred" is just as secure (random) as
> the password "blurflpox"; the only problem is that "fred" is in a more
> easily searched part of the "password space". Both of these passwords
> are more easily found than "Dxk&2+15^N" however. Now you must ask
> yourself if you can cope with remembering "Dxk&2+15^N".

OK, great, we've chosen a good password... oh crap. We have about ten
accounts, some on BBS's, some on systems we can't ssh to, and some are
the root passwords on systems we administer for major businesses. or we
have to rotate them often. How do we keep track of them all?

Myself, I do two things: I keep *levels* of passwords. I have a handful
of passwords for disposable things. Yeah, if someone grabs a password of
mine I use on a BBS and posts some flamebait, I will get some flack. But
honestly, I doubt anyone will do that, it's the systems I care about and
administer that I really protect. Hence, I cycle passwords there, using
very strong passwords that **never** go out on the wire without some
strong crypto behind them (ie secure shell). A new password is chosen
(randomly), and the old ones are bumped down the chain to less demanding
positions, system and accounts. I use the tricks I outlined above, and
it has paid off. Sometimes I forget them, and that's always a scary
moment, but it's usually no more than a minute or two.

Keeping track of multiple passwords is easily handied using the
[Password Safe](http://www.counterpane.com) from Counterpane systems,
but that only works on Windows systems. I once started writing the
implementation for Linux, but given my poor programming skills and heavy
load of other things, I doubt it will ever see the light of day (it's
sitting idle now, if anyone wants to know). I do, however, often
reccomend this program to people with lots of passwords to remember.
Other similar applications exist for the Palm Pilot of other PDAs, which
protect a bank of passwords with one password. Since most Linux geeks I
know also have PDAs, this would be a handy solution.

For some real fun, though, check out FIPS 181 (1), a scheme the
government uses to generate passwords based on pronounceable sounds.
It's pretty cool, and I started playing with it (via Gpw, a related
tool(2)). And check out how Crack (3) works, it's chez pimp. For
comparison's sake, find out how L0phtCrack (4) works, and you'll snicker
at NT's security. If you're feeling particularily brave and have some
computing power to burn, consider brute forcing passwords (6), which is
an interesting problem in dictionary generation and optimization of
routines.

### Notes and Links:

1\. FIPS 181 is Federal Information processing Standard 181. The
document can be found (with source for DOS) at
<http://www.itl.nist.gov/fipspubs/fip181.htm>. A companion FIPS
document, [FIPS 112](http://www.itl.nist.gov/fipspubs/fip112.htm),
discusses the usage and considerations of passwords.

2\. Gpw is a UNIX utility in C/C++ (and Java, too) to generate
pronoucable passwords. Handy and fun.
<http://www.multicians.org/thvv/gpw.html> . An additional one can be
found on <http://freshmeat.net/projects/apgd/>.

3\. Crack 5.0a source can be found at
<http://www.users.dircon.co.uk/~crypto/>. It can also be found at
<http://packetstorm.securify.com/Crackers/crack/>

4\. L0phtcrack... how I love thee. <http://www.l0pht.com/l0phtcrack/> .
Mudge tells us how L0phtcrack works at this realaudio presentation from
beyond Hope, 1997, NYC (1 hour)
<http://www.2600.com/offthehook/rafiles/l0pht.ram> (Note that since this
piece was originally written, L0phtcrack version 3 has been released.
Several people have noted a dramatic drop in the efficiency of cracking
passwords, though new extraction tools have been incoporated into the
code. Many people I know who use L0phtcrack use version 2.52 for
cracking after extractions with version 3.)

5\. John the Ripper is another useful password cracking utility. Several
modules for cracking S/Key and MD5 passwords have been introduced
lately. <http://www.openwall.com/john/>.

6\. This is a great description of brute forcing passwords and some of
the techniques involved... I may have to try it\! [The ambitious amateur
vs. crypt(3)](http://attila.stevens-tech.edu/~khockenb/crypt3.html)
