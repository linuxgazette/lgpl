# Compiling and Installing a Linux Kernel

#### By [Subhasish Ghosh](mailto:auspicious_blessingsindia@hotmail.com)

-----

#### **Abstract:**

Hi everyone, this article provides you with an extremely detailed and
step-by-step process describing how to Compile, Configure and then
Install a Customized Red Hat Linux Kernel. As we all know, a Customized
Kernel is required for many reasons, and I wouldn't want to go into the
details of those. I will only show how to Compile, Configure and Install
a Custom Kernel. Though the steps mentioned below are primarily targeted
for the Red Hat Linux users, but the same process applies to the users
of other Linux Distributions also, of course, with a few minor
modifications as required. (For instance, not all systems use initrd.)

#### **Main:**

Please note that I have performed all the steps mentioned below on a
computer system with the following configurations: Compaq Presario 4010
Series computer system, 15.5 GB Hard Disk Space, 96 MB RAM, 400 MHz
Intel Celeron Processor, Red Hat Linux 7.0 Distribution Release.
Underlying Kernel: **2.2.16-22**

#### **Aim:**

Our aim would be to obtain a fully working **Customized Kernel** after
all the steps mentioned below have been completed, for example, I have a
Customized Kernel named "2.2.16-22ghosh" running on my system (cause my
name is Subhasish Ghosh, you could have anything else, in fact a couple
of them running together\!). So, happy hunting and compiling the Linux
Kernel.

#### **Steps to Compile, Configure and Install a Customized Red Hat Linux Kernel:**

The steps to be followed are as follows:

Step 1: Login as "**root**" and then perform these steps.

Step 2: At the command prompt, type in: "**rpm -q kernel-headers
kernel-source make dev86**"

Step 3: If these RPMs are already installed, then proceed to step 4.
Otherwise, mount the Red Hat Linux 7.0 CD-ROM and then perform a rpm
-Uvh for installing these RPMs.

Step 4: If you have a fully working X Window System, then type in
"**startx**" at the command-prompt. In case you don't have an X Window
System configured, I personally would suggest you to have it done before
proceeding cause it would be extremely beneficial. If X Window System is
NOT configured, then type in "make config" or "make menuconfig" at the
command-prompt. Please note that I have assumed that you have an X
Window System running on your system, and for that reason, just type in
"startx".

Step 5: Then once within the GNOME environment, open the GNOME Terminal
and type in: "**cd /usr/src/linux**" and press enter.

Step 6: Then from within /usr/src/linux, type in "**make xconfig**".

Step 7: The GUI version of "make config" would come up on the screen. It
provides you with various options that you have for obtaining a
Customized Kernel.

Step 8: Now, I would suggest you to leave most of the default options
just as it is. Just don't try to fiddle around cause most of the options
are sensitive and requires expert handling. Just make sure you make the
following changes:

1\.**<span class="underline">Processor Type and Features</span>**:
Choose the correct Processor depending on whether you are working on a
Pentium 2, 3, or Intel Celeron like me. For example, I did the
following: Processor Family: PPro/686MX, Maximum Physical Memory: 1 GB,
Math Emulation: Yes, MTRR: Yes, SMP: Yes.

2.Open the **<span class="underline">Filesystems</span>** dialog and
then make the following changes to it: For example I did: DOS FAT fs
support: Yes(y), MSDOS fs support: Yes(y), UMSDOS: m, VFAT(Windows 95)
support: Yes(y), NTFS filesystem support (read-only): Yes(y), NTFS
read-write support(DANGEROUS): No(n). After you have made these changes,
please make sure you haven't changed the others in the process. All
these above-mentioned changes are quite harmless and won't cause any
harm to your existing Linux Kernel.

3.Save and Exit from the Main dialog.

Step 9: Then, perform a "**ls -al**" from within the path:
/usr/src/linux.

Step 10: I am sure you can see a file called: "**Makefile**". It is an
extremely important file for this entire Compilation process. So, make
sure you create a backup of this file, by using: "**cp Makefile
Makefile.bak**"

Step 11: Now, do: (from within /usr/src/linux) "**vi Makefile**".

Step 12: Go to line **EXTRAVERSION** and change it to something like
this, for example I changed EXTRAVERSION=-22, to
EXTRAVERSION="-22ghosh". You are free to name it anyway you wish.

Step 13: Save and exit the file.

Step 14: All the following steps should be done from within:
/usr/src/linux. Type in: "**make dep clean**", and press enter.

Step 15: Then type in: "**make bzImage modules**". This would take some
time, go and have a drink while it compiles all the necessary files. I
usually take a nap during this time, cause I do all this stuff in the
middle of the night.

Step 16: After this step is over, a "**bzImage**" file would be created
in the directory /usr/src/linux/arch/i386/boot, just go to this
directory and check whether a file called "bzImage" has been produced or
not. IF AND ONLY IF all the compilation steps have been executed
correctly and all the options that we have had chosen in "make xconfig"
are correct, this file would be produced. If you can find this file,
which I am sure you would, well, you can start enjoying already, cause
you have won 75% of the battle. If you can't see this file, I am sorry,
but you must have had made a mistake somewhere, just take a break and
carry out all the steps again from the start. I am sure you would
succeed.

Step 17: Type in (from within /usr/src/linux): "**cp
./arch/i386/boot/bzImage /boot/vmlinuz-2.2.16-22ghosh**" and press
enter.

Step 18: Then type in: "**cp System.map
/boot/System.map-2.2.16-22ghosh**"

Step 19: Then type in: "**make modules\_install**" and press enter. You
would see all the modules being installed in a new customized directory.

Step 20: Then type in: "**mkinitrd /boot/initrd-2.2.16-22ghosh.img
2.2.16-22ghosh**"

Step 21: Then, type in: "**vi /etc/lilo.conf**" and then add the
following entry:

**image=/boot/vmlinuz-2.2.16-22ghosh**

> **label=GhoshKernel  
> initrd=/boot/initrd-2.2.16-22ghosh.img  
> root=/dev/hdc5  
> read-only**

Step 22: Save and exit. Please note that you can change the entries in
the lilo.conf file as you desire, and the root should be the root in
your system, in my machine, it's at /dev/hdc5. So, insert the info from
your own system.

Step 23: Type in: "**/sbin/lilo -v -v**"

Step 24: Read all the info on the screen. If there are no errors, well,
the job's all done. Congratulations\!\!\!\!

Step 25: Reboot the system by typing in: "**/sbin/reboot**" or
"**/sbin/shutdown -r now**".

Step 26: In the start-up screen, press Tab (or Control-X, if you have
the LILO start-up screen), and you can see the entry: "GhoshKernel"
along with the other pre-existing entries.

Step 27: Type in: **GhoshKernel** and press enter. The fully working
Customized Kernel will be seen booting on your system. So, you have a
fully working Customized Kernel working on your system now.

#### **Conclusion:**

After logging in as "root", type in: "**uname -r**" and press Enter. You
can see the following entry on the screen:
**<span class="underline">2.2.16-22ghosh</span>** that proves that you
are running a Customized Kernel, and not **2.2.16-22** which was the
Kernel we had started out with. That's all. Also remember, that you can
have as many number of Kernel versions as you like running on a single
computer system. In case this doesn't work out or you guys (and gals)
face problems, make sure you do e-mail me at
<auspicious_blessingsindia@hotmail.com> for questions and suggestions. I
would really like to hear from you and help you and I hope this detailed
HowTo helps everyone out there who wants to run a fully working
Customized Kernel.

#### **Resources:**

There exists a lot of info on how to configure and run fully Optimized
and Customized Kernels at a number of web-sites. Make sure you check
out: <http://www.vmlinuz.nu> and a few other ones for the HowTo's and
Documentation on Linux Kernel.

#### ![](https://linuxgazette.net/68/gx/note.gif)Subhasish Ghosh

I'm 20 years old, and currently living in India. I am a computer-systems
engineering student as well as a Computer Professional. I currently hold
6 Microsoft Certified Professional(MCP) Certifications, and also am a
Microsoft Certified Professional Certified on NT 4.0 Track. I have been
working with Linux for a long time, especially Red Hat Linux. I am
currently preparing for the Red Hat Linux Certified Engineer(RHCE)
certification Exam and plan to work primarily with the Linux operating
system in the future.
