# XKCD

**By [Randall Munroe](../authors/munroe.html)**

<div class="cartoon1">

[![\[cartoon\]](misc/xkcd/trochee_fixation.png
"If you Huffman-coded all the 'random' things everyone on the internet has said over the years, you'd wind up with, like, 30 or 40 bytes *tops*.
")  
Click here to see the full-sized image](misc/xkcd/trochee_fixation.png)


More XKCD cartoons can be found [here](http://xkcd.com).

Talkback: [Discuss this article with The Answer
Gang](mailto:tag@lists.linuxgazette.net?subject=Talkback:184/xkcd.html)

