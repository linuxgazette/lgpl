# HAL: Part 2 My Favourite Aliases

Autor: Henry Grebler

I began this series with [HAL: Part 1 Preliminaries](../183/grebler1.html). 
I plan to discuss scripts and functions in other articles.

If you downloaded my HAL from the the first of these articles, you will
be drowned in 171 aliases, many of them obsolete and long-forgotten. I'm
a hoarder: you never know when something might come in handy.

Of those 171 aliases, you can discard anything with "cd " in it. I no
longer cd to anywhere (well, almost never). That gets rid of 30 aliases.

I will mention this one however:

```sh
alias a='cd'
```

This exemplifies the point of HAL. I'd forgotten that it was there. I
can no longer remember when I created it; I can confirm that it existed
by November 1993 (because I use RCS).

On Prime computers, we "attached" to a directory. The command could be
abbreviated to "a". When I joined
[Optimation](http://optimation.com.au/), the company's main environment
was Prime. Some form of \*n\*x did not come until later in the form of
SunOS. This was one of the aliases which facilitated the transition from
Prime to SunOS, and enabled me to switch back and forth seamlessly.

It became superfluous when I discovered pushd (also before November
1993), aliased it to its first letter, and began using it in preference
to cd.

Aliases in this category include

```sh
alias del='rm'
alias dir='ls -la'
```

In all these cases, the alias acted like scaffolding. As I grew more
comfortable with my new environment, I gradually discarded these props.

Before proceeding, I will make one prefatory remark about aliases. I
have a function (dating back perhaps to Mar 1991) called "alias" which
is defined in $HOME/bash/csh-compat. Normally, you have to write

```sh
alias name=value
```

My function allows me to write

```sh
alias name value
```

This is why in $HOME/.ENV./.cshrc\_bash you will see all the aliases
defined without the use of an equal sign (=).

I started using c-shell before I discovered bash (which originated about
1989). In c-shell, the syntax for aliases does not use an equal sign:

```sh
alias name wordlist
```

My function facilitated the transition from c-shell to bash.

```sh
alias cad='cat > /dev/null'
```

cad is a great alias. It's actually a lot more than an alias. I usually
have at least one xterm with "cad" in the title bar. The xterm icon is
placed near the top-right of my screen next to the clock in the corner.
As soon as I launch this xterm, I enter the cad alias and just leave the
window waiting.

I use it as a general purpose scratch pad or clipboard. If I want to
remember something for a little while, I paste it there.

Sometimes I want to select several fields from, say, a browser window
and paste it somewhere else, maybe another browser window. It is often
more convenient to have the first browser window and the cad window open
side by side and copy and paste from the first browser window into the
cad window; and then bring up the destination window and copy and paste
in the other direction.

This is extremely comfortable in my environment: I have
focus-follows-mouse so I don't have to click to change windows; the
window which has the focus does not have to be on top; the act of
selecting causes the selection to be copied (no Ctrl-C is necessary);
pasting is simply Middle-Click (no Ctrl-V is necessary). So, it's just
swipe, move mouse, click, move back; swipe, move mouse, click, move
back.

I use the cad window hundreds of times a day. It means I do very little
typing. This is a good thing because I live in a command-line world and
I am not a great typist. Yes, I'm lazy; no question. But, more
importantly, copy-and-paste improves accuracy. Computers are more
pedantic than humans; you can't get away with, "You know what I mean."
And they're unforgiving. Better to avoid errors - especially when it can
be done conveniently.

Finally, I sometimes use the cad alias in any window to temporarily
store text. It's hard to think of a reason off the top of my head; but
when the tool is there, it often finds a use.

```sh
alias acroread='acroread -geometry 800x898+152-0'
```

This is an example of an inadvertent alias. The way I have defined it,
my usual invocation of acroread will invoke the alias. I'd have to do
something special to not invoke the alias.

I don't recall why I defined this alias. My best guess is that I was
annoyed with the defaults offered by acroread. I probably fooled around
with different settings before settling on these. I can only say that it
must be doing something right, because I cannot recall being annoyed
with the acroread window when it pops up.

Of course, it's always possible that this alias has outlived its
usefulness; that acroread now behaves differently. After all, I've had
this alias for over 10 years.

```sh
alias cp='cp -ip'
```

Another inadvertent alias. The "p" option preserves inode information
(date, permissions, ownership). The "i" option prevents me from
inadvertently overwriting a file.

In addition to the "i" option, I also use bash's noclobber option so
that output redirection (\>) cannot overwrite an existing file.

I rely on the dates in the inode. If I copy a file, I haven't changed
its contents. I want the default date to reflect that fact. The "c" date
(ls -ltc) will indicate when I copied the file; the "t" date (ls -lt)
will reflect the date the contents last changed.

Also

```sh
alias mv='mv -i'
```

```sh
alias ed='emacs -nw --basic-display --unibyte'
```

One of my core aliases. Fits into almost all categories. I think "ed"
was the name of the editor on Prime. Further, this alias helps protect
me from GNU. (Almost) every new rev of emacs comes out with some
incompatibility with a previous rev of emacs. In my view, FSF has an
awful lot to answer for. I won't repeat myself: see [Ten Things I
Dislike About Linux](../183/grebler.html).

My most recent change comment:

```
# 15 Jan 2010 Don't let emacs colourise.
```

```
alias gcat='gunzip -vdc'
alias gunzip='gzip -vd'
```

Just for convenience. I prefer "gcat" to "gzcat".

```
alias h='history'
```

Saves so much typing when referring to bash's history.

```sh
alias hmg='more /home/henryg/hmg/help'
```

Long forgotten. I'm guessing that, once upon a time, I had a single help
file\! Wow\! What a discovery\! Sometimes this archaeology pays
dividends. This is where it all began. Look at the first line:

```sh
help - a file to help me with things I forget
```

There is also help2 through help5. Waddya know?

```sh
alias lok='sleep 1;xset s activate'
```

I've acquired the habit of using this whenever I leave my computer. It
blanks the screen.

```sh
alias mph='less /home/henryg/hmg/phone'
```

I use this to look up my personal phone directory. It has other
information, not just phone numbers.

```sh
alias psa='/bin/ps auxw'
```

Apart from its obvious purpose, this is a different sort of alias. It is
not defined in $HOME/.ENV./.cshrc\_bash, but rather in
$HOME/.ENV./.shells\_rc.\* where the asterisk represents one of the
possible platforms I've worked on - because the ps command is rarely the
same on any two platforms. So far I have these platforms: AIX, HP-UX,
Linux, SCO\_SV, SunOS4, SunOS5.

Ben has reminded me that I also ought to have

```sh
alias psag 'psa | grep -v grep | grep'
```

And now I have!

```sh
alias rlok='rlog -R -L RCS/*,v'
alias rloka='rlog -R -L RCS/*,v RCS/.*,v'
```

Answers the question, "Which files in RCS are locked?"

```sh
alias rs='rsync -urlptog'
```

should be

```sh
alias rs='rsync -urlptogH'
```

The essential rsync options.


For FreeBSD only:

``` 
    alias seq='gseq'
```

On Linux it's called seq; on FreeBSD gseq.

```sh
alias su='BSD_su'
```

su is one of the most idiosyncratic commands - a bit like the ps command
(above). Because I'm human and inconsistent (perhaps because the alias
goes back further), this gets defined in \~/.bashrc - unlike ps.

```sh
alias w='(pwd; /bin/pwd ) | uniq'
```

Bash's notion of where you are sometimes differs from where you really
are if there are symlinks involved. If the two pwds agree, only one
entry is printed. The use of "w" for "where am I?" comes from DEC or
Prime.

I've just discovered that I have defined this twice - and
inconsistently. Here's
    why:

    #  Henry Grebler    31 Jul 96  bash (and only bash) sometimes has a different
    #                               answer for pwd. Print both if different.

The file .cshrc\_bash is intended to be shared between csh and bash.

I have just changed this alias to

```sh
alias w '(pwd; /bin/pwd ) | uniq; df -h | tail -1'
```

I often also want to know whether I'm on a local disk or not.

```sh
alias xd       "date '+%a %d %b %Y %H:%M:%S %z %Z';\
                date '+%A %d %B %Y %H:%M:%S %z %Z';\
                date '+%a %e %b %Y %H:%M:%S %z %Z';\
                date '+%A %e %B %Y %H:%M:%S %z %Z';\
                date '+%Y%b%d.%H:%M:%S';
                date '+%d %b %y'"
```

This looks meaningless until you see its output:

    Tue 08 Feb 2011 10:47:00 +1100 EST
    Tuesday 08 February 2011 10:47:00 +1100 EST
    Tue  8 Feb 2011 10:47:00 +1100 EST
    Tuesday  8 February 2011 10:47:00 +1100 EST
    2011Feb08.10:47:00
    08 Feb 11

In my opinion, there is no more useless date format than the default for
Linux:

```sh
date
Tue Feb  8 10:47:32 EST 2011
```

Who uses this format? Why?

In this alias I have a template for every form of the date I'm ever
likely to want: both full and abbreviated day and month name; day of
month with leading space and leading zero. The second last form is for
appending to filenames, e.g.

```sh
cp /var/log/xxx /var/log/xxx.2011Feb08
```

The last form is for change log entries at the head of shell scripts and
C code:

```sh
#  12 Nov 01  Henry Grebler    First cut.                       sh
12 Nov 99  Henry Grebler    Start of initial coding.         c
```

But I'm starting to prefer 4 digits for the year to avoid any
possibility of ambiguity:

```sh
#  12 Nov 2011  Henry Grebler    First cut.
```

### Aliases Which Invoke Scripts

Arguably, these are unnecessary: I could simply use a symlink.

I will discuss the shell scripts in another article.

I have written these in the form they appear in $HOME/.ENV./.cshrc\_bash
because then they retain environment variables.

```sh
alias cleanup $HOME/scripts/cleanup.sh
alias diff $HOME/scripts/diff.sh
alias lns '$HOME/scripts/lns.sh'
sol='/home/henryg/scripts/sol.sh'
alias tcg='/home/henryg/scripts/tarcompressuuencode.sh +u -g'
alias tcu='/home/henryg/scripts/tarcompressuuencode.sh'
alias uue='/home/henryg/scripts/uuencode.sh'
alias uut='/home/henryg/scripts/uudecodeuncompresstar.sh'
alias x='/home/henryg/scripts/expr.sh'
alias xr='/home/henryg/scripts/xrenamewindow.sh'
alias xt='/home/henryg/scripts/xterm.sh'
```

### Summing Up

I've tried to restrict myself to aliases which are in some way
instructive.

If anyone is interested in any of this, I have a recommendation. Don't
try to expand your repetoire with too many aliases (or anything else) at
one time. It can be extremely difficult, and the exercise may end in
disappointment.

The aliases can be categorised. Clearly, many are obsolete, as this
waltz down memory lane has revealed. They may have been very important
for a while, then fell into disuse.

But that is true for many things in life. At home, we have many Thomas
the Tank Engine videos. There was a time when my younger son watched
them constantly. Now, at 22, not so much.

On the other hand, some become more important, give rise to variations
which sometimes outgrow the capabilities of aliases. They become
functions or shell scripts.

Sometimes, I create an alias and soon after wonder how I ever got by
without it.

I've shown ([What Really Matters or The Counting
Article](../181/grebler.html)) that I almost cannot live without 2 of
them, 'p' and 'bdx'. Some are not quite as essential, but are still used
frequently:

```
cad cp mv ed h lok mph psa rs su w xd
```

Some are only invoked occasionally.

The last category is aliases which invoke scripts. I will discuss these
in an article about
scripts.

