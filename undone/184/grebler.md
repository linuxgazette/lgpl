# Henry's Techno-Musings: "The Wit and Wisdom of Chairman John"

**By [Henry Grebler](../authors/grebler.html)**

### The Move

*"... we'll have 4 weeks to move, beginning early October. We need to
get as much gear racked by then as possible, so it can be switched off,
the whole rack wheeled to the new server room, plugged in and switched
on."*

I came out of my reverie and looked up. I found our fortnightly meetings
pointless. What had I been thinking? I couldn't remember. It didn't
matter.

"What kind of downtime are we advertising?" I asked.

"None during business hours," replied Chairman John. "If we have
networking done, we can wheel over racks out of hours."

Damn. I should have been paying attention. I'd asked the wrong question.

I decided to try again.

"Shouldn't we advertise greater vulnerability at this time? Things can
go wrong with moving racks, especially old hardware, e.g. unix33. What
happens if something slips and a rack falls over? If you over-promise
and under-deliver, you'll be unpopular."

Unsurprisingly, Chairman John latched onto the improbability of a rack
falling over, completely missing the point that what characterises
unforeseen setbacks is that they are, um, unforeseen.

A few others also expressed the sentiment that I was being overly
pessimistic. I felt like Cassandra.

"I just think we should hope for the best, but prepare for the worst," I
ventured.

Believing that he sensed the mood of the meeting, Chairman John
summarised, "Anyway, there won't be any problems because...", he paused
for effect, "... it is *mandated* that there will be no interruption to
computer service during business hours."

He looked at me with satisfaction. He had played his trump card\!

I was left speechless. Later, reviewing this part of the meeting, I
realised what I should have come back with: *"In the end, crossing one's
fingers is not really the best risk-management strategy."*

### ELVIS - Extremely Large Very Important Storage

The meeting continued. Later the discussion turned to ELVIS, our very
large (expandable to a petabyte, maybe more) disk storage.

Chairman John announced, "HP tech is due today to rebuild ELVIS with the
very latest OS. This new OS still won't handle CIFS and NFS
simultaneously."

Here's the deal. Chairman John has bought two ELVISes from HP, one for
us and one for our remote satellite site. Originally, the technology was
developed by IBRIX, which has since been acquired by HP. In essence,
it's a
[NAS](http://en.wikipedia.org/wiki/Network_access_server "Network
access server").

But there seems to have been a slip between the cup and the lip.

As I understand it, it can export NFS; it can export CIFS. But it seems
that it can't export both at the same time: there's some sort of locking
problem. HP can't guarantee that there won't be corruption.

Why am I using such vague terminology? Well, I have discovered in the
last few months that some people can have a Microsoft effect on me. OK,
one particular person. I've discussed this with my colleagues. They
agree that whenever there's a meeting with Chairman John, a fog of
stupidity envelopes the room, reducing all participants to the IQ of
cattle.

But I still didn't get it. "Why do they need to rebuild from scratch for
an upgrade?"

"Because I told them they could. Did you have any data that you needed
on the disk?"

It's a bit late to be asking me that now. Shouldn't he have asked me
**before** he gave permission to erase the data?

And why hasn't he answered my question?

I expressed my misgivings that if every upgrade of the ELVIS OS required
a complete wipe of the disk and a rebuild, then what do we do after
ELVIS is populated with data?

Chairman John stated categorically that this would not be the case, that
in the future, upgrades would take place over the existing data.

Um, and he knows that *how*?

Here is a piece of technology that is touted as the wave of the future.
But, let's just examine the proposition.

First of all, we already have this technology, the aging unix33
mentioned before. This is our file server. It only has about 30 or 40
terabytes, but it works. It's a Sun-Fire-V245 running Solaris 10. It
serves all those terabytes as NFS, Samba (SMB) and AppleTalk -
concurrently. In our organisation, it serves to all the Unix and Linux
machines as well as all the PCs and Macs.

Its only problem is that it lacks grunt.

But the technology to serve NFS and Samba (or CIFS) is not rocket
science. Crumbs, I do it on my desktop at home. I have never for an
instant had a concern that there could be locking problems.

Why wouldn't HP simply use CentOS?

Why did Chairman John allow himself to purchase technology that did not
work when, demonstrably, he can get technology that does work? I say
again, this is not rocket science.

> Most people in their domestic management have to deal with
> electricians and plumbers - and car salesmen. Most people don't know
> an awful lot about electricity or plumbing - and most women know very
> little about the technology behind automobiles. What colours does it
> come in? And yet, usually, they are able to make competent decisions
> about which plumber or electrician to engage. With car salesmen, it's
> not possible to deal with a good one. But most of us seem to know how
> to negotiate a reasonable deal. But when it comes to computers, most
> people seem to turn their brains off. The salesman only needs to start
> talking technicalities (megabytes or megaherz, gigabytes or gigabits)
> and purchasers forget the fundamentals of purchasing, and behave
> absurdly. "He said I needed to spend at least $5000 to get decent
> performance." *But all you do is use Hotmail\!*

I could, perhaps, consider the way most people behave excusable. But
**the head of IT?**

The rest of the department is fine; a nice blend of computer
professionals with various skills. But whenever I think of Chairman John
in relation to the department, I think of the mother as the troops march
past. "Look\! Here they come. And my son is the only one in step\!"

