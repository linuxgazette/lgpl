# The Foolish Things We Do With Our Computers

**By [Ben Okopnik](../authors/okopnik.html)**

"Foolish Things" is a now-and-again compilation we run based on our
readers' input; once we have several of these stories assembled in one
place, we get to share them with all of you. If you enjoy reading these
cautionary tales of woe, proud stories of triumph, and just plain weird
and fun things that happen between humans and silicon, that's great; if
you have some to share so that others may enjoy them, even better.
Please send them to ![email address](../gx/lgmail/articles.png).

\[ You can even tell us that it happened to A Friend of Yours, and we'll
believe you. \]

\-- Ben

-----

### "Measure Seven Times, Cut Once"

**Marcello Romani**  

I had a horror story [similar to Ben's](../183/lg_foolish.html), about
two years ago. I backed up a PC and reinstalled the OS with the backup
usb disk still attached. The OS I was reinstalling was a version of
Windows (2000 or XP, I don't remember right now). When the partition
creation screen appeared, the list items looked a bit different from
what I was expecting, but as soon as I realized why, my fingers had
already pressed the keys, deleting the existing partitions and creating
a new ntfs one. Luckily, I stopped just before the "quick format"
command...

Searching the 'net for data recovery software, I came across TestDisk,
which is targeted at partition table recovery. I was lucky enough to
have wiped out only that portion of the usb disk, so in less than an
hour I was able to regain access to the all of my data.

Since then I always "safely remove" usb disks from the machine before
doing anything potentially dangerous, and check "fdisk -l" at least
three times before deciding that the arguments to "dd" are written
correctly...

### \*Snap\*, and Your Data is Gone

**Derek Robertson**  

I'd just started as a trainee computer operator in a datacentre which
housed an IBM 370/158 mainframe. It was a weekday with lots of users
grabbing a piece of the 1 megabyte of main memory our state of the art
machine had installed. The shift leader was showing me the basics and
had the panels open on the mainframe. He was telling me what would
happen if the CPU overheated. Pointing to one of the switches he said
"Yeah - the switch will move over like so..." making a swift lateral
movement of his hand - neatly hitting the switch so flicking it over. It
went awfully quiet and seconds later the helpdesk switchboard lit up
like a christmas tree...."Ah for pete's sake" was his approximate
response.

### NOT an Apocryphal Story

**Derek Robertson**  

Another tale came from one of our programmers who was in the business at
the time PC's ran off floppies - a 5 meg winchester costing the earth.
He had installed some financial package onto the PC of a clerk who was
instructed to take a copy of the data floppy every week, keeping 4 weeks
worth of data. One day the main floppy went belly up and our programmer
went to the clerk to ask for the backup floppies to do a restore with.
She pulled open her drawer and handed him a neatly clipped sheaf of 4
pages of A4 paper. It turned out that "taking a copy of a floppy" can
have a different meaning to some people when you have a photocopier
handy\!

\[ Just to back up Derek's story, I've seen this happen - in no less
than three different places. Working tech support and field service back
in the late 80s, before computers became common home furnishings,
certainly had its moments... -- Ben \]

Last but not least this was a joke, that who knows, has probably
happened. A user rings the helpdesk to complain that the system he is
attempting to log onto won't accept his password.

Helpdesk: "Are you sure you're putting in the right password?"  
User: "Yeah - of course\!"  
Helpdesk: "You are making sure the case is right, you know, by typing in
lower and uppercase where necessary?"  
User: "Yes its a simple straightforward password."  
Helpdesk: "And it is the current password?"  
User: "Yeah, I watched my manager typing it in this morning\!"  
Helpdesk: "Ok, what is the password you've got?"  
User: "Eight stars\!"  

  