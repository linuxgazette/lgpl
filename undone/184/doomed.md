
# Doomed to Obscurity

**By [Pete Trbovich](../authors/trbovich.html)**

*These images are scaled down to minimize horizontal scrolling.*

[![](misc/doomed/0000401.jpg)  
Click here to see the full-sized image](misc/doomed/0000401.jpg)


[![](misc/doomed/0000402.jpg)  
Click here to see the full-sized image](misc/doomed/0000402.jpg)


[![](misc/doomed/0000403.jpg)  
Click here to see the full-sized image](misc/doomed/0000403.jpg)


All "Doomed to Obscurity" cartoons are at Pete Trbovich's site,
<http://penguinpetes.com/Doomed_to_Obscurity/>.

Talkback: [Discuss this article with The Answer
Gang](mailto:tag@lists.linuxgazette.net?subject=Talkback:184/doomed.html)
