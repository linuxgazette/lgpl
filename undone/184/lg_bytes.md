# News Bytes

**By [Howard Dyckoff](../authors/dyckoff.html)**

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><img src="../gx/bytes.gif" alt="News Bytes" /></td>
<td><h3 id="contents">Contents:</h3>
<ul>
<li><a href="lg_bytes.html#general">News in General</a></li>
<li><a href="lg_bytes.html#Events">Conferences and Events</a></li>
<li><a href="lg_bytes.html#distro">Distro News</a></li>
<li><a href="lg_bytes.html#commercial">Software and Product News</a></li>
</ul></td>
</tr>
</tbody>
</table>

**Selected and Edited by [Deividson
Okopnik](mailto:bytes@linuxgazette.net)**

Please submit your News Bytes items in **plain text**; other formats may
be rejected without reading. \[You have been warned\!\] A one- or
two-paragraph summary plus a URL has a much higher chance of being
published than an entire press release. Submit items to
<bytes@linuxgazette.net>. Deividson can also be reached via
[twitter](http://www.twitter.com/deivid_okop).

-----

## News in General

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)Google's Honeycomb may influence Android 2.4

Honeycomb is the new variation of the Android Operating System for
tablets, just announced in February and shipping currently on the
Motorola Xoom tablet. But a new version of Android will probably be
announced at or just before the Google IO conference in May and it is
expected some of the new features in Honeycomb will migrate to the
standard Android phone distro.

The newest version of the Android smart phone OS, nicknamed Gingerbread,
Android 2.3, was announced just before Christmas It features UI
improvements, better copy and paste selection, and improved power
management. Android 2.3 better supports VOIP and Internet telephoney by
including a full SIP protocol stack and integrated call management
services so applications can set up outgoing and incoming voice calls.
Android 2.3 also adds API support for several new sensor types,
including gyroscope, rotation vector, linear acceleration, gravity, and
barometer sensors.

Honeycomb was announced in February and is designed for bigger,
multi-touch screen and longer user interactions. It features improved 2D
and 3D graphics, redesigned widgets, improved applications such as
Google Maps that can take advantage of improved hardware and screen
size, Honeycomb was pre-announced in January when a marketing video for
the tablet OS surfaced on YouTube and referenced Android 3.0:
<http://www.youtube.com/v/hPUGNCIozp0&hl=en_US>

A transition version of Android is expect to bridge the feature gap for
rest of 2011.

The developer site developer.android.com lists the Honeycomb features
for tablets as being part of the an upcoming Android 3.0 without a
release date target:
<http://developer.android.com/sdk/android-3.0-highlights.html>

According to developer.android.com, Android 3.0 will allow users to "...
connect full keyboards over either USB or Bluetooth, for a familiar
text-input environment. For improved wi-fi connectivity, a new combo
scan reduces scan times across bands and filters. New support for
Bluetooth tethering means that more types of devices can share the
network connection of an Android-powered device."

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)Intel Goes MeeGo

At Mobile World Congress in Barcelona, Intel made several announcements
regarding the MeeGo open source platform and the industry momentum
backing both MeeGo and Intel Atom processors.

According to Intel, since MeeGo was introduced one year ago, the open
source operating system platform has had multiple code releases. MeeGo
has also gained industry momentum with system integrators as well as
OEMs and products are shipping today in multiple form factors including
netbooks, tablet, set-top-boxes and in-vehicle infotainment systems in
cars.

Intel demonstrated a compelling new MeeGo tablet user experience now
available through the Intel AppUp Developer Program. Intel also released
the MeeGo tablet user experience (UX). It features an intuitive
object-oriented interface with panels displaying content and contacts
This gives consumers fingertip access to social networks, people, videos
and photos.

The Intel AppUpSM developer program is expanding to accept apps for
MeeGo-based tablets and netbooks. The program now offers technical
resources, tools and incentives for developers to create, distribute and
sell MeeGo-based apps. MeeGo apps will also be included in the Intel App
Store.

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)The Document Foundation launches LibreOffice 3.3

The Document Foundation recently released LibreOffice 3.3, the community
version of OpenOffice 3.3, a short time after setting itself up as an
alternative to the Oracle-controlled product. This first stable release
is available for download.

The next release, version 3.3.1, is expected around Feb. 14, following
the FOSDEM conference in Brussels on February 5 and 6, where LibreOffice
developers will be presenting their work during a one-day workshop with
speeches and hacking sessions coordinated by several members of the
project.

In less than four months, the number of developers hacking LibreOffice
has grown from less than twenty in late September 2010, to well over one
hundred. This has allowed them to release on the aggressive schedule set
by the project.

LibreOffice 3.3 is also important for a number of other reasons:

  - \- the developer community has been able to build an independent
    process, and get up and running in a very short time
  - \- with a large number of new contributors attracted to the project,
    the source code is quickly undergoing a major clean-up to provide a
    better foundation for future development of LibreOffice;
  - \- the Windows installer, which is going to impact the largest and
    most diverse user base, has been integrated into a single build
    containing all language versions, thus reducing the size for
    download sites from 75 GB to 11GB, making it easier to deploy new
    versions more rapidly and lowering the carbon footprint of the
    entire infrastructure.

Caolán McNamara from RedHat, one of the developer community leaders,
commented, "This is our very first stable release, and therefore we are
eager to get user feedback, which will be integrated as soon as possible
into the code, with the first enhancements being released in February.
Starting from March, we will be moving to a real time-based,
predictable, transparent and public release schedule....". The
LibreOffice development roadmap is available at
<http://wiki.documentfoundation.org/ReleasePlan>

LibreOffice 3.3 provides all the new features of OpenOffice.org 3.3,
such as new custom properties handling; embedding of standard PDF fonts
in PDF documents; new Liberation Narrow font; increased document
protection in Writer and Calc; auto decimal digits for "General" format
in Calc; 1 million rows in a spreadsheet; new options for CSV import in
Calc; insert drawing objects in Charts; hierarchical axis labels for
Charts; improved slide layout handling in Impress; a new easier-to-use
print interface; more options for changing case; and colored sheet tabs
in Calc. Several of these new features were contributed by members of
the LibreOffice team prior to the formation of The Document Foundation.

LibreOffice 3.3 also brings several new features, including in no
particular order: the ability to import and work with SVG files; an easy
way to format title pages and their numbering in Writer; a more-helpful
Navigator Tool for Writer; improved ergonomics in Calc for sheet and
cell management; and Microsoft Works and Lotus Word Pro document import
filters. In addition, many great extensions are now bundled, providing
PDF import, a slide-show presenter console, a much improved report
builder, and more besides. A more-complete and detailed list of all the
new features offered by LibreOffice 3.3 is viewable on the following web
page: <http://www.libreoffice.org/download/new-features-and-fixes/>

The home of The Document Foundation is at
<http://www.documentfoundation.org>. The home of LibreOffice is at
<http://www.libreoffice.org>.

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)IBM's Watson, DeepBlueQA, Wins on Jeopardy\!

Reminiscent of the classic battle of John Henry and the Steam Shovel,
the first-ever man vs. machine Jeopardy\! competition was aired in
February with two matches being held over three consecutive days. And
like John Henry, the humans lost.

At a press conference in January, Jeopardy\! and IBM revealed the
non-profit beneficiaries of the upcoming contest between two of the most
celebrated Jeopardy\! Champions - Brad Rutter and Ken Jennings - and
IBM's "Watson" computing system. Rutter and Jennings planned to donate
50 percent of their winnings to charity, and IBM will donate 100 percent
of its winnings to charity.

The grand prize for this competition was be $1 million, with second
place earning $300,000, and third place earning $200,000.

Watson, named after IBM founder Thomas J. Watson, was built by a team of
IBM scientists who set out to accomplish a grand challenge - build a
computing system that rivals a human's ability to answer questions posed
in natural language with speed, accuracy and confidence. The Jeopardy\!
format provides the ultimate challenge because the game's clues involve
analyzing subtle meaning, irony, riddles, and other language
complexities in which humans excel and computers traditionally do not.

The Jeopardy\! game show, winner of 28 Emmy Awards since its debut in
1984, was inducted into the Guinness Book of World Records for the most
awards won by a TV Game Show. For more information, please visit
<http://www.ibmwatson.com>.

-----


## Conferences and Events

  - ****

<!-- end list -->

  - **Linux Foundation End User Summit**  
    March 1-2, 2011, Hyatt Jersey City/Jersey City, NJ  
    <http://events.linuxfoundation.org/events/end-user-summit>

<!-- end list -->

  - **AnDevCon: Android Developer Conference**  
    March 8-9, San Francisco, CA  
    <http://andevcon.com/>  
    \[save $300 if you Reg by Jan 14th, $100 more for NPOs and
    Government organizations.\]

<!-- end list -->

  - **CloudConnect 2011**  
    March 7-10. Avatar Hotel. Santa Clara, CA  
    <http://www.cloudconnectevent.com>

<!-- end list -->

  - **QCon London 2011**  
    March 7-11, London, UK  
    <http://qconlondon.com/london-2011/>

<!-- end list -->

  - **SxSW Interactive**  
    March 11-15, Austin, TX  
    <http://sxsw.com/attend>

<!-- end list -->

  - **Cisco Live, Melbourne**  
    March 29 - April 1, 2011  
    <http://www.cisco.com/web/ANZ/cisco-live/index.html>

<!-- end list -->

  - **EclipseCon 2011**  
    March 21-24, Hyatt Regency, Santa Clara CA  
    <http://www.eclipsecon.org/2011/>

<!-- end list -->

  - **NSDI '11 USENIX Symposium on Networked Systems Design and
    Implementation**  
    Sponsored by USENIX with ACM SIGCOMM and SIGOPS  
    March 30-April 1, 2011, Boston, MA  
    <http://www.usenix.org/events/nsdi11/>

<!-- end list -->

  - **Linux Foundation Collaboration Summit 2011**  
    April 6-8, Hotel Kabuki, San Francisco, CA  
    <http://events.linuxfoundation.org/events/collaboration-summit>

<!-- end list -->

  - **Impact 2011 Conference**  
    April 10-15, Las Vegas, NV  
    <http://www-01.ibm.com/software/websphere/events/impact/>

<!-- end list -->

  - **Embedded Linux Conference 2011**  
    April 11-13, Hotel Kabuki, San Francisco, CA  
    <http://events.linuxfoundation.org/events/embedded-linux-conference>

<!-- end list -->

  - **MySQL Conference & Expo**  
    April 11-14, Santa Clara, CA  
    <http://en.oreilly.com/mysql2011/>

<!-- end list -->

  - **Ethernet Europe 2011**  
    April 12-13, London, UK  
    <http://www.lightreading.com/live/event_information.asp?event_id=29395>

<!-- end list -->

  - **Cloud Slam - Virtual Conference**  
    April 18-22, 2011 - On-line  
    <http://www.cloudslam.org>

<!-- end list -->

  - **O'Reilly Where 2.0 Conference**  
    April 19-21, 2011, Santa Clara, CA  
    <http://where2conf.com/where2011>

<!-- end list -->

  - **Lean Software and Systems Conference 2011**  
    May 3-6, 2011, Long Beach CA  
    <http://lssc11.leanssc.org/>

<!-- end list -->

  - **Red Hat Summit and JBoss World**  
    May 3-6, 2011, Boston, MA  
    <http://www.redhat.com/promo/summit/2010/>

<!-- end list -->

  - **USENIX/IEEE HotOS XIII - Hot Topics in Operating Systems**  
    May 8-10, Napa, CA  
    <http://www.usenix.org/events/hotos11/>

<!-- end list -->

  - **Google I/O 2011**  
    May 10-11, Moscone West, San Francisco, CA  
    \[Search for conference registration in March or April\]

<!-- end list -->

  - **OSBC 2011 - Open Source Business Conference**  
    May 16-17, Hilton Union Square, San Francisco, CA  
    <http://www.osbc.com>

<!-- end list -->

  - **Scrum Gathering Seattle 2011**  
    May 16-18, Grand Hyatt, Seattle, WA  
    <http://www.scrumalliance.org/events/285-seattle>

<!-- end list -->

  - **RailsConf 2011**  
    May 16-19, Baltimore, MD  
    <http://en.oreilly.com/rails2011>

<!-- end list -->

  - **USENIX HotPar '11- Hot Topics in Parallelism**  
    May 26-27, Berkeley, CA  
    <http://www.usenix.org/events/hotpar11/>

<!-- end list -->

  - **LinuxCon Japan 2011**  
    June 1-3, Pacifico Yokohama, Japan  
    <http://events.linuxfoundation.org/events/linuxcon-japan>

<!-- end list -->

  - **Semantic Technology Conference**  
    June 5-9, 2011, San Francisco, CA  
    <http://www.semanticweb.com>

<!-- end list -->

  - **Creative Storage Conference**  
    June 28, 2011, Culver City, CA  
    <http://www.creativestorage.org>

<!-- end list -->

  - **Cisco Live US**  
    July 10 - 14, 2011  
    <http://www.ciscolive.com/us/>

<!-- end list -->

  - **Cloud Identity Summit**  
    July 18-21, Keystone, CO  
    <http://www.cloudidentitysummit.com/index.cfm>

<!-- end list -->

  - **20th USENIX Security Symposium**  
    August 10-12, 2011, San Francisco, CA  
    <http://www.usenix.org/sec11/>

<!-- end list -->

  - **LinuxCon North America 2011**  
    August 17 - 19, Hyatt Regency, Vancouver, Canada  
    <http://events.linuxfoundation.org/events/linuxcon>


-----

## Software and Product News

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)Gemalto Launches One Time Password Application for Mobile Users

At the RSA Conference in San Francisco and Mobile World Congress, in
Barcelona, in February, Gemalto introduced Protiva Mobile OTP, a
convenient, secure and cost-effective new way for businesses and their
employees to deploy two-factor authentication, with their mobile phones.
The new solution is part of Gemalto's Protiva Strong Authentication
family, which encompasses the validation server and a range of Protiva
authentication application software and authentication devices.

A One Time Password (OTP) replaces static passwords with strong
authentication and provides a convenient additional level of security
for transactions and access control.

Protiva Mobile OTP works with the popular handset platforms, including
Blackberry, iPhone, and handsets running Java, Windows CE and Brew.
Employees simply need to download Gemalto's secure app onto their mobile
phone, which is setup to immediately generate and receive OTPs using the
phone.

Combining the mobile credential with their username and
one-time-password grants employees the appropriate access to company
resources such as a VPN, intranet, mail directory, digital signature,
mail and Web pages. Protiva Mobile OTP is simple for IT administrators
to deploy and provision, and is compatible with the majority of
industry-leading IT infrastructure elements.

For more information about Protiva SA Server and solutions, visit
<http://www.gemalto.com/products/strong_auth_server/>.

  