# Book Review: Snip, Burn, Solder, Shred

Autor: Ben Okopnik

---

    SBSS.RVW 20110228
    ================
    %A   David Erik Nelson
    %C   San Francisco CA USA
    %D   2011
    %G   978-1-59327-259-3
    %I   No Starch Press, Inc
    %O   http://nostarch.com/snipburn.htm
    %P   337 pages
    %T   "Snip, burn, solder, shred: seriously geeky stuff to make with your kids"

First things first: this book is not about Linux. What it is about,
however, is the thing that for me is indistinguishable from Linux: a
spirit of exploration, of curiosity, of getting your hands into an
interesting, fascinatingly-complicated Thing and making it work - even
if you have to assemble it piece by piece. This book is a terrific
catalyst for igniting the imagination - and not only in kids, but in any
adult with even a bit of curiosity.

The thing that I found instantly appealing about it is the absence of
what I call "assumed stupidity" on the part of the reader. The
Do-It-Yourself (DIY) air throughout the book, the underlying assumption
of intelligence, caution, and ability in the reader is refreshing. Sure,
there are plenty of warnings about the various dangerous bits you're
going to deal with - and it says quite a lot for the author that even
these are interesting to read\! - but... one of the projects is a
"Ticklebox", a way to zap yourself and your friends with a little jolt
of electricity, while another is a Marshmallow Gun, which drives its
soft, fluffy projectile with (I kid you not) *breath spray* exploded by
the flash circuitry from a disposable camera.

(If you're anything like me, the last sentence will have you running out
the door and to your nearest book store to grab this book RIGHT NOW. Go
ahead; I'll wait.)

The project list veers wildly, from an squid sock puppet to an electric
guitar that you can build for $10; in fact, most of these projects can
be built very cheaply, and the pages are packed with suggestions on
using scraps of this, bits of that, and cheap but strong goop to secure
it all inside an empty cigar box or maybe an empty can with its ends cut
out. The required factor here is a will to tinker and explore, not a
pocket full of cash.

And since you're off and building that guitar, you might as well build
an amp and a stomp box, too - oh, did I mention that this book gives you
a mini-education in practical electronics just so you can complete these
projects? Perfect example of the author's DIY mindset - and as a
life-long teacher as well as an electronics engineer, I admit to being
quite impressed by it. Very well done, and bursting at the seams with
even more "curiosity bait"; anyone who gets hooked on playing with
soldering irons and electronic components is left with lots of info
resources which they can use to expand their new-found skill.

All in all, I'm very pleasantly impressed by this book - and I am
usually not the kindest of reviewers. If I have any complaint to make,
it is that the author sometimes forgets that the readers don't have
access to all the info in his head: the section on musical instruments,
for example, has very little info on actually using them - you're more
or less expected to know all that stuff. To be fair, this is a very
common and completely natural failing: we're not always aware of
everything we know as a learned skill, because after a while it becomes
"just the way the world is". But then, if you're interested in music,
this becomes just another avenue of learning and exploration...

Contents:

    Introduction
    
    PART 1 KID STUFF
    Project 1: Lock-n-Latch Treasure Chest
    Project 2: Switchbox
    Project 3: The Sock Squid
    Project 4: The PVC Teepee
    Project 5: Cheap Mesh Screen Printing
    Project 6: Shut-the-Box
    Project 7: The Ticklebox
    Project 8: Small-board Go/Tafl
    
    PART 2 THE ELECTRO-SKIFFLE BAND
    Project 9: X-Ray Talking Drums
    Project 10: Thunderdrums
    Project 11: Didgeridoo
    Project 12: Amplifier
    Project 13: The $10 Electric Guitar
    Project 14: Tremolo
    Project 15: Reverb
    Project 16: Fuzztone
    Project 17: Cigar-Box Synthesizer
    
    PART 3 LOCOMOTIVATED
    Project 18: Boomerang
    Project 19: Pop Can Flier
    Project 20: Water Rockets
    Project 21: Steamboat
    Project 22: Jitterbug
    Project 23: FedEx Kites
    Project 24: Marshmallow Muzzloader
    Appendix: Electronics and Soldering
    Index

