# Playing around with IPv6 on Linux and Freenet6

Autor: Anderson Silva

---

Earlier this month, ICANN announced that it had assigned the last
remaining blocks of IPv4 available under its control. Essentially, this
means that for regions without free addresses, it will no longer be
possible to get new devices to directly connect to the Internet.

With that in mind, I decided to learn a bit more about IPv6, and to try
to get a Linux server working with an IPv6 address. In this article, I
am not going to go over the basics of IPv6 and how it compares to IPv4.
You can read all about that on
[Wikipedia](http://en.wikipedia.org/wiki/IPv6#Comparison_to_IPv4). I
also don’t claim to be an IPv6 expert or a networking expert. In fact,
part of this article is based on [IPV6 Go6
Mini-HowTo](http://www.mrball.net/tutorials/ipv6-go6.html) from 2008. I
basically used it to set up my own host, but had to do quite a few
modifications for it to work in 2011.  I will show you how to set up a
linux box to connect the Internet using IPv6 using the gogo client from
Freenet6.net. I will also show you how to set up lighttpd to serve pages
on IPv6, and a few other IPv6 related tools.

Before we start, let me give you everything I needed to get up and
running:

1. A linux host with root access and Internet connectivity: I have a
slicehost running CentOS 5.5, running as a dns and web server (lighttpd).
2. An [freenet6.net](http://freenet6.net) account: http://gogonet.gogo6.com/page/freenet6-registration
3. The freenet6 gogoCLIENT: http://gogo6.com/downloads/gogoc-1_2-RELEASE.tar.gz

## Making sure your linux box is IPv6 ready:

Check for the ipv6 kernel module:

```sh
/sbin/lsmod | grep ipv6
ipv6          222188 xxxxx
```

Check for ifconfig output:

```sh
/sbin/ifconfig | grep inet6
inet6 addr: fe80::4240:6fff:fef7:334a/64 Scope:Link
```

If neither of the above commands return the desirable output then check
your /etc/modprobe.d/blacklist and make sure ipv6 isn’t being
blacklisted. If it is, comment it out and load the module:

```sh
/sbin/modprobe ipv6
```

## Installing and Setting up the gogoCLIENT:

Download the client:

```sh
wget http://gogo6.com/downloads/gogoc-1_2-RELEASE.tar.gz
```

Install the router advertisement daemon for IPv6:

```sh
yum install radvd
```

Enable IPv6 forwarding kernel parameter:

```sh
echo “net.ipv6.conf.default.forwarding=1” >> /etc/sysctl.conf
sysctl -p
```

Install necessary packages to compile package:

```sh
yum install gcc gcc-c++ openssl-devel
make
```

Untar, compile and install package:

```sh
tar -zxvf gogoc-1_2-RELEASE.tar.gz
cd gogoc-1_2-RELEASE
make target=linux install installdir=/usr/local/gogoc
```

Configure gogoc:

```sh
cd /usr/local/gogoc
mkdir etc logs
mv bin/gogoc.conf* etc
```

Edit gogoc.conf and change the following parameters:

```sh
userid=<enter the id you created with freenet6
passwd=<chosen password>
server=broker.freenet6.net  
auth_method=any
host_type=router
if_prefix=eth0 # or whatever device you are
going to connect to the Internet with
log_file=2
log_filename=/usr/local/gogoc/logs/gogoc.log
```

Set up a service script, source of script:
https://github.com/afsilva/config-files/raw/master/gogoc

```sh
cd /etc/init.d
wget https://github.com/afsilva/config-files/raw/master/gogoc
chmod 755 gogoc
chkconfig --add gogoc
chkconfig --list gogoc
```

At this point you should be able to connect to freenet6:

```sh
service gogoc start
```

Testing connection:

```sh
ping6 -n ipv6.google.com
PING ipv6.google.com(2001:4860:b007::67) 56 data bytes
64 bytes from 2001:4860:b007::67: icmp_seq=0 ttl=53 time=274 ms
64 bytes from 2001:4860:b007::67: icmp_seq=1 ttl=53 time=274 ms
```

Finding out your IPv6 address:

```sh
/sbin/ifconfig | grep Global
inet6 addr: 2001:5c0:1400:b::9e55/128 Scope:Global
```

You can also install elinks, and go to <http://www.whatismyipv6.net>

## A few extra notes

Note 1: radvd is used by the gogoc service script; do not try to start
it manually or tell it to start at boot time.

Note 2: The first time I tried to do this, connecting to
broker.freenet6.net timed out. I tried two or three more times and then
it eventually started working.

Note 3: Be mindful of the logs; if you have any issues, looking at the
logs will most likely help.

Note 4: Also be aware that any iptables rules you may have on your
system are probably set for IPv4. To set up your IPv6 iptables you must
install, configure and run: iptables-ipv6.

## Enabling IPv6 on lighttpd

Add the following to your /etc/lighttpd config:

```sh
$SERVER["socket"] == "[YOUR_IPv6_ADDRESS]:80"
{
  accesslog.filename = "/var/log/lighttpd/ipv6.access.log"
  server.document-root = "/var/www/html6/"
}
```

Note: Replace YOUR\_IPv6\_ADDRESS with the IPv6 assigned to you by
gogoc.

Make sure you create the /var/www/html6/ directory (or whatever other
directory you want your document-root to be) and place an index.html in
there.

Restart lighttpd:

```sh
service lighttpd restart
```

And you should be good to go. To test via command line with links:

```sh
links http6://YOUR_IPv6_ADDRESS/
```

Or on your firefox, if you have an IPv6 provider, use the following
(don’t forget the brackets):

```sh
http://[YOUR_IPv6_ADDRESS]/
```

Finally, if you want to have your IPv6 address on your DNS server, add
the following to your domain’s zone file:

```sh
some_subdomain   IN  AAAA  YOUR_IPv6_ADDRESS
```

Don’t forget to update your zone file’s serial number and restart named
service.

You should now be able to access your IPv6 web server via a browser with
the address: http://some\_subdomain.yourdomain.com

