---
title: XKCD
---

* Autor: Randall Munroe
* Tłumaczył:
* Original text:
---

[![\[cartoon\]](misc/xkcd/extended_mind.png
"Wikipedia trivia: if you take any article, click on the first link in the article text not in parentheses or italics, and then repeat, you will eventually end up at \"Philosophy\".
")  
Click here to see the full-sized image](misc/xkcd/extended_mind.png)

</div>

More XKCD cartoons can be found [here](http://xkcd.com).

  