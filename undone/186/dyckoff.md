---
title: Away Mission - May/June 2011
---

* Autor: Howard Dyckoff
* Tłumaczył: 
* Original text:
---


## Events in May

This is very full month, a Red Hat/MeeGo/Synergy month.

### Red Hat Summit

The first week is dominated by the combined Red Hat Summit and
co-located JBoss World in Boston. The joint venue is billed as "...open
source events that bring together engineers, business decision makers,
developers and community enthusiasts to learn about the latest in open
source cloud, virtualization, platform, middleware, and management
technologies." Right. As does every major conference.

But this event has the Red Hat imprimatur and a very long history with
the Linux Community, making it a Gold Standard Linux conference.

At the Red Hat Summit, you can learn about open source advancements
through:  
Technical and business seminars  
Hands-on labs and demos  
Customer case studies  
Networking opportunities  
Keynotes by Open Source leaders, and Red Hat partners  
Direct collaboration with Red Hat and JBoss engineers

It's this last item - talking with the engineers - that can justify a
trip to Boston.

Among keynotes by executives from Cisco, HP, IBM and Intel, will be
Jeremy Gutsche, the founder of TrendHunter.com and an author of
thousands of postings and articles on viral trends and innovation. He
has been described as "a new breed of trend spotter" by The Guardian,
"an eagle eye" by Global TV, an "Oracle" by the Globe and Mail and "on
the forefront of cool" by MTV.

Also, there is Red Hat's Cloud Evangelist and author of the Pervasive
Datacenter blog on CNET, Gordon Haff. He has been speaking with
enterprise architects and CIOs about their aspirations for cloud
computing, how they're approaching it, and areas of potential concern.
Gordon will be sharing these experiences in his session "Trends in Cloud
Computing: What You Need to Know." In addition, he'll be moderating the
panel discussion "Real World Perspectives Panel: Cloud" and he will also
participate in the Expert Forum panel where he and other experts will
answer questions about cloud computing.

As a preview, here are the 2010 presentations, organized by tracks:  
<http://www.redhat.com/promo/summit/2010/presentations/>

Early Bird rates are past but were $1295; registering the week of the
conference is $1595. That's high. Alumni rates for prior attendees are
$400 less in either category, which is a fairer rate for a 3 day event.
You can Register for Red Hat Summit and JBoss World until May 2.

### Google IO 2011

If you work with Google technologies, especially Android OS, then Google
IO is the place to be. But only Alumni from the last 2 years were able
to register in advance, and only a lucky few got to the registration
site within the first 30-40 minutes. I missed it too....

But there was a silver lining in that: the sell-out at Google IO led to
sell out attendance at the recent AnDevCon in San Mateo and the Android
Builders Conference that was co-located with the recent Embedded Linux
Conference. Those were good events and they should be noted for next
year. Try to follow some Google IO alumni on Twitter if you want to get
a jump on the 2012 registration.

Here is a link to content from Google IO 2010:  
<http://www.google.com/events/io/2010/sessions.html>

### Open Source Business Conference (OSBC)

Open Source has become almost common at mainline businesses. But OSBC
brings long term leaders of Open Source businesses together to discuss
new trends and developments. If you or your company is planning to jump
into the Open Source world, this is an event to attend.

Admittedly, the pronouncements and trends discussed have been less
stunning than in the first years of this conference. But as shown by the
NewsBytes story on the non-compliance of mobile apps, a lot of
developers don't do Open Source correctly. Expect this and larger
issues, like the Oracle-Google conflict, to be addressed.

The OSBC agenda sports many panels over its 2 day run. These are often
more interesting than the prepared presentations. Last year, a panel
with Microsoft executives had them talking about improving their
relationship with the Open Source community and pledging quick support
for HTML 5.

Bob Sutor of IBM gave a keynote called "Asking the Hard Questions about
Open Source Software" at the OSBC 2010 conference. It distilled the
wisdom he and other IBMers acquired after a decade of Linux and Open
Source contributions. He concluded that OSSw is still SW - that it needs
to be measured and compared with regular SW in the enterprise. There
should be no free pass due to ideology or community affiliation.

Here's a link to a PDF version of that talk, aimed at developers and
enterprise IT staff: <http://www.sutor.com/d/Sutor-OSBC-2010.pdf>

One of the best talks I experienced was given by Matt Aslett, an analyst
with The 451 Group: "The Evolution of Open Source Business Strategies"
This focused on the relations between the developer community and
supporting companies in varying business models. Aslett discussed the
evolution of those models and showed that there was a tendency to both
open the underlying platforms more and also to move to a mix of open and
proprietary licensing. In the end, he argued that there was no basic
model for open source businesses and the future remained, literally,
"mixed" For more info on his point of view, visit his blog at The 451
Group, <http://blogs.the451group.com/opensource/author/maslett/> .

This year, OSBC moves to the Hilton near Union Square from the slightly
grander Palace Hotel. This could be a sign that the event has expanded.
Let's hope the quality remains the same.

### MeeGo 2011

This is the second MeeGo conference and is sponsored by Intel, Nokia,
and the Linux Foundation.

Created by merging Moblin and Maemo, this distribution is designed for
smartphones, netbooks, entry level desktop computers and in-vehicle
enteratainment systems. Its been annointed by Intel and the Linux
Foundation. But Android seems to be getting more developer attention, so
far.

MeeGo netbooks and tablets will be coming out shortly, so this may be
the moment to ride an up-and-coming MeeGo wave. Jim Zemlin of the Linux
Foundation thinks so.

We didn't attend last year but from the quality of sessions at the Linux
Foundation's Embedded Linux Conference this year, which included some
MeeGo sessions, and the Android Builder's Conference in April, we think
this is a good bet for Linux developers and OEMs of portable and
embedded devices. And the price is right: Free.

Register at <http://sf2011.meego.com/> .

### Citrix Synergy 2011

This is the Xen Conference and Citrix, after buying XenSource, is like
EMC to VMware. This is a good event for the Xen faithful, but Citrix is
increasingly commercializing Xen offerings. And the spreading adoption
of KVM for hosting guest OSes is putting Xen out of fashion. But this is
still a solid technology and business conference with good sessions and
decent grub. If you are using XenServer or other Xen technologies, or if
you use the popular NetScaler appliances, this is the place to be.

Arguably, Citrix has had success positioning XenServer and its
management tools as a safe niche between VMware and Microsoft's HyperV.
The underlying hypervisor technology in both Xen and HyperV come out of
the same Cambridge virtualization research and the Citrix tools
integrate exceedingly well with Microsoft servers. So the slideware and
market-tecture lean a bit more toward Redmond and many of the Expo
businesses are Microsoft Partners.

More than 75 breakout sessions are planned for the 2011 Synergy. Here is
the current list:  
<http://www.citrixsynergy.com/sanfrancisco/learning/breakoutsessions.html>

There are also new self-paced labs and instructor-directed labs and many
whiteboard sessions with leading Citrix engineers.

Those self-paced Learning Labs will be held more than half a mile away
at the Hilton San Francisco Union Square. Just find an available
workstation, select your topic from the menu and get set for a 90-minute
lab experience. There will be 200 seats and computers are provided. And
no reservations are required for these labs.

Sneak peak: SYN203 Managing VM networking across the datacenter with
XenServer  
<http://www.citrix.com/tv/#videos/3781>

This session will give a glimpse into distributed virtual switching
(DVS) in Citrix XenServer will describe best practices for using DVS to
deliver more secure, measurable and reliable network services for
virtualized applications.

And here are additional video sessions from Synergy San Francisco
2010:  
<http://www.citrix.com/tv/#search/synergy+SF>

Unfortunately, aside from those edited session videos, there is no
archive of materials for previous Synergy conferences - not for previous
attendees or the public. You got to be there to get the Synergy content.

The gala Synergy party on Thursday evening, May 26, will wrap up the
conference with great entertainment by Train, the Grammy award-winning
group that recently released a very appropriate album title – "Save Me
San Francisco" – that has since gone gold.

This could be a lead-up to a sweet Memorial Day Weekend in the San
Francisco Bay Area.

Summary list:

**Red Hat Summit and JBoss World  
** May 3-6, 2011, Boston, MA

**Google I/O 2011  
** May 10-11, Moscone West, San Francisco, CA  
(Sorry - this conference filled in about an hour - We missed it too\!)

**OSBC 2011 - Open Source Business Conference  
** May 16-17, Hilton Union Square, San Francisco, CA  
<http://www.osbc.com>

**MeeGo 2011  
** May 23-25, Hyatt Regency, San Francisco  
<http://sf2011.meego.com/>

**Citrix Synergy 2011  
** May 25-27, Convention Center, San Francisco, CA  
<http://www.citrixsynergy.com/sanfrancisco/>

## Events in June

### The Semantic Technology Conference

SemTech is the world's largest, most authoritative conference on
semantic technology. This is the eigth year of the conference and the
second year the event is at a San Francisco Hotel - in this case, the
Hilton Union Square. About 1000 attendees are expected to attend SemTech
2011, and for the first time there will be a SemTech London at the end
of September and a SemTech East in Washington at the end of November.

One new theme for this 8th year of SemTech is the Future of Media. In
the session, "The paradox of content liberation: More structure means
more freedom" Rachel Lovinger will discuss how applying greater
structure to content via semantic metadata allows it to be more widely
re-used, referenced and adapted for different purposes. Semantic
metadata is what runs the Web know, extending and sharpening searches,
enhancing ecommerce, and developing social context.

If you can fit it into your schedule, this one is worth attending.

### Ottawa Linux Symposium (OLS)

The goal of the Ottawa Linux Symposium is to provide a vendor neutral
and open environment to bring together Linux developers, enthusiasts,
and systems administrators as well as to strengthen the personal
connections within the Linux Community. This conference has been running
for the last 13 years. so they must be doing several things right. This
year the main keynote is presented by the irascible Jon "Maddog" Hall, a
long-time advocate for the open source community.

OLS usually opens with an update of the current state of the Linux
Kernel and key events and contributions over the past 12 months. These
presentations were first done by Jon Corbett from LWN.net - who runs a
kernel panel at the Linux Collaboration Summit annually as well - but
this year the OLS kernel session will be done by Jon C. Masters, who is
an author and also the producer of the Kernel Podcast.

Costs are fairly low as the event is hosted by the University of Ottawa
so the full Enthusiast/Small Business Rate is only $500 and the full
Corporate rate is only $800 for 3 days. This represents their first
increase in the 13 years OLS has been running.  
OLS has set up a travel fund to help people make it to the event who
would otherwise not be able to in the current economic climate. They are
taking contributions by Paypal to Community Travel Assistance Fund page:
<http://www.linuxsymposium.org/2011/travel_assistance.php>

They also have an active following on Twitter and run contests like the
May 16th-20th raffle to win admission at the heavily discounted student
rate of $200.

Here's the link for OLS list of presentations and tutorials:  
<http://www.linuxsymposium.org/2011/speakers.php?types=talk,tutorial>

### Amazon Web Services Summits 2011

The AWS Summit is actually held on 2 different days: June 10th in New
York, and June 14th in both San Francisco and London. These regional
full-day conferences will provide information on Amazon's Cloud
offerings and feature a keynote address by Amazon.com CTO Werner Vogels,
customer presentations, how-to sessions, and tracks designed for new
users.

Among the reasons to attend:

\-- Gain a deeper understanding of Amazon Web Services, including best
practices for developing, architecting, and securing applications in the
Cloud.  
and  
\-- Learn how Solutions Providers from the AWS community have helped
businesses launch applications in the Cloud, utilizing enterprise
software, SaaS tools, and more.

These events are free if you register early, and there will be lunch.

### eComm - the Emerging Communications Conference

This event focuses on the convergence of communications and emphasizes
the impact of new technology and also open source software on digital
networks. Its a fairly young event but has been steadily growing, even
in the time of the Great Recession.

The conference has moved this year from its traditional early Spring
time to the beginning of Summer, June 27-29. There was some risk earlier
this year that it might be postponed but the main organizer, Lee
Dryburgh, is fully committed to making it happen in the new time slot.

In 2008, eComm was billed as the communications industry rethink. It
tries to be like a TED conference for the telcom sector, said Dryburgh.
It certainly remains the place to examine the varied strands of digital
communications.

You help shape the event by contributing to the on-going and open topic
discussion document - no registration required - at
<http://bit.ly/fT9L7y> .

Here's a slide share link to earlier presentations at eComm:  
<http://www.slideshare.net/search/slideshow?type=presentations&q=ecomm>

I have always found interesting speakers and attendees at eComm and the
evenings are set up like mixers to encourage interaction. The food isn't
bad either. So come if you can.

The event list:

**Semantic Technology Conference**  
June 5-9, 2011, San Francisco, CA  
<http://www.semanticweb.com>

**Amazon Web Services Summit 2011**  
10 June, New York, USA  
14 June, London, UK  
14 June, San Francisco, USA  
<http://aws.amazon.com/about-aws/aws-summit-2011/>

**Ottawa Linux Symposium (OLS)**  
Jun 13 – 15 2011, Ottawa, Canada  
<http://www.linuxsymposium.org/2011/>

**eComm - Emerging Communications Conference**  
June 27-29, 2011, Airport Marriott, San Francisco, CA  
<http://america.ecomm.ec/2011/>

  