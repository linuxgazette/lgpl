---
title: News Bytes
---

* Autor: Deividson Luiz Okopnik, Howard Dyckoff
* Tłumaczył:
* Original text:
---

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><img src="../gx/bytes.gif" alt="News Bytes" /></td>
<td><h3 id="contents">Contents:</h3>
<ul>
<li><a href="lg_bytes.html#general">News in General</a></li>
<li><a href="lg_bytes.html#Events">Conferences and Events</a></li>
<li><a href="lg_bytes.html#distro">Distro News</a></li>
<li><a href="lg_bytes.html#commercial">Software and Product News</a></li>
</ul></td>
</tr>
</tbody>
</table>

**Selected and Edited by [Deividson
Okopnik](mailto:bytes@linuxgazette.net)**

Please submit your News Bytes items in **plain text**; other formats may
be rejected without reading. \[You have been warned\!\] A one- or
two-paragraph summary plus a URL has a much higher chance of being
published than an entire press release. Submit items to
<bytes@linuxgazette.net>. Deividson can also be reached via
[twitter](http://www.twitter.com/deivid_okop).

---


## News in General

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)71% of iPhone and Android Apps Violate Open Source Licenses

At the March AnDevCon Conference, OpenLogic, Inc., a provider of open
source governance solutions. announced the results of a scan and license
compliance assessment of 635 leading mobile applications. Among other
findings, the results show that 71% of Android, iPhone and iPad apps
containing open source failed to comply with basic open source license
requirements.

OpenLogic found several apps with extensive EULAs that claimed all of
the software included was under their copyright and owned by them - when
in fact some of the code in the app was open source.

Using its scanner, OSS Deep Discovery, OpenLogic scanned compiled
binaries and source code where available for 635 mobile applications to
identify open source under GPL, LGPL and Apache licenses. For the 66
applications scanned that contained Apache or GPL/LPGL licenses, 71%
failed to comply with four key obligations that OpenLogic analyzed.
These included:

GPL/LGPL license requirements to:

  - provide source code or an offer to get the source code
  - provide a copy of the license

Apache license requires to:

  - provide a copy of the licenses
  - provide notices/attributions

"Many mobile and tablet developers may not have a complete picture of
the open source they are using and the requirements of the open source
licenses. This has real-world implications. For example, the Free
Software Foundation has stated that the GPL and iTunes license are not
compatible, and Apple has already pulled several apps from the store
that were determined to be under the GPL," said Kim Weins, senior vice
president of products and marketing at OpenLogic. "Google has also
received takedown requests for Android market apps that violated the
GPL. App developers need to pay attention to open source license
compliance to ensure their apps are not impacted by legal actions."

Out of the 635 apps scanned, OpenLogic identified 52 applications that
use the Apache license and 16 that use the GPL/LGPL license.

OpenLogic found that among the applications that use the Apache or
GPL/LGPL licenses, the compliance rate was only 29%. Android compliance
was 27% and iPhone/iOS compliance was 32%. Overall compliance of Android
applications using the GPL/LGPL was 0%.

Although the research did not specifically analyze conflicts between
different licenses, OpenLogic noted that 13 of the applications came
from the Apple App Store, used GPL/LGPL. The App Store has already
removed other applications that included GPL/LGPL licenses. In addition,
two of the applications on Android contained LGPLv2.1. This license
could have potential conflicts with Apache 2.0 - which is the major
license of the Android operating system. OpenLogic provides enterprises
with a certified library of open source software that encompasses
hundreds of the most popular open source packages via OpenLogic Exchange
(OLEX), a free web site where companies can find, research, and download
certified, enterprise-ready open source packages on demand. For more
information, visit: <https://http://olex.openlogic.com/>.

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)IronKey finds UK Organisations Fear Organised Cyber-crime

IronKey, a leader in securing data and online access, released a survey
of IT security professionals working at UK-based organisations
including, Lloyds Banking Group, HP, Fujitsu, Siemens, Worcester County
Council and Cleveland Police. The study showed that 31 per cent suffered
one or more organised attacks in the last 12 months resulting in theft
of data or money.

Besides suffering at least one cyber attack in the last 12 months, 45
per cent believed their organisation is a target of organised
cyber-crime which could result in the theft or sabotage.

"Unfortunately the results of our research don't really come as a shock,
as the past 12 months have seen some of the biggest and most successful
cyber-attacks our industry has ever witnessed," said Dave Jevans,
founder of IronKey and the Anti-Phishing Working Group.

When asked about the significant information security threat facing
their organisation today, 54 per cent of respondents highlighted
accidental data leakage by staff, contractors or vendors as the biggest
threat. The past five years of highly publicised data breaches and the
power of the Information Commissioner's Office (ICO) to levy £500,000
have gained the attention of organisations. In contrast, only 10 per
cent fear external attack on networks and systems and only 13 per cent
see Trojans that steal data, money, or sabotage systems as a significant
threat to their organisation.

While 44 per cent of respondents believed an untrusted desktop or laptop
is the most vulnerable location for an advance persistent threat (APT)
attack, it appears respondents prefer traditional methods, such as end
user education (44 per cent) or anti-virus (29 per cent), as opposed to
technology that isolates user and data from threats (19 per cent), as
the most effective tool to prevent APT attacks.

"Unfortunately, end user education and anti-virus were all in place at
organisations that suffered painful losses as a result of APT attacks.
Doing the same thing over and over won't make the problem go away -
criminals are only more encouraged," commented Jevans. "As an industry,
we need to shift away from trying to be all knowing and detecting
threats we can't know about until they happen. Instead, we need to
isolate users of sensitive data and transactions away from the problem."

As a result of cyber-crime, British business is estimated to be losing
£20bn a year. Targeted attacks on the global energy industry as part of
the Night Dragon attacks, the breach of infrastructure at RSA,
compromise of digital certificate issuance at Comodo, and theft of
millions of customer records from Epsilon all show that any organisation
is a potential target.

IronKey also announced the availability of IronKey Trusted Access for
Banking 2.7 which addresses the continuing needs of banks to isolate
customers from the growing threat of crimeware and online account
takeovers. The new update includes IronKey's keylogging protection that
blocks the capture of user credentials, one-time passcodes (OTP),
challenge questions, and other sensitive data criminals can otherwise
easily steal.

---

## Conferences and Events

  - **ESC Silicon Valley 2011 - Enbedded Systems**  
    May 2-5, McEnery Center, San Jose.  
    <http://schedule-siliconvalley.esc.eetimes.com/>.
  - **Lean Software and Systems Conference 2011**  
    May 3-6, 2011, Long Beach California.  
    <http://lssc11.leanssc.org/>.
  - **Red Hat Summit and JBoss World**  
    May 3-6, 2011, Boston, MA.  
    <http://www.redhat.com/promo/summit/2010/>.
  - **USENIX/IEEE HotOS XIII - Hot Topics in Operating Systems**  
    May 8-10, Napa, CA.  
    <http://www.usenix.org/events/hotos11/>.
  - **OSBC 2011 - Open Source Business Conference**  
    May 16-17, Hilton Union Square, San Francisco, CA.  
    <http://www.osbc.com>.
  - **Scrum Gathering Seattle 2011**  
    May 16-18, Grand Hyatt, Seattle, WA.  
    <http://www.scrumalliance.org/events/285-seattle>.
  - **RailsConf 2011**  
    May 16-19, Baltimore, MD.  
    <http://en.oreilly.com/rails2011>.
  - **FOSE 2011**  
    May 19-21, Washington, DC.  
    <http://fose.com/events/fose-2010/home.aspx>.
  - **MeeGo 2011**  
    May 23-25, Hyatt Regency, San Francisco.  
    <http://sf2011.meego.com/>.
  - **Citrix Synergy 2011**  
    May 25-27, Convention Center, San Francisco, CA.  
    <http://www.citrixsynergy.com/sanfrancisco/>.
  - **USENIX HotPar '11- Hot Topics in Parallelism**  
    May 26-27, Berkeley, CA.  
    <http://www.usenix.org/events/hotpar11/>.
  - **LinuxCon Japan 2011**  
    June 1-3, Pacifico Yokohama, Japan.  
    <http://events.linuxfoundation.org/events/linuxcon-japan>.
  - **Semantic Technology Conference**  
    June 5-9, 2011, San Francisco, CA.  
    <http://www.semanticweb.com>.
  - **Ottawa Linux Symposium (OLS)**  
    June 13 - 15 2011, Ottawa, Canada.  
    <http://www.linuxsymposium.org/2011/>.
  - **USENIX Annual Technical Conference '11  
    [![USENIX ATC
    '11](http://www.usenix.org/events/atc11/art/atc11_banner_450.jpg)](http://www.usenix.org/atc11/promote)**  
    Don't miss the 3-day conference program filled with cutting-edge
    systems research, including invited talks by Stefan Savage on "An
    Agenda for Empirical Cyber Crime Research," Finn Brunton on "Dead
    Media: What the Obsolete, Unsuccessful, Experimental, and
    Avant-Garde Can Teach Us About the Future of Media," and Mark
    Woodward on "Linux PC Robot." Gain insight into a variety of
    must-know topics in the paper presentations and poster session.
    Since USENIX ATC '11 is part of USENIX Federated Conferences Week,
    you'll also have increased opportunities to mingle with colleagues
    and leading experts across multiple disciplines.
    
    Register by May 23, and save\! Additional discounts are available.  
    http://www.usenix.org/atc11/lg
    
    June 15-17, Portland, OR.  
    <http://www.usenix.org/atc11/lg>.

  - **USENIX Conference on Web Application Development  
    [![WebApps
    '11](http://www.usenix.org/events/webapps11/art/webapps11_banner_450.jpg)](http://www.usenix.org/webapps11/promote)**  
    Back for 2011, WebApps '11 features cutting-edge research that
    advances the state of the art, not only on novel Web applications
    but also on infrastructure, tools, and techniques that support the
    development, analysis/testing, operation, or deployment of those
    applications. The diverse 2-day conference program will include
    invited talks by industry leaders including Finn Brunton, NYU, on "A
    Tour of the Ruins: 40 Years of Spam Online," a panel on "The Future
    of Client-side Web Apps," a variety of topics and new techniques in
    the paper presentations, and a poster session. Since WebApps '11 is
    part of USENIX Federated Conferences Week, you'll also have
    increased opportunities for interaction and synergy across multiple
    disciplines.
    
    Register by May 23, and save\! Additional discounts are available.  
    http://www.usenix.org/atc11/lg
    
    June 15-17, Portland, OR.  
    <http://www.usenix.org/webapps11/lg>.

  - **Creative Storage Conference**  
    June 28, 2011, Culver City, CA.  
    <http://www.creativestorage.org>.

  - **Mobile Computing Summit 2011**  
    June 28-30, Burlingame, California.  
    <http://www.netbooksummit.com/>.

  - **Cisco Live, U.S.**  
    July 10 - 14, 2011.  
    <http://www.ciscolive.com/us/>.

  - **Cloud Identity Summit**  
    July 18-21, Keystone, Colo.  
    <http://www.cloudidentitysummit.com/index.cfm>.

  - **ApacheCon NA 2011**  
    7-11 November 2011, Vancouver, Canada  
    
    ApacheCon 2011: Open Source Enterprise Solutions, Cloud Computing,
    Community Leadership
    
    Apache technologies power more than 190 million Websites and
    countless mission-critical applications worldwide; over a dozen
    Apache projects form the foundation of today’s Cloud computing.
    There’s a reason that five of the top 10 Open Source software
    downloads are Apache, and understanding their breadth and
    capabilities has never been more important. ApacheCon is the
    official conference, trainings, and expo of The Apache Software
    Foundation, created to promote innovation and explore core issues in
    using and developing Open Source solutions "The Apache Way".
    Highly-relevant sessions demonstrate specific professional problems
    and real-world solutions focusing on “Apache and”: Enterprise
    Solutions, Cloud Computing, Emerging Technologies + Innovation,
    Community Leadership, Data Handling, Search + Analytics, Pervasive
    Computing, and Servers, Infrastructure + Tools. Join the global
    Apache community of users, developers, educators, administrators,
    evangelists, students, and enthusiasts 7-11 November 2011 in
    Vancouver, Canada. Register today http://apachecon.com/
    
    [![](misc/lg/apachecon_linuxgazette.gif)  
    ApacheCon NA 2011 Vancouver](http://apachecon.com/)


## Distro News

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)Ubuntu 11.04 Goes Gold

The newest release of Ubuntu Linux was released at the end of April and
features the new Unity user interface - a source of some controversy
among beta testers - and added support for Cloud deployments and DevOps
automation.

Ubuntu 11.04 Server, while not a release with Long Term Support (LTS) ,
will include will upgrades for the core cloud solution, Ubuntu
Enterprise Cloud. The release includes the latest stable version of
technology from Eucalyptus for those looking to build private clouds.
Also included is OpenStack's latest release, 'Cactus.

For those working on public clouds, Ubuntu Server 11.04 will be
available from Amazon Web Services (AWS). In addition, Canonical is
announcing Ubuntu CloudGuest later in May, allowing individuals and
businesses to test and develop on the cloud with support and systems
management from Canonical. For the first time, potential users can
test-drive Ubuntu online using only a web browser. Visitors to
Ubuntu.com will be able to access a complete version of the latest
product without having to download anything.

In part, the CloudGuest program replaces the free CD program that
Canonical has discontinued.

With the Unity UI, Ubuntu has better support for touch screens and
multi-touch gesture controls. There also better support for Sandy Bridge
and Radeon graphics chip sets and drivers. Also Ubuntu Software Center,
used to download free applications, has been integrated with the Unity
program launcher and shows users reviews of potentially useful software.

Ubuntu 11.04 includes Cobbler and MCollective to help automate system
administration tasks and orchestrate operations, with policy-based
configurations and RPC calls.

Check here the [Ubuntu Downloads Page.](http://www.ubuntu.com/download)

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)Slackware 13.37 released

Here comes Slackware Linux 13.37, a new version of the world's oldest
surviving Linux-based operating system.

Slackware 13.37 has been released with enhanced performance and
stability from a year of rigorous testing. Slackware 13.37 uses the
2.6.37.6 Linux kernel and also ships with 2.6.38.4 kernels for those
want to be at the bleeding edge. Firefox 4.0 is the default web browser,
the X Window System has been upgraded (and includes the open source
nouveau driver for NVIDIA cards) and the desktop is and KDE 4.5.5.. Even
the Slackware installer has been improvedl with support for installing
to btrfs, a one-package-per-line display mode option, and is an easy to
set-up PXE install server that runs off the Slackware DVD.

The Speakup driver, used to support speech synthesizers providing access
to Linux for the visually impaired community, has now been merged into
all of the provided kernels.

See the [complete list of core packages in
Slackware 13.37](ftp://ftp.slackware.com/pub/slackware/slackware-13.37/PACKAGES.TXT).  
See the list of [official mirror sites](http://slackware.com/getslack/).

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)Yocto 1.0 and Carrier Grade Linux 5.0 Released

Two Linux Foundation Working Groups released major updates of their
collaborative work in April.

vThe Yocto 1.0 Project Release became availabile and includes major
improvements to its developer interface and build system, providing
developers with even greater consistency in the software and tools
they're using across multiple architectures for embedded Linux
development. [For more information, Click
here.](http://www.yoctoproject.org)

Carrier Grade Linux 5.0 covers several specification categories that
include Availability, Clustering, Serviceability, Performance,
Standards, Hardware, and Security. Also, a number of requirements have
been dropped from the specification due to the mass adoption and
ubiquity of CGL and its inclusion in the mainline Linux kernel, which
allows these specifications to become more consistent fixtures across
different distributions. For more information and to review the CGL 5.0
specification, please visit [Carrier Grade Linux'
Page](http://www.linuxfoundation.org/collaborate/workgroups/cgl).

-----

## Software and Product News

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)VMware Delivers Cloud Foundry as an Open PaaS

This next generation open application platform provides a broad choice
of developer frameworks and cloud deployment options.

VMware delivered Cloud Foundry in April, an open Platform as a Service
(PaaS) architected specifically for cloud computing environments and
delivered as a service from both enterprise datacenters and public cloud
service providers. Cloud Foundry enhances the ability of developers to
deploy, run and scale their applications in cloud environments while
allowing wide choice of public and private clouds, developer frameworks
and application infrastructure services.

VMware is introducing a new VMware-operated developer cloud service, a
new open source PaaS project and the first ever "Micro Cloud" PaaS
solution. VMware introduced Cloud Foundry at an event where developer
community leaders highlighted the value of an open PaaS in advancing
highly productive development frameworks for the cloud. Speakers
include: Dion Almaer and Ben Galbraith, co-founders of FunctionSource,
Ryan Dahl, creator of Node.JS from Joyent, Ian McFarland, VP,
Technology, Pivotal Labs, Roger Bodamer, 10Gen, steward of MongoDB, and
Michael Crandell, CEO and co-founder of RightScale. Further industry
support and blogs are available from 10Gen and RightScale.

Modern application development faces a growing set of challenges such as
diverse application development frameworks, choices in new data,
messaging, and web service application building blocks, heterogeneous
cloud deployment options, and the customer imperative to deploy and
migrate applications flexibly across enterprise private clouds and
multiple cloud service providers.

PaaS offerings have emerged as the modern solution to the changing
nature of applications, increasing developer efficiency, while promising
to let developers focus exclusively on writing applications, rather than
configuring and patching systems, maintaining middleware and physical
machines and worrying about network topologies.

Early PaaS offerings, however, restricted developers to a specific or
non-standard development frameworks, a limited set of application
services or a single, vendor-operated cloud service. "For all of the
developer interest in the potential benefits to PaaS solutions, actual
adoption has been slowed by their employment of non-standard components
and frameworks which raise the threat of lock-in," said Stephen O'Grady,
Principal Analyst at RedMonk." With Cloud Foundry, VMware is providing
developers a PaaS platform with the liberal licensing and versatility to
accommodate the demand for choice in developer programming languages."

Cloud Foundry is a modern application platform built specifically to
simplify the end-to-end development, deployment and operation of cloud
era applications. Cloud Foundry orchestrates heterogeneous application
services and applications built in multiple frameworks and automates
deployment of applications and their underlying infrastructure across
diverse cloud infrastructures.

Cloud Foundry supports popular, high productivity programming
frameworks, including Spring for Java, Ruby on Rails, Sinatra for Ruby
and Node.js, as well as support for other JVM-based frameworks including
Grails. The open architecture will enable additional programming
frameworks to be rapidly supported in the future. For application
services, Cloud Foundry will initially support the MongoDB, MySQL and
Redis databases with planned support for VMware vFabric services.

Cloud Foundry is not tied to any single cloud environment, nor does it
require a VMware infrastructure to operate. Rather, Cloud Foundry
supports deployment to any public and private cloud environment,
including those built on VMware vSphere those offered by VMware vCloud
partners, non-VMware public clouds and demonstrated support for Amazon
Web Services by cloud management provider RightScale.

Cloud Foundry will be offered in multiple delivery models:

  - New VMware-Operated Developer Service - Available now in beta
    release, <http://www.cloudfoundry.com> is a full function public
    cloud PaaS service, operated by VMware, enabling developers to
    access Cloud Foundry and providing a test bed for new services and
    operational optimization. Via this multi-tenant PaaS environment,
    developers can deploy and cloud-scale their applications in seconds.
    Developers can sign up for invitations today to use this service.
  - Open Source, Community PaaS Project - Available now at
    <http://www.cloudfoundry.org>, this open source project and
    community under Apache 2 license enables developers to inspect,
    evaluate and modify Cloud Foundry software based on their own needs,
    while also minimizing the risk of lock-in. This model provides the
    highest degree of extensibility, allowing the community to extend
    and integrate Cloud Foundry with any framework, application service
    or infrastructure cloud.
  - New Cloud Foundry Micro Cloud - Available later in Q2 2011, Cloud
    Foundry Micro Cloud will be a complete, downloadable instance of
    Cloud Foundry contained within a Linux virtual machine that can run
    on a developer's desktop, enabling simplified development and
    testing. Then developers can build and test applications on their
    own machines and ensure that applications running locally will also
    run in production, without modification, on any Cloud Foundry-based
    private or public cloud.
  - Cloud Foundry for the Enterprise and Service Providers - In the
    future, VMware will also offer a commercial version of Cloud Foundry
    for enterprises who want to offer PaaS capabilities within their own
    private clouds and for service providers who want to offer Cloud
    Foundry via their public cloud services. This commercial solution
    will enable enterprises to integrate the PaaS environment with their
    application infrastructure services portfolio. Service provider
    solutions will further deliver on the promise of portability across
    a hybrid cloud environment, enabling freedom to deploy internally or
    migrate to one of VMware's nearly 3,500 vCloud partners.

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)New Version of Intel Atom CPU aimed at NetBooks and Tablets

In April, Intel released the Intel Atom chip formerly codenamed "Oak
Trail," which will be available in devices starting in May. In addition,
at the Intel Developer Forum in Beijing, the company gave a sneak peak
of its next-generation, 32nm Intel Atom platform, currently codenamed
"Cedar Trail." This solution will help to enable a new wave of fanless,
cool and quiet netbooks, entry-level desktops and all-in-one designs.

The new Intel Atom processor Z670, part of the "Oak Trail" platform,
delivers improved video playback, fast Internet browsing and longer
battery life. The rich media experience available with "Oak Trail"
includes support for 1080p video decode, as well as HDMI. The platform
also supports Adobe Flash, enabling rich content and Flash-based gaming.

The platform also helps deliver smaller, thinner and more efficient
devices by packing integrated graphics and the memory controller
directly onto the processor die. The processor is 60 percent smaller
than previous generations with a lower-power design for fanless devices
as well as up to all-day battery life. Additional features include Intel
Enhanced Deeper Sleep that saves more power during periods of
inactivity. An integrated HD decode engine enables smooth 1080p HD video
playback with less power used.

The Intel Atom processor Z670 allows applications to run on various
operating systems, including Google Android, MeeGo and Windows. This
flexibility aides hybrid designs that combine the best features of the
netbook and tablet together.

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)SPLUNK 4.2 Features real-time alerting

Splunk has released version 4.2 of its software that collects, indexes
and harnesses any machine data logs generated by an organization's IT
systems and infrastructure - physical, virtual and in the cloud.

Splunk 4.2 builds on the innovation of previous releases, adding
real-time alerting, a new Universal Forwarder, improved usability and
performance, and centralized management capabilities for distributed
Splunk deployments.

Machine data holds a wealth of information that can be used to obtain
operational intelligence and provide valuable insights for IT and the
business. Splunk is the engine for machine data that helps enterprises
improve service levels, reduce operations costs, mitigate security
risks, enable compliance and create new product and service offerings.

Splunk 4.2 new features include:

Real-time alerting. Provides immediate notification and response for
events, patterns, incidents and attacks as they occur. Universal
Forwarder. New dedicated lightweight forwarder delivers secure,
distributed, real-time data collection from thousands of endpoints with
a significantly reduced footprint. Easier and faster. New ways to
visualize data, quick start guides for new users, integrated workflows
for common tasks and up to 10 times faster search experience in
large-scale distributed deployments. Better management of Splunk. New
centralized monitoring and license management facilitate the management
of multiple Splunk instances from one location.

For more on the Splunk 4.2 release, [download a free copy
here.](http://www.splunk.com/goto/Download_4_2).

[Watch the Splunk 4.2 video](http://www.splunk.com/goto/4_2video) .

