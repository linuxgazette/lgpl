---
title: The Linux Launderette
---

* Tłumaczenie: Marcin Niedziela
* Original text: https://linuxgazette.net/186/lg_launderette.html

---


### \[Off-topic\] Cultural notes

****

Ben Okopnik \[ben at linuxgazette.net\]

**Tue, 15 Mar 2011 07:44:09 -0400**

From a conversation I had yesterday with a man in his 80s that I met on
the street; all this in a slow, unhurried Southern accent.

I used to work f' the Forestry Service, y'know. A man there - he must be
dead b'now - tole me once: "Never hire a man t'do a job o'work for you
who eats salt herrin' for breakfast, rolls his own cig'rettes, er wears
a straw hat. Reason bein', he's either chasin' his hat down th' road,
rollin' a cig'rette, er lookin' fer a drink o'water\!"

Mark Twain would have felt right at home. ![:)](../gx/smile.png)

    --
    * Ben Okopnik * Editor-in-Chief, Linux Gazette * http://LinuxGazette.NET *

**\[ <span id="mb-off_topic_cultural_notes"></span> [Thread continues
here (3 messages/4.78kB)](misc/lg/off_topic_cultural_notes.html) \]**

-----

### Spam: I just received compensation\!

****

Ben Okopnik \[ben at linuxgazette.net\]

**Wed, 9 Mar 2011 17:44:15 -0500**

On Wed, Mar 09, 2011 at 02:30:54PM -0800, Mike Orr wrote:

    > Wow, a second one in the same day, this one with more details.

You must be famous or something. Can I have your autograph?
![:)](../gx/smile.png)

    --
    * Ben Okopnik * Editor-in-Chief, Linux Gazette * http://LinuxGazette.NET *

**\[ <span id="mb-spam__i_just_received_compensation"></span> [Thread
continues here (2
messages/2.45kB)](misc/lg/spam__i_just_received_compensation.html) \]**

