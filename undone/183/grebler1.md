# HAL: Part 1 Preliminaries

Autor: Henry Grebler

```
Happy families are all alike; every unhappy family is
unhappy in its own way.

Leo Tolstoy, "Anna Karenina", Chapter 1, first line
Russian mystic & novelist (1828 - 1910)
```

## Introduction

Most flavours of Unix/Linux/BSD are broadly alike. But they are often
subtly different (sometimes, not so subtly).

At the moment, I work in an environment where I have inherited a large
number of Unix-like machines. The platforms include mainly CentOS and
Solaris, but I've seen references to FreeBSD and others. When I have to
work on a machine, I want to concentrate on **what** I have to do, not
**how** to do it.

In the past, I have worked on different platforms for other reasons. One
company I worked for supported several different Unix-like platforms:
Solaris, AIX, HP-UX, IRIX, SCO; all considered to be flavours of Unix,
yet all different.

That's part of the reason for having an abstraction layer: to work in an
environment which allows me to be unconcerned about the specific details
of the underlying platform.

Another reason is to provide myself with a toolkit of commands that
conveniently perform common tasks.

In the very early days, I was trying to smooth the transition from DEC's
VMS to Unix.

Ultimately, HAL is an aspiration. Probably, it can never be achieved, in
part because it is aimed at a moving target. Operating systems keep
evolving; and I keep searching for the ultimate environment. As long as
these change, there will be a requirement to modify or refine my HAL.

## Motivation and Direction

A reader has asked for more information on my HAL. Perhaps he did not
expect as much as he is going to get. I'm going to try to juggle keeping
things interesting with a how-to which will allow people to create their
own PAL (Platform Abstraction Layer).

If you want to run HAL exactly as I do, I don't mind. I just don't think
it makes much sense. First, as no two people are exactly alike, I
suspect no other person would be completely comfortable in my world.

Second, as I trawled through my HAL recently in preparation for making
it available, I discovered to my horror how much obsolete material I had
accumulated over 20 years. There are scripts there I don't even remember
writing.

I don't know how far I will take this, but I suspect there will be
several articles. Unfortunately, there's a bit of a learning curve.
Consequently, this episode is likely to be a little disappointing. But,
if you stay with me, I think you'll find the next episodes much more
rewarding.

## Look and Feel

What does it feel like to live in my world? Here's how it begins:

```
HAL_ROOT/GO.sh 
Starting .bash_profile
Start .bash_profile
. /home/henryx/henry/HAL_ROOT/HAL_HOME/.bashrc
Starting .bashrc
Setting up for UTS
bash: /home/henryx/henry/HAL_ROOT/HAL_HOME/scripts/Uts_setup: No such
file or directory
GNUTAR set to tar. Use export GNUTAR=xxx to change.
End of .bashrc
source .login_sh
Starting .login_sh
Dropping '/opt/firefox' from PATH 'coz it don't exist.
Dropping '/opt/thunderbird' from PATH 'coz it don't exist.
Dropping '~/bash' from PATH 'coz it don't exist.
Dropping '/home/henryx/bin' from PATH 'coz it don't exist.
Dropping '/usr/local/bin' from PATH 'coz it's already in the PATH.
Dropping '/usr/sbin' from PATH 'coz it's already in the PATH.
Dropping '/usr/local/sbin' from PATH 'coz it's already in the PATH.
Dropping '/sbin' from PATH 'coz it's already in the PATH.
ls: ./.ENV./Xserver.*: No such file or directory
No Xserver file. Setting DISPLAY from hostname.
DISPLAY = freew.t-s-f.com.au:0.0
Warning: TERMINFO not set
Setting FONTPATH to /opt/optimation/fonts
Invoking bash
Starting .bashrc
Setting up for UTS
bash: /home/henryx/henry/HAL_ROOT/HAL_HOME/scripts/Uts_setup: No such
file or directory
GNUTAR set to tar. Use export GNUTAR=xxx to change.
End of .bashrc
freew:~-20:40:29-bash$
```

Some quick observations.

Usually, if I've set up my environment, when I log in, I get pretty much
what you see above. I'll take you through it a little later. However,
for the purpose of this exercise, I've arranged that someone could log
in as themselves, unpack a package containing my HAL and simply invoke a
single command to jump into my world.

That's what you see in the first line.

I usually log in as `henryg` but I've created a special account `henryx`
which has none of my HAL by default, so that I can be as like a vanilla
user as possible. I then run **GO.sh** to invoke HAL.

Next you'll notice that my world is quite chatty. When I first
encountered Unix - in 1985 - I read somewhere that Unix commands do
their work silently. I've never been comfortable with that, perhaps
because I'd previously worked for a computer marketing company, Prime.
When we were to do presentations to prospects, the Branch Manager would
say, "Tell 'em what you're gonna tell 'em; tell 'em; then tell 'em what
you've told 'em."

I want my world to do it; and tell me that it has done it.

I'm not clear why people want commands to execute silently. I've seen
too many examples of things going wrong silently. Perhaps, in the early
days of Unix, when interaction occurred on a Teletype - slow, noisy and
consuming paper - silent made sense. But now?

Mind you, one needs balance. Too much output and one will miss important
messages in the chatter. The above is starting to get too noisy.

I suppose, also, that the verbosity of the output reflects who I am. I
may have few faults :-), but I'll admit to a tendency to prolixity.

I've noticed a few errors in the above. These relate to elements of my
HAL that I have started to weed out because they are so old and no
longer the least bit relevant. I'm going to leave them for the moment,
for several reasons. It will confirm for you that I am human; and as
fallible as any other human.

It will show that things don't have to be perfect to be good. In fact,
as Ben says, and as I've written before, perfect can be the enemy of
good.

One can aspire to perfection. Perhaps one ought to aspire to perfection.
I've spent a lot of time in the business world. Every choice must be
weighed up in terms of cost/benefit. Even in the non-business world,
there are limited resources. The time I spend cleaning up my examples
has to be weighed against the time I spend writing this explanation. I
am not blessed with unlimited time.

Even if I'm not in business, in the end, it's a business decision.

## A walk through the setup

The script **GO.sh** creates an environment similar to that of a user
who has logged in. It is near enough if one is on a platform that uses
bash.

**GO.sh** invokes bash explicitly because every Linux comes with bash.
If you prefer some other shell, you will have to adjust some scripts.

``` 
When bash is invoked as an interactive login shell ..., it
first reads ... /etc/profile [then] looks for ~/.bash_profile
...

  -- from the BASH(1) man page
```

So the first thing you see above is the invocation of
**~/.bash_profile**. My **~/.bash_profile** explicitly sources
**$HOME/.bashrc**. It then explicitly sources **.login_sh**.

This perhaps leads to some redundancy, where some scripts are invoked
twice. But, better some redundancy than that a script be omitted.

You will see messages indicating the invocation and termination of these
scripts in the messages above.

## The Startup Scripts

Here is a map of the scripts involved in startup and how they are
invoked. If there is no path, the scripts are relative to $HOME.

```
.bash_profile
    . $HOME/.bashrc
            source ~/bash/csh-compat
            source ~/.ENV./.cshrc_bash
                source $HOME/scripts/Uts_setup
            source ~/.ENV./.kshrc_bash
            `$HOME/.ENV./set_gnutar.sh`
    source .login_sh
        `$HOME/.ENV./.login.set_terminal`
        . $HOME/.ENV./.login$xxx.optimation_sh
        . $HOME/.ENV./.login.`uname`
        . /tmp/HMGUSESHELL
```

Here's what they do (in brief):

```
.bash_profile   login-time startup file for bash

        wrapper; basically, gets things going

.bashrc     Bourne Again SHell init file

        configures tty settings
        sets various path variables, other environment variables
        defines some "su" functions
        defines many aliases
        displays my motd (message of the day)

csh-compat  C-shell compatibility package

.cshrc_bash a script file to be shared between csh and bash

        mainly aliases

Uts_setup   specific to an old project

.kshrc_bash a script file to be shared between ksh and bash

        mainly functions

set_gnutar.sh   set GNUTAR environment variable 

.login_sh   login time setups

        tty settings
        Set PATH
        Set other paths
        Set DISPLAY
        Platform-specific settings
        work-specific settings
        Calculate shell to use
```

## The Rest of the Login Messages

The message *Setting up for UTS* (followed by an error message) dates
back to 1991; it's obsolete, referring to a project I was working on
back then.

The line beginning *GNUTAR* dates back to 1995 when I was working on
machines which had rather feeble versions of **tar**. Where possible, I
would assign the environment variable *GNUTAR* to refer to a more
feature-rich version of tar, usually, GNU tar. *GNUTAR* was used in
several functions.

We then have several *Dropping* messages. These come from
**.login\_sh**. Since different flavours of Unix had software in
different places, I used to set the PATH variable to every possible
place on every possible platform. I then realised that this was
dreadfully inefficient (probably while trawling through a Solaris truss
output). So I changed the logic. I set another variable to every
possible path; then tested if the path actually existed on the current
platform before adding it to **PATH**. Those that didn't were dropped.

The next 3 lines refer to my search for an appropriate DISPLAY variable.

Finally, there are a couple of messages to do with other environment
variables.

## Wrap Up

That's probably enough for Part 1.

In subsequent episodes I hope to walk you through various parts of my
HAL, stopping along the way to examine various bits in some detail.

For now, if I've done my job, you ought to be able to [obtain the
package](misc/grebler/HAL_ROOT.tar.gz), unpack it and invoke it - and
find yourself in my world.

I'm not suggesting that you want to live in my world, but you might find
bits that you like and can use for
yourself.

