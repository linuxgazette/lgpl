# Mailbag

### This month's answers created by:

**\[ Anderson Silva, Matt Woodson, Ben Okopnik, S. Parthasarathy, Henry
Grebler, Kapil Hari Paranjape, Mulyadi Santosa, Neil Youngman, Raj
Shekhar \]**  
...and you, our readers\!  

-----

# Our Mailbag

-----

### Looking for a Shuttle replacement

****

Thomas Adam \[thomas at fvwm.org\]

  
**Mon, 17 Jan 2011 22:14:40 +0000**

Hi all,

\[This might help drum up conversation here, if nothing else, but I
appreciate it's not really Linux-centric.\]

Some of you might recall some years ago the following:

[http://linuxgazette.net/141/misc/lg/shuttle\_sd39p2\_\_should\_i\_buy\_one.html](../141/misc/lg/shuttle_sd39p2__should_i_buy_one.html)

Unfortunately, said machine literally went up in white smoke just before
Christmas. From what I can tell, an integrated circuit on the
motherboard blew, as the PSU is still intact, according to my volt
metre, as are the harddrives, etc. The point is, it's something
esoteric, and not the usual suspects I would expect to have blown, else
I could have replaced those parts.

However, because it's a Shuttle, replacing the motherboard outright is
not an option either, as it's an integrated system due to its cooling
fans, etc. Not to mention the apparent cost of doing so amounts to an
equivalent cost of the Shuttle overall. ![:)](../gx/smile.png) Buying a
new machine therefore seems logical.

So, would anyone be able to help me in replacing my Shuttle with a
modern equivalent? When I say equivalent, I mean:

\* Must be of a similar size to the Shuttle (form-factor). If this
implies mini-itx, that's fine. \* Should be as low-powered as possible
-- i.e., not consuming the planet. Think green. \* Would need space for
at least two hard drives minimum. \* Minimum of two PCI slots (one of
which must be an PCI-e slot.) \* Support for dual-head graphics a must.
\* On-board wireless would be nice (without the need for an additional
card)

I appreciate some of the above might not necessarily fit any one system,
and I'd be willing to sacrifice on-board wireless for one of the PCI
slots (as my Shuttle only had two PCI(e) slots anyway), but who knows
what's possible? ![:)](../gx/smile.png)

I don't know an awful lot about hardware, so any thoughts/suggestions
are gratefully received. Of course, the systems should support
Linux/\*BSD \[1\] -- I don't want to have to grapple with anything
Windows-specific.

Any help is thankfully appreciated. ![:)](../gx/smile.png)

TIA,

\-- Thomas Adam

\[1\] I've been using OpenBSD for a long time now, so anything to help
further that is definitely a bonus. ![:)](../gx/smile.png)

    -- 
    "It was the cruelest game I've ever played and it's played inside my head."
    -- "Hush The Warmth", Gorky's Zygotic Mynci.

**\[ <span id="mb-looking_for_a_shuttle_replacement"></span> [Thread
continues here (2
messages/4.09kB)](misc/lg/looking_for_a_shuttle_replacement.html) \]**

-----

### Linux Gazette Facebook Page

****

afsilva at gmail.com \[(afsilva at gmail.com)\]

  
**Mon, 10 Jan 2011 23:26:08 -0500**

A while back LG created an facebook 'group', but that didn't work out so
well. We have now created a Linux Gazette facebook page where instead of
'joining' all you need to do it 'Like' it. Once you do that, you will
automatically receive updates from LG on your facebook wall.

I am also linking the twitter account to the facebook page, so updates
there will show directly onto facebook (when twitter isn't overloaded
![:-)](../gx/smile.png)).

And finally, through a Linux Gazette facebook you will be able to share
our updates onto your wall as well.

Here's the URL:
<http://www.facebook.com/pages/Linux-Gazette/155446237840163>

AS

    -- 
    http://www.the-silvas.com

**\[ <span id="mb-linux_gazette_facebook_page"></span> [Thread continues
here (2 messages/1.77kB)](misc/lg/linux_gazette_facebook_page.html) \]**

-----


### Finding duplicate images, redux

****

Ben Okopnik \[ben at linuxgazette.net\]

  
**Tue, 28 Dec 2010 12:58:41 -0500**

Amusing example of serendipity: one of our readers just sent me an email
letting me know that a link to a SourceForge project in one of our
articles was outdated and needed to be pointed to the new, renamed
version of the project. I changed it after taking a moment to verify the
SF link - and noticed that some of the project functionality was
relevant to Neil Youngman's question of a couple of months ago.

Pulling down the (small) project tarball and reading the docs supported
that impression:

``` 
  'repeats' searches for duplicate files using a multistage process. Ini-
  tially, all files in the specified directories (and all of their subdi-
  rectories) are listed as potential duplicates.  In the first stage, all
  files with a unique filesize are declared unique and are  removed  from
  the list.  In the second stage, any files which are actually a hardlink
  to another file are removed, since they don't actually take up any more
  disk space.  Next, all files for which the first 4096 bytes (adjustable
  with the -m option) have a unique filehash are declared unique and  are
  removed from the list.  Finally, all files which have a unique filehash
  (for the entire file) are declared unique  and  are  removed  from  the
  list.   Any remaining files are assumed to be duplicates and are listed
  on stdout.
```

The project is called "littleutils", by Brian Lindholm. There's a number
of other handy little utilities in there, all worth exploring.

    -- 
    * Ben Okopnik * Editor-in-Chief, Linux Gazette * http://LinuxGazette.NET *

**\[ <span id="mb-finding_duplicate_images_redux"></span> [Thread
continues here (4
messages/5.55kB)](misc/lg/finding_duplicate_images_redux.html) \]**

-----

### Odd system load situation

****

Ben Okopnik \[ben at linuxgazette.net\]

  
**Mon, 10 Jan 2011 21:21:19 -0500**

Hi, all -

I've got something odd going on, and I'm trying to get some perspective
on it. Perhaps someone here can cast a light.

I'm running a long-term rsync job (a full backup, after way too long of
a hiatus. I know - **really** \*bad\* for someone who hounds people to
keep on top of their backups professionally... but you know the saying
about the cobbler's kids being the last to have new shoes.) It's copying
the files to an external drive, connected via USB. Here's the problem:
now that it's running, my system has become extremely "sensitive" to any
additional loads - even very light ones. Firing up an xterm visibly
spikes the CPU (which, with nothing more than two xterms open, is
running at a load average of \~4.) Starting 'vim' takes about 4 seconds.
Opening a PDF with the rather lightweight 'xpdf' takes about 9 seconds.
Reformatting a paragraph in 'vim' turns the xterm gray for a good 5
seconds and almost freezes the cursor. Opening Firefox *does* freeze the
cursor and prevents me from being able to tab between open applications
for a good 30 seconds - and when it's open, the system is essentially
useless for anything else. Needless to say, all but the last one are
usually nearly instant, and Firefox normally takes just a couple of
seconds, and doesn't lock anything up while loading.

Here's the kicker: "top" shows... nothing particularly unusual.
vmstat/iostat report essentially the same story.

``` code
$ (vmstat -a;iostat)
procs -----------memory---------- ---swap-- -----io---- -system-- ----cpu----
 r  b   swpd   free  inact active   si   so    bi    bo   in   cs us sy id wa
 2  2 492908  15776 580684 341224    3    4    14     9    2   21 17  4 74  6
Linux 2.6.31-22-generic (Jotunheim)     01/10/2011      i686  (2 CPU)
 
avg-cpu:  %user   %nice %system %iowait  %steal   %idle
          16.70    0.20    3.81    5.71    0.00   73.59
 
Device:            tps   Blk_read/s   Blk_wrtn/s   Blk_read   Blk_wrtn
sda               3.27       131.12        48.13  219433573   80544152
sdb               0.62         0.85        72.43    1429100  121212480
```

Memory usage is reasonable, swap is barely being touched, the CPU is
spending 3/4 of its time being idle, even the number of context switches
is very reasonable as compared to the I/O rate. If I saw this on a
remote machine, I'd figure it was being under-utilized. :\\

Now, it is true that I'm not running some mad smokin' powerhouse machine
that requires a dedicated nuclear power plant:

``` code
$ cat /proc/cpuinfo|egrep '^(processor|model name)'
processor       : 0
model name      : Intel(R) Atom(TM) CPU N270   @ 1.60GHz
processor       : 1
model name      : Intel(R) Atom(TM) CPU N270   @ 1.60GHz
$ cat /proc/meminfo|grep MemTotal
MemTotal:        1016764 kB
```

It's just a little Acer laptop... but this is usually enough for pretty
much anything I need, including serving fairly heavy-duty Perl and PHP
scripting via Apache. So... what's going on? What is "rsync" doing that
is essentially invisible but is enough to make this thing behave like a
286 with 64k of memory? I thought I understood what the above numbers
mean, and could reasonably estimate system state from them - but it
seems that I'm wrong.

\[ ... \]

**\[ <span id="mb-odd_system_load_situation"></span> [Thread continues
here (26 messages/52.68kB)](misc/lg/odd_system_load_situation.html) \]**

-----

### Bypassing GRUB

****

Dr. Parthasarathy S \[drpartha at gmail.com\]

  
**Fri, 10 Dec 2010 19:16:43 +0530**

I often experiment with multiple distros (for learning/teaching value),
by installing them on my machine side by side. I then get to use a
specific distro/kernel by selecting it through GRUB, at boot time. Is
there some way to by-pass GRUB altogether and boot a specific kernel
manually ?

Let me be clear, I want to bypass GRUB and choose the kernel/distro
manually. It is not about replacing the sick GRUB by a healthy GRUB.

I would appreciate any clue or pointer.

Thank you,

partha

    -- 
    ---------------------------------------------------------------------------------------------
    Dr. S. Parthasarathy                    |   mailto:drpartha at gmail.com
    Algologic Research & Solutions    |
    78 Sancharpuri Colony                 |
    Bowenpally  P.O                          |   Phone: + 91 - 40 - 2775 1650
    Secunderabad 500 011 - INDIA     |
    WWW-URL: http://algolog.tripod.com/nupartha.htm
    GPG Public key :: http://algolog.tripod.com/publikey.htm
    ---------------------------------------------------------------------------------------------

**\[ <span id="mb-bypassing_grub"></span> [Thread continues here (5
messages/7.59kB)](misc/lg/bypassing_grub.html) \]**
