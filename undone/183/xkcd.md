# XKCD

**By [Randall Munroe](../authors/munroe.html)**

[![\[cartoon\]](misc/xkcd/good_code.png
"You can either hang out in the Android Loop or the HURD loop.
")  
Click here to see the full-sized image](misc/xkcd/good_code.png)

More XKCD cartoons can be found [here](http://xkcd.com).

Talkback: [Discuss this article with The Answer
Gang](mailto:tag@lists.linuxgazette.net?subject=Talkback:183/xkcd.html)
