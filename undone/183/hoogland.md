# Enlightened Distros

**By [Jeff Hoogland](../authors/hoogland.html)**

I've developed a bit of an obsession with the
[Enlightenment](http://jeffhoogland.blogspot.com/search/label/enlightenment)
desktop of the late. Even though this desktop is fantastic there are
currently not very many distributions that utilize it. Today I would
like to take the time to mention those that offer a version with my
favorite desktop.

[](http://www.pclinuxos.com/)
<span style="font-weight: bold;">[PCLinuxOS](http://www.pclinuxos.com/):</span>

![](misc/hoogland/pclinuxos.png)

The biggest name distribution to (currently) offer an "official"
Enlightenment variation is PCLinuxOS. PCLOS is an RPM based distribution
that uses apt-get for its package manager. As this is an official
PCLinuxOS variation it is fully compatible with and uses the standard
PCLinuxOS repositories. PCLinuxOS E17 currently has the beta 3 EFL
packages in its repositories and stays regularly up to date.

<span style="font-weight: bold;">[Sabayon](http://www.sabayon.org/):</span>

![](misc/hoogland/sabayon.png)

Sabayon is a derivative of [Gentoo](http://www.gentoo.org/) that is
fully backwards compatible. They recently released an "experimental"
spin that utilizes the Enlightenment desktop. This spin is based on the
latest version of Sabayon. You can find more information on it
[here](http://forum.sabayon.org/viewtopic.php?f=60&t=22208&start=0) .

<span style="font-weight: bold;">[Bodhi
Linux](http://bodhilinux.com/):</span>

![](misc/hoogland/bodhi.png)

Bodhi Linux is an Ubuntu derivative that uses 10.04 as a base, but back
ports newer software from Maverick and even Natty repositories. It
receives regular Enlightenment updates and currently features the EFL
beta 3 libraries. It is fully backwards compatible with Ubuntu 10.04
packages.

<span style="font-weight: bold;">[Macpup](http://macpup.org/):</span>

![](misc/hoogland/macpup.png)

Macpup is a distro based on [Puppy Linux](http://www.puppylinux.org/) .
Macpup's latest release is based on "Lucid Puppy" a version of Puppy
that is compatible with binaries made for Ubuntu 10.04. Macpup ships
with ELF beta 1 version of the Enlightenment files compiled from SVN
revision 52995. Something worth noting is that Macpup feels a bit
incomplete as certain Enlightenment features do not work (such as
shutting down). Finally I'd like to note that if you want to update
Enlightenment under Macpup you will need to compile and install the
updates yourself from source.

<span style="font-weight: bold;">[Unite17](http://www.unity-linux.hu/unite17/):</span>

![](misc/hoogland/unite17_2010_02.png)

Unite17 is the first derivative built on top of [Unity
Linux](http://unity-linux.org/), which in turn is a derivative of
[Mandriva](http://www2.mandriva.com/en/) . Unite17 is a Hungarian
Enlightenment distribution that has a large default application set.
Unite17 was formly known as PCe17OS.

[<span style="font-weight: bold;">Elive</span>](http://jeffhoogland.blogspot.com/2010/03/elive-20-distro-review.html)
:

![](misc/hoogland/elive.png)

Elive is based on Debian stable (5.0), over time it has become what I
believe to be kind of the defacto standard of Enlightenment
distributions. While it does work well and is more than elegant all of
its packages are fairly old. Even its Enlightenment packages are dated
at this point, its last release was over nine months ago.

<span style="font-weight: bold;">[Pentoo](http://www.pentoo.ch/):</span>

![](misc/hoogland/pentoo.png)

Pentoo gets its name from its Gentoo base and its function for network
penetration testing. It utilizes Enlightenment more for its speed than
for its elegance. It is backwards compatible with Gentoo, but its last
release is over a year old at this point.

[MoonOS](http://moonos.org/) :

![](misc/hoogland/moonos.png)

MoonOS is an Ubuntu derivative whose last release used 9.04 as a base.
It is important to note that MoonOS no longer receives updates as 9.04's
life span has run out. Still, it is nice looking and if you are willing
to compile your own Enlightenment updates it can work just fine as an OS
(after upgrading Ubuntu versions). This 9.04 version is the last copy of
MoonOS to use the Enlightenment desktop, an announcement was posted that
future versions will be using the Gnome desktop.

<span style="font-weight: bold;">[YellowDog](http://www.yellowdoglinux.com/):</span>

![](misc/hoogland/Yellow-Dog-Linux_1.jpg)

Yellow Dog is an Enlightenment distro that works on PowerPC and Cell
processor architectures. It is designed for home use (on the PS3/PPCs
Macs) and server use/cluster computing. It is built on top of the
community version of Redhat Linux known as CentOS and is maintained by
the company Fixstars.

<span style="font-weight: bold;">[OpenGEU](http://opengeu.intilinux.com/):</span>

![](misc/hoogland/opengeu.png)

OpenGEU is another Ubuntu derivative (there are always lots of those)
whose most recent version is built on 8.10. It has been a long while
since we saw any new releases from the OpenGEU team, but they promise us
a Debian based release at some point in the future.

I think I covered most all the Enlightenment based Linux distributions -
really shows how few there are that I can list them all like this in one
post\! If I missed anything please drop a comment below to let me know.
