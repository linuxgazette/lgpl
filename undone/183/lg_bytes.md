# News Bytes

**By [Deividson Luiz Okopnik](../authors/dokopnik.html) and [Howard
Dyckoff](../authors/dyckoff.html)**

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><img src="../gx/bytes.gif" alt="News Bytes" /></td>
<td><h3 id="contents">Contents:</h3>
<ul>
<li><a href="lg_bytes.html#general">News in General</a></li>
<li><a href="lg_bytes.html#Events">Conferences and Events</a></li>
<li><a href="lg_bytes.html#distro">Distro News</a></li>
<li><a href="lg_bytes.html#commercial">Software and Product News</a></li>
</ul></td>
</tr>
</tbody>
</table>

**Selected and Edited by [Deividson
Okopnik](mailto:bytes@linuxgazette.net)**

Please submit your News Bytes items in **plain text**; other formats may
be rejected without reading. \[You have been warned\!\] A one- or
two-paragraph summary plus a URL has a much higher chance of being
published than an entire press release. Submit items to
<bytes@linuxgazette.net>. Deividson can also be reached via
[twitter](http://www.twitter.com/deivid_okop).

-----

## News in General

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)Debian 6.0 Squeeze to have Completely Free Linux Kernel

The Debian community is inching toward the full release of a major
update of its Linux distro that will be a major milestone for the Linux
Community. That release has been rescheduled to February 5th or 6th and
should include a fully open source distro with earlier, optional
non-free elements in a separate branch.

The Debian project has been working on removing non-free firmware from
the Linux kernel shipped with Debian for its past two release cycles but
it was not yet possible to ship Linux kernels without non-free firmware
bits - until now.

(See the full text in the Distro Section, below)

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)CLUG's Survey

The CLUG (Calgary Linux User Group) is aplying a survey to gather some
data on Linux User Groups across North America, aiming to improve the
user experience.

"We've made a survey for Linux User Group members across North America.
We would like to get the pulse of different groups and see what's
working so that all LUG's can improve the experience. This data will be
published on <http://clug.ca> (the Calgary Linux User Group website) in
August, 2011. The survey is only 12 questions, and shouldn't take more
than 5 minutes to complete" said [Dafydd
Crosby](../authors/crosby.html).

So, if you have a few minutes to spare, take part in the survey, which
can be found here:
<http://www.lonesomecosmonaut.com/limesurvey/index.php?sid=94921&newtest=Y&lang=en>.

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)LinuxCon goes Embedded and to Europe in 2011

The Linux Foundation has announced the expansion of its leading Linux
technical conference, LinuxCon, to Europe. LinuxCon Europe will take
place October 26-28, 2011, in Prague, Czech Republic, and will be
co-located with the Linux Kernel Summit, the GStreamer Conference, and
Embedded Linux Conference Europe.

In addition, both LinuxCon US and LinuxCon Europe will be enriched by
the addition of the Embedded Linux Conference (ELC) and ELC Europe as a
result of last year's merger between The Linux Foundation and the
Consumer Electronics Linux Forum.

The Embedded Linux Conference, now in its sixth year, is dedicated
exclusively to embedded Linux and embedded Linux developers.

A Call for Papers (CFP) is now open for the Collaboration Summit, the
Embedded Linux Conference, and all other LinuxCon 2011 events. Anyone in
the Linux community can submit presentation materials to
<http://events.linuxfoundation.org>.

-----

## Conferences and Events

  - **Cisco Live, London**  
    January 31 - February 3, 2011  
    <http://www.ciscolive.com/europe/>

<!-- end list -->

  - **O'Reilly Strata Conference**  
    Feb 1-3, 2011, Santa Clara, CA  
    <http://strataconf.com/strata2011>  
    30% off with code str11usrg

<!-- end list -->

  - **Scrum Gathering Lisbon 2011**  
    Feb 10-11, Hotel Olissippo Oriente, Lisbon  
    <http://www.scrumalliance.org/events/269-portugal>

<!-- end list -->

  - **1st Mobile Security Symposium**  
    Feb 14, 2011, W Hotel, San Francisco, CA  
    <http://www.mobilesecuritysymposium.com/index.php>

<!-- end list -->

  - **20th RSA Conference - 2011**  
    Feb 14-18, Moscone Center, San Francisco  
    Save $400, register by January 14, 2011  
    <http://www.rsaconference.com/2011/usa/index.htm>

<!-- end list -->

  - **FAST '11 USENIX Conference on File and Storage Technologies**  
    Sponsored by USENIX in cooperation with ACM SIGOPS  
    Feb 15-18, 2011, San Jose, CA  
    <http://www.usenix.org/events/fast11/>

<!-- end list -->

  - **SCALE 9x - 2011 Southern California Linux Expo**  
    Feb 25-27, Airport Hilton, Los Angeles, CA  
    <http://www.socallinuxexpo.org/scale9x/>

<!-- end list -->

  - **Enterprise Connect (formerly VoiceCon)**  
    Feb 28-Mar 3, 2011, Orlando, FL  
    <http://www.enterpriseconnect.com/orlando/?_mc=CNZMVR07>

<!-- end list -->

  - **Linux Foundation End User Summit**  
    March 1-2, 2011, Hyatt Jersey City/Jersey City, NJ  
    <http://events.linuxfoundation.org/events/end-user-summit>

<!-- end list -->

  - **AnDevCon: Android Developer Conference**  
    March 7-9, San Francisco, CA  
    <http://andevcon.com/>  
    Save $300 if you register by Jan 14th, $100 more for NPOs and
    Government organizations

<!-- end list -->

  - **Cisco Live, Melbourne**  
    March 29 - April 1, 2011  
    <http://www.cisco.com/web/ANZ/cisco-live/index.html>

<!-- end list -->

  - **NSDI '11 USENIX Symposium on Networked Systems Design and
    Implementation**  
    Sponsored by USENIX with ACM SIGCOMM and SIGOPS  
    March 30-April 1, 2011, Boston, MA  
    <http://www.usenix.org/events/nsdi11/>

<!-- end list -->

  - **Linux Foundation Collaboration Summit 2011**  
    April 6-8, Hotel Kabuki, San Francisco, CA  
    <http://events.linuxfoundation.org/events/collaboration-summit>

<!-- end list -->

  - **Embedded Linux Conference 2011**  
    April 11-13, Hotel Kabuki, San Francisco, CA  
    <http://events.linuxfoundation.org/events/embedded-linux-conference>

<!-- end list -->

  - **MySQL Conference & Expo**  
    April 11-14, Santa Clara, CA  
    <http://en.oreilly.com/mysql2011/>

<!-- end list -->

  - **Ethernet Europe 2011**  
    April 12-13, London, UK  
    <http://www.lightreading.com/live/event_information.asp?event_id=29395>

<!-- end list -->

  - **O'Reilly Where 20 Conference**  
    April 19-21, 2011, Santa Clara, CA  
    <http://where2conf.com/where2011>

<!-- end list -->

  - **Lean Software and Systems Conference 2011**  
    May 3-6, 2011, Long Beach, CA  
    <http://lssc11.leanssc.org/>

<!-- end list -->

  - **Red Hat Summit and JBoss World**  
    May 3-6, 2011, Boston, MA  
    <http://www.redhat.com/promo/summit/2010/>

<!-- end list -->

  - **USENIX/IEEE HotOS XIII - Hot Topics in Operating Systems**  
    May 8-10, Napa, CA  
    <http://www.usenix.org/events/hotos11/>

<!-- end list -->

  - **Google I/O 2011**  
    May 10-11, Moscone West, San Francisco, CA  
    Search for conference registration in March or April

<!-- end list -->

  - **OSBC 2011 - Open Source Business Conference**  
    May 16-17, Hilton Union Square, San Francisco, CA  
    <http://www.osbc.com>

<!-- end list -->

  - **Scrum Gathering Seattle 2011**  
    May 16-18, Grand Hyatt, Seattle, WA  
    <http://www.scrumalliance.org/events/285-seattle>

<!-- end list -->

  - **RailsConf 2011**  
    May 16-19, Baltimore, MD  
    <http://en.oreilly.com/rails2011>

-----

## Distro News

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)Debian 6.0 Squeeze to have Completely Free Linux Kernel

Debian is inching toward the full release of a major update of its Linux
distro that will be a major milestone for the Linux Community. That
release has been rescheduled to February 5th or 6th and should include a
fully open source distro with earlier, optional non-free elements in a
separate branch.

The Debian project has been working on removing non-free firmware from
the Linux kernel shipped with Debian for its past two release cycles but
it was not yet possible to ship Linux kernels without non-free firmware
bits - until now.

In December, they announced that all issues were fundamentally solved
and they will be able to deliver a Linux kernel which is completely
'Free', according to the Debian Free Software Guidelines (DFSG), with
Debian "Squeeze" or version 6.0.

Recognizing that some users may require the use of works that do not
conform to the DFSG and might include non-free firmware bits, for the
time being, there will be a non-free area in the archives for
alternative installation images and additional packages for Debian
Squeeze that include non-free firmware bits needed for specific pieces
of hardware. These elements cannot and will not be supported to the same
extent as the Free firmware.

This is the first Debian release that supports the Haskell Platform, in
version 2010.1, including the ghc compiler in version 6.12.1, and ships
more than 200 additional Haskell libraries. This version of Debian Linux
will also sport a graphical installer.

As of mid-January, there were 4 blocker bugs for the release of Squeeze,
but all of them are fixed in the unstable/sid release.

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)New Release of OpenIndiana, an Open Source Solaris

December saw a new release of OpenIndiana, version oi\_148. This version
may shortly form the basis for the project's first stable,
production-ready release.

OpenIndiana continues the OpenSolaris legacy and aims to be binary and
package compatible with Oracle Solaris 11 and Solaris 11 Express.

Version oi\_148 fixes Java SSL crypto problems, adds PostgreSQL packages
back in, and has admin improvements. Since several users of this version
and oi\_147 are applying it to production servers, the project community
is considering applying several package fixes and security updates to
oi\_148.

The release would be planned for February or March with a limited set of
server-oriented packages and may be called "Open Indiana 2011.02". It
would be based on oi\_148 with only the Text Installer and Automated
Installer ISOs. To make the release easier to manage, versions of
Apache, PHP, MySQL, Tomcat, etc, would match the same versions shipped
in RHEL 6/CentOS 6.

From the September 2010 announcement on the forming of the OpenIndiana
project:

"OpenIndiana is a brand new distribution of OpenSolaris, constructed by
the community, for the community. Our primary goal is to be a binary and
package compatible drop in replacement for the official OpenSolaris and
forthcoming Solaris 11 and Solaris 11 Express releases."

"This project was created to address the longstanding issue that the
OpenSolaris distribution has constructed entirely by Sun/Oracle, not by
the community. Depending on a single commercial entity has led to
several issues, notably that bug fixes and security updates are only
available via a paid for support subscription, that community
participation has been limited in steering the direction of the
operating system, and that commercial decisions have led to stricter
licensing terms."

The full text is here: <http://wiki.openindiana.org/oi/Press+Release>.

-----

## Software and Product News

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)Zend Studio 8.0 PHP IDE Released

Zend Studio 8.0 Streamlines PHP Application Development in VMware
Virtualized Environments and Features Enhanced Support for Building Ajax
and PHP Applications.

In November, Zend Technologies announced the general availability of
Zend Studio 8.0, its leading Integrated Development Environment (IDE)
for professional PHP developers. The new release helps PHP developers
streamline development and testing processes through integration with
VMware Workstation and revamped support for servers running on-premise
or in the cloud. Zend Studio 8.0 also delivers extensive support for the
development of rich Internet applications that use JavaScript front-ends
and PHP back-ends.

Zend provides ready-to-use virtual images for Zend Server to further
optimize the debugging experience. A new pricing model was also
announced for Zend Studio 8.0, making it accessible to more developers.

"VMware is committed to increasing the productivity of software
development organizations and evolving application and data delivery
environments into a user-centric model," said Parag Patel, vice
president, Global Strategic Alliances at VMware. "Our partnership with
Zend brings the full benefits of virtualization to PHP developers,
enabling them to accelerate software development times and reduce
hardware costs."

Developing PHP code on a remote server running on-premise or in the
cloud is significantly easier in Zend Studio 8.0, with its simplified
workflow and improved performance. SSH or FTP connections can be used to
import projects into Zend Studio, and then export them back to the
remote server to be run or debugged.

Zend Studio 8.0 enables faster coding of PHP and JavaScript applications
through the built-in Content Assist support for many JavaScript-based
libraries including jQuery, Dojo, Ext JS and Prototype. A new set of
integrated Ajax tools simplifies debugging of PHP and JavaScript
applications by supporting debugging of the JavaScript front-end and the
PHP back-end in one combined session.

For more information about the new features and functionality in Zend
Studio 8, please visit <http://www.zend.com/products/studio>.

