# The Foolish Things We Do With Our Computers

**By [Ben Okopnik](../authors/okopnik.html)**

"Foolish Things" is a now-and-again compilation we run based on our
readers' input; once we have several of these stories assembled in one
place, we get to share them with all of you. If you enjoy reading these
cautionary tales of woe, proud stories of triumph, and just plain weird
and fun things that happen between humans and silicon, that's great; if
you have some to share so that others may enjoy them, even better.
Please send them to <editor@linuxgazette.net>.

You can even tell us that it happened to A Friend of Yours, and we'll
believe you.

\-- Ben

-----

### Copy and Paste, Repent at Leisure

**Paul Hernaus**  

This reminds me of a disaster I created.

I was working on a Sun Sparc, running Solaris and was trying to get a
program to work. The main problem was to get the LD\_LIBRARY\_PATH set
to a value so that all shared objects could be resolved.

Of course, 'ldd' is your friend. Now look for a moment at the output of
ldd on Solaris. Unfortunately I don't have a Solaris system at hand, but
it would look something like this (fortunately on Linux the output is
less dangerous):

``` 
   > /lib/libc.so libc.so
   > /usr/lib/libwhatever.so libwhatever.so
```

etc. I'm sure this is not completely accurate.

So, I had an xterm full of output like that. Then I wanted to grab the
mouse, and had an unfortunate motion control problem: I moved the mouse
and by accident hit the left mouse button and immediately after that the
middle mouse button. Result: I copied and then pasted a large part of
the xterm contents into my (root) shell. Immediately afterward I got a
message saying "session closed by peer" or something along those
lines...

***Oops...***

I thought I should share this with you. I can now laugh about it, but
when it happened it wasn't funny.

### B\*\*\*\*, What's My Name???

**Ben Okopnik**  

This one happened a while ago. I still blush when I think of it - and
I'm not the blushing type. I'm reminded of it now because I am again
doing a backup and a distro upgrade - but this time, having learned my
lesson, I'm doing it the right way.

I was about to upgrade the distro on my laptop, and being a good little
Linux hacker, I first ran a backup to an external USB drive. Had to let
it run overnight - "USB" still meant "very slow" back then - but in the
morning, it was done. I wandered over to the machine,
half-asleep<span id="lg_foolish.html_1_back"></span>[\[1\]](lg_foolish.html#lg_foolish.html_1),
plugged in a USB dongle, and fired up the program that would install a
bootable version of the distro on the dongle. Yeah, choose the ISO file,
OK... oh, look, the USB device is already selected, great... says that
it needs formatting - OK, click "Format this device"...

At which point, my brain caught up with my fingers. Um... oh-oh. The
name of the USB device that was listed in the one-line window was that
of the backup drive. The dongle was there too - but it was *below that
window*, where you'd have to scroll down to see it. I had just formatted
the backup drive that I had spent all night filling up.

No, 'gpart' and friends were of no help. Not only did the partition info
get wiped out, but the drive had also been reformatted to VFAT - since
that's what was used for setting up the bootable distro. A few moments
of half-awake typing was all that was necessary to scrap a night's
backup.

These days, whenever I'm doing any disk formatting or any similar
operations, I 1) unplug any unnecessary storage devices, 2) **always**
triple-check the device name, and 3) make sure that I'm fully awake and
focused on the task. Certain things really do require all your
attention.
