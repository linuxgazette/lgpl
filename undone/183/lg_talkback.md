# Talkback

<span id="talkback_181_lg_bytes"></span>

### Talkback:181/lg\_bytes.html

****

Anderson Silva \[afsilva at gmail.com\]

  
**Thu, 2 Dec 2010 08:33:21 -0500**

I missed the news about red hat enterprise linux 6 being released on Nov
10.

AS

**\[ <span id="mb-talkback_181_lg_bytes"></span> [Thread continues here
(5 messages/5.84kB)](misc/lg/talkback_181_lg_bytes.html) \]**

-----

<span id="talkback_182_crawley"></span>

### Talkback:182/crawley.html

****

Jim Jackson \[jj at franjam.org.uk\]

  
**Fri, 7 Jan 2011 12:04:19 +0000 (GMT)**

Hi gang,

I've just read Don's article and think there is a sshd\_config option
omitted. Surely in section 9 one needs

PasswordAuthentication no

as well? Enabling PubkeyAuthentication just adds an extra way of
logining in.

cheers Jim

**\[ <span id="mb-talkback_182_crawley"></span> [Thread continues here
(8 messages/7.43kB)](misc/lg/talkback_182_crawley.html) \]**
