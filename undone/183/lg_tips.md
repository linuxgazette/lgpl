# 2-Cent Tips

### 2-Cent Tip: Making space while installing

****

Chris Bannister \[mockingbird at earthlight.co.nz\]

  
**Mon, 6 Dec 2010 02:11:52 +1300**

On Tue, Nov 30, 2010 at 06:30:23PM -0500, Ben Okopnik wrote:

    > Date: Sun, 28 Nov 2010 11:22:04 +0100
    > From: Jacopo Sadoleto <jacopo.sadoleto at gmail.com>
    > To: ben at linuxgazette.net
    > Subject: 2-Cent Tips
    > 
    > Sometimes upgrading an Linux distro to a recent version, yields an "No space
    > left on device" (usually /var);
    > For those not willing to use a tool to create, resize, grow a partition, a
    > simple artifice will do the trick:
    > 
    > CTRL-ALT-F2

Upgrading from within X should be discouraged.

    > # mv /var /home/user/? <== or wherever space suffice
    > # cd /
    > # ls -s /home/user/var .
    > CTRL-ALT-F7

I believe that would just be ALT-F7

    -- 
    "Religion is excellent stuff for keeping common people quiet."
       -- Napoleon Bonaparte

**\[ <span id="mb-2_cent_tip__making_space_while_installing"></span>
[Thread continues here (8
messages/9.47kB)](misc/lg/2_cent_tip__making_space_while_installing.html)
\]**
