# Away Mission - RSA 2011

**By [Howard Dyckoff](../authors/dyckoff.html)**

As usual, the annual RSA Conference is arguably the most comprehensive
one-stop forum in information security. Although there are many
conferences that are more technical, the breadth of the sessions and the
caliper of some speakers makes this a not-to-miss event for those
interested in the security of computer systems and networks. And, yes,
RSA has moved from March in recent years to February 14-18 this year.
Happy Valentines Day, hackers and crackers.

There is a lot to do on RSA Monday - besides getting chocolate for your
significant other - and most of it is free with an Expo Pass. And many
attending vendors will happily send you an Expo registration code. I
have one listed below from Fortify. With the Expo Pass, the keynotes and
Town Hall events are available as well as several vendor-sponsored
sessions. So come for Monday and come back if you can. There are many
rewards for being a full attendee, or delegate, but the full freight
will be $2,195\! A Delegate One-Day Pass (Tue-Fri) costs a mere $995.
See, Monday is a deal.

So what's included?? First off, there is the all morning Cloud Security
Alliance Summit 2011. And in the afternoon there is the Innovation
Sandbox covering the best new security solutions - this actually
requires a Delegate Pass or an Expo Plus registration. But there's more.

That same afternoon, there are the Corporate Research Labs, which run
from 2-4 p.m. You can join interactive discussions from CA and RSA Labs
researchers on recent security technology. And the Trusted Computing
Group returns again for its own set of presentations from 11-3, with
lunch included. This half-day workshop will discuss the role of trust
with hardware-based security - the now-common Trusted Platform Module
(TPM).

After that, you can attend the Trailbreakers Panel which celebrates
those technology leaders who weren't afraid to buck the status quo and
introduce innovative approaches that overcame mediocrity. Dr. Hugh
Thompson, Chief Security Strategist, People Security, and RSA Conference
Program Committee Chairman, will moderate the discussion of technology
that can change the world as we know it. And guess who is on the panel?
HD Moore, Chief Security Officer at Rapid7 and Chief Architect of
Metasploit.

If that wasn't enough, Fortify has a vendor code to get a free Expo Pass
and is also hosting its own all-day developer systems security
conference. But you have to register by Feb 11, the Friday before RSA.
No pre-registration is required for the Fortify Developer Seminar. To
register for your complimentary Expo pass (a $100 value), enter code
SC11FTS upon RSA Expo registration check out.

If you want to know more about RSA Monday, check these links:  
<http://www.rsaconference.com/2011/usa/agenda/mondayevents.htm>  
<https://365.rsaconference.com/community/connect/innovation-sandbox>

For each RSA event since 1995, a unique theme has highlighted a
significant historical contribution to cryptography, mathematics or
information security. This spans from World War II Navaho Codetalkers to
the Chinese Remainder Theorem. For 2010, it was the influence of the
Rosetta Stone - literally code cracking. This year, its about security
roles and Identity, with the longish theme name of The Adventures of
Alice & Bob. Ron Rivest of the RSA algorithm fame used these as
placeholder names to explain the RSA encryption method and the steps
involved in the complex system. Alice & Bob were born to make the
subject matter easier to grasp - replacing Person A and Person B. Over
the years other characters have been added to make technical topics more
understandable. This cast of friends and enemies - including Eve the
Eavesdropper, Mallory the Malicious Attacker and Walter the Warden,
among others - populate Alice & Bob's universe and are now common
parlance in cryptography and computer security.

You can review several podcasts of interesting RSA sessions at your
leisure. The address for the 2010 previews is
<https://365.rsaconference.com/community/connect/rsa-conference-usa-2010?view=video>
- and out of that list, I'd suggest listening to Mark Risher, Sr.
Director or Product Management at Yahoo, on his HT1-301 session: "Yokai
vs. the Elephant: Hadoop and the Fight Against Shape-Shifting Spam",
which is about discovering polymorphic spam. You can find that one here:
<https://365.rsaconference.com/videos/1009;jsessionid=D39EC9C6A573499321B59ACEC09859DB.node0>

Another memorable moment was Bruce Schneier's reflections on RSA
Conference via an RSA Conference Bingo card of security problems, like
witnessing WiFi sniffing. Here's the full card to enjoy:
<https://365.rsaconference.com/servlet/JiveServlet/previewBody/2414-102-1-3050/Bruce%20Schneier%20RSA%20Conference%20Bi>

This address is for this year's RSA 2011 Preview Podcasts:
<https://365.rsaconference.com/community/connect/rsa-conference-usa-2011?view=video>
This will give you a taste of the upcoming sessions.

### Keynotes

Among several keynote sessions for 2011, William Lynn, Deputy Secretary
of Defense, will be speaking on "Defending a New Domain: The Pentagon's
Cyber Strategy." James Lewis, Director at the Center for Strategic and
International Studies, will be moderating a panel on "Cyberwar,
Cybersecurity, and the Challenges Ahead." And, on the last day of RSA,
42nd President Bill Clinton will be speaking.

The Cryptographers Panel, following the first keynote of RSA 2010,
featured crypto legends Ron Rivest and Adi Shamir (the R and the S of
RSA fame) and also Whitfield Diffie and Martin Hellman (of
Diffie-Hellman fame in Public Key Exchange). They talked about the
recent history of cryptography and current challenges, also sharing some
insight into the relationship between academic research and NSA
capabilities.

For the 2010 RSA Keynotes and Industry Panels, go here:  
<http://www.rsaconference.com/2010/usa/recordings/catalog.htm?utm_source=linkedin&utm_medium=email&utm_campaign=connectmar30>

There are 17 2010 Keynote sessions posted plus 14 Industry sessions
posted, but some of them are videos of full track sessions.
Unfortunately, you need 2010 Delegate credentials to view the full
sessions. But do check out the links.

Among my favorite sessions over the previous several years were
technical sessions led by engineers from Mandiant and technical sessions
led by instructors from SANS course. The latter group led a mini-class
and Q/A session on advanced hacking techniques. This later SANS session
was led by SANS faves Ed Skoudis and Johannes Ulrich. Skoudis noted that
the best anti-virus software was falling to under 80 percent detection
for the newer polymorphic malware and recommended using both signature
and behaviour-based detection for better results. He also described a
newly detected vulnerability in SSL key renegotiation that required
patching in all OSes. I believe everyone learned something and the
applause at the end was long and sincere. I'd recommend going to any
session with speakers from SANS or Mandiant.

If you make to this year's RSA, keep these sessions in mind. And bring
your own chocolate.

Something to keep in mind for March is the Linux Foundation End User
Summit, March 1-2, in Jersey City, NJ. See
<http://events.linuxfoundation.org/events/end-user-summit> for more
details.

  
