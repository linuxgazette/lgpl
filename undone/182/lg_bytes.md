[![Linux
Gazette](../gx/2003/newlogo-blank-200-gold2.jpg)](https://linuxgazette.net/)

...making Linux just a little more fun\!

<div id="navigation">

[Home](https://linuxgazette.net/index.html) [Main
Site](http://linuxgazette.net) [FAQ](../faq/index.html) [Site
Map](../lg_index.html) [Mirrors](../mirrors.html)
[Translations](../mirrors.html)
[Search](https://linuxgazette.net/search.html)
[Archives](../archives.html)
[Authors](https://linuxgazette.net/authors/index.html) [Mailing
Lists](http://lists.linuxgazette.net/listinfo.cgi) [Join
Us\!](../jobs.html) [Contact Us](../contact.html)

-----

</div>

The Free International Online Linux Monthly

ISSN: 1934-371X

Main site: <http://linuxgazette.net>

<div id="breadcrumbs1">

[Home](https://linuxgazette.net/index.html) \> [January 2011
(\#182)](index.html) \> Article

</div>

<div class="articlecontent1">

<div class="content">

<div id="previousnexttop">

[next --\>](brownss.html)

</div>

# News Bytes

**By [Deividson Luiz Okopnik](../authors/dokopnik.html) and [Howard
Dyckoff](../authors/dyckoff.html)**

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><img src="../gx/bytes.gif" alt="News Bytes" /></td>
<td><h3 id="contents">Contents:</h3>
<ul>
<li><a href="lg_bytes.html#general">News in General</a></li>
<li><a href="lg_bytes.html#Events">Conferences and Events</a></li>
<li><a href="lg_bytes.html#distro">Distro News</a></li>
<li><a href="lg_bytes.html#commercial">Software and Product News</a></li>
</ul></td>
</tr>
</tbody>
</table>

**Selected and Edited by [Deividson
Okopnik](mailto:bytes@linuxgazette.net)**

Please submit your News Bytes items in **plain text**; other formats may
be rejected without reading. \[You have been warned\!\] A one- or
two-paragraph summary plus a URL has a much higher chance of being
published than an entire press release. Submit items to
<bytes@linuxgazette.net>. Deividson can also be reached via
[twitter](http://www.twitter.com/deivid_okop).

-----

<div id="news">

<span id="general"></span>

## News in General

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)Russia's Prime Minister Signs Order to Move to Open Source

As reported in Glyn Moody's
[Open](http://opendotdotdot.blogspot.com/2010/12/putin-orders-russian-move-to-gnulinux.html)
blog, Prime Minister Vladimir Putin has ordered Russian government
agencies to switch to open source software by 2015. The transition is
due to begin during the second quarter of 2011. The Russian government
also plans to build a unified national repository for open source
software.

According to Moody, previous attempts to move parts of the Russian
government to open source software had experienced a lukewarm reception
due to a lack of political support. However, he writes, *"if Putin says:
'make it so,' I suspect that a lot of people will jump pretty fast to
make sure that it \*is\* so. \[...\] once that happens, other plans to
roll out free software might well suddenly look rather more
attractive."*

The [order](http://filearchive.cnews.ru/doc/2010/06/17/2299p.doc)
mandates a broad set of related changes in education at all levels,
including professional training for federal and civilian employees. It
also covers a number of specific directives for the ministries of
health, science, and communications, and specifies the implementation of
"open source operating systems, hardware drivers, and programs for
servers and desktop use."

The Business Software Alliance, a trade group representing large U.S.
software vendors, estimates that 67 percent of software used in Russia
in 2009 was pirated.

\[ Given that the net effect of "pirated" software is, and has always
been, a positive benefit to the members of the Alliance - no actual cost
for the software distribution, and a large pool of IT people trained in
the use of their platforms and products - it's interesting to speculate
on the eventual result of these changes with regard to those members. It
seems that trading on that particular "pity bait" is coming to an abrupt
end...  
\-- Ben Okopnik \]

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)Linux Foundation's Annual 'Who Writes Linux' Study Released

The Linux Foundation has published its annual report on Linux kernel
development, detailing who does the work, who sponsors it and how fast
the Linux kernel is growing.

As Amanda McPherson states in her blog, this paper documents how hard at
work the Linux community has been. There have been 1.5 million lines of
code added to the kernel since the 2009 update. Since the last paper,
additions and changes translate to an amazing 9,058 lines added, 4,495
lines removed, and 1,978 lines changed every day, weekends and holidays
included.

A significant change this year was the increasing contributions by
companies and developers working in the mobile and embedded computing
space. For more information on the study, see the blog entry here:
<http://www.linuxfoundation.org/news-media/blogs/browse/2010/12/our-annual-kernel-development-report-new-and-old-faces>.

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)Novell Agrees to be Acquired by Attachmate Corporation

In November, Novell entered into a definitive merger agreement under
which Attachmate Corporation would acquire Novell for $6.10 per share in
cash, which was valued at approximately $2.2 billion. Attachmate
Corporation is owned by an investment group led by Francisco Partners,
Golden Gate Capital and Thoma Bravo. Novell also entered into a
definitive agreement for the concurrent sale of certain intellectual
property assets to CPTN Holdings LLC, a consortium of technology
companies organized by Microsoft Corporation, for $450 million in cash.
That sale of IP assets has raised some concerns in the Open Source
community.

Attachmate Corporation plans to operate Novell as two business units:
Novell and SUSE; and will join them with its other holdings, Attachmate
and NetIQ.

The $6.10 per share consideration represents a premium of 28% to
Novell's closing share price on March 2, 2010, and a 9% premium to
Novell's closing stock price on November 19, 2010. Since the merger was
announced, 2 shareholder suits have been filed questioning the deal.

"After a thorough review of a broad range of alternatives to enhance
stockholder value, our Board of Directors concluded that the best
available alternative was the combination of a merger with Attachmate
Corporation and a sale of certain intellectual property assets to the
consortium," said Ron Hovsepian, president and CEO of Novell. "We are
pleased that these transactions appropriately recognize the value of
Novell's relationships, technology and solutions, while providing our
stockholders with an attractive cash premium for their investment."

There was some concern in the blogosphere that this patent portfolio
might strengthen Microsoft's hand with regard to Linux. However, many
are expected to reflect on Novell's Office and collaboration products.

Groklaw's November post Novell Sells Some IP to a MS-Organized
Consortium asks "...so what goes to Microsoft's consortium? No doubt
we'll find out in time. It is being reported that what it will get is
882 patents. Blech. How many does Novell own? Is that all of them? If
so, will we get to watch Son of SCO, but with patents this time? But
keep in mind that the WordPerfect litigation could be in this picture,
and I wonder if this could be a kind of deal to tactfully settle it out,
with Microsoft paying to end it this way?"

Groklaw also notes that prior contracts and promises made by Novell will
probably remain in force: "As for the question of what happens to prior
promises Novell made, if they are contractual, you go by the contract.
In a stock for stock merger, I'm told by a lawyer, all obligations
remain in force. In an asset sale, the two negotiate who gets what. But
if the buyer \*takes over\* a contract, then they have to honor all of
the terms of the contract, such as a patent license or cross license."

Attachmate Corporation's acquisition of Novell is subject to customary
closing conditions and is expected to close in the first quarter of
2011.

-----

<span id="links"></span>

## Conferences and Events

  - **CES 2011**  
    Jan 6-9, Las Vegas, NV.  
    <http://www.cesweb.org/>.

<!-- end list -->

  - **MacWorld Expo and Conference**  
    Jan 25-29, Moscone Center, San Francisco, CA  
    <http://www.macworldexpo.com>.

<!-- end list -->

  - **20th RSA Conference - 2011**  
    Feb 14-18, Moscone Center, San Francisco  
    Save $400, register by January 14, 2011  
    <http://www.rsaconference.com/2011/usa/index.htm>.

<!-- end list -->

  - **FAST '11 USENIX Conference on File and Storage Technologies**  
    Sponsored by USENIX in cooperation with ACM SIGOPS  
    Feb 15-18, 2011, San Jose, Ca  
    <http://www.usenix.org/events/fast11/>.

<!-- end list -->

  - **SCALE 9x - 2011 Southern California Linux Expo**  
    Feb 25-27 , Airport Hilton, Los Angeles, Ca  
    <http://www.socallinuxexpo.org/scale9x/>.

<!-- end list -->

  - **Enterprise Connect (formerly VoiceCon)**  
    Feb 28-Mar 3, 2011, Orlando, FL  
    <http://www.enterpriseconnect.com/orlando/?_mc=CNZMVR07>.

<!-- end list -->

  - **NSDI '11 USENIX Symposium on Networked Systems Design and
    Implementation**  
    Sponsored by USENIX with ACM SIGCOMM and SIGOPS  
    March 30-April 1, 2011, Boston, MA  
    <http://www.usenix.org/events/nsdi11/>.

<!-- end list -->

  - **Linux Foundation Collaboration Summit 2011**  
    April 6-8, Hotel Kabuki, San Francisco, CA  
    <http://events.linuxfoundation.org/events/collaboration-summit>.

<!-- end list -->

  - **MySQL Conference & Expo**  
    April 11-14, Santa Clara, CA  
    <http://en.oreilly.com/mysql2011/>.

<!-- end list -->

  - **Ethernet Europe 2011**  
    April 12-13, London, UK  
    <http://www.lightreading.com/live/event_information.asp?event_id=29395>.

<!-- end list -->

  - **O'Reilly Where 2.0 Conference**  
    April 19-21, 2011, Santa Clara, CA  
    <http://where2conf.com/where2011>.

-----

<span id="distro"></span>

## Distro News

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)Red Releases RHEL 6

In mid-November, Red Hat Enterprise Linux 6 was delivered with over
three years' worth of customer-focused product innovation for advancing
data-center efficiency.

In a blog post, Tim Burke, Vice President, Linux Development at Red Hat
stated:

"In my development team we feel a tremendous sense of pride in that we
have the privilege of being at a confluence point of technology
innovation and customer need fulfillment. This gives us a full
life-cycle of gratification in that we get to build it, test it, harden
it and learn from this where the next iteration of technology
advancement may flow. Red Hat is well positioned to provide the 10-year
support life-cycle for Red Hat Enterprise Linux 6 - nobody can support
something better than the team that leads the technology building and
integration. From our perspective, there's no greater praise than to see
the releases harnessed by customers - that's a win-win situation."

Some notable enhancements among the literally several thousand
comprising Red Hat Enterprise Linux 6:

#### Scalability

\- up to 2X improvement in network rates; - 2X to 5X improvement in
multi-user file-system workloads; - Virtualization I/O enhancements
allowing increased consolidation (more guests per host) while at the
same time reducing I/O overhead significantly in comparison to bare
metal.

#### Datacenter Operational Flexibility

\- Control groups - enables the system administrator to control resource
consumption - for network & disk I/O, memory consumption and CPU
utilization.

\- Svirt - refers to SELinux-based security enhancements for
virtualization - enabling policy to constrain each virtualized guest's
ability to access resources like files, network ports and applications.
This forms a two-layer check and balance system whereby in a
multi-tenancy environment if one guest were able to exploit a
vulnerability in the virtualization layer, the enhanced policy is
designed to block that guest from accessing resources of other
virtualized guests or the host platform.

\- More Efficient IT - from a power consumption perspective, the most
efficient CPU is the one that is powered off - especially important for
large systems - ie: 64 CPUs. Red Hat Enterprise Linux 6 combines a new
system scheduler with more intimate knowledge of the low-level hardware
topology to yield a 25 percent reduction in power consumption versus Red
Hat Enterprise Linux 5 for a lightly loaded system - by intelligently
placing underutilized CPUs (and other peripherals) into low power
states.

#### Reliability

\- Enhanced resilience and isolation of hardware failures with
fine-grained error reporting to mark faulty memory pages as unusable,
plus hardware based memory mirroring and failing peripheral isolation.
Integration of hardware-based data check-summing at the storage level
for improved end-to-end data integrity.

\- A new automated bug reporting utility that captures the state of
applications and system service crashes and can aggregate this
information either centrally on premise or to automatically log
incidents with Red Hat support.

#### Open source collaborative development

The KVM-based virtualization scalability benefits from work with
component providers and several peripheral vendors to optimize hardware
I/O accelerators. The Red Hat Enterprise Linux 6 kernel is based on the
upstream 2.6.32 kernel (of which Red Hat is independently recognized as
the leading contributor).

A Red Hat Enterprise Linux 6 example of openness is libvirt - a Red Hat
initiative delivering a high-level management interface to
virtualization. This abstraction layer is designed to insulate customers
from system specific quirks in configuration and management. Providing
choice at the hardware level, Deltacloud provides an abstraction layer
aimed at insulating customers from lock-in at the cloud provider layer.
Red Hat Enterprise Linux 6 contains what will be a growing foundation of
Deltacloud platform enablers.

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)FreeBSD 8.2-BETA1, 7.4-BETA1 Development Releases

Now available: first beta releases of FreeBSD 8.2 and 7.4, new upcoming
versions in the the production (version 8) and the legacy production
(version 7) series. The first of the test builds for the 8.2/7.4 release
cycle is available for amd64, i386, ia64, pc98, and sparc64
architectures. Files suitable for creating installation media or doing
FTP-based installs through the network should be on most of the FreeBSD
mirror sites. The ISO images for this build do not include any packages
other than the docs. For amd64 and i386, 'memstick' images can be copied
to a USB 'memory stick' and used for installs on machines that support
booting from that type of media.

The freebsd-update(8) utility supports binary upgrades of i386 and amd64
systems running earlier FreeBSD releases. Systems running 8.0-RELEASE or
8.1-RELEASE can upgrade simply by running

    # freebsd-update upgrade -r 8.2-BETA1

Users of earlier FreeBSD releases (FreeBSD 7.x) can also use
freebsd-update to upgrade to FreeBSD 8.2-BETA1, but will be prompted to
rebuild all third-party applications (e.g., anything installed from the
ports tree) after the second invocation of "freebsd-update install", in
order to handle differences in the system libraries between FreeBSD 7.x
and FreeBSD 8.x. Substitute "7.4-BETA1" for "8.2-BETA1" in the above
instructions if you are targeting 7.4-BETA1 instead.

<span id="commercial"></span>

-----

## Software and Product News

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)MySQL 5.5 Features New Performance and Scalability Enhancements

Showing its commitment to MySQL users, Oracle announced in December the
general availability of MySQL 5.5, which delivers significant
enhancements in performance and the scalability of web applications
across multiple operating environments, including Linux, Solaris,
Windows, and Mac OS X.

The MySQL 5.5 Community Edition, which is licensed under the GNU General
Public License (GPL), and is available for free download, includes
InnoDB as the default storage engine. This release benefited from
substantial user community participation and feedback on the MySQL 5.5
Release Candidate, helping to provide a more broadly tested product.

With MySQL 5.5, users benefit from:  
\- Improved performance and scalability: Both the MySQL Database and
InnoDB storage engine provide optimum performance and scalability on the
latest multi-CPU and multi-core hardware and operating systems. In
addition, with release 5.5, InnoDB is now the default storage engine for
the MySQL Database, delivering ACID transactions, referential integrity
and crash recovery.  
\- Higher availability: New semi-synchronous replication and Replication
Heart Beat improve fail-over speed and reliability.  
\- Improved usability: Improved index and table partitioning,
SIGNAL/RESIGNAL support and enhanced diagnostics, including a new
PERFORMANCE\_SCHEMA, improve the manageability of MySQL 5.5.

In recent benchmarks, the MySQL 5.5 release candidate delivered
significant performance improvements compared to MySQL 5.1, including:  
\- On Windows: Up to 1,500 percent performance gains for Read/Write
operations and up to 500 percent gain for Read Only.  
\- On Linux: Up to 360 percent performance gain in Read/Write operations
and up to 200 percent improvement in Read Only.

For more details, replay the MySQL Technology Update web-cast from Dec.
15th: <http://bit.ly/eS99uo>.

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)SPARC M-Series Servers with New CPU for Mission-Critical Computing

Continuing a 20-year partnership in mission-critical computing, Oracle
and Fujitsu announced in December the enhanced SPARC Enterprise M-Series
server product line with a new processor SPARC64 VII+. Oracle and
Fujitsu also unveiled the unified enclosure design of the Oracle and
Fujitsu SPARC Enterprise M-series servers which are jointly designed and
manufactured by Oracle and Fujitsu.

The new SPARC64 VII+ processor provides faster memory access and
increased compute power, including:  
\- Increased clock speed of up to 3.0 GHz and double the size of the L2
cache up to 12MB, delivering up to 20 percent more performance.  
\- SPARC hardware and Oracle Solaris jointly engineered for maximum
performance.

The SPARC Enterprise M-series servers with Oracle Solaris provide
mission-critical reliability, availability and serviceability (RAS) for
applications that need to be "always on" including:  
\- Predictive self-healing, component redundancy and hot-swap, memory
mirroring and fault containment with Dynamic Domains and Solaris
Containers.  
\- Extensive combined hardware, operating system and application testing
to improve reliability and performance of the servers.

The recently announced Oracle Enterprise Manager 11g OpsCenter has new
SPARC Enterprise M-series-specific management capabilities, including:  
\- Simplified management for the entire integrated hardware and software
stack.  
\- The ability to create, manage, provision and configure Dynamic
Domains and Solaris Containers, making the SPARC Enterprise M-Series
servers a better consolidation platform.

The SPARC Enterprise M-Series servers are Oracle and Fujitsu's
comprehensive line of SPARC Solaris servers, ranging from the
single-socket M3000 to the 64-socket M9000. This announcement completes
a full refresh of Oracle and Fujitsu's entire line of SPARC servers.

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)Google releases Chrome OS and netbook for Xmas

In early December, with the announcement of a Chrome 8 browser update
and its Chrome-OS beta, Google also opened a pilot program for diverse
users. Unlike the social engineering emails of the 1990s that falsely
lured responders with offers of a Microsoft laptop, there really is a
Google netbook and many folks got one before the Xmas holiday.

The browser update includes 800 bug fixes, better HTML 5 support, a
built-in PDF reader, and integration of graphics acceleration. At its
press event at a San Francisco art gallery, engineers displayed a 3D
aquarium app that could be scaled up to thousands of fish with correct
perspective. The message was that the Chrome browser offers
significantly improved performance. In a demo, engineers loaded all 1990
pages of the recent health care law in seconds and smoothly scrolled
through it. Google also said that the new Chrome 8 browser would be
released for all major platforms, including Linux and Mac.

The star of the event was the much anticipated beta of the Chrome OS.
This is actually a very stripped down version of Debian Linux supporting
only the web browsing app. It ties into security hardware, like TPMs
\[trusted programming modules\], and is designed to boot in seconds.
What wasn't expected was the CR-48 beta test platform and the fact that
these netbooks were shipping immediately. Even more unexpected was the
fact that the public was invited to participate in the beta by receiving
a free CR-48 after applying on-line.

Google is looking for diverse and even unusual web browsing activity to
collect info on how the Chrome OS handles all web browsing requirements.
There may be a preference for users of on-line applications and SaaS.
You can apply for the Pilot program here:
<http://google.com/chromenotebook>.

According to press reports, Google has 60,000 CR-48s on order from
Taiwanese manufacturer Inventec and that about 15,000-20,000 had arrived
by the announcement date. Some of these were immediately made available
to trade press and technical publications, as well as Google staff.

The CR-48 is a cool black Linux netbook with running a tightly coupled
version of the Chrome Browser. Its a web machine, designed as a web top
without the option of a desktop - unless you put it in the secret
developer mode where the underlying Linux OS is available for testing or
hacking. It is designed to live in the Cloud, storing only some user
info and preferences locally.

Google has called it a laptop, mostly due to its 12 inch display and
full size key pad. But it weighs in at just under 4 pounds with 2 GB of
RAM and an N455 Atom processor. Many newer netbooks already use the dual
core 500 series of Atom CPUs, but the real performance hit is in the
slow links to the graphics sub-system. AMD has recently released a
dual-core Atom-class processor with an integrated graphics core and such
a chip should greatly outperform the current Atom family in the
rendering and multi-media area. (see the Product section below).

The low power Atom CPU and Mac-like Lithium polymer battery yield a
claimed 8 hours of use and 8 days of standby. (Our experience at LG with
a test machine was more like 6 hours plus and about 6 days of standby,
but the battery may have been short of a full charge.) The CR-48 looks
like an older 13 inch MacBook and is frequently mistaken for one.

The Google netbook supports WiFi, Bluetooth, and a 3G cellular adapter
with 100 MB/month Verizon service for free as a standby when WiFi is not
available. Larger data plans are available from Verizon starting at $10
per month.

These specifications should be expected when Chrome OS powered devices
from Acer and Samsung start making their debut some time next year. It
is also very likely that we will begin to see demo's of such devices
during CES in January.

Some press reports note the over-sized touchpad is over-sensitive and
the Adobe Flash plug-in for Linux has had awful performance, but both
Adobe and Google engineers are working on it.

To see what's inside the CR-48, go here:
<http://chromeossite.com/2010/12/11/google-cr-48-notebook-dissected-pictures/>.

There is also an open source project and community around the code base,
<http://chromiumOS.org>.

From the FAQ page: "Chromium OS is the open source project, used
primarily by developers, with code that is available for anyone to
checkout, modify and build their own version with. Meanwhile, Google
Chrome OS is the Google product that OEMs will ship on Netbooks this
year."

For more info, see:
<http://www.chromium.org/chromium-os/chromium-os-faq>.

Here is an initial list of netbooks that were tested with the open
source Chromium OS:
<http://www.chromium.org/chromium-os/getting-dev-hardware/dev-hardware-list>.

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)AMD Gears up for its Fusion Family of APUs

In December, AMD announced a new lineup of motherboard products for
AMD's 2011 low-power mobile platform (code-named "Brazos") and the AMD
Embedded G-Series platform for embedded systems (code-named "eBrazos"),
both based on the first AMD Fusion Accelerated Processing Units (APUs).
The 2011 low-power mobile platforms feature the new 18-watt AMD E-Series
APU (code-named "Zacate") or the 9-watt AMD C-Series APU (code-named
"Ontario").

"AMD is ushering in a new era of personal computing, and our industry
partners are ready to take advantage of the first ever AMD Fusion APU,"
said Chris Cloran, Corp. VP and GM, Computing Solutions Group, Client
Division, AMD. "By combining the processing of the CPU with the GPU on a
single energy efficient chip, we believe their customers can take
advantage of better price/performance, superior 1080p HD playback and
small form factors for innovative designs."

During Intel's Developer Forum, AMD held preview sessions showing
low-power Zacate netbook chips matching or besting a 35-watt commercial
laptop running an Intel Core I-5 CPU and standard Intel GPU. In a recent
Taiwan computer fair, MSI showed off an 11.6 inch netbook that will
shown at CES that is based on the Zacated APU with 4 GBs of RAM.

Numerous motherboard designs based on the AMD E-Series APU are planned
for retail channels and system builders from leading original design
manufacturers (ODMs), including ASUS, GIGABYTE, MSI and SAPPHIRE.
Additional motherboard designs based on the AMD Embedded G- Series
platform for embedded systems are scheduled for Q1 2011 availability to
enable the next generation of embedded computing devices from digital
signage and medical imaging to casino gaming machines and point-of-sale
kiosks.

"With the new AMD E-Series APU, we can provide our customers the
cost-effective solutions they're looking for to build PCs with
unrivalled image quality," said Joe Hsieh, Vice President of ASUS. "Our
AMD E-Series APU-based motherboards redefine the low-power, small PC
experience to go beyond basic Internet browsing for today's digital
lifestyle."

</div>

-----

  

|  |                                             |                                                                                                                                                                                                          |
|  | ------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
|  | [Share](http://www.facebook.com/sharer.php) | [![](../gx/twitter.png)](http://twitter.com/home?status=Currently%20reading:%20http://linuxgazette.net/182/lg_bytes.html%20at%20Linux%20Gazette%20%23linuxgazette "Click to share this post on Twitter") |

Talkback: [Discuss this article with The Answer
Gang](mailto:tag@lists.linuxgazette.net?subject=Talkback:182/lg_bytes.html)

#### Deividson Luiz Okopnik

-----

![\[BIO\]](../gx/authors/dokopnik.jpg)

**

Deividson was born in União da Vitória, PR, Brazil, on 14/04/1984. He
became interested in computing when he was still a kid, and started to
code when he was 12 years old. He is a graduate in Information Systems
and is finishing his specialization in Networks and Web Development. He
codes in several languages, including C/C++/C\#, PHP, Visual Basic,
Object Pascal and others.

Deividson works in Porto União's Town Hall as a Computer Technician, and
specializes in Web and Desktop system development, and Database/Network
Maintenance.

  

#### Howard Dyckoff

-----

![Bio picture](../gx/authors/dyckoff.jpg) **

Howard Dyckoff is a long term IT professional with primary experience at
Fortune 100 and 200 firms. Before his IT career, he worked for Aviation
Week and Space Technology magazine and before that used to edit SkyCom,
a newsletter for astronomers and rocketeers. He hails from the Republic
of Brooklyn \[and Polytechnic Institute\] and now, after several trips
to Himalayan mountain tops, resides in the SF Bay Area with a large book
collection and several pet rocks.

Howard maintains the
[Technology-Events](http://technology-events.blogspot.com) blog at
blogspot.com from which he contributes the Events listing for Linux
Gazette. Visit the blog to preview some of the next month's NewsBytes
Events.

  

<div id="articlefooter">

Copyright © 2011, Deividson Luiz Okopnik and Howard Dyckoff. Released
under the [Open Publication License](../copying.html) unless otherwise
noted in the body of the article. Linux Gazette is not produced,
sponsored, or endorsed by its prior host, SSC, Inc.

Published in Issue 182 of Linux Gazette, January 2011

</div>

<div id="previousnextbottom">

[next --\>](brownss.html)

</div>

</div>

</div>

![Tux](../gx/tux_86x95_indexed.png)
