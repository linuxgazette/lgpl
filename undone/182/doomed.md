# Doomed to Obscurity

**By [Pete Trbovich](../authors/trbovich.html)**

*These images are scaled down to minimize horizontal scrolling.*


[![](misc/doomed/0000372.jpg)  
Click here to see the full-sized image](misc/doomed/0000372.jpg)


[![](misc/doomed/0000367.jpg)  
Click here to see the full-sized image](misc/doomed/0000367.jpg)


[![](misc/doomed/0000370.jpg)  
Click here to see the full-sized image](misc/doomed/0000370.jpg)


[![](misc/doomed/0000365.jpg)  
Click here to see the full-sized image](misc/doomed/0000365.jpg)


All "Doomed to Obscurity" cartoons are at Pete Trbovich's site,
<http://penguinpetes.com/Doomed_to_Obscurity/>.

Talkback: [Discuss this article with The Answer
Gang](mailto:tag@lists.linuxgazette.net?subject=Talkback:182/doomed.html)

-----