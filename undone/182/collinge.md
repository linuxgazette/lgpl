# HelpDex

**By [Shane Collinge](../authors/collinge.html)**

*These images are scaled down to minimize horizontal scrolling.*

[**Flash problems?**](../124/misc/nottag/flash.html)  


[Click here to see the full-sized image](misc/collinge/139ouija.swf)


[Click here to see the full-sized image](misc/collinge/137css2.swf)


[Click here to see the full-sized image](misc/collinge/132abacus.swf)


All HelpDex cartoons are at Shane's web site,
[www.shanecollinge.com](http://www.shanecollinge.com/).

Talkback: [Discuss this article with The Answer
Gang](mailto:tag@lists.linuxgazette.net?subject=Talkback:182/collinge.html)
