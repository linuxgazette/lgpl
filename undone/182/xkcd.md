[![Linux
Gazette](../gx/2003/newlogo-blank-200-gold2.jpg)](https://linuxgazette.net/)

...making Linux just a little more fun\!

<div id="navigation">

[Home](https://linuxgazette.net/index.html) [Main
Site](http://linuxgazette.net) [FAQ](../faq/index.html) [Site
Map](../lg_index.html) [Mirrors](../mirrors.html)
[Translations](../mirrors.html)
[Search](https://linuxgazette.net/search.html)
[Archives](../archives.html)
[Authors](https://linuxgazette.net/authors/index.html) [Mailing
Lists](http://lists.linuxgazette.net/listinfo.cgi) [Join
Us\!](../jobs.html) [Contact Us](../contact.html)

-----

</div>

The Free International Online Linux Monthly

ISSN: 1934-371X

Main site: <http://linuxgazette.net>

<div id="breadcrumbs1">

[Home](https://linuxgazette.net/index.html) \> [January 2011
(\#182)](index.html) \> Article

</div>

<div class="articlecontent1">

<div class="content">

<div id="previousnexttop">

[\<-- prev](collinge.html) | [next --\>](doomed.html)

</div>

# XKCD

**By [Randall Munroe](../authors/munroe.html)**

<div class="cartoon1">

[![\[cartoon\]](misc/xkcd/tech_support.png
"I recently had someone ask me to go get a computer and turn it on so I could restart it. He refused to move further in the script until I said I had done that.
")  
Click here to see the full-sized image](misc/xkcd/tech_support.png)

</div>

More XKCD cartoons can be found [here](http://xkcd.com).

Talkback: [Discuss this article with The Answer
Gang](mailto:tag@lists.linuxgazette.net?subject=Talkback:182/xkcd.html)

-----

![\[BIO\]](../gx/2002/note.png)

**

I'm just this guy, you know? I'm a CNU graduate with a degree in
physics. Before starting xkcd, I worked on robots at NASA's Langley
Research Center in Virginia. As of June 2007 I live in Massachusetts. In
my spare time I climb things, open strange doors, and go to goth clubs
dressed as a frat guy so I can stand around and look terribly
uncomfortable. At frat parties I do the same thing, but the other way
around.

  

<div id="articlefooter">

Copyright © 2011, Randall Munroe. Released under the [Open Publication
License](../copying.html) unless otherwise noted in the body of the
article. Linux Gazette is not produced, sponsored, or endorsed by its
prior host, SSC, Inc.

Published in Issue 182 of Linux Gazette, January 2011

</div>

<div id="previousnextbottom">

[\<-- prev](collinge.html) | [next --\>](doomed.html)

</div>

</div>

</div>

![Tux](../gx/tux_86x95_indexed.png)
