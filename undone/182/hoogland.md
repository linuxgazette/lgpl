[![Linux
Gazette](../gx/2003/newlogo-blank-200-gold2.jpg)](https://linuxgazette.net/)

...making Linux just a little more fun\!

<div id="navigation">

[Home](https://linuxgazette.net/index.html) [Main
Site](http://linuxgazette.net) [FAQ](../faq/index.html) [Site
Map](../lg_index.html) [Mirrors](../mirrors.html)
[Translations](../mirrors.html)
[Search](https://linuxgazette.net/search.html)
[Archives](../archives.html)
[Authors](https://linuxgazette.net/authors/index.html) [Mailing
Lists](http://lists.linuxgazette.net/listinfo.cgi) [Join
Us\!](../jobs.html) [Contact Us](../contact.html)

-----

</div>

The Free International Online Linux Monthly

ISSN: 1934-371X

Main site: <http://linuxgazette.net>

<div id="breadcrumbs1">

[Home](https://linuxgazette.net/index.html) \> [January 2011
(\#182)](index.html) \> Article

</div>

<div class="articlecontent1">

<div class="content">

<div id="previousnexttop">

[\<-- prev](grebler.html) | [next --\>](parkin.html)

</div>

# Eight Reasons to give E17 a Try

**By [Jeff Hoogland](../authors/hoogland.html)**

![](misc/hoogland/elogo.png) If you are new to Linux you may never have
tried any desktop environments beyond [Gnome](http://www.gnome.org/) and
[KDE](http://www.kde.org/). If you have been in the Linux world for a
while, odds are you are aware of the fact that [several
other](https://linuxgazette.net/182/misc/hoogland/th_overview-and-explanation-of-linux.html)
desktop environments exist. During the three and a half years I have
spent using Linux, I have tried every different type of desktop under
the sun and of them all, [Enlightenment](http://enlightenment.org/)'s
E17 is my personal favorite. The following are a few reasons why it may
be worth breaking out of your Gnome/KDE comfort zone to give E17 a try:

<span style="font-weight: bold;"> 1.) -
<span style="font-style: italic;">Low Resource Consumption</span></span>

The [suggested minimum](http://enlightenment.org/p.php?p=about&l=en) for
running E17 is 16MB of RAM and a 200mhz ARM processor for embedded
devices. The recommended RAM is 64MB (and a stripped down version of E17
can be happy running on 8MB of RAM). From personal experience, E17
utilizes around 100MB of RAM on a fully loaded desktop install - meaning
that if you have at least 128MB of system memory in your computer, E17
will function fantastically. Because of this, E17 makes for a great
choice on older computers.

<span style="font-weight: bold;">2.) -
<span style="font-style: italic;">It is Fast</span></span>

One of the reasons many people use Linux in the first place is because
it is quicker than some [other operating
systems](https://linuxgazette.net/182/misc/hoogland/th_hands-on-with-windows-7-ubuntu-users.html).
With E17, your Linux desktop will run faster than ever. E17's low system
requirements leave more power for the rest of your applications to
utilize.

<span style="font-weight: bold;">3.) -
<span style="font-style: italic;">Desktop Effects on All
Systems</span></span>

Don't ask me how it is done, but E17 provides elegant window effects and
desktop transitions regardless of your hardware and driver setup. Intel,
nVidia, or ATI chipset; closed source or open source driver - they will
all give you a sleek looking desktop with E17. With the
[itask-ng](http://exchange.enlightenment.org/module/show/4) module, E17
can also provide a dock launcher that has a sleek look without a need
for a [compositing window
manager](http://en.wikipedia.org/wiki/Compositing_window_manager) to be
enabled.

<span style="font-weight: bold;">4.) -
<span style="font-style: italic;">It is Elegant</span></span>

If configured properly, E17 can be so much more than just a desktop
environment. In fact, many consider it to be a work of art. E17 is
designed to be pretty, and it does a fantastic job to this end.

<span style="font-weight: bold;">5.) -
<span style="font-style: italic;">It is 100% Modular</span></span>

[![](misc/hoogland/th_e17module.png)](misc/hoogland/e17module.png)

  

Not using some of the features E17 has and don't want them taking up
unnecessary resources? Not a problem\! E17 allows you to easily load and
unload each and every part (module) of the desktop through the
configuration menu. This way, only the parts of the system you are using
are loaded at start-up.

[![](misc/hoogland/e17config.jpg)](misc/hoogland/th_e17config.jpg)

  

<span style="font-weight: bold;">6.) -
<span style="font-style: italic;">It is
</span><span style="font-style: italic;">100% Configurable</span></span>

Should you want to, you can easily spend
<span style="font-style: italic;">days</span> tinkering with your E17
configuration. You can adjust anything and everything. Most notably
appearance-wise, E17 allows you to easily theme each individual module
with a different theme.

<span style="font-weight: bold;">7.) -
<span style="font-style: italic;">Core ELF are now Beta</span></span>

For many years now, people have been saying that they will not use
Enlightenment because it is "unstable". At the start of this month,
October 3rd, the Enlightenment foundation finally released a "beta"
version of their libraries. To quote the Enlightenment
[homepage](http://enlightenment.org/):

<div style="text-align: center;">

<span style="font-style: italic;">"There may be some minor bugs, but
most if not all are
gone."</span><span style="font-weight: bold;"></span>

<div style="text-align: left;">

I have been using the beta packages for nearly three weeks now and I can
attest that the above statement is true.

<span style="font-weight: bold;">8.) -<span style="font-style: italic;">
You Don't have to Compile It Anymore</span></span>

Just like many other open source applications these days, E17 can be
downloaded as an installation package for your favorite distro. In fact,
there are a couple of different pre-compiled Linux distros that use E17
as their default Window manager. These include:

</div>

</div>

  - [Elive](https://linuxgazette.net/182/misc/hoogland/th_elive-20-distro-review.html)
    - Full Desktop built on Debian Stable
  - [Unite17](http://www.unity-linux.hu/unite17/) - Desktop Distro
  - [MacPup](http://distrowatch.com/table.php?distribution=macpup) -
    Minimal CD built on Puppy Linux
  - [Bodhi](http://bodhilinux.com/index.html) - Minimalistic Ubuntu
    10.04 based

Finally, in case you didn't check out any of the links I provided above,
one of my past Enlightenment desktops appeared as such:

[![](misc/hoogland/e17desktop.jpg)](misc/hoogland/th_e17desktop.jpg)

  

Pretty, isn't it? If I've persuaded you to give E17 a try, let me know
what you think about it. Also, if you are looking to chat in real time
about Enlightenment - drop by \#e over on Freenode\!

  

|  |                                             |                                                                                                                                                                                                          |
|  | ------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
|  | [Share](http://www.facebook.com/sharer.php) | [![](../gx/twitter.png)](http://twitter.com/home?status=Currently%20reading:%20http://linuxgazette.net/182/hoogland.html%20at%20Linux%20Gazette%20%23linuxgazette "Click to share this post on Twitter") |

Talkback: [Discuss this article with The Answer
Gang](mailto:tag@lists.linuxgazette.net?subject=Talkback:182/hoogland.html)

-----

![\[BIO\]](../gx/authors/hoogland.jpg)

**

I am currently a full time student working my way through a math
education program on the south side of Chicago. I work in both theatre &
computer fields currently. I am a huge believer in Linux and believe we
will see Microsoft's dominant market share on the personal computer
crumble at some point in the next twenty years. I write a good deal
about technology and you can always find my most current
thoughts/reviews/ramblings at http://jeffhoogland.blogspot.com/

  

<div id="articlefooter">

Copyright © 2011, Jeff Hoogland. Released under the [Open Publication
License](../copying.html) unless otherwise noted in the body of the
article. Linux Gazette is not produced, sponsored, or endorsed by its
prior host, SSC, Inc.

Published in Issue 182 of Linux Gazette, January 2011

</div>

<div id="previousnextbottom">

[\<-- prev](grebler.html) | [next --\>](parkin.html)

</div>

</div>

</div>

![Tux](../gx/tux_86x95_indexed.png)
