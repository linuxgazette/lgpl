# Linux and MIDI: In the beginning...

Autor: Dave Phillips

> The Musical Instrument Digital Interface (MIDI) protocol has been
> variously described as an interconnection scheme between instruments and
> computers, a set of guidelines for transferring data from one instrument
> to another, and a language for transmitting musical scores between
> computers and synthesizers. All these definitions capture an aspect of
> MIDI. 

Roads, Curtis. 1995. [Computer Music Tutorial](http://mitpress.mit.edu/mitp/recent-books/comp/roads.html). Cambridge, Massachusetts: The MIT Press. p. 972

Greetings\! This article will hopefully be the first in a series
covering various aspects of
[MIDI](http://www.midi-classics.com/whatmidi.htm) and sound with Linux.
The series will be far from exhaustive, and I sincerely hope to hear
from anyone currently using and/or developing MIDI and audio software
for use under Linux.

Perhaps most Linux users know about MIDI as a soundcard interface
option, or as a standalone interface option during kernel configuration
for sound. As usual, some preparatory considerations must be made in
order to optimally set up your Linux MIDI music machine. Be sure to read
the kernel configuration notes included in /usr/src/linux/Documentation:
you will find basic information about setting up your soundcard and/or
interface, and you will also find notes regarding changes and additions
to the sound driver software.

Common soundcards such as the
[SoundBlaster16](http://www.creativelabs.com/) or the MediaVision
[PAS16](http://www.mediavis.com/mainmenu.htm) require a separate MIDI
connector kit to provide the MIDI In/Out ports, while standalone
interface cards such as the [Roland MPU-401](http://www.rolandcorp.com)
and [Music Quest MQX32M](http://www.opcode.com) have the ports built-in.
Dedicated MIDI interface cards don't usually have synthesis chips (such
as the [Yamaha](http://www.yamaha.com) OPL3 FM synthesizer) on-board,
but they often provide services not usually found on the soundcards,
such as MTC or SMPTE time code and multi-port systems (for expanding
available channels past the original limit of 16).

Having successfully installed your card and kernel (or module) support,
you will still need a decent audio system and a MIDI input device. If
you use a soundcard for MIDI record/play via the internal chip, you will
also need a software mixer; if you record your MIDI output to tape, and
then record your tape to your hard-disk, you will also want a soundfile
editor.

When the essential hardware and software is properly configured, it's
time to look at the available software for making music with MIDI and
Linux. Please note that in this article I will only supply links and
very brief descriptions, while further articles will delve deeper into
the software and its uses.

Nathan Laredo's
[playmidi](ftp://sunsite.unc.edu/pub/Linux/apps/sound/midi/) is a simple
command-line utility for MIDI playback and recording which can also be
compiled for ncurses and X interfaces.
[JAZZ](http://rokke.grm.hia.no/per/jazz.html) is an excellent sequencer
which has some unique MIDI-processing features and an interface which
will feel quite familiar to users of Macintosh and Windows sequencers.
[Vivace](http://www.calband.berkeley.edu/~gmeeker/vivace/) and
[Rosegarden](http://www.bath.ac.uk/~masjpf/rose.html) are notation
packages which provide score playback, but each with a difference:
Rosegarden accesses your MIDI configuration, while Vivace "renders" the
score. [tiMiDity](http://www.clinet.fi/~toivonen/timidity/) is a
rendering program which compiles a MIDI file into a soundfile, using
patch sets or WAV files as sound sources. Ruediger Borrmann's
[MIDI2CS](http://www.inx.de/%7Erubo/songlab/midi2cs/csound.html#top) is
also a rendering program, but it acts as a translator from a MIDI file
to a [Csound](http://www.leeds.ac.uk/music/Man/c_front.html) score file.
Mike Durian's
[tclmidi](http://jagger.me.berkeley.edu/~greg/tclmidi.html) and
[tkseq](http://jagger.me.berkeley.edu/~greg/tclmidi.html) provide a
powerful MIDI programming environment, and Tim Thompson has recently
announced the availability of his
[KeyKit](http://www.nosuch.com/keykit/index.html), a very interesting
GUI for algorithmic MIDI composition.

4-track recording to hard disk can be realized using Boris Nagels'
[Multitrack](http://rulhmpc38.leidenuniv.nl/private/multitrack/multitrack.html),
but Linux has yet to see an integrated MIDI/audio sequencer such as
Opcode's [Studio Vision](http://www.opcode.com) for the Mac or Voyetra's
[Digital Orchestrator Plus](http://www.voyetra.coma/) for Windows. Linux
also lacks device support for the digital I/O cards such as the
[](http://www.zefiro.comi)Zefiro or DAL's
[Digital-only](http://www.digitalaudio.com/).

If you use the tiMiDity package or MIDI2CS you will want to edit your
sample libraries. Available soundfile editors include the remarkable
[MiXViews](http://www.ccmrc.ucsb.edu/~doug/htmls/MixViews.html), the
Ceres
[Studio](http://www.ceressoft.com%3ESoundStudio%3C/a%3E,%0Aand%20Paul%20Sharpe's%20tk-based%20%3Ca%0Ahref=).

The excellent [Linux MIDI & Sound
Pages](http://www.digiserve.com/ar/linux-snd/) are the best starting
point in your search for software, and be sure to check the
[Incoming](ftp://sunsite.unc.edu/pub/Linux/Incoming) directory at
[sunsite](ftp://sunsite.unc.edu/pub/Linux/). Newsgroups dedicated to
MIDI include [comp.music.midi](news:comp.music.midi) and
[alt.binaries.sounds.midi](news:alt.binaries.sounds.midi); please write
to me if you know what mail-lists are available, I'll list them in a
later article.

Feel free to write concerning corrections, addenda, or comments to this
article. Linux has great potential as a sound-production platform, and
we can all contribute to its development. I look forward to hearing from
you!

