# Office Linux: Ideas for a Desktop Distribution

Autor: Matthias Arndt

---

## Introduction

I remember one of the meetings of my LUG a few weeks ago. We argued
about Linux and its readiness for the desktop. We all had the same
opinion that Linux is ready for the desktop, at least where software is
concerned. We discussed other related things but this is the thing that
made me think about distributions.

In this article I want to propose to create a special desktop
distribution for end users especially those who sit and work in an
office all day long like secretaries.

## Why another distribution of GNU/Linux?

To summarize my thoughts:

- Most current Linux distributions like [SuSE](http://www.suse.com/),
    [Debian](http://www.debian.org/) or [Red
    Hat](http://www.redhat.com/) come with huge amounts of available
    software. This has many advantages but many new users are confused
    with this. They ask: *I want to do .... . I installed lots of
    software from CD but which one should I use?* So why not only
    install one package that fits well for the required work?
- A distribution for offices and end user use should be easy to
    install and easy to administrate. The people shall work with the
    computer not fix bugs and tweak configurations.
- A distribution consisting of a few but proven components might be
    much easier to promote its use.

## Office Linux Manifest

*Office Linux* should not be one of the bloated 6 CDs full of programs
distributions but a simple distribution that fits on one CD and that
brings all needed applications and tools to create a productivity
environment using GNU/Linux.

*Office Linux* should

- run on standard PC hardware as found in offices
- be easy to use
- be easy to install
- consist only of selected applications and environments
- fit on one CD
- be easy to upgrade
- be designed for a workstation setup - not a single server
  application should be included
- bring along optional development packages to allow users to compile software
- have a nice and comfortable standard look'n'feel compatible to M$
    Windows ™ so that the intended end user audience feels at home
- come with all applications and tools needed for office work such as
    word processors, spreadsheets file viewers and printing utilities
- be easy to network to Linux and M$ servers
- be easy to administrate from remote locations
- bring standard compliant Internet software such as
    [Mozilla](http://www.mozilla.org/)
- allow network install for disk-less workstations

*Office Linux* could consist only of free software but this is not a
requirement.

## The bare system

*Office Linux* should only come with a proven and stable version of the
[Linux kernel](http://www.kernel.org/). The kernel should be compiled to
run on standard hardware out of the box supporting typically office
hardware as networking and printing. Multimedia support would be nice
but not required.

The standard set of [GNU tools](http://www.gnu.org/) like Bash, sed, awk
and find should come with *Office Linux*. However *Office Linux* should
not present the user or admin with a huge list of tools to be installed.
Installing a standard subset should be enough.

As *Office Linux* puts emphasize on secretaries and other office
personnel it should not come with much applications for the console. One
or two proven editors should be enough.

## Desktop environment

*Office Linux* should be easy to use. Therefor a proven stable and
possibly fast desktop environment is required. [The K Desktop
Environment](http://www.kde.org/) could fit to meet this. However it is
not the fastest possible solution.


| Pro KDE | Contra KDE |
| --- | --- |
| 
* easy to use
* known and well supported in the GNU/Linux community
* can be configured to feel like M$ Windows™
* Desktop environment with file manager and panel
* easy to configure by the end user
* comes fully internationalized
|
* needs a considerable time to launch
* huge memory footprint both in RAM and on hard-disk 
|

Personally I do not like KDE that much but I recommend it for *Office
Linux*.

## Office productivity

This is a very important field and *Office Linux* should concentrate on
this field as its name suggests. A reliable and commonly accepted office
suite like Star Office or OpenOffice should come with it.

Compatibility with M$ Office ™ is required to allow the intended user
audience to import and reuse their old files. This compatibility should
be achieved through the office suite and not through external tools. Not
only to provide GUI but to make it more easy to use. A worst case
scenario may invoke a GUI shell for command line tools.

I do not recommend KOffice for *Office Linux* just because it will find
more resistance from the intended audience than suites that resemble M$
Office ™.

The distribution should provide reliable PDF readers and converters.
Perhaps an installable PDF printer for *Office Linux* would be a nice
idea. Users could print PDFs from any application then.

The printing subsystem should be able to communicate with existing
network printers of any kind including SAMBA printers and standard Unix
printers. The subsystem should be easy to install and use. It should be
compatible with Unix convention in resembling the BSD printing system.
CUPS would be a fine solution and I suggest using it in *Office Linux*

## Internet

A standard compliant Internet suite is another main part of *Office
Linux*.

Although there a many fine programs out there *Office Linux* should only
provide on of them in a preconfigured and working way. A stable
[Mozilla](http://www.mozilla.org/) release in a complete install with
all needed plugins such as Macromedia Flash and a Java VM.

A security tweaked default configuration should be included.

## Help System

To be easy to use *Office Linux* has to include a help system that is
easy to use and navigate.

The help system should provide

  - a general tutorial for new users
  - tutorial / help system for the desktop environment
  - access to software documentation
  - a wizard for application selection depending upon the task the user
    wants to accomplish
  - general introductions to Unix concepts
  - HOWTOs on Internet security
  - wizards to install default configurations for main applications like
    the office suite or the Internet software

Markup in HTML is recommended for the Help System.

## Conclusion

I think the creation of a distribution upon these ideas is entirely
possible. It will require some work and patience but it shouldn't be
impossible.

A distribution providing only a few but proven components might be as
easy to use as M$ Windows ™. And then GNU/Linux might be ready for the
desktop. It is a matter of time, hard work and patience but it is
possible.

