
# Introduction to Programming Ada

#### By [Thomas Adam](mailto:thomas_adam16@yahoo.com)

-----

## Introduction

I'm quite old-fashioned when it comes to computers. I am one of these
people whom prefers working at a command-line driven interface rather
than a GUI. So it should not come as a shock to you that many of the
programming languages I have experimented in are also based on *textual*
input / output. **Ada**, along with **Perl, Bash, Sed, Awk, C**, etc is
no such exception.

Over the years, there have been quite a few programming languages
mentioned in the *Linux Gazette*. Ben Okopnik has done two ***very***
good tutorials on both **Perl** and **Bash**, and other people have
contributed to describing other languages, such as: **Smalltalk, C++,
Python**. Over the next couple of months, I shall be writing a series of
articles to do with programming in Ada.

## What is Ada?

Glad you asked :-) Originally **Ada** was a US governmental (DoD)
developed programming language. The standard was originally known as
Ada83, but this is now obsolete, as it was recently "overhauled" and
re-born as **Ada95**. This is now the preferred standard and
implementation of the Ada programming language.

In 1983, Ada was standardised by ANSI. Thus, it went through all the
official motions and in that year, that first edition was released. Then
four years later in 1987, ISO released an equivalent standard. At this
time though, the idea of so called OOP (Object-Orientated Programming)
was a concept that had not really been considered.

Ada however, was not designed by a committee. The original design was
implemented by Jean Ichbiah, who won a language design competition. Then
in 1995, Tucker Taft led a small group of developers and Ada95 was born.
Unlike the previous version (Ada83), the implementation of Ada95 (or
Ada9X, as it is sometimes known) underwent a public "benchmark" test;
whereby testers of the language gave their feedback, and suggestions to
make the syntactical and lexicographical layout more efficient.

The name **Ada** is attributed to a woman called **Ada Loveless**
(1815-1852) who is considered to be the world's first programmer. Ada is
used in all sorts of situations, and since it is a **concurrent**
programming language, it is most commonly used in **embedded systems**.
Ada has been used in some of the following:

  - Airplanes
  - Air Traffic Control systems
  - Financial Systems
  - Telecommunications Systems
  - Medical Devices

The list however is by no means exhaustive :-)

## Ada Compilers

Unlike other scripting programming languages (Perl, Bash, Python, tcsh,
etc), Ada like C is compiled rather than interpreted. This means that
the person that is going to run the program does not need the
interpreter installed to use it. Ada programs are therefore standalone
from having any kind of Ada packages installed. *\[Unless you have used
pragmas to interface with other languages, like C, in which case you
might have libraries, but more on that later -- TA\]*

The Ada compiler that I recommend to you is called **GNAT**, which
stands for: **GNU** **NYU** (New York University) **A**da
**T**ranslator. It is free (GNU license :-), and there is a wealth of
information on it. It is based on the **gcc** compiler, which has had
the Ada syntax bundled in with it.

It is available from the following website, which then has a link to the
GNAT compiler:

[www.gnuada.org/alt.html](http://www.gnuada.org/alt.html)

A word of caution here. I recommend that you download a pre-compiled
binary version of GNAT, and use the package **alien** if need be to
convert to .DEB .RPM .TGZ, etc. The reason I say this, is because you
will need an Ada version of **gcc** (often called **gnatcc**) to
bootstrap the compiler. If this is the first time you are installing
GNAT then the compilation from source code will not be possible.

That said, you can then go ahead and install the package once you have
downloaded it. You'll find that with the RPM version of GNAT, that there
should be one single RPM: "GNAT-3.13p-7.rpm" which will contain
everything you need to start programming in this wonderful language :-).

## Ada IDE's

Before we start our first program in Ada, I thought it would be good to
make you aware of some of the IDE's (**I**ntegrated **D**evelopment
**E**nvironment). These are programs which help you to program in the
specified language by offering features such as:

  - Syntax Highlightling
  - Compiler Support
  - Keyword Definitions
  - Pre-defined Templates

The two that I would recommend to you are:-

**TIA** (TIny Ada) -- a console based IDE written in Ada and is built
around the use of GNAT

**GRASP** -- an X11 IDE which supports among other languages, Ada\!

For all you EMACS fans, there is an extension called **Glade** which is
installed as part of the main GNAT distribution. This is an EMACS
extension which supports the GNAT compiler, syntax highlighting, etc.
More information on that can be found at [gnuada](http://www.gnuada.org)
website

You do not have to use and IDE at all to be able to use GNAT. I actually
don't bother, and instead use **jed** if I am at the console (although
this does not yet support Ada syntax highlighting) and
[**Nedit**](http://linuxgazette.net/issue79/adam.html#prognedit) if I am
in X11. This *does* support Ada syntax highlighting :-)

## The Features of Ada

Ada95 has been greately enhanced over its predecesor Ada83. The biggest
improvement has been **object orientation**, which many people will find
useful. Some of the features that Ada has are:

  - Packages (Modules), related types, objects can all be defined
  - Packages and data types can be made generic
  - Data representation gives a means to support systems programming
  - Object-orientated programming is supported
  - Errors can be caught and delt with explicitly
  - Tasks (multiple paralell threads) can be created and used. This
    ability is inherent to Ada95 and is uncommon is most other langugaes
  - Interfaces to other languages (such as: C, Fortran, COLBOL) can be
    included in the language
  - Ada supports hard **types** of data, both for textual and
    mathematical Input/Output. As a result, Ada is well suited to
    writing and maintaining complex systems

In addition to the above, there are also:

  - Packages
  - Functions
  - Procedures
  - Task Units
  - Records
  - Arrays

And many more....

## Hello World\!

Now it's time to write our first ada program. In time-honoured
tradition, we are going to start by writing a *Hello World* example.
Open up a text editor, and type in the following:

<table>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><pre><code>  
  with text_io;
  use text_io;

  procedure hello_world is  

  begin

    put(&quot;Hello World!&quot;);

  end hello_world;
  </code></pre></td>
</tr>
</tbody>
</table>

Easy, isn't it :-). Before we can run the program, we have to save it.
But it has to be with the correct suffix. GNU/Linux doesn't require any
suffix (file extension) as a rule, but it is **essential** when
programming in **Ada** and using the GNAT compiler. A list of valid
extensions are:

  - .ads - Ada package specification
  - .adb - Ada package body or program

When writing anything other than a package (which we won't be doing for
*some* time yet -- I can assure you) :-) you should append a ".adb"
extension to your filename. This is so that the compiler knows that the
file it is compiling is a program and not a package specification\!

So, save your file as **hello\_world.adb**

Now we are ready to start to compile / build the program. This has to be
done so that we can run it. You cannot run an Ada program until it has
been compiled and built.

Change to the directory that you have just saved the file, and issue the
command:

    gnatmake hello_world.adb

This will compile -\> link -\> build your Ada code into a compiled
program.

Now if you type in:

    ./hello_world

the response:

    hello world!

is output to the screen, and the program exits.

You should also have noticed that as you issued the command, the
following output was produced:

    gnatgcc -c hello_world.adb
    gnatbind -x hello_world.ali
    gnatlink hello_world.ali

You could, if you wish, type each of the above commands in turn to both
**compile, bind and link** your program (respectively). Luckily
**gnatmake** provides a nice automation for this :-). If you now look in
your directory, along with the main program, you'll find that GNAT has
created other files too, namely:

  - hello\_world.ali
  - hello\_world.o

**.ali** files are GNAT link files that contain information about
debugging and linking for the main program

**.o** files are object files which can be used in conjunction with the
program-debugger: **gdb**.

In short, unless you plan to debug your program, you can delete these
files.

## Explanation: Hello World

In perl, you can issue a command such as: **print("Hello");** and that
can be the ***only*** line in your program (excluding the she-bang
line), and it will run.

Ada however, has to be told exactly which packages it is to use before
it can perform even the simplest of commands like echoing statements to
the VDU. A **package** is a collection of functions and procedures that
perform specific tasks. If you do not declare explicitly these at the
start of the program, GNAT, when it comes to compile your program, will
bomb out immediately.

Therefore, if we wish to read and write I/O (Input/Output) to a screen
terminal, this has to be stated. All I/O functions are found within the
package **text\_io**, and the first two lines within our hello\_world
example are crucial....

``` 
 
  with text_io;
  use text_io;
```

The with statement in Ada indicates that we will be requiring the use of
the named package, in this case **text\_io**. If more than one package
is required then this can be added, by separating each package name by a
comma (,). When we have finished, we must append a semi-colon (;) to the
end of the line, similiar to that of Perl. The with statement is a
mandatory command that must **ALWAYS** be present at the start of your
program in order for it to work.

The package **text\_io**, as I have already stated allows I/O functions
/ procedures. This involves printing messages to the screen, allowing
user input to be entered, etc. It is a package that is used in virtually
every program you will ever write in Ada.

The use statement MUST be used only after the with statement has been
made. It allows for unqualified references to be made to procedures and
functions from other packages. Without the use of this statement, each
procedure or function call must have the name of the package that it
belongs to, followed by a period (full stop) preceeding it. For example,
below is what the **hello\_world** program would look like **without**
the use statement.

<table>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><pre><code>with text_io;
  
  procedure hello_world is  

  begin

    text_io.put(&quot;Hello World!&quot;); 

  end hello_world;
  </code></pre></td>
</tr>
</tbody>
</table>

You can see how this has increased the amount of information that we
have to type in, without the use of the use statement. When more than
one package is used that might have the same procedure or function
names, the compiler can usually tell to which package you are referring,
based on the parameters passed to it.

The third line:

``` 
procedure hello_world is  
```

declares that we are writing a new procedure with the name
**hello\_world**. The statement word is tells us that we are about to
start the declarative section of the procedure, more on that later.

The keyword  
  
begin  
  
then tells us that we are going to start the executable part of the
procedure -- i.e. where all the statements will appear and be executed,
which in this case is:

    put("Hello World!")

Which calls the procedure put from the package **text\_io** to print the
message **Hello World\!** on the screen.

The last line:

    end hello_world;

simply just ends the named procedure.

In short, the basic structure for an Ada program looks like the
following:

<table>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><pre><code>with text_io;
use text_io;

procedure program_name is 

      [ declaritive part here ]

begin

      [ executable section here ]  

end program_name;</code></pre></td>
</tr>
</tbody>
</table>

Also within the package **text\_io** are commands such as:

    put
    put_line
    get
    get_line
    new_line

Plus many others...

put does what we have already seen.  
put\_line does the same as put, except starts on a new line.  
new\_line is a command issued on its own, which starts a new line. If
you use it, make sure that you put a semicolon at the end of it, like:  
  
new\_line;  
  
In fact, that statement about the semicolon (;) goes for each command
that you make in Ada.

Next month, we will be looking at:

  - text\_io (inputting data via the get statement)
  - data types
  - simple arithmetic expressions
  - enumeration types

## Exercises

Well, that is all for this month. I'm sorry if it seems like I'm not
explaining enough things all in one go, but trying to explain anything
more at this point, is I think overload. So, I am going to leave you
with a few exercises for you to try.....

1\. Print your name on the screen

2\. Print your address on the screen, using only put and new\_line

3\. Repeat exercise 2, this time with put\_line

If you submit them to me, I will print them in my next installment of
this article\!

As with all of my articles, if you have any questions, suggestions,
rants or raves (hopefully not complaints :-) drop me a line\!\!

-----

    with text_io, ada.integer_text_io;
    use text_io, ada.integer_text_io;
    
    procedure happy_programming is
    
    loop_number : integer :=0;
    
    begin
    
      while loop_number /= 10 loop
        loop_number := loop_number + 1;
        put("Happy Programming in Ada");
        new_line;
      end loop;
    
    end happy_programming; 
