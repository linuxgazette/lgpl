
# Creating Reusable Software Libraries

#### By [Rob Tougher](mailto:robt@robtougher.com)

-----

### 1\. Introduction

Software libraries provide functionality to application developers. They
consist of reusable code that developers can utilize in their projects.
Software libraries targeted for Linux are usually available in both
binary and source code form.

A well-written software library:

  - is easy to use
  - works flawlessly
  - provides detailed error information

This article describes the above principles of library creation, and
gives examples in C++.

<table>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><h4 id="is-this-article-for-you">Is This Article For You?</h4>
<p>Create software libraries only when you have to. Ask yourself these questions before proceeding:</p>
<ul>
<li><em>Will anyone (including you) need functionality X in future applications?</em></li>
<li><em>If so, does a library implementing functionality X already exist?</em></li>
</ul>
<p>If no one will need the functionality you are developing, or a software library implementing it already exists, don't create a new library.</p></td>
</tr>
</tbody>
</table>


### 2\. Making It Easy To Use

The first step in creating a software library is designing its
**interface**. Interfaces written in procedural languages, like C,
contain functions. Interfaces written in object-oriented languages, like
C++ and Python, can contain both functions and classes.

Remember this motto when designing your interface:

  - *The easier to use, the better*

As a library designer, I am constantly faced with finding the right
balance between functionality and ease of use. The above motto helps me
resist adding too much functionality into my designs.

Stick with the following guidelines, and you'll be fine.

#### 2.1 Keeping It Simple

The more complex a library, the harder it is to use.

  - *Keep It Simple, Stupid*

I recently encountered a C++ library that consisted of one class. This
class contained 150 methods. *150 methods\!* The designer was most
likely a C veteran using C++ - the class acted like a C module. Because
this class was so complex, it was *very difficult* to learn.

Avoid complexity in your designs, and your interfaces will be cleaner
and easier to understand.


#### 2.2 Being Consistent

Users learn consistent interfaces more easily. After learning the rules
once, they feel confident in applying those rules across all classes and
methods, even if they haven't used those classes and methods before.

One example I am guilty of involves public accessors for private
variables. I sometimes do the following:

    class point
    {
    public:
      int get_x() { return m_x; }
      int set_x ( int x ) { m_x = x; }
    
      int y() { return m_y; }
    
    private:
      int m_x, m_y;
    };

Do you see the problem here? For the m\_x member, the public accessor is
"get\_x()", but for the m\_y member, the public accessor is "y()". This
inconsistency generates more work for the users - they have to look up
the definition of each accessor before using it.

Here's another example of an inconsistent interface:

    class DataBase
    {
    public:
    
      recordset get_recordset ( const std::string sql );
      void RunSQLQuery ( std::string query, std::string connection );
    
      std::string connectionString() { return m_connection_string; }
    
      long m_sError;
    
    private:
    
      std::string m_connection_string;
    };

Can you spot its problems? I can think of at least these items:

  - Methods and variables are not named consistently
  - Two different terms, *sql* and *query*, are used to denote a SQL
    string
  - *m\_sError* does not have a public accessor
  - *get\_recordset()* does not have a *connection* in its argument list

Here is a revised version that solves these problems:

    class database
    {
    public:
    
      recordset get_recordset ( const std::string sql );
      void run_sql_query ( std::string sql );
    
      std::string connection_string() { return m_connection_string; }
      long error() { return m_error; }
    
    private:
    
      std::string m_connection_string;
      long m_error;
    };

Keep your interfaces as consistent as possible - your users will find
them much easier to learn.

#### 2.3 Making It Intuitive

Design an interface how you would expect it to work from a user's point
of view - don't design it with the internal implementation in mind.

I find that the easiest way to design an intuitive interface is to write
code that will use the library *before* actually writing the library.
This forces me to think about the library from the user's standpoint.

Let's look at an example. I was recently considering writing an
encryption library based on OpenSSL. Before thinking about the library
design, I wrote some code snippets:

    crypto::message msg ( "My data" );
    crypto::key k ( "my key" );
    
    // blowfish algorithm
    msg.encrypt ( k, crypto::blowfish );
    msg.decrypt ( k, crypto::blowfish ):
    
    // rijndael algorithm
    msg.encrypt ( k, crypto::rijndael );
    msg.decrypt ( k, crypto::rijndael ):

This code helped me think about how I should design the interface - it
put me in the user's shoes. If I decide to implement this library, my
design will flow from these initial ideas.

### 3\. Testing Thoroughly

A software library should work flawlessly. Well not *flawlessly*, but as
close to flawless as possible. Users of a library need to know that the
library is performing its tasks correctly.

  - *Why use a software library if it doesn't work correctly?*

I test my software libraries using automated scripts. For each library,
I create a corresponding application that exercises all features of the
library.

For example, say I decided to develop the encryption library I
introduced in the previous section. My test application would look like
the following:

    #include "crypto.hpp"
    
    int main ( int argc, int argv[] )
    {
      //
      // 1. Encrypt, decrypt, and check
      //    message data.
      //
      crypto::message msg ( "Hello there" );
      crypto::key k ( "my key" );
    
      msg.encrypt ( k, crypto::blowfish );
      msg.decrypt ( k, crypto::blowfish );
    
      if ( msg.data() != "Hello there" )
        {
          // Error!
        }
    
      //
      // 2. Encrypt with one algorithm,
      //    decrypt with another, and check
      //    message data.
      //
    
      // etc....
    }

I would occasionally run this application to make sure that my software
library did not have any major errors.


### 4\. Providing Detailed Error Information

Users need to know when a software library cannot perform its tasks
correctly.

  - *Alert the user when there is a problem*

Software libraries written in C++ use **exceptions** to pass information
to its users. Consider the following example:

    #include <string>
    #include <iostream>
    
    
    class car
    {
    public:
      void accelerate() { throw error ( "Could not accelerate" ); }
    };
    
    
    class error
    {
    public:
      Error ( std::string text ) : m_text ( text ) {}
      std::string text() { return m_text; }
    private:
      std::string m_text;
    };
    
    
    int main ( int argc, int argv[] )
    {
      car my_car;
    
      try
        {
          my_car.accelerate();
        }
      catch ( error& e )
        {
          std::cout << e.text() << "\n";
        }
    }

The *car* class uses the **throw** keyword to alert the caller to an
erroneous situation. The caller catches this exception with the **try**
and **catch** keywords, and deals with the problem.

### 5\. Conclusion

In this article I explained the important principles of well-written
software libraries. Hopefully I've explained everything clearly enough
so that you can incorporate these principles into your own libraries.
