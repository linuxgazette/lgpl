
# Playing DVDs on Linux

#### By [Tunji Durodola](mailto:oladokun@consultant.com)

-----

Hello dear readers.

My name is Tunji Durodola and I write from Nigeria, West Africa, the
largest collection of black peoples on the face of the planet.

The purpose of this article is to give an insight into how to get Linux
to play DVDs using one or more of the now readily available tools on the
web. You should have basic DVD playing in a matter of minutes; are more
detailed section will follow later.

**Preamble.**

The key to watching DVDs lies in the ability of hardware or software to
decode and read encrypted movies. DVDs are encrypted with a special
algorithm called Content Scrambling System or CSS, to prevent illegal
copying of the material contained on the disc. The algorithm is not a
secret, but to get a copy of it to put in your device (hardware or
software), you have to pay a license fee **plus** sign a mean set of
agreements to prevent you from disclosing the algorithm to anyone.

Each DVD has its own key, rather akin to each door having a separate key
to unlock it. The key itself in Windows is kept secret.

All commercial Windows DVD players have the algorithm contained in it,
but they have paid, and as such, charge for their software, or the cost
is embedded in the price of the DVD drive your purchase, so in effect
you are paying a fee for the “bundled” software player.

The whole philosophy of Linux is **freedom**, which would be defeated if
you have to pay for a Linux DVD player. Some chaps tried to get the
algorithm from the owners, but were told they had to go through the same
process as the Windows people.

For those earthlings who haven't got a clue as to what DeCSS is, I'll
give a brief summary.

A young lad, a few years ago, desiring to watch **his** legally
purchased DVDs in Linux, thought to develop a player for Linux, when
none existed at the time, stumbled on a flaw in a now defunct Windows’
player called Xing, which had the unfortunate habit of leaving the key
in the program itself. He then used his knowledge of maths to
reverse-engineer the code and generate the algorithm.

The software he wrote to do that job was called DeCSS. He then teamed up
with a few friends collectively called Masters Of Reverse Engineering
(MORE) to develop a DVD ripper on Windows, and a small set of
Linux-based utilities to view the un-encrypted files.

No fee was charged, but was posted on the ‘net for anyone with a similar
desire to view their DVDs in Linux. The MPAA found out and subsequently
obtained a court order forbidding any US site from hosting DeCSS. That
of course sparked worldwide interest in Linux-based DVD players. The
case is still in court in the Land of the Free. For more info please
click [here.](http://www.eff.org/)

Today, there are other software decryptors available for Linux which do
not use the original DeCSS code, but do the same job, and are not
subject to any litigation. We shall focus on these.

**The Goods\!**

Just to get you warm, I'll tell you what system I've got in my crib.

 

**Hardware**

CPU: Pentium III 750 (old, I know, I'm planning for an Athlon XP 1900+)

RAM: 1GB PC 133 SDRAM (hey, ram was cheeeep when I bought)

BOARD: MSI BX Master, 4 IDE Slots (2 on an on-board Promise Controller)

Case: ATX Extended Tower with 9 5.25 Slots

Sound: SoundBlaster Live\! 5.1 Platinum (lovely card\!)

VGA: NVidia TNT2, 32MB SDRAM

HDD: 2x WD400 7200 RPM, 40GB drives, 2MB Cache (I'm showing off here)

Speakers: **Front:** 80W Kenwood speakers, driven by a Technics 80W
Power Amp connected directly to the card

**Rear:** Some mid-budget 20W RMS computer speakers

**Center:** As Above

**Sub:** A no name 40W Sub in a wooden enclosure

Monitor: 18" NEC TFT Flat Panel

 

**Software**

**OS:** SuSE Linux 8.0 Professional

**Sound:** **ALSA 0.9.0rc2,** running emu10k1 SoundBlaster driver. This
is the only audio driver for Linux capable of using the Surround
capabilities of the SB Live 5.1. Even the Windows drivers and software
don't have half the features of this driver. Linux driver can handle up
to 8 such cards on 1 system, whereas Windows can't handle two (don't
bother, I've tried it). Hats off to the ALSA team\!

**Video:**

1.     **Xine 0.9.12** (Complete with its plugin capabilities makes Xine
hard to beat)

2.     **Ogle 0.8.2** (Fast and quick DVD-only player that supports DVD
menus)

3.     **Mplayer 0.90** (Mainly Console-based player with an unusual
assortment of options. Mplayer will play almost any type of file format
available today including VOB, VIVO, ASF/WMV, QT/MOV, Xanim, AVI, DiVX,
VCD, SVCD, and of course DVDs It has a GUI option with skins.)

Both Xine and the Mplayer now offer FULL multi-channel (5.1) surround
audio.

  

**To compile mplayer:**

Requirements

libdvdread 0.8 **and** libcss (<span class="underline">not</span>
libdvdcss)

**or**

[libdvdread 0.9](http://www.dtek.chalmers.se/groups/dvd/downloads.shtml)
**and** [libdvdcss 0.0.3](http://www.videolan.org/) (not libcss NOR
libdvdcss 1.0)

all may be obtained at
[www.dtek.chalmers.se/groups/dvd](http://www.dtek.chalmers.se/groups/dvd)

The libdvdcss is used to decrypt the DVD and libdvdread to read its
contents, and for chapter support.

I recommend you use **ALSA 0.9.0rc2**, for audio, if you have a modern
sound card, such as the SoundBlaster Live\! 5.1 series. The Audigy range
may work, but alas, I don't have one :-(

*Please read the* INSTALL *and* README *files in all packages*

**Step 1**

libdvdcss

./configure

compile install it with "make && make install && ldconfig"

**Step 2**

libdvdread

compile and install as above

**Step 3**

mplayer 0.90

./configure –help

make && make install

It should then install itself in /usr/local/bin as mplayer

**Step 4**

(a) if /dev/hdc is your dvd drive, make a link *ln -s /dev/hdc /dev/dvd*

(only needs to be done once)

type *mplayer -dvd 1 -ao oss*

The software should give some info such as the encryption key for the
DVD, and then start to play the "encrypted" movie.

There are a gazillion options available, too numerous to dig into here,
but multichannel audio is possible with *-channels x*, where x is 2,4 or
6 speakers. Remember, it is pointless if you have a basic 2-channel
card. These multichannel cards are affordable these days so spoil
yourself and get one\!

Other useful options:

*-title x*                  – select DVD title

*-chapter y*               – select chapter in title specified above

*-ss hh:mm:ss*           –jump to specific time point

*-vcd x*                  - play vcd chapter

\-channels 4 - play thru 4 discrete channels (front & rear)

On-screen display is also available, but not regular DVD subtitles.

Mplayer has rapidly become the most widely downloaded Linux software by
a far margin (see <http://freshmeat.net/stats/#popularity> if you don’t
believe me), but it is not as easy to set up as Xine, if you don’t like
compiling apps.0

**To get Xine up and running in 5 minutes flat.**

**Step 1**

download the latest xine releases from
[xine.sourceforge.net](http://xine.sourceforge.net/)

You will need the following RPMs if you do not feel like compiling. x86
refers to your type of Pentium processor; i686 for Pentium III or
higher, i586 for Pentium and AMD K6

  - xine-lib-0.9.12-x86.rpm
  - xine-lib-0.9.12-oss-x86.rpm
  - xine-lib-0.9.12-oggvorbis-x86.rpm
  - xine-lib-0.9.12-xv.rpm
  - xine-lib-0.9.12-x86.rpm
  - xine-ui-0.9.12-x86.rpm
  - xine-lib-dvdnav-0.9.10-x86.rpm

*There are others, but these are the bare essentials.*

**Step 2**

Copy all the RPMs into an empty folder and from there, logged in as
root, run the following:

rpm -Uvh xine\*.rpm

If you are averse to using the console, call up kpackage or gnorpm and
install them in the GUI instead.

**Step 3**

In GUI, open up a console (purely to see the output from the player,
once you are comfortable with the settings, you won't need the console),
and type the following (mind the case sensitivity of each letter) *xine
-pq -A oss -V xv -u0 dvdnav://*

It may look cryptic but it is easy to explain. The purpose of the
switches is to set defaults for audio and video in the config file which
is stored in

“.xine/config” in your home folder.

\-pq play immediately, and quit when done

\-A oss use oss as the audio driver

\-V xv use xv as the video driver

\-u0 select the first subtitle (usually English, u1 refers to French,
etc.)

dvdnav:// is the optional plugin that actually plays the DVD. It also
has menu functionality and allows you to jump from chapter to chapter
with 9/3 on the numeric keypad.

Type "xine --help" or man xine for full details.

As stated earlier, the skin may be changed in the menu. All settings are
also possible in the menu including multichannel audio.

Xine plays a whole range of media: DVDs, VCDs, CDs, ogg, mp3, wav,
DiVX... on and on and on.

 

**URLs**

xine: [xine.sourceforge.net](http://xine.sourceforge.net/)

ogle:
[www.dtek.chalmers.se/groups/dvd](http://www.dtek.chalmers.se/groups/dvd)

mplayer: [www.mplayerhq.hu/homepage](http://www.mplayerhq.hu/homepage)

ALSA: [www.alsa-project.org](http://www.alsa-project.org/)

xinedvdnav plugin (to decrypt DVDs, with DVD menus):
[http://dvd.sourceforge.net/](http://dvd.sourceforge.net/d)

I hope to keep you posted with a more detailed paper sometime soon, with
tips and tricks.

'Later.
