
# SysRq: The Process-nuke

#### By [Vikas G P](mailto:vikasgp386@hotmail.com)

-----

So you thought you could always kill an offending program with `kill -9`
? But what if it's your X server that has crashed, or that nifty svgalib
program you wrote ? That's where magic SysRq comes in.

## What is it

Magic SysRq is a key combination directly intercepted by the kernel and
can be used, among other things, to perform an emergency shutdown. It is
described in Documentation/sysrq.txt and implemented in
drivers/char/sysrq.c in the kernel source tree. It exists primarily for
kernel hackers, but it can be useful to people in user-space also. Since
it is implemented as a part of the [keyboard
driver,](http://www.linuxjournal.com/article.php?sid=1080) it is
guaranteed to work most of the time, unless the kernel itself is dead.

A note: In the rest of this article, when I say "SysRq key" I mean the
single key beside the Scroll lock key. But when I say "magic SysRq" I
mean the *combination* \< Alt+SysRq \>.

## Enabling

To do the SysRq magic, your kernel needs to be compiled with
CONFIG\_MAGIC\_SYSRQ. Most distributions would have enabled it by
default. If your's hasn't, you'll just have to
[recompile](http://www.tldp.org/HOWTO/Kernel-HOWTO.html)... :)

After everything is OK with the kernel, check if SysRq is enabled by
default.

    $ cat /proc/sys/kernel/sysrq
    0

If it shows zero, it's not enabled. Writing any non-zero value to
/proc/sys/kernel/sysrq will enable it.

    $ echo "1" > /proc/sys/kernel/sysrq

If you want it to be always enabled, append these lines to one of your
initialization scripts(preferably rc.local).

    #Enable SysRq
    echo -e "Enabling SysRq\n"
    echo "1" > /proc/sys/kernel/sysrq

Alternatively, you might look for a file called /etc/sysctl or
/etc/sysctl.conf which some distributions have(mine, RedHat, does). You
can add a line like this to it, and sysrq will be enabled at boot-time.

    kernel.sysrq = 1

The magic SysRq combination is a unique one. Now, every key on the
keyboard sends a code when pressed or released, called the scan-code.
The magic SysRq combination (Alt+SysRq), however, sends only *one*
scan-code(0x54, decimal 84) even though two keys have been pressed.
Check this out using `showkey -s`.

## What can I do with it ?

Magic SysRq is invoked as \< Alt+SysRq \> + \< command \>. The SysRq key
is also labelled as Print Screen. The commands are:

k: Secure access key - This kills all processes running on the current
virtual console, so that no snoopy program can grab your keystrokes
while you type your password.

u: Attempts to unmount the root device, and remount it read-only. In
addition to an emergency shutdown, this command also comes in handy if
you have only one partition for Linux and need to do an fsck or
low-level filesystem editing(for example, ext2 undeletion. See [Ext2fs
Undeletion Howto](http://www.tldp.org/HOWTO/mini/Ext2fs-Undeletion.html)

s: This command syncs the kernel buffers to disk. You should do this
before unmounting.

b: Does an immediate reboot, pretty much like pressing the reset button.
For a safe shutdown, precede this with a sync and Unmount.

p: Prints the contents of the CPU registers.

m: Shows memory information.

t: Shows information about the tasks currently running.

0-9: Sets the console log-level to the specified number.

e: Sends a SIGTERM to all processes, except init.

i: Sends a SIGKILL(sure kill) to all processes, except init.

l: Sends a SIGKILL to all processes, including init(you won't be able to
anything after this).

## Getting out

How do you get out of SysRq mode ? There is no definite documentation
about this in sysrq.txt. It talks about having to press the left and
right control,alt and shift keys, but a simpler thing worked for me.
Just press Alt+SysRq once again, and you'll get out of it.

The way I understand this is: The kernel remembers the state of the
magic SysRq combination: it's either down or up. When you press the key
for the first time, the state is changed to down. And when you press any
other key while SysRq's state is down, the kernel interprets it as a
command. If you press SysRq again, the state is changed to up, and
further keystrokes are handed to whatever program requests it.  
(Actually, it's not that simple. Sometimes, the above mentioned method
doesn't work. I believe it's because the kernel uses a separate
translation table when magic SysRq is down.)

The SysRq key originally meant, as you can guess, "System Request". It
was used by early IBM terminals to get the attention of a central
computer to execute a command. The key seems to have few uses now,
except in the linux kernel.

## Security

Leaving magic SysRq enabled on a production machine can be potentially
dangerous. Anyone with physical access to the machine can bring it down
instantly.  
You should also disable SysRq if other people can log in to your system
remotely. A \< break \> sent from a remote console will be interpreted
as \< Alt+SysRq \>, and the consequences can be disastrous. See the
[Remote-Serial-Console-HOWTO](http://www.tldp.org/HOWTO/Remote-Serial-Console-HOWTO/index.html)
for more details.

## Conclusion

The magic SysRq hack can come in very handy at times. However, it must
be used with care. It can also give you some insights into the inner
workings of the kernel. If you are enterprising, you might even hack the
kernel and add new commands \!
