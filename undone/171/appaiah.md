# Random signatures with Mutt

Autor: Kumar Appaiah

---

## Introduction

Ever since the start of the Internet, electronic mail has been a
mainstay for communication. Even as new social networking phenomena such
as online-friend connection databases and microblogging are taking
communication on the Web by storm, e-mail still remains a preferred
means of communication for both personal and official purposes, and is
unlikely to fade away soon.

One aspect of e-mail that we often observe is the sender's signature -
the tiny blurb that appears on the bottom. If it's an official e-mail,
then you're most likely constrained to keep it something sober that
talks about your position at work, or similar details. However, if you
are mailing friends, or a mailing list, then wouldn't it be nice to have
a signature that is customized and/or chosen from a random set of
signatures? Several Web-based e-mail services offer this already; a
cursory Web search reveals that services such as GMail offer this
feature. However, the customizability is limited, and you are restricted
in several ways. However, if you use a customizable mail client like
Mutt, several fun things can be done with e-mail signatures, using more
specific details that are within your control.

In this article, I describe one of the possible ways of having random
signatures in your e-mail in the [Mutt](http://www.mutt.org/) mail
client, and switching signature themes and styles based on certain
characteristics, such your inbox or recipient. I will, therefore, assume
that you have a working configuration for Mutt ready, though the concept
I describe applies equally well to other mail clients which support
this, such as Pine. I'll make use of the `BSD/Unix fortune` program
(available in almost all GNU/Linux and \*BSD distributions) in this
article, though any solution that chooses random quotes would do.

## Preparation: collect your signatures for `fortune`

The Unix `fortune` program is a simple program that uses a database of
strings (called "fortunes") to randomly choose a string and display it
onto the screen. The program is named appropriately, since it uses a
collection of files on various subjects to display messages, much like
those on fortune cookies.

It is usually easy to install the program on most GNU/Linux
distributions. For example, on Debian/Ubuntu-like systems, the following
command will fetch the fortune program and several databases of strings:

```sh
apt-get install fortune-mod fortunes
```

Similarly, `yum install fortune-mod` should help on Red Hat/Fedora-like
systems. It shouldn't be too hard to figure out how to install it on
your GNU/Linux or BSD distribution.

Once installed, you can test out the existing files. For example, on my
Debian machine, the following command lists a random signature from one
of the `/usr/share/games/fortunes/linux` or
`/usr/share/games/fortunes/linuxcookie` files:

```sh
fortune linux linuxcookie
```

If you now open one of the files in `/usr/share/games/fortunes/`, such
as `linux`, you'll observe that they consist of strings which are
separated by the "%" sign, resembling the format of this file:

```sh
An apple a day keeps the doctor away.
%
A stitch in time saves nine.
%
Haste makes waste
```

Also, associated with each file, such as `linux`, is a `.dat` file, e.g.
`linux.dat`. The way `fortune` works is that it looks for the file
specified at the command line, (absent such a command line option,
choosing a random file among those in the path searched by default), and
looks for a corresponding .dat file, which has a table of file offsets
for locating strings in that file. If it doesn't find the dat file
corresponding to a text file with signatures, it ignores that file.

While you can use the signatures present already, if you want to create
and use your own signatures, get them from wherever you want, and place
them in a file, separated by % signs as discussed above. For example,
I'll put the above file in a location called $HOME/Signatures. Let me
call my file $HOME/Signatures/adages. To use it with `fortune`, I use
the `strfile` program to generate the table of offsets:

```sh
[~/Signatures] strfile adages
"adages.dat" created
There were 3 strings
Longest string: 38 bytes
Shortest string: 18 bytes
```

Now, running `fortune` with the full path of the file causes a random
string to be displayed from the above file. (Note that the full path
must be provided, if the file is outside the default fortunes
directory.)

```sh
fortune $HOME/Signatures/adages
Haste makes waste
```

The `fortune` program is versatile, and has several options. Be sure to
read its manual page for more details.

## Tailoring `fortune` output for signatures

The `fortune` output is a random string, and often, these strings are
too long for use as good signatures in your e-mail. To avoid such
situations, we make use of the `-s` option so `fortune` will display
only short strings. The default definition of a short string is up to
160 characters, but you can override it with the `-n` option. For
example:

```sh
fortune -n 200 -s \<files\>
```

could be a suitable candidate as a signature file.


## Configuring Mutt

Now, you're ready to use your neat fortunes in Mutt. If you have no
custom signature, and want to uniformly use a random signature from your
newly created signature file, as well as the default `linuxcookie` file,
then add the following to your `.muttrc`:

```sh
set signature="fortune -n 200 -s linux $HOME/Signatures/adages|"
```

If this is what you wanted, you're done. However, if you want to
customize the signatures based on other parameters such as recipient, or
current folder, read on.

## Customizing your signature based on different criteria

Let me describe my setup here. I use the `procmail` program to filter my
mail, and let's say that I've got the following folders of interest:

1.  `inbox`: where all my personal e-mail arrives.
2.  `linux`-list: which consists of my local Linux user group mail.
3.  `work`-mail: where I read all my work mail.

Now, I want to have the following configuration:

* When I reply to or compose mail in the work mail folder, I want a
fixed signature.
* When I send to linux-list, I want a signature from the linux
fortunes database.
* Finally, in all other situations, I want a signature from my adages
list.

There are simple steps to achieve this using the hooks in Mutt. Note
that I have interspersed it with some other configurations for
demonstration.

```sh
# Set the default options
folder-hook . 'set record=+sent; set from=me@my-personal-email; set signature="fortune $HOME/Signatures/adages|"'

# Set the custom hooks
send-hook linux-list@example.com 'set signature="fortune -n 200 -s linux|"'
folder-hook work-mail 'set record=+sent-work; set from=me@my-work-email; set signature="$HOME/.signature-work"'
```

What the above snippet does is to set, amongst other things, the default
signature to be my adages set, and automatically switch the signature to
a fixed file, `$HOME/.signature-work`, when I move to the `work-mail`
folder. Finally, if I send mail to the `linux-list@example.com` mailing
address, the signature is altered to display a fortune from the `linux`
fortunes file. Simple enough, and the possibilities are infinite\!

## Getting good fortunes

You can scout the Internet to get nice quotations and interesting
messages to add to your signatures collection; all you need to do is to
put each in the right format, use `strfile` to enable fortune to use it,
and plug it into your Mutt configuration. I often like to hunt for
quotations on [Wikiquote](http://www.wikiquote.org), amongst other Web
sites. While I am sure most of us don't mean any harm while using other
people's sentences in our signatures, ensure that you stay on the right
side of the law while doing so.

I wish to make a mention of the [Linux One Stanza
Tip](http://lost.sourceforge.net/) project, which provides a very nice
set of signatures with GNU/Linux tips, and more, in their distribution.
They also provide templates for you to generate your own signatures with
designs and customizations. Sadly, the project doesn't seem to be that
active now, but the tips and software provided are very apt and usable
even today.

## Conclusion

In conclusion, I hope I've managed to show you that with Mutt, you can
customize the way you handle signatures in a neat manner. Should you
have comments or suggestions to suggestions to enhance or improve this
workflow, or just want to say that you liked this article, I'd love to
hear from you\!

