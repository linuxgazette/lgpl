# Dotfile generator

Autor: Jesper Pedersen

---

In this article I will describe a configuration tool called The Dotfile
Generator (TDG for short). TDG is a configuration tool, which configures
programs, using X11 widgets like check boxes, entries, pull-down menus
etc.

For TDG to configure a given program a module must be made for it. At
the moment modules exist for the following programs: Bash, Fvwm1, Fvwm2,
Emacs, Tcsh, Rtin and Elm.

The article will describe common use of TDG, so if you do not have it
yet, it might be a good idea to [download
it](ftp://ftp.imada.ou.dk/pub/dotfile/dotfile.tar.gz) (It's free\!) You
may also go to [the home page of the Dotfile
Generator](http://www.imada.ou.dk/~blackie/dotfile/) for further
information.

## Intro

The UNIX system, was developed many years ago, long before graphical
user interfaces became commonplace. This means that most of the
applications today work fine without a graphical user interface.
Examples of this are editors and shells.

A basic concept in UNIX is that the programs are very configurable. Here
is an example from Emacs, which shows this:

> *What should be done if the user asks to go to the next line at the
> of* a file?
> There are two logical possibilities:
> 
> 1.  Insert a blank line, and move to it.
> 2.  Beep, to tell the user that there is no next line.
> 
> Instead of implementing only one of the solution the people behind
> Emacs, have chosen to implement both, and let you decide which one you
> prefer.

Since the program works without a GUI, the standard method for
configuring such options is to use a *dot-file*. In this file, you may
*program*, which method you will use.

This solution, however, requires that the user has to learn the
programming language used in the dot-file, and has to read lots of
documentation to find out which configurations can be made. This task
may be difficult and tedious, and for that reason many users often
choose to use the default configuration of the program.

If you take a look at some dot-files, you may find that most of the
configurations can be described by the following items:

  - Configurations with two possibilities (like above)
  - Configuration, where the program wishes to know a number or text.
    Examples of this could be questions like: *how many times should one
    press CTRL-d to quit*, and *email address to use in reply-to field*.
  - Configuration, where the user may choose an option from a list,
    eg: *which editor would you like to use: emacs, jed, vi or vim*

The configurations above may easily be done with a GUI, with the
following widgets in order: A check box, an entry and a pull down menu.
This is exactly what is done in TDG.

## The basic concept of TDG

TDG is a tool, which configures other programs (eg. emacs,bash and fvwm)
with widgets as those described above, and many more. The widgets are
placed in groups, which makes it easy to find the correct configuration
without having seen it before. And most important of all, help is
located at the configuration of each option, instead of in a manual far
away. To get help, you just press the right mouse button on the widget,
which contains the configuration you want to know more about\!

When you start TDG, you will be offered a list of standard
configurations, where you may pick one to start out with. This may be
convenient, if you do not have a dot-file for the given program, or if
you would like to try a new configuration. If on the other hand, you
already have a dot-file, which you would like to put the finishing
touches to, you may read this file into TDG. Note, however, that it is
not all modules, which have the capability to read the dot-file (the
fvwm2, rtin and elm modules have, the other modules do not, since it
would be to complicated to create such a parser.)

When you have selected a start-up configuration, the menu-window will be
displayed (see figure 1). In this window, you can travel through the
configuration pages, just like a directory structure. If you select a
page, a new window will be displayed, with the configuration for this
page (see figure 2). This window will be reused for all the
configuration pages, ie. only one configuration page is visible at a
time, so you do not have to destroy the window yourself.

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><img src="./gx/pedersen/menu_window.jpg" width="294" height="196" />
<p>Figure 1</p></td>
<td><img src="./gx/pedersen/config_page.jpg" width="399" height="203" />
<p>Figure 2</p></td>
</tr>
</tbody>
</table>

In region 1, the actual configuration is located. Region 2 is the help
region. In this region help for the whole page is shown, when the window
is displayed. It's also here, help for the individual configuration is
shown, when you press the right mouse button on one of the widgets.

In region 3, information is shown on what will be generated. You have
three possibilities:

1.  You may generate all pages. This is the most natural thing to do,
    when you just want a configuration for a given program.
2.  You may generate just the page shown. This is useful if you are
    plying around with TDG to see what will be generated for the
    different configurations.
3.  Finally you may tell TDG, to just generate some of the pages, which
    is done with radio buttons in this region.

In the "Setup > Options" menu, you may select which of the three
methods above will be used.

When you have done all the configurations, you have to tell TDG which
file you wish to generate. This is done from the Options menu
("Setup > Options>). And now it's time to create the actual dot-file,
which is done by selecting "Generate" in the "File" menu.

Once you have generated the dot-file, you may find that you would like
some of the configuration to be different. You could now go to the
configuration page in question, change your configuration, and then
generate once again. If, however, you are testing several different
options for a single configuration (ie. several items from a pull-down
menu) you may find it cumbersome to generate the whole module over and
over again. In this situation, you may chose `Regenerate this page` in
the `File` menu. Note, however, that if some part of the configurations
on the page effects other pages, these will not be generated, so in
these situation you have to generate the whole module.

To see how to use the generated dot-file, please go to the `Help` menu,
and select the `How to use the output` item.

## The configuration widgets

TDG uses a lot of widgets to let you configure the different options.
Some of them are well known from other applications and include: check
boxes, radio buttons, pull-down menus, entries, text boxes (for
multi-line text), directory and file browsers. Others, however, are
specifically designed for use in TDG, and they will be described in the
following.

### The ExtEntry widget

The ExtEntry is a container, which repeats its elements, just like a
list-box repeat labels. A number of the elements in the ExtEntry may be
visible on the screen at a time. The elements in the ExtEntry may be any
of the widgets from TDG (ie. check boxes, pull-down menus and even other
ExtEntries.) One element in an ExtEntry is called a tuple. In Figure 3,
you can see an ExtEntry from the Tcsh module.

![](misc/extentry1.jpg)
Figure 3

This ExtEntry has three visible tuples, though only two of them contain
values (you can see, that the third one is grayed out). To add a new
tuple to the ExtEntry, you have to press the button in the lower right
corner, just below the scroll bar. If the ExtEntry contains more tuples
than can be shown in it, you may scroll to the other tuples with the
scroll bar.

If you press the left mouse button on one of the scissors, a menu with
four elements will be displayed. These elements are used to cut, copy
and paste tuples within the ExtEntry.

If the tuples get very large, only one of them may be shown on the
screen at a time. An example of that is seen in figure 4.

When the tuples contain many widgets, scrolling the ExtEntry becomes
slow. In these cases, the ExtEntry may have a quick index. In figure 4,
you can see the quick index at the top of the ExtEntry (it's the button
labeled Idx.) When this quick index is invoked, a pull-down menu is
display with the values of the element associated with the quick index.
This makes it much easier to scroll the ExtEntries.

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><img src="./gx/pedersen/extentry2.jpg" width="310" height="272" />
<p>Figure 4</p></td>
<td><img src="./gx/pedersen/prompt.jpg" width="360" height="274" />
<p>Figure 5</p></td>
</tr>
</tbody>
</table>

### The FillOut widget

Every shell has a configuration option called `Prompt`. This option is
some text, which will be printed, when the shell is ready to execute a
new command. In this text special tokens may be inserted, and when the
prompt is printed, these tokens will be replaced, with some information
from the shell. Eg. in Bash `\w` will be expanded to the current working
directory.

In TDG, a special widget has been created called a `FillOut`, which does
configurations like the above. In Figure 5, you can see a FillOut widget
from the Bash module. At the top of the widget there is an entry, where
you can type ordinary text. Below it, the tokens are placed. If you
select one of the tokens, it is inserted in the entry at the point of
the cursor. Some of the tokens may even have some additional
configurations. Eg. the token `Current working directory` has two
possible options: `Full directory`, and `only the last part`. When
tokens with additional configurations are selected, a window will be
displayed, where these configurations can be done. If you wish to change
such a configuration, press the left mouse button on the token in the
entry.

### The Command widgets

TDG can be extended by the module programmer through the Command widget.
This makes it possible to configure specific options with widgets they
have developed themseves. At the moment three such widgets exist: The
directory/file browser, the color widget and the font widget.

The widgets will appear as a button within TDG, and when the button is
pressed a new window will be displayed, where the actual configuration
is done.

## Save, Export and Reload

When you have configured the different options in TDG, you may wish to
leave it, and come back later, and change some of the configurations.
When you leave TDG, you may save your changes, which you do from the
`File` menu.

Next time you enter TDG, your saved file will be one of the the files
you will be offered as a start-up configuration.

One important point you have to note is that this `save file` is an
internal dump of the state of TDG. This means that this file dependson
the version of TDG and the module. This means that if you wish to send a
given configuration to another person, this format is not appropriate. A
version independent format exists, which is called the *export format*.
To create such a file, you have to select `Export` instead of `Save` in
the `File` menu.

Sometimes you may wish to restore the configuration on a single page, to
its value as it was before you started playing around with it, or you
may wish to merge another person's configuration with your own. This is
done by selecting `Reload` in the `File` menu. To tell TDG that you only
want to reload some of the pages, you have to select the `Detail` button
in the load window. This will bring up a window, where you can select
which configuration pages, you wish to reload. Here you can also tell it
how you want the pages to be reloaded. You have two possibilities:

  -  Overwrite
    The pages you are loading, will totally overwrite the contents of
    the file
  -  Merge
    Tuples in the ExtEntries will be appended to those which already
    exist in the module. Other configurations will be ignored in the
    file.

Here's another difference between the save-files and the export-files:
You cannot merge with save-files. This means that if you have a
save-file, which you wish to merge with, you first have to load it,
export it, and then you can merge with it.

## The End

Additional information can be found on [the home
page](http://www.imada.ou.dk/~blackie/dotfile/) for TDG.

It's always a good idea to have a bookmark on this page, as work is
currently in progress on new modules.

  -  procmail
    I have finished a module on procmail, a mail filter, which can sort
    your incoming mail.
  -  firewall configuration (ipfwadm)
    John D. Hardin (<jhardin@wolfenet.com>) is working on a module for
    configuring the fire walling and IP Masquerading setup for
    standalone systems connected to the Internet via dialup. He may,
    however, expand it to more general fire walling.

If you have some spare time, I would very much like to encourage you to
develop a module for your favorite program. On the home page of TDG,
there is a link to a document, which describe how to create a module for
TDG. [Send me a letter](mailto:blackie@imada.ou.dk), and I will be happy
to help you get started with it.

