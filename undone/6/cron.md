# Using Cron

Autor: Brian Freeze

Tłumaczył: 

---

## Description of Cron

To best describe cron is to first look at all those annoying tsr's in
the DOS world that use up major amounts of memory and require many
programs to run in the background to complete various tasks on the
system. All of these drain CPU time and use up valuable RAM. Linux and
unix as a whole have a method in which one process is the master which
runs all the time and calls programs as they are needed so that valuable
resources are not wasted. Call it the master scheduler if you will
because that is what it is.

Cron lives up to what the computer was designed for. Give it a task,
walk away, and know it will get done without any more manual direction
from you. Schedule backups, remove old logs, create stats from logs, or,
if you run a dialup connection, run a script that will ping a server to
ensure the connection is up and, if down, redial your provider in
whatever time frame the script is told. The nice thing about cron is you
tell it when to run and how often to run. I have a script launched by
cron to check my connection every 5 mins and if down redial and another
to check mail every 2 hours from a popserver all of which is done behind
the scenes and system resources are only called upon at the given time
the script must run.

A good source for beginners on this subject is a book by Matt Welsh and
Lar Kaufman called **Running Linux**. Pages 191 to 196 give details on
using cron.

## Cron Entries

First of all, we need a script to run for a job we want done. Let's take
my slip connection and run through the process of setting a cron entry
for this as all will be similar and shell commands can be used in the
entries as well. This gives an even wider range of things you can do
with this. Once we have a script to run it is time to edit the crontab
file. You do not do this in the normal way but enter this command:

```
www# crontab -e 
```

this will bring up crontab into the "vi editor" which is set as the
default editor. You can change this by sending the following command at
the prompt.

```
www# setenv VISUAL=emacs    This is for the Bourne-compatable shell.
www# setenv VISUAL emacs    This is for the C shell.
```

Check out the man page for cron as well as the man page on your shell
and how to set the default editor. Once in the editor, we can see that
there are already system scripts or programs running from cron. Most
have 2 things in common: they have a comment line started with a "\#",
and the next line is the command line.

Crontab command line:

"minute" "hour" "dayofmonth" "month" "day of week" "script and shell
commands go here"

example:

```
0,5,10,15,20,25,30,35,40,45,50,55 * * * * /usr/local/bin/checkmail
```

As you can see, I have set cron to run every 5 mins which would not be
very efficient unless I got a lot of mail and was at my machine all the
time. It would be better to set it as follows using the hour field:

```
0 6,7,8,9,10,11,12,13,14,15,16,17,18,23 * * * /usr/local/bin/checkmail
```

As you can see, the fields are seperated by a space and you can put
multiple entries in the field, as I have done, seperated by a comma. You
can put shell commands in the command field such as this example taken
from the book Running Linux:

```
0 1 1 * * find /tmp -atime 3 -exec rm -f {} \;
```

This one will run the command at 1AM every month on the 1st day to check
and remove old files from /tmp.

```sh
########################################################################
# This is a script that I have included that will check a dialup line by 
# pinging a name server to see if the lines up and if down call up dip to 
# redial the line. I set cron to run this every 5 mins. My provider has a 
# limit of 15 mins if the line is inactive and will drop it.
#########################################################################
#                             check-link

#!/bin/sh
PING=/tmp/ping.$$

#check that link is up
ping -q -c 1 131.202.3.3 > $PING 2>1

awk '
/transmitted/ {
          if ($1 == $4) {
                  exit 0
          } else {
                    exit 254
          }
} ' $PING

rslt=$?

rm $PING

if [ $rslt -eq 254 ]
then
               #restart link
              /sbin/dip /etc/dipscript & 
fi
#########################################################################
```

Now that you know how to edit and enter things in cron, experiment with
it as this is what linux is all about and, no, I haven't figured out how
to get cron to let the dogs out at 3am but I'm working on it.

regards

Brian C. Freeze  
freezeb@nbnet.nb.ca  
freezeb@www.deltastar.nb.ca

\[Well, here's *another* subject about which I admit to knowing very
little. I honestly haven't tinkered much with cron and so VERY much
appreciate Brian's helpful write up. I'm sure that there are a LOT of
things that can be done with cron. If you have any ideas or suggestions
please drop a note to Brian or myself\!

\[Enjoy\! --John\]
