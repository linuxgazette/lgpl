# Updating XTerm Titlebar Followups

\[Ok... this was the hands-down winner in the "Followup of the Month"
department\! :-) For all you "Weekend Linux Mechanics" out there...
here's some more too-way-Cool stuff to play with... drop the authors a
note and say thanks\! --John\]

Autorzy:

* Haykel Ben Jemia
* Adam Sjoegren
* Liang Ng
* Jacob Waltz
* Jean Liddle
* David Billington
* Alan Wild

---

Date: Tue, 19 Dec 1995 17:48:15 CST  
From: Haykel Ben Jemia \<haykfaje@linux.zrz.TU-Berlin.DE\>  
Subject: Changing that xterm titlebar interactively\!  

Hi John,

first of all I want to thank you and all the others for doing this GREAT
work and publishing such a NICE, USEFULL and FUNNY gazette like the
Linux Gazette.

In the November issue there is an interesting article entitled:
"Changing that xterm titlebar interactively\!" It was for me especially
interesting because at the same day when I read this article, a friend
asked me if I knew how to make an 'xterm' put the current working
directory in his titlebar. And here is the extract from that article
that spoke about this feature : "...One of the interesting things that
is described is using escape sequences to force an xterm to display the
current working directory. This is described nicely on page 259. Thing
is, you need to be using the C Shell in order to make this work because
it uses the "cwd" variable to keep track of where you are. In the
process of tinkering around with this I wasn't able to get BASH to
cooperate, BUT, I did learn a couple tricks that are kinda fun...."

That's why I tried to make this possible. And after reading some man
pages, I really succeeded to find a solution on how to make BASH force
an xterm to print the content of the $PWD variable in its titlebar, and
here is how : these lines must be added to $HOME/.bashrc :

```
-------------------------------CUT HERE--------------------------------------
# set the title of an xterm to $PWD
xtitle()
{
        if [ "$TERM" = "xterm" ]; then
                echo -e "^[]2;$PWD^G"
        fi
}

#Change th 'cd' command to use xtitle()
cd()
{
        builtin cd $1
        xtitle
}
------------------------------------CUT HERE---------------------------------
```

P.S. Don't forget to enter the ESC-characters like discribed in the
article \!\!

So I hope this will enjoy some BASH users ;)

Ciao

---

Date: Mon, 25 Dec 1995 20:54:41 CST  
From: Adam Sjoegren \<asjo@diku.dk\>br\> Subject: LG Nov 95, xterm
titlebar  

Hi John.

I really like your Linux Gazette. All those little things that are so
easy and obvious, that takes an enourmous amount of time to figure out,
if you don't start your search the right place. Collected in LG. Great
:-)

One thing about your piece about changing the titlebar interactively:
For those of us who don't know, or don't care, how to enter ESCAPE and
CTRL-G chars in our favourite text-editors; do like this:

```
echo -n -e "\033]2;Too way cool (right, John?) :-)\007"
```

The man-page for "echo" gave me the hint for the "e" option and voila: I
don't have to learn how to enter weird chars in my text-editor, and even
better - when your write in LG, you don't have to explain the chars (in
the examples the chars show up blank (on my paper - I like to read the
Linux Gazette on paper sitting in the couch :-))).

Thanks for a great effort - I will write something for LG when I find
something of interest.

---

Date: Tue, 09 Jan 1996 16:15:46 CST  
From: Liang Ng \<L.S.Ng@ecs.soton.ac.uk\>  
Subject: Update of xterm title bar. Solution.  

Hi,

After I posted my previous email, I started to tinker with xtitle a bit
more. And I found a solution to my problem. It is easy: just redirect
the output to the tty of xterm whose title bar you want to change. e.g.

``` 
xtitle "News" > /dev/ttyp1
```

will change the xterm title bar in ttyp1.

This command can be executed from any local xterm shell. Just a note on
'ps'. 'ps' will show the tty and the command line for a xterm. However,
if the program in the xterm is not active, it will show only the command
instead of the command line. So for example when I do rlogin to other
hosts, it is a nuisance when I cannot see from the ps output the host
which rlogin is connected to. I will need to press a key or two in the
rlogin xterm to activate it, so that ps will now show the full command
line with the hostname of rlogin included.

This is what I do typically:

```
ps      <  to find out which tty is doing what
PID TTY STAT  TIME COMMAND
 877 pp2 SW    0:01 (trn)
 976 pp3 S     0:00 (rlogin)
 977 pp3 S     0:00 (rlogin)
1183 pp4 S     0:00 rlogin monet
1185 pp4 S     0:00 rlogin monet
```

Then I set 'trn' title to 'News' by

```
xtitle "News" > /dev/ttyp2
```

And ttyp4 title to 'Monet' by

```
xtitle "Monet" > /dev/ttyp4
```

But I do not know which host is the rlogin talking to on ttyp3, because
it has been inactive for a long time. So I need to touch it and ps
again, so that the hostname is shown:

```
ps
976 pp3 S     0:00 rlogin renoir
977 pp3 S     0:00 rlogin renoir
```

So I will now do:

```
xtitle "Renoir" > /dev/ttyp3
```

One last thing, anyone know how to solve this 'ps' command line problem?

L S Ng

From: Liang Ng \<L.S.Ng@ecs.soton.ac.uk\>  
Subject: xterm title bar. Again.  

Arrggghhh.. This is addicting. I have been wasting my time.

Anyway, here are some suggestion about the default xtitle, which I think
would be more useful than 'xterminal:/dev/ttyp1 plus Date'.

```
NAME=`hostname | perl -ne 'print "\u$_"'`

xtitle() 
{ 
        if [ "$*" != "" ]; then
                echo -n "ESC]2;$*^G" 
        else
                echo -n "ESC]2;$NAME `/usr/bin/tty`^G"
        fi
}
```

(Note: please replace ESC with the actual ESC character.)

So the default will show the hostname with the first character
capitalized and the tty.

L S Ng

Date: Sun, 03 Dec 1995 20:08:49 CST  
From: Jacob Waltz \<waltz@pcjiw.lampf.lanl.gov\>  
Subject: XTerm title tricks  

Greetings!

Here's another little trick I use with regards to xterm titles.. My
linux box is smack dab in the middle of an SGI cluster at work, and
needless to say I do a lot of telnetting. So to remind me which machine
I'm on, I put the following into my .cshrc file on every machine I use:

``` 
#!/bin/csh
set MYHOST=`echo $HOST | sed 's/\./ /g' | awk '{print $1}'`
alias cd 'cd \!*;echo -n "^[]2;$MYHOST-$cwd^G"'
```

and then the following into my .login:

``` 
echo -n "^[]2;$MYHOST-$cwd^G"
echo -n "^[]1;$MYHOST^G"
```

Now, let me explain what this does...in the .cshrc file, the first line
sets the host. Since some machines here spit out the entire name, while
some just spit out the machine name (i.e. pcjiw.lampf.lanl.gov versus
just pcjiw) the sed and the awk trim off everything but just the machine
name. Now, the two lines in the .login file echo the machine name to
both the xterm title bar and the icon name. Secondly, you will notice
the alias for cd ... this just puts the current working directory into
the titlebar - this little trick is basically straight out of O'Reilly's
\_X\_Window\_System\_User's\_Guide\_ .

On thing I have yet to figure out is how to get the xterm title and icon
name to change back when I log out...my temporary fix is the following
alias in my .login file:

``` 
alias renew 'source ~/.cshrc; source ~/.login'
```

When I log off a machine, I just type in 'renew' first thing. Perhaps
not the most elegant way to do it, but it works\! Although I think one
might be able to do something with the .logout file and the REMOTEHOST
environment variable...but the current directory might be a little
trickier...

Anyhow, hope this is useful to someone\!

---

Date: Thu, 14 Dec 1995 10:27:03 CST  
From: Jean Liddle \<jean@interaccess.com\>  
Subject: xterm title prompts with bash  

Hi,

Thanks for the nice tip in the Linux Gazette -- dynamic xterm titles
seem to be a trick which I relearn and forget again from time to time --
it's nice to have a handy reference on-line\!

To get a dynamic path working under bash I did the following:

```
function goto { cd $1; /usr/local/bin/xterm_title `pwd`; }
alias cd=goto
```

where /usr/local/bin/xterm contains just one line taken from your
example script:

```
echo "^[]2;$*^G"
```

(my editor won't let me enter the escape character - I did this by
cutting and pasting to a cat redirected to the abovementioned filename)

there may be more elegant ways to do this, but this works for getting
the current directory on the titlebar.

enjoy,

Jean.

---

Date: Sat, 30 Dec 1995 19:36:36 +0000 (GMT)  
From: David Billington \<bli@betty.compulink.co.uk\>  
Subject: Xterm title bar & working directory  

Dear John, thanks for Linux Gazette, it's by far the best resource I've
found on the Web.

Getting the bash shell to update an xterm title bar with your current
directory :-

```
if [ $TERM = xterm ]
then
    PS1='\033]2;`pwd`\007$ '
fi
```

\\033 & \\007 are the octal vales for ESC and ^G respectively. \`pwd\`
gets replaced with the working directory each time the prompt is
displayed. It's important to use single quotes to enclose the string not
double.

Putting the directory path in the title bar is much better than having
it clutering up your prompt line.

Thanks again for a superb publication \!

Dave Billington.

---

From: Alan Wild \<arwild01@scic.intel.com\>  
Subject: Interactive X-title Bars  

I don't know is someone has beaten me to this or not, but here is how
you can place your current path in your title bar using either bash or
tcsh. These will probably port to other shells to, I have only checked
them with the two aforementioned.

Note:

* ^\[ represents the Esc character, you can input this by typing ^VEsc
* ^G represents the ^G character. You can input this by typing ^V^G

Anyway. . .

bash:

```
export PS1="^[]xterm:  \w^G\h\$ "
```

tcsh

```
set prompt = "%{^[];%}xterm:  %~%{^G%}%m%% "
```

\-Alan Wild

    Note:  please direct all replies to arwild01@starbase.spd.louisville.edu
    the account that I am at currently will only be in use for a few short
    months.
    
    -- 
    Alan Wild
    arwild01@starbase.spd.louisville.edu
    f/s/c  arwild01@scic.intel.com

