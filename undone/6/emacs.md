# Emacs tips

Autor: Paul Lussier

Tłumaczył: 

---

Hi John,

Listen, I must agree with you regarding the "My Favorite Editor" Holy
War. Telling some one they should use a different editor is a great way
to start a huge flame war. I, of course, must put my $.02 in though\!

I personnally think emacs is the way to go. Not to say that one
shouldn't know the basics of vi. If you're system goes down, vi will
always be there\! But there are just some things emacs can do that vi is
either not suited for or just can't do. Here are some of my favorite
emacs features:

1.  Incredibly extensible online help in hypertext form (called Info
    pages in emacs lingo)

2.  A built in tutorial (though I've only used it once :)

3.  The ability to have several files open simultaneously (called
    buffers in emacs lingo)

4.  Several progamming modes perl, C, Lisp, Pascal, Basic, Fortran, etc.
    (some one mentioned this feature for one of the other editors)

5.  Built in abbreviations that can be added to or updated on the fly
    (rather than having to define them in a `.\*rc` file)

6.  Mouse capability (though that's becoming a standard)

7.  The ability to assign text characteristics (bold, italics,
    underline, etc. using the mouse)

8.  The ability to read man pages on the fly

9.  "dired" - the ability to edit directories i.e. show an "ls -alg"
    like listing of any directory and perform functions on the files in
    it; like mark these files and rm/cp/mv/gzip (or any other command)
    them

10. Built in revision control renames the current file to whatever.\~1\~
    (upto whatever \# you specify) anytime you edit a file, so you can
    instantly get back to what you just screwed up\!

    And my single, most favorite feature of them all! This one
    let's you save "TONS" of space:

11. The ability to read/create/edit \*.tar, \*.tar.gz, and \*.gz files\!
    What this means is if you have a whole directory floating around
    that you don't want to get rid of, but don't really need all the
    time, you can tar the directory up, then gzip it\! When you want to
    look at what's in there or need to edit one of the files, open up
    the tar.gz file as you would any normal text file, edit it, then
    save it. emacs will uncompress and un-tar the whole thing in memory,
    the save your changes, and re-tar and re-gzip the whole thing
    automatically\!

Some one may say, "Why is this good, why would I need this?" Well, I for
one use this feature all the time. For example, every single file in my
home directory (at work, soon to be at home) is gzipped. I mean
everything\! If I need to edit it, I open it in emacs, I need to print
it (this works for post scripts too:) I zcat filename.gz | lpr, if I
need to grep it, zgrep whatever filename.gz, if I need to quikly look at
it, zless it\! To give you an idea of the amount of space I've saved, at
work my home directory structure is about 116MB. If everything were
untarred and un-gzipped, the total size would be over 1/2GB (yes, that's
Gigabyte\!). That means my total directory size is 1/5 what it would be
uncompressed.

Well, that's it for now. As I said, these are just a few of my favorite
features of emacs. And if anyone is interested in what the .emacs file
needs to contain, I'll be more than happy to help them out. If anyone
wants to play with emacs, I highly recommend trying the built in,
on-line tutorial. Type emacs at the command line, and when emacs starts
up, type Ctrl-h,t (hit the Control & h keys together, let go, then hit
't'), and follow the directions.

See ya,  
Paul  

P.S. Another helpful hint on saving space: Netscape and Mosaic are both
capable of reading, decompressing, and displaying .gz and .Z files. So,
all those .html files could be stored as .html.gz :)

    =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
  * Paul Lussier      =   It is a good day    =The next best thing to doing -
    = Raytheon ESD SEL*    to put slinkies    -something smart is not doing =
  * pll@swl.msd.ray.com   =     on escalators =      something stupid.      -
    =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
       =                           Quote of the week:                          =
     *        Real programmers don't document. If it was hard to write,      -
       =                it should be hard to understand.           =
       -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

\[Well, as I've mentioned before, I'm a bit of a VI affectionado myself
and use it all the time (actually, I use VIM :-). I've tried to get a
handle on emacs and am slowly learning the ropes. I definitely welcome
any ideas or suggestions for using emacs and appreciate Paul's offering
these ideas\! --John\]

