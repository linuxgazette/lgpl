# Introduction to Tcl/Tk

Autor: Jesper Pedersen

Tłumaczył: 

---

## Tcl/Tk -- what is it

In this article I'll try to describe what Tcl/Tk is, its strong points,
and its draw backs. Furthermore, I'll give some examples of using tcl/tk
and provide references to materials about Tcl/Tk. Finally, I'll try to
help you avoid a few of the common pitfalls with using Tcl/Tk.

### Tcl

Tcl is an interpreter language which is very simple to learn and use. At
the very beginning, John K. Ousterhout (the author of the program)
didn't meant it to be a programming language in itself, but rather a
glue for creating interfaces. The idea was that one should write his/her
program in C and then use tcl as an interface to the code.

Despite this initial intention, Tcl has come to be used heavily as a
programming language in itself, and it has turned out to be a very good
language for quick and dirty programming. It's much more difficult to
use it as a language for larger programs. (This requires much more
dicipline and structure)

### Tk

Tcl is a very simple programming language, and would never have been
used for anything else, other than an interface glue language, if it
weren't for its brother TK. Tk is a library of Tcl functions which
create and manipulate X11 widgets like *entries*, *listboxes*,
*buttons*, and many many other objects. Tk provides very high level X11
programming, *i.e.,* it requires about 10-30 times fewer lines to create
a graphical interface in Tk than in pure X11 programming.

## Getting started

To start using Tk, you have to know just a little bit about Tcl. Though
this section is quiet boring, it'll be of great value for you when using
Tk in itself. (please note that Tk is *only* a library package to the
Tcl language.) To start Tcl type **tclsh**. This will start the
interpreter for Tcl.

### Variables and Evaluation strategy

The very core of a language is variables. In tcl you set variables with
the command **set**, and read the content of a variable, either with
**set** or prepend the name with a dollar sign. Here is an example:  
`set var 10`, this will set **var** to the value **10**  
`set var`, this will read the content of var, ie. return the value 10.  
`$var`, this will expand to the value of **var**

To try this, use the command **puts**, which will write it's argument to
standard out (stdout), try this:

    set var 10
    puts [set var]
    puts $var

This will write 10 twice to standard out. Well what were those brackets
for? In Tcl, commands inside brackets are expanded, so the evaluation
strategy for the command `puts [set var]`, is as follows:

1.  Evaluate `set var` which evaluates to **10**
2.  Evaluate `puts 10`, which write 10 to standard out.

### braces or quotes

That was the first part of Tcl, the second part is the differentiation
between curly brackets and quotion marks (**{}** and **""**)  
Lets see an example:

    set var 10
    puts "var is $var"
    puts {the name of the variable is $var}

Executing the second line prints **var is 10**, while the third line
prints **the name of the variable is $var**. Here is how it works: if
the outermost delimiter is a brace, then everything inside is seen
literally, whereas if the outermost delimiter is a quotion mark, then
every thing inside is expanded.

    puts {every thing "is [$litterals]"}
    puts "here \[things\] shall be \"escaped\" !\$"

This will print `every thing "is [$litterals]"` and `here [things] shall
be "escaped" !$`

Different commands require different numbers of parameters, while braces
and quotes are used to group things which are meant as one parameter\!
I.e if you want to set **var** to "hello world", write `set var "hello
world"` or `set var {hello world}`. In this example there is no
difference between using braces or quotes. However, if you want to set
var to the content of var1 plus a string you would write `set var "var1
is $var1"`. If you had written `set var {var1 is $var1}` **var1**
wouldn't have been expanded, since it is protected by the braces.

### References to Tcl

The online manual pages are the most important references in Tcl/Tk as
they contain all the information you need, and it might be a very good
idea to [print them out](#installation). There are also a lot of places
where the manual pages have been converted to HTML. Here are a few
references:

* To learn more about braces, quotes, evaluation strategies, etc, see
    the manual pages: [man
    Tcl,](http://www.sco.com/Technology/tcl/man2/tcl_man/Tcl.n.html)
* John K. Ousterhouts [draft to his
    book](http://www.cica.indiana.edu/cica/faq/tcl/tcl.html) about
    Tcl/Tk. Please note that this book is about Tcl version 7.3 and Tk
    3.6 See the note about [Tcl/Tk versions](#versions)
* Brent Welch's [Practical Programming in Tcl and
    Tk](http://www.smli.com/~bwelch/book/index.html)
* Other online Tcl/Tk manual pages can be found at the following
    sites:  
    [www.sco.com](http://www.sco.com/Technology/tcl/Tcl.html#ManPages)
    [www.elf.org](http://www.elf.org/tcltk-man-html/contents.html)
    [www.imada.ou.dk](http://www.imada.ou.dk/Technical/Manpages/tcl/index.html)

## Getting started with Tk

Now it's time to see what Tk is all about (That's why you read this
article right?\!) As I mentioned earlier, Tk is *just* Tcl functions
used to manipulate X11 widgets. Well were do I start...

### Hello world

Yes\! All introductions to new programming languages offer a Hello World
example, so here it comes. Please start the Tcl interpreter with support
for Tk. This one is called **wish** or **wish4.0**

    label .label -text "Hello World"
    button .button -text "Quit" -command exit
    pack .label .button

Try it\!

Well what happens? Three Tk functions are called: **label**, **button**
and **pack**. The first one creates a label with the text "Hello World".
The next one create a button, gives it the title "Quit", and associates
it with the Tcl command "exit". The third line maps them in a window, so
you can see them on the screen.

Ok let's try to understand what this is all about. To do that, please
invoke the manual page for the
[label](http://www.sco.com/Technology/tcl/man2/tk_man/label.n.html)
command. In this you can see that the label command has several options
available. The idea is that you can configure the label in several ways.
All the options have a default value You can, for example, configure the
color of the text, the font, the text itself (as we did in the example),
and so forth. Some of the options are specific for the label command,
while others are general for several different Tk functions. The
specific ones are listed and described in the manual page for the label
command, while the more general ones are listed and described in the
manual page called
[options](http://www.sco.com/Technology/tcl/man2/tk_man/options.n.html).

The last thing to understand about the label command is its first
argument, which is *.label* in our example above. This argument is the
name of the label. The name is used, when you want to place the label on
the screen, or later on when you want to configure some of its
properties (eg. change the text of the label).

To fully understand this, we have to learn about the packing strategy of
Tk.

### The Packer

The last command which was invoked in the example above was **pack**, if
you try to run the example without this command, you will see that
nothing appears on the screen. The idea is that you still need to
describe how things are packed on the screen.

To see what you can do with this command, see the manual page for
[pack](http://www.sco.com/Technology/tcl/man2/tk_man/pack.n.html). Here
are a few examples which will give you an idea of what can be done.
Exchange the pack line above with each of these lines, and rerun the
example:

* `pack .label .button -side left`
* `pack .label .button -pady 20`
* `pack .label -pady 100 -padx 100 -anchor c`  
    `pack .button -anchor e`

### Interaction with the user

So far the only interaction with the user has been a button which he
could press to exit the program. Often, programs need a bit more
interaction, so let's see how entries and check buttons work. Here is a
bit code which will create a check button and an entry on the screen:

    ### creating two frames for the different part of the text
    frame .top
    frame .middle
    frame .bottom
    pack .top .middle .bottom
    
    ### creating the entry with a label
    label .top.label -text "Please type your name:"
    entry .top.entry -textvariable name
    pack .top.label .top.entry -side left -padx 5
    
    ### creating the check button
    checkbutton .middle.but -text "I'm good at tcl!" \
       -variable good
    pack .middle.but
    
    ### creating the OK and CANCEL button
    button .bottom.ok -text OK -command okCmd
    button .bottom.cancel -text Cancel -command exit
    pack .bottom.ok .bottom.cancel -side left
    
    ### creating the command to call when the OK 
    ### button is pressed
    proc okCmd {} {
      global name good
      puts "Hi $name"
      if {$good} {
        puts "You are good at tcl...great"
      } else {
        puts "You should really learn Tcl, before you play with TK"
      }
    }

In Tk, variables for entries, check buttons, radio buttons and other
widgets which require user interaction, are saved in global variables.
So when I create an entry with *- textvariable name* then the contents
of this entry is reflected in the global variable *name*.

In the example above, I create a procedure which is called when the *OK*
button is pressed. The procedure definition is, in fact, very simple and
very much like in C. See the manual page for
[proc](http://www.sco.com/Technology/tcl/man2/tcl_man/proc.n.html) for a
full description on how it works.

In this procedure I declare the variables *name* and *good* for global
variables with the global command. (Again, if you want to know more see
the manual page\!) In the example above you can see an example of a
widget which is packed inside another. There is nothing fancy about it.
The *frame* widget is just a container which help you pack the things in
a smart way on your screen. When I have created the frame *.top* I can
pack things inside it, and I have to name its sub widget with the .top
name prefixed.

The rest of the example should be clear, but if there is something you
don't understand, please see the manual page, all the information is
located in them\!

## Programming Tcl/TK

In this section I'll give you some hints about how you can create Tcl/Tk
programs.

First of all, don't start creating large applications in Tcl right away.
There are several reasons for this, but the the most important one is
that Tcl is a **very** bad language for incremental programming, when
you want structure too... The best advice I can give you is to try
tcl/tk out, see other applications and read about Tcl and Tk's
possibility. When you've done all these things you'll understand what it
can do, and then you start to **design** your applications before you
program. The worst thing you can do in Tcl is to program a basic version
of your interface, and then start to patch it until it is what you
really want\! I know that...I've tried it myself :-(

When you have a better understanding of Tcl, you'll find that it lacks
composed types: the only types you have in Tcl are strings, list of
strings, and associative arrays (also known as maps in other languages).
The associative arrays *can not* contain arrays them self\! This is a
significant problem if you use Tcl as a programming language for larger
programs. One solution for this problem is to create *virtual modules*.
The idea is that you name your variables with a module prefix. If
you,for example, have some part of your program which takes care of
help, you may see this as a module, and variables used by this part of
the program are all name something like help\`*variable*. This strategy
makes it a bit harder to expand variables since the pling character is a
word separator in variables. This means that if you write $a\`b, Tcl
will try to expand a variable called $a. So, to expand this variable you
would write ${a\`b} or \[set a\`b\].

## <span id="installation">Installing Tcl/Tk</span>

It is very simple to install Tcl/Tk. Since you may also desire to expand
your Tcl/Tk installation with additional tools like TclX, \[Incr Tcl\]
or Tix, here is a short description of how you would do it.

* Get Tcl and Tk from <ftp://ftp.smli.com/pub/tcl/>

* unpack it with tar xvzf *file name*

* cd to the Tcl directory

* type ./configure, and add --prefix=*directory* if you want place it
    in another directory than /usr/local/{bin,lib,man}

* type make, to compile Tcl

* type make install -k, to see that *make install* does the right
    thing (You should always do that\!)

* type make install, to install Tcl on you system.

* cd to the tk directory, ./configure, make, make install -k and
    finally make install

* if you install in /usr/local instead of /usr, you should put
    /usr/local/bin in front of /usr/bin, to ensure that it is your newly
    installed version of Tcl/Tk, which is called, and not the one which
    comes with your standard Linux installation.

* Now it's a good time to print out the manual pages for Tk (You don't
    need the ones for Tcl as much), do this by cd'ing to the doc
    directory, which is a sub directory of Tk. In this directory you'll
    find the source for the manual pages.
    
    You can view these using the command:
    
    ``` 
        man ./filename.n
        
    ```
    
    or, you can convert these to plain ASCII text files which can be
    viewed or printed using a BASH shell script such as:
    
        #!/bin/sh
        #
        for SUFFIX in 1 3 n
        do
            find -name "*.$SUFFIX" -print |
            while read FILE
            do
                echo -n "converting $FILE to ${FILE%%.$SUFFIX}.txt ..."
                groff -Tascii -mandoc $FILE 2>/dev/null | col -b > ${FILE%%.$SUFFIX}.txt 
                echo "done."
            done
        done
        echo 
        echo "Finished converting files."
        echo
    
    If you wish to convert these to PostScript format for printing, you
    can use a similar BASH script:
    
        #!/bin/sh
        #
        for SUFFIX in 1 3 n
        do
            find -name "*.$SUFFIX" -print |
            while read FILE
            do
                echo -n "converting $FILE to ${FILE%%.$SUFFIX}.ps ..."
                groff -Tps -mandoc $FILE > ${FILE%%.$SUFFIX}.ps 2>/dev/null
                echo "done."
            done
        done
        echo 
        echo "Finished converting files."
        echo

## <span id="versions">Tcl/Tk versions</span>

There are a number of different Tcl/Tk versions, so to help avoid
confusion, here is a list of the differences:

* Tcl 7.0 / Tk 3.0  
    This version should be considered outdated. If you find applications
    which require this version (and not Tcl 7.3 / Tk 3.6) you should
    look for other programs which do the same job (I believe they
    exist\!)
* Tcl 7.3 / Tk 3.6  
    This was the first version with really great success (I believe...)
    Many applications still use this one, since the next version was
    released only about a year ago. Many differences exist between this
    release and the official Tcl 7.4 / Tk 4.0 release, so applications
    written for this release may not work with version 7.4 / Tk 4.0
* Tcl 7.4 / Tk 4.0  
    This is the newest official release from the author of the program
    and is the one that most people are currently using.
* Tcl7.5a2 / Tk4.1a2  
    This is an *alpha* release of the next version of Tcl, with support
    of dynamic loading, and Windows (The operating system we all love to
    hate). You should only use this if you really want dynamic loading
    or need Tcl for Windows, since no new features has been added so far
    (as far as I know).

## Other resources

Here is a list of resources you should check out if you plan to use Tcl:

Well first of all, the USENET group for Tcl/Tk is **comp.lang.tcl**. The
traffic is heavy, but don't fear: it's worth it..\!

### Extensions

A few extensions are worth mentioning here.

#### Tix

Tix is a set of Mega widgets for Tk. If you plan to use Tk you should
really check out Tix. Personally, I've been using Tk for about 2 years,
and when I learned about Tix I smashed my head against a wall....Tix was
just the extension I needed (WE ALL DO\!\!\!\!). A mailing list exists
for Tix, to subscribe, please send mail to the author Ioi Lam
([ioi@CS.Cornell.EDU](mailto:%20ioi@CS.Cornell.EDU)). Also, be sure to
check out the [Tix home
page](http://www.cis.upenn.edu/~ioi/tix/tix.html) for additional
information.

#### \[Incr Tcl\]

\[Incr Tcl\] is an object orient extension for Tcl. I have never tried
it, but from various USENET postings regarding Itcl, it appears that
this is one of the best object oriented extensions available for Tcl.

#### TclX

TclX is a package which provides a lot of functions for Tcl which are
useful for System Administrators. This is the packages which makes it
possible to use Tcl instead of Perl.

### WWW places

* [Tcl/Tk Resources](http://web.cs.ualberta.ca/~wade/Auto/Tcl.html)  
    This is a collection of resources for Tcl/Tk.
* [Tcl/Tk Project At Sun Microsystems
    Laboratories](http://www.sunlabs.com:80/research/tcl/)  
    This is the official Tcl/Tk page
* [The Tcl/Tk
    Faq](http://www.cis.ohio-state.edu/hypertext/faq/usenet/tcl-faq/top.html)  
    Check this one out before you ask on the news\!\!
* [Tcl WWW Info](http://www.sco.com/Technology/tcl/Tcl.html)  
    A lot of references for Tcl/TK
* [The Dotfile Generator](http://www.imada.ou.dk/~blackie/dotfile/)  
    This is a configuration tool for emacs/tcsh/fvwm written in Tcl/Tk
    by ME (I just had to make a short advertise ;-)

\[I want to publically thank Jesper for his very gracious offer to write
this article which was no small feat. Please do not hesitate to drop him
a note and send your comments, suggestions, and most of all a very
hearty thanks. Also, if you're interested in joining the **Dotfile
Generator** development team, let Jesper know -- he'll be glad to let
you know how you can join this effort\! Drop him a note at:

Jesper Pedersen [\<blackie@imada.ou.dk\>](mailto:%20blackie@imada.ou.dk)

Thanks\! --John\]

