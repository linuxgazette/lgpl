# Using Strace

Autor: Jesper Pedersen

Tłumaczył: 

---

This short overview will describe some of the things you can do with
**strace**. First of all, *what is strace*? Strace is a program which
traces system calls, so that you can see what other programs do
internally. This article will not describe how strace works, or any of
its many options, but rather give some examples of what it can be used
for.

1.  Which files does xterm search for information in, when it starts?
    Try "strace -o strace.info xterm" and when the xterm window appears
    on the screen, close it and search for open in the strace.info file:

    ```
    open("etc/ld.so.cache" O_RDONLY)      = 4
    open("home/blackie/.Xauthority" O_RDONLY) = 5
    open("usr/X11R6/lib/X11/locale/locale.alias" O_RDONLY) = 5
    open("usr/X11R6/lib/X11/locale/locale.alias" O_RDONLY) = 5
    open("usr/X11R6/lib/X11/locale/locale.dir" O_RDONLY) = 5
    open("usr/X11R6/lib/X11/locale/C/XLC_LOCALE" O_RDONLY) = 5
    open("home/blackie/.Xdefaults-blixen" O_RDONLY) = -1 ENOENT (No such
    open("usr/X11R6/lib/X11/app-defaults/XTerm" O_RDONLY) = 5
    ```

    Well what does this mean?

    First, you can see that it is the system call **open** which has
    been called. Second, you can see which file has been opened (or was
    tried to open). Next, you can see which mode the file has been open
    in (see open(2)) And finally, you can see which file handler (if
    any) was returned from the system call.

    Of course it's not xterm which reads all these files, but X11, which
    allows xterm to map the window. BUT, if you wanted to know where to
    put your app-default files, you would have found it.

2.  My program seems to hang though it uses a lot of CPU power, what
    does it do? Try "strace -p " PID is the process id of the process
    which is the problem. If the process is trapped in a "while true"
    loop, strace will say nothing, and you are out of luck, but if
    strace is waiting for a file to appear or something like that,
    strace may say:

    ```
    access(".hi", F_OK)                     = -1 ENOENT (No such file or directory)
    access(".hi", F_OK)                     = -1 ENOENT (No such file or directory)
    access(".hi", F_OK)                     = -1 ENOENT (No such file or directory)
    ```

    which means that it tries to access the file .hi repeatily. Knowing
    this, you may want to check this file.

3.  Does tail -f do busy waiting or .... strace tail -f \<file\> ===\>
    tail -f set an alarm and wake up every second to check the file for
    input.

4.  My program just crashes, and I haven't written it, so I can't just
    debug it. Well, BTW, my grandmother runs this program without any
    problem, what do I do? (For your information: the author, and all
    his family is dead -- no technical support) Why don't you try
    strace, it might turn out that the program eg. opens a file your
    grandmother has, but you don't. But don't check the return code from
    the open system call, and then, later on it starts reading from the
    unopened file....

5.  It's all up to you, only the imagination sets the limit!

Kind regards Jesper.

\[Again, I want to thank Jesper, and all those who have taken the time
to sit down and write up their suggestions and ideas! There's no way
that I could write with the breadth of experience that many of you
offer. Please don't forget to drop Jesper, or any of these authors, a
note and let them know that you appreciate their efforts!

Thanks! --John\]

