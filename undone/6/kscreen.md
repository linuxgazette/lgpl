# KScreen followups

Autor: Mats Andtbacka, Matt Welland, Jon Klippenstein

Tłumaczył:

---

\[One of the cool things about Linux is -- as has been pointed out over
and over again -- that there are often a myriad of ways to get the same
task done. As the following three messages point out, this is very much
the case with the kscreen function presented in last month's LG which
clears up a buggered screen. Thanks to all who wrote back\! -- John\]

Date: Sat, 09 Dec 1995 18:09:23 CST
Sender: \<mandtbac@ra.abo.fi\>
From: Mats Andtbacka \<mandtbac@abo.fi\>
Subject: Linux Gazette: kscreen

I was just reading the Nov. edition, and ran into your defining
kscreen() in BASH. Nice little reimplementation of reset(1). ;-)

Not a tip, but a curious coincidence: On every system I've used - Linux,
SunOs, Solaris - cat'ing the ls(1) binary to the screen does the very
same. On Linux consoles, dumb VT220 clones, MS-Kermit doing its VT320
dumb terminal impersonation; haven't tried the Windoze terminal yet,
mostly because I haven't screwed it up like that yet. ;-)

I've no clue \_why\_ it works, presumably those ls binaries contain
terminal control escape sequences somewhere near the end of the file;
but the curious fact remains that it always does seem to work. Mention
this to newbies, and watch them go "so what"; mention it to Unix
"journeymen", and watch their jaws drop. Gurus tend to know it, but then
they would...

    -- 
    " ... got to contaminate to alleviate this loneliness
          i now know the depths i reach are limitless... "
            -- nin

Date: Tue, 12 Dec 1995 14:48:51 CST
Sender: \<welland@node71.tmp.medtronic.COM\>
From: Matt Welland \<welland@node71.tmp.medtronic.COM\>
Subject: Fixing hosed screen font on a VT

I've had pretty good luck simply entering the command "setfont" Seems to
fix the screen every time.

    -Matt
    
    P.S. Thanks for the Gazette!
    -- 
          ,------------------------------------------------------------.
          | Matt Welland            | Medtronic/Micro-Rel              |
          | Senior Design Engineer  | IC CAE group                     |
          `--------------welland@tmp.medtronic.com---------------------'

Date: Sun, 17 Dec 1995 01:14:26 CST  
From: Jon Klippenstein \<jon@dparrot.bohica.net\>  

Hiya\!

I just started reading the LG, but I think it's great\!\! I downloaded
all the issues tonight, so I can view them in netscape on me linux box.
(I've gotta junk freenet account, running lynx, as my only net.access,
except for my dparrot.bohica.net UUCP link).

Anyway, a couple of things I'd like to point out about the ... uhh (zips
back to X) ... November issue:

1\) Under 'Using kscreen to clear the screen': I didn't know about the
'echo ne "\\017"' trick, but if you are ever stuck, and forget kscreen
(yah right :)), I originally used echo ^V (control-V) hit Escape, c and
then hit enter. Same thing I'm sure, but I thought I'd point it out :)

2\) Under 'Space Savings with a Floppy Library': You say that you like
using ext2 for floppys, and I'd have to agree that the filenames feature
is nice, but I was reading on the Linux 8086 website, that they are
going to use the minix filesystem. To get to the point, from what I've
read, MINIX's filesystem is very small, compact and fast on floppys. I'd
have to assyme that's why Slackware uses it for their boot floppys.

Anyway, just me 2 cents..

jon

    p.s. I'm only in Jr. High School, am still starting with linux, but IT's 
    GREEEEAAAAAAAAAAAAAAAAAAAAAAAATTTTTTTTTTTTTTTT!!!!!!!!!!!! :)
    
    --
    Jon Klippenstein -- jon@dparrot.bohica.net 
    
    Linux: The Choice of a GNU Generation

