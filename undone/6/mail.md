# Salutations and the MailBag

Autor: John M. Fisk

---

Well, howdy\! :-)

Happy 1996\! I had a nice, albeit event-filled, Christmas break and am
now glad to be back in school and starting a new semester. I wanted,
once again, to say a very heart-felt "thanks\!" to everyone that took
the time to write. I know that y'all are pretty busy and I appreciate
the time that many of you took out of busy schedules to write.

I've tried to include a number of interesting and informative letters
below. Since my mailbox, at last count, has ballooned to 700+ messages
over the past couple months, I can't squeeze all your messages in here
:-) although I've tried hard to drop everyone at least a short note of
thanks.

Let me encourage you to read these things\!

Seriously.

There are a LOT of quick tips and excellent ideas here. I haven't set up
a "mailto:" for each person that wrote, but if you find something useful
then by all means drop that person a note and let them know about it.
Most of all...

Enjoy\!

---

![](../gx/letter.gif) **Kevin Pearson**  
Date: Sat, 06 Jan 1996 18:54:10 CST  
From: Kevin Pearson \<adonis2@localnet.com\>  
**Subject: African American Pioneer Link**  

On Jan. 1st, was the release of the two newest Black Educational Video.
The name of the program is called: "A Salute to Our Black Pioneers" -
Profife: Dr. Charles R. Drew, Credited with saving thousands of lives
daily due to his invention of plasma \* and \* Profile: Frederick M.
Jones, invented the first refrigeration unit for long haul trucks.

We at Adonis are creating our home page at this time. We are let
everyone one know about our company, which is producing Black
Educational video programs. We would like to use your like address so
that others can link up to your services. Also we would like our
infomation be part of your information home page. If you can get back to
me and see what we can do. Thank you. Kevin Pearson, President Adonis
Productions

P.S. Our Home Page will be online on Martin Luther Kings Birthday

<http://www.adonis.com>

    -- 
    ĐĎŕĄ

![](../gx/letter.gif) **Jeremy Laidman**  
Date: Sat, 23 Dec 1995 22:56:45 CST  
From: Jeremy Laidman \<JPLaidman@ACSLink.net.au\>  
**Subject: Another user for setterm**  

John

I hope you find this useful enough to include in the Gazette.

This is something I use in my Unix profiles to guide my eyes to previous
command lines. By highlighting the prompt I can quickly tell where
output from a command ends and my keystrokes begin.

For a bash shell, in my .profile I have:

``` 
  export PS1="\[`setterm -foreground green`\]\[`setterm 
                     -bold on`\]\h:\w\\$\[`setterm -default`\] "
```

My prompt appears as:

```
hostname:/current/dir$
```

with the whole prompt a bright green (looks OK on a black screen). If
I'm logged in as root, the $ becomes \#. For more info on bash-isms,
check out the bash man page. I use variants of this technique under
other shells and on other versions of UNIX (see example below).

Some explanation for the curious:

The brackets \\\[ and \\\] are required to inform bash of escape
sequences (as generated by setterm. Otherwise, command-lines that extend
to multiple lines will not wrap quite right after a bit of editing.

Note the extra backslash escaping the $. It won't show as \# for root
without it. I could have enclosed the whole string in single quotes, and
not had to use the extra bashslash, but this delays the execution of the
'setterm' commands to when the prompt is displayed (can be useful)
rather than when it is set.

For systems without setterm, but with terminfo capabilities, use tput,
replacing \`setterm -foreground green\` with \`tput bold\`, and
\`setterm -default\` with \`tput sgr0\`.

For non-bash shells, the brackets \\\[ and \\\] for escape-sequences are
not needed. For a non-Linux korn-shell (ksh) account I have, I use:

``` 
export PS1="`tput bold``hostname`:\$PWD$`tput sgr0` "
```

This does everything except show \# for root.

Congratulations on a fab publication. Most helpful. I like the quick
tips idea. So much that makes more Linux useful for me has been shown to
me by fellow Linuxers who just discovering things accidentally. Ideal
snippets for such a publication.

Cheers

    ---------------------------------------------------------------------
    Jeremy Laidman, Internet/Netware Contractor  JPLaidman@ACSLink.net.au
    Canberra, Australian Capital Territory    Phone: (61 6) 241 7969 (ah)

![](../gx/letter.gif) **Simon Arthur**  
Date: Sat, 02 Dec 1995 00:07:33 CST  
From: Chroma \<chroma@mindspring.com\>  
**Subject: FvwmWinList modification**  

I thought you might be interested in a cool modification I made to
FvwmWinList.

I made a simple change to FvwmWinList for my own use. To make it more
like the Windows 95 taskbar, I modified ButtonArray.c

In function DoButton, I changed the line

    XDrawString(dpy,win,graph,x+newx,y+3+ButtonFont->ascent,ptr->title,strlen(ptr->title));
    
    to read  
    
    XDrawString(dpy,win,graph,x+2,y+3+ButtonFont->ascent,ptr->title,strlen(ptr->title));

This removes the centering. I find this works much better if you have
FvwmWinList on the left side of the screen as it lets you see the name
of the window even if FvwmWinList is partially covered.

Here's what I have in my .fvwmrc:

    *FvwmWinListBack #908090
    *FvwmWinListFore Black
    *FvwmWinListFont -adobe-helvetica-bold-r-*-*-10-*-*-*-*-*-*-*
    *FvwmWinListAction Click1 Iconify -1,Focus
    *FvwmWinListAction Click2 Iconify
    *FvwmWinListAction Click3 Module "FvwmIdent" FvwmIdent
    *FvwmWinListUseSkipList
    *FvwmWinListGeometry +3-48
    
    Function "InitFunction"
            Module  "I"     FvwmWinList

    ----------------------------------------------------------------------------
    Simon Arthur
    chroma@mindspring.com            http://www.mindspring.com/~chroma/home.html
    hypemaster@hyper-g.com   http://www.mindspring.com/~chroma/hgc/hgc-home.html

![](../gx/letter.gif) **Tim Newsome**  
Date: Fri, 29 Dec 1995 09:38:00 CST  
From: Tim Newsome \<drz@cybercomm.net\>  
**Subject: RE: Linux Gazette**  

On Thu, 28 Dec 1995, John M. Fisk wrote:

    > >Dunno if the following is in one of the issues I haven't read yet, but here
    > >goes. To have a background picture for X put the following at the end of your
    > >xinitrc:
    > >xv +root -rmode 5 background.gif -quit &
    > >I can't remember what the -rmode 5 means. Probably centered in the screen.
    > >Neat thing, anyway.
    
    > Thanks for the tip!  I've actually run across this and played with 
    > wallpapering the root window using xv -- very cool!  Definitely ought
    > to mention this.

As for tips, I wrote this shell-script a while ago:

    #!/bin/sh
    
    TMP=/tmp/$$_
    MAXLENGTH=1000
    
    for logfile; do
      tail -$MAXLENGTH $logfile > $TMP
      # Don't use mv to keep the permissions the same.
      cat $TMP > $logfile
      rm -f $TMP
    done

It simple goes through all the logfiles you specify on the commandline
and trims them to a 1000 lines. (Nothing really useful, just another 2c
one. I actually like the 2c tips a lot.)

\> and your taking the time to write. Hope you had a merry Christmas --
\> have a Happy New Year\!

Same to you\!

Tim

    Tim Newsome. drz@cybercomm.net. http://www.cybercomm.net/~drz/. HotJava
    rules. Pretending to work is almost as hard as the real thing. Linux >
    Windows. question=(be||!be); answer=42; hack!=crack; IRC=fun; cogite ergo
    rideo. 1+1=3 for large values of 1. CMU here I come! NO CARRIER

![](../gx/letter.gif) **Kurt M. Hockenbury**  
Date: Fri, 08 Dec 1995 21:55:01 CST  
From: Kurt M. Hockenbury \<kmh@linux.stevens-tech.edu\>  
**Subject: Linux Gazette syslog tip**  

In the November issue, you meantion syslog.conf. Here's a quick tip: I
have a line at the end of my /etc/syslog.conf which reads

    # Throw everything on tty8 as well
    *.*                         /dev/tty8

This way, I also get a copy of \*every\* syslog entry on Virtual Console
8 (which I don't otherwise use - no getty running on it, etc). You can
use any unused console you want.

It's handier than having to tail the various logging files, and cheaper
on memory than running a "tail -f" under some shell, and uses no extra
disk space.

When my hard disk makes swapping noises, I can look on VC8 and see the
log of the incoming connection or mail. When I'm configuring or
debugging daemons, it's easy to check what they're logging. :)

Hopefully, other may find this a useful suggestion.

\-Kurt

    -- 
                                                                          [Place]
    snail://USA/07030/NJ/Hoboken/PO Box 5136/Kurt M. Hockenbury           [Stamp]
                                                                          [Here.]

![](../gx/letter.gif) **Lee Olds**  
Date: Fri, 12 Jan 1996 10:40:58 CST  
From: Lee Olds \<leeo@slmd.com\>  
**Subject: Minor nit in oct LG article on TRAP**  

Hi. Linux Gazette looks real good\!

I have a small correction for the act article on TRAP. You said:

    >So, what are the signals that can be trapped? The most common ones,
    >I'm told are the following: 
    >
    >      0 - program exit 
    >      1 - hang-up 
    >      2 - user interrupt (ctrl-c) 
    >      9 - kill signal from another process 

Those are probably the most commonly sent signals. All except for signal
9 could be the most commonly trapped signals as well. But signal 9 is
special and can never be caught, so the shell has no way to trap it.

    -- 
    Lee Olds
    olds@eskimo.com

![](../gx/letter.gif) **Brian Matheson**  
Date: Thu, 21 Dec 1995 22:14:44 CST  
From: Brian Matheson \<bmath@sirius.com\>  
**Subject: Stuff**  

Just wanted to drop you a line and say hey again and let you know that
I've become another of your avid fans\! Since you mentioned that you'd
be doing a little work on the ole' toy box, I thought I'd point out
something in SunSite's Toys directory that's just too groovey to be
overlooked: Xfishtank.

It turns your root window into a fish tank and runs beautifully in the
background while I'm doing other stuff on my poor-boy's 486/33 with 8
megs of RAM, and it doesn't even complain about netscape eating up the
color palette.

Another thing that I'd forgotten about but came across while trying to
set up a ppp connection with our system at work is a little technique
for trimming down those pesky syslogs. I don't know about you, but I
usually forget about them until something doesn't work and then realize
that they've grown to outrageous proportions in my absent-mindedness. So
I came up with a one-liner to grep out the lines for a certain date:

    grep "`date | cut -b5-10`" /var/adm/syslog | cut -b8-

Just make an alias for it and you're all set. Works great on
/var/adm/messages too\!

Many thanks for sharing the great work you do with the rest of us, the
countless hours you spend aren't going unappreciated\!

Happy Hollidays\!

Brian  
bmath@sirius.com  

![](../gx/letter.gif) **Liang Ng**  
Date: Fri, 12 Jan 1996 22:00:14 CST  
From: Liang Ng \<L.S.Ng@ecs.soton.ac.uk\>  
**Subject: Re: Update of xterm title bar. Solution.**  

John M. Fisk writes:

``` 
> Excellent!  I've saved your message and will _definitely_ include it with
> the next edition of the LG!  Glad to see that you were able to get this
> worked out :-)  Also, thanks SO much for taking the time to write and
> offer this suggestion.  
```

Thanks John. I am very pleased to hear that.

BTW, here is a suggestion about xlock.

I am not using xdm. I use startx to start the X Window. When I used
xlock on the X Window, I found that it was actually possible to switch
to the Virtual Console (VC) from which I started X Window. i.e. xlock is
broken\! I, in fact anyone, was able to suspend startx by pressing
ctrl-z at the VC, then kill xlock.

Solution to this: log myself out from the virtual console, before I
leave my desk.

This solution obviously works. But it is a nuisance to see the closing
down message at the VC when X Window is shut down later. So I created a
Bash function startx() which redirect the stderr to /dev/null:

    startx()
    {
       /usr/X11/bin/startx $* 2> /dev/null
    }

Note that the full path of the actual startx must be used or else Bash
will interpret it as recursive function. Note also that alias in Bash
does not carry arguments, hence I have to use function instead.

Of course, if you really want, you could do:

    startx()
    {
       /usr/X11/bin/startx $* 2> /dev/null &
       clear
       logout
    }

Which logs you out from the VC as soon as X Window is started.

Cheers

Liang S Ng

![](../gx/letter.gif) **Tim Wallace**  
Date: Sat, 23 Dec 1995 08:43:30 CST  
From: Tim Wallace \<root@web.tenn.com\>  
**Subject: RE: Stuff**  

John,

Here's the crontab that comes with RH2.0 and RH2.1.

    SHELL=/bin/bash
    PATH=/sbin:/bin:/usr/sbin:/usr/bin
    MAILTO=root
    
    # Run any at jobs every minute
    * * * * * root /usr/sbin/atrun
    
    # Make the whatis databases
    21 03 * * 1 root /usr/sbin/makewhatis /usr/man /usr/X11R6/man
    
    # Make the find.codes database
    47 02 * * * root /usr/bin/updatedb --prunepaths='/tmp /proc /mnt /var/tmp 
    /dev' 2> /dev/null
    
    # Trim wtmp
    45 02 * * * root find /var/log/wtmp -size +32k -exec cp /dev/null {} \;
    
    # Remove /var/tmp files not accessed in 10 days
    43 02 * * * root find /var/tmp/* -atime +3 -exec rm -f {} \; 2> /dev/null
    
    # Remove /tmp files not accessed in 10 days
    # I commented out this line because I tend to "store" stuff in /tmp
    # 41 02 * * * root find /tmp/* -atime +10 -exec rm -f {} \; 2> /dev/null
    
    # Remove formatted man pages not accessed in 10 days
    39 02 * * * root find /var/catman/cat?/* -atime +10 -exec rm -f {} \; 2> 
    /dev/null
    
    # Remove and TeX fonts not used in 10 days
    35 02 * * * root find /var/lib/texmf/* -type f -atime +10 -exec rm -f {} 
    \; 2> /dev/null
    
    # Trim log files
    34 02 * * * root find /var/log/maillog -size +16k -exec /bin/mail -s "{}" 
    root < /var/log/maillog \; -exec cp /dev/null {} \;
    33 02 * * * root find /var/log/messages -size +32k -exec /bin/mail -s 
    "{}" root < /var/log/messages \; -exec cp /dev/null {} \;
    32 02 * * * root find /var/log/secure -size +16k -exec /bin/mail -s "{}" 
    root < /var/log/secure \; -exec cp /dev/null {} \;
    31 02 * * * root find /var/log/spooler -size +16k -exec /bin/mail -s "{}" 
    root < /var/log/spooler \; -exec cp /dev/null {} \;
    30 02 * * * root find /var/log/cron -size +16k -exec /bin/mail -s "{}" 
    root < /var/log/cron \; -exec cp /dev/null {} \;

The wordwrapping should be un-done of course.

Tim

![](../gx/letter.gif) **Tim Newsome**  
Date: Wed, 27 Dec 1995 17:53:03 CST  
From: Tim Newsome \<drz@cybercomm.net\>  
**Subject: Linux Gazette**  

Hi,

You're probably getting millions of these, but I just wanted to say the
Linux Gazette absolutely positively completely rules. Just discovered it
and reading from all of them. You taught me a bunch of stuff about fvwm
there. :-) Dunno if the following is in one of the issues I haven't read
yet, but here goes. To have a background picture for X put the following
at the end of your .xinitrc:

    xv +root -rmode 5 background.gif -quit &

I can't remember what the -rmode 5 means. Probably centered in the
screen. Neat thing, anyway.

Keep it up\!

Tim

    Tim Newsome. drz@cybercomm.net. http://www.cybercomm.net/~drz/. HotJava
    rules. Pretending to work is almost as hard as the real thing. Linux >
    Windows. question=(be||!be); answer=42; hack!=crack; IRC=fun; cogite ergo
    rideo. 1+1=3 for large values of 1. CMU here I come! NO CARRIER

![](../gx/letter.gif) **Matt Welland**  
Date: Tue, 12 Dec 1995 15:49:22 CST  
From: Matt Welland \>welland@node71.tmp.medtronic.COM\>  
**Subject: System configuration - beyound RCS**  

John,

I've been using RCS for some time to control my system files but I found
I needed something more. I wanted to be able to check out an entire
machine configuration so I installed cvs. I checked in all the pertinent
files in /etc, /usr/lib/uucp, /usr/lib/news and so forth and gave them a
module name. Now I can check out any of several machine configurations
each with its own hostname and setup. CVS took a little learning but if
you know and understand RCS it is pretty easy. Follow the instructions
that come with CVS and keep a system backup until you are confident
everything is rock solid and predictable.

\-Matt

    -- 
          ,------------------------------------------------------------.
          | Matt Welland            | Medtronic/Micro-Rel              |
          | Design Engineer         | IC CAE group                     |
          `--------------welland@tmp.medtronic.com---------------------'

![](../gx/letter.gif) **Ed Petron**  
Date: Sat, 30 Dec 1995 11:39:04 -0500  
From: Ed Petron \<epetron@leba.net\>  
**Subject: Linux**  

John, I've been looking at your Linux home page and it's really great\!
I've got a home page devoted to technical and network computing at
<http://www.leba.net/~epetron>. It's always great to "meet" another
Linux afficianado. Drop me line if you get a chance. I'll be looking
forward to hearing from you.

Ed Petron

![](../gx/letter.gif) **Morpheus**  
Date: Fri, 15 Dec 1995 23:40:26 CST  
From: Morpheus \<morpheus@markwoon.student.Princeton.EDU\>  
**Subject: RE: Comments and xterm titlebars**  

On Fri, 15 Dec 1995, John M. Fisk wrote:

    > Hmmm... sounds interesting.  Are these shell scripts or binaries?  I've
    > seen the pushd and popd programs mentioned before but have never run
    > across them.  Do you recall where you picked these up?

Whoops. I just checked, and pushd and popd are shell built-ins. See how
much cooler tcsh is? ;^) (Just joking, don't want to start a
my-shell-is- better-than-your-shell flamewar here.)

Another neat tcsh built-in is "where" which searches your path for a
file name you provide (eg. "where popd" in which case it returns "popd"
is a shell built-in)

And on killing proceses, check out killall, it should come with the
standard distribution. It'll kill all proceses with the name you provide
(the slay alias I sent you probably only works on the first, I've never
tested this). Very handy with runaway processes that spawn child
processes faster than rabbits in heat.

    > I'm not sure... I've not enough of a guru to know how you'd enter a 
    > control or escape character from the keyboard.  I simply used a text
    > editor to do this and then messed around with it after creating a 
    > small shell program.

BTW, the emacs sequence for inserting control sequences is Control-q,
followed by the desired control sequence. Adding this to the Nov. issue
of LG will probably help out emacs users, fitting in well with your
description on how to do so with vi... ;)

``` 
> Have a Merry Christmas and Happy New Year.  
```

You too\!

    ~Morpheus~~~~morpheus@vanaheim.princeton.edu~~~~~~~~E Pluribus Linux~~~~~

