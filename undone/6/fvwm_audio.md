# FVWM and Audio!

Autor: Nic Tjirkalli

Tłumaczył: 

---

## FVWM and Sound Effects

The current trend for GUI based operating systems appears to be
multimedia - incorporating the use of cute little sounds when a program
starts or a window is maximised or minimised.

Well, there is no reason why dozing or apparently warping OSs should
have the monopoly on this - the same can be done with the FVWM in X. As
of verision 1.24 (release r) of the FVWM, an **FvwmAudio** module has
been included, as a standard module. Quoting the FvwmAudio (1.0) man,
the FvwmAudio module :-

"communicates with the FVWM window manager to bind audio sounds to window 
manager actions. Different audio queues may be assigned to any window manager 
action. FvwmAudio can be used with any independent audio player, and therefore 
requires no special audio library APIs of its own. Simple in its design, it 
merely binds the audio filenames to particular actions and forks off the audio 
player program with the appropriate filename when that action occurs."

To incorporate sound effects with the Fvwm, you will need :-

1.  A sound enabled/capable Linux System
2.  Version 1.24r, or higher, of the Fvwm
3.  An audio player utility
4.  Instructing FVWM to play the sounds of your choice

-----

## 1. A sound enabled/capable Linux System

This is way out of the scope of this discussion, but there is a wealth
of info out there on the net pertaining to sound and linux  

-----

## 2. Version 1.24r, or higher, of the Fvwm

You will need to get and compile version 1.24r (or higher) of the Fvwm,
unless you already have FVWM 1.24r or higher. I have not been able to
found out how to get the version number of FVWM. I have compiled and
tested the sound module with version **2.0.39** of FVWM. You can
download the fvwm source from **Sunsite** or your favourite Linux
mirror.

\[To find out which version of fvwm you're using you can enter the
following either in an xterm or at the console -- note that what is
printed to the screen is actually an error message that happens to
contain the Fvwm version number.

``` 
fvwm --version
```

What you'll see might be something like:

``` 
Fvwm Ver 1.23b

usage: fvwm [-d dpy] [-debug] [-f config_file] [-s]
fvwm: another WM is running
```

\--John\]

**Compile** - have no fear, Fvwm compiles **VERY EASILY** and the
included documentation is complete and useful.

To compile Fvwm,

1.  Pull down the Fvwm source. The file you need is 
    ftp://sunsite.unc.edu/pub/Linux/X11/window-managers/fvwm-2.0.39.tgz

2.  Place the Fvwm archive file where you normally place source code - I
    use */usr/src*.

3.  Uncompress and untar the archive as follows :

```sh
tar -zxvf fvwm-2.0.39.tgz
```

4.  Compile as follows: (also, read the included *README.install*
    file)

    1.  Change into the newly created (the tar -zxvf process made it)
        fvwm directory, as follows:

        ```
        cd fvwm
        ```

    2.  Run xmkmf as follows:

        ``` 
        xmkmf 
        ```

    3.  Make the makefiles and compile the actual code, as follows:

        ```
        make Makefiles
        make all
        ```

    4.  If the make went well, install the new Fvwm and the
        documentation, as follows **(note, you will probably need root
        privileges to do the actual installation)**:
 
        ```
        make install
        make install.man 
        ```

## 3. An audio player utility

The FvwmAudio module requires an external audio player to play sound
files.

**NOTE:-**

Extract from fvwm-2.0.39/modules/FvwmAudio/README

This version of FvwmAudio has builtin rplay support which does not need
to invoke an external audio player to play sounds. The rplay support is
enabled when FvwmAudio is compiled with HAVE\_RPLAY defined (see the
Imakefile) and when FvwmAudioPlayCmd is set to builtin-rplay. Rplay can
be obtained via anonymous ftp at ftp.sdsu.edu in the /pub/rplay
directory and at ftp.x.org in the /contrib/audio/rplay directory.

However, I have opted to use an external audio player (SOX) for a whole
host of reasons. Pull down LSOX (the linux version of SOX) from
**Sunsite** or your favourite mirror. The file you need is :-
[Lsox-linux.tar.gz](ftp://sunsite.unc.edu/pub/Linux/apps/sound/convert/Lsox-linux.tar.gz)
in the /pub/Linux/apps/sound/convert/ directory. Compile - yes compile
as follows :

1.  Pull down the SOX source. The file you need is :-
    ftp://sunsite.unc.edu/pub/Linux/apps/sound/convert/Lsox-linux.tar.gz.

2.  Copy the Lsox-linux.tar.gz file to the directory you normally use
    for compilations - I use */usr/src*.

3.  Uncompress and unarchive the SOX archive as follows :-

    ``` 
    tar -zxvf Lsox-linux.tar.gz
    ```

4.  Change to the newly created **Lsox** directory, as follows :-

    ``` 
    cd Lsox
    ```

5.  The **INSTALL** file says you should apply the Linux patches -
    **IGNORE THIS** all linux patches are already applied.

6.  Copy the files *Makefile* and *pat.c* into the **SOX** directory, as
    follows :-

    ```
    cp Makefile Sox/
    cp pat.c Sox/ 
    ```

7.  Change into the *Sox* directory and run make, as follows :-

    ```
    cd Sox
    make
    ```

8.  After the compilation has completed, copy the sox binary to a
    directory in your path, I used /usr/bin. Then create symlinks for
    the files *play* and *pplay*, as follows :-

    ```
    cp sox /usr/bin/
    ln -s /usr/bin/sox /usr/bin/play
    ln -s /usr/bin/sox /usr/bin/pplay
    ```

## 4. Obtaing Sound files/Clips

A useful thing to have is a collection of **.au** or **.wav** sound
files/clips to play for different FVWM events. There are loads of sites
on the net to find them, including:

* http://artemis.ess.ucla.edu/sounds/
* ftp://sunsite.unc.edu/pub/multimedia/sun-sounds/
* http://www.cqs.washington.edu/~josh/sounds/


## 5. Instructing FVWM to play the sounds of your choice

A full discusion on configuring the audio capabilites of FVWM is
presented in the FvwmAudio man page. A brief outline is presented here.
Please refer to the above mentioned man page for more info.

There are two requirements for activating the FVWM audio features :-

1.  Invoking the FvwmAudio module.
2.  Assigning a sound player and the sounds corresponding to. window
    manager events

---

**NOTE**

The configuration info for allowing the Fvwm to produce sounds can
either be placed in

* the global/system wide fvwm configuration file -
    `/usr/X11/lib/X11/fvwm/system.fvwmrc`
* or the individual users configuration file - `\~/.fvwmrc`

---

1.  Invoking the FvwmAudio Module.

    For sound effects, FVWM needs to be instructed to load the audio
    module. As this module is spawned by FVWM, it cannot be invoked via
    the command line.

    Add the following statement to the fvwm configuration file, uder the
    *Function "InitFunction"* section :

    ```
    Module "I" FvwmAudio
    ```

2.  Assigning a sound player and the sounds corresponding to window manager events.

    The following configuration options can be inserted in the required
    FVWM configuration file (either *.fvwmrc* or *system.fvwmrc*)

        *FvwmAudioPlayCmd /usr/bin/play

    * This command specifies the audio program to be used to play the
        sound events. For example,

        **\*FvwmAudioPlayCmd /usr/bin/play**  
        will cause FVWM to use the play program  

        **\*FvwmAudioPlayCmd builtin-rplay**  
        will cause FVWM to use the built in rplay support  

        *FvwmAudioDir /usr/lib/sounds

    * This statement specifies the directory in which to locate the
        audio files. It is ignored if the built in rplay audio player is
        used

        *FvwmAudioDelay 5

    * This specifies that a sound event will only be played if it
        occurs at least 5 seconds after the previous sound started to be
        played. This option is **VERY** useful as it prevents several
        sounds from trying to be played at the same time.

        *FvwmAudio  window-manager-event sound

    * This specifies that when event *window-manager-event* occurrs,
      the sound file *sound* will be played.

      Possible *window-manager-events* include :

      * add\_window
      * configure\_window
      * deiconify
      * destroy\_window
      * end\_windowlist
      * focus\_change
      * icon\_name
      * iconify
      * lower\_window
      * new\_desk
      * new\_page
      * raise\_window
      * res\_class
      * res\_name
      * shutdown
      * startup
      * toggle\_paging
      * window\_name
      * unknown

So, for example, if you want to use the */usr/bin/play* utility to play
audio files which are loacted in the /usr/local/sounds directory and
have a 6 second sound delay and play the file *bang.au* when a window is
iconified and play *up.wav* when a window is raised, you would add the
following statements to your *.fvwmrc* file :

```
*FvwmAudioPlayCmd /usr/bin/play
*FvwmAudioDir /usr/local/sounds
*FvwmAudioDelay 6
*FvwmAudio iconify bang.au
*FvwmAudio raise_window up.wav
```

---

\[Pretty cool, eh? Betcha always wanted to to hear that
gravelly-Dirty-Harry warning "*Go ahead... Make my day...*" when you
reached for the 'ol **kill** button to zap some ill-mannered program...?

\[Well, here's your chance\! :-) Don't forget to drop Nic and note and
let him know how things worked out. --John\]

