# Yahoo! Got Sendmail to queue!

Autor: John M. Fisk

---

Yup, it's true. Finally got sendmail to "work correctly". What I mean is
this... I've got a standalone Linux box that has intermittent connection
to the 'Net via a PPP connection through the University. Previously,
when trying to send mail to a remote address, sendmail would complain
that it was undeliverable and bounce it back as a reject...

Arghh...

Several months ago, I discovered XF-Mail, a graphical XForms-based mail
client, which can be configured to queue outgoing mail. I've been
happily using it ever since. However, I recently got sendmail to queue
up remote mail while still delivering local mail right away\! Very
cool...

But the hero of this story actually is...

**Leif Erlingsson**
[\<Leif.Erlingsson@mailbox.swipnet.se\>](mailto:%20Leif.Erlingsson@mailbox.swipnet.se)

Truth is, Leif wrote a very handy little guide to getting this done.
It's part of the **Linux HOWTO** series and is one of the mini-HOWTO's.
I happened to come across this one on the **InfoMagic Linux Developer's
November, 1995** CDROM on disk 1 in the /HOWTO/mini/ directory. If you
haven't been purusing these valuable documents from time to time...

you ought to. :-)

First, let me extend a very heartfelt thanks to Leif for making this
available. And, to make this easy for all of you to find, I've included
a plain text copy of the [Queue-R-Mail mini-HOWTO](./misc/queue-r-mail)
which you should *definitely* read in its entirety before plunging into
this.

So... let's see what Lief did to make this bit of magic happen...

**Stuff you'll need:**

Here's a list of the programs and packages that I used to get this
working. FYI, I'm currently using the Slackware 3.0.0 ELF distribution
and so you'll find these packages at any Linux archive site that mirrors
that Slackware distribution.

* **sendmail.tgz** - this is the stock sendmail distribution that
    comes with Slackware 3.0.0. It contains the precompiled binaries and
    config files for BSD sendmail ver. 8.6.12. It also contains the
    binaries for the program **deliver** which is used for local mail
    delivery.
* **smailcfg.tgz** - this archive contains the full set of
    configuration files used by BSD sendmail. In order to create a new
    /etc/sendmail.cf file you'll need to install these files.
* **m4.tgz** - the GNU m4 macro pre-processor. This is what actually
    does the work of creating the `*.cf` file.

Now it goes without saying that you can probably find these packages and
tools in other distributions and should be able to work with these. I'm
using these as an example -- also, Pat Volkerding has made the job of
setting this stuff up pretty simple and so we'll gladly make use of
this\! :-)

At this point, it might be helpful for you to repeat after me...

THIS WORKED FOR JOHN... IT MIGHT NOT WORK FOR ME...  
THIS WORKED FOR JOHN... IT MIGHT NOT WORK FOR ME...  
THIS WORKED FOR JOHN... IT MIGHT NOT WORK FOR ME...  

You get the picture.

The standard disclaimer at this point is: I'm NOT a Linux/UNIX/sendmail
guru and probably won't be for a couple decades. In light of this, you
should realize that while this worked for me, it might not work for you.
There's an old proverb about "the blind leading the blind..."

*Caveat emptor*

Anyway, now that I have *that* off my chest :-)

From this point on, it was pretty easy to get things going. I read
through the "Queue-R-Mail" document a few times, AND skimmed over the
/usr/src/sendmail/README's that came with the distribution. What I ended
up doing was the following:

\[1\] logged in as root (yeah, I've finally broken the "gotta be root
all the time" habit :-) and changed directory to the
/usr/src/sendmail/cf/cf directory.

\[2\] in the /usr/src/sendmail/cf/cf directory, I located the file
"linux.smtp.mc" which I copied to "fiskhaus.mc". I did this at the
instruction of the **README.linux** file that comes with the Slackware
distribution. There are two pre-configured \*.mc files that come with
the smailcfg.tgz package that have already been customized for Linux --
one to be used for SMTP-only sites with a nameserver, and one for
SMTP-only sites without a nameserver. Since I have access to a
nameserver through the University, I used the first one.

\[3\] having created my own "fiskhaus.mc" file, I made the following
modifications:

this is the original **linux.smtp.mc** file:

    include(`../m4/cf.m4')
    VERSIONID(`linux for smtp-only setup')dnl
    OSTYPE(linux)
    FEATURE(nouucp)dnl
    FEATURE(always_add_domain)dnl
    MAILER(local)dnl
    MAILER(smtp)dnl

and this is the modified **fiskhaus.mc** file:

    include(`../m4/cf.m4')
    VERSIONID(`linux for smtp-only setup')dnl
    OSTYPE(linux)
    FEATURE(nouucp)dnl
    FEATURE(always_add_domain)dnl
    dnl # The following is the suggested method for getting Sendmail 8.6.x to
    dnl # queue up remote mail instead of sending it immediately -- from the
    dnl # `Queue-R-Mail' mini-HOWTO.
    dnl #
    dnl # Defer Delivery to "expensive" mailers until next time the queue is
    dnl # processed using "OcTrue" and make sure smtp mailers are "expensive".
    dnl # (The "sendmail" book, Chapter 30: Options, "Oc - Don't connect to expensive
    dnl # mailers".)                        / Leif.Erlingsson@mailbox.swipnet.se
    dnl #
    define(`confCON_EXPENSIVE', `True')
    define(SMTP_MAILER_FLAGS, e)
    MAILER(local)dnl
    MAILER(smtp)dnl

Now, those of you who've already read Lief's HOWTO will recognize this\!
Basically, all I did was add the comment lines (denoted by the "dnl" at
the beginning of the line) and the two "define" lines.

I'm NOT going to hazard a guess as to why this works. I've read the
HOWTO and understand the essence of the logic. However, just *exactly*
why these additions work is something that I honestly can't say that I
fully grasp...

Then again, I'm not sure exactly how my *car* works either, but it still
gets me to school each day... :-)

\[4\] once those modifications were added, I simply created the new
\*.cf file by invoking:

``` 
m4 fiskhaus.mc > ../../FiskHaus.cf
```

in the /usr/src/sendmail/cf/cf directory. This puts the newly created
file in the /usr/src/sendmail directory.

\[5\] the last step was to copy the "FiskHaus.cf" file to
/etc/sendmail.cf **AFTER** I'd made a backup copy of the current
sendmail.cf file. (Remember... "rm is forever...")

    cp /etc/sendmail.cf /etc/sendmail.cf.bak
    cp FiskHaus.cf /etc/sendmail.cf

At this point, we're ready to play\! If you do this, you'll need to
force sendmail to re-read the /etc/sendmail.cf file. Doing a **ps -ax**
listing will give you the PID of the sendmail daemon:

    FiskHaus /usr/src/sendmail # ps -ax

    
      PID TTY STAT  TIME COMMAND
        1  ?  SW    0:01 (init)
        6  ?  S     0:00 (update)
        7  ?  S     0:00 update (bdflush)
       30  ?  S     0:00 /usr/sbin/syslogd
       32  ?  SW    0:00 (klogd)
       34  ?  SW    0:00 (inetd)
       36  ?  SW    0:00 (lpd)
       45  ?  SW    0:00 (sendmail)
       [...snip!]

Knowing that the PID for sendmail is 45, we can get it to reinitialize
itself using something like:

``` 
    kill -HUP 45
```

Sending sendmail the "HUP" signal causes it to initialize itself once
again, re-reading its configuration file. Having done this, sendmail was
now queuing up all remote mail\!

Major *coup d'etat*\!

Now in reality, I'm still using XF-Mail for doing the majority of my
mail reading and composing. However, it's great to now be able to use
the text-based mail clients such as pine, elm, or even plain 'ol mail to
send off a quick note from the console. I must admit, too, that pine is
still pretty near and dear to my heart. It is a pretty huge program but
it comes with so many features that it really is an excellent mail
client and the one that I occasionally still fall back on when I don't
want to fire up X.

One last point. To check the mail queue, you simply invoke the command:

``` 
mailq
```

This will print out a listing of all the mail in the outgoing queue. To
send queued mail once a connection has been made, you invoke:

``` 
sendmail -q
```

Beauty of it is, you *don't* have to be root in order to get these to
work -- any user can check the mail queue or tell sendmail to attempt to
deliver mail that is in the queue. Also, I used the suggestion in Lief's
HOWTO to set up an **rc.sendmail** file that starts the sendmail daemon
at boot time but does NOT attempt to deliver it. There is a bit of file
shuffling involved with this, but it works quite well and so far nothing
has gotten lost in the process.

I'll leave it up to you and Lief to decide if you want to do this... :-)

Anyway, that's my product testimonial. Have fun and enjoy\!

