# Colophon

Autor: John M. Fisk

---

Hey... you made it\! This is the end, my friend :-)

I don't know about you, but I happen to really like this "Colophon"
idea. Kind of gives me a chance to wrap up loose ends and just chat a
bit... now that the majority of the writing has been done. I honestly
don't write this until everything else is pretty much finished and then
include all of those "... now, where am I going to put *this*" kind of
stuff.

It also let's me ramble a bit, which I'm sure you've noticed that
happens fairly frequently here :-)

Let me, once more, extend a sincere "thanks\!" to everyone that took the
time to write and offer their suggestions, thoughts, corrections, and
ideas. I can't write this all on my own, both because of time and
because I honestly don't know everything there is to know :-) Several of
the authors in this month's LG sent me complete HTML documents that were
pretty much "drop-in's". I am especially indebted to these folks.

Also, thanks to everyone who wrote asking about when the next LG would
come out... I appreciate your interst AND your patience. As you can
tell, these just keep getting bigger and bigger. This has been a HUGE
amount of fun but I probably won't be able to get something out every
month since I'm starting, what appears to be, a pretty busy semester.
I'll try to get something out as often as I can, but I can't promise
that it'll be every month.

Also, in my wanderings, I've come across a few sites that you might want
to drop by... just some things that I found a bit interesting.

* [DTOP Chinese-English desktop publishing program at
    ftp.ifcss.org](ftp://ftp.ifcss.org/pub/software/linux/X11R6/dtop1.4/pub/)
    This is the port of the DTOP Chinese-English desktop publishing
    package to Linux. Quite frankly, it's HUGE. You'll need a bit of
    download time if you don't have a fast network connection, since the
    file you'll need are from 5 MB to 9 MB in size. I tinkered around
    with this and probably won't use it myself, but y'all should have a
    look at it and then send email to the maintainer to let them know
    what kind of interest there is in the Linux version.
* [Popclient FTP site at ftp.mal.com](ftp://ftp.mal.com/pub/pop/)
    For those of you who pick up your mail from a POP server, you might
    be interested in looking over the most recent version of the
    **popclient** program at ftp.mal.com. While a production version
    appears regularly on sunsite and its mirrors, the development
    version has some interesting features that hackers and the
    advertureous might find useful. Drop by and have a look\!
* [Official comp.os.linux.announce (COLA) home site at
    www.iki.fi](http://www.iki.fi/liw/linux/cola.html)
    This site is an absolute must-have for every Linux user's bookmark
    file. It's the home site of the comp.os.linux.announce newsgroup
    maintained by the moderator **Lars Wirzenius**. It contains an
    up-to-date listing of all current and past announcements that have
    appeared on c.o.l.a.
* [Another *excellent* WWW page for the FVWM Window Manager at
    mars.superlink.net](http://mars.superlink.net/user/ekahler/fvwm.html)
    Here's yet another excellent resource for all you FVWM users out
    there. This is the work of **E. Kahler** and provides useful tips
    and suggestions for using FVWM.
* [The GIMP homepage at
    www.xcf.berkeley.edu](http://www.xcf.berkeley.edu/~gimp/)
    This is *the* site where you'll find the **GIMP** (Graphical Image
    Manipulation Program) brought to you by the folks at U.C. Berkeley.
    The site has Linux binaries for both a.out and ELF and is based on
    the idea of plugin's for easy addition of functions. If you're into
    graphics, check this program out.
* [Java Development Kit (JDK) for Linux at
    substance.blackdown.org](ftp://substance.blackdown.org/pub/Java/linux/)
    For all you Web authors out there... here's your chance to join the
    **Java** movement\! :-) You'll find the JDK for Linux there. I can't
    say that I've had the time to try this stuff out, but for those of
    you who want to try to your hand at a bit of Java programming, here'
    your chance.

Finally, a couple of you wrote and asked where to find the program
source for **skim/xskim** and **addressbook-0.5** which I mentioned in
the last edition of the LG. I apologize for not including the URL's and
so here they are:

[skim-0.82.tar.gz](ftp://ftp.cc.gatech.edu/pub/linux/system/News/readers/)
at the GA Tech sunsite mirror
[addressbook-0.5.tar.gz](ftp://ftp.aud.alcatel.com/code/) at the
ftp.aud.alcatel.com ftp site.

For those of you with a budding interest in **Tcl/Tk** programming,
there is a HUGE collection of Tcl/Tk stuff at the alcatel site. If
you're looking for a Tcl/Tk program and can't find it at your favorite
Linux watering hole, this should be the next stop on your search. Also,
you can find this site mirrored by the folks at
[ftp.neosoft.com](ftp://ftp.neosoft.com/pub/tcl/mirror/ftp.aud.alcatel.com/code/).

Well, I guess that's about it. Hope you enjoyed\!

See y'all later!

