# Using the Keypad as a VT Switcher

Autor: John M. Fisk

---

OK, let me 'fess up here that this is *another* minor bit of plagerism
:-)

Using the keypad as a means of changing from one virtual terminal to
another is an idea that I originally came across in the **November, 1995
issue of the Linux Journal** (which is Issue 19). The article entitled
"The Best Without X" was written by **Alessandro Rubini**
[\<rubini@ipvvis.unipv.it\>](mailto:%20rubini@ipvvis.unipv.it) -- the
author of the **gpm** mouse server. Alessandro presents an excellent
discussion of maximizing a Linux system in a non-X Window environment.
He includes a discussion of the programs gpm, open, and SVGATextMode,
and then recommends several text-based productivity tools such as emacs,
jed, mc, minicom, and gnuplot.

Before going on, let me put in a blatant plug for the **Linux Journal**.

Buy it\!
Read it\!

Seriously. The guys at [SSC](http://www.ssc.com) who publish the Linux
Journal are providing a VERY valuable service to the Linux community. In
the last couple issues (Issues 19 - 21) there have been articles on:

* LessTif and the Hungry ViewKit
* Getting the Most Out of X Resources
* The Best Without X
* How to Build a Mac (a review of the Executor Macintosh emulator for Linux)
* Caldera Network Desktop Preview I review
* Tutorials for using *find, grep, glimpse, etc.*
* An Introduction to Python
* Using Linux and DOS Together
* CVS: Version Control beyond RCS

There have also been regular feature articles by **Stephen Uhl** on tips
for Tcl/Tk programming and by **Aeleen Frisch** (author of the must-have
book "Essential System Administration", 2nd Edition, published by
O'Reilly & Associates, Inc.) on issues of Linux system administration
such as using LILO, adding a new disk to your system, and security.

If you haven't been getting this journal, write your mom, tell her you
want your birthday present *now*, and give her the URL for the Linux
Journal. If you mom happens not to be "connected to the 'Net" :-) you
can still have her call in your subscription to:

phone (206) 782-7733

This will put her in contact with the nice folks at the LJ who'll be
happy to take her $22 and start sending you this excellent journal. BTW,
this is a U.S. phone number. In case you think your mother would rather
use plain 'ol snail mail...

Linux Journal
PO Box 85867
Seattle, WA 98145-1867

$22 for U.S. subscribers
$27 for Canadian and Mexican subscribers
$32 for All Others

End blatant plug :-)

Now, where were we...?

Oh yeah... the VT thingie...

Among other suggestions that Alessandro makes in his article is that of
using the keypad as a virtual terminal (VT) switcher. Since the keypad
is rarely used unless you're using a calculator, this isn't too bad an
idea. He didn't really explain fully how to do this and so, after a bit
of tinkering and skimming over a few docs and manual pages, I was able
to get this working and have been using it every since\!

First thing to mention is that you'll need the **kbd** package which you
should be able to find at one of the Sunsite mirrors in the
[pub/Linux/system/Keyboards](ftp://ftp.cc.gatech.edu/pub/linux/system/Keyboards/)
directory. The current version appears to be kbd-0.90.tar.gz. If you're
a Slackware user then you'll be offered this package at installation.
Even if you don't have this installed and you're NOT using Slackware, I
found that it compiled cleanly and installed without a hitch. This is
the first thing you'll need to attend to.

Once you've gotten the kbd package installed, you'll need to create a
keymap file to tinker with. Change to the /usr/lib/kbd/keytables
directory and then make a copy of the default "defkeymap.map" file. I
did something like:

```
cp defkeymap.map mykey.map
```

which created the "mykey.map" file I then used for customizations. Now
at this point, you'll probably be tired of my saying this, but...

**Be Careful!

Remapping the keyboard can get you into **serious** trouble if you
somehow manage to bugger it up. Loading a customized keymapping remaps
the keys for *everyone* on the system, including root. If you make a
complete mess of your keyboard you can hose yourself immensely\!\!

There, I feel better now.

Seriously, though, be sure to READ the manual pages BEFORE you go
scrambling off to do this stuff. Make sure that you understand what
you're doing. :-) The manual pages that you probably ought to have a
look at are:

* keytables
* showkey
* loadkeys

The first one, keytables, will give you a succinct overview of the
keytables files used for keyboard mapping. This is considered **required
reading** before you do anything. The next two are programs that you'll
need to be familiar with -- **showkey** will let you find a key's
keycode, **loadkeys** is the program that will actually let you load up
a customized keytable mapping.

Once you've read these, we can continue...

Ok, now for the fun part :-)

What we'll need to do is edit the custom keymap file ("mykey.map") we
just created. Fire up your favorite editor and edit the entries for the
keypad keys. What you might first want to do is get a listing of the
keycodes for these keys. When I use **showkey**, this is the output that
I get:

```
FiskHaus [VT5] /usr/lib/kbd/keytables$ showkey
kb mode was XLATE

press any key (program terminates after 10s of last keypress)...
keycode  82 press
keycode  82 release
keycode  79 press
keycode  79 release
keycode  80 press
keycode  80 release
keycode  81 press
keycode  81 release
keycode  75 press
keycode  75 release
keycode  76 press
keycode  76 release
keycode  77 press
keycode  77 release
keycode  71 press
keycode  71 release
keycode  72 press
keycode  72 release
keycode  73 press
keycode  73 release
FiskHaus [VT5] /usr/lib/kbd/keytables$
```

This is a listing of the keycodes for the keypad keys 0, 1, 2, 3,..., 9.
I simply invoked the showkey program and pressed keys 0 through 9. So,
now I know that keypad key "0" represents keycode 82, key "1" represents
79, key "2" represent keycode 80, and so forth. Armed with this
knowledge (and the results scratched down in my ever-present notebook...
:-) we're ready to start tinkering.

The modifications you'll need to make might look something like this:

```
#
# NUMERIC KEYPAD MAPPING
#
#keycode  71 = KP_7
keycode  71 = Console_7
    shift keycode  71 = Console_17
    alt keycode  71 = KP_7
#keycode  72 = KP_8
keycode  72 = Console_8
    shift keycode  72 = Console_18
    alt keycode  72 = KP_8
#keycode  73 = KP_9
keycode  73 = Console_9
    shift keycode  73 = Console_19
    alt keycode  73 = KP_9
keycode  74 = KP_Subtract
#keycode  75 = KP_4
keycode  75 = Console_4
    shift keycode  75 = Console_14
    alt keycode  75 = KP_4
#keycode  76 = KP_5
keycode  76 = Console_5
    shift keycode  76 = Console_15
    alt keycode  76 = KP_5
#keycode  77 = KP_6
keycode  77 = Console_6
    shift keycode  77 = Console_16
    alt keycode  77 = KP_6
keycode  78 = KP_Add
#keycode  79 = KP_1
keycode  79 = Console_1
    shift keycode  79 = Console_11
    alt keycode  79 = KP_1
#keycode  80 = KP_2
keycode  80 = Console_2
    shift keycode  80 = Console_12
    alt keycode  80 = KP_2
#keycode  81 = KP_3
keycode  81 = Console_3
    shift keycode  81 = Console_13
    alt keycode  81 = KP_3
#keycode  82 = KP_0
keycode  82 = Last_Console
    shift keycode  82 = Console_10
    alt keycode  82 = KP_0
keycode  83 = KP_Period
    altgr   control keycode  83 = Boot
    control alt keycode  83 = Boot
```

Those of you who've skimmed over the manual page for **keytables** will
immediately recognize what's going on here :-) The manual page describes
the form each entry takes as:

```
Each complete key definition line is of the form:

      keycode keynumber = keysym keysym keysym...

keynumber  is  the  internal  identification number of the
key, roughly equivalent to the scan code of it.  keynumber
can  be  given  in decimal, octal or hexadecimal notation.
Octal is denoted by a leading zero and hexadecimal by  the
prefix 0x.

Each  of  the keysyms represent keyboard actions, of which
up to 256 can be bound to a single key. The actions avail-
able contain outputing Latin1 character codes or character
sequences, switching  consoles  or  keymaps,  booting  the
machine etc. (The complete list can be obtained from dump-
keys(1) by saying  dumpkeys -l .)
```

So, using this "keycode keynumber = keysym keysym keysym" notation we
can see that the first non-commented line:

```
keycode  71 = Console_7
```

maps the key with keycode "71" (which we discovered above is the keypad
key "7") to "Console\_7". So, what happens when we hit the keypad 7
key...?

You guessed it...!

*ShaZZzzammm\!\!* you're at VT number 7\!

Too way cool :-)

Now, if you're wondering whether you can still, after this bit of
remapping magic, use the 'ol ALT-F7 to get you to the same point, the
answer is a *qualified* yes. It's qualified only in that is assumes that
you haven't modified the function key entries. Let's look at this a
moment...

Have you ever wondered just exactly *where* that 'ol ALT-Fn key combo
came from and how it's able to transport you to another VT...? Just skim
over the keymap file. Remember, the file that you used initially was the
default keymap file and so it should indicate how the keys are mapped in
the default case. So, grepping through the file takes you to another
entry for the Console\_7 keysym:

```
keycode  65 = F7               F19              Console_19
        control keycode  65 = F31
        shift   control keycode  65 = F43
        alt     keycode  65 = Console_7
        control alt     keycode  65 = Console_7
```

And there it is... on both the fourth and fifth stanzas for the entry
for the F7 key. You can see here that if you hit either the ALT-F7 or
the CTRL-ALT-F7 key combinations, that you'll find yourself at VT number
7. Which brings up another important point in this key mapping stuff...

Another bit of magic that we can apply to the keymap entries has to do
with specifying what happens when you hit a key in combination with one
or more **modifiers**. These are:

```
Which of the actions bound to a given key is taken when it
is pressed depends on what modifiers are in effect at that
moment.  The keyboard driver supports 8  modifiers.  These
modifiers  are  labeled  (completely  arbitrarily)  Shift,
AltGr, Control, Alt,  ShiftL,  ShiftR,  CtrlL  and  CtrlR.
Each  of these modifiers has an associated weight of power
of two according to the following table:

      modifier                weight

      Shift                     1
      AltGr                     2
      Control                   4
      Alt                       8
      ShiftL                   16
      ShiftR                   32
      CtrlL                    64
      CtrlR                   128
```

(replicated from the keytables manual page)

So, now that "alt keycode 65 = Console\_7" entry makes a bit more
sense...

You see, if you add one or more modifiers to the "keycode keynumber =
keysym" entry, you can map a modifier(s)+key combination to a different
event. In the case mentioned above, this causes the ALT-F7 and the
CTRL-ALT-F7 key combinations to switch to VT number 7.

So, let's see what we did with the keypad...

Taking a sample entry from the customized "mykey.map" file for the
keypad "1" key (which is keycode 79) we find:

``` 
keycode  79 = Console_1
    shift keycode  79 = Console_11
    alt keycode  79 = KP_1         
```

This maps the keypad key 1 to Console\_1, which has the action of
switching to VT number 1. The next entry uses the **shift** key
modifier. This allows us to use the shift-keypad 1 combination to switch
to VT number 11, if this tty exists. The last entry is an important one
that you'll want to think about adding. What it does is simply allow you
to map the alt-keypad 1 combo to the default KP\_1 action, which let's
you use this as a numeric keypad when the NumLock is on.

So, if you've mapped your keys using the above, and you want to use the
**bc** calculator and the keypad, you'd fire up bc, hit the NumLock key,
and then use the ALT-Keypad combination to enter numbers. Careful,
though\! If you forget the ALT key, you might find yourself at another
VT\! :-)

Ok, now that you've gotten the basics, and you've modified your
mymap.key file, all that's left to do is load this thing up. You can do
this by invoking the **loadkeys** command, which you've alreay read
about since you've skimmed over the manual page.

And, of course, you *always* read the manual page... :-)

To use your new keytable mapping, just enter the command:

```sh
loadkeys /usr/lib/kbd/keytables/mykey.map
```

You should see a message indicating that the new keytable map is being
loaded and *voila\!* your new mapping is ready\! Now, when you hit the
keypad key 1 you should find yourself at VT 1, hit keypad 2 and you're
at VT 2, hit keypad 0 and...

Ooops... we're back at VT 1

Which leads me to make a couple more points... :-)

There are a couple other VERY useful mappings that you can try, one of
which is included with the above keypad remapping. These include:

* Last_Console
* Decr_Console
* Incr_Console

Their actions are probably pretty intuitive: "Last\_Console" teleports
you back to whatever VT you were previously on, "Decr\_Console" takes
you to the "VT - 1" console (that is, if you were on VT 3, it would take
you to VT 2), and "Incr\_Console" takes you to the "VT + 1" console
(that is, if you were on VT 3, it would take you to VT 4).

Pretty cool...eh?

You'll see that, from the example above, we've mapped the keypad "0" key
to Last\_Console. This lets you quickly switch back and forth between
two consoles simply by hitting the keypad "0" key.

The quick of wit will probably already have noticed that the keypad "0"
key is quite a ways away from your fingers if you happen to be a
touch-typer. The way to remedy this is actually suggested by Alessandro,
and so let me finish by adding a few tips that he offers:

First, unless you find that you use the Caps Lock key often, it might be
worthwhile to remap this to something more useful. Alessandro suggests
turning it into a *control* key using the following modification:

```
keycode 58 = Control
alt keycode 58 = Caps_Lock
```

this will remap the Caps Lock key (keycode 58 on my 101 keyboard) to
*control*. Hitting the alt-Caps Lock combination restores the Caps\_Lock
function. Now, in case you'd rather use this as a means of switching
quickly between two VT's using Last\_Console which we just tried, you
might use something like this:

```
keycode 58 = Last_Console
alt keycode 58 = Caps_Lock
```

Now, it's a bit more awkward to use the alt-Caps Lock combo for Caps
Locking, but it's a LOT faster to switch between VT's if you happen to
be using a couple for editing/compiling or editing/reading-docs type of
tasks. BTW, for all you VIM users out there... :-) In case you're
willing to give up easy access to Caps Lock and want an ESC key that's a
LOT handier...

```
keycode 58 = Escape
alt keycode 58 = Caps_Lock
```

This converts your Caps Lock key to an extra ESC key. Now, when you're
editing and need to alternate between edit and command modes, instead of
fishing around for the Escape key, or trying to hit the control-\[
combination, you have a handy ESC key twin right there next to the "a"
key. Very cool :-)

Here are a couple more ideas...

We said above that the Decr\_Console and Incr\_Console functions will
let us move either forward or backward though our VT's. Guess where
they've been mapped...? You got it...\! to the control-left\_arrow and
control-right\_arrow key combinations:

```
keycode 105 = Left
        control keycode 105 = Decr_Console
keycode 106 = Right
        control keycode 106 = Incr_Console
```

Now, when you hit the control-L or control-R arrow keys, you can cycle
through your VT's. Those of you who happen to be using the default FVWM
1.2 configuration file may have discovered this little trick for desktop
mobility.

Lastly, you can configure the **Home** and **End** keys to function as
control-a and control-e keys, which is a standard Emacs keybinding that
positions the cursor at the beginning or end of a line respectively.
Again, you'll want to check the keycode numbers for the Home and End
keys using showkey. Mine are 102 and 107 respectively. Now, to remap,
you'd enter something like:

```
keycode 102 = Control_a
keycode 107 = Control_e
```

and you'd be all set\! Home and End now take you to the beginning and
end of a line. I can tell you that this works under the BASH shell.
According to Alessandro, this should also work under both tsch and
emacs.

Well, that should get you started\! One last point to make: once you
have a keymapping that you like (you can use **loadkeys** repeatedly to
test new mappings) you should probably put an entry in the **rc.local**
file that will load up the customized mapping. Something like:

```
if [ -r /usr/lib/kbd/keytables/mykey.map ]; then
    /usr/bin/loadkeys /usr/lib/kbd/keytables/mykey.map
fi
```

this will ensure that your mykey.map file exists and is readable before
invoking the loadkeys program.

Have fun and let me know what sort of mischief you've gotten yourself
into!

