# Using rdist for Backups

Autor: John Pate

---

In **Linux Gazette** Issue number 24 **Gerd Bavendiek** described his
script which he calls "mirror." In this script he makes use of the
utility 'rdist' which is widely available on Unix systems. In my article
I shall explain how I use 'rdist' which I hope will give the beginning
Linux user a background to Gerd's article and show how useful 'rdist' is
to those with multiple computers (which should be every Linux user
nowadays\!)

In the UK today you can get a complete 486DX66 system including monitor
for a hundred pounds, which has got to be a bargain and ideal as a
backup system for your "main" computer. Even if you feel its hard disk
is too small (typically they have them in the 200-300meg range) today
you can get a 2gig hard disk for under one hundred pounds or an 850meg
one for sixty pounds. So for no more than a couple of a hundred pounds
you not only have a backup device but a working computer you can use in
emergency in the event of the failure of your "main" machine. I have two
486DX66s, both other peoples' discards which cost me nothing apart from
extra RAM and a bigger hard disk for the "main" machine (and a modem of
course - though now I have a couple of spare V32bis other people have
junked, isn't progress wonderful). One has 40meg of RAM and is my "main"
computer running Slackware 3.3. The other has 8meg of RAM and Slackware
3.3 again. The "backup" has a half gig hard disk given to me because it
makes an unpleasant whine during operation. In spite of the noise, the
disk works fine so is good for backup space. A couple of discarded
network cards from a "parts bin" and a look at the **NET-3-HOWTO** (you
can find this in the directory '/usr/doc/faq/howto' in Slackware 3.3)
gets the two computers talking to each other. Other alternatives are
PLIP or a null modem cable, but network cards are easy to come by
nowadays. (Note: network cards with RJ45 interfaces can connect two
computers together if you make a crossover cable, it works for me.)

Once the computers are talking you can use ftp or even Samba to transfer
files, as well as mounting various drives and partitions by NFS. All
good clean fun. But the ideal way to maintain a "mirror" of your home
directories so you don't lose any precious datafiles is to use 'rdist'.
(Note: the following describes a Slackware 3.3 setup but the procedure
should give the rest of you a start.) Firstly you must amend the file
'hosts.equiv' on the "backup" machine (i.e. the one you are going to
back-up the files to) to allow users to 'rlogin' (see the man page for
'rlogin') to the "backup" computer from the "main" computer without
having to supply a password. 'hosts.equiv' is found in '/etc'
(Slackware), the stock 'hosts.equiv' has one entry, 'localhost'. Simply
add the relevant name or IP address for your "main" computer (go back
and read the **NET-3-HOWTO,** you know you have to) on a new line. If
you have accounts (same user name) on both machines, you are now able to
'rlogin' to the "backup" from the "main" with the command
'rlogin \<other\_computer\_name\>' and it should not demand a password
(unless you are 'root', but you don't want to mess around letting 'root'
rlogin without a password, do you?).

Now you're cooking with gas. Make a file called 'distfile' in your home
directory on your "main" computer. Here's one I made earlier--

``` 
HOSTS = ( johnny@pod )
FILES = ( ~/ )
EXCEPT = ( ~/.netscape ~/\(PROG\) ~/StarOffice-3.1 ~/.trash ~/.bash_history )

${FILES} -> ${HOSTS}
install -oremove,chknfs,younger ;
except ${EXCEPT} ;
```

Now's the time for you to consult 'man rdist' to see what all this
means. The first three lines set variables - it is possible to give
'rdist' command line switches to modify these, so it could be helpful
when you get more adventurous. The last three lines are commands for
'rdist'. The 'install' command copies/deletes files/directories as
specified. The 'except' command omits the specified files/directories
from processing. (If you specify a directory it means that directory's
files and subdirectories and files.) The '-\>' line tells it what to
where. Note the 'host' is the "backup" computer and given of the form
'user@domain'. This should be an account you can 'rlogin' to without
needing a password (modify 'hosts.equiv' see above). If you look at the
above example, it says,

> "Copy the entire home directory to "pod," excepting the Netscape cache
> directory, the trash directory, the StarOffice stuff and the
> .bash\_history file."

The switches to 'install' say,

> "Remove any files on the backup that aren't on the main machine, don't
> follow through NFS links, and don't update files on the backup machine
> that are younger than the equivalent on the main machine."

**Note** how 'remove'and 'younger' interact: 'remove' will remove any
orphan files, so 'younger' won't keep brand new files on the backup that
haven't been made on the main machine - decide what you want\! In the
case of a backup to an account you don't use then 'younger' isn't really
necessary. If it's an account you do use, then don't use 'remove' but do
use 'younger'.

A perusal of the 'rdist' man page should make this all clear. Having
made the 'distfile,' all you have to do to synchronize the files on your
two machines is type 'rdist' at the prompt on the "main" machine and it
will go ahead and do it all for you and tell you about it. 'rdist' will
create directories as needed, preserve permissions, ownerships etc.,
will only change files that don't match (you can alter the criteria it
uses to decide this, see the 'man' page). It is excellent\! Run 'rdist'
regularly and you need never "lose" your data-files. Even better if you
back-up over the Internet to a remote location.

Now you can go back to [Gerd's article](../issue24/bavendiek.html) and
see how he uses his script to keep his laptop and desktop in synch. A
study of the 'rdist' man page will make evident that 'rdist' has great
power and flexibility for the adventurous. There really is no excuse for
not backing up your work\!

