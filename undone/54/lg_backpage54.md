# The Back Page

## About This Month's Authors

### ![](../gx/note.gif)Carlos Betancourt

Carlos is a young Venezuelan Linux and GNU philosophy advocate since
1996. Working as a research student for the University of Tachira's
(UNET) Network development laboratory (CETI) since 1995, he has been
involved in the development of the University's network infrastructure
and services. In a University with no Unix roots, Carlos was part of the
research team of students in charge of learning, testing, teaching,
supporting and implementing Unix (by means of Linux, Solaris and HP-UX)
technologies to the UNET's new computer infrastructure, as well as
assistant administrator of internet services. His hobbies involve
Astronomy, Electronics, Reading Science Fiction, Classical and Political
literature, \[Astro\]Photography and Poetry. He currently live in
Brussels, Belgium, where he has recently married.

### ![](../gx/note.gif)Shane Collinge

Part computer programmer, part cartoonist, part Mars Bar. At night, he
runs around in a pair of colorful tights fighting criminals. During the
day... well, he just runs around. He eats when he's hungry and sleeps
when he's sleepy.

### ![](../gx/note.gif)Fernando Correa

Fernando is a computer analyst just about to finish his graduation at
Federal University of Rio de Janeiro. Now, he has built with his staff
the best [Linux portal](http://www.olinux.com.br) in Brazil and have
further plans to improve services and content for their Internet users.

### ![](../gx/note.gif)Ray Ferrari

I am a new linux enthusiast who has been following the trend for over a
year now. I have successfully installed Debian and participate in
helping bring Linux to more people. I have been working with computers
for seven years on my own, learning as much as possible. I currently am
looking for a sales position within the Linux community. Talks are under
way with VALinux; my dream company. I have been a volunteer for both
Debian and LPI.

### ![](../gx/note.gif)Mark Nielsen

Mark founded The Computer Underground, Inc. in June of 1998. Since then,
he has been working on Linux solutions for his customers ranging from
custom computer hardware sales to programming and networking. Mark
specializes in Perl, SQL, and HTML programming along with Beowulf
clusters. Mark believes in the concept of contributing back to the Linux
community which helped to start his company. Mark and his employees are
always looking for exciting projects to do.

### ![](../gx/note.gif)Krassimir Petrov

Krassimir has a PhD in Agricultural Economics from Ohio State
University. He also has an MA in Economics and a BA in Business
(Finance, Accounting, Management).

### ![](../gx/note.gif)Ben Okopnik

A cyberjack-of-all-trades, Ben wanders the world in his 38' sailboat,
building networks and hacking on hardware and software whenever he runs
out of cruising money. He's been playing and working with computers
since the Elder Days (anybody remember the Elf II?), and isn't about to
stop any time soon.

### ![](../gx/note.gif)Pramode C.E and Gopakumar C.E

Pramode works as a teacher and programmer while Gopakumar is an
engineering student who likes to play with Linux and electronic
circuits.

### ![](../gx/note.gif)Richard Sevenich

Richard is a Professor of Computer Science at Eastern Washington
University in Cheney, WA and also teaches occasional introductory Linux
device driver courses in the commercial sector for UniForum. His
computer science interests include device drivers, Fuzzy Logic,
Application-Specific Languages, and State Languages for Industrial
Control. He has been an enthusiastic user of Linux since 1993.

### ![](../gx/note.gif)Chris Stoddard

I work for Dell Computer Corporation doing "not Linux" stuff. I have
been using computers since 1979 and I started using Linux sometime in
1994, exclusivly since 1997. My main interest is in networking
implementations, servers, security, Beowulf clusters etc. I hope someday
to quit my day job and become the Shepard of a Linux Farm.
<span id="notlinux"></span>

### Not Linux

I really should change the title of this column, because most of the
material *is* about Linux....

I'm really happy with how the Answer Gang is turning out. Thanks also to
Michael "Alex" Williams for helping to format the Mailbag and 2-Cent
Tips starting this month.

*Linux Gazette* is now available via anonymous rsync. This is good news
for our mirrors, because it decreases their update bandwidth. See [the
FAQ, question 14](../faq/index.html#rsync) for instructions.

I've got a new title now, **Chief Gazetteer**. This was invented by our
sysadmin assistant Rory Krause.

Michael Orr  
Editor, [*Linux Gazette*](http://www.linuxgazette.net/),
<gazette@linuxgazette.net>  

