# Creating A Linux Certification Program, Part 9

Autor: Ray Ferrari

---

## A Call to Action

The Linux Professional Institute(LPI) invites all Linux enthusiasts and
professionals to consider certification as a means of furthering their
career or increasing their earnings potential. Anyone interested in
Linux certification should visit LPI's web site at
[www.lpi.org](http://www.lpi.org/). The volunteers and staff of LPI
along with their long list of sponsors, have spent thousands of man
hours; as well as dollars; to bring a non-biased, vendor neutral testing
procedure to the world.

LPI exams are now available globally in English through Virtual University
Enterprises(VUE) which has 2,200 test centers; these centers are for
testing only. Interested individuals should visit the LPI web site at
[www.lpi.org/c-index.html](http://www.lpi.org/c-index.html) for more
information. Persons interested in Linux related training information
should visit </span>[www.lintraining.com](http://www.lintraining.com/).
The format for testing has been changed to simplify the process.
Numerous questions arise, so we have posted these frequently asked
questions at
[www.lpi.org/faq.html](http://www.lpi.org/faq.html).

LPI is currently working on development of their Level 2 certification. A
survey has been organized by professional Linux system administrators
from the U.S., Canada, and Germany for the purpose of allowing as many
volunteers as possible to participate in the structuring of the Level 2
tests. They need the responses of Level 2 system administrators to help
them formulate the writing of these exams. Any individual who would like
to participate in this phase should contact Kara Pritchard by e-mailing
her at <kara@lpi.org> or <scott@lpi.org> .

Some exciting things have been happening all around the globe. This
year, LPI has participated in events in France, Germany, Australia,
Canada and the U.S. In April, LPI was in Chicago for the Comdex and
Linux Business Expo; see [www.comdex.com](http://www.comdex.com). At
this show, almost 200 people took the Level One tests. These tests were
made possible by the cooperation of Linux International, VUE, Linux
Business Expo, and LPI. It was a great success, and the first time
testing was performed at an exhibition. LPI is looking to further this
success by its presence at future events. Other shows included Advanstar
in Toronto,May 16-18(see [www.newmedia.ca](http://www.newmedia.ca/));
and Montreal Skyevents in April,
([www.skyevents.com](http://www.skyevents.com/)).

Future events currently scheduled include a booth in the Olympia
Convention Center, London, England on June
1-2([www.itevents.co.uk](http://www.itevents.co.uk/)), the Linux
Business Expo. in Toronto, July
12-14([www.zdevents.com](http://www.zdevents.com/)), and Linux World in
San Jose, California, August15-17
([www.idg.com/events](http://www.idg.com/events)). Anyone interested in
volunteering to help staff these booths should contact <wilma@lpi.org>.

The Linux Professional Institute(LPI) continues to attract the attention
and sponsorship from some of the most influential companies and
individuals in the world. We appreciate their guidance and support and
are pleased to welcome Hewlett-Packard, Mission Critical Linux, Psycmet
Corporation, SmartForce Corporation, TeamLinux, and VA Linux Systems
among our ever-growing list of contributors and sponsors. For a complete
list of corporate and individual sponsors, please visit
[www.lpi.org/a-sponsors.html](http://www.lpi.org/a-sponsors.html).

For anyone interested in viewing any presentations put on by LPI, you
can visit [www.lpi.org/library/](http://www.lpi.org/library/).Also, for
user group information or to find a linux group in your area, visit
[www.linux.org/users](http://www.linux.org/users) or
[http://lugs.linux.com](http://lugs.linux.com/).

At LPI, we continue to bring you the latest on testing fundamentals and
certification. We invite you to stand up and be noticed. Put some
credentials beside your knowledge, and create the future in Linux. This
is "a call to action." We'll see you around the next bend.

Linux is a trademark of Linux Torvalds; Linux Professional Institute
is a trademark of the Linux Professional Institute Inc.

