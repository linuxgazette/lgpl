# Pick an Editor, Any Editor

Autor: Jens Wessling

---

## im (VI iMproved)

One day I realized that I seem to spend an inordinate amount of time in
front of a computer screen and more precisely with a text editor in
front of me. This should hardly have been a surprise to me considering I
work with computers for eight to ten hours a day and then I go home and
spend several more. I guess that this obvious fact finally struck home
with me. I would guess I spend about 20-30 hours a week in a text editor
alone. With some quick calculations I realized that this adds up to over
1000 hours a year. That is over 40 solid days of my life every year in
an editor. (That is a conservative estimate.) This realization spurred
me to try to optimize the time I spend in my editor.

The first step I took was to try and find the *best* editor around. I
started asking around to see who used what and to try to find out what
the important qualities of an editor were. **Don't** make this mistake.
Editors are one of the most religious beliefs a programmer holds. Every
programmer is convinced that there's is the best. My office-mate uses
PICO, some of my co-workers use EMACS, VI, SlickEdit, or any one of an
unending list. Every person I talked to insured me that their selection
was by far the best. When I inquired about the differences, they were
primarily insignificant. That was when I learned the horrible truth.
Most editors are essentially equivalent. No matter how hard people
insist, most editors have more features than any user will ever use.
(Except PICO). In the Linux community, these selections basically fall
in to one of two categories. VI clones, or Emacs. My recommendation is
that everyone learn one of these well. It doesn't really matter which
one, just pick one, stick with it and use it. (Religiously if you must.)

I have gone to great lengths to learn VIM, a VI clone. And certainly if
not THE best, one of the top contenders. Many features are shared among
VI clones, basically the VI subset. The additional features are
basically individual to each clone. VIM comes with most, if not all,
Linux distributions. The home page for VIM is
http://www.math.fu-berlin.de/~guckes/vim/. VIM is in active
development and is getting better by the day. Syntax highlighting should
be out, if not by the time you read this, then soon thereafter.

I will assume that most people know the basics of VI and want to change
it from a simple tool to a powerful one. I will share some of the handy
tips and tricks I use.

## Programming, Tabs, and Tags

ctags is a marvelous utility for C and C++ Programmers. ctags comes with
the VIM distribution. What this utility does is create a list of all the
subroutine names in the files you specify and allow you to jump to the
given subroutine while in you editor with just one keypress. The way you
run ctags is simple.

```sh
ctags *.c
    or
ctags *.cpp
```

Then, crank up your editor and move to wherever it is you call any
subroutine from and press \[CTRL\]-\]. This will take you immediately to
wherever the routine is, even if it is in a different file.

## File Switching

I frequently work with several files concurrently and I need to switch
between these files continually. The command to switch to another file
in VIM is ":e *fn*". The shortcut to switch to the last file edited is
":e \#". This is fine for normal use, but I switch files often, and 4
keystrokes seems like a bit much. This is where VIM's key mapping comes
in. VIM like most editors has an *rc* file. It is called .vimrc, what a
shock. 8) In this file I have the following command.

```
" Save and switch to other buffer.
map N :w [CTRL]-M 
```

This command lets me switch buffers with a single keypress. The other
nice feature in VIM for switching between files is tab completion for
file names. The way tab completion works is to take whatever letters you
have typed in so far on for the file name and find all of the files that
could possibly match. Hitting tab will scroll through the list of files
until you find the one you want. If no beginning letters are specified
for the file name, it will scroll through them all.

## Mapping

I do a LOT of coding and I find that I often need to comment out blocks
of lines. I have developed 2 macros for handling this with a minimum of
effort.

```sh
map C 0i/*[CTRL-ESC]A*/[CTRL-ESC]j
map T 0xx$xxj
```

If you examine the first line, you will see that it does the
following.

* Moves to the beginning of the line
* Enters Insert Mode
* Places "\\\*" there
* Escapes to command mode
* Adds "\*/" to the end of the line
* Escapes to command mode
* Moves down One line

The second line does the following.

* Moves to the beginning of the line
* Removes 2 characters
* Moves to the end of the line
* Removes 2 characters
* Moves down One line

I can type "12C" in command mode and it will comment out the next dozen
lines. and "12T" will uncomment a dozen lines that were commented by "C".

Keep in mind that when you remap keys, they lose there original values.
In this case, "C" was an odd Delete until end of line and next several
lines into a given buffer, and "T" was a command I can't really figure
out from the documentation. I don't really miss these two but be careful
that you don't map "i" or "x" or anything else you might need later.

Have fun with this. I hope to have more later.

