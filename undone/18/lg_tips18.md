
# <span id="tips">![](../gx/twocent.gif) More 2¢ Tips\!</span>

  
Send Linux Tips and Tricks to <gazette@linuxgazette.net>

-----


### ![](../gx/lil2cent.gif) Monitoring a ftp Download.

Date: Tue, 27 May 1997 09:57:20 -0400  
From: Bob Grabau [bob\_grabau@fmso.navy.
mil](mailto:bob_grabau@fmso.navy.mil)

Here is a tip for monitoring a ftp download. in another virtual console
enter the following script:

    while :
    do
    clear
    ls -l <filename that you are downloading>
    sleep 1
    done

This virtual console can be behind (if you are using X) any other window
and just showing a line of text. This will let you know if your download
is done or stalled. This will let you do other things, like reading the
Linux Gazette.

When you type this in, you wll get a \> prompt after the first line and
continue until you enter the last line.

\-- Bob Grabau

-----


### ![](../gx/lil2cent.gif) Logging In To X Tip

Date: Mon, 26 May 1997 10:17:12 -0500 (CDT)  
From: Tom Barron <barron@usit.net>  
**Xlogin.mini-howto**

Several people regularly use my Linux system at home (an assembled-from-
components box containing a 133 Mhz Pentium, 2Gb of disk, 32Mb of
memory, running the Slackware distribution) -- my step-son Stephen,
who's learning to program and likes using X, my younger step-son
Michael, who likes the X screen-savers and games like Doom, my wife
Karen, who prefers the generic terminalness of the un-X'd console, and
myself -- I like to use X for doing software development work since it
lets me see several processes on the screen at once. I also like to keep
an X screen saver running when no-one is using the machine.

I didn't want to run [xdm](http://squirl.nightmare.com:5150/xdm\(1\))
(an X-based login manager), since Karen doesn't want to have to deal
with X. She wants to be at the console when she logins in and not have
to worry about where to click the mouse and such. But I wanted to have a
simple way of getting into X when I login without having to start it up
manually.

Here's what I came up with:

  - In my .profile (my shell is bash), I put:
    
        if [ "$DISPLAY" = "" ]; then
        
           cal > ~/.month
           xinit .Xsession > /dev/null 2>&1
           clear
           if [ ! -f .noexit ]; then
              exit
           fi
        
        else
        
           export TTY=`tty`
           export TTY=`expr "$TTY" : "/dev/tty\(.*\)"`
           export PS1="<$LOGNAME @ \h[$TTY]:\w> \n$ "
           export PATH=${PATH}:~/bin:.
           export EDITOR=emacs
           export WWW_HOME=file://localhost/home/tb/Lynx/lynx_bookmarks.html
           export DISPLAY
        
           alias cls="clear"
           alias dodo="$EDITOR ~/prj/dodo"
           alias e="$EDITOR"
           alias exit=". ~/bin/off"
           alias l="ls -l"
           alias lx="ls -x"
           alias minicom="minicom -m"
           alias pg=less
           alias pine="export DISPLAY=;'pine'"
           alias prj=". ~/bin/prj"
           alias profile="$EDITOR ~/.profile; . ~/.profile"
        
        fi
    
    When I first login, on the console, $DISPLAY is not yet set, so the
    first branch of the if statement takes effect and we start up X.
    When X terminates, we'll clear the screen and, unless the file
    .noexit exists, logout. Running cal and storing the output in .month
    is in preparation for displaying a calender in a window under X.

  - Once X comes up, $DISPLAY **is** set. My .Xsession file contains:
    
        :
        xsetroot -solid black
        fvwm &
        oclock -geometry 75x75-0+0 &
        xload -geometry 100x75+580+0 &
        emacs -geometry -0-0 &
        xterm -geometry 22x8+790+0 -e less ~/.month &
        color_xterm -font 7x14 -ls -geometry +5-0 &
        exec color_xterm -font 7x14 -ls -geometry +5+30 \
           -T "Type 'exit' in this window to leave X"
    
    So when my color\_xterms run, with -ls as an argument (which says to
    run a login shell), they run .profile again. Only this time $DISPLAY
    is set, so they process the else half of the if, getting the
    environment variables and aliases I normally expect.

-----


### ![](../gx/lil2cent.gif) xlock Tip

Date: Mon, 26 May 1997 10:14:12 -0500 (CDT)  
From: Tom Barron <barron@usit.net> **Xscreensaver.mini-howto**

Several people regularly use my Linux system at home (an assembled-from-
components box containing a 133 Mhz Pentium, 2Gb of disk, 32Mb of
memory, running the Slackware distribution) -- my step-son Stephen,
who's learning to program and likes using X, my younger step-son
Michael, who likes the X screen-savers and games like Doom, my wife
Karen, who prefers the generic terminalness of the un-X'd console, and
myself -- I like to use X for doing software development work since it
lets me see several processes on the screen at once. I also like to keep
an X screen saver running when no-one is using the machine.

I didn't want to run [xdm](http://squirl.nightmare.com:5150/xdm\(1\))
(an X-based login manager), since Karen doesn't want to have to deal
with X. She wants to be at the console when she logins in and not have
to worry about where to click the mouse and such. But I wanted to have a
simple way of starting up the X-based screensaver xlock when I (or
anyone) logged out to the console login.

Here's what I did (as root):

  - I created a user called xlock. It has no password and its home
    directory is /usr/local/xlock. Its shell is bash.

  - In xlock's .profile, I put
    
        if [ "$DISPLAY" = "" ]; then
        
           xinit .Xsession > /dev/null 2>&1
           clear
           exit
        
        fi

  - In xlock's .Xsession, I put
    
        :
        exec xlock -nolock -mode random

Now, anybody can login xlock and instantly bring up the X screen-saver.
The "random" keyword tells it to select a pattern to display at random,
changing it every so often. When a key is pressed or a mouse button
clicked, the screensaver process exits, the X session is ended, and
control returns to the console login prompt.

In my next article, I show how I arranged to jump into X from the
console login prompt just by logging in (i.e., without having to start X
manually).

-----


### ![](../gx/lil2cent.gif) Hex Dump

Date: Sat, 24 May 1997 00:29:20 -0400  
From: Joseph Hartmann <joeh@arakis.sugar-river.net>

Hex Dump by Joseph L. Hartmann, Jr.

This code is copyright under the GNU GPL by Joseph L. Hartmann, Jr.

I have not been happy with Hex Dump. I am an old ex-DOS user, and am
familiar with the HEX ... ASCII side-by-side presentation.

Since I am studying awk and sed, I thought it would be an interesting
excercise to write this type of dump.

Here is a sample of what you may expect when you type the (script)
command "jhex " to the shell:

    0000000  46 69 6c 65 6e 61 6d 65  0000000 F i l e n a m e
    0000008  3a 20 2f 6a 6f 65 2f 62  0000008 :   / j o e / b
    0000010  6f 6f 6b 73 2f 52 45 41  0000010 o o k s / R E A
    0000018  44 4d 45 0a 0a 62 6f 6f  0000018 D M E . . b o o
    0000020  6b 2e 74 6f 2e 62 69 62  0000020 k . t o . b i b
    0000028  6c 69 6f 66 69 6e 64 2e  0000028 l i o f i n d .
    0000030  70 65 72 6c 20 69 73 20  0000030 p e r l   i s

If you like it, read on....

    The 0000000 is the hexadecimal address of the dump
    46 is the hexadecimal value at 0000000
    69 is the hexadecimal value at 0000001
    6c is the hexadecimal value at 0000002
    ...and so on.

To the right of the repeated address, "F i l e n a m e" is the 8 ascii
equivalents to the hex codes you see on the left.

I elected to dump 8 bytes in one row of screen output. The following
software is required: hexdump, bash, less and gawk.

gawk is the GNU/Linux version of awk.

There are four files that I have installed in my /joe/scripts directory,
a directory that is in my PATH environment.

The four files are: combine -- an executable script: you must "chmod +x
combine" jhex -- an executable script: you must "chmod +x jhex"
hexdump.dashx.format -- a data file holding the formatting information
for the hex bytes. hexdump.perusal.format -- a data file holding the
formatting information for the ascii bytes.

Here is the file jhex:

    hexdump -f /joe/scripts/hexdump.dashx.format $1 > /tmp1.tmp
    hexdump -f /joe/scripts/hexdump.perusal.format $1 > /tmp2.tmp
    gawk -f /joe/scripts/combine /tmp1.tmp > /tmp3.tmp
    less /tmp3.tmp
    rm /tmp1.tmp
    rm /tmp2.tmp
    rm /tmp3.tmp

Here is the file combine:

    # this is /joe/scripts/combine -- it is invoked by /joe/scripts/jhex
    {  getline < "/tmp1.tmp"
       printf("%s  ",$0)
       getline < "/tmp2.tmp"
       print 
    }

Here is the file hexdump.dashx.format:

``` 
           "%07.7_ax  " 8/1 "%02x "  "\n"
```

Here is the file hexdump.perusal.format:

``` 
           "%07.7_ax "  8/1  "%_p " "\n"
```

I found the "sed & awk" book by Dale Dougherty helpful.

I hope you find jhex useful. To make it useful for yourself, you will
have to replace the "/joe/scripts" with the path of your choice. It must
be a path that is in your PATH, so that the scripts can be executed from
anyplace in the directory tree.

A trivial note: do not remove the blank line from the
hexdump.dasx.format and hexdump.perusal.format files: it will not work
if you do\!

A second trivial note: when a file contains many characters all of same
kind, the line-by-line display will be aborted and the display will look
similar to the example below:

    0000820  75 65 6e 63 65 20 61 66  0000820 u e n c e   a f
    0000828  74 65 72 20 74 68 65 20  0000828 t e r   t h e
    0000830  0a 20 20 20 20 20 20 20  0000830 .
    0000838  20 20 20 20 20 20 20 20  0000838
    *  *
    0000868  20 20 20 20 20 6c 61 73  0000868           l a s
    0000870  74 20 72 65 63 6f 72 64  0000870 t   r e c o r d

Instead of displaying \*all\* the 20's, you just get the

    *  *  .

I don't like this myself, but I have reached the end of my competence
(and/or patience), and therefore, that's the way it is\!

-----


### ![](../gx/lil2cent.gif) A Fast and Simple Printing Tip

Date: Fri, 23 May 1997 07:30:38 -0400  
From: Tim Bessell <tbessell@buffnet.net>

I have been using Linux for about a year, as each day passes and my
knowledge increases, my Win95 patitions decrease. This prompted me to by
a notebook, which of course is loaded with Windows. Currently these two
machines are NOT networked :-( But that doesn't mean I can't print a
document created in Word for Windows, Internet Explorer, etc., without
plugging my printer cable into the other machine.

My solution is rather simple. If you haven't already, add a new printer
in the Windows control panel, using the driver for the printer that is
connected to your Linux box. Select "FILE" as the port you wish to print
to and give it a name, eg: Print File (HP Destjet 540). Now print your
document to a floppy disk file, take it to the Linux machine, and issue
a command simular to: cat filename \> /dev/lp1. Your document will be
printed with all the formatting that was done in Windows.

Enjoy,  
Tim Bessell

-----


### ![](../gx/lil2cent.gif) Grepping Files in a Directory Tree

Date: Wed, 21 May 1997 21:42:34  
From: Earl Mitchell <earlm@Terayon.COM>

Ever wonder how you can grep certain files in a directory tree for a
particular string. Here's example how

    grep foo `find . -name \*.c -print`

This command will generate a list of all the .c files in the current
working directory or any of its subdirectories then use this list of
files for the grep command. The grep will then search those files for
the string "foo" and output the filename and the line containing "foo".

The only caveat here is that UNIX is configured to limit max chars in a
command line and the "find" command may generate a list of files to huge
for shell to digest when it tries to run the grep portion as a command
line. Typically this limit is 1024 chars per command line.

\-earl

-----


### ![](../gx/lil2cent.gif) ViRGE Chipset

Date: Wed, 30 Apr 1997 22:41:28  
From: Peter Amstutz <amstpi@freenet.tlh.fl.us>

A couple suggestions to people with video cards based on the ViRGE
Chipset...

1.  XFree 3.2 has a ViRGE server\! I have heard a number of people
    complain about XFree's lack of ViRGE support. Yo GUYZ\! That's
    because your wonderful Linux CD has XFree86 3.1.2 WHICH IS NOT THE
    MOST RECENT VERSION\!
2.  There is a minor hack you can make to svgalib 1.12.10 to get it to
    reconignize your nice S3 based card as actually being such. The
    s3/ViRGE chip is, in the words of some guy at C|Net, "basically a S3
    Trio 64 with a 3d engine bolted on top." Unfortunately, it returns a
    card code totally different to the Trio64. With just a minor little
    bit of hacking, you too can do 1024x768x16bpp through svgalib. Get
    the source, untar it & everything. Go into the main source
    directory, and with your favorite editor, open up s3.c (or it maybe
    vga.c it has been sometime since I did this and I do not have the
    source now in front of me) Now, search for the nice little error
    message it gives you when it says something like "S3 chip 0x(some
    hex number) not reconignized." Above it there should be a
    switch()/case statement that figures out which card it is. Find the
    case statement that matches a Trio64. Insert a fall-through case
    statement that matches the code your card returns, so svgalib treats
    it as a Trio64\! You're home free\! Recompile, re-install libraries,
    and now, what we've all been waiting for, test 640x480x256\!
    640x480x16bpp\! 800x600x24bpp\! YES\!\!\!

Note: this trick has not been authorized, reconignized, or in any way
endorsed, recommended, or even considered by the guy(s) who wrote
svgalib in the first place. (that last version of svgalib is over a year
old, so I don't expect there to be any new versions real soon) It works
for me, so I just wanted to share it with the Linux community that just
might find it useful. Peter Amstutz

-----


### ![](../gx/lil2cent.gif) Maintaining Multiple X Sessions

Date: Sun, 04 May 1997 21:02:10 +0200  
From: David Kastrup <dak@neuroinformatik.ruhr-uni-bochum.de>

Suppose you have an X running, and want to start another one (perhaps
for a different user).

startx alone will complain.

Writing

    startx -- :1

  
will work, however (if screen 0 is already taken). Start another one
with

    startx -- :2

  
if you want. You want that to have hicolor, and your Xserver would
support it?

Then start it rather with

    startx -- -bpp 16 :2

Of course, if no Xserver is running yet, you can get a non-default depth
by just starting with

    startx -- -bpp 16

or

    startx -- -bpp 8

  
or whatever happens to be non-standard with you. -- David Kastrup

-----


### ![](../gx/lil2cent.gif) Automatic File Transfer

Date: Sat, 3 May 1997 12:58:11 +0200 (MDT)  
From: Gregor Gerstmann <gerstman@tfh-berlin.de>  

Hi there, Here is a small tip concerning the 'automatic' file transfer;
Linux Gazette Issue 17, May 1997. Everything is known stuff in Unix and
Linux. To 'automate' file transfer for me means to minimize the load on
the remote server as well as my own telephone costs - you have to pay
for the time you think if or not to get a special file, for changing the
directories and for the time to put the names into the PC. The procedure
is called with the address as parameter and generates a protocol.

    #!/bin/bash
    #
    date > prot
    #
    ftp -v $1 >> prot
    #
    #
    date >> prot
    #

Ftp now looks if a .netrc file exists; in this file I use macros written
in advance and numbered consecutively:

    ...
    machine ftp.ssc.com login anonymous password -gerstman@tfh-berlin.de
    macdef T131
    binary
    prompt
    cd ./pub/lg
    pwd
    dir . C131.2
    get lg_issue17.tar.gz SSC17
    
    macdef init
    $T131
    bye
    ...

Now I first get the contents of several directories via dir . C131...
and, to have some book-keeping, logically use the same numbers for the
macros and the directories. The protocol shows, if I am really in the
directory I wished to. Until the next session begins, the file C131...
is used to edit the last .netrc file, therefore the names will always be
typed correctly. If you are downloading under DOS from your account the
shorter names are defined in the .netrc file. Everything is done
beforehand with vi under Linux.

Dr.Werner Gerstmann

-----


### ![](../gx/lil2cent.gif) Setting Up Newsgroups

Date: Mon, 05 May 1997 16:19:05 -0600  
From: "Michael J. Hammel" <mjhammel@emass.com>  

*But I just can't seem to find any documentation explaining how to set
up local newsgroups. smtpd and nntpd are running, but the manpages won't
tell anything about how to set up ng's*

smtpd and nntpd are just transport agents. They could just as easily
transport any sort of message files as they do mail or NetNews files.
What you're looking for is the software which manages these files on
your local system (if you want newsgroups available only locally then
you need to have this software on your system). I used to use CNEWS for
this. I believe there are some other packages, much newer than CNEWS,
that might make it easier. Since I haven't used CNEWS in awhile I'm
afraid I can't offer any more info than this.

Michael J. Hammel

-----


### ![](../gx/lil2cent.gif) Color Applications in X

Date: Tue, 06 May 1997 09:25:01 -0400 (EDT)  
From: Oliver Oberdorf <oly@borg.harvard.edu>  

Saw some X Window tips, so I thought I'd send this one along..

I tend to use lots of color rich applications in X. After cranking up
XEmacs, Gimp, etc., I find that I quickly run out of palette on my 8-bit
display. Most programs don't behave sensibly when I run out of colors -
for example, CGoban comes up black and white and realaudio refuses to
run at all (not enough colors to play sound, I suppose.

I've found I can solve these problems by passing a "-cc 4" option to the
X server. This tells it to pretend I have a bigger pallete and to pass
back closest matches to colors when necessary. I've never run out of
colors since then.

There are caveats: programs that check for a full colormap and install
their own (color flashing) will automatically do so. This includes
netscape and XForms programs (which I was running with private color
maps anyway). My copy of LyriX makes the background black. Also, I tried
Mosaic on a Sun and had some odd color effects.

oly

-----


### ![](../gx/lil2cent.gif) X With 256 Colors

Date: Tue, 06 May 1997 09:40:10 -0400 (EDT)  
From: Oliver Oberdorf <oly@borg.harvard.edu>  

I forgot to add that the -cc 4 can be used like this:

    startx -- -cc 4

  
(I use xdm, so I don't have to do it this way)

sorry about that

oly

-----


### ![](../gx/lil2cent.gif) Video Cards on the S3/ViRGE

Date: Mon, 05 May 1997 20:44:13 -0400  
From: Peter Amstutz <amstpi@freenet.tlh.fl.us>  

A couple suggestions to people with video cards based on the S3/ViRGE
Chipset... (which is many video cards that ship with new computers that
claim to have 3D accelerated graphics. Don't believe it. The 3D graphics
capability of all ViRGE-based chips sucks. They make better cheap 2D
accelerators)

XFree 3.2 has a ViRGE server\! I have heard a number of people complain
about XFree's lack of ViRGE support. Yo GUYZ\! That's because your
wonderful Linux CD has XFree86 3.1.2 WHICH IS NOT THE MOST RECENT
VERSION\!

There is a minor hack you can make to svgalib 1.12.10 to get it to
reconignize your nice S3 based card as actually being such. The s3/ViRGE
chip is, in the words of some guy at C|Net, "basically a S3 Trio 64 with
a 3d engine bolted on top." (as noted, the 3D engine is really slow)
Unfortunately, it returns a card ID code totally different to the
Trio64. But, drum roll please, with just a little bit of hacking, you
too can do 1024x768x16bpp through svgalib\! Just follow these E-Z steps:

I)Get the source, untar it & everything. II) Go into the main source
directory, and with your favorite editor (vim forever\!), open up s3.c
III) Now, search for the nice little error message "S3: Unknown chip id
%02x\\n" around line 1552. Above it there should be a switch()/case
statement that figures out which card it you have based on an ID code.
Find the case statement that matches a Trio64. Insert a fall-through
case statement that matches the code your card returns, so svgalib
treats it as a Trio64\! Like this: (starts at line 1537 of s3.c)

``` 
       case 0x11E0:
        s3_chiptype = S3_TRIO64;
        break;
becomes
        case 0x11E0:
        case 0x31E1:
        s3_chiptype = S3_TRIO64;
        break;
```

  
Replace 0x31E1 with the appropriate ID if your card returns a different
code.

Save it\! You're home free\! Recompile, re-install libraries, and now,
what we've all been waiting for, test some svga modes\! 640x480x256\!
640x480x16bpp\! 800x600x24bpp\! YES\!\!\!

But wait\! One thing to watch out for. First, make sure you reinstall it
in the right place\! Slackware puts libvga.a in /usr/lib/, so make sure
that is that file that you replace. Another thing: programs compiled
with svgalib statically linked in will have to be rebuilt with the new
library, otherwise they will just go along in their brain dead fashion
blithely unaware that your card is not being used to nearly it's full
potential.

Note: this hack has not been authorized, reconignized, or in any way
endorsed, recommended, or even considered by the guy(s) who wrote
svgalib. The last version of svgalib is over a year old, so I don't
expect there to be any new versions real soon. It works for me, so I
just wanted to share it with the Linux community that just might find it
useful. This has only been tested on my machine, using a Diamond Stealth
3D 2000, so if you have a different ViRGE-based card and you have
problems you're on your own.

No, there are no Linux drivers that use ViRGE "accelerated 3D" features.
It sucks, I know (then again, the 3D performance of ViRGE chips is so
bad you're probably not missing much)

Peter Amstutz

-----


### ![](../gx/lil2cent.gif) C Source with Line Numbers

Date: 5 May 1997  
From: <joeh@sugar-river.net>  

I wanted to print out a c source with line numbers. Here is one way to
do it:

Assuming you are using bash, install the following function in your
.bashrc file.

    jnl () {
               for args
             do
               nl -ba $args > /tmp.tmp
             done
             lpr /tmp.tmp
           }

"nl" is a textutils utility that numbers the lines of a file.

"-ba" makes sure \*all\* the lines (even the empty lines) get numbered.

/tmp.tmp is my true "garbage" temporary file, hence I write over it, and
send it to the line printer.

For example to print out a file "kbd.c", with line numbers:

    jnl kdb.c 

There are probably 20 different methods of accomplishing the same thing,
but when you don't even have \*one\* of them in your bag of tricks, it
can be a time-consuming detour.

Note: I initially tried to name the function "nl", but this led to an
infinite loop. Hence I named it jnl (for Joe's number lines).

Best Regards,  
Joe Harmann

-----


### ![](../gx/lil2cent.gif) ncftp Vs. ftplib

Date: Thu, 08 May 1997 13:30:04 -0700  
From: Igor Markov <imarkov@math.ucla.edu>  

Hi, I read your 2c tip in Linux gazette regarding ftplib.

I am not sure why you recommend downloading ftpget, while another
package, actually, a single program, which is available on many systems
does various ftp services pretty well.

I mean ncftp ("nikFTP"). It can do command line, it can work in the mode
of usual ftp (with the "old" or "smarter" interface") and it also does
full-screen mode showing ETA during the transfer. It has filename and
hostname completion and a bunch of other niceties, like remembering
passwords if you ask it to.

Try man ncftp on your system (be in Linux or Solaris) ... also, ncftp is
available from every major Linux archive (including ftp.redhat.com where
you can find latest RPMs)

Hope this helps, Igor

-----


### ![](../gx/lil2cent.gif) Domain and Dynamic IP Names

Date: Thu, 08 May 1997 13:52:02 -0700  
From: Igor Markov <imarkov@math.ucla.edu>

I have a dial-up with dynamic IP and it has always been an incontinence
for me and my friends to learn my current IP address (I had an ftp
script which put the address every 10 minutes into \~/.plan file on my
acct at UCLA, then one could get the address by fingering the account).

However, recently I discovered a really cool project <http://www.ml.org>
which

  - can give you a dynamic IP name, i.e. when your computer gets a new
    IP address, it needs to contact www.ml.org and update its record.
    
    Once their nameserver reloads its tables (once every 5-10mins\!)
    your computer can be accessed by the name you selected when
    registered.
    
    For example, my Linux box has IP name math4.dyn.ml.org
    
    Caveat: if you are not online, the name can point to a random
    computer. In my case, those boxes are most often wooden (i.e.
    running Windoze ;-) so you would get "connection refused".
    
    In general, you need some kind of authentication scheme (e.g. if you
    telnet to my computer, it would say "Office on Rodeo Drive")

  - allows you to register domain name for free (e.g. you can register
    an alternative name for your computer at work which has a constant
    IP)

  - offer nameserver support for free (if you need it)

Isn't that cool ?

Cheers, Igor

-----


### ![](../gx/lil2cent.gif) netcfg Tool

Date: Sat, 10 May 1997 11:55:28 -0400  
From: Joseph Turian <turian@idt.net>

I used Redhat 4.0's netcfg tool to install my PPP connection, but found
that I could only use the Internet as root. I set the proper permissions
on my scripts and the pppd (as stated in the PPP Howto and the Redhat
PPP Tips documents), but I still could not use any Internet app from a
user's account. I then noticed that a user account \_could\_ access an
IP number, but could not do a DNS lookup. It turns out that I merely had
to chmod ugo+r /etc/resolv.conf

-----


### ![](../gx/lil2cent.gif) Putting Links to Your Dynamic IP

Date: Wed, 28 May 1997 13:24:45  
From: Nelson Tibbitt <nelson@interpath.com>

Sometimes it might be useful to allow trusted friends to connect to your
personal Linux box over the Internet. An easy way to do this is to put
links to your IP address on a full-time web server, then give the URL to
whomever. Why would you want to do that? Well, I do it so my sister can
telnet to Magnon, my laptop, for a chat whenever I'm connected.

However it might prove difficult if, like me, your ISP assigns your IP
address dynamically. So I wrote a short script to take care of this...
The script generates an html file containing my local IP address then
uploads the file via ftp to a dedicated web server on which I have
rented some web space. It runs every time a ppp connection is
established, so the web page always contains my current IP, as well as
the date/time I last connected.

This is pretty easy to set up, and the result is way cool. Just give my
sis (or anyone else I trust) the URL... then she can check to see if I'm
online whenever she wants, using Netscape from her vax account at RIT.
If I am connected, she can click to telnet in for a chat.

Here's how it works....

  - determine local IP address
  - write an html file containing date/time and links to the IP address
    that has been assigned
  - upload the html file to a dedicated web server using ftp (and a
    .netrc file)

To get ftp to work, I had to create a file named .netrc in my home
directory with a line that contains the ftp login information for the
remote server. My .netrc has one line that looks like this:

    machine ftp.server.com login ftpusername password ftppassword

For more information on the .netrc file and its format, try "man ftp".
Chmod it 700 (chmod 700 .netrc) to prevent other users from reading the
file. This isn't a big deal on my laptop, which is used primarily by
yours truly. But it's a good idea anyway.

Here's my script. There might be a better way to do all of this, however
my script works pretty well. Still, I'm always interested in ways to
improve my work, so if you have any suggestions or comments, feel free
to send me an email.

``` 

#!/bin/sh
# *** This script relies on the user having a valid local .netrc ***
# *** file permitting automated ftp logins to the web server!!   ***
#
# Slightly modified version of:
# Nelson Tibbitt's insignificant bash script, 5-6-97
# nelson@interpath.com
#
# Here are variables for the customizing...
# Physical destination directory on the remote server
# (/usr/apache/htdocs/nelson/ is the httpd root directory at my virtual
domain)
REMOTE_PLANDIR="/usr/apache/htdocs/nelson/LinuX/Magnon"
# Desired destination filename
REMOTE_PLANNAME="sonny.htm"
# Destination ftp server
# Given this and the above 2 variables, a user would find my IP address
at
# http://dedicated.web.server/LinuX/Magnon/sonny.htm
REMOTE_SERVER="dedicated.web.server"
# Local (writable) temporary directory
TMPDIR="/usr/tmp"
# Title (and header) of the html file to be generated
HTMLHEAD="MAGNON"
# Existing image on remote server to place in html file..
# Of course, this variable isn't necessary, and may be commented out.
If commented out,
# you'll want to edit the html file generation below to prevent an empty
image from appearing
# in your web page.
HTMLIMAGE="/LinuX/Magnon/images/mobile_web.gif"
# Device used for ppp connection
PPP_DEV="ppp0"
# Local temporary files for the html file/ftp script generation
TFILE="myip.htm"
TSCPT="ftp.script"
# Used to determine local IP address on PPP_DEV
#  There are several ways to get your IP, this was the first
command-line method I came
# up with.   It works fine here.  Another method, posted in May 1997
LJ  (and which looks
# much cleaner) is this:
#  `/sbin/ifconfig | awk 'BEGIN { pppok = 0} \
#   /ppp.*/ { pppok = 1; next } \
#  {if (pppok == 1 ) {pppok = 0; print} }'\
#  | awk -F: '{print $2 }'| awk  '{print $1 }'`
GETMYIP=$(/sbin/ifconfig | grep -A 4 $PPP_DEV \
  | awk '/inet/ { print $2 } ' | sed -e s/addr://)
# Used to place date/time of last connection in the page
FORMATTED_DATE=$(date '+%B %-d, %I:%M %p')
#
#
# Now, do it!  First give PPP_DEV time to settle down...
sleep 5
echo "Current IP: $GETMYIP"

# Generate the html file...
# Edit this part to change the appearance of the web page.
rm -f $TMPDIR/$TFILE
echo "Writing $REMOTE_PLANNAME"
echo >$TMPDIR/$TFILE
echo "<html><head><title>$HTMLHEAD</title></head><center>"   >> 
$TMPDIR/$TFILE
echo "<body bgcolor=#ffffff><font size=+3>$HTMLHEAD</font>"  >> 
$TMPDIR/$TFILE
# Remove the <imgtag in the line below if you don't want an image
echo "<p><img src='$HTMLIMAGE' alt='image'<p>The last "     >> 
$TMPDIR/$TFILE
echo "time I connected was <b>$FORMATTED_DATE</b>, when the " >>
$TMPDIR/$TFILE
echo "Net Gods dealt <b>$GETMYIP</bto Magnon. <p><a href="  >>
$TMPDIR/$TFILE
echo "http://$GETMYIP target=_top>http://$GETMYIP</a><p>"     >>
$TMPDIR/$TFILE
echo "<a href=ftp://$GETMYIP target=_top>ftp://$GETMYIP"  >>
$TMPDIR/$TFILE
echo "<p><a href=telnet://$GETMYIP>telnet://$GETMYIP</a><br>" >>
$TMPDIR/$TFILE
echo "(Telnet must be properly configured in your browser.)"  >>
$TMPDIR/$TFILE
# Append a notice about the links..
echo "<p>The above links will only work while I'm connected." >>
$TMPDIR/$TFILE

# Create an ftp script to upload the html file
 echo "put $TMPDIR/$TFILE" $REMOTE_PLANDIR/$REMOTE_PLANNAME >
$TMPDIR/$TSCPT
 echo "quit" >$TMPDIR/$TSCPT

# Run ftp using the above-generated ftp script (requires valid .netrc
file for ftp login to work)
 echo "Uploading $REMOTE_PLANNAME to $REMOTE_SERVER..."
 ftp $REMOTE_SERVER > $TMPDIR/$TSCPT &/dev/null

# The unset statements are probably unnecessary, but make for a clean
'look and feel'  
echo -n "Cleaning up... "
rm -f $TMPDIR/$TFILE ; rm -f $TMPDIR/$TSCPT
unset HTMLHEAD HTMLIMAGE REMOTE_SERVER REMOTE_PLANDIR REMOTE_PLANNAME
unset GETMYIP FORMATTED_DATE PPP_DEV TMPDIR TFILE TSCPT
echo "Done."

exit

```

-----


### ![](../gx/lil2cent.gif) Hard Disk Duplication

Date: Tue, 27 May 1997 11:16:32  
From: Michael Jablecki [mcablec@ucsd.edu](mailto:mcjablec@ucsd.edu)  

Shockingly enough, there seems to be a DOS product out there that will
happily make "image files" of entire hard disks and copy these image
files onto blank hard disks in a sector-by-sector fashion. Boot sectors
and partition tables should be transferred exactly. See:
<http://www.ingot.com> for more details. Seagate (I think...) has also
made a program that does the duplication in one step - transfers all of
one hard disk to another identical disk. I'm not sure which of these
products works with non-identical disks.

Hope this helps.

Michael Jablecki

-----


### ![](../gx/lil2cent.gif) Untar and Unzip

From: Paul

Oh, here's a little tidbit of info to pass on, this has been bugging me
for a while. Often times when people send in tips 'n' tricks, it
requires one to untar and unzip an archive. It usually suggested that
this be done in one of several cumbersome ways: gzcat foo.tar.gz | tar
zxvf - or 1. gunzip foo.tar.gz 2. tar xvf foo.tar or some other
multi-step method. There is a much easier, time-saving, space saving
method. The version of tar shipped with most distributions of Linux is
from the FSF GNU project. These people recognized that most tar archives
are usually gzipped and provided a 'decompress' flag to tar. This is
equivalent to the above methods: tar zxvf foo.tar.gz This decompress the
tar.gz file on the fly and then untars it into the current directory,
but it also leaves the original .tar.gz alone. However, one step I
consider essential that is usually never mentioned, is to look at what's
in the tar archive prior to extracting it. You have no idea whether the
archiver was kind enough to tar up the parent directory of the files, or
it they just tarred up a few files. The netscape tar.gz is a classic
example. When that's untarred, it dumps the contents into your current
directory. Using: gtar ztvf foo.tar.gz allows you to look at the
contents of the archive prior to opening it up and potetially writing
over files with the same name. At the very least, you will know what's
going on and be able to make provisions for it before you mess something
up. For those who are adventurous, (X)Emacs is capable of not only
opening up and reading a tar.gz file, but actually editing and re-saving
the contents of these as well. Think of the time/space savings in that\!
Seeya, Paul
