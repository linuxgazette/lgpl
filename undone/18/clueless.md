## Clueless at the Prompt

#### By Mike List, <troll@net-link.net>

-----

![](../gx/list/gnub.jpg)

#### Welcome to installment 5 of Clueless at the Prompt: a new column for new users.

-----

#### Getting Serious

If you've been experimenting with linux, reading all the docs you can
get your hands on, downloading software to try, and generally cleaning
up after the inevitable ill advised rm as root, you are probably
starting to get enough confidence in linux to use it to do more than
browse the internet. After all, why use Gates when you can jump the
fences? This month I'm going to discuss some strategies for damage
control, and how you can safely upgrade without losing valuable files
and configurations, as well as some more general scouting around the
filesystem.

-----

#### Partitions as Safety Devices

If you have your entire linux installation on one partition, or
partition, you could be putting your files and accmulated data in
jeopardy as well as making the business of upgrading more difficult.

I understand that some distributions, notably Debian, are capable of
upgrading any part of the system's component software without a full
install, but I'm running Slackware, and it's generally recommended that
when certain key system components are upgraded, a full reinstall is the
safest way to avoid conflicts between old and new parts. What to do when
the time comes can be much simpler if you have installed at least your
/home direcory on a separate partition.

When you do a fresh install you are asked to describe mount points for
your partitions. You are also asked if you want to format those
partitions. If your /home directory doesn't contain much in the way of
system files you can opt to skip formatting it, thereby reducing the
chance that you'll have to use your backup to recover lost files in
those directories. No, I'm not suggesting tht you don't have to backup
your /home or other personal files, since there is no reliable undelete
for linux that I'm aware of at this time. However, if you are just
experimenting with linux and using a separate OS to do your important
work and it's located on another disk, you may not feel to compelled to
backup much in the way of linux files. Sooner or later though, if you
are committed(or ought to be :) ) enough to linux to drop the other
system, you WILL want to rethink that omission.

-----

#### Formatting Floppies

When you format a floppy disk in MSDOS you do several operations in one
fell swoop. You erase files, line up the tracks, sectors, etc, and
install a MSDOS compatible filesystem. Another thing to recognize is
that MS mounts the floppy drive as a device, while in linux the device
is mounted as a part of the filesystem, to a specific directory.

There is a suite of utilities called mtools that can be used to create
DOS formatted floppies, as well as some other MS specific operations,
but I haven't had a lot of fun with it. I use the standard utilities
instead Here is how I format a floppy disk:

```
fdformat /dev/fd0xxx
```

where xxx is the full device name. My floppy drive is /dev/fd0u1440 but
your mileage may vary. Try ls'ing your /dev directory to see. I
installed from floppies, so I'm not real sure about CDROM installation
but I took note of the drive specified to install the system. When the
drive finishes formatting, you can type:

``` 
mkfs -t msdos /dev/fd0xxxx
```

once again if necessary adding any specifiers. Your disk should be
formatted.

-----

#### Writing to your Floppy Disk

You are probably sitting there with a newly msdos formatted floppy disk
and wondering how to write to it. If you use mtools, you are on your
own, but don't feel bad you will save some steps, ie. mount and umount
the floppy drive before and after writing to the drive, but it seems
that I always fail to remember some option when I try to use mtools, so
I don't use them. I type :

``` 
mount -t msdos /dev/fd0xxxx /mnt
```

you can specify another mount point besides /mnt if you would like,
perhaps a different mount point for each filesystem type that you might
want to use, ext2, or minix for example, but if you or people that you
work with use MS the msdos format might be the best, at least for now.

You can put an entry in your /etc/fstab that specifies the mount point
for your floppy drive, with a line that looks something like:

``` 
/dev/fd0         /mnt      msdos       rw,user,noauto  0   0 
```

This particular line will keep the floppy drive from mounting on bootup
(noauto), and allow users to mount the drive. You should take the time
to alert your users that they MUST mount and umount /dev/fd0 each time
they change a disk, otherwise they will not get a correct ls when they
try to read from the mount point. Assuming that this line is added to
the /etc/fstab file the correct command for mounting the drive is:

``` 
mount /dev/fd0
```

which will automatically choose /mnt as the mount point.To read from the
drive, the present working directory must be changed by:

``` 
cd /mnt
```

after which the contents of the disk can be read or written to\> Linux
is capable of reading files from several filesystem types, so it's a
pretty good first choice, since you can share files with DOS users.

Anyway, assuming you didn't get any error messages, you are ready to
copy a file to the disk using the:

``` 
cp anyfile.type /mnt
```

assuming tha /mnt is the mount point that you specified in the mount
command, you should have copied the file to your floppy disk. Try:

``` 
ls /mnt
```

you should see the file you just cp'ed. if not, you should retry the
mount command, but if you didn't get any error messages when you tried
to mount the drive, you should be OK. To verify that you did write to
the floppy instead of the /mnt directory, (there is a difference, if no
drive is mounted it's just a directory) you can:

``` 
umount /dev/fd0xxxx
```

and then try:

``` 
ls /mnt
```

upon which you should get a shell prompt. If you get the file name that
you tried to copy to floppy, merely rm it and try the whole routine
again. If you find this confusing, read up on mtools by:

``` 
info mtools
```

You may like what you see, give them a try. As I said I haven't had much
luck with them, but basically the mformat command should do the
abovementioned format tasks in one pass. Mcopy should likewise copy the
named file to the floppy without the need to separately mount the drive.

-----

#### Other Filesystems

There are several filesystems, as mentioned above that can be read by
linux. Minix, ext2, ext, xiaf, vfat, msdos(I'm still a little bit foggy
on the difference between these two).Still others can be read with the
use of applications, amiga for instance. That's why it makes sense to
split up what is a single step process in DOS.

-----

#### Humbly acknowledging...

I got a lot of mail regarding the locate command, which I'm woefully
guilty of spreading misinformation about. The real poop is that locate
is a byproduct of a command, updatedb, which can be run at any time. It
is run as default in the wee hours of the morning from /usr/bin/crontab,
which is where I got the idea to leave the computer on overnight.

-----

Next Time- Let me know what you would like to see in here and I'll try
to oblige just e-mail<troll@net-link.net> me and ask, otherwise I'll
just write about what gave me trouble and how I got past it.

TTYL, Mike List
