
![Welcom to the Graphics Muse](../gx/hammel/gm3.gif)

<table>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td style="text-align: center;">Set your browser to the width of the line below for best viewing.<br />
© 1997 by <a href="mailto:mjhammel@csn.net">mjh</a></td>
</tr>
</tbody>
</table>

-----

![Button Bar](../gx/hammel/buttons3.gif)

**muse:**

1.  *v;* to become absorbed in thought
2.  *n;* \[ fr. Any of the nine sister goddesses of learning and the
    arts in Greek Mythology \]: a source of inspiration

![W](../gx/hammel/w.gif)elcome to the Graphics Muse\! Why a "muse"?
Well, except for the sisters aspect, the above definitions are pretty
much the way I'd describe my own interest in computer graphics: it keeps
me deep in thought and it is a daily source of inspiration.

\[[Graphics Mews](gm.html#mews)\] \[[Musings](gm.html#musings)\]
\[[Resources](gm.html#resources)\]

<table>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><img src="../gx/hammel/cleardot.gif" width="1" height="1" alt="indent" /> <img src="../gx/hammel/t.gif" width="26" height="28" alt="T" />his column is dedicated to the use, creation, distribution, and discussion of computer graphics tools for Linux systems. This month I'll finally get around to the article on HF-Lab, John Beale's wonderful tool for creating 3D Heightfields. I've been meaning to do this for the past few months. I made sure I made time for it this month.<br />
      The other article from me this month is a quick update on the 3D modellers that are available for Linux. I didn't really do a comparative review, it's more of a ``this is what's available, and this is where to find them''. A full comparative review is beyond the scope of this column. Perhaps I'll do one for the Linux Journal sometime in the future.<br />
      I had planned to do a preview of the Gimp 1.0 release which is coming out very soon. However, I'll be doing a full article on the Gimp for the November graphics issue of <em>Linux Journal</em> and decided to postpone the introduction I had planned for the Muse. At the same time I had decided to postpone my preview, Larry Ayers contacted me to see if I was still doing my Gimp article for the Muse. He had planned on doing one on the latest version but didn't want to clash with my article. I told him to feel free and do his since I wasn't doing one too. He has graciously offered to place the preview here in the Muse and it appears under the ``More Musings...'' section.</td>
</tr>
</tbody>
</table>

<span id="mews"></span>

|                                         |
| :-------------------------------------- |
| ![Graphics Mews](../gx/hammel/mews.gif) |

  

  
      Disclaimer: Before I get too far into this I should note that any
of the news items I post in this section are just that - news. Either I
happened to run across them via some mailing list I was on, via some
Usenet newsgroup, or via email from someone. I'm not necessarily
endorsing these products (some of which may be commercial), I'm just
letting you know I'd heard about them in the past month.

![indent](../gx/hammel/cleardot.gif)

#### Zgv v2.8

      Zgv is a graphic file viewer for VGA and SVGA displays which
supports most popular formats. (It uses svgalib.) It provides a
graphic-mode file selector to select file(s) to view, and allows panning
and fit-to-screen methods of viewing, slideshows, scaling, etc.

Nothing massively special about this release, really, but some of the
new features are useful, and there is an important bugfix.

  - PCX support. (But 24-bit PCXs aren't supported.)
  - Much faster generation of JPEG thumbnails, thanks to Costa
    Sapuntzakis.
  - Optionally ditch the logo to get a proper, full-screen selector,
    with \`f' or \`z', or with \`fullsel on' in config file.
  - Thumbnail files can be viewed like other images, and thumbnail files
    are their own thumbnails - this means you can browse thumbnail
    directories even if you don't have the images they represent.
  - \`-T' option, to echo tagged files on exit.

<!-- end list -->

  - Thumbnail create/update for read-only media and DOS filesystems
    fixed. It previously created all of them each time rather than only
    doing those necessary.
  - Fixed problem with uncleared display when switching from zoom mode
    to scaling up.
  - The switching-from-X etc. now works with kernel 2.0.x. Previously it
    hanged. (It should still work with 1.2.x, too.)
  - Now resets to blocking input even when ^C'ed.
  - Various documentation \`bugs' fixed, e.g. the \`c' and \`n' keys
    weren't previously listed.

<!-- end list -->

  - ANSIfied the code. This caught a couple of (as it turned out)
    innocuous bugs. (Fortuitously, they had no ill effect in practice.)
  - Updated PNG support to work with libpng 0.81 (and, hopefully, any
    later versions).
  - Sped up viewing in 15/16-bit modes a little.
  - Incorporated Adam Radulovic's patch to v2.7 allowing more files in
    the directory and reducing memory usage.

Zgv can be found either in  
[sunsite.unc.edu:/pub/Linux/Incoming](ftp://sunsite.unc.edu:/pub/Linux/Incoming)
or  
[sunsite.unc.edu/pub/Linux/apps/graphics/viewers](ftp://sunsite.unc.edu/pub/Linux/apps/graphics/viewers).  
The files of interest are zgv2.8-src.tar.gz and zgv2.8-bin.tar.gz.

Editor's Note: I don't normally include packages that aren't X-based,
but the number of announcements for this month were relatively small so
I thought I'd go ahead and include this one. I don't plan on making it a
practice, however.

![indent](../gx/hammel/cleardot.gif)

#### Attention: OpenGL and Direct3D programmers

      [Mark Kilgard](mailto:mjk@fangio.asd.sgi.com), author of *OpenGL
Programming for the X Window System*, posted the following announcement
on the [comp.graphics.api.opengl](news:comp.graphics.api.opengl)
newsgroup. I thought it might be of interest to at least a few of my
readers.

The URL below explains a fast and effective technique for applying
texture mapped text onto 3D surfaces. The full source code for a tool to
generate texture font files (.txf files) and an API for easy rendering
of the .txf files using OpenGL is provided.

For a full explanation of the technique including sample images showing
how the technique works, please see:  
[http://reality.sgi.com/mjk\_asd/  
     
tips/TexFont/TexFont.html](http://reality.sgi.com/mjk_asd/tips/TexFont/TexFont.html)

Direct3D programmers are invited to see how easy and powerful OpenGL
programming is. In fact, the technique demonstrated is not immediately
usable on Direct3D because it uses intensity textures (I believe not in
Direct3D), polygon offset, and requires alpha testing, alpha blending,
and texture modulation (not required to be implemented by Direct3D). I
mean this to be a constructive demonstration of the technical
inadequacies of Direct3D.

I hope you find the supplied source code, texture font generation
utility, sample .txf files, and explanation quite useful.

Note: for those that aren't aware of it, Direct3D is Microsoft's answer
to OpenGL. Despite their original support of OpenGL, they aparently
decided to go with a different 3D standard, one they invented (I think).
Anyway, the discussion on comp.graphics.api.opengl of late has been
focused on which of the two technologies is a better solution.

![indent](../gx/hammel/cleardot.gif)

![indent](../gx/hammel/cleardot.gif)

#### Epson PhotoPC and PhotoPC 500 digital cameras

      Epson PhotoPC and PhotoPC 500 are digital still cameras. They are
shipped with Windows and Mac based software to download the pictures and
control the camera parameters over a serial port.

Eugene Crosser wrote a C library and a command-line tool to perform the
same tasks under UNIX. See

      <ftp://ftp.average.org/pub/photopc/>

MD5(photopc-1.0.tar.gz)= 9f286cb3b1bf29d08f0eddf2613f02c9

Eugene Crosser; 2:5020/230@fidonet; <http://www.average.org/~crosser/>

![indent](../gx/hammel/cleardot.gif)

#### ImageMagick V3.8.5

      Alexander Zimmerman has released a new version of ImageMagick. The
announcment, posted to comp.os.linux.announce, reads as follows:

> I just uploaded to sunsite.unc.edu
> 
> ImageMagick-3.8.5-elf.lsm  
> ImageMagick-3.8.5-elf.tgz
> 
> This is the newest version of my binary distribution of ImageMagick.
> It will move to the places listed in the LSM-entry at the end of this
> message. Please remember to get the package libIMPlugIn-1.1 too, to
> make it working.
> 
> This version brings together a number of minor changes made to
> accomodate PerlMagick and lots of minor bug fixes including multi-page
> TIFF decoding and writing PNG.
> 
> ImageMagick (TM), version 3.8.5, is a package for display and
> interactive manipulation of images for the X Window System.
> ImageMagick performs, also as command line programs, among others
> these functions:
> 
>   - Describe the format and characteristics of an image
>   - Convert an image from one format to another
>   - Transform an image or sequence of images
>   - Read an image from an X server and output it as an image file
>   - Animate a sequence of images
>   - Combine one or more images to create new images
>   - Create a composite image by combining several separate images
>   - Segment an image based on the color histogram
>   - Retrieve, list, or print files from a remote network site
> 
> ImageMagick also supports the Drag-and-Drop protocol from the OffiX
> package and many of the more popular image formats including JPEG,
> MPEG, PNG, TIFF, Photo CD, etc.
> 
> **Primary-site:**

ftp.wizards.dupont.com /pub/ImageMagick/linux

986k ImageMagick-i486-linux-ELF.tar.gz

884k PlugIn-i486-linux-ELF.tar.gz

**Alternate-site:**

sunsite.unc.edu /pub/Linux/apps/graphics/viewers/X

986k ImageMagick-3.8.5-elf.tgz

1k ImageMagick-3.8.5-elf.lsm

sunsite.unc.edu /pub/Linux/libs/graphics

884k libIMPlugIn-1.1-elf.tgz

1k libIMPlugIn-1.1-elf.lsm

**Alternate-site:**

ftp.forwiss.uni-passau.de /pub/linux/local/ImageMagick

986k ImageMagick-3.8.5-elf.tgz

1k ImageMagick-3.8.5-elf.lsm

884k libIMPlugIn-1.1-elf.tgz

1k libIMPlugIn-1.1-elf.lsm

![indent](../gx/hammel/cleardot.gif)

#### VARKON Version 1.15A

      VARKON is a high level development tool for parametric CAD and
engineering applications developed by Microform, Sweden. 1.15A includes
new parametric functions for creation and editing of sculptured surfaces
and rendering based on OpenGL.

Version 1.15A of the free version for Linux is now available for
download at:  
<http://www.microform.se>

![indent](../gx/hammel/cleardot.gif)

![indent](../gx/hammel/cleardot.gif)

#### Shared library version of xv 3.10a

      xv-3.10a-shared is the familiar image viewer program with all
current patches modified to use the shared libraries provided by libgr.

xv-3.10a-shared is available from <ftp://ftp.ctd.comsat.com/pub/>.
libgr-2.0.12.tar.gz is available from
<ftp://ftp.ctd.comsat.com/pub/linux/ELF/>.

![indent](../gx/hammel/cleardot.gif)

#### t1lib-0.2-beta - A Library for generating Bitmaps from Adobe Type 1 Fonts

      t1lib is a library for generating character- and string-glyphs
from Adobe Type 1 fonts under UNIX. t1lib uses most of the code of the
X11 rasterizer donated by IBM to the X11-project. But some disadvantages
of the rasterizer being included in X11 have been eliminated. Here are
the main features:

  - t1lib is completely independent of X11 (although the program
    provided for testing the library needs X11)
  - fonts are made known to library by means of a font database file at
    runtime
  - searchpaths for all types of input files are configured by means of
    a configuration file at runtime
  - characters are rastered as they are needed
  - characters and complete strings may be rastered by a simple function
    call
  - when rastering strings, pairwise kerning information from .afm-files
    may optionally be taken into account
  - an interface to ligature-information of afm-files is provided
  - rotation is supported at any angle
  - there's limited support for extending and slanting fonts
  - new encoding vectors may be loaded at runtime and fonts may be
    reencoded using these encoding vectors
  - antialiasing is implemented using three gray-levels between black
    and white
  - a logfile may be used for logging runtime error-, warning- and other
    messages
  - an interactive test program called "xglyph" is included in the
    distribution. This program allows to test all of the features of the
    library. It requires X11.

Author: Rainer Menzner ( <rmz@neuroinformatik.ruhr-uni-bochum.de>)

You can get t1lib by anonymous ftp at:  
[ftp://ftp.neuroinformatik.ruhr-uni-bochum.de/  
   
pub/software/t1lib/t1lib-0.2-beta.tar.gz](ftp://ftp.neuroinformatik.ruhr-uni-bochum.de/pub/software/t1lib/t1lib-0.2-beta.tar.gz)

An overview of t1lib including some screenshots of xglyph can be found
at:  
[http://www.neuroinformatik.ruhr-uni-bochum.de/  
   
ini/PEOPLE/rmz/t1lib.html](http://www.neuroinformatik.ruhr-uni-bochum.de/ini/PEOPLE/rmz/t1lib.html)

![indent](../gx/hammel/cleardot.gif)

#### Freetype Project - The Free TrueType Font Engine  
Alpha Release 4

      The FreeType library is a free and portable TrueType font
rendering engine. This package, known as \`Alpha Release 4' or \`AR4',
contains the engine's source code and documentation.

What you'll find in this release are:

  - better portability of the C code than in the previous release.
  - font smoothing, a.k.a. gray-level rendering. Just like Win95, only
    the diagonals and curves are smoothed, while the vertical and
    horizontal stems are kept intact.
  - support for all character mappings, as well as glyph indexing and
    translation functions (incomplete).
  - full-featured TrueType bytecode interpreter \!\! The engine is now
    able to hint the glyphs, thus producing an excellent result at small
    sizes. We now match the quality of the bitmaps generated by Windows
    and the Mac\! Check the \`view' test program for a demonstration.
  - loading of composite glyphs. It is now possible to load and display
    composite glyphs with the \`zoom' test program. However, composite
    glyph hinting is not implemented yet due to the great incompleteness
    of the available TrueType specifications.

Also, some design changes have been made to allow the support of the
following features, though they're not completely implemented yet:

  - multiple opened font instances
  - thread-safe library build
  - re-entrant library build
  - and of course, still more bug fixes ;-)

Source is provided in two programming languages: C and Pascal, with some
common documentation and several test programs. The Pascal source code
has been successfully compiled and run with Borland Pascal 7 and
fPrint's Virtual Pascal on DOS and OS/2, respectively. The C source code
has been successfully compiled and run on various platforms including
DOS, OS/2, Amiga, Linux and several other variants of UNIX. It is
written in ANSI C and should be very easily ported to any platform.
Though development of the library is mainly performed on OS/2 and Linux,
the library does not contain system-specific code. However, this package
contains some graphics drivers used by the test programs for display
purposes on DOS, OS/2, Amiga and X11.

Finally, the FreeType Alpha Release 4 is released for informative and
demonstration purpose only. The authors provide it \`as is', with no
warranty.

The file freetype-AR4.tar.gz (about 290K) is available now at
<ftp://sunsite.unc.edu/pub/Linux/X11/fonts> or at the FTP site in:
<ftp://ftp.physiol.med.tu-muenchen.de/pub/freetype>

Web page:  
<http://www.physiol.med.tu-muenchen.de/~robert/freetype.html>  
The home site of the FreeType project is  
<ftp://ftp.physiol.med.tu-muenchen.de/pub/freetype>  
There is also a mailing list:  
<freetype@lists.tu-muenchen.de>  
Send the usual subscription commands to:  
<majordomo@lists.tu-muenchen.de>

Copyright 1996

David Turner

Copyright 1997

Robert Wilhelm

Werner Lemberg

![indent](../gx/hammel/cleardot.gif)

![indent](../gx/hammel/cleardot.gif)

![indent](../gx/hammel/cleardot.gif)

#### Did You Know?

...the Portal web site for **xanim** has closed down. The new primary
sites are:

> <http://xanim.va.pubnix.com/home.html>  
> <http://smurfland.cit.buffalo.edu/xanim/home.html>  
> <http://www.tm.informatik.uni-frankfurt.de/xanim/>
> 
> The latest revision of xanim is 2.70.6.4.

I got the following message from a reader. Feel free to contact him with
your comments. I have no association with this project.

> I'm currently working on an application to do image processing and
> Computer Vision tasks. In the stage of development, I would like to
> know what the community expects from such a product, so if you would
> like the status of the work, please come and visit:  
> <http://www-vision.deis.unibo.it/~cverond/cvw>  
> Expecially the "sample" section, where you can see some of the
> application's functionality at work, and leave me a feedback. Thanks
> for your help. Cristiano Verondini
> [cverondini@deis.unibo.it|](mailto:cverondini@deis.unibo.it%7C)

**Q and A**

*Q: Can someone point me to a good spot to download some software to
make a good height map?*

A: I'd suggest you try either John Beale's hflab available at:
<http://shell3.ba.best.com/~beale/> Look under sources. You will find
executables for Unix and source code for other systems. It is pretty
good at manipulating and creating heightfields and is great at making
heightfields made in a paint program more realistic.  
      For the ultimate in realism use dem2pov by Bill Kirby, also
available at John Beale's web site to convert DEM files to TGA
heightfields. You can get DEM files trough my DEM mapping project at
<http://www.sn.no/~svalstad/hf/dem.html> or directly from
<ftp://edcftp.cr.usgs.gov/pub/data/DEM/250/>  
      As for your next question about what the pixel values of
heightfields mean, there are three different situations:

1.  High quality heightfields use a 24bit TGA or PNG file to store 16
    bit values with the most significant byte in the red component, the
    least significant byte in the green component and the blue component
    empty.
2.  8bit GIF files store a colour index where the colour with index
    number 0 becomes the lowest part of the heightfield and the colour
    with index number 255 becomes the highest part.
3.  8bit greyscale GIF files; the darkest colours become the lowest part
    of the heightfield and the lightest colours becomes the higherst
    part.

From Stig M. Valstad via the IRTC-L mailing list  
<svalstad@sn.no>  
<http://www.sn.no/~svalstad>

*Q: Sorry to pester you but I've read your minihowto on graphics in
Linux and I still haven't found what I'm looking for. Is there a tool
that will convert a collection of TGA files to one MPEG file in Linux?*

A: I don't know of any offhand, but check the following pages. They
might have pointers to tools that could help.  

<http://sunsite.unc.edu/pub/multimedia/animation/mpeg/berkeley-mirror/>
<http://xanim.va.pubnix.com/home.html> (this is Xanim's home page).

You probably have to convert your TGA's to another format first, then
encode them with mpeg\_encode (which can be found at the first site
listed above).

*Q: Where can I find some MPEG play/encode tools?*

A:
<http://sunsite.unc.edu/pub/multimedia/animation/mpeg/berkeley-mirror/>

*Q: Where can I find free textures on the net in BMP, GIF, JPEG, and PNG
formats?*

A: Try looking at:  
      <http://axem2.simplenet.com/heading.htm>

These are the textures I've started using in my OpenGL demos. They are
very professional. There are excellent brick and stone wall textures. If
you are doing a lot of modeling of walls and floors and roads, the web
site offers a CD-ROM with many more textures.

Generally, I load them into "xv" (an X image viewer utility) and
resample them with highest-quality filtering to be on even powers of two
and then save them as a TIFF file. I just wish they were already at
powers of two so I didn't have to resample.

Then, I use Sam Leffler's very nice libtiff library to read them into my
demo. I've got some example code of loading TIFF images as textures
at:  
      <http://reality.sgi.com/mjk_asd/tiff_and_opengl.html>

From: Mark Kilgard \<<mjk@fangio.asd.sgi.com>\>, author of *OpenGL
Programming for the X Window System*, via the
[comp.graphics.api.opengl](news:comp.graphics.api.opengl) newsgroup.

*Q: Why can't I feed the RIB files exported by AMAPI directly into
BMRT?*

A: According to <shem@warehouse.net>:

> Thomas Burge from Apple who has both the NT and Apple versions of
> AMAPI explained to me what the situation is - AMAPI only exports RIB
> entity files; you need to add a fair chunk of data before a RIB
> WorldBegin statement to get the camera in the right place and facing
> the right way. As it were, no lights were enabled and my camera was
> positioned underneath the object, facing down\! There is also a Z-axis
> negation problem in AMAPI, which this gentleman pointed out to me and
> gave me to the RIB instructions to compensate for it.

*Q: Is there an OpenGL tutorial on-line? The sample code at the OpenGl
WWW center seems pretty advanced to me.*

A: There are many OpenGL tutorials on the net. Try looking at:  
<http://reality.sgi.com/mjk_asd/opengl-links.html>

Some other good ones are:

  - OpenGL overview -
    <http://www.sgi.com/Technology/openGL/paper.design/opengl.html>
  - OpenGL with Visual C++ -
    <http://www.iftech.com/oltc/opengl/opengl0.stm>
  - OpenGL and X, an intro -
    <http://www.sgi.com/Technology/openGL/mjk.intro/intro.html>

From Mark Kilgard

*Q: So, like, is anyone really reading this column?*

A: I have no idea. Is anyone out there?

![indent](../gx/hammel/cleardot.gif)

![indent](../gx/hammel/cleardot.gif)

![indent](../gx/hammel/cleardot.gif)

<span id="musings"></span>

|                                      |
| ------------------------------------ |
| ![Musings](../gx/hammel/musings.gif) |

  

#### 3D Modellers Update

      Recently there has been a minor explosion of 3D modellers. Most of
the modellers I found the first time out are still around, although some
are either no longer being developed or the developers have not released
a new version in awhile. Since I haven't really covered the range of
modellers in this column since I started back in November 1996, I
decided it was time I provided a brief overview of what's available and
where to get them.  
      The first thing to do is give a listing of what tools are
available. The following is the list of modellers I currently know
about, in no particular order:

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li>AC3D</li>
<li>SCED/SCEDA</li>
<li>Midnight Modeller</li>
<li>AMAPI</li>
<li>Bentley Microstation 95</li>
</ul></td>
<td><ul>
<li>Aero</li>
<li>Leo3D</li>
<li>MindsEye</li>
<li>3DOM</li>
</ul></td>
</tr>
</tbody>
</table>

There is also the possibility that bCAD is available for Linux as a
commercial port, but I don't have proof of this yet. Their web site is
very limited as to contact information, so I wasn't able to send them
email to find out for certain. The web pages at 3DSite for bCAD do not
list any Unix ports for bCAD, although they appear to have a command
line renderer for Unix.  
      There are also a couple of others I'm not sure how to classify,
but the modelling capabilities are not as obvious so I'll deal with them
in a future update (especially if they contact me with details on their
products).  
      All of these use graphical, point-and-click style interfaces.
Other modellers use programming languages but no graphical interface,
such as POV-Ray, Megahedron and BMRT (via its RenderMan support). Those
tools are not covered by this discussion.  
      The list of modellers can be broken into three categories: stable,
under development, and commercial. The stable category includes AC3D,
SCED/SCEDA, and Midnight Modeller. Commercial modellers are the AMAPI
and Megahedron packages, and Bentley Microstation. The latter is
actually free for non-commercial unsupported use, or $500 with support.
Below are short descriptions of the packages, their current or best
known status and contact information. The packages in the table are
listed alphabetically.

Product and description

Imports

Exports

Availability

Contact

[3DOM](gx/hammel/3dom.gif) - Very early development. I haven't tried
this one yet.

Unknown

Unknown

Freeware

http://www.cs.kuleuven.ac.be/cwis/research/graphics/3DOM/

[AC3D](../gx/hammel/ac3d.jpg) - OpenGL based vertex modeller with
multiple, editable views plus a 3D view. Includes ability to move,
rotate, resize, position, and extrude objects. Objects can be named and
hidden. Includes support for 2D (line (both poly and polylines) ,
circle, rectangle, ellipse, and disk) and 3D (box, sphere, cylinder and
mesh). Fairly nice 3D graphical interface that looks like Motif but
doesn't require Motif libraries.

Imports DXF, Lightwave, Triangle, vector formatted object files.

Generates RenderMan, POV-Ray 2.2, VRML, Massive, DVS, Dive and Triangle
formatted object files.

Shareware

[http://www.comp.lancs.ac.uk/  
computing/users/andy/ac3dlinux.html](http://www.comp.lancs.ac.uk/computing/users/andy/ac3dlinux.html)

Aero - The following is taken from the documentation that accompanies
the package:

> AERO is a tool for editing and simulating scenes with rigid body
> systems. You can use the built-in 4-view editor to create a virtual
> scene consisting of spheres, cuboids, cylinders, planes and fix
> points. You can link these objects with rods, springs, dampers and
> bolt joints and you can connect forces to the objects. Then you can
> begin the simulation and everything starts moving according to the
> laws of physics (gravitation, friction, collisions). The simulation
> can be viewed as animated wire frame graphics. In addition you can use
> POV-Ray to render photo-realistic animation sequences.

This package requires the FSF Widget library, which I don't have. The
last time I tried to compile that library it didn't work for me, but
maybe the build process works better now. Anyway, I haven't seen this
modeller in action.

Proprietary ASCII text format

POV-Ray

<http://www.informatik.uni-stuttgart.de/ipvr/bv/aero/>  
<ftp://ftp.informatik.uni-stuttgart.de/pub/AERO>

[AMAPI](gx/hammel/amapi.jpg) - Fairly sophisticated, including support
for NURBS and a macro language. Interface is quit unique for X
applications, probably based on OpenGL. The version available from
Sunsite doesn't work quite right on my system. Some windows don't get
drawn unless a refresh is forced and the method for doing a refresh is
kind of trial-and-error. The trial version of 2.11 has the same problem.
Perhaps this is a problem with the OpenGL they use, although a check
with ldd doesn't show any dependencies on OpenGL. I wish this worked. I
really like the interface.

Yonowat, the maker of AMAPI, has a trial version, 2.11, available for
download from their web site. They are also porting another of their
products AMAPI Studio 3.0, a more advanced modeling tool, to Linux. The
web site doesn't mention when it might be ready but the description on
the pages look \*very\* interesting.

DXF, 3DS R3 and R4, IGES, Illustrator, Text, has its own proprietary
format

DXF, CADRender, Text, AMAPI, 3DS R3 and R4, Ray Dream Designer,
Lightwave, 3DGF, Truespace V2.0, Caliray, POV 3.0, IGES, Explore, VRML,
STL, Illustrator, RIB

Shareware - $25US, $99US will get you a 200 page printed manual.
Personal use copies for Linux are free for a year, but commercial,
government, and institutional users must register their copies.

<http://www.informatik.uni-stuttgart.de/ipvr/bv/aero/>  
<ftp://ftp.informatik.uni-stuttgart.de/pub/AERO>

[Leo3D](gx/hammel/leo3d.jpg) - The following is taken from the README
file in the Leo3D distribution:

> Leo 3D is a real time 3D modelling application which enables you to
> create realistic 3D scenes using different rendering applications
> (such as Povray or BMRT for example). It also exports VRML files.
> 
> What distinguishes Leo 3D from most other modelling applications is
> that all object transformations are done directly in the viewing
> window (no need for three seperate x, y, and z windows). For example,
> to move an object, all you need to do is grab and drag (with the
> mouse) one of the 'blue dots' which corresponds to the 2D Plane for
> which you wish to move the object. Scaling and rotation is done in the
> same way with the yellow and magenta dots respectively.

This modeller has a very cool interface based on OpenGL, GLUT, TCL and
Tix. I had problems with it when trying to load files, but just creating
and shading a few objects was quite easy and rather fun, actually. This
modeller certainly has some of the most potential of the non-commercial
modellers that I've seen. However, it still has some work to do to fix a
few obvious bugs.

DXF

POV-Ray, RenderMan, VRML 1.0, JPEG

Shareware - $25US

<ftp://s2k-ftp.cs.berkeley.edu/pub/personal/mallekai/leo3d.html> (Yes,
that's an ftp site with an HTML page.)

Bentley Microstation 95 and MasterPiece - Commercial computer-aided
design product for drafting, design, visualization, analysis, database
management, and modeling with a long history on MS, Mac and other Unix
platforms. Includes programming support with a BASIC language and
linkages to various commericial databases such as Oracle and Informix.
The product seems quite sophisticated based on their web pages, but I've
never seen it in action. I have seen a number of texts at local
bookstores relating to the MS products, so I have a feeling the Linux
ports should be quite interesting. Bentley's product line is quite
large. This looks like the place to go for a commercial modeller,
although I'm not certain if they'll sell their educational products to
the general public or not. If anyone finds out please let me know. Note
that the Linux ports have not been released (to my knowledge - I'm going
by what's on the web pages).

DXF, DWG and IGES

Unknown

Commercial, primarily targeted at educational markets, however they
appear open to public distributions and ports of their other packages if
enough interest is shown by the Linux community.

<http://www.bentley.com/ema/academic/aclinux.htm>  
<http://www.bentley.com/ema/academic/academic.htm>

[Midnight Modeller](gx/hammel/mnm.jpg) - A direct port of the DOS
version to Linux. The X interface looks and acts just like the DOS
version. On an 8 bit display the colors are horrid, but it's not so bad
on 24 bit displays. It seems to have a problem seeing all the
directories in the current directory when trying to open files.

The DOS version is being ported to Windows but it doesn't appear a port
of this version will be coming for Linux. The original Linux-port author
says he's still interested in doing bug fixes but doesn't expect to be
doing any further feature enhancement.

DXF, Raw

DXF, Raw

Freeware

[ftp://ftp.infomagic.com/pub/mirrors/.mirror1/  
      sunsite/apps/graphics/rays/pov/  
     
mnm-linux-pl2.static.ELF.gz](ftp://ftp.infomagic.com/pub/mirrors/.mirror1/sunsite/apps/graphics/rays/pov/mnm-linux-pl2.static.ELF.gz)  
[ftp://ftp.infomagic.com/pub/mirrors/.mirror1/  
      sunsite/apps/graphics/rays/pov/  
     
mnm-linux-pl2.static.ELF.gz](ftp://ftp.infomagic.com/pub/mirrors/.mirror1/sunsite/apps/graphics/rays/pov/mnm-linux-pl2.static.ELF.README)  
Author: Michael Lamertz \<<mlamertz@odars.de>\>

[MindsEye](mindseye.html) - MindsEye - A new modeller in very early
development which is based on both OpenGL/MesaGL and QT. Is designed to
allow plug-ins. The project has a mailing list for developers and other
interested parties and appears to have more detailed design
specifications than most "community developed" projects of this nature.
It's been a while coming, but the modeller is starting to take shape.
Last I looked they were beginning to work on adding autoconf to the
build environment, which is a very good thing to do early on in a
project, like this one is.

DXF, others planned

Unknown

GNU GPL

<http://www.ptf.hro.nl/free-d/> - Web Site  
<ftp://ftp.cs.umn.edu/users/mein/mindseye/> - source code

[SCED/SCEDA](gx/hammel/sced.jpg) - The following is taken from the
README file in the SCED distribution:

> Sced is a program for creating 3d scenes, then exporting them to a
> wide variety of rendering programs. Programs supported are: POVray,
> Rayshade, any VRML browser, anything that reads Pixar's RIB format,
> and Radiance. Plus a couple of local formats, for me.
> 
> Sced uses constraints to allow for the accurate placement of objects,
> and provides a maintenance system for keeping this constraints
> satisfied as the scene is modified.

This is a very sophisticated modeller, but the Athena interface makes it
look less powerful than it is. I used this modeller for many of the
scenes I created when I first started into 3D and still like its
constraint system better than what is available in AC3D (which doesn't
really have constraints in same sense, I don't think). SCED's biggest
limitation is its lack of support for importing various 3D formats.

SCEDA is a port of SCED that allows for keyframed animation. Objects are
given initial and ending positions and the modeller creates the frames
that will fill in the spaces between these two points.

Proprietary scene format and OFF (wireframe format)

POV 3.0, Radiance, RenderMan, VRML 1.0

Freeware (GPL'd)

<http://http.cs.berkeley.edu/~schenney/sced/sced.html>  
<ftp://ftp.cs.su.oz.au/stephen/sced>  
<ftp://ftp.povray.org/pub/pov/modellers/sced>

![indent](../gx/hammel/cleardot.gif)

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td><h4 id="hf-lab"><em>HF-Lab</em></h4>
      Height fields are convenient tools for representing terrain data that are supported directly by POV-Ray and through the use of displacement maps or patch meshes in BMRT. With POV-Ray and displacement maps in BMRT, a 2D image is used to specify the height of a point based on the color and/or intensity level for the point in the 2D image. The renderer uses this image, mapped over a 3D surface, to create mountains, valleys, plateaus and other geographic features. Creating a representative 2D image is the trick to realistic landscapes. <a href="http://www.best.com/~beale">HF-Lab</a>, an X-based interactive tool written by <a href="mailto:beale@best.com">John Beale</a>, is an easy to use and extremely useful tool for creating these 2D images.<br />
      Once you have retrieved the source, built (instructions are included and the build process is fairly straightforward, although it could probably benefit from the use of imake or autoconf) and installed it, you're ready to go. HF-Lab is a command line oriented tool that provides its own shell from which commands can be entered. To start HF-Lab using BASH type
<p>  % export HFLHELP=$HOME/hf/hf-lab.hlp<br />
  % hlx</p>
<p>and in csh type</p>
<p>  % setenv HFLHELP $HOME/hf/hf-lab.hlp<br />
  % hlx</p>
<p>Note that the path you use for the HFHELP environment variable depends on where you installed the hf-lab.hlp file from the distribution. The build process does not provide a method for installing this file for you so you'll need to be sure to move the file to the appropriate directory by hand. You definitely want to make sure this file is properly installed since the online help features in HF-Lab are quite nice.<br />
      The first thing you notice is the <a href="hf1.html">shell prompt</a>. From the prompt you type in one or more commands that manipulate the current height field (there can be more than one, each of which occupies a place on the stack). We've started by using the online help feature. Typing <em>help</em> by itself brings up the list of available commands, categorized by type. Typing <em>help &lt;command&gt;</em> (without the brackets, of course) gets you help on a particular command. In <a href="hf1.html">Figure 1</a> the help for the <em>crater</em> command is shown.<br />
      Now lets look at the available features. John writes in the documentation that accompanies the source:</p>
<blockquote>
HF-Lab commands fall into several categories: those for generating heightfields (HFs), combining or transforming them, and viewing them are the three most important. Then there are other 'housekeeping' commands to move HFs around on the internal stack, load and save them on the disk, and set various internal variables.
</blockquote>
Generating HFs are done with one of <em>gforge, random, constant,</em> and <em>zero</em>. The first of these, <em>gforge</em>, is the most interesting as it will create fractal-based fields. <em>Random</em> creates a field based on noise patterns (lots of spikes, perhaps usable as grass blades up close in a rendered scene) while <em>constant</em> and <em>zero</em> create level planes. <em>Zero</em> is a just a special case of constant where the height value is 0.<br />
      Each HF that is generated gets placed on the <em>stack</em>. The stack is empty to start. Running one of the HF generation commands will add a HF to top of the stack. By default there are 4 slots in the stack that can be filled, but this number can be changed using the <em>set stacksize</em> command. The HFs on the stack can be popped, swapped, duplicated, and named and the whole stack can be rotated. Also, rotation can be between the first 3 HFs on the stack.<br />
      The normal proces for creating a HF usually includes the following steps:
<ol>
<li>Generate one or two HFs with gforge</li>
<li>Manipulate the HFs with the <em>crater</em> or <em>pow</em> commands.</li>
<li>View the HF in 3D.</li>
<li>Manipulate some more.</li>
<li>Check it again.</li>
<li>Continue, ad infinitum.</li>
</ol>
Manipulating a HF can be done in several ways. First, there are a set of commands to operate on a single HF, the <em>One HF-Operators</em>. A few of the more interesting of these are the <em>pow, zedge, crater, fillbasin,</em> and <em>flow</em> commands. <em>Zedge</em> flattens the edges of the HF (remember that a HF is really just a 3D representation of a 2D image, and those images are rectangular). <em>Crater</em> adds circular craters to the HF of various radii and depths. <em>Fillbasin</em> and
<a href="gm.html#next-column">-Top of next column-</a></td>
<td><img src="../gx/hammel/cleardot.gif" width="0" height="0" alt="indent" /></td>
<td><table>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><ul>
<li><a href="gimp.html">Gimp 1.0</a> - Larry Ayers provides a preview of the newest version of the Unix worlds answer to Adobe Photoshop.</li>
</ul>
<img src="../gx/hammel/cleardot.gif" width="1" height="1" alt="indent" /></td>
</tr>
<tr class="even">
<td><img src="../gx/hammel/cleardot.gif" width="1" height="1" alt="indent" /></td>
</tr>
<tr class="odd">
<td><img src="../gx/hammel/cleardot.gif" width="1" height="1" alt="indent" /><br />
<em>flow</em> can be used together to etch out river valleys. There are examples, <a href="hf1.html#erosion">erosion1.scr</a> and <a href="hf1.html#erosion">erosion2.jpg</a> in the distribution which show this.<br />
      There are two ways to view the images you create with HF-Lab from within the application. One is to view the 2D greyscale image that will be saved to file. Viewing the 2D image is done with the <em>show</em> command. The other method is as an representative rendering of the HF in 3D, so that you'll get a better idea of what the final rendering will be with POV or BMRT. Viewing the 3D images is done in a secondary shell (although it is also possible to simply ask that shell to display the image and return immediately to the command shell - this is probably what you'll do once you've gotten more experienced with HF-Lab). The <em>view</em> command enters the user into the 3D viewing shell. From here you can set the level of detail to show, the position of a lightsource or the cameras eye, lighten, darken, tile and change the scale of the display. To exit the secondary shell, simply type <em>quit</em>.<br />
      HF-Lab supports a number of different file formats for reading and writing: PNG, GIF, POT, TGA, PGM, MAT, OCT, and RAW. Most of these formats have special purposes, but for use with POV-Ray and BMRT you should save files in TGA format. POV-Ray can use this format directly, but for use with BMRT you will need to convert the TGA image to TIFF format. Using TGA allows you to save the image information without data loss and conversion from TGA to TIFF is relatively easy using XV, NetPBM, or ImageMagick.<br />
      Since creating a reasonably realistic HF can be a long session of trial and error you may find it useful to use the builtin scripting capability. John provides a very good set of sample scripts along with the source. A quick glance at one of these, <a href="erosion1.scr">erosion1.scr</a>, shows that multiple commands can be run at a time. This is also possible from the <strong>HF&gt;</strong> prompt, so you can try these commands one at a time to see what effect each has. Once you have a rough guess as the to process you need to create the scene you want, you should place this in a script and then edit the script to get the detail level desired.<br />
      HF-Lab creates its images through the use of lots of mathematical tricks that are far beyond the scope of this column. I'd love to say I understand all of them, but I only have a limited understanding of fractals and their use in creating terrain maps and I have no real understanding of Fast Fourier Transforms or Inverse Fast Fourier Transforms. These latter two are methods of filtering a HF in order to smooth or sharpen features. Filters include a high pass filter (<em>hpfilter</em>), low pass filter (<em>lpfilter</em>), band pass filter (<em>bpfilter</em>) and band reject filter (<em>brfilter</em>). Although I don't understand the math behind them, I was able to use a High Pass Filter to take a simple gforge-created HF and turn it into a very nice heightfield that simulates a <a href="hf1.html#leather">leathery surface</a>. This HF was created in only two steps:
<ol>
<li>gforge 400 2.2</li>
<li>hpfilter 0.095 30</li>
</ol>
So, you can see how powerful this tool can be. Using height fields in BMRT, or as bump maps in POV, can produce some very interesting textures!<br />
      There are many other features of HF-Lab which I have not covered. And in truth, I really didn't give much detail on the features I did discuss. John gives much better descriptions of some of the features in the README file that accompanies the source and I highly recommend you read this file while you experiment with HF-Lab for the first few times. He has gone to great lengths to provide very useful online help and sample scripts. The interface may not be point-and-click, but it certainly is not difficult to learn.<br />
      When I first came across John Beale and HF-Lab I was quite impressed with its ease of use for creating interesting landscapes. I haven't really used it much since the early days of my 3D rendering lifetime, but since writing this article I've rediscovered how powerful this tool can be. Originally I viewed the tool only as a tool for creating landscapes, ie as a tool for modelling a world. Now I see how it can be used to create surface features of all kinds that can be used as textures and not just models. I think I'll be making more use of this tool in the future.</td>
</tr>
</tbody>
</table></td>
</tr>
</tbody>
</table>

<span id="resources"></span>

|                                          |
| ---------------------------------------- |
| ![Resources](../gx/hammel/resources.gif) |

  
The following links are just starting points for finding more
information about computer graphics and multimedia in general for Linux
systems. If you have some application-specific information for me, I'll
add them to my other pages or you can contact the maintainer of some
other web site. I'll consider adding other general references here, but
application or site-specific information needs to go into one of the
following general references and will not be listed here.  

[Linux Graphics
mini-Howto](http://www.csn.net/~mjhammel/linux-graphics-howto.html)  
[Unix Graphics
Utilities](http://www.csn.net/~mjhammel/povray/povray.html)  
[Linux Multimedia Page](http://www.digiserve.com/ar/linux-snd/)

Some of the mailing lists and newsgroups I keep an eye on, where I get
alot of the information for this column:

[The Gimp User and Gimp Developer Mailing
Lists](http://www.XCF.Berkeley.EDU/~gimp/).  
[The IRTC-L discussion list](http://www.irtc.org)  
[comp.graphics.rendering.raytracing](news:comp.graphics.rendering.raytracing)  
[comp.graphics.rendering.renderman](news:comp.graphics.rendering.renderman)  
[comp.graphics.api.opengl](news:comp.graphics.api.opengl)  
[comp.os.linux.announce](news:comp.os.linux.announce)  

<span id="future"></span>

## Future Directions

Next month:

  - *BMRT Part 3: Advanced Topics* or a short tutorial on writing an
    OpenGL application. I'm currently working on a little Motif/OpenGL
    application which I plan on using to create models for use with
    BMRT. I'd like to finish it before I return to BMRT, but I have
    promised the third part on BMRT for July. I'm not sure which I'll
    get to, especially since I also have an article for *Linux Journal*
    due July 1st.
  - ..and who knows what else

  
[Let me know what you'd like to hear about\!](mailto:mjhammel@csn.net)
