#### "Linux Gazette...*making Linux just a little more fun\!*"

-----

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><img src="../gx/bytes.gif" alt="News Bytes" /></td>
<td><h3 id="contents">Contents:</h3>
<ul>
<li><a href="lg_bytes18.html#general">News in General</a></li>
<li><a href="lg_bytes18.html#software">Software Announcements</a></li>
</ul></td>
</tr>
</tbody>
</table>

<span id="general"></span>

-----

### News in General

-----

### ![ ](https://linuxgazette.net/gx/bolt.gif) Atlanta Linux Showcase

Linus Torvalds, the "Kernel-Kid" and creator of Linux, Jon "Maddog"
Hall, Linux/Alpha team leader and inspiring Linux advocate, David
Miller, the mind behind Linux/SPARC, and Phil Hughes, publisher of Linux
Journal, and many more will speak at the upcoming Atlanta Linux
Showcase.

For more information on the Atlanta Linux Showcase and to reserve your
seat today, please visit our web site at
[http://www.ale.org.showcase](http://www.ale.org/showcase/)

-----

### ![ ](https://linuxgazette.net/gx/bolt.gif) Linux Speakers Bureau

SSC is currently putting together a Linux Speaker's Bureau.
<http://www.ssc.com/linux/lsb.html>

The LSB is designed to become a collage of speakers specializing in
Linux. Speakers who specialize in talks ranging from novice to advanced
- technical or business are all welcome. The LSB will become an
important tool for organizers of trade show talks, computer fairs and
general meetings, so if you are interested in speaking at industry
events, make sure to visit the LSB WWW page and register yourself as a
speaker.

We welcome your comments and suggestions.

-----

### ![ ](https://linuxgazette.net/gx/bolt.gif) The Linux System Administrator's Guide (SAG)

The Linux System Administrator's Guide (SAG) is a book on system
administration targeted at novices. Lars Wiraenius has been writing it
for some years, and it shows. He has made an official HTML version,
available at the SAG home page at:  
[http://www.iki.fi/liw/linux/sag](http://www.iki.fi/liw/linux/sag/)

Take a Look\! B

-----

### ![ ](https://linuxgazette.net/gx/bolt.gif) Free CORBA 2 ORB For C++ Available

The Olivetti and Oracle Research Laboratory has made available the first
public release of omniORB (version 2.2.0). We also refer to this version
as omniORB2.

omniORB2 is copyright Olivetti & Oracle Research Laboratory. It is free
software. The programs in omniORB2 are distributed under the GNU General
Public Licence as published by the Free Software Foundation. The
libraries in omniORB2 are distributed under the GNU Library General
Public Licence.

For more information take a look at <http://www.orl.co.il/omniORB>.

Source code and binary distributions are available from
<http://www.orl.co.uk/omniORB/omniORB.html>

-----

### ![ ](https://linuxgazette.net/gx/bolt.gif) The Wurd Project

The Wurd Project, a SGML Word Processor for the UNIX environment (and
hopefully afterwards, Win32 and Mac) is currently looking for developers
that are willing to participate in the project. Check out the site at:
<http://sunsite.unc.edu/paulc/wp>  

Mailing list archives are available, as well as the current source,
documentation, programming tools and various other items can also be
found at the above address.

-----

### ![ ](https://linuxgazette.net/gx/bolt.gif) Linus in Wonderland

Check it out...

Here's the online copy of Metro's article on Linus...  
[http://www.metroactive.com/metro/cover/linux-9719.html](http://www.metroactive.com/papers/metro/05.08.97/cover/linus-9719.html)  

Enjoy\! <span id="software"></span>

-----

### Software Announcements

-----

### ![ ](https://linuxgazette.net/gx/bolt.gif) BlackMail 0.24

Announcing BlackMail 0.24. This is a bug-fix release over the previous
release, which was made public on April 29th.

BlackMail is a mailer proxy that wraps around your existing mailer
(preferrably smail) and provides protection against spammers, mail
forwarding, and the like.

For those of you looking for a proxy, you may want to look into this.
This is a tested product, and works very well. I am interested in
getting this code incorporated into SMAIL, so if you are interested in
doing this task, please feel free.

You can download blackmail from
[ftp://ftp.bitgate.com](ftp://ftp.bitgate.com:/pub/blackmail). You can
also view the web page at
[http://www.bitgate.com](http://www.bitgate.com/spam).

-----

### ![ ](https://linuxgazette.net/gx/bolt.gif) CDE--Common Desktop Environment for Linux

Red Hat Software is proud to announce the arrival of Red Hat's TriTeal
CDE for Linux. Red Hat Software, makers of the award-winning,
technologically advanced Red Hat Linux operating system, and TriTeal
Corporation, the industry leader in CDE technology, teamed up to bring
you this robust, easy to use CDE for your Linux PC.

CDE includes Red Hat's TriTeal CDE for Linux provides users with a
graphical environment to access both local and remote systems. It gives
you icons, pull-down menus, and folders.

Red Hat's TriTeal CDE for Linux is available in two versions. The Client
Edition gives you everything you need to operate a complete licensed
copy of the CDE desktop, incluidng the Motif 1.2.5 shared libraries. The
Developer's Edition allows you to perform all functions of the Client
Edition, and also includes a complete integrated copy of OSF Motif
version 1.2.5, providing a complete development environment with static
and dynamically linked libraries, Motif Window Manager, and sample Motif
Sources.

CDE is an RPM-based product, and will install easily on Red Hat and
other RPM-based Linux systems. We recommend using Red Hat Linux 4.2 to
take full advantage of CDE features. For those who do not have Red Hat
4.2, CDE includes several Linux packages that can be automatically
installed to improve its stability.

Order online at: <http://www.redhat.com> Or call 1-888-REDHAT1 or (919)
572-6500.

-----

### ![ ](https://linuxgazette.net/gx/bolt.gif) TCFS 2.0.1

Announcing the release 2.0.1 of TCFS (Transparent Cryptographic File
System) for Linux. TCFS is a cryptographic filesystem developed here at
Universita' di Salerno (Italy). It operates like NFS but allow users to
use a new flag X to make the files secure (encrypted). Security engine
is based on DES, RC5 and IDEA.

The new release works in Linux kernel space, and may be linked as kernel
module. It is developed to work on Linux 2.0.x kernels.

A mailing-list is available at <tcfs-list@mikonos.dia.unisa.it>.
Documentation is available at <http://mikonos.dia.unisa.it/tcfs>. Here
you can find instructions for installing TCFS and docs on how it works.
Mirror site is available at
[http://www.globenet.it](http://www.globenet.it/~ermmau/tcfs) and
[http://www.inopera.it/\~ermmau.tcfs](http://www.inopera.it/~ermmau/tcfs)

-----

### ![ ](https://linuxgazette.net/gx/bolt.gif) Qddb 1.43p1

Qddb 1.43p1 (patch 1) is now available

Qddb is fast, powerful and flexible database software that runs on UNIX
platforms, including Linux. Some of its features include:

  - Tcl/Tk programming interface
  - Easy to use, you can have a DB application completely up and
  - running in about 5 minutes, using nxqddb.
  - CGI interface for quick and easy online databases/guestbooks/etc...
  - Fast, and powerful searching capability
  - Report generator
  - Barcharts and graphs
  - Mass mailings with Email, letters and postcards

Qddb-1.43p1 is the first patch release to 1.43. This patch fixes a few
minor problems and a searching bug when using cached secondary
searching.

To download the patch file:
<ftp://ftp.hsdi.com/pub/qddb/sources/qddb-1.43p1.patch>  

For more information on Qddb, visit the official Qddb home page:
<http://www.hsdi.com/qddb>

-----

### ![ ](https://linuxgazette.net/gx/bolt.gif) Golgotha

AUSTIN, TX- Crack dot Com, developers of the cult-hit Abuse and the
anticipated 3D action/strategy title Golgotha, recently learned that
Kevin Bowen, aka Fragmaster on irc and Planet Quake, has put up the
first unofficial Golgotha web site.

The new web site can be found at
[http://www.planetquake.com/grags/golgotha](http://www.planetquake.com/frags/golgotha),
and there is a link to the new site at
<http://crack.com/games/golgotha>. Mr. Bowen's web site features new
screenshots and music previously available only on irc.

Golgotha is Crack dot Com's first $1M game and features a careful
marriage of 3D and 2D gameplay in an action/strategy format featuring
new rendering technology, frantic gameplay, and a strong storyline. For
more information on Golgotha, visit Crack dot Com's web site at
<http://crack.com/games/golgotha>.

Crack dot Com is a small game development company located in Austin,
Texas. The corporation was founded in 1996 by Dave Taylor, co-author of
Doom and Quake, and Jonathan Clark, author of Abuse.

-----

### ![ ](https://linuxgazette.net/gx/bolt.gif) ImageMagick-3.8.5-elf.tgz

ImageMagick-3.8.5-elf.tgz is now out.

This version brings together a number of minor changes made to
accomodate PerlMagick and lots of minor bugs fixes including multi-page
TIFF decoding and writing PNG.

ImageMagick (TM), version 3.8.5, is a package for display and
interactive manipulation of images for the X Window System. ImageMagick
performs, also as command line programs, among others these functions:

  - Describe the format and characteristics of an image
  - Convert an image from one format to another
  - Transform an image or sequence of images
  - Read an image from an X server and output it as an image file
  - Animate a sequence of images
  - Combine one or more images to create new images
  - Create a composite image by combining several separate images
  - Segment an image based on the color histogram
  - Retrieve, list, or print files from a remote network site

ImageMagick supports also the Drag-and-Drop protocol form the OffiX
package and many of the more popular image formats including JPEG, MPEG,
PNG, TIFF, Photo CD, etc. Check out:
<ftp://ftp.wizards.dupont.com/pub/ImageMagick/linux>

-----

### ![ ](https://linuxgazette.net/gx/bolt.gif) Slackware 3.2 on CD-ROM

Linux Systems Labs, The Linux Publishing Company is pleased to announce
Slackware 3.2 on CD-ROM This CD contains Slackware 3.2 with 39 security
fixes and patches since the Official Slackware 3.2 release. The CD
mirrors the slackware ftp site as of April 26, 1997. Its a great way to
get started with Linux or update the most popular Linux distribution.

This version contains the 2.0.29 Linux kernel, plus recent versions of
these (and other) software packages:

  - Kernel modules 2.0.29
  - PPP daemon 2.2.0f
  - Dynamic linker (ld.so) 1.8.10
  - GNU CC 2.7.2.1
  - Binutils 2.7.0.9
  - Linux C Library 5.4.23
  - Linux C++ Library 2.7.2.1
  - Termcap 2.0.8
  - Procps 1.01
  - Gpm 1.10
  - SysVinit 2.69
  - Shadow Password Suite 3.3.2 (with Linux patches) Util-linux 2.6

LSL price: $1.95

Ordering Info: <http://www.lsl.com>

-----

### ![ ](https://linuxgazette.net/gx/bolt.gif) mtools

A new release of mtools, a collection of utilities to access MS-DOS
disks from Unix without mounting them.

Mtools can currently be found at the following places:
<http://linux.wauug.org/pub/knaff/mtools>  
<http://www.club.innet.lu/~year3160/mtools>  
<ftp://prep.ai.mit.edu/pub/gnu>  

Mtools-3.6 includes the features such as Msip -e which now only ejects
Zip disks when they are not mounted, Mzip manpage, detection of bad
passwords and more. Most GNU software is packed using the GNU \`gzip'
compression program. Source code is available on most sites distributing
GNU software. For more information write to <gnu@prep.ai.mit.edu>  
or look at: <http://www.gnu.ai.mit.edu/order/ftp.html>

-----

### ![ ](https://linuxgazette.net/gx/bolt.gif) CM3

CM3 version 4.1.1 is now available for Unix and Windows platforms:
SunOS, Solaris, Windows NT/Intel, Windows 95, HP/UX, SGI IRIX, Linux/ELF
on Intel, and Digital Unix on Alpha/AXP. For additional information, or
to download an evaluation copy, contact Critical Mass, Inc. via the
Internet at info@cmass.com or on the World Wide Web at  
<http://www.cmass.com>

**newsBot:**  
Extracts exactly what you want from your news feed. Cuts down on
"noise". Sophisticated search algorithms paired with numerous filters
cut out messages with ALL CAPS, too many $ signs, threads which won't
die, wild cross posts and endless discussions why a Mac is superior to a
Chicken, and why it isn't. newsBot is at:  
<http://www.dsb.com/mkt/newsbot.html>  

**mailBot:**  
Provides itendical functionality but reads mailing lists and e-zines
instead of news groups. Both are aimed at responsible Marketers and
Information managers. The \*do not\* extract email addresses and cannot
be mis-used for bulk mailings. mailBot is at:
[http://www.dsb.com/mkt/mail.bot.html](http://www.dsb.com/mkt/mailbot.html)  

**siteSee:**  
A search engine running on your web server and using the very same
search technology: a very fast implementation of Boyer Moore. siteSee
differs from other search engines in that it does not require creation
and maintenance of large index files. It also becomes an integrated part
of your site design. You have full control over page layout. siteSee is
located at:
[http://www.dsb.com/publish/seitesee.html](http://www.dsb.com/publish/sitesee.html)  

-----

### ![ ](https://linuxgazette.net/gx/bolt.gif) linkCheck

**linkCheck:**  
A hypertext link checker, used to keep your site up to date. Its
client-server implementation allows you to virtually saturate your comms
link without overloading your server. LinkCheck is fast at reading and
parsing HTML files and builds even large deduplicated lists of 10,000 or
more cross links faster than interpreted languages take to load.
linkCheck is at:
[http://www.dsb.com/maintain/linkckeck.html](http://www.dsb.com/maintain/linkcheck.html)  

All products require Linux, SunOS or Solaris. And all are sold as "age
ware": a free trial license allows full testing. When the license
expires, the products "age", forget some of their skills, but they still
retain about 80% of their functionality.

A GUI text editor named "Red" is available for Linux. The editor has a
full graphical interface, supports mouse and key commands, and is easy
to use.

These are some of Red's features that might be interesting:

  - Graphical interface
  - Full mouse and key support
  - 40 step undo (and redo)
  - User-definable key bindings
  - Automatic backup creation
  - Cut/paste exchange with other X Windows applications
  - On-line function list, help and manual

It can be downloaded free in binary form or with full source code.  
[ftp://ftp.cs.su.oz.au/mik/red](ftp://ftp.cs.su.oz.au/mik/red/)  
Also, take a look at the web site at:  
<http://www.cs.su.oz.au/~mik/red-manual/red-main-page.html>  

The web site also includes a full Manual - have a look if you are
interested.

-----

### ![ ](https://linuxgazette.net/gx/bolt.gif) Emacspeak-97++

Announcing Emacspeak-97++ (The Internet PlusPack). Based on InterActive
Accessibility technology, Emacspeak-97++ provides a powerful Internet
ready audio desktop that integrates Internet technologies including Web
surfing and messaging into all aspects of the electronic desktop.

Major Enhancements in this release include:

  - Support for WWW ACSS (Aural Cascading Style Sheets)
  - Audio formatted output for rich text
  - Enhanced support for browsing tables
  - Support for speaking commonly used ISO Latin characters
  - Speech support for the Emacs widget libraries
  - Support for SGML mode
  - Emacspeak now has an automatically generated users manual thanks to
    Jim Van Zandt.

Emacspeak-97++ can be downloaded from:  
<http://cs.cornell.edu/home/raman/emacspeak>  
<ftp://ftp.cs.cornell.edu/pub/raman/emacspeak>  

-----

Published in Linux Gazette Issue 18, May 1997

-----

[![\[ TABLE OF CONTENTS \]](../gx/indexnew.gif)](index.html) [![\[ FRONT
PAGE \]](../gx/homenew.gif)](https://linuxgazette.net/index.html) [![
Back ](../gx/back2.gif)](lg_tips18.html) [![ Next
](../gx/fwd.gif)](lg_answer18.html)

-----

##### This page written and maintained by the Editor of *Linux Gazette*, <gazette@linuxgazette.net>  
Copyright © 1997 Specialized Systems Consultants, Inc.
