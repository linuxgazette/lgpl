
## DOSEMU & MIDI: A User's Report

Dave Phillips, <dlphilp@bright.net>

-----

First, the necessary version info:

  - Linux kernel 2.0.29
  - dosemu 0.66.1
  - Sound Driver 3.5.4

And then there's the hardware:

  - AMD 486/120
  - MediaVision Pro Audio Spectrum 16 (PAS16) soundcard w. MIDI
    interface adapter
  - Music Quest MQX32M MIDI interface
  - two Yamaha TX802 synthesizers

[dosemu](http://www.ednet.ns.ca/~macleajb/dosemu.html) is an MS-DOS
emulator for Linux. The [on-line
manual](http://lexington.lasermoon.co.uk/uncompressed.shtml/texinfo/dosemu_1.html)
describes it as

> "...a user-level program which uses certain special features of the
> Linux kernel and the 80386 processor to run MS-DOS in what we in the
> biz call a DOS box. The DOS box, a combination of hardware and
> software trickery, has these capabilities:
> 
>   - the ability to virtualize all input/output and processor control
>     instructions
>   - the ability to support the word size and addressing modes of the
>     iAPX86 processor family's real mode, while still running within
>     the full protected mode environment
>   - the ability to trap all DOS and BIOS system calls and emulate such
>     calls as are necessary for proper operation and good performance
>   - the ability to simulate a hardware environment over which DOS
>     programs are accustomed to having control.
>   - the ability to provide MS-DOS services through native Linux
>     services; for example, dosemu can provide a virtual hard disk
>     drive which is actually a Linux directory hierarchy.
> 
> The hardware component of the DOS box is the 80386's virtual-8086
> mode, the real mode capability described above. The software component
> is dosemu."

I installed version 0.66.1 because I read that it supported MIDI, and I
was curious to find whether I would be able to run my favorite DOS MIDI
sequencer, Sequencer Plus Gold from [Voyetra](http://www.voyetra.com).
Installation proceeded successfully, and after some initial fumbling
(and a lot of help from the Linux newsgroups), I was running some DOS
programs under Linux.

However, the MIDI implementation eluded me. I followed the directions
given in the dosemu package: they are simple enough, basically setting
up a link to /dev/sequencer. But since Sequencer Plus requires a Voyetra
API driver, I ran into trouble: the VAPI drivers wouldn't load.

I tried to use the VAPIMV (Voyetra API for Media Vision) drivers, but
they complained that MVSOUND.SYS wasn't loaded. These drivers are
specific to the PAS16 soundcard, so I was puzzled that they couldn't
detect MVSOUND.SYS (which was indeed successfully loaded by config.sys).
I also tried using the SAPI drivers, Voyetra's API for the SoundBlaster:
the PAS16 has a SB emulation mode which I had enabled in MVSOUND.SYS,
but those drivers wouldn't load, again complaining that MVSOUND.SYS
wasn't installed. VAPIMQX, the driver for the MQX32M, refused to
recognize any hardware but a true MQX. Checking the Linux sound driver
status with 'cat/dev/sndstat' reported my MQX as installed, but complete
support for the sound driver (OSS/Free) has yet to be added to dosemu.

Since MVSOUND.SYS was indeed installed (I checked it in dosemu using
MSD, the Microsoft Diagnostics program), and since the MIDI interface on
the soundcard was activated, I began to wonder whether that interface
could be used. I tested the DOS MIDI programming environment
[RAVEL](ftp://ftp.cs.pdx.edu/pub/music/ravel), which is "hardwired"
internally to only an MPU-401 MIDI interface: to my surprise and
satisfaction, the soundcard's MIDI interface worked, and I now had a DOS
MIDI program working under Linux.

Following that line of action, I figured that the Voyetra native MPU
driver just might load. I tried VAPIMPU: it failed, saying it couldn't
find the interrupt. I added the command-line flag /IRQ:7 and the driver
loaded. I now had a Voyetra MIDI interface device driver loaded, but
would Sequencer Plus Gold run ?

Not only does Sequencer Plus run, I am also able to use Voyetra's
Sideman D/TX patch editor/librarian for my TX802s. And I can run RAVEL,
adding a wonderful MIDI programming language to my Linux music & sound
arsenal.

All is not perfect: RAVEL suffers the occasional stuck note, and the
timing will burp while running Seq+ in xdos, particularly when the mouse
is moved. The mouse is problematic with Seq+ in xdos anyway, sometimes
locking cursor movement. Since my configuration for the dosemu console
mode doesn't support the mouse, that problem doesn't arise there.
Switching to another console is possible; this is especially useful if
and when dosemu crashes. Also, programs using VGA "high" graphics will
crash, but I must admit that I have barely begun to tweak the video
subsystem for dosemu. It may eventually be possible to run Sound Globs,
Drummer, and perhaps even M/pc, but for now it seems that only the most
straightforward DOS MIDI programs will load and run without major
problems.

And there is a much greater problem: only version 1.26 of the VAPIMPU
driver appears to work properly. A more recent version (1.51) will not
load, even with the address and interrupt specified at the command-line.
However, Rutger Nijlunsing has mentioned that he is working on an
OSS/Free driver for dosemu which would likely permit full use of my MQX
interface card. When that arrives I may be able to utilize advanced
features of Seq+ such as multiport MIDI (for 32 MIDI channels) and SMPTE
time-code.

\[Since writing the above text, I have tweaked /etc/dosemu.conf for
better performance in both X and console modes. Setting **hogthreshold
0**seems to improve playback stability. I have yet to fix the problem
with the mouse in xdos, but it isn't much of a real problem.

Linux is free, dosemu is free, RAVEL is free. My DOS MIDI software can't
be run in a DOS box under Win95 with my hardware: it *can*be done, but
I'd have to buy another soundcard. Linux will run its DOS emulator, with
MIDI and sound support, from an X window or from a virtual console (I
have six to choose from). If I want to run Sequencer Plus in DOS itself,
I have to either drop out of Win95 altogether (DOS mode) or not boot
into Win95 at all. With Win95 I get one or the other; with Linux, I get
the best of all possible worlds.
