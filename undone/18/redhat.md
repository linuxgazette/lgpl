
SSC is expanding Matt Welsh's *Linux Installation & Getting Started* by
adding chapters about each of the major distributions. Each chapter is
being written by a different author in the Linux community. Here's a
sneak preview--the Red Hat chapter by Henry Pierce.--editor

-----

## Red Hat Linux

#### By Henry Pierce, <hmp@boris.infomagic.com>

-----


The Red Hat distribution is an ever-growing and popular commercial
distribution from Red Hat Software, Inc. Even though it is a
"Commercial" distribution under the *Official Red Hat Linux* label
purchased directly from Red Hat Software Inc., it may be downloaded from
the Internet or purchased from third party CD-ROM vendors (see Appendix
B) as well.

Much of Red Hat's growing popularity is due to its Red Hat Package
Management Technology (RPM) which not only simplifies installation, but
software management as well. This in fact, is one of the goals of the
Red Hat distribution: to reduce the system administration burdens of
obtaining, fixing and installing new packages so that Linux may be used
to get some real work done. RPM provides software as discrete and
logical packages. For example, the Emacs editor binary executable file
is bundled together in a single package with the supporting files
required for configuration of the editor and the extension of basic
functionality.

The version of Red Hat described here is version 4.0/4.1, released
October 1996/December 1996. Installation of earlier installations of Red
Hat do differ in their installation procedures than the version
described here. Installation of later versions of Red Hat should be very
similar to the information given here. This document focuses on Intel
based installation of Red Hat Linux. However, many aspects of installing
the Alpha and Sparc versions of Red Hat are similar to Intel Systems
which are out lined here. 

### Getting Started With Red Hat

The process of installing or upgrading Red Hat Linux requires backing up
the existing operating system, obtaining the Red Hat distribution,
planning your installation, preparing the hard disk, making the
appropriate installation diskettes, going through the installation
program and, finally, rebooting your system with the newly installed
operating system. For those who currently have Red Hat Linux 2.0 or
higher installed, you may upgrade by following the same process outlined
here except you should choose "UPGRADE" instead of "INSTALL" when
prompted by the installation program. 
### Obtaining Red Hat Linux

There are only two ways of obtaining the Red Hat Linux Distribution: on
CD-ROM from Red Hat Software, Inc.\\ or other 3rd party CD-ROM
distributor or via FTP from: <ftp://ftp.redhat.com/pub/redhat> or anyone
of the frequently less busy Red Hat mirror sites. No matter how Red Hat
Linux is obtained, you should read the Red Hat Errata which contains a
list of known problems for the release you install. You can obtain the
current errata via: <http://www.redhat.com/errata> or by send email to
<errata@redhat.com>. If you obtained Red Hat Linux from a 3rd party
CD-ROM distributor (such as InfoMagic, Inc.) they often delay releasing
their CD-ROM kit for 2 weeks to a month+ after a major new release of
Linux so they can include the inevitable bug fixes and updates that
follow on the CD-ROM an saving the trouble of downloading them.


### Planning Your Installation

Planning an installation of Linux cannot be understated. The success or
failure of installing or upgrading Linux is directly related to how well
you know your hardware and understand how Linux should be installed on
the target computer. This section outlines basic installation planning
and considers common mistakes and oversights that prevent the successful
installation of Linux. This is also true for people upgrading Red Hat
Linux version 2.0 or higher to version 4.X. In either case, it cannot be
understated that you should **back up** you existing system before going
further. In the rare cases that something should go wrong when you have
not **backed up** your system that results in the loss of an existing
operating system, your data is lost. So if it is worth saving, **back
up** your system before continuing. I now get off my soap box.

#### What Is RPM Anyway?

Before we begin, it is worth taking a moment to discuss Red Hat Package
Management (RPM) Technology as it is the core of installing and
maintaining Red Hat Linux and helps you simplify the planning of
installing and provides Red Hat Linux's ability to upgrade from an older
version of Red Hat Linux to a current one.

Traditionally, software under Linux and Unix system has been distributed
as a series of

    package.tar package.tgz
    package.tar.gz

or

    package.tar.Z

files. They often required the system administrator who installs the
packages to configure the package for the target system, install the
auxiliary and documentation files separately, and setup any
configuration files by hand. And if the package requires another
supporting package that isn't installed, you won't know a package is
missing until you try to use the new package. And the more add-on
packages installed, the harder it is to keep track of them. Then if you
want to remove or upgrade such a package, you have to remember where all
the files for the package are, and remove then. And if you are upgrading
a package, and forgot a pesky configuration file, then the upgraded
package may not work correctly. In summary, the traditional method of
distributing software does provide centralized management system of
installing nor upgrading software packages which is crucial to easing
the administrative burdens of managing the system.

RPM, in contrast, is designed to manage software packages by defining
how a package is built and collecting information about the package and
its installation process the during package's build process. This allows
RPM to create an organized packet of data in the header of a

    package.rpm

that can be added to an organized database that describes where the
package belongs, what supporting packages are required, are the required
packages installed and a means to determine package dependency
information. These are, in fact, describe the design goals of RPM: the
ability to upgrade an individual component or the entire system without
re-installing while preserving the configuration files for the
system/package; be able querying the RPM database to find the location
of files, packages or other relevant package information; to perform
package verification to make sure packages are installed properly or can
be installed at all; to keep source packages "pristine" (provide the
package author's original source with second party patches separate) so
that porting issues can be tracked. Because RPM does this management for
you, you can install, upgrade, or remove a package with a single command
line in text mode or a few clicks of the mouse in the X Window Package
Management Tool. Simple examples of using RPM from the command line are:

    rpm --install package.rpm

\--this will install package

    rpm --upgrade package.rpm

\--this will upgrade package

    rpm --erase package

\--this will remove/erase package

There are many more complicated things RPM can do such as querying a
package to find out if it is installed, what version the package is, or
query an uninstalled package for information. In essence, it does almost
everything a package management tool should do. And Red Hat has GPL'd
this innovative system.

**Anatomy of An RPM Package**

Essentially, RPM works by maintaining a central database of installed
packages, the packages files and its version. A properly built

    package.rpm

has all of the following characteristics: its name will identify the
package, the version of the package, the build revision of the package,
the architecture the package is intended for, and of course the
extension "rpm" to identify it as an rpm based package. Take, for
example,

    bash-1.14.7-1.rpm

. The name, itself, contains a lot of useful information: the package is
"bash", the Bourne Again Shell, it is version 1.14.7 and it is build 1
of the current version for Red Hat, it was built for an Intel or
compatible 386 or higher CPU, and of course, it is in "rpm" format. So,
if you see a package called bash-1.14.7-2.i386.rpm, you know it is a
second build of bash v1.14.7 and probably contains fixes for problems
with build 1 and obviously more current. And while the internal
organization of an \*.rpm is beyond the scope of this discussion, a
properly built package contains an executable file, the configuration
files (if any), the documentation (at least man pages for the package),
any miscellaneous files directory related to the package, and record of
where the packages files should be installed and a record of any
required packages. Upon successful installation of a
\\\<package\\\>.rpm, information about the package is registered in the
RPM database. A more thorough discussion of RPM may be found in the
RPM-HOWTO available from:
<http://www.redhat.com/support/docs/rpm/RPM-HOWTO/RPM-HOWTO.html>


### A Note About Upgrading Red Hat Linux

From the discussion above, you should have the sense that RPM is a
powerful tool, so powerful in fact, that Red Hat Linux is one of the few
Linux and Unix distributions that can truly claim to upgrade from an old
release to a current release. If you are planning to upgrade, you should
know that only upgrades from version 2.0 of Red Hat Linux and onward are
supported due to major changes in Linux's binary format. Otherwise,
upgrades can be performed from the same methods of installation: CD-ROM,
NFS, FTP and a Hard Drive. As of Red Hat Linux v4.0, the upgrade option
is incorporated into the Boot Diskette instead of being a program. For
example, if you upgraded in the past from v2.1 to v3.0.3 and now want to
upgrade to version 4.0, you will need to create the Boot Diskette
(instead of looking for an upgrade script) just like those installing
Red Hat 4.X from scratch. However, it will not reformat you partitions
nor delete your configuration files.

#### Know Your Hardware

Given the scope and variety of hardware, it is not surprising many
people become confused. However, taking a little time to collect the
following information will save much frustration and the time
frustration costs when things don't install or work correctly:

  - Any existing operating systems on the target system and the hard
    drives on which they are installed.
  - Hard drive: interface type; the hard drive settings; the number of
    cylinders, heads, and sectors. The main consideration is whether
    your hard drive uses a SCSI or an IDE interface. If it is SCSI, you
    should know the SCSI ID of the drive for its settings. If it is an
    IDE drive, you should know if the drive(s) are on the primary or
    secondary IDE controller and which drives are set to "master" or
    "slave". The settings are crucial in determining whether LILO (LInux
    LOader) should be used to manage the booting of your operating
    system(s).
  - SCSI adaptor: You should know the make and model. This is useful in
    troubleshooting if you have a supported card that is not detected.
  - Memory: amount of installed RAM. Used to consider the amount of swap
    space.
  - Network Card: You should know the make and model.
  - CD-ROM: If you are installing from CD-ROM, you must know its make
    and model and settings as you would for a hard drive.
  - Mouse: You need to know if you have a PS/2, serial or bus mouse. You
    also need to know what protocol it uses. This is necessary for both
    the character based mouse server and for configuration of the X
    Window System (if you choose to install it).
  - Video Card: If you want to run the X Window System, you must know
    the brand and model of your card to configure the system to run X.
  - Monitor: If your want to run the X Window System, you must know the
    allowable vertical and horizontal synchronization frequencies for X
    to work.

Again, taking the time to list the above information before going
further will save you time and frustration and make the installation
both easier and smoother. If your system didn't come with literature
detailing the above parameters for your hardware, you should consult
with your system vendor or the manufacturer of the equipment. Other
useful information to have if you are going to be on a network are the
TCP/IP networking settings for your system (check with your system
administrator for these if you don't already know them).


### Choosing Your Installation Method

Red Hat Linux may be installed or upgraded via CD-ROM, FTP, NFS or from
an existing Hard Drive partition. Installation nor Upgrading is not
supported from floppy diskettes containing Red Hat packages. Which
supported method chosen depends on your needs, available equipment,
availability of Red Hat Linux and time. For example, if you are a
network administrator that needs to update or install 16 Linux boxes
over the weekend, an NFS install is generally the most prudent way. If
you have a Red Hat CD-ROM for your personal machine, then a CD-ROM
install is order or Hard Drive install if your CD-ROM drive isn't
supported. If you don't have the CD-ROM and simply want to try Red Hat
out and have a couple of hours to spare, then an FTP/Hard Drive install
is a reasonable choice with a 28.8 speed modem or faster connection to
the Internet. No matter which method you choose, the installation of Red
Hat is similar in all cases. To begin, everyone needs to have the
following files available and then create the Installation Floppy Kit
described below to install Red Hat. 

### Creating the Installation Floppy Kit

To create the Installation Floppy Kit, you need to obtain the following:

1.  The Red Hat Boot diskette, boot.img which is available via:
    <ftp://ftp.redhat.com/pub/redhat/current/i386/images/boot.img> or in
    the
    
        $\backslash$images
    
    directory on a properly laid out Red Hat CD-ROM. Obviously, this is
    required for all installation methodologies.

2.  The Red Hat Supplemental Diskette, supp.img, which is available via:
    <ftp://ftp.redhat.com/pub/redhat/current/i386/images/supp.img> or in
    the
    
        $\backslash$images
    
    directory on a properly laid out Red Hat CD-ROM. This diskette is
    required if you are method of install is not CD-ROM based or you
    need PCMCIA support for any devices such as a CD-ROM on the laptop
    to install properly. This diskette can also be used with the Boot
    Diskette for an emergency start disk for an installed system.

3.  The program RAWRITE.EXE which is available via:
    [ftp://ftp.redhat.com/pub/redhat/current/i386/dosutils/rawrite.ext](ftp://ftp.redhat.com/pub/redhat/current/i386/dosutils/rawrite.exe)
    or in the
    
        $\backslash$DOS
    
    directory on a properly laid out Red Hat CD-ROM. This program is run
    from and existing DOS or Windows 95 system to create usable
    diskettes from the boot.img and supp.img described above If you have
    an existing Linux/Unix system, the
    
    ``` 
    dd
    ```
    
    command can be used instead. This is described later in the
    document.

4.  DOS and Windows 95 users installing Red Hat Linux for the first time
    on a machine that will have Linux installed as a second operating
    system should also obtain:
    <ftp://ftp.redhat.com/pub/redhat/dos/fdips11.zip> and unzip into
    
        C:$\backslash$FIPS
    
    if you need to free space on your hard drive. This utility can
    non-destructively shrink and existing DOS 16-bit FAT (Please see
    Using FIPS for compatibility notes). This will achieve will unpack
    into the program files FIPS.EXE and RESTORB.EXE which are to be
    placed on the emergency boot disk made below. Your should also read
    FIPS.DOC (part of the package fips11.zip) for information on using
    FIPS not covered in this document.

5.  Create an Emergency Boot Diskette for an existing operating system
    on the target machine that Linux will be installed on as a second
    operating system must be created. This diskette should contain basic
    tools for trouble shooting. For example, a DOS or Windows 95
    emergency boot diskette should include a copy of FDISK.EXE,
    SCANDISK.EXE (or CHKDSK.EXE), DEFRAG.EXE and RESTORB.EXE as a
    minimum. This diskette is also used to back up an existing partition
    table for those that will use FIPS.EXE to non-destructively shrink
    existing partitions. By backing up the partition table, you can
    restore it with RESTRORB.EXE if the need arises.

#### Creating the Boot and Supplemental Diskettes

A note about creating the Boot and Supplemental Diskettes: If you are
re-formating existing diskettes, DO NOT use

    format /s A:

to format the diskettes, just use

    format A:

. The diskette images need the entire capacity of the diskette and

``` 
/s
```

switch seems to prevent the diskette images from being properly copied
to the floppies. For the emergency diskette below, you will of course
want to use the /s switch.

One blank DOS formatted floppy is needed to create the Boot Diskette and
one blank DOS formatted diskette is needed for the Supplemental
Diskette. This diskette set is used for both installing or upgrading Red
Hat Linux. Starting with Red Hat 4.0, a "one boot diskette fits all"
strategy is employed to install or upgrade Red Hat Linux from the
CD-ROM, FTP, NFS or Hard Drive medium. Other distributions (and older
RHS distributions require you to match a boot image to your hardware,
RHS v4.0 and higher do not). The Boot Diskette is made from the file
"boot.img" and is located in the

    \images

directory on the Red Hat CD-ROM or can be downloaded from:
<ftp://ftp.redhat.com/pub/redhat/current/i386/images/boot.img> or one of
Red Hat's mirror sites. If you are installing to a laptop with PCMCIA
hardware, or from a Hard Drive, NFS or FTP you will need to create the
Supplemental Diskette made from the file "supp.img" which is located in
the

    \images

directory on the Red Hat CD-ROM or can be downloaded from:
[htp://ftp.redhat.com/pub/redhat/current/i386/images/boot.img](ftp://ftp.redhat.com/pub/redhat/current/i386/images/boot.img)  
or one of Red Hat's mirror sites.

The Boot Diskette image contains the bootable kernel and the module
support for most combinations of hardware and the Supplemental Diskette
contains additional tools for non CD-ROM installs. You should make the
Supplemental Diskette even if you are installing from CD-ROM because the
Boot and Supplemental Diskette can be used as an emergency boot system
if something should go wrong with the install or with your system after
it is installed and allow to examine the system.

NOTE: some will notice the size of the boot.img and supp.img being
1.47MB which is larger than 1.44MB. Remember that the unformatted
capacity of a 1.44MB is really 1.47MB and that boot.img and supp.img are
exact byte for byte images of a floppy diskette. They will fit using one
of the tools below:

#### Using RAWRITE to Create the Boot and Supplemental Diskettes

The utility

    RAWRITE.EXE

may be used from DOS, Windows 95 or OS/2 to create the Boot and
Supplemental Diskettes.

    RAWRITE

can be found in the

    \DOSUTILS

directory on the Red Hat CD-ROM or it can be downloaded from:
[ftp://ftp.redhat.com/pub/redhat/current/i386/dosutils/rawrite.ext](ftp://ftp.redhat.com/pub/redhat/current/i386/dosutils/rawrite.exe)
or one of Red Hat's mirror sites. Once you have obtained it, copy

    RAWRITE.EXE

to

    C:\DOS

or

    C:\WINDOWS

directory (or other system directory in the command path) which will
place the

    RAWRITE

utility in your command path. From the CD-ROM (presuming it is the D:
drive or which ever drive and directory you downloaded RAWRITE.EXE to on
the system) to use

    RAWRITE

, copy it to one of your system directories:

    D:\DOSUTILS> copy RAWRITE.EXE C:\WINDOWS

Once rawrite has been copied to a system directory (such as

    C:\DOS

or

    C:\WINDOWS

, change to the images directory on the CD-ROM or to the directory you
copied boot.img and supp.img to and do the following to create the Boot
Diskette:

    C:\> D: 
    D:\> cd \images 
    D:\images> rawrite 
    Enter disk image source file name: boot.img
    Enter target diskette drive: a: 
    Please insert a formatted disk into drive A: and press -Enter-: 

Once rawrite is done creating the Boot Diskette, remove the diskette
from the floppy drive and label it "Red Hat Boot Diskette". Remember,
Red Hat Linux 4.X uses a "one boot disk fits all" strategy so you don't
have to worry about matching a boot image to your hardware as earlier
distributions of Red Hat required.

To create the Supplemental Diskette, follow the instructions above but
substitute "supp.img" for "boot.img". Remember to label this diskette
"Red Hat Supplemental Diskette".

#### Using dd Under Linux or Unix

If you are creating the Boot and Supplemental Diskettes from and
existing Linux or Unix box, make sure it has a 1.44-3.5" floppy
available and you know how your system refers to the floppy device. If
you don't know how the system accesses the floppy device, ask you system
administrator. For Linux, Floppy Drive A: is called /dev/fd0 and Floppy
Drive B: is called /dev/fd1. To create the diskettes under Linux, \`cd\`
to the system directory containing the boot.img and supp.img image
files, insert a blank formatted diskette and type the following enter

    dd if=boot.img of=/dev/fd0

to make the Boot Diskette. Once dd is done, remove the diskette from the
floppy drive, label it "Red Hat Boot Diskette" and set it aside. Then
insert a second formatted diskette and type

    dd if=supp.img
    of=/dev/fd0

. Once dd is done, remove the diskette from the floppy drive, label it
"Red Hat Supplemental Diskette" and set it aside.

#### Creating an Emergency Boot Diskette

If you are installing Linux to a machine that has an existing operating
system, make sure you create an emergency start diskette with useful
diagnostic and recovery tools. Exactly how you want to create such a
diskette various from operating system to operating system. However,
MS-DOS 6.X and Windows 95 will be covered here and should give you some
ideas for other operating systems.

Windows 95 users should press *"Start---Settings---Control---Panel---
Add/Remove Software"* and select the *"Startup Disk"* tab. Insert a
blank, DOS formatted disk and press *"Create Disk"*. When Windows 95 is
done, you will have a boot diskette for Windows 95 containing use tools
such as FDISK.EXE, SCANDISK.EXE and DEFRAG.EXE. Once the diskette is
created, you need to copy

    C:FIPS\RESTORB.EXE

(obtained and unpacked above) to the Windows 95 Boot Diskette you made.
When you are done, remove the diskette and label it "Windows 95
Emergency Boot Diskette and Partition Table Back Up".

MS-DOS 6.X users need to place a blank MS-DOS formatted diskette into
floppy drive A: and do the following to create their emergency boot
diskette:

    C:\> format A:\
    C:\> copy C:\DOS\FDISK.EXE A:\
    C:\> copy C:\DOS\SCANDISK.EXE A:\
    C:\> copy C:\DOS\DEFRAG.EXE A:\
    C:\> copy C:\DOS\SYS.COM A:\
    C:\> copy C:\FIPS\RESTORB.EXE A:\

Once you are done creating the diskette, remove it from the floppy drive
and label it "MS-DOS Emergency Boot disk and Partition Table Back Up".

You are ready to continue\! 

### Setting Up Your Installation Media

Once you have created the Installation Floppy Kit, you should ensure
your installation method is properly setup for using the Red Hat
installation diskettes. For CD-ROM, NFS, FTP and Hard Drive installation
methods, the medium must have the directory

    \RedHat

on the "top level" with the directories

    \base

and

    \RPMS

underneath:

    RedHat 
       |----> \RPMS (contains binary the .rpm s to be installed) 
       |----> \base (contains a base system and files to setting up the hard drive)

CD-ROMs will, of course have additional directories but the key
directories needed for the installation are

    \RedHat

on the top level of the CD-ROM with

    \base

and

    \RPMS

underneath on third party CD-ROMs. Obviously, Red Hat Software will
ensure their *Official Red Hat Linux* CD-ROM will have the proper
directory structure. So, if you are installing from CD-ROM, you may go
to Preparing Your System for Installation. For the other types of
installs, read the section appropriate section for your installation
medium:

#### Setting Up for an NFS Installation

For NFS installs, you will either need a Red Hat CD-ROM on a machine
(such as an existing Linux box) that can support and export an
**ISO-9660 file system with Rockridge Extensions** or you need to mirror
one of the Red Hat distribution with the directory tree organized as
indicated above. And of course the proper files in each directory. The
directory

    \RedHat

then needs to be exported to the appropriate machines on the network
that are to have Red Hat Linux installed or upgraded. This machine must
be on a Ethernet, you can not do an NFS install via dialup link.

#### Setting Up For a Hard Drive Installation

Hard Drive installs need to have the

    \RedHat

directory created relative to the root directory of the partition (it
doesn't matter which partition) that will contain the Red Hat
distribution obtained either from CD-ROM or an FTP site. For example, on
the primary DOS partition the path to

    \RedHat

should be

    C:\RedHat

. On a DOS 16-bit FAT file system, it does not matter that the

    package.rpm

names get truncated. All you need to do is make sure

    \RedHat\base

contains the base files from a CD-ROM or FTP site and

    \RedHat\RPMS

contain all the

    package.rpm

files from the CD-ROM or FTP site. The you can install or upgrade from
that partition. If you have an existing Linux partition not needed for
an installation or upgrade, you can set it up as outlined here as well
and use it.

TIP: NFS and Hard Drive installs can provide more flexibility in the
packages available to install. NFS and Hard Drive installs/upgrades
implied that you can be selective about which packages are placed in the
RPMS directory. For example, if you only want a text based system, then
the X-based packages may be excluded. Also, if there are updates for the
Red Hat system you wish to install, they may be placed in the RPMS
directory in place of the distributions original packages. The only
caveat for customizing the available packages for installing or
upgrading Red Hat Linux is that package dependencies are meet. That is,
if package A needs package B to be installed, both packages must be
present to meet the interdependencies. This may, however, take a little
experimenting to ensure all package dependencies are met. For more
information, please see "Customizing Your NFS or Hard Drive
Installation" below.

#### FTP Installations

For FTP installs over the Internet, all you need is the IP address of
your nearest FTP server and the root directory path for the Red Hat
Linux system you wish to install. If you don't know the nearest FTP
site, consult with your system administrator or your ISP. If you are
intending to do an FTP install over a low band width connection (defined
as anything slow than a 128K ISDN link), it is highly recommend that you
FTP the file files to a hard drive with an existing DOS partition and
then do the hard drive install install described in this chapter. The
total size of the binary packages available in the

    /RedHat/RPMS

directory is currently around 170MB which will take many hours to
install. If anything goes wrong with the installation such as the link
goes down, you will have to start over again. If you ftp the files
first, setup your hard drive for installing Linux, it is then less work
and less flustering to recover from a failed install. You don't even
have to download all the files in

    /RedHat/RPMS

to successfully install a minimal system that can grow with your needs.
Please see Customizing Your NFS or Hard Drive Installation for details.

#### Customizing Your NFS or Hard Drive Installation

One of the interesting things you can do with Red Hat Linux is customize
the install process. However, this is not for the faint of heart. Only
those already familiar with Red Linux or Linux in general should attempt
customizing the install. As of Red Hat v4.X, the

    /RedHat/RPMS

directory contains approximately 170MB of rpm files. RPM does compress
these packages and can assume the package will need and average 2-3MB of
hard drive space for every 1MB of

    package .rpm

available for installation. For example, if the

    package .rpm

is 6MB in size, you will need between 12 to 18MB of free space to
install the package. If you know what software you want and don't want,
much of the software provided will not have value for the installation,
and for for low band width connects, it is not feasible to download the
entire tree. With this in mind, an installation can be customized to
remove unwanted software.

Customizing the packages to install is an advantage and possible for the
following types of installs: FTP, NFS and Hard Drive methods. CD-ROM
cannot be written to (but you can copy the files to the hard drive and
do a hard drive install with the customized package list). FTP and NFS
installs can only be designed if you have administrator access to the
server(s) on your network or your system administrator is willing to
work with you. The following installation situations make customizing
the installation desirable: Obtaining Red Hat Linux via FTP over a low
band width connection or designing a suite of software to be used by all
installation of a network of Red Hat Linux boxes.

To customize the installation, you must obtain the

    /base/comps

file which will provide you with the list of packages the a full install
would normally have. Then then packages you actually want to install
from

    /base/comps

need be download. Then, the

    /base/comps

needs to be edited to reflect the packages you obtained and are going to
install. (NOTE: if you have local package.rpms you can add them to the
comps file as well).

#### Understanding the COMPS file

The Red Hat installation program uses the file

    /RedHat/base/comps

(the file here is an example from RHS v4.0) to determine what packages
are available in the

    /RedHat/RPMS

directory for each category to be installed. The file is organized by
category and each category contains a list of packages Red Hat believes
are the minimum required for that section. NOTE: only the

    package

part of a packages name

    package-version-build.rpm

is listed in the file. This means the comps file is generally usable
from one version of Red Hat to the next. A section in this file has the
structure:

    number category 
    package 
    ... 
    end 

That is a tag to identify the category number, the category, a list of
the package names in the category and the tag "end" to mark the end of
the category.

Without exception, everyone needs the all of the software packages
listed in the **Base** section of the file. The other sections, though,
generally can be customized or eliminated to suit a particular need. For
example, there are three types of **Networked Stations**: "plain",
Management, and Dialup. An examination of these sections shows that many
of the software packages are listed in all three categories, but some
software packages are specific to the category. If you are creating a
**Dialup Networked Station**, then you can safely eliminate the "Plain"
and "Management" sections and any software unique to those categories.
Conversely, if you only need basic networking capability for a networked
work stations, the other sections can be eliminated from the file as
well as the unique software to each of those sections. All you need do
is make sure you have the all the software packages listed in that
category. If you have some local custom packages (those not provided by
Red Hat Software), you should add them to an existing category that is
appropriate rather than creating a new category.

Because the list of packages in each category only contains the name of
the package, i.e., not the entire

    package-name-version-build.rpm

, you can substitute any updates Red Hat has made available in the

    updates

directory on: <ftp://ftp.redhat.com/pub/redhat/current/updates>  
or one of Red Hat's mirror sites for the original package found in the
distribution's original

    /RedHat/RPMS

directory. The means installation program is relatively version
insensitive. The only caveats here are that package dependencies are met
. When an rpm'd package is built, RPM itself tries to determine what
packages must be installed for another package to work (the rpm
developer also has direct control of this as well---he can add
dependencies that rpm might not ordinarily detect). This is where a
little experimentation, or research may be needed. For example, one way
to determine package dependencies (if you have user access to your NFS
server on an existing Red Hat Linux Box) is to telnet or login into it
or if you have the CD-ROM, mount it and cd to the

    RedHat/RPMS

directory and query the packages for its dependencies:

    [root@happy RPMS] rpm -q -p -R bash-1.14.7-1.i386.rpm 
    libc.so.5 
    libtermcap.so.2 

The "-q" puts RPM in query mode, the "-p" tells RPM to query an
uninstalled package and the "-R" tells RPM to list the target package's
dependencies required. In this example, we see libc.so.5 and
libtermcap.so.2 are required. Since libc and termcap are part of the
base of required software (as is bash really), you must insure the libc
and libtermcap packages (the dependency packages) are present to be able
to install bash (the target). Overall, as long as you get the entire
**base** packages installed, you will be able to boot the system when
the Installation Program completes. This means you can add additional
packages to Red Hat as required even if the Installation Program reports
a package failed to install because dependencies were not met. The
following table describes the categories of software are found in

    /base/compsin

of Red Hat v4.0:

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>RPM Category</strong></td>
<td><strong>Required?</strong></td>
<td><strong>Comments</strong><br />
</td>
</tr>
<tr class="even">
<td>BASE</td>
<td>Yes</td>
<td>Should not be customized.<br />
</td>
</tr>
<tr class="odd">
<td>C Development</td>
<td>Highly Recommend</td>
<td>Need the minimal system to compile a kernel<br />
</td>
</tr>
<tr class="even">
<td>Development Libs</td>
<td>Highly Recommend</td>
<td>Need the minimal system to compile a kernel<br />
</td>
</tr>
<tr class="odd">
<td>C++ Development</td>
<td>Optional</td>
<td>C++ Development<br />
</td>
</tr>
<tr class="even">
<td>Networked Workstation</td>
<td>Recommend; Required &amp; Whether you are on an Ethernet or for other network software</td>
<td>going to dialup networking, you need to install this package suite You shouldn't customize this.<br />
</td>
</tr>
<tr class="odd">
<td>Anonymous FTP/Gopher Server</td>
<td>Optional</td>
<td>If your Linux box is going to serve files via FTP or Gopher<br />
</td>
</tr>
<tr class="even">
<td>Web Server</td>
<td>Optional</td>
<td>Useful for Web Developers for local development, required if you serve web pages.<br />
</td>
</tr>
<tr class="odd">
<td>Network Management Workstation</td>
<td>Optional</td>
<td>Has additional tools useful for dialup as well as Ethernet network<br />
</td>
</tr>
<tr class="even">
<td>Dialup Workstation</td>
<td>Recommended</td>
<td>Required if you are going to dialup<br />
</td>
</tr>
<tr class="odd">
<td>Game Machine</td>
<td>Optional</td>
<td>Need I say more? Fortunes are required for humor :-)<br />
</td>
</tr>
<tr class="even">
<td>Multimedia Machine</td>
<td>Optional</td>
<td>If you have supported hardware<br />
</td>
</tr>
<tr class="odd">
<td>X Window System</td>
<td>Optional</td>
<td>If you want to run X<br />
</td>
</tr>
<tr class="even">
<td>X Multimedia Support</td>
<td>Optional</td>
<td>If you have supported hardware<br />
</td>
</tr>
<tr class="odd">
<td>TeX Document Formatting</td>
<td>Optional</td>
<td>Customize as needed<br />
</td>
</tr>
<tr class="even">
<td>Emacs</td>
<td>Recommend</td>
<td>The One True Editing Environment<br />
</td>
</tr>
<tr class="odd">
<td>Emacs with X</td>
<td>Recommend</td>
<td>Requires X<br />
</td>
</tr>
<tr class="even">
<td>DOS/Windows Connectivity</td>
<td>Optional</td>
<td>Huh?<br />
</td>
</tr>
<tr class="odd">
<td>Extra Documentation</td>
<td>Required</td>
<td>Man pages and should ALWAYS be installed. Other packages optional.<br />
</td>
</tr>
</tbody>
</table>


### Recommendations

It is difficult to determine exactly what any one installation will
require. However, someone installing via FTP should get the **Base**
system and the **Dialup Networked Station** and install these. Then
additional software can be obtained and added as the need arises. Of
course if you want to do C programming, you should get the relevant
packages and edit the comps file appropriate.

One last caveat: If you encounter a file during the install that
requires another package you don't have available, or you make a mistake
in the comps file, you can generally finish the install and have a
bootable working system. You can correct the problem by manually adding
the failed packages and their dependencies later. Overall, get the
entire Base system and a Networked Station packages installed and you
can add anything you need or want later.

#### Preparing Your System to Install

Before continuing, if you have an existing operating system, and have
not yet backed up your data, you must back it up now. While most of the
time installing Linux will not result in the loss of data, the
possibility exists, and the only way to guarantee a recovery in such a
catastrophic event is to **back up your data**.

At this point with the information collected above and having decided on
an installation method above, preparing your system should offer no
obstacles. Essentially, you need to make sure you have free and
unpartitioned space on one the system's hard drives. (NOTE: there is a
file system type known as UMSDOS that some distributions use as an
optional way to install Linux onto an existing DOS file system; Red Hat
Linux does not support this type of installation.) If you are installing
on a system that will only have Linux and does not currently have an
operating system installed, then you are set to partition your hard
drive and can go to the next section. If you have an existing operating
system, such as DOS/Windows 3.1, Windows 95, OS/2 or another operating
system, then things are a bit more complex. The following should help
determine what you need to do to free hard drive space:

  - DOS or Windows 95 using DOS 16-bit FAT: You may use utility FIPS.EXE
    that is part of the Installation Floppy Kit described above that
    will allow you to non-destructively make a single DOS 16-bit file
    allocation table (FAT) into two or more DOS 16-bit FATs. These new,
    empty partitions can be deleted, creating free space to be used for
    Linux partitions. See FIPS.EXE below. If you have a CD-ROM
    containing Red Hat, there should be a directory called
    
        \dosutils
    
    containing a copy of FIPS.EXE. Otherwise, the FIPS package can be
    downloaded from: <ftp://ftp.redhat.com/pub/redhat/dos/fips11.zip>  
    or one of Red Hat's mirror sites.
    
    NOTE: Microsoft has introduced a new 32-bit FAT system with recent
    Windows 95 releases. This 32-bit FAT system cannot be shrunk by the
    current version of FIPS.EXE. In Windows 95, if you check under *My
    Computer* | *Control Panel* | *System* and your Windows 95 kernel
    version ends in a "B", Windows 95 is likely to be using a 32-bit
    FAT.

  - OS/2, Windows NT, DOS 32-bit FAT and Other Users: You will need to
    either back up existing partitions and delete them, or if using a
    single partition, delete the partition and re-install the operating
    system into a smaller partition, leaving free space to be used for
    Linux partitions.

#### Planning to Partitioning The Hard Drive

Linux has its own version of the program

    fdisk

used to create native Linux and swap partitions. However, the details of
its use are described later in this guide. However, discussion of the
concepts on how to partition your hard drive are important now so
reasonable decisions can be made on how much and how to make free space
available on the target system.

One way of installing Linux is to use two partitions---one for the
operating system and one for the swap file in the free space on your
hard disk. However, this is not an ideal way for Linux to be installed.
While some hardware configurations may only allow this type of
organization, the recommend method is to use a minimum four partitions
for Linux: One for

``` 
/
```

(the "root" partition), one for

    /var

, one for

    /home

and one for swap. Unlike logical DOS drives which are assigned a drive
letter, Linux partitions are "glued" together into one virtual directory
tree. This scheme takes advantage of how Linux operates in the real
world. Essentially, each file system reflects the life time of a file:
the files on the

``` 
/
```

partition have the longest "time to live" because they are infrequently
updated and often last as long as the operating system itself does on
the hardware; The

    /home

partition represents medium file life times that can be measured in
weeks or days, such as user documents;

    /var

represents files with the shortest life time (such as log files),
measured in minutes or even seconds. This type of setup also suggests a
backup strategy: the root file system only needs to be backed up when a
new program is added or configuration files are changed. The

    /home

partition can be put on some sensible full/incremental back up schedule
while /var never needs to be backed up, with the exception of

    /var/spool/mail

. A more through discussion of this can be found in Kristian Koehntopp's
Partition mini-HOWTO and Stein Gjoen's Multiple Disks Layout mini-HOWTO.

A PC can have either have a maximum of four primary partitions or have
three primary partitions and 1 extended which can contain many "logical"
drives. One model in which to understand this are Russian Stacking
Dolls. Basically, Russian Stacking Dolls are containers within
containers but each container is a discrete doll. A partition is a
mechanism describing a container within the master container of the hard
drive that an operating system does not leave the confines of. A normal
PC hard drive can have up to four primary containers (Primary
Partitions) or three primary containers and one extended container
(Extended Partition) that contains Logical containers (Logical
Drives/Partitions). This means you can have one primary partition for
DOS/Windows, one primary partition for the root file system, one primary
partition for a swap partition, and one Extended partition containing
logical drives for

    /var

and one logical drive for

    /home

(as well as other "optionally" defined partitions). However, Linux can
and it is often prudent to have more than the partitions outlined here.
Due to some design limitations in typical PCs BIOS, there are
limitations on how partitions can be setup and still be boot partitions.

Overall, IBM designers didn't think that a PC would ever have 1 GIG
drives 15 years ago when the PC was originally designed. As a result, a
PC BIOS is limited to a 10-bit address for describing the initial
geometry of a hard drive. This happens to correspond to one of the
values used in calculating the location of a piece of data on a hard
disk known as cylinders. A 10-bit number is sufficient to describe the
numbers 0 through 1023 in decimal notation. A drive with 1024 cylinders,
16 heads and 63 sectors per track, is approximately 504MB. This is
important for 2 primary reasons: Most boot loaders have to depend on
BIOS to get a drives initial geometry for calculating the beginning of a
partition and the average drive size on the market these days is 1.2 GIG
which contain 2,000+ cylinders. Luckily, most newer system (usually
those with a BIOS designed in 1994 or later) have a BIOS that supports
Logical Block Addressing (LBA). LBA mode is a means of supporting Large
Hard Drives by 1/2 or 1/4 the number of cylinders and doubling (or
quadrupling) the number of heads. This allows for the proper calculation
of drive geometry while working within the constraints of BIOS. So a
drive with 2048 cylinders, 16 heads and 63 sectors per tract will, under
LBA mode, have 1024 cylinders, 32 heads, and 63 sectors per tract. Now,
we can potentially use any primary partition as a boot partition.

Now, with all this theory and practical advice, it is time to provide
some example of how this can be put together; the first example is an
850MB drive with LBA mode enabled which might be divided:

    Partition   File System Type    Use Size
    /dev/hda1   MS-DOS  DOS/Win95   400MB 
    /dev/hda2   Linux Native (ext2) /   325MB 
    /dev/hda3   Linux Swap  Swap    32MB 
    /dev/hda4   Extended    N/A 93MB 
    /dev/hda5   Linux Native (ext2) /var    40MB 
    /dev/hda6   Linux Native (ext2) /home   53MB 

This table might be useful for a machine used by a single person. There
a couple of things to note here. First, the labeling of partitions by
Linux. The

    /dev

is the Linux directory where "device files" are kept (this is different
than a device driver but it is related to device drivers) that Linux
uses to identify devices by user programs. The next part,

    hda

, means "hard disk **A**" used to designate "Fixed Disk 1" as it is
called under DOS. But it also means that the drive is an IDE drive. SCSI
drives would use

    sda

for "SCSI Disk **A**. The whole line

    /dev/hda1

means the 1st partition on hard disk **A**. As for the sizes that are
being used here, they are a little arbitrary, but fall under the
following guidelines: A virtual decision was made to use half of the
drive for DOS or Windows 95 and roughly half for Linux. So, 400MB was
allocated for DOS and it is presumed that is enough for those needs. The

``` 
/
```

root file system is 325MB which is enough for the base Linux system
(usually about 50MB), programming tools such as C, C++, perl, python and
editors such as vi and EMACS as well as the X Window System and some
additional space for extra useful packages you might find in the future.
If you do not plan to run X, you can subtract 150MB from this total. The
swap partition is determined by multiplying 2x physical ram installed on
our virtual machine (which has 16MB of core RAM installed). If you are
tight on space or have less than 16MB of ram, you should have at least a
16MB swap partition. However, you must have a swap partition defined.
40MB is used for

    /var

which includes enough space for log files and email handling for one or
two people. and 53MB for

    /home

provides plenty of space for a user or two to work in.

#### How Much Space Do You Really Need?

By now, an installation method has been chosen and a view of what
partitioning for Linux has been discussed. But how much space do I
really need? The answer is: "It depends." To make a decision on how much
space is needed, This a the goal(s) of why you are installing Linux must
be reviewed because it has a direct bearing on the space needed to meet
these goal(s). If you install everything, you will need about 550MB for
all the binary packages and supporting files. This does not include swap
space or space for your own files. When these are factored in, a minimum
of 650MB or more is needed. If your goal is more modest such as having a
text only system with the C compiler, the kernel source tree, EMACS,
basic Internet dialup support, then 125 to 150MB of hard drive space is
sufficient. If your plans are more demanding such as having a web
development platform and X then 450MB or so described in the model above
should be enough. If you are planning to start and ISP or commercial web
site, then 2 or more GIGs of hard drive space may be needed depending on
the scope of services being offered. The overall rule of thumb is having
to much real estate is a good thing, not having enough is bad. To help
you decide how much space is enough, here are some basic formulas/values
for different needs:

<table>
<colgroup>
<col style="width: 33%" />
<col style="width: 33%" />
<col style="width: 33%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Use of Partition</strong></td>
<td><strong>Recommend</strong></td>
<td><strong>Size Comments</strong><br />
</td>
</tr>
<tr class="even">
<td>Swap</td>
<td>2 x Physical RAM</td>
<td>If less than 16MB of RAM installed, 16MB is a must. If space is tight, and 16MB RAM installed, 1 x Physical RAM is the minimum recommended.<br />
</td>
</tr>
<tr class="odd">
<td>Root system, no X</td>
<td>100 - 200MB</td>
<td>Depends on tools such as compilers, etc. needed<br />
</td>
</tr>
<tr class="even">
<td>Root system, with X &amp; 250-350MB</td>
<td>Depends on tools such as compilers, etc., needed<br />
</td>
<td></td>
</tr>
<tr class="odd">
<td>/home</td>
<td>5 - Infinite MB</td>
<td>Depends on being single or multiple users and needs<br />
</td>
</tr>
<tr class="even">
<td>/var</td>
<td>5 - Infinite</td>
<td>Depends on news feeds, # of users, etc.<br />
</td>
</tr>
<tr class="odd">
<td>/usr/local</td>
<td>25 - 200MB</td>
<td>Used for programs not in RPM format or to be kept separate from the rest of Red Hat<br />
</td>
</tr>
</tbody>
</table>

<span id="fips"></span>

### Using FIPS

Many people installing Linux have one hard drive with a single DOS or
Windows 95 partition already using the entire hard drive, or they may
have two drives with 1 DOS or Windows 95 partition per drive. FIPS is a
utility that can non-destructively shrink a 16-bit DOS FAT in use by DOS
3.X or higher and many implementations of Windows 95. (NOTE: if you are
using revision "B" of the Windows 95 kernel, you may be using FAT32
which FIPS currently cannot shrink.) If you meet the requirements above,
then you can shrink an existing primary partition on any drive. NOTE:
FIPS cannot shrink logical drives or extended partitions. If you have
Red Hat on CD-ROM, the utility should be in the

    \dosutils

directory on the CD-ROM. If you have downloaded Red Hat Linux, you
should also download FIPS package available from:
<ftp://ftp.redhat.com/pub/redhat/dos/fips11.zip>  
or one of the many Red Hat's mirror sites. You should also read FIPS.DOC
included with this package for details on FIPS operation.

A few caveats about using FIPS: As a reminder, you should **back up**
your existing data before using it. While it is rare for FIPS to damage
a partition, it can happen, and backing up your data is the only way to
recover from such a catastrophe. FIPS can only be used on primary DOS
16-bit FAT partitions. It cannot be used on any other types of
partitions, nor can FIPS be used on Extended partitions or Logical
drives. It can only split primary partitions. Before running FIPS, you
must run SCANDISK to make sure any problems with your partition are
fixed. Then you must run DEFRAG to place all the used space at the
beginning of the drive and all the free space at the end of the drive.
FIPS will split an existing primary partition into to two primary DOS
16-bit FAT partitions: One containing your original installation of
DOS/Windows 95, and one empty, unformatted DOS 16-bit DAT partition that
needs to be deleted using the DOS or Windows 95 *fdisk* program. The
following steps outline how to use FIPS.EXE:

1.  Copy
    
        FIPS.EXE
    
    to
    
        C:\WINDOWS
    
    or
    
        C:\DOS
    
    . This will place
    
        FIPS.EXE
    
    in your command path.

2.  Create or use the bootable DOS or Windows 95 emergency disk
    described in the Installation Floppy kit above and place the program
    
        RESTORB.EXE
    
    on the disk if you have not already done so. FIPS gives you the
    ability to back up your existing partition table, allowing you to
    return your system to its previous state using
    
        RESTORB.EXE
    
    .

3.  Run
    
        scandisk
    
    and
    
        defrag
    
    (included with DOS 6.X and higher). This makes sure there are no
    errors on your hard drive and places all the free space at the end
    of the drive.

4.  Make sure you are in DOS mode (i.e., not running Windows 3.X or
    Windows 95).

5.  Type
    
        fips
    
    . An introductory message will appear and you will be prompted for
    which hard drive on which to operate (if you have more than 1). Most
    people will choose "1" for the first hard disk to shrink.

6.  After confirming that you wish to continue, you will be asked to
    make a backup copy of your existing boot and root sectors on the
    bootable disk made above. This will allow you to restore the hard
    drive if needed.

7.  FIPS will ask if all the free space on your existing partition
    should be used to create a second partition, with an initial
    partition table if you accept the defaults. If this isn't
    acceptable, say "no" and then use the up and down arrow keys to
    adjust the amount of space used for the second partition. Once you
    are happy with the division, hit Enter to stop editing. If the sizes
    with the new partition table are acceptable, choose "c" to continue.
    If not, choose "r" to re-edit the table.

8.  One last chance is given to quit FIPS without making changes or
    writing out the new partition table. If you are happy, write it
    out\!

9.  Once FIPS is done, re-boot your computer to have FIPS changes take
    effect.

10. Next, use DOS's
    
        fdisk
    
    to delete the second DOS partition. This will leave unallocated
    space to be used by Linux's version of f:disk later to create Linux
    native and Linux swap partitions.

With the appropriate things done in this section for installing Linux,
you are now ready to Install Red Hat Linux\! <span id="install"></span>

### Installing Red Hat Linux

By now, you should have created an Installation Floppy Kit, Prepared
Your Hard Drive, and Have your Installation Media ready. for the
install. The details of the installation follow, however, you first
begin by booting your system and configuring the install program to
install from your selected medium. Once this is done, the installation
proceeds with the same steps for each everyone one after that. At this
point, you need to begin by booting your computer with the diskette
labeled "Boot Diskette".

### Using Your Installation Media

As the boot diskette starts up, the kernel will attempt to detect any
hardware which the boot diskette has drivers compiled directly in to it.
Once booting is complete, a message asking if you have a color screen
appears (if you do, select OK). Next comes the Red Hat Introduction
Screen welcoming you to Red Hat Linux. Choose OK to continue. The next
questions asks if you need PCMCIA support which you need to say yes to
if you are installing to a laptop; say yes and insert the Supplemental
Diskette when prompted. Once PCMCIA support is enabled (if needed), you
will be presented with a screen asking what type of installation method
you will be using. Follow the instructions for the installation method
you've chosen described in the following sections.

#### Installing From CD-ROM

If installing from CD-ROM, you should choose "Local CD-ROM" by
highlighting it from the list of installation types. Once you choose
"Local CD-ROM" and click "OK", you will be asked if you have a SCSI,
IDE/ATAPI or Proprietary CD-ROM that you wish to install from. This is
where some of the hardware research pays off: if you have a recently
made 4X or faster CD-ROM drive that was bundled with a Sound Blaster or
other sound card, you most likely have an IDE/ATAPI type drive. This is
one of the most confusing issues facing you.

If you choose SCSI, you will be asked what kind of SCSI card and be
presented a list. Scroll down the list until you find your SCSI card.
Once you have choose it, you will be asked if you wish to AUTOPROBE for
it or SPECIFY OPTIONS. Most people should choose "AUTOPROBE" which will
cause the setup to scan for your SCSI card and enable the SCSI support
for you card when found

Once the Installation Program has successfully located the Red Hat
CD-ROM, you should proceed to "Walking Through the Rest of the
Installation."

#### Installing From The Hard Drive

If you are installing from a hard drive, then highlight this option and
choose "OK". If you have not already choose PCMCIA support, you be
prompted to insert the Supplemental Diskette.

#### Installing via NFS

If you are installing via NFS, then highlight this option and choose
"OK". You will next be asked to choose which Ethernet card you have
installed on the target machine so the Installation Program may load the
correct Ethernet driver. Highlight the appropriate card from the list
and then select "OK" allowing the Installation Program to AUTOPROBE for
you card. However, if you machine hangs, you will need to do

    Ctrl-\Alt-Delete

to reboot the system. Most of the time, when this happens, it is because
the probing "touches" a non Ethernet card. If this should happen, try
again and choose "SPECIFY OPTIONS" and give data about your card in the
form of:

    ether=IRQ,IO\_PORT,eth0

This will instruct the probe to look at the location specified by the
values

    IRQ

and

    IO\_PORT

for an Ethernet card. For example, if you Ethernet card is configured
for IRQ 11 and IO\\\_PORT 0x300, you would specify:

    ether=11,0x300,eth0

Once your card has been successfully found, you will be prompted for
TCP/IP information about your machine and the NFS server with the Linux
installation. First, you will be asked to provide the target machines
*IP Address, Netmask, Default Gateway*, and *Primary Name Server*. For
example:

    IP Address:          192.113.181.21
    Netmask:             255.255.255.0
    Default Gateway:     192.113.181.1
    Primary Nameserver:  192.113.181.2

Once you press OK, you will prompted for the target machines
*Domainname* and *Hostname*. For example, if you domain name is
infomagic.com and hostname is vador, enter:

    Domainname:               infomagic.com
    Host name:                vador.infomagic.com
    Secondary nameserver IP:  Enter if needed
    Tertiary nameserver IP:   Enter if needed

The last screen will prompt you for the NFS server and the exported
directory containing the Red Hat distribution. For example, if you NFS
server is redhat.infomagic.com, enter:

    NFS Server name:    redhat.infomagic.com
    Red Hat Directory:  /pub/mirrors/linux/RedHat

Again, if you do not know these values, check with you system
administrator. Once you have entered the values, choose "OK" to
continue. If the Installation program reports and error locating the Red
Hat distribution, make sure you have the correct values filled in above
and that your network administrator has given the above target machine
information export permission.

#### Installing via FTP

An FTP install is very similar to the NFS install outlined above. You
will be prompted for the Ethernet card and your machines TCP/IP
information. However, you will be asked for the *FTP site name* and *Red
Hat directory* on the Red Hat mirror site. instead of NFS server
information. There is one caveat about performing an FTP install: find
the closest and least busy FTP site to your location. If you don't know
how to do this, check with your network administrator.

TIP: If your hardware isn't detected, you may need to provide an
override for the hardware to be enabled it properly. You may also want
to check: <http://www.redhat.com/pub/redhat/updates/images>  
to see if Red Hat has updated boot diskettes for your hardware.

### Walking Through the rest of the Installation

1.  Next, you will be asked if you are installing to a New System or
    Upgrading RedHat 2.0 or higher. If you are upgrading, you will not
    be offered the chance to partition your hard drive or configure
    anything with your system except LILO. Press either INSTALL or
    UPGRADE to continue.

2.  If you are upgrading, you will be asked for the root partition of
    your existing Red Hat system. Highlight the appropriate partition of
    your existing Red Hat system and Press "OK". If you are installing
    for the first time, you will need to partition your hard disk with
    free space determined above. The following discussion is an example
    based on Planning to Partition the Hard Drive. If you do not have
    any free space on your hard disk to create partitions and are using
    a 16-bit FAT such as that used by DOS or most Windows 95
    installations, please review the Using FIPS section of this
    document. To use fdisk, highlight the disk you wish to partition
    from the list presented to you by the Installation Program. You will
    be dropped from the "graphic" screen and presented with a black and
    white screen with the following prompt:
    
        Command (m for help):
    
    This rather mysterious prompt is Linux's fdisk's command prompt. If
    you press \`m\`, you will get a list of commands with a short
    definition of what each does. However, the most useful one to start
    with is "p". This will print your existing partition on the screen.
    If you have existing partition(s) on the drive they will be
    displayed. Make sure you can create at least one 25-50MB partition
    that starts before cylinder 1024 and ends on or before cylinder 1023
    as this type of locations is required by LILO to be able to boot the
    root partition which will in turn allow the kernel to take over you
    system which is not restricted in the way LILO is. Once the kernel
    boots your system, it queries the hardware directory and ignore
    BIOS.
    
    To create a primary root partition of 50MB according to our example
    above, enter "n". First, you will be asked for a partition number
    between one and four. Our example in Planning to Partition the Hard
    Drive suggests two. You will be asked if the partition is to be a
    primary or extended, enter \`p\` primary. Next you are asked to
    enter the beginning cylinder which should be the first available
    cylinder from the range given. After you hit enter, you will be
    asked for the ending cylinder. Since we want to make this partition
    50MB, you can enter +50M and fdisk will calculate the nearest ending
    cylinder for a space of about 50MB. Once you have done this, enter
    the "p" command so you can make sure the new partition ends on or
    before cylinder 1023. If the new partition doesn't, use the "d"
    command to delete partition two and try again except enter +40MB for
    the new primary partition and check again with the "p" command. Keep
    doing this until you get a root partition below cylinder 1024.
    Overall, if you cannot create a root partition of at least +25M
    below cylinder 1024, then you will either need to free more space
    below cylinder 1024 or not use LILO.
    
    Next, according to our example, you will want to create a swap
    partition that is 2 x physical ram installed. Creating a swap
    partition requires two steps, first using the "n" command to create
    a primary partition (three in the example). Following the
    instructions above, except enter the value of +(2 x physical RAM)
    MB. For the swap and other partitions, we don't care what there
    beginning and ending cylinders are because they are not crucial for
    LILO to work correctly---only the root partition is. Once you have
    created the Linux native partition to be used as the swap partition,
    you need to use the "t" command to change the partition ID to type
    "82" when prompted. This changes the partition ID so Linux well
    recognize it as a swap partition. When you have successfully done
    this, the "p" command will report that you have a native Linux
    partition and a Linux swap partition.
    
    Now, since we need two more partition, but the hard drive in a PC
    can only support four primary partitions and three primary
    partitions have been used, we need to create an Extended partition
    that occupies the rest of the drive that will allow the creation of
    Logical drive with end the extended partition. This time, to create
    the Extended partition with the "n" command, enter four for the
    partition number and choose "e" when prompted to create an Extended
    partition. When asked for the beginning cylinder, use the first one
    available and for the last cylinder, enter the last available
    cylinder. You are now ready to create Logical drives for
    
        /var
    
    and
    
        /home
    
    according to our example.
    
    To create a logical drive of 40MB to be used as
    
        /var
    
    , enter "n" to create a partition. Because there is no longer a
    choice of Primary or Extended, you are not prompted for this
    information but instead asked if this is to be partition five.
    
    Once you have completed this, you will be asked for the starting
    cylinder which should be the first available cylinder. For the
    ending cylinder, enter +40M for the size as the size was entered
    above. For the
    
        /home
    
    partition, you may have a choice. If your drive is larger than the
    850MB suggested in the example, you can enter +53Mb as indicated
    above and use the extra space for partition such as
    
        /var/spool/mail
    
    and
    
        /usr/local
    
    . Otherwise, just use the last available cylinder to define
    
        /home
    
    . Once you are done creating partitions, you can use the "p" command
    to print the partition one last time to review it. However, you
    won't modify any thing until you use the "w" command to write the
    partition out to the hard disk. If you decided not to modify the
    partition table at this time, choose "e" to exit without modifying
    the partition table. NOTE: When creating Logical partitions, you
    must reboot the system in order for Logical Partitions to be usable.
    Simply go through the options as you did up to being asked to
    partition you drive. However, say no the second time around and
    proceed to the next step.

3.  Once you have created the necessary Linux Native and Linux Swap
    partitions. You are required to have one swap partition. After the
    swap partition is initialized, you will then be asked which
    partition(s) you intended to install Linux to (if upgrading, simply
    indicate your existing root partition): You must configure and
    choose one partition for the root partition. Highlight the root
    partition. Then (unless you are upgrading) you will be presented
    with a table of other available partitions. Choose the appropriate
    partitions and "EDIT" to indicated which partitions will be used for
    which directories. If you have more than one partition for the Linux
    installation, now is the time to designate them as well.

4.  Next is the Software Package Selection. First, a list of software
    categories to install is presented, followed by a chance to
    customize which software packages from each category is to be
    installed. If you have not installed Red Hat or other distribution
    of Linux before, simply choose the category of software you wish to
    install and let the setup program install the defaults for each
    categories. If you find you need a package that wasn't installed
    originally, you can always install it easily later. While the
    software is installing, you will see a progress indicator and should
    get a cup or two of coffee. Installation can take anywhere from
    thirty minutes to an hour or so, depending on software choices and
    hardware configuration.

5.  After the software installation is done, you will be asked to
    configure you mouse. Again, choose what is appropriate for your
    hardware.

6.  Next is the X Window System configuration. We recommend you wait
    until after you boot your system for the first time to configure X.
    If something goes wrong with the X configuration, you may need to
    start the installation procedure from the beginnings the
    Installation Program isn't able to recover.

7.  If you do not have an Ethernet Card, DO NOT configure your network
    at this time. If you do have a network card and didn't configure it
    earlier, you should configure it now. Configuring for a dialup
    network should be done after the installation is complete.

8.  Next, you need to configure the system clock. UTC is a good choice
    if you are on a network and want daylight savings time handled
    properly. Local Time is good if the computer is a stand-alone
    machine.

9.  If you do not have a US Keyboard, you will need to configure for the
    country keyboard you have at this time.

10. You will now be prompted for the system password for the root
    account. Write it down and don't forget it as it is a non-trivial
    matter to recover the password and you will need it to access the
    system when you first reboot.

11. Finally, you will be asked to configure LILO. If you have not
    installed a root partition that begins and ends between cylinder
    0-1023, DO NOT INSTALL LILO\! If, when you reboot the system for the
    first time LILO does not allow you to boot your system correctly,
    use the Emergency DOS/WINDOWS 95 boot diskette and type the
    following at the
    
        A:\> fdisk /mbr
    
    . This will allow your system to boot into an existing DOS or
    Windows 95 system as it did before LILO was installed. You can then
    use the Red Hat Boot Diskette from v4.1 with the following
    parameters at the boot: prompt to you system on the hard drive:
    
        boot: rescue root=/dev/???? ro load\_ramdisk=0
    
      
    Where
    
        ????
    
    is the root partition such hda2 in the example used in this
    document.

Once the installation procedure is completed, you are ready to reboot
your system and use Linux\!

### After Installing Red Hat

Now that you have installed Linux and are booting your system for the
first time, there are some useful things to know about using your system
such Understanding the LILO prompt, Logging In for the First Time and
Using RPM. 

### Understanding the LILO prompt

If you have installed LILO to manage one or more operating systems, the
following useful things should be known:

When you power-on or reboot the system you get the "LILO" prompt which
you have hopefully configured for a 30 second or so delay before it
boots the system. When LILO appears on the screen, if you do nothing,
the default operating system will boot at the prescribed time out
period. However, from LILO, you can control several aspects of how Linux
boots, or tell LILO to boot an alternative operating system. If you wish
to override the default behavior of LILO, pressing the

    Shift

key at the appearance of LILO will cause a "boot:" prompt to appear.
Pressing

    Tab

at this prompt will produce a list of available operating systems:

    LILO boot: 
    dos linux 
    boot:

This tells use that "dos" is the default operating system that will boot
if nothing is typed, or to boot Linux, type "linux" (without the
quotes). However LILO lets you pass overrides to the Linux kernel which
will override the kernels default behavior. For example, you may have
been experimenting with the start-up configuration files and done
something that prevents the system from coming up properly, so you want
to boot the system up to the point (but not after which) it reads the
configuration files. The override for this is "single":

    boot: linux single

  
will boot the system into single user mode so you can take corrective
action. This is also useful if your system won't come all the way up to
the login: prompt some other reason. 

### Logging In the First Time

Now that you are faced with the "login:" prompt for the first time you
may be wondering how to get into the system. At this point on a newly
installed system, there is only one account to login to which is the
administrative account "root". This account is used to manage your
system and doing such things as configuring your system, adding and
removing users, add/removing software, etc. To login into the account,
type "root" (without the quotes) at the login: prompt and hit enter. You
will then be prompted for the password you entered during setup. Enter
that password at the password: prompt. The system prompt

    [root@locahost] #

will appear once you have successfully negotiated the login. The system
prompt tells you two things: you are logged in as "root" and in this
case your machine is called "localhost". If you named your machine
during the installation process, then your machine name will appear
instead of "localhost". Now that you are logged in, you can use such
commands as

``` 
ls
```

to list files,

``` 
cd
```

to change directory, and

    more

to look at the contents of ASCII test files. The root account also has
its own home directory,

    /root

. A home directory is where a valid system accounts places you in the
file system hierarchy once you have successfully logged in. Some Unix
systems use

``` 
/
```

instead, so don't be fooled if you don't see any files if you type "ls";
there aren't any in the root home directory.

#### Creating A User Account

One of the first things you should do on a newly installed system is to
create a regular user account for yourself and plan on using the root
account only for administrative functions. Why is this important?
Because if you make a critical error in manipulating files you are
working on, you can damage the system. Another reason is that programs
run from the root account have unlimited access to the system resources.
If a poorly written program is run from the root account, it may do
unexpected things to the system (because a program run as root has root
access, a program run as a user has restricted resource access) which
will also damage it. To create a user account, you will want to use the

    adduser

and

    passwd

commands:

    [root@bacchus]# adduser hmp
    Looking for first available UID...501 
    Looking for first available GID...501 
    Adding login: hmp...done. 
    Creating home directory: /home/hmp...done 
    Creating mailbox: /var/spool/mail/hmp...done 
    Don't forget to set the password. 
    $[$root@bacchus$]$\# passwd hmp 
    New password: \textsl{new\_passwd} 
    New password (again): \textsl{new\_passwd} 
    Password Changed. 
    passwd: all authentication tokens updated successfully

The new account is now be created and is ready to use. Other things that
may need to be done as root are configuring X Window System, configuring
dialup services, and configuring printer services. These topics are
covered elsewhere.

#### Accessing the CD-ROM and Floppy

One concept under Linux for accessing devices that confuses new users is
that things like CD-ROM disks and floppy diskettes are not automatically
made available when inserted in the drive. Linux abstracts a device to
be file (although in the case a special type of file), And much like a
word processor, you have to tell the system that you want to open a file
or close a file. The command used to open (make a device available) a
device is

    mount

and the command to close (tell the system you are no longer using a
device) is

    umount

. When you open a device under Linux, you make it part of the directory
tree and navigate with the

``` 
cd
```

,

``` 
ls
```

and

``` 
cp
```

(copy) command normally. Red Hat Linux suggests making removable or
temporary devices available under the directory

    /mnt

. They create the directory

    /mnt/floppy 

by default, but not

    /mnt/cdrom

. So, the first time you want to access the CD-ROM, you will need to
create the directory

    /mnt/cdrom

by typing:

    [root@bacchus]\# mkdir /mnt/cdrom

Once you have created the directory, you can access the CD-ROM by
typing:

    [root@bacchus]\# mount -t iso9660 -r /dev/\textsl{cdrom\_device} /mnt/cdrom

The break down of the command line above is this: the "-t" switch tells
the mount command the next argument is a file system type, in this case
"iso9660" is the format on most computer CD-ROM diskettes. The "-r" is a
read-only flag since the CD-ROM is read-only. The next argument,

    /dev/{\sl cdrom\_device}

, is the device file you wish to open. If you performed a CD-ROM
install, you want to replace \\textsl{cdrom\\\_device} with the
designation of your CD-ROM such as:

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><strong>Device File</strong></td>
<td><strong>CD-ROM type</strong><br />
</td>
</tr>
<tr class="even">
<td>hd[a,b,c,d]</td>
<td>for IDE/ATAPI CD-ROMs<br />
</td>
</tr>
<tr class="odd">
<td>scd[0,1,2,...]</td>
<td>for SCSI cdrom drives<br />
</td>
</tr>
<tr class="even">
<td>sbpcd</td>
<td>for Sound Blaster 2X speed drives<br />
</td>
</tr>
<tr class="odd">
<td>mcd or mcdx</td>
<td>for Mitsumi 2X drives<br />
</td>
</tr>
</tbody>
</table>

There are other drive types as well, but these are the most common. Some
literature refers to

    /dev/cdrom

which is a symbolic link. For example, if you have a Secondary IDE/ATAPI
drive set as the master drive, the command:

    ln -sf /dev/hdc /dev/cdrom

  
will create a symbolic link so that the CD-ROM drive can be referred to
as

    /dev/cdrom

as well as

    /dev/hdc

.

Floppy drives are assessed in a similar manner:

    mount -t msdos /dev/fd0 /mnt/floppy

Will make a floppy formatted under DOS in drive "a" available under the
directory /mnt/floppy. If you want to access the floppy diskette in the
b drive, substitute /dev/fd1 for /dev/fd0.

When you are finished with a device such as a CD-ROM or floppy diskette,
it is extremely important that you "close" the file before removing it
from the system. This needs to be done for a variety of reasons, but if
you don't and try to remove it you can make the system unstable and
floppies may get erased. To release a device from the file system, type:

    umount /dev/fd0 (to un-mount a floppy) 
    umount /dev/cdrom (to un-mount a cdrom drive)

For more information on either of these commands, please see the man
pages (e.g., by entering

    man mount

). 

### Shutting Down Linux

It is extremely important that the power is not simply shut off while
Linux is running. You can damage or even make the system un-bootable by
doing so. The proper way to shutdown Linux is to log in as root and
type:

    [root@bacchus]\# shutdown -h now

  
which will cause Linux to write out any files it still has in memory and
close down active programs cleanly. When you get the message

    The system
    has halted

, it is safe to turn the power off. If you want to reboot the computer
with out shutting of the power, use:

    [root@bacchus]\# shutdown -r now

  
which performs all the insectary shutdown work but directs the computer
to restart instead.

