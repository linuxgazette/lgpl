#### "Linux Gazette...*making Linux just a little more fun\!*"

-----

![](../gx/fisk/wkndMech.gif)

# Welcome to The Linux Weekend Mechanic\!

## Published in the June 1997 Edition of the Linux Gazette

**Copyright (c) 1997 John M. Fisk \<fiskjm@ctrvax.vanderbilt.edu\>  
The Linux Gazette is Copyright(c) 1997 [Specialized Systems Consultants
Inc.](http://linuxgazette.net/)**

-----

## Time To Become... *The Linux Weekend Mechanic\!*

|                              |                                                                                                                                                                                                                                                                                                                                                                             |
| ---------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ![](../gx/fisk/mechanic.gif) | You've made it to the weekend and things have finally slowed down. You crawl outa bed, bag the shave 'n shower 'cause it's Saturday, grab that much needed cup of caffeine (your favorite alkaloid), and shuffle down the hall to the den. It's time to fire up the Linux box, break out the trusty 'ol Snap-On's, pop the hood, jack 'er up, and do a bit of overhauling\! |

-----

## Table of Contents

  - [Welcome to the June 1997 Weekend Mechanic\!](wkndmech.html#welcome)
  - [Wallpapering with XV: A Followup](wkndmech.html#wallpapering)
  - [VIM Programming Perks](wkndmech.html#vim-perks)
  - [Closing Up The Shop](wkndmech.html#closing)

-----

## <span id="welcome">![](../gx/fisk/attndant.gif) Welcome to the June 1997 Weekend Mechanic\!</span>

Hey, c'mon in\!

Thanks for dropping by\! How y'all been doing?

So... everyone survive the semester?\! I just finished taking my last
final a day or so ago AND managed to find work (with the folks in
Biomedical Informatics at Vanderbilt Univ. Medical Center :-) within 24
hours of finishing up. PHEW\!\! Nice to be done.

Anyway, I'm going to apologize for the potentially "dry" topics in this
month's WM. I've not been doing much besides programming, cramming, and
making occasional trips to the 'fridge, restroom, and bedroom (pretty
much in that order...). I ended up doing a fair amount of programming
for a couple classes and got VERY acquainted with a number of the
programming tools available under Linux -- VIM, ctags, xxgdb, ddd, and
so forth. Since this is what I've been doing of late, I thought that
this might be an appropriate topic. The proviso is that you understand
that you take this strictly as a novice's introduction to a couple of
these tools.

How's *that* for being wishywashy... :-)

Anyway, I've found a few useful things along the way and thought someone
might enjoy my sharing them.

Also, I want to continue to thank all of you who've taken the time to
write and offer comments and suggestions. Believe me, I don't claim
extensive knowledge or expertise in most of the things I write about --
these are mostly discoveries and ideas that I've hit upon and am sharing
in the hopes that they might be helpful. I welcome corrections,
clarifications, suggestions, and enhancements\! Several of you wrote in
with regards to **wallpapering using XV** which I'll be sharing below.

Well, thanks again for stopping by\! Hope you enjoy :-)

John M. Fisk  
Nashville, TN  
Thursday, 8 May 1997

-----

## <span id="wallpapering">![](../gx/fisk/desklamp.gif)</span>Wallpapering with XV: A Followup

My sincerest thanks to **Brent Olson, Peter Haas, and Bill Lash** for
taking the time to write and offer these suggestions. I tried tinkering
around with a few of these suggestions and they work like a champ\! Here
they are:

> 
> 
> -----
> 
> Date: Wed, 02 Apr 1997 09:24:59 -0800  
> From: Brent Olson \<brent@primus.com\>  
> To: fiskjm@ctrvax.Vanderbilt.Edu  
> Subject: re: reducing the colors in a background  
> 
> You've probably already been told this, but in the LG article relating
> to reducing the number of colours used in the background, there is no
> need to convert the picture first. It can be done on the fly:
> 
>     xv -root -quit -maxpect -ncols 16 filename.gif
> 
> Works great on my lousy 8-bit NCD display at work.
> 
> Brent Olson  
> mailto: brent@primus.com
> 
> -----
> 
> Date: Tue, 08 Apr 1997 08:42:01 +0200 (MET DST)  
> From: hap@adv.magwien.gv.at  
> To: fiskjm@ctrvax.Vanderbilt.Edu  
> Subject: xv - interesting options  
> 
> There are another two interesting options of xv:
> 
>     -random filepattern   selects a random picture of given given filepattern 
>     -ncols #colors        to limit number of used colors
> 
> An example out of my .fvwm2rc95:
> 
>     xv -quit -root -ncols 16 -random /var/X11R6/lib/xinit/pics/*.gif
> 
> Regards, Peter
> 
>     -- 
>      (~._.~)    From the keyboard of Peter Haas (hap@adv.magwien.gv.at)
>      _( Y )_    Located at MA14-ADV, Rathausstr.1, A-1082 Wien, Austria
>     ()_~*~_()   Phone +43-1-4000/91126   FAX +43-1-4000/7141
>      (_)-(_)    "Big iron" division
> 
> -----
> 
> From lash@tellabs.com Thu Apr 24 21:20:39 1997  
> Date: Thu, 24 Apr 1997 17:52:27 -0500  
> From: Bill Lash \<lash@tellabs.com\>  
> To: fiskjm@ctrvax.Vanderbilt.Edu  
> Subject: Limiting colors with XV  
> 
> John,
> 
> I read your article on wallpapering with XV. You suggest choosing
> images with a limited number of colors. You go on to suggest several
> options, but you missed a simple solution. You can tell XV how many
> colors to use in displaying the picture using the -ncols option.
> 
> At work, I usually run with a background of 100 colors on an 8-bit
> pseudocolor display with the following command line:
> 
>     xv -root -quit -max -rmode 5 -ncols 100 image.gif
> 
> Bill Lash  
> lash@tellabs.com

-----

Again, guys, thanks for writing. Happy wallpapering\!

John

-----

## <span id="vim-perks">![](../gx/fisk/desklamp.gif)VIM Programming Perks</span>

Well, as I mentioned above, I ended up spending a good deal of time
programming this semester. Our software engineering team designed and
coded a simple FORTRAN 77 spell checker in C++. Thing was, the analysis
and design phase consumed 11 of the 14 weeks of the semester AND it was
done using Structured Analysis. Problem was, we had decided to code this
thing in C++ and so ended up almost completely redesigning it using OO
Analysis and Design during the last couple weeks (when we were
*supposed* to be doing nothing but coding :-).

Anyway, this meant a LOT of late nights -- integrating everyone's code
got a bit hairy, since none of us had much experience with team coding.
I was mighty thankful for the development tools under Linux. I spent the
better part of 13 hours one Saturday debugging our first effort at
integrating the code -- chasing down Segmentation Faults and infinite
loops :-)

Ahhh... the stuff of programming... :-)

Along the way I learned a few interesting and nifty things about the VIM
editor, which has been my 'ol workhorse editor for the past couple years
now. I wanted to give this thing another plug as I think it's one of the
best things since sliced bread. I'll admit that the **emacsen**,
including the venerable **XEmacs**, are a LOT more powerful and full
featured. But, having developed the finger memory for the
"one-key-vi-commands" I've found that I can get a lot of work done fast.
I'd like to 'tip the hat at this point to **Jesper Pedersen and Larry
Ayers** both of whom have written very nice articles on emacs and XEmacs
in past issues of the LG and the Linux Journal. I'd encourage anyone
interested in these to have a look at these articles. I'll also be
mentioning XEmacs below and give you a screen shot of the latest 19.15
iteration.

Anyway, here's a few (hopefully) interesting notes and ideas for using
the VIM editor\!

### GVIM -- Going Graphical\!

Yup, that's right\! VIM has gone GUI :-)

I recently downloaded and compiled the latest beta version of VIM which
is version 5.0e. If you have the Motif development libraries you can
compile VIM with a Motif interface -- gvim. This rascal is pretty good
sized and not exactly fleet of foot. It's a bit slow getting out of the
gate on startup and so it's probably prudent to heed the Makefile
suggestions and compile separate versions of VIM both with and without X
support. I tried starting versions of vim (at the console) compiled with
and without X support and the extra X baggage definitely slows things
down.

A bit later on in this article I've provided several screen dumps of
gvim as well as a couple other editors and the xxgdb and ddd debuggers.
If you're the impatient or curious type, please feel free to jump ahead
and have a look. Also, I've included a couple links for additional
information.

Actually, VIM has provided a GUI since around version 4.0. I've been
using this for some time now and find that it adds a several
enhancements over vim at the console:

  - it has a reasonably handsome and functional scrollbar
  - mouse support is automatic and allows VERY easy cursor movement,
    text highlighting, and cut-and-paste operations
  - it provides a customizable menubar
  - it intelligently understands the movement keys -- Home, End, Page
    Up, Page Down, arrow keys -- *even* in insert mode
  - depending on how you have your .Xmodmap set up, it will
    intelligently handle Back Space, and Delete keys AND you can delete
    backwards over multiple lines\!

This last point is wonderful. Anyone who's ever tried to backspace onto
the end of a previous line and gotten that miserable BEEP\! will
appreciate this. What's particularly nice about the graphical version of
vim is that it provides several basic features of a GUI style editor
while retaining the speed and flexibility of vi.

### The *Big* News: Syntax Highlighting\!

This is truly a godsend and was one of the features that was definitely
on the 'ol wish list\! VIM now provides color syntax (lexical)
highlighting for a variety of languages including C, C++, HTML (which
I'm using right now...), Java, Ada95, FORTRAN, Perl, and TeX. But that's
not all...\!

(...this is like the guy hawking the $2.99 Ginzu knives, "*they slice,
they dice, here... I can cut through a cinder block wall, this lamp
post, a street sign, AND the hood of this guy's car and never loose an
edge\! But that's not all... if you act right now...*")

You get the point.

What I was going to say was that vim also provides syntax highlighting
for shell scripts (VERY COOL\!), makefiles, and the VIM help files
(which you'll see here in just a bit). All in all, this is pretty nice.
I've been tinkering around with this a bit and am really starting to
like it. Be aware that the highlighting isn't quite as "intelligent" as
with something like XEmacs -- it doesn't provide the same degree of
sophistication. Still, it's very good and, being an order of magnitude
smaller and a good deal more nimble, it's well worth trying.

VIM installed the support files for syntax highlighting (at least on my
system) under /usr/local/share/vim/syntax. There are individual files
for the various languages and file types as well as the syntax.vim file
that does a lot of the basic coordination. You can tinker around with
these to get the "look-n-feel" that you want. Keep in mind that to get
automatic syntax highlighting you'll need to add something like this to
your \~/.vimrc or \~/.gvimrc file:

    " Enable automatic color syntax highlighting on startup
    source /usr/local/share/vim/syntax/syntax.vim

I have to admit that I wrestled with this for longer than I should have
trying to figure out how this was done. Hopefully, this will save you
some trouble :-)

Again, I've included screen dumps below so that you can see what this
looks like. In addition, the VIM home page has a couple nice screen
shots that you might want to have a look at. I should add that syntax
highlighting is individually configurable for the console and the X
version. Now, before you go dashing off and "*rushing in where angels
fear to tread...*" you will probably want to have a look at the help
files or documentation -- it gives some basic guidelines for customizing
this stuff.

And speaking of which...

### Help is on the way\!

One of the coolest and most useful things about VIM is the mind numbing
amount of documentation that comes with it. There's a small library of
support documents covering everything from a blow-by-blow description of
*each* feature and command to information about showing thanks by
providing help for a needy Uganda.

And what's more, all of this is provided on-line. In command mode you
simply type in:

    :help

and the window (under gvim) splits and loads up the top level help file.
This is your gateway to knowledge.

"*...use the Source, Luke*"

The help system is set up in a hypertext fashion. If you've enabled
automatic syntax highlighting then even the help system is colorized. To
follow a link you can either hit the letter "g" and then single click
with the mouse on a topic, or you can move the cursor to that topic and
hit "Ctrl-\]" (hold down the control key and hit the left square bracket
key -- "\]"). To get back up to where you started, hit "Ctrl-t".

It's that simple :-)

IMHO, this is one of the most laudable features of VIM. The
documentation is generally well written and reasonable understandable.
It is VERY thorough and, since it's available from within the editor,
provides a high level of usability. It also provides a "Tips" section as
well as numerous "How Do I...?" sections. It's Must Reading...

### Ask "*The Man\!*"

Another really useful thing to try is accessing manual pages from within
vim. Say you were writing a shell script and needed to quickly look up
something in the bash manual page or you were setting up a test
condition and couldn't remember the syntax for the "greater than" test,
all you have to do is:

    :!man test

and *presto\!*, information. It's instant gratification at its best...
:-)

To be honest, I've found that this works a LOT better at the console
than under gvim, although the exact reason eludes me. Under gvim, I get
the following error:

    WARNING! Terminal is not fully functional

got me on this one...

My suspicion is that it has to do with the termcap stuff built into the
editor. Forward movement down the manual page (hitting the space bar) is
reasonable smooth, but backward movement is very jerky and screen
redraws are incomplete. Still, if you can live with that you'll find
this VERY convenient.

### TAG, You're It\!

This is another one of those things that makes life SO much easier. If
you've not used tags before then brother, it's time to start\!

Basically what tags allow you to do is find that point at which a
function or variable is declared. For example, suppose you ran across
the following snippet of code:

``` 
    HashTable HTbl;
    HTbl.Load("hash.dat");
    found = HTbl.Lookup(buf);
    .
    .
    .
```

and were interested in finding out how the Load method was implemented.
To jump to the point in the file where this is defined simply move the
cursor so that it sits on "Load":

``` 
    HTbl.Load("hash.dat");
         ^
```

and hit "Ctrl-\]" (hold down the control key and hit the right square
bracket key -- "\]"). Beauty of this is, that even if the definition is
not in the file you're currently working on, vim will load up the needed
file and position the cursor at the first line of the function
definition.

This is seriously cool\!

When you're ready to move back to your original location, hit "Ctrl-t"
(which moves you back up the tag stack). I've been using **Exuberant
Ctags, version 1.5, by Darren Hiebert** for the past little bit now and
really like this a lot. As the name implies, it does a pretty thorough
job of scouring your source files for all sorts of useful stuff --
function declarations, typedefs, enum's, variable declarations, macro
definitions, enum/struct/union tags, external function prototypes, and
so forth. It continues on in the time honored tradition of providing a
bazillion options, but not to fear: it's default behavior is sane and
savvy and provides a very nice OOBE\*.

*\*(Out Of Box Experience)*

You should be able to find Darren's Exuberant Ctags (ctags-1.5.tar.gz
was the most recent version on sunsite and its mirrors at the time of
writing) at any sunsite mirror. I happened across it in the Incoming
directory. You'll probably find is somewhere under the /devel
subdirectory now. If you get stuck and really can't find it, drop me a
note and I'll see what I can do to help. This one is definitely worth
having.

Oh, BTW, using ctags is child's play: simple give it the list of files
that you want it to search through and it'll create a "tags" file in
your current directory. Usually, this is something like:

    ctags *.cc *.h

if you happen to be doing C++ programming, or:

    ctags *.c *.h

if you're programming in C. That's all there is to it\! Keep in mind
that you can use tags without having to position the cursor on top of
some function or variable. If you'd defined a macro isAlpha and wanted
to jump to your definition, then simply type in:

    :ta isAlpha

and vim will take you to that point. How 'bout that for easy? There's a
good deal more info on using tags in the VIM online documentation.
Browse and enjoy\!

### Using the *Real* Windows...

Another very handy item that gvim (and vim) provides is multiple
windows. This makes cutting and pasting from one file to another (or
from one section of a file to another) quite easy. It also is nice if
you're reading one file and editing another (for example, reading an
INSTALL file while making changes to the Makefile).

To pop up a second (or third, or fourth...) window with a specific file,
simply use something like:

    :split ctags.README

This would create a second window and load up the ctags.README file. If
you want a second window with the current file displayed there, then
simply use:

    :split

and a second window will be created and the current file loaded into
that window. Under gvim, moving the cursor from one window to the other
is as simple as mouse clicking in the desired window. You can also use
the keystrokes

    Ctrl-w j (hold down control and hit "w", then hit j)
    Ctrl-w k (hold down control and hit "w", then hit k)

to move to the window **below** or the window **above** the current
window respectively. But, use the mouse... it's a lot easier :-)

Resizing the windows is nicely handled using the mouse: simply click
anywhere on the dividing bar between the two windows and drag the bar to
whatever size you want. This is really handy if you are using one file
as an occasional reference but want to edit in a full window. You can
resize the reference file down to a single line when it's not needed.

Again, there's a lot more information in the online help about using
multiple windows.

### *SHHHHHhhhh.....\!* Let Me Tell You A Secret\!

Here's a little something that ought to part of one of those blood oath,
"cross-my-heart-and-hope-to-die", secret society initiations into the
"Secret Lodge of Some Large North American Mammal Society"

Ready...? (look furtively around with squinty, shifty gaze...)

(... the clock ticks loudly in the other room, somewhere in the distance
a dog barks, the room falls into a stifling hush...)

He clears his throat loudly and in a harsh whisper exclaims...

"The "%" sign expands to the current buffer's filename\!"

Phew\! Glad that's over... :-)

Yup, with this little tidbit you can do all sorts of cool and groovy
things. Like what you ask...? (you knew this was coming, didn't you...
:-)

**RCS checkin and checkout**

I won't go into using RCS for version control except to say that doing
checkin's and checkout's from within VIM is VERY easily accomplished
doing something like:

``` 
    :w!
    :!ci -l %
    :e %
    
```

So what's going on...? Well, the first line writes the current buffer to
file, the real good stuff happens on the second line in which you use
the RCS **ci** to checkin and lock the current file. And finally, since
the checkin process may have altered the file if you've included
"Header", "Id", "Log", etc., reloads the file with the new RCS
information (if any).

Now, for all you VIM jockeys out there, the handy thing to do is use
"map" to bind this sequence to a single keystroke. I've bound this to
Alt-r and it makes the whole operation smooth and painless.

**Printing that 'ol file**

This is another favorite trick. To print the current file from within
vim simply:

``` 
    :w!
    :!lpr %
    
```

what could be easier? :-)

Seriously, this is a very convenient means of getting a hard copy of
your current file. The one caveat to remember is that you'll probably
want to commit the contents of your current editing buffer to file
*before* you try to print it.

I've been using the **apsfilter** program for last year or so and
absolutely love it. It is a series of shell scripts that automate the
process of printing. Basically, it uses the **file** command to
determine the type of file to print and then invokes lpr with the
appropriate print filter. As a backend, it uses the **a2ps** program to
format ASCII into Postscript and then uses Ghostscript to do the actual
printing. Now, using something like:

``` 
    lpr [filename]
    
```

transparently formats the file to Postscript and sends it to the
printer. I've been quite pleased with this. You should be able to find
this and similar programs at any of the sunsite FTP sites under the
/pub/Linux/system/print (printer?, printing?) subdirectory (sorry, I'm
not connected to the 'net at the moment and can't recall the exact
subdirectory name off the top of my head :-).

Also, I've played with the a2ps program itself and found all sorts of
cool and nifty options -- single page/double page printing, headers,
boundary boxes, setting font sizes, and so forth. I particularly like
being able to set the font size and header information. And, as always,
IHABO\*.

\*(*It Has A Bazillion Options*)

**Word Counts...**

If you hit the Ctrl-g key combo, VIM prints the filename, number of
line, and the current position in the file on the bottom status line.
However, if you want a word or byte count, simply invoke the **wc**
program on the file:

``` 
    :w!
    :!wc %
    
```

which will print out the file's line, word, and byte count.

You get the picture. Basically, any command that takes the form

    command [-options] filename

can be used from within VIM doing something like:

    :! command [-options] filename

Note that there are a couple other handy little items you might be
interested in. If you want to include the contents of a file in the
current buffer, OR if you want to capture the output of a command into
the current buffer (for example, a directory listing), then use:

    :r a2ps.README
    :r! ls /usr/local/lib/sound/*.au

The first command would insert the contents of the a2ps.README file in
the current buffer wherever the cursor was located; the second command
would insert the output of an **ls** listing for the
/usr/local/lib/sound/ directory. That is, you can use this second form
for any command that *prints its output to standard out*.

This discussion leads directly into the question of spell checking the
current buffer. And the answer that *I've* got is that I haven't found
an easy or convenient way to do this. I ran across a key mapping
definition a while ago that basically copied the current file to the
/tmp directory, ran ispell on this file, and then copied this file back
over the original. It worked, but it was clunky. I've also tried, with
some modest success, to do something like:

    :w!
    :! ispell %
    :e %

which basically commits the current buffer to file, starts a shell and
runs ispell on the file, and then reloads that file once the spell
checking is done. Thing is, this works at least reasonably well running
vim in text mode; under gvim, ispell gives an error message to the
effect:

    Screen too small:  need at least 10 lines
    Can't deal with non-interactive use yet.
    
    1 returned

Ideas anyone?

### Running Make\!

The specifics of setting up a makefile are, in academic parlance,
"*beyond the scope of this article...*". (You can, however, find a good
deal of information about makefiles using **info**; or, more
appropriately, **O'Reilly & Assoc.** put out a very nice little book on
managing projects using make -- hit up you friendly neighborhood
librarian or find it at your favorite Linux bookstore\!)

I've found that gvim, in particular, provides excellent support for
make. Basically, once you have a working makefile, simply invoke it
using:

    :make

As the build process proceeds, you'll see all sorts of nifty messages go
whizzing by. If make terminates on an error, gvim will very kindly load
up the errant file and position the cursor at the line that was
implicated as being the culprit. This is VERY handy. Also, if multiple
errors are encountered, you can move from one error to the next using:

    :cn

which advances to the next error. For some reason, the console version
of vim hasn't worked quite a well as gvim. It doesn't always
automatically go to the first error encountered, although using the
":cn" command seems to work fine.

### And So Forth...

Phew\! How's everyone doing...? Still hanging in there? I'm almost done
here so stay tuned. :-)

There are LOTS of other rather nifty features that vim/gvim provides.
The adventurous will find all sorts of goodies to experiment with in the
online documentation. Let me call your attention to just a few more and
we'll wrap this up and have a look at some screen shots\!

**Unlimited Undo**

The way vim is (generally) configured, it keeps track of ALL the editing
changes you've made to a file. So, after an hour's worth of editing,
should you decide that *War And Peace really didn't need a another
chapter, then you can back out of all your changes by repeatedly hitting
the "u" key. This reverses the changes you've made to the file in a
sequential fashion. Now for a major back out, you'd have done well to
check the original file in under RCS and then retrieve this version if
you decide not to keep your current changes. Still, you can back all the
way out if you don't mind hitting "u" for a while... :-)*

**Compressed File Support**

One of the other nice things introduced into vim around version 4.0 was
support for editing compressed files. Essentially, what this involves is
transparent uncompressing of the file upon the start of editing and
recompressing the file when vim terminates. This is quite helpful as it
allows you to save a LOT of space if you work with a large number of
text files that can be compressed. You may also be aware of the fact
that the pager "less" has this support built in and so do most all of
the "emacsen".

The support for this is configured in using an entry in your \~/.vimrc
or \~/.gvimrc. I use the stock vimrc example that comes with the
distribution:

    " Enable editing of gzipped files
    "    read: set binary mode before reading the file
    "          uncompress text in buffer after reading
    "   write: compress file after writing
    "  append: uncompress file, append, compress file
    autocmd BufReadPre,FileReadPre      *.gz set bin
    autocmd BufReadPost,FileReadPost    *.gz '[,']!gunzip
    autocmd BufReadPost,FileReadPost    *.gz set nobin
    
    autocmd BufWritePost,FileWritePost  *.gz !mv <afile> <afile>r
    autocmd BufWritePost,FileWritePost  *.gz !gzip <afile>r
    
    autocmd FileAppendPre           *.gz !gunzip <afile>
    autocmd FileAppendPre           *.gz !mv <afile>r <afile>
    autocmd FileAppendPost          *.gz !mv <afile> <afile>r
    autocmd FileAppendPost          *.gz !gzip <afile>r

I still haven't completely gotten the hang of the autocmd stuff -- I
suspect that there's all sorts of wild and fun things that you can do
with this. Ahhh... places to go and things to do...\!

**Auto-Fill and Auto-Comment Continuation**

Here's yet *another* nifty little feature that makes life fuller and
richer... :-)

You can set a text width variable in your \~/.gvimrc file that will do
auto-fill (or auto-wrapping) at that line length. Currently, I have this
set to 78 so that whenever the line exceeds 78 characters the line is
automagically continued on the next line. This is a Very Nice Thing when
typing text, although it can be a bit of a nuisance (and can be shut
off) when programming.

However...

There's an additional benefit to using this auto-fill thingie... if
you're inserting a comment in C, C++, a shell script, whatever..., all
you have to do is start the first line with a comment character ("/\*",
"//", "\#") and then start typing. If the comment extends to the text
width column, it automatically continues this on the next line AND adds
the appropriate comment character\!

Very Slick\! :-)

**Choices, Choices...\!**

Well, the recurrent theme of the day is "choices\!". VIM comes with more
options than you can shake a stick at. I'd encourage you to have a look
at the online docs for a description of these. Not all of them will be
useful to you but there are a LOT of interesting things that you can
configure. My own favorite ones include:

    set ai                " turn auto indenting on
    set bs=2              " allow backspacing over everything in insert mode
    set noet              " don't expand tabs into spaces
    set nowrap            " disable line wrapping
    set ruler             " display row,col ruler
    set showmatch         " show matching delimiter for parentheses, braces, etc
    set ts=4              " set tab stop width to 4
    set tw=78             " always limit the width of text to 78
    set sw=4              " set the shift width to 4 spaces
    set viminfo='20,\"50  " read/write a .viminfo file, don't store more
                          " than 50 lines of registers

One thing to call you attention to: the shift width stuff is something
that you might not have tried yet or come across. Suppose that you've
coded some horrendous switch statement and then realize that you need to
add a while loop before it. You code in the while loop stuff and then go
back and arduously re-indent everything in between.

There's an easier way... :-)

Simply highlight the lines that you want to indent, either indent in or
indent back out, using the mouse or ":v" (for keyboard highlighting) and
then hit the "\>" key to indent the lines in farther or the "\<" key to
indent back out. Now, the nice thing is that you can set the amount of
indentation using the "sw" (shiftwidth) variable.

Also, keep in mind that while you normally set options in the \~/.vimrc
or \~/.gvimrc configuration files, there's nothing to prevent your
changing these options on the fly, and in different parts of your file.
It's pretty common to turn off autoindentation when you're doing cutting
and pasting. To turn autoindenting off, simply type in:

    :set noai

and off it goes. To turn it back on use ":set ai".

Two other options that I particularly like are the ruler and the
showmatch options. The ruler option puts a row,column indicator in the
status line at the bottom of the file. Although the documentation
mentions that this can slow performance a bit, I've found that it works
with no noticeable delay whatsoever.

The other option is showmatch, which highlights the matching brace,
bracket, or parenthesis as you type. Be aware that it sounds a warning
beep if you insert a right brace/bracket/parenthesis without its opening
mate. This can be a little annoying, but the time it saves you a syntax
error, you'll be glad for it. I did a little bit of LISP programming
this Spring in our Theory of Programming Languages course and was mighty
happy to use this\!

### Ahhh\! Time For The Pictures\!

Congrats\! If you've made it this far you might be interested in finally
having a look at all the good stuff that I've been mentioning here.

Here's the skinny...

What I did was create a number of screen dumps of gvim in action --
editing a \*.cc file (show off the syntax highlighting stuff...), using
the online help system (also shows the multi-window look), and
displaying a manual page from within gvim ("Look ma\! No hands...\!"). I
used the venerable **ImageMagick** to make the thumbnail prints after
using a combination of **xv, xwpick, and xwd** to make the actual screen
dumps and crop the pics.

Also, for the comparison shoppers out there, I've included similar
screen dumps of **XEmacs, GNU Emacs, NEdit, and XCoral** -- other very
nice and feature-rich editors that some of you will be familiar with.
All of these provide syntax-highlighting and a set of extended features.

Finally, I've included a couple shots of the **xxgdb** and the **DDD**
debuggers. I've been using both quite a bit lately and found that they
are absolutely indispensable for tracking down mischievous bugs. I've
included a couple URL's below as well, but let's start with the Family
Photo Album:

### gvim Screen Shots

All of these are approximately 20k.

[![](misc/gvim-th.gif)](misc/gvim.gif)
[![](misc/gvim-help-th.gif)](misc/gvim-help.gif)
[![](misc/gvim-man-th.gif)](misc/gvim-man.gif)

### The "Other Guys..."

All of these are approximately 20-25k

[![](misc/xemacs-19.15-th.gif)](misc/xemacs-19.15.gif)
[![](misc/emacs-th.gif)](misc/emacs.gif)
[![](misc/nedit-th.gif)](misc/nedit.gif)
[![](misc/xcoral-th.gif)](misc/xcoral.gif)

### The xxgdb Debugger

[![](misc/xxgdb-th.gif)](misc/xxgdb.gif)

### The DDD Debugger

All of these are approximately 20-25k

[![](misc/ddd-2.1-th.gif)](misc/ddd-2.1.gif)
[![](misc/ddd-2.1-data-th.gif)](misc/ddd-2.1-data.gif)
[![](misc/ddd-2.1-msg-th.gif)](misc/ddd-2.1-msg.gif)

Let me make just a couple comments about the debuggers.

First, I've found both of these to be very usable and helpful in terms
of making debugging easier. They are both front ends to the GNU GDB
debugger (and DDD can be used with a variety of other debuggers as
well). The xxgdb debugger is the simpler of the two and probably is a
good place to start learning and tinkering if you've not used a
graphical debugger before.

I ended up having to do a bit of tinkering with the resource settings
for xxgdb. I'm currently using Fvwm95 with a screen resolution of
1024x768 and 8-bit color. To get all the windows comfortably in 1024x768
I messed around with the geometry resources. Also, the file selection
box was completely whacked out -- I did a bit of adjustment to this to
provide for a more sane display. If you're interested, here's the XDbx
resource file I'm currently using:

### [Xxgdb resource file](misc/XDbx.ad)

Also, the DDD debugger shown above is the most current public release --
version 2.1 which just recently showed up in the Incoming directory at
sunsite. I don't know if it'll still be there, but you have have a try.
If you don't find it there, try the /pub/Linux/devel/debuggers
subdirectory and see if it hasn't been moved there.

### [Sunsite Linux Incoming Directory](http://sunsite.unc.edu/pub/Linux/Incoming/)

Keep in mind that you probably *should* be using one of the sunsite
mirrors. If there's one near you, then use it\! :-) There should be
dynamic and static binaries available as well as the source code. In
addition, there's an absolutely HUGE postscript manual page with lots of
nifty pictures included in the /doc subdirectory in the source file.

I've not had a chance to use the new DDD debugger as much as xxgdb, but
what I've tried I'm been VERY impressed with. You'll see from the screen
shots above that it has a much improved GUI as compared to xxgdb. Also,
a number of new features have been added since the previous 1.4 release.
One feature that I really like is setting a breakpoint, running the
program, and then, by positioning the mouse pointer over a variable or
data structure, getting a pop up balloon with the current value of that
data structure.

This is huge. It rocks\!

I really don't have time to talk about this, so you'll have to do a bit
of exploring on your own\! Also, note that the folks working on DDD are
encouraging the Motif-havenot's to either use the static binaries or
give the **LessTif** libraries a try. Apparently, there have been some
successes using this toolkit already. I'm sorry that I don't have the
URL for LessTif, but a Yahoo, Alta Visa, etc., search should turn up
what you need.

And lastly (and this really is the last... :-), here's some URL's for
the editors listed above:

### [VIM Home Page](http://www.math.fu-berlin.de/~guckes/vim/)

### [XEmacs Home Page](http://www.xemacs.org/)

### [ftp.x.org FTP site (XCoral)](ftp://ftp.x.org/contrib/)

### [sunsite.unc.edu FTP site (NEdit)](ftp://sunsite.unc.edu/pub/Linux/X11/xapps/)

The first two links should put you at the VIM and XEmacs home pages
which provide a wealth of helpful information about each of these
excellent editors. The last two I apologetically provide as approximate
FTP links. The first will drop you into ftp.x.org in its /contrib
subdirectory. You should be able to find the latest version of XCoral
there, probably under the /editors subdir. The version shown above is
version 2.5; the latest version of xcoral is 3.0, which I've not had a
chance to compile or tinker with. The last link will put you at sunsite
in the /X11/xapps subdirectory. Have a look in the /editors subdir for
the latest source or binaries for NEdit.

Phew\! That was a *tour de force*\! Glad you hung in there\!

I'd be happy to try to field questions about this stuff or hear back
from anyone with comments or suggestions about any of these excellent
programs.

Hope you enjoyed\!

John

-----

## <span id="closing">![](../gx/fisk/desklamp.gif)Closing Up The Shop</span>

Well, I apologize again for the brevity of this month's column. I'd
hoped to do a bit more writing on a couple different things,
particularly one of the topics that's near and dear to my heart: shell
scripting. I'm absolutely convinced that learning even basic shell
scripting will forever sour you to DOS and will make you think twice
even about the Windows stuff. Shell programming opens up a tremendous
world of possibilities and, probably most importantly, *it puts you in
control of your system*. It let's you do all sorts of cool and groovy
things that would be difficult or impossible under a DOS/Win system.

As a quick example, I'd recently had an occasion in which I needed to
format a stack of 30-40 floppies (I was planning to do an afio backup of
the XEmacs distribution I'd spent several hours downloading) and decided
to use *superformat* to do this. Now superformat is a great little
program that has the typical bazillion options. Since I needed only a
few of these options for my particular system, I whipped together a
shell script to help automate this process. It's no marvel of
programming genius, but here it is:

    #!/bin/sh
    #
    # fdformt.sh   formats 1.44 HD floppies in the fd0 drive
    #
    # Author:      John M. Fisk <ctrvax.vanderbilt.edu>
    # Date:        6 May 1997
    
    FORMAT_CMD="superformat -v 3 "
    FLOPPY_DEV="dev/fd0"
    
    while : ; do
        echo -n "Format floppy [y,n]? "
        read yesno
        if [ "yesno" = "y" -o "yesno" = "Y" ]; then
            echo -n "Insert floppy and hit any key to continue..."
            read junk
            ${FORMAT_CMD} ${FLOPPY_DEV}
        else
            break
        fi
    done

Now, I'm sure that this could easily be improved upon, but the point was
that it took me all of about 3 minutes to write this, it's easily
maintained, and the logic is simple enough that it needs no
documentation.

Why bring this up?

Well, I think this points to one of the larger issues with using and
running Linux: *the sense of control*. Thing is, under a Linux system,
you have an impressive arsenal of powerful and mature tools at your
disposal that allow you to do things with you system. You can make it do
what *you* need and want it to do.

Don't get me wrong, I enjoy many of the features of the OS/2, Win95, and
MacOS OS's and I hope that the day will come when office suites and
productivity tools of the highest caliber exist for Linux as they do
under these other OS's. The thing that sets Linux apart is the freely
available set of powerful tools that provide an unparalleled measure of
freedom and control over your system.

Think about it...

Shell scripting, Perl, Tcl/Tk, the entire range of GNU development tools
and libraries, and a suite of immensely powerful utilities and programs.

That's impressive.

Anyway, I'm preaching to the choir... :-)

Also, this is something of "old news", but I wanted to thank the folks
at **RedHat Software, Inc.**, the LUG at **North Carolina State
University**, and the myriad participants in this year's **Linux Expo
'97**. It was a blast\!

A bunch of us from MTSU headed East and managed to get to most of the
two day affair. All in all, with the minor exception of some parking
problems, the whole affair when smoothly and was VERY professionally
done. The talks were delightful, the facilities very nice, and there
were lots of great displays and vendor booths to visit and check out the
latest Linux offerings. The book tent out front cleaned out more than
one person's wallet, sending them home laden down with all sorts of
goodies.

All in all, it was a great trip.

For anyone who went, I was, in fact, the annoying short nerdy looking
fella in the front row with the camera. Sorry... :-)

But, I just got the prints back and have sent a stack of them off to
**Michael K. Johnson** at RedHat. Since I don't have a scanner or my own
web site, I figured the Right Thing To Do would be to send the doubles
to the guys at RedHat and let them put up anything they thought
worthwhile. If you're interested in seeing who some of the various Linux
folks are, drop Michael a note and I'm sure that he'll help out.

Well, guess it's time to wrap this up. I had a great year this year at
MTSU and am looking forward to finishing up school here one of these
years :-). I'm also looking forward to having a summer of nothing more
than Monday through Friday, 9:00 - 5:00. I don't know about you, but
I've always got a long list of projects that I want to work on. I'm
really looking forward to this. I've finally started learning **emacs**
-- actually, I've just gotten the most recent public release of
**XEmacs** and have been having all sorts of fun trying to figure this
one out. My wife and I will be leaving tomorrow for a couple weeks in
Africa -- actually, Zimbabwe and Zambia. Her parents are finishing up
work there and will be returning this Fall. After a busy year for both
of us, we're excited about a vacation and the chance to see them again.
I should be back by the time this month's LG "hits the stands", although
if you wrote during much of May, be aware that I'm definitely going to
have a mail black-out\! :-)

So... trust y'all are doing well. Congrats to all of this year's grads\!

Take care, Happy Linux'ing, and Best Wishes,

John M. Fisk  
Nashville, TN  
Friday, 9 May 1997  

-----

![](../gx/fisk/mailme.gif) If you'd like, drop me a note at:

John M. Fisk
[\<fiskjm@ctrvax.vanderbilt.edu\>](mailto:fiskjm@ctrvax.vanderbilt.edu)

-----

#### Previous "Weekend Mechanic" Columns

[Weekend Mechanic \#1, November 1996](../issue11/wkndmech.html)  
[Weekend Mechanic \#2, December 1996](../issue12/wkndmech.html)  
[Weekend Mechanic \#3, February 1997](../issue14/wkndmech.html)  
[Weekend Mechanic \#4, April 1997](../issue16/wkndmech.html)

-----

##### Copyright © 1997, John M. Fisk  
Published in Issue 18 of the Linux Gazette, June 1997

-----

[![\[ TABLE OF CONTENTS \]](../gx/indexnew.gif)](index.html) [![\[ FRONT
PAGE \]](../gx/homenew.gif)](https://linuxgazette.net/index.html) [![
Back ](../gx/back2.gif)](sybase.html) [![ Next
](../gx/fwd.gif)](lg_backpage18.html)

-----
