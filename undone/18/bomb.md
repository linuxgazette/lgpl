## An Interactive Image Generator

By Larry Ayers, <layers@vax2.rainis.net>

### Introduction

Last month I wrote about Cthugha, a sound-to-image converter and display
engine. Bomb is another image-generating program, but the sound
component is subsidiary. The images produced have an entirely different
character than those produced by Cthugha. Rather than working with and
displaying audio data, bomb uses a variety of algorithms to generate
images. Most of these are one form or another of artificial life (John
Conway's Life is the most familiar of these), while some others are
fractal, reaction-diffusion, or IFS-related.

Bomb is a console Svgalib program, with no X11 version at this time.

### Bomb's Images

The output of bomb has a distinctive character, due in large part to the
color palettes used by the program, which are contained in the file
*cmap-data*. The images have a naturalistic, painting-like character,
with earth-tones predominating. The reason for this is that Scott Draves
generated the palettes using his program **image2cmap**, which extracts
a representative 256-color palette from an image file. Scott used a
variety of scanned photographs as input. The result is that bomb is
strongly marked by Scott Draves' esthetic preferences.

The format of the *cmap-data* file is ascii text, with an example
palette's first lines looking like this:  
`  `

(comment leafy-face)  
(cmap  
(42 37 33) (31 23 25) (23 19 22) (20 20 24) \[etc\]

This is similar to the format of the palette files used by Fractint and
Cthugha; it probably wouldn't be too difficult to convert one format to
the other.

The images are displayed full-screen, at 320x200 resolution. This gives
them a somewhat chunky, pixel-ish appearance, and also seems to
contribute to the painting-like quality. Many of the screens are
reminiscent of a magnified view of microorganisms; there is an illusion
of opaque, non-human purpose.

Here are a pair of sample bomb screens. The program has a built-in
capture facility with the images saved as **ppm** files.  

![Bomb Screen \#1](gx/ayers/bomb1.gif)

![Bomb Screen \#2](gx/ayers/bomb2.gif)

-----

### Compilation and/or Installation

The bomb archive file is rather large, over two megabytes; installed the
bomb directory occupies nearly four and one-half mb., which seems like a
lot for a relatively small program. Most of this space is taken up by
the **suck** subdirectory. **Suck** contains about 200 *TIFF* image
files. Some of the bomb modes use these images as seeds. The program
will work fine without these images, so if you're short of disk space
they could be deleted; another approach is to weed through the images
and retain just a few favorites. If examined with an image viewer the
*TIFF* files can be seen to be mostly basic, small black-and-white
images, including large heavily-serifed single letters and logo-like
images from a variety of cultures. When used as a seed, the image
appears nearly full-screen but is eventually "eaten" by the pullulating
AI organisms until it is unrecognizable.

Another subdirectory, called **dribble**, is where your screen-captures
end up. Each captured *PPM* image takes up 197 kb., so it is wise to
check the directory from time to time and weed through the captures.

Bomb is rather picky about the versions of the required JPEG and TIFF
libs on your system; they must be compatible with each other in some
mysterious way. Initially I couldn't get it to run at all, but a
reinstallation of the two graphics lib packages (from the same
distribution CD, so that theoretically they would be compatible) cured
this. Oddly enough my previous TIFF and JPEG libs, though updated
independently of each other, worked with other programs which required
them. Another argument for staying with a distribution\!

A binary is included in the distribution; the source is there if for
some reason the binary won't work, or if you'd like to modify it.

This program is one of those which is designed to be run from its own
directory; in other words, you just can't move the executable to a
pathed directory and leave the datafiles somewhere else. The easiest way
to install it is to unarchive the package right where you want it to
stay. Then when you want to run bomb, *cd* to its directory and start it
from there.

### Controlling the Display

You can get by using bomb just knowing that the spacebar randomizes all
parameters and control-c quits. I found it worthwhile to print out the
section of the readme file which details the various keyboard commands,
as nearly every key does something.

A different mode of keyboard control is enabled by pressing one of the
first four number keys. Scott calls this the "mood organ", and when in
this mode subtle parameters of the currently active display-type can be
changed. In this state the entire keyboard changes parameters within the
current mode; it's completely remapped, and can be returned to the
default mode by pressing the "1" key.

Left to its own devices, bomb periodically randomizes its parameters.
Some combinations of color-map and algorithm are more appealing than
others, so that if it seems stuck in a type of image you don't like,
just press the spacebar and give it a fresh start. Another approach is
to key in some preferred parameters; the display will still randomly
change but will remain within the category selected.

Bomb is the sort of program I like to set running when I'm doing
something else within sight of the computer; if something interesting
appears some tweaking will often nudge the program along a fruitful
channel.

### Obtaining the Archive

The current version of bomb (version 1.14) can be obtained from
[Sunsite](ftp://sunsite.unc.edu/pub/Linux//apps/graphics/hacks/svgalib/)
or from the [Bomb Home FTP site](ftp://hopeless.mess.cs.cmu.edu/spot).

### Is There Any Real Use For Such Programs?

Aside from the obvious real-time entertainment value, programs such as
bomb, cthugha, and xlockmore can serve as grist for the Gimp, the
incredible (but difficult to learn) GNU image-processing tool. Lately
I've been fascinated by the 0.99.9 developer's version of the Gimp. In
this release an image can be saved as a **\*.pat** file, which is a
Gimp-specific image format used most often as flood-fill material. There
is a "Patterns" window which, when invoked, shows thumbnails of all of
the **\*.pat** files in the Gimp pattern directory, including new ones
you've dropped in. These are available for flood-fill if, in the "Tool
Options" dialog, *patterns* rather than *color* has been checked. (Don't
ask how long it took me to discover this\!) Many of the bomb modes will
produce tileable images, which makes them particularly useful as
background fill material. The tricky aspect of this (as is true with any
animated image generator) is capturing the screen at the right time. All
too often the perfect image fleetingly appears (on its way to /dev/null)
and is gone before you can save it.
