<span id="musings"></span>

<table>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><h2 id="more...">More...</h2>
<img src="../gx/hammel/musings.gif" width="247" height="52" alt="Musings" /> </td>
</tr>
</tbody>
</table>

 

|  |
|  |
|  |

![indent](../gx/hammel/cleardot.gif)

|                                                               |
| ------------------------------------------------------------- |
| © 1997 [Michael J. Hammel](mailto:mjhammel@graphics-muse.org) |
| ![indent](../gx/hammel/cleardot.gif)                          |

<span id="1"></span>

 ![](gx/hammel/mindseye1.jpg)

## Main Window

 

![](gx/hammel/mindseye2.jpg)

 

## Materials Editor

![indent](../gx/hammel/cleardot.gif)

 

|                                                                  |
| ---------------------------------------------------------------- |
| © 1997 by [Michael J. Hammel](mailto:mjhammel@graphics-muse.org) |
