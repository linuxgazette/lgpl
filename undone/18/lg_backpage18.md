# ![Linux Gazette Back Page](../gx/backpage.gif)


### Contents:

  - [About This Month's Authors](lg_backpage18.html#authors)
  - [Not Linux](lg_backpage18.html#notlinux)


-----

### About This Month's Authors

-----

#### ![](../gx/note.gif)Larry Ayers

Larry Ayers lives on a small farm in northern Missouri, where he is
currently engaged in building a timber-frame house for his family. He
operates a portable band-saw mill, does general woodworking, plays the
fiddle and searches for rare prairie plants, as well as growing shiitake
mushrooms. He is also struggling with configuring a Usenet news server
for his local ISP.

#### ![](../gx/note.gif)Jim Dennis

Jim Dennis is the proprietor of [Starshine Technical
Services](http://www.starshine.org). His professional experience
includes work in the technical support, quality assurance, and
information services (MIS) departments of software companies like
[Quarterdeck](http://www.quarterdeck.com), [Symantec/ Peter Norton
Group](http://www.symantec.com), and [McAfee
Associates](http://www.mcafee.com) -- as well as positions (field
service rep) with smaller VAR's. He's been using Linux since version
0.99p10 and is an active participant on an ever-changing list of mailing
lists and newsgroups. He's just started collaborating on the 2nd Edition
for a book on Unix systems administration. Jim is an avid science
fiction fan -- and was married at the World Science Fiction Convention
in Anaheim.

#### ![](../gx/note.gif)John M. Fisk

John Fisk is most noteworthy as the former editor of the *Linux
Gazette*. After three years as a General Surgery resident and Research
Fellow at the Vanderbilt University Medical Center, John decided to
\&quot:hang up the stethoscope\&quot:, and pursue a career in Medical
Information Management. He's currently a full time student at the Middle
Tennessee State University and hopes to complete a graduate degree in
Computer Science before entering a Medical Informatics Fellowship. In
his dwindling free time he and his wife Faith enjoy hiking and camping
in Tennessee's beautiful Great Smoky Mountains. He has been an avid
Linux fan, since his first Slackware 2.0.0 installation a year and a
half ago.

#### ![](../gx/note.gif)Guy Geens

One of Guy Geens's many interests is using Linux. One of his dreams is
to be paid for being a Linux geek. Besides his normal work, he is the
(rather inactive) maintainer of his research group's web pages
[http://www.elis.rug.ac.be/\~ggeens](http://www.elis.rug.ac.be/ELISgroups/ve).

#### ![](../gx/note.gif)Ivan Griffin

Ivan Griffin is a research postgraduate student in the ECE department at
the University of Limerick, Ireland. His interests include C++/Java,
WWW, ATM, the UL Computer Society (http://www.csn.ul.ie) and of course
Linux (http://www.trc.ul.ie/\~griffini/linux.html).

#### ![](../gx/note.gif)Michael J. Hammel

Michael J. Hammel, is a transient software engineer with a background in
everything from data communications to GUI development to Interactive
Cable systems--all based in Unix. His interests outside of computers
include 5K/10K races, skiing, Thai food and gardening. He suggests if
you have any serious interest in finding out more about him, you visit
his home pages at http://www.csn.net/\~mjhammel. You'll find out more
there than you really wanted to know.

#### ![](../gx/note.gif)Mike List

Mike List is a father of four teenagers, musician, printer (not
laserjet), and recently reformed technophobe, who has been into
computers since April,1996, and Linux since July.

#### ![](../gx/note.gif)Dave Phillips

Dave Phillips is a blues guitarist & singer, a computer musician working
especially with Linux sound & MIDI applications, an avid t'ai-chi
player, and a pretty decent amateur Latinist. He lives and performs in
Findlay OH USA.

#### ![](../gx/note.gif)Henry Pierce

Henry graduated from St. Olaf College, MN where he first used BSD UNIX
on a PDP-11 and VAX. He first started to use Linux in the Fall of 1994.
He has been working for InfoMagic since June of 1995 as the lead Linux
technical person. He is now an avid Red Hat user.

#### ![](../gx/note.gif)Michael Stutz

Michael lives the Linux life. After downloading and patching together
his first system in '93, he fast became a Linux junkie. Long a proponent
of the GNU philosophy (publishing books and music albums under the GPL),
he sees in Linux a Vision. Enough so that he spends his time developing
a custom distribution (based on Debian) and related documentation for
writers and other "creative" types and have formed a consulting firm
based on GNU/Linux. His company, Design Science Labs, does Linux
consulting for small-scale business and art ventures. He has written for
*Rolling Stone*, *2600*: *The Hacker Quarterly* and *Alternative Press*.
He's a staff writer for *US Rocker*, where he writes about underground
rock bands.

#### ![](../gx/note.gif)Josh Turial

Josh Turiel is the IS Manager of a small advertising agency South of
Boston. He also runs the Grater Boston Network Users Group
(http://www.bnug.org/). He also writes and does consulting work, as
well. Since he has no life whatsoever as a result, his rare home time is
spent sucking up to his wife and maintaining his cats.
<span id="notlinux"></span>

-----

### Not Linux

-----

Thanks to all our authors, not just the ones above, but also those who
wrote giving us their tips and tricks and making suggestions. Thanks
also to our new mirror sites.

![](../gx/rose.gif)My assistant, Amy Kukuk, did **ALL** the work this
month other than this page. If this keeps up, I may have to make her the
Editor. Thanks very much for all the good work, Amy.

These days my mind seems to be fully occupied with *Linux Journal*. As a
result, I've been thinking I need a vacation. And, in fact, I do. I had
been planning to take off a week in June to visit my grandchildren in
San Diego, California, but just learned that their current school
district is year round -- no summers off. Somehow this seems anti-kid,
anti-freedom and just plain hideous. I remember the summers off from
school as a time for having fun, being free of assignments and tests --
a time to sit in the top of a tree in our backyard reading fiction,
while the tree gently swayed in the breeze (I was fairly high up). It
was great. I wouldn't want to ever give up those summers of freedom. I
wish I still had them. Ah well, no use pining for "the good ol' days".
The grandkids will get some time off from school in August, and I will
just have to put off the vacation until then.

### Stop the Presses

Be watching the Hot Linux News (link on The Front Page) on June 7 for an
important announcement concerning the trademark issue.

Have fun\!
