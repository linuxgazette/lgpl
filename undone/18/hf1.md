<span id="hf1"></span>

<table>
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><h2 id="more...">More...</h2>
<br />
<img src="../gx/hammel/musings.gif" width="247" height="52" alt="Musings" /></td>
</tr>
</tbody>
</table>

  
![indent](../gx/hammel/cleardot.gif)  

|                                                     |
| --------------------------------------------------: |
| © 1997 [Michael J. Hammel](mailto:mjhammel@csn.net) |
|                ![indent](../gx/hammel/cleardot.gif) |

![](gx/hammel/hf1.jpg)

**Figure 1**: HF-Lab command line interface

<span id="erosion"></span>

|                                             |                                             |
| ------------------------------------------- | ------------------------------------------- |
| ![](gx/hammel/hf2.jpg)                      | ![](gx/hammel/hf3.jpg)                      |
| **Figure 2**: HF produced from erosion1.scr | **Figure 3**: HF produced from erosion2.scr |

![](gx/hammel/hf4.jpg)

**Figure 4**: leathery surface, which I created completely by accident

![indent](../gx/hammel/cleardot.gif)

|                                                        |
| -----------------------------------------------------: |
| © 1997 by [Michael J. Hammel](mailto:mjhammel@csn.net) |
