

## Brave GNU World: Towards A Bioregional, Community-based Linux Support Net

#### By Michael Stutz, <stutz@dsl.org>

-----

I believe there's strong potential now for the growing LUG phenomenon to
intertwingle with both the Linux Documentation Project and the Linux
support network of the c.o.l.\* newsgroups and create the next "level"
of support for Linux. The net result of this would be a
self-documenting, technical support, training and social network on an
Internet-wide scale (perhaps some would say that's what it already is --
then I mean it would be the same only exponentially better). Right now,
I see a lot of work (documentation, debugging, support) being
duplicated. If these efforts could be combined (LUG + LDP + c.o.l.\*),
it would eliminate a lot of this excess work; the net result would would
be greater than its parts, a synergy.

Many LUGs give demos and post the notes on their web servers. That
information is diffused across many obscure sites, but bringing these
efforts together with the LDP folks, I wonder if a new breed of HOWTOs
(DEMOs?) could be created; a common indexing scheme could have a list of
all demos or tutorials ever given at any LUG, both searchable and listed
by subject or other criteria.

And while the c.o.l.\* newsgroups are invaluable for a great many
things, sometimes local help is preferable. With the right organization,
community-based LUGs could be the first stop for a Linux user's
questions and problems, with an easy forwarding mechanism to go up a
chain to be broadcast to the next larger bioregion, then continent-wide
and finally, if the question is still not answered, world-wide.

By not duplicating the work, we'll be freeing up our time to develop
even more things than the current rate, plus the increased support net,
replete with documentation and local support, will allow for a greater
user base. More ideas could be implemented to strengthen this base, such
as "adopt-a-newbie" programs. For instance, there's a guy in town named
Rockie who's in this rock band called Craw; I once saw in a zine he
published that he was starting a volunteer initiative to collect old
donated computer equipment, refurbish them, and make them available to
musicians who otherwise wouldn't be able to use computers. Why not take
that a step further and make them Linux boxes? Not only would you get a
non-corporate, rock-solid OS, but you'd have an instant support network
in your own town. This kind of community-based approach seems the best
way to "grow" GNU/Linux at this stage.

This community-based LUG network would be capable of handling any and
all GNU/Linux support, including the recently-discussed Red Hat Support
Initiative, as well as Debian support, Slackware support, etc. It's
above and beyond any single "distribution" and in the interest of the
entire Linux community.

I think the key to all this is planning. It need not happen all at once.
It's happening already, with local LUGs making SQL databases of LUG
user's special interests and/or problems, and their own bioregional
versions of the Consultants-HOWTO, etc. What is needed most of all is a
formal protocol, a set of outlines and guidelines, that all LUGs, when
ready, can initiate -- from technical details such as "What format to
develop the database?" to everything else. It need not be centralized --
like the rest of Linux, it will probably come together from all points
in the network -- but our base is large enough now that taking a look at
the various Linux efforts from a biological and geographical
community-based standpoint, and re-coordinating from there, is something
that only makes sense.

Copyright (C) 1997 Michael Stutz; this information is free; it may be
redistributed and/or modified under the terms of the GNU General Public
License, either Version 2 of the License, or (at your preference) any
later version, and as long as this sentence remains.
