# HelpDex

**By [Shane Collinge](../authors/collinge.html)**

*These images are scaled down to minimize horizontal scrolling.*

[**Flash problems?**](../124/misc/nottag/flash.html)  

[Click here to see the full-sized image](misc/collinge/113personals.swf)

[Click here to see the full-sized image](misc/collinge/092fries.swf)

[Click here to see the full-sized image](misc/collinge/098gantt.swf)

All HelpDex cartoons are at Shane's web site,
[www.shanecollinge.com](http://www.shanecollinge.com/).
