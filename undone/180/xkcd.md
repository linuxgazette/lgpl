# XKCD

**By [Randall Munroe](../authors/munroe.html)**


[![\[cartoon\]](misc/xkcd/constructive.png
"And what about all the people who won't be able to join the community because they're terrible at making helpful and constructive co-- ... oh.
")  
Click here to see the full-sized image](misc/xkcd/constructive.png)


More XKCD cartoons can be found [here](http://xkcd.com).
