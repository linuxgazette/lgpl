# Away Mission - ApacheCon, QCon, ZendCon and LISA

**By [Howard Dyckoff](../authors/dyckoff.html)**

## Away Mission - ApacheCon, QCon, ZendCon and LISA

This November has a surfeit of superior conferences, most actually
running in the first week. The conferences listed in the title all start
on Nov 1st which might force you to make some hard choices.

I'll be putting notes on ApacheCon after ZendCon and QCon, but not out
of a preference; instead, it's simply in order of shortest commentary to
longest. These are all great events and we could even include the Linux
Kernel Summit, which is also the first week of November, but we haven't
attended one before.

### LISA

Unlike the conferences reviewed below, LISA - the Large Installation
System Administrator conference organized by USENIX - is during the
second week of November and has few conflicts with other events. This is
an event with a long history, dating back to the '80s and predating the
Internet boom. Indeed, this is the 24th LISA conference. The focus
nowadays is on the backends for companies like Google, Yahoo, and
Facebook.

I haven't attended a LISA conference since I was a sysadmin, before the
beginning of the millennium, but I fondly remember having an opportunity
to talk with other sysadmins who were working on the bleeding edge of
data center operations. This is the first LISA actually in the San
Francisco area, even though USENIX is headquartered in Berkeley. Most
West Coast LISA events have been held in San Diego.

Presentations at LISA cover a variety of topics, e.g. IPv6 and ZFS, and
training includes Linux Security, Virtualization, and cfEngine among
others. Topics covered by the invited presenters include:

  - "10,000,000,000 Files Available Anywhere: NFS at Dreamworks," by
    Sean Kamath and Mike Cutler, PDI/Dreamworks
  - "Operations at Twitter: Scaling Beyond 100 Million Users," by John
    Adams, Twitter

You can find online proceedings from LISA going back to 1993
[here](http://usenix.com/events/bytopic/lisa.html).

### ZendCon 2010

The 6th Annual Zend/PHP Conference will bring together PHP developers
and IT staff from around the world to discuss PHP best practices. This
year it will be held at the Santa Clara Convention Center which has lots
of free parking and is convenient to Silicon Valley and the San Jose
airport. It starts on November 1st and - unfortunately - conflicts with
QCon in San Francisco and ApacheCon in Atlanta.

This used to be a vest-pocket conference at the Hyatt near the San
Francisco airport and was much more accessible from that city. There was
a charm in its smallness and its tight focus and it brought the PHP
faithful together with the Zend user community.

This year, ZendCon will host technical sessions in 9 tracks plus have
in-depth tutorials and an enlarged Exhibit Hall (with IBM, Oracle and
Microsoft as major sponsors). A variety of tracks - SQL, NoSQL,
architecture, lifecycle, and server operations - are available. This is
an opportunity to learn PHP best practices in many areas.

There will also be an unconference running the 2nd and 3rd days of
ZendCon, featuring both 50 minute sessions and 20 minute lightning
talks. There is also a CloudCamp unconference on the evening of the
tutorial day where early adopters of Cloud Computing technologies
exchange ideas. And the 3 days of regular conferencing, without
tutorials, is only a modest $1100 before it starts. Not a bad deal
either way.

If you want to look for the slides from ZendCon 2009, they can be found
[here](http://dz.zend.com/a/11246); to listen to the audio recordings
being released as a podcast, take a look
[here](http://devzone.zend.com/podcasts/zendconsessions).

For full 2010 conference info, visit <http://www.zendcon.com/>.

### QCon 2010

QCon is a personal favorite of mine because of its breadth and its
European roots. The mix of advanced Agile discussions with Java and Ruby
programming and new database technology is intoxicating. Its also a
place to see and hear something not repeated at other US conferences.

This year, tracks include Parallel Programming, the Cloud, Architectures
You Never Heard Of, Java, REST, Agile process, and NoSQL. Check out the
tracks and descriptions [here](http://qconsf.com/sf2010/tracks/).

Last year, the Agile track (and the tutorials) included several sessions
on Kanban as form of 'Lean' and 'Agile' development methodology. Kanban
is derived from the signaling used to control railroad traffic and uses
token-passing to optimize queues and flows. The idea is to do the right
amount of work to feed the next step in the process. An example of a
Kanban work flow can be seen in the anime feature "Spirited Away" by
Hayao Miyazaki, where a ghostly bath house regulates the use of hot
water by passing out use tokens. That way the capacity of the system is
never exceeded.

To get info on last year's event, including videos of Martin Fowler and
Kent Beck speaking, visit this [link](http://qconsf.com/sf2009/).

### ApacheCon 2010

I was pleasantly surprised to find this 'Con so close to home since it
is held in Boston half the time. The Oakland Convention Center is an
underutilized gem in the redeveloped Oakland downtown, very close to the
regional Bay Area Rapid Transit station and many fine eateries in
Oakland's burgeoning new cuisine district. And the weather is warmer
than in San Francisco.

ApacheCon spanned an entire week in 2009. The training period started
the week with 2 days of half day classes. This was paralleled by 2 days
of Bar Camp - the Unconference that is \`included in ApacheCon' - as
well as a Hackathon, which in this case was a kind of code camp for
project submitters.

This was the first year that the Bar Camp and Hackathon paralleled the
training days. Since these two programs were 'free' as in beer, it
afforded a chance for many local developers to get involved; it also
drew several folk who had never been to ApacheCon. It was also the 10th
anniversary of the Apache Software Foundation and there was a
celebration during the conference.

Sessions were 50 min long and spanned 4 consecutive tracks. The track
content varied over the 3 days of ApacheCon and included many prominent
Apache projects. Some presentations were a bit dry and concerned with
the details and philosophy of ongoing projects. Some were report cards
on the progress of joint efforts with industry and academia.

I attended sessions on Tomcat and Geronimo as well as Axis and other
Apache projects. These were very detailed and informative.

The Lightning Talks are short sessions held the last night of the
conference. This ApacheCon variant is up to 5 minutes, on anything you
want. The limited rules state "No Slides and No Bullets" (as there would
be in presentations.) They provide the beer and wine. And they encourage
recitals - its supposed to be fun... And they were.

The closing keynote was by Brian Behlendorf, titled "How Open Source
Developers Can (Still\!) Save The World". Behlendorf was the primary
developer of the Apache Web server and a founding member of the Apache
Group and is currently a Director of CollabNet, the major sponsor of
Subversion and a company he co-founded with O'Reilly & Associates.

He spoke about the important contribution developers can make to
non-profits and noted that he is on the board of Bentech, which provides
the Martus encryption tools to the human rights and social justice
sector to assist in the collection, safeguarding, organization, and
dissemination of information about human rights violations. Martus
Server Software accepts encrypted bulletins, securely backs them up and
replicates them to multiple locations, safeguarding the information from
loss. The software was used in the Bosnia war crimes trials.

Among other non-profit development projects, Behlendorf mentioned
[PloneGov.org](http://plonegov.org) which produces modules for Plone to
implement common services (like a city meeting template). The aim is to
make government more transparent and more efficient.

Behlendorf also mentioned Sahana, a part of Sri Lankan Apache Community
that was started after 2004 Tsunami to help relocated survivors. This
effort has now resulted in a disaster relief management package used in
20 cities. The UN is starting to use it as this was a space neglected by
commercial software corporations.

His summary points: we geeks have skills that are worth more than just
dollars and hours to non-profit organizations. So find a project and
help out, even non-code contributions can matter.

This year's ApacheCon will be held in Atlanta starting on November 1st.
Sessions will feature tracks on Cassandra/NoSQL, Content Technologies,
(Java) Enterprise Development, Geronimo, Felix/OSGi, Hadoop + Cloud
Computing, Tomcat, Tuscany, Commons, Lucene, Mahout + Search, Business &
Community. Free events include a 2-day BarCamp and the evening MeetUps.
For more info, visit <http://na.apachecon.com/c/acna2010/>.

Lunches were boxed on most conference days, but the last day had hot
pizza and pasta with great desserts so it was worth lasting out the
conference. Also, all the vendor swag was piled on a table for the
taking on the last afternoon - so with ApacheCon, patience is a virtue.

The wiki for ApacheCon has some links for presentation slides, but not
after the European ApacheCon that preceded the North America ApacheCon
in 2009. See [here](http://wiki.apache.org/apachecon/).

### SalesForce, DreamForce

If you are interested in Cloud Computing, consider the user conference
for [SalesForce.com](http://salesforce.com), the annual DreamForce event
in San Francisco. It was a November event in 2009 but will happen on
December 6-9 this year. There are usually a few open source vendors at
the DreamForce expo.

\[ The Away Mission Column will be on holiday leave next month. \]
