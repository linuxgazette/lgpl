# Ecol

**By [Javier Malonda](../authors/malonda.html)**

The Ecol comic strip is written for
[escomposlinux.org](http://escomposlinux.org) (ECOL), the web site that
supports es.comp.os.linux, the Spanish USENET newsgroup for Linux. The
strips are drawn in Spanish and then translated to English by the
author.

*These images are scaled down to minimize horizontal scrolling.*

[![\[cartoon\]](misc/ecol/tiraecol_en-391.png)  
Click here to see the full-sized image](misc/ecol/tiraecol_en-391.png)

All Ecol cartoons are at
[tira.escomposlinux.org](http://tira.escomposlinux.org/) (Spanish),
[comic.escomposlinux.org](http://comic.escomposlinux.org/) (English) and
<http://tira.puntbarra.com/> (Catalan). The Catalan version is
translated by the people who run the site; only a few episodes are
currently available.

<span class="small">These cartoons are copyright Javier Malonda. They
may be copied, linked or distributed by any means. However, you may not
distribute modifications. If you link to a cartoon, please
[notify](mailto:jmr@escomposlinux.org) Javier, who would appreciate
hearing from you.</span>

Talkback: [Discuss this article with The Answer
Gang](mailto:tag@lists.linuxgazette.net?subject=Talkback:180/ecol.html)

-----
