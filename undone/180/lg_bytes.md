# News Bytes

**By [Deividson Luiz Okopnik](../authors/dokopnik.html) and [Howard
Dyckoff](../authors/dyckoff.html)**

<table>
<colgroup>
<col style="width: 50%" />
<col style="width: 50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><img src="../gx/bytes.gif" alt="News Bytes" /></td>
<td><h3 id="contents">Contents:</h3>
<ul>
<li><a href="lg_bytes.html#general">News in General</a></li>
<li><a href="lg_bytes.html#Events">Conferences and Events</a></li>
<li><a href="lg_bytes.html#distro">Distro News</a></li>
<li><a href="lg_bytes.html#commercial">Software and Product News</a></li>
</ul></td>
</tr>
</tbody>
</table>

**Selected and Edited by [Deividson
Okopnik](mailto:bytes@linuxgazette.net)**

Please submit your News Bytes items in **plain text**; other formats may
be rejected without reading. \[You have been warned\!\] A one- or
two-paragraph summary plus a URL has a much higher chance of being
published than an entire press release. Submit items to
<bytes@linuxgazette.net>. Deividson can also be reached via
[twitter](http://www.twitter.com/deivid_okop).

-----

## News in General

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)Linux Foundation User Survey Shows Enterprise Gains

At its recent End User Summit, the Linux Foundation published "Linux
Adoption Trends: A Survey of Enterprise End Users," which shares new
data that shows Linux is poised to take significant market share from
Unix and especially Windows while becoming more mission critical and
more strategic in the enterprise.

The data in the report reflects the results of an invitation-only survey
of The Linux Foundation's Enterprise End User Council as well as other
companies and government organizations. The survey was conducted by The
Linux Foundation in partnership with Yeoman Technology Group during
August and September 2010 with responses from more than 1900
individuals.

This report filters data on the world's largest enterprise companies and
government organizations - identified by 387 respondents at
organizations with $500 million or more a year in revenues or greater
than 500 employees.

The data shows that Linux vendors are poised for growth in the years to
come as large Linux users plan to both deploy more Linux relative to
other operating systems and to use the OS for more mission critical
workloads than ever before. Linux is also becoming the preferred
platform for new or "greenfield" deployments, representing a major shift
in user patterns as IT managers break away from legacy systems.

Key findings from the Linux adoption report:

\* 79.4 percent of companies are adding more Linux relative to other
operating systems in the next five years.

\* More people are reporting that their Linux deployments are migrations
from Windows than any other platform, including Unix migrations. 66
percent of users surveyed say that their Linux deployments are brand new
("Greenfield") deployments.

\* Among the early adopters who are operating in cloud environments,
70.3 percent use Linux as their primary platform, while only 18.3
percent use Windows.

\* 60.2 percent of respondents say they will use Linux for more
mission-critical workloads over the next 12 months.

\* 86.5 percent of respondents report that Linux is improving and 58.4
percent say their CIOs see Linux as more strategic to the organization
as compared to three years ago.

\* Drivers for Linux adoption extend beyond cost - technical superiority
is the primary driver, followed by cost and then security.

The growth in Linux, as demonstrated by this report, is leading
companies to increasingly seek Linux IT professionals, with 38.3 percent
of respondents citing a lack of Linux talent as one of their main
concerns related to the platform.

Users participate in Linux development in three primary ways: testing
and submitting bugs (37.5%), working with vendors (30.7%) and
participating in The Linux Foundation activities (26.0%).

"The IT professionals we surveyed for the Linux Adoption Trends report
are among the world's most advanced Linux users working at the largest
enterprise companies and illustrate what we can expect from the market
in the months and years ahead," said Amanda McPherson, vice president of
marketing and developer programs at The Linux Foundation.

To download the full report, please visit:
<http://www.linuxfoundation.org/lp/page/download-the-free-linux-adoption-trends-report>.

-----

## Conferences and Events

  - **Linux Kernel Summit**  
    November 1-2, 2010. Hyatt Regency Cambridge, Cambridge, MA  
    <http://events.linuxfoundation.org/events/linux-kernel-summit>.

<!-- end list -->

  - **ApacheCon North America 2010**  
    1-5 November 2010, Westin Peachtree, Atlanta GA  
    
    The theme of the ASF's official user conference, trainings, and expo
    is "Servers, The Cloud, and Innovation," featuring an array of
    educational sessions on open source technology, business, and
    community topics at the beginner, intermediate, and advanced levels.
    
    Experts will share professionally directed advice, tactics, and
    lessons learned to help users, enthusiasts, software architects,
    administrators, executives, and community managers successfully
    develop, deploy, and leverage existing and emerging Open Source
    technologies critical to their businesses.
    
    [![](../gx/conferences/ACNA_2010.png)  
    ApacheCon North America 2010](http://na.apachecon.com)

<!-- end list -->

  - **QCon San Francisco 2010**  
    November 1-5, Westin Hotel, San Francisco CA  
    <http://qconsf.com/>.

<!-- end list -->

  - **ZendCon 2010**  
    November 1-4, Convention Center, Santa Clara, CA  
    <http://www.zendcon.com/>.

<!-- end list -->

  - **9th International Semantic Web Conference (ISWC 2010)**  
    November 7-11, Convention Center, Shanghai, China  
    <http://iswc2010.semanticweb.org/>.

<!-- end list -->

  - **LISA '10 - Large Installation System Administration Conference**  
    November 7-12, San Jose, CA  
    <http://usenix.com/events/>.

<!-- end list -->

  - **ARM Technology Conference**  
    November 9-11, Convention Center, Santa Clara, CA  
    <http://www.arm.com/about/events/12129.php>.

<!-- end list -->

  - **Sencha Web App Conference 2010**  
    November 14-17, Fairmont Hotel, San Francisco CA  
    <http://www.sencha.com/conference/>.

<!-- end list -->

  - **Agile Development Practices East**  
    November 14-19, Rosen Centre, Orlando, FL  
    <http://www.sqe.com/AgileDevPracticesEast/>.

<!-- end list -->

  - **Devox 2010 (Java Dev Conference)**  
    November 15-19, Metropolis, Antwerp, Belgium  
    <http://reg.devoxx.com/>.

<!-- end list -->

  - **Semantic Web Summit 2010**  
    November 16-17, Hynes Center, Boston, MA  
    Discount Code: SEMTECH  
    <http://www.semanticwebsummit.com/>.

<!-- end list -->

  - **DreamForce 2010 / SalesForce Expo**  
    December 6-9, Moscone Center, San Francisco  
    <http://www.salesforce.com/dreamforce/DF10/home/>.

<!-- end list -->

  - **CARTES & IDentification 2010**  
    December 7-9, Nord Centre, Paris, Fance  
    \[Use code VCGR01 for 20% off or free expo pass\]  
    <http://www.cartes.com/ExposiumCms/do/exhibition/>.

-----

## Distro News

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)Ubuntu 10.10 Released

The latest Ubuntu version, 10.10, was released on 10:10:10 UCT on Oct
10th.

This release is focused on home and mobile computing users, and
introduces an array of online and offline applications to Ubuntu Desktop
edition with a particular focus on the personal cloud.

Canonical has also launched the 'Ubuntu Server on Cloud 10' program
which allows one to try out Ubuntu 10.10 Server Edition on Amazon EC2
for free for 42 minutes. Visitors to the download pages will now be able
to experience the speed of public cloud computing and Ubuntu. For a
direct link to the trial, please go to: <http://10.cloud.ubuntu.com>.

Ubuntu 10.10 will be supported for 18 months on desktops, netbooks, and
servers.

Ubuntu Netbook Edition users can experience a new desktop interface
called 'Unity' -- specifically tuned for smaller screens and computing
on the move. Unity includes a brand-new Ubuntu font family, a redesigned
system installer, the latest GNOME 2.32 desktop, Shotwell as the new
default photo manager, plus other new features.

Find more information on Ubuntu 10.10 here:
<http://www.ubuntu.com/getubuntu/releasenotes/1010overview>.

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)Puppy Linux 5.1 released

The new Puppy Linux 5.1, code-name "Lucid" as it is binary-compatible
with Ubuntu Lucid Lynx packages, has been released. Lucid Puppy 5.1 is a
"full-featured compact distro."

As a Puppy distro, it is fast, friendly, and it can serve as a main
Linux desktop. Quickpet and Puppy Package Manager allow easy
installation of many Linux programs, tested and configured for Lucid
Puppy.

The Puppy UI looks better because the core Lucid team now includes an
art director. The changes relative to Puppy 5.0 has been to improve the
user interface, both to make it more friendly and to enhance the visual
appearance.

Lucid Puppy boots directly to an automatically configured graphical
desktop, with the tools to personalize the desktop and recommends which
add-on video driver to use for high-performance graphics.

Download Puppy 5.1 from <http://www.puppylinux.com/download/>.

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)Ubuntu Rescue Remix 10.10 Released

Version 10.10 (Maverick Meerkat) of the data recovery software toolkit
based on Ubuntu is out. This is an Ubuntu-based live medium which
provides the data recovery specialist with a command-line interface to
current versions of the most powerful open-source data recovery software
including GNU ddrescue, Photorec, The Sleuth Kit and Gnu-fdisk. A more
complete list is available here:
<http://ubuntu-rescue-remix.org/Software>.

The live Remix provides a full shell environment and can be downloaded
here:
<http://ubuntu-rescue-remix.org/files/URR/iso/ubuntu-rescue-remix-10-10.iso>.

This iso image is also compatible with the USB Startup Disk Creator that
has included with Ubuntu since 9.04. PendriveLinux also provides an
excellent way for Windows users to put Ubuntu-Rescue-Remix on a stick.

The live environment has a very low memory requirement due to not having
a graphical interface (command-line only). If you prefer to work in a
graphical environment, a metapackage is available which will install the
data recovery and forensics toolkit onto your current Ubuntu Desktop
system.

The toolkit can also be installed on a live USB Ubuntu Desktop with
persistent data. To do so, add the following archive to your software
channels:  
deb http://ppa.launchpad.net/arzajac/ppa/ubuntu lucid main

Then authenticate this software source by running the following
command:  
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys BDFD6D77

Then, install the "ubuntu-rescue-remix-tools" package.

-----

## Software and Product News

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)Novell Delivers SUSE Linux Enterprise Server for VMware

VMware and Novell have announced the general availability of SUSE Linux
Enterprise Server for VMware, the first step in the companies' expanded
partnership announced in June of 2010. The solution is designed to
reduce IT complexity and accelerate the customer evolution to a fully
virtualized datacenter.

With SUSE Linux Enterprise Server for VMware, customers who purchase a
VMware vSphere license and subscription also receive a subscription for
patches and updates to SUSE Linux Enterprise Server for VMware at no
additional cost. Additionally, VMware will offer the option to purchase
technical support services for SUSE Linux Enterprise Server for VMware
for a seamless support experience available directly and through its
network of solution provider partners. This unique solution benefits
customers by reducing the cost and complexity of deploying and
maintaining an enterprise operating system running on VMware vSphere.

With SUSE Linux Enterprise Server for VMware. both companies intend to
provide customers the ability to port their SUSE Linux-based workloads
across clouds. Such portability will deliver choice and flexibility for
VMware vSphere customers and is a significant step forward in delivering
the benefits of seamless cloud computing.

"This unique partnership gives VMware and Novell customers a simplified
and lower-cost way to virtualize and manage their IT environments, from
the data center to fully virtualized datacenters," said Joe Wagner,
senior vice president and general manager, Global Alliances, Novell.
"SUSE Linux Enterprise Server for VMware is the logical choice for
VMware customers deploying and managing Linux within their enterprise.
This agreement is also a strong validation of Novell's strategy to lead
in the intelligent workload management market."

"VMware vSphere delivers unique capabilities, performance and
reliability that enable our customers to virtualize even the most
demanding and mission-critical applications," said Raghu Raghuram,
senior vice president and general manager, Virtualization and Cloud
Platforms, VMware. "With SUSE Linux Enterprise Server for VMware. we
provide customers a proven enterprise Linux operating platform with
subscription to patches and updates at no additional cost, improving
their ability to complete the transformation of their data center into a
private cloud while further increasing their return on investment."

### ![lightning bolt](https://linuxgazette.net/gx/bolt.gif)Kanguru Hardware Encrypted USB Flash Drives for Linux

Kanguru Solutions has added Mac and Linux to the list of compatible
platforms for its Kanguru Defender Elite, manageable, secure flash
drives.

The Kanguru Defender Elite offers security features including: 256-bit
AES Hardware Encryption, Remote Management Capabilities, On-board
Anti-Virus, Tamper Resistant Design, and FIPS 140-2 Certification. The
addition of GUI support for Mac and Linux opens Kanguru's secure flash
drives to a much wider audience, allowing Mac and Linux users to benefit
from its industry leading security features.

"Our goal has been to develop the most comprehensive set of security
features ever built into a USB thumb drive." said Nate Cote, VP of
Product Management. "And part of that includes making them work across
various platforms. Encryption and remote management are only useful if
they work within your organizations' IT infrastructure."

The Kanguru Defender Elite currently works with:

\* Red Hat 5  
\* Ubuntu 10  
\* Mac 10.5 and above  
\* Windows XP and above

Additional compatibility is in development for several other Linux
distributions and connectivity options.
