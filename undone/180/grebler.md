# Enemy Action

**By [Henry Grebler](../authors/grebler.html)**

``` 
    "Once is happenstance. Twice is coincidence. Three times is
    enemy action." 
            - Ian Fleming (spoken by Auric Goldfinger)
```

### Once

Sometimes I buy tickets on the Internet. About a month ago, I bought
tickets to the
[footy](http://en.wikipedia.org/wiki/Australian_rules_football). There
are several options, but I choose to get the tickets emailed to me as a
PDF. I have no idea what research the ticketing agencies do, but they
obviously do not include me in the profile of their typical user.

I planned to go with a friend, Jeremy, who supports the opposition team.
Jeremy is in paid employment, whereas I am currently "between
engagements". He intended to ride his bicycle to the stadium; I would
take the train. I agreed to meet Jeremy inside the stadium.

But I've still got to get him his ticket.

Sometimes the tickets come as separate PDF files, but this time they
were both in a single PDF. I struggled mindlessly for some time and
eventually sent Jeremy a 6.5 MB .ppm\! Fortunately, it gzipped very
nicely and the email was less than 350 KB.

### Twice

Recently, I bought more tickets for some future footy game. Again, they
came as a single PDF. This time it was Mark and Matt to whom I wanted to
send tickets.

I went to my help directory to see what I had done the previous time.
Nothing. As disciplined as I try to be, I remain disappointingly human.

Never mind. I've had lots of time living with myself. I now know there
are two of me. I've learnt to put up with the hopeless deficiencies of
the Bad Henry. After all, what can you do?

So, I worked out (again\!) how to split out pages from a PDF. Between
the first and second time, I'd been on a ten-day trip to Adelaide.
Before I leave home on long trips I shut down my computer. And my brain.

My solution this time involved PostScript files. I wrote it up (Good
Henry was in charge). Then, for completeness (Good Henry can be quite
anal), I reconstructed what I probably did the first time and wrote that
up also.

### Three times

Today it happened again. Last night I booked tickets for the theatre.
(You see? There's more to me than just football.) When I looked at the
tickets, I found a single PDF containing 4 tickets: two for me and my
wife, two for Peter and his wife, Barbara.

And finally I hit the roof. I'd tried `Adobe Acrobat`, `xpdf`, and
`evince`. They all have the same problem: they insist on honouring the
stupid restrictions included in the PDF, things like "no copying".

```
Encrypted:      yes (print:yes copy:no change:no addNotes:no)
```

Think about it. How stupid is this? If I have **x.pdf**, no matter what
some cretin includes in the file, nothing can stop me from going:

```
cp x.pdf x2.pdf
```

So what has been achieved by the restriction? The words of the song
about the benefits of war come to mind: absolutely nothing\! And I
**am** tempted to say it again: absolutely nothing.

\[ These days, there are so many ways to remove/bypass/eliminate/ignore
that so-called "protection" - e.g.,
[ensode.net](http://ensode.net/pdf-crack.jsf)'s online "unlocker" - that
it is indeed meaningless. -- Ben \]

So let's say I send Jeremy or Mark or Matt a copy of the entire PDF.
What I'm concerned about is that they may inadvertently print the wrong
ticket. Sure, in the email, I tell each person which ticket is his.
But... since each ticket has a barcode, perhaps the stadium checks and
complains about a second attempt to present the same ticket.
Embarrassment all round.

Notice that I'm not trying to do anything illegal or underhanded. I'm
not trying to violate some cretin's digital rights. I'm not trying to
harm anyone. I'm trying to get a ticket to my friend; a ticket that I
bought legitimately; one that I'm not only entitled, but required, to
print out. I guess they want me to fax it to him. But isn't that also
making a copy? Do they expect me to drive across town to get him his
ticket?

It seems to me amazing that I'm the only one who wants this capability.
Every time I've looked on my computer (`apropos`) or anywhere else
(Internet), I've never found a convenient mechanism. I now have a
mechanism (see next section), but this is really a plea for help. Can
anyone suggest something a little more elegant than what I've come up
with?

### 2-Cent Tip

Here is the best method I could come up with for extracting one page, or
several contiguous pages, from a PDF into a separate PDF:

```
gs -dSAFER -sDEVICE=pdfwrite -dNOPAUSE -dBATCH \
-dFirstPage=3 -dLastPage=4 \
-sOutputFile=PeterAndBarbara.pdf Tickets.pdf
```

Basically this command says to extract pages 3 and 4 from Tickets.pdf
and put them into PeterAndBarbara.pdf. The first line consists mainly of
window dressing.

If you want to extract each page as a separate PDF file (not a bad
idea), here's a script:

```sh
#! /bin/sh
#       pdfsplit.sh - extract each page of a PDF into a separate file

        myname=`basename $0`

#----------------------------------------------------------------------#
Usage () {
        cat <<EOF
Usage: $myname file

where   file    is the PDF to split

Output goes to /tmp/page_n.pdf
EOF
        exit
}

#----------------------------------------------------------------------#
Die () {
        echo $myname: $*
        exit
}

#----------------------------------------------------------------------#
        [ $# -eq 1 ] || Usage

        pages=`pdfinfo $1 | grep Pages | awk '{print $2}'`
        [ "$pages" = '' ] && Die "No pages found. Perhaps $1 is not a pdf."
        [ "$pages" -eq 1 ] && Die "Only 1 page in $1. Nothing to do."

        jj=1
        while [ $jj -le $pages ]
        do
                gs -dSAFER -sDEVICE=pdfwrite -dNOPAUSE -dBATCH \
                        -dFirstPage=$jj -dLastPage=$jj \
                        -sOutputFile=/tmp/page_$jj.pdf $1
                jj=`expr $jj + 1`
        done

```

### Notes

You can look at `gs(1)` for a limited amount of help. For more useful
help, do this:

```
gs -h
```

and look at the second last line. Mine says:

```
For more information, see /usr/share/ghostscript/8.15/doc/Use.htm.
```

I've probably broken my system, because it is actually in
**/usr/share/doc/ghostscript-8.15/Use.htm**. Another system refers
correctly to **/usr/local/share/ghostscript/8.64/doc/Use.htm**.

You can use your usual browser or you can use:

```
lynx /usr/share/doc/ghostscript-8.15/Use.htm
```

See also `pdfinfo(1)`. Here's an example of its output:

```
pdfinfo Tickets.pdf
Producer:       Acrobat Distiller 7.0.5 (Windows)
CreationDate:   Tue Mar 16 04:21:58 2010
ModDate:        Tue Mar 16 04:21:58 2010
Tagged:         no
Pages:          4
Encrypted:      yes (print:yes copy:yes change:no addNotes:no)
Page size:      595 x 842 pts (A4)
File size:      1290313 bytes
Optimized:      no
PDF version:    1.4
```
