# Ten Tips to Harden Your Website

Autor: Sander Temme

---

Sander will be presenting “Hardening Enterprise Apache Installations
Against Attacks” and is Apache HTTP Server Track Chair at
[ApacheCon](http://na.apachecon.com/), the ASF’s official conference,
training, and expo, taking place 1-5 November 2010 in Atlanta, Georgia.

## Introduction

Websites are under continuous attack. Defacement, malware insertion and
data theft are but a few of motivators for the attackers, and anyone who
runs an Internet-facing website should be prepared to defend themselves.

Website owners have many tools at their disposal with which to thwart
online attackers; entire books have been written about this field. Here
are ten tips that will quickly improve the security of your website.

## 1. Harden your Operating System

The default installation of your server operating system likely comes
with a number of features you would not use. Turn off operating system
services that are not used by your web application. Be especially wary
of running anything that exposes a network listener (a network listener
is a system task that listens on a given network port for incoming
client connections). Uninstall software packages you do not use, or opt
not to install them in the first place: the smaller your software
installation, the smaller the attack surface. Use the extra security
features offered by your operating system like SELinux or BSD
securelevel. For instance, default SELinux configuration on Red Hat
Enterprise Linux strikes a good balance where server programs that
listen on the network are locked down tightly (which makes sense), but
regular system maintenance utilities have the run of the system (because
locking them down would be very complex to configure and maintain).

## 2. Use Software and Operating System that You can Maintain

The idea that one vendor’s operating system is inherently more secure
than any others is largely a myth. You do not achieve increased security
just by picking an operating system and then forgetting about it. Every
operating system environment is subject to vulnerabilities, which get
fixed and patched over time. Run your website on an operating system
that you and your organization are comfortable maintaining and patching:
a well maintained, up-to-date Windows server may well be more secure
than a stale, unmaintained Linux installation. Your task is to maintain
and defend your website, not to engage in a religious war of one
operating system versus another.

## 3. Slim Down your Server Software

When first installed, the default configuration of server software is
often intended to be a feature showcase, rather than an Internet facing
deployment. Turn off unused Apache modules. Slim down your server
configuration file: only include configuration directives for
functionality that you actually use. Remove sample code, default
accounts and manual pages: install those on your development system.
Turn off all error reporting to the browser. A lean server configuration
is easier to manage, and presents attackers with less opportunity.

## 4. Access your Management Interface over SSL

Many website environments and Content Management Systems are managed
through an interface that is part of the website itself. Consider
running this part of your site over SSL to protect login credentials and
other sensitive data. An SSL-enabled website is protected by a
cryptographic key and certificate that identify the server. You do not
need to buy an expensive certificate to do this: generate your own
certificate and add it to the trusted certificate store of your browser:
as website manager, you are the only one who needs to trust your own
certificate. Content Management Systems like Wordpress allow you to
enable SSL on the management interface with a few configuration
settings. Encrypting management connections with SSL is an effective way
to protect your all-important login credentials.

## 5. White-list Allowed Actions, Blacklist Attacks

As site manager, you should know which legitimate HTTP operations users
can perform on your site. Ideally, you should treat any other action as
hostile, and deny it. Unfortunately, this is not always feasible or
politically achievable: you may have inherited an environment in which
all the valid URL and operation combinations are simply unknown, and
aggressive restrictions may interfere with business operations. On the
Apache web server, you can use a Web Application Firewall like
ModSecurity by Ivan Ristic to allow and disallow access to particular
URLs and actions. Other web server software packages provide similar
features. If you are unable to white-list allowed actions, you should be
able to blacklist attacks as you see them happen.

## 6. Use a Firewall

After a web application is compromised, the attackers will often attempt
to have the application download additional malware. Or, installed
malware may connect to a command-and-control server hosted elsewhere.
Generally, a web application should only respond to incoming requests,
and not be allowed to make outgoing TCP connections. This makes server
maintenance a little more cumbersome, but it is a small price to pay for
stopping attackers’ operations.

## 7. Keep your Software Up-to-date

Vulnerabilities are discovered on a regular basis in every operating
system and on all application platforms. Fortunately, once discovered,
they are usually fixed reasonably swiftly. Be sure to keep your system
and software up-to-date and install all vendor patches.

## 8. Peruse your Logs

Your web server will log every request it handles, and will note any
errors that occur. An attempt to attack your web server will likely
leave a rash of error messages in your server logs, and you should
monitor those logs on a regular basis to see how you are being attacked.
Log monitoring can also reveal URLs and operations that initiate attacks
and should be blacklisted.

## 9. Prevent Writing to Document Root

Your website might serve two types of content: content that you put
there, and content that someone else puts there unbeknownst to you. The
Internet-facing network listener that provides access to your
application is its lifeline: without it your application would not be
usable. However, it is also an attack vector. Through specially crafted
HTTP requests, attackers will probe vulnerabilities in your application
and configuration. They will try to upload content or scripts to your
website: if they can get a PHP script uploaded and call it through the
web server, it runs with the same privileges as the web server program
and can likely be used to wreak havoc.

Content Management Systems like Joomla\! and Wordpress allow site
managers to upload and update plug-ins, and in some cases, to configure
the server-side software through a web browser. This makes these
packages really easy to use, but the drawback is that the web server
program must be able to write to the server directories that contain the
software. This capability is easily abused by intruders. Never allow the
web server program to write to directories within the URL space of the
site, where uploaded content can be served and possibly called as
interpreted scripts. Instead, upload your software and plug-ins through
an out-of-band mechanism like SSH or SFTP.

## 10. Use One Time Passwords

Abuse of login credentials is often a more successful attack method than
exploitation of software vulnerabilities. Active Internet users often
have dozens of User IDs and passwords on dozens of different websites.
Expecting users to follow the recommendations concerning password
strength and change frequency is unrealistic.

However, if there is one site for which you use strong passwords that
you change on a regular basis, make it your own website management
interface.

An even better way to protect login credentials is through use of One
Time Passwords. This method lets you calculate a new password for every
time you log into your site management interface, based on a challenge
sent by the site and a PIN that only you know. Programs to calculate One
Time Passwords are available for all operating systems, including smart
phones, so you can carry your OTP generator with you and separate it
from the network interaction with your website. Since an OTP can never
be used twice, this technique makes your website management interface
immune to keystroke loggers and network sniffers: even if your login
information is sniffed or captured, the attackers cannot re-use it.

## Conclusion: Use "The CLUB™"

The CLUB™ is a lock that you put on the steering wheel of your car as
you park it. It is inexpensive and clearly visible to anyone who might
consider breaking into your car and stealing it. In addition, it will
encourage miscreants to skip your car and move on to a car that does not
have a visible security device. Making your own car a less attractive
target is a highly effective security control technique.

There is a clear analogy to the CLUB™ for defending your website: with a
few relatively simple measures like the ones described above, you can
thwart the scripts that automatically scan the web for vulnerable sites,
and have their owners focus their attention on the many, many websites
that lack basic protection.

Of course basic protective measures do not absolve you entirely: your
website is likely up 24/7 and unlike car thieves, who operate in a
public place, web attackers have all the time in the world to quietly
pick away at the locks on your website until they find a weakness they
can exploit. As a webmaster, you need to plug all the holes, but the
villains need to find only one leak. Therefore, your continued vigilance
is required and the tips discussed above should give you a good start.

