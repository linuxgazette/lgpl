# Doomed to Obscurity

**By [Pete Trbovich](../authors/trbovich.html)**

*These images are scaled down to minimize horizontal scrolling.*

[![](misc/doomed/0000327.jpg)  
Click here to see the full-sized image](misc/doomed/0000327.jpg)

[![](misc/doomed/0000328.jpg)  
Click here to see the full-sized image](misc/doomed/0000328.jpg)

[![](misc/doomed/0000329.jpg)  
Click here to see the full-sized image](misc/doomed/0000329.jpg)

[![](misc/doomed/0000330.jpg)  
Click here to see the full-sized image](misc/doomed/0000330.jpg)


All "Doomed to Obscurity" cartoons are at Pete Trbovich's site,
<http://penguinpetes.com/Doomed_to_Obscurity/>.
