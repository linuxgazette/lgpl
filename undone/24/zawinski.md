# xscreensaver

Autor Jamie Zawinski

---

I saw the article on Xlockmore by Larry Ayers in issue 18 of the Linux
Gazette, and I was surprised that it didn't mention, even in passing, my
XScreenSaver program!

Allow me to engage in a bit of advocacy.

Back in 1991, before Xlockmore existed, there was only Xlock. Xlock was
not a screensaver: it was only a locker. There was no way to make it
activate itself automatically when the console became idle, nor was
there any way to avoid having it lock the screen: that is, there was no
way to have it turn off when the mouse moved.

So, I wrote XScreenSaver.

XScreenSaver is superior to Xlockmore in a number of ways. The most
important way, of course, is that it is actually a \*screen saver\*.
Although Xlockmore can be configured to not require a password, it still
doesn't have the ability to turn on when the machine is idle; for that
you have to use an external program that launches and kills it.

The second way in which XScreenSaver is better is that it takes a
server/client approach: the "xscreensaver" program itself knows how to
detect idleness, and to lock the screen. The graphics hacks are not
built in: the beauty of XScreenSaver is that any program which can draw
on the root window can be launched by XScreenSaver for use as a graphics
hack\! This has several benefits:

* You don't have to recompile and reinstall xscreensaver to install a
new graphics hack: all you have to do is change your X resources, and
issue one command.

* Since programs don't have to be written \*specifically\* to run
inside the xscreensaver framework, there are many more potential
graphics hacks available. They don't even need to be written in the same
language: they just have to draw on the root. Thus, it's easier to write
programs to work with XScreenSaver than with Xlock or Xlockmore, because
they don't have to follow a complex set of idiosynchratic rules on how
to structure the code: the only rule is, "draw on the root."

* By separating the task out into two processes, the whole system
becomes more robust: the memory protection provided by the OS serves us
well, in that, if one particular graphics hack has a bug (leaks memory,
corrupts memory, gets a floating-point exception, etc) the integrity of
the screen saver itself is not compromised. The offending hack may exit,
but the screen saver itself is still running, and the screen is still
blackened (or locked.) Also, since a screen saver is, by its nature, a
very- long-running background task, even a small leak would build up
over time. By arranging for the graphics hacks themselves to be
relatively short-running tasks, this problem goes away.

* On some systems, only programs which are running as root can check
passwords. Therefore, on such systems, a screen locker would need to be
a setuid-root program. Obviously one needs to be very careful about what
programs one allows out of the security sandbox; a conscientious
sysadmin would want to examine such a program very carefully before
installing it. The XScreenSaver model allows this, by having the
priveleged part of the program (the saver itself) be small, while
letting the larger part (the graphics hacks) remain unpriveleged.

XScreenSaver also includes a nice Demo Mode that lets the user
interactively experiment with the currently-configured graphics hacks.
(Until recently, most Linux users wouldn't have been able to take
advantage of this, since the code had required Motif; but that is now
configurable, and demo mode works with Athena widgets too -- since
release 1.27 back in January 1997.)

If you have a system with more than one monitor, XScreenSaver can save
them both at once: a different graphics hack will run on each, and if
the two monitors have different depths (for example, if one is
monochrome and the other color) they can be configured to choose their
screenhacks from different lists, so that each monitor is running the
hacks that look best on it.

Most of the xlockmore hacks (the ones that I liked, anyway, including
the GL modes) are included with the XScreenSaver distribution; the only
change made being to extract the various display modes from the
monolithic XLock executable, and turn them into standalone
root-window-oriented programs.

However, it's possible to have XScreenSaver run xlock itself, as just
another one of its many modes -- the best of both worlds\!

Do check it out: the canonical web page is http://people.netscape.com/jwz/xscreensaver
which includes screen shots and descriptions of most of the included
graphics hacks.

The latest version, as of this writing, is 2.12. At last count, it came
with 64 different graphics hacks. Roughly a third of these were written
by me; the others were graciously contributed by others.

And, of course, it's all free, under the usual X-Consortium-style
copyright.

You also might enjoy my philosphical ramblings on the nature of
screensavers, at <http://people.netscape.com/jwz/gruntle/savers.html>.

