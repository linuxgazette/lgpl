# My Linux Revolution

Autor: Ylian Saint-Hilaire, Erik Campo

---

For a long time, I lived in my cave, doing my thing. One day, I got out
of my nice, very functional cave and saw that my fellow man had built a
house just outside. It had all the same functions that my cave had but
was much comfortable and livable (for one, you didn't have to push a
boulder to close the door every night).

Many years later, I got Linux installed on my personal computer, doing
my thing. One day, surfing the net, I saw my fellow man programming a
new version of Linux, which had a great user interface, was easy to
install and possessed much more new added features. For a long time user
of Linux on an i386 like me all Linux was good for was routing packets.
This was a shock.

Of course, the new user interface I am talking about is called the KDE
project, which along with the new Red Hat distribution version 5.0, the
very active www.linux.org site and the many applications available form
an incredible package. To my great astonishment, Linux development is in
full acceleration and is starting to be viewed as a real contender to
Microsoft Windows NT. Not too long ago, people installing Linux on a
computer where viewed as computer gurus, or mystic "roots". With the
arrival of user friendliness, the question I ask myself is: Is Linux
going to become an operating system for the general public?

Some people will feel bad of loosing the respect of being the only ones
able to install Linux in their social group. As opposed to them, I
welcome this new age. I can't wait to install Linux at my grandma's
place. The operating system of the people will finally come back to the
people.

This will, however change many things. If less technically inclined
people jump on the Linux wagon, new demands will be generated for
easier-to-use software and better support and help files. My grandma
will ask for drag-and-drop support and very large fonts. The new people
on the wagon will not be of much help in moving it forward (have you
ever seen your grandma code lately?). Still, they will bring new respect
to the OS, and, well, why not, perhaps new ideas.

Some time ago, word from "Santa Cruz" was that we had to upgrade out of
Linux. This of course was funny and it highlighted the maturity of
Linux. Not only that Linux is free but it compares better (on my
scorecard) to almost any other operating system. And unlike some "other"
operating system, Linux is soon to become a general public operating
system (hello grandma\!).

So, I finally decided I was going to move out of my cave and into I much
more respectable house. But the most important thing of it all, I must
start keeping track of the developments and start pushing the wagon
myself. By the way, Linux makes a great Christmas gift\!

