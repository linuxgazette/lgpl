# Heroes and Friends -- Linux Comes of Age

Autor: Jim Schweizer

---


> I've found only two things that last 'til the end
> One is your heroes, the other's your friends.

-- Randy Travis/Don Schlitz

Could it be that one of the reasons the Linux phenomena is so strong is
that it fulfills the above? Quick, without thinking, name one or two
people you really look up to. Chances are, since you're using Linux, the
names of Torvalds, Raymond or Stallman may have flashed through your
mind.

As members of the Linux community, we have heroes. We have people we can
look up to. We have heroes we can look up to and still disagree with.
Can we say the same of our physical communities, our companies, our
nations?

And what of friends? Think about the mailing lists you belong to, the
news groups you read, and the Linux users group you belong to - who do
you turn to when you need advice about your latest upgrade?

Does commercial software and Microsoft give you the same feeling? Can
they compete with the feeling you just had while thinking about what
Linus has wrought and the last helpful Linux-related email you received?

Community\! That's what this is really all about. It's about having the
best operating system, and the best software and the best support. It's
about having the best. Period. And we know the best is still to come.

The question is often asked, "Will Linux be able to defeat the marketing
muscle of Microsoft?" We already know the answer. And the answer is
being provided by the growing number of people who use Linux as an
everyday solution to their own needs.

Will there be an 'office suite'? Probably. But that's not what brought
us to Linux in the first place, is it? So, why are you here?

What makes Linux really special is the people you never hear about in
the press. The people who patch software and give it back to the
community - you all know someone who's done this, or helped you with a
shell script, or guided you as you learned more about Linux. You also
know someone who is maintaining a Linux site, writing a driver or
volunteering in some way to bring Linux to fruition. Linux is what it is
because thousands of people, every day, contribute in small ways to
Linux's success.

Heroes help you see a goal worth attaining. Your friends help you get
there. When someone new to Linux asks a question, what they are really
asking for is a friend's advice. Be there for them.

So, the next time some one asks you why you are using Linux, smile and
think, "That's how it goes, with heroes and friends."

