# The Foolish Things We Do With Our Computers

Autor: Mike "Iron" Orr

## Genericide

[Adrian Watson](mailto:adrian.watson@bg-group.com)

I wondered if you'd like to include this in your series. This is a true
story of a not too techie mate of mine. He had two Windows machines that
he had networked, and they had stopped working. I went to see if I could
fix the problem for him.

I checked and re-inserted the network cards but the machines still had
trouble talking to each other. I used the 'Control Panel' to display a
list of the drivers present, thinking that perhaps they were using
different protocols. My mate was keenly watching everything I did. Then
he asked me what 'genericide' means?

Totally baffled, I followed his eyes to the screen. He was looking at
the Hard Disk Driver: "Generic IDE".

I told him it was when a drive crashes.

P.S. I found out later why his network cards wouldn't talk to each
other. He'd run the cable across the floor and used to roll his office
chair across the cable to access whichever machine he needed at the
time. The co-ax gave up after a few weeks of that treatment.

-----

## Peripheral abuse

[Karl Lattimer](mailto:k.lattimer@nnc-consultancy.co.uk)

The stupidest thing I've ever done with a computer, I havn't done once,
but repeatedly. If it weren't for these idiotic mishaps I would have an
AMD Athlon XP 2.2 by now, but alas my Duron will suffice....

### Weakness 1: the keyboard

It started with my first keyboard about 3 years back now. I had my
drinking buddy over and we were down to our last glass of wine after the
beer, sherry, gin, vodka, etc, had all ran out. In a scooner, because we
were short on real glasses. I sat the glass near my poor helpless
keyboard while I continued to talk a slurred form of cod and
gesticulated rapidly, \<chingggggg\!\> went the glass, and my keyboard
started sparking. (It's true what they say, water don't do nothing, but
coke, wine, beer etc make sparks.) The problem was, I had a college
assignment in the next day and, well, I was quite distraught.

My next keyboard was donated by a friend, but with a PS2 connector
instead of an AT. so I removed the cable from my dead keyboard and
swapped it around (4 pins, 16 combinations, eight nails and some
particle board), and voila\! The keyboard was complete and working\! It
looked better without the plastic cover on it (an old one with a black
metal lump and an exposed circuit board), so I left it as is.

The next incident happened about 2 years later. Stupidly I was sitting
at my computer, on the back rest, not the seat, while talking on IRC
with one hand and untying my shoe laces with the other. I wear Doc
Martins, 10 hole. While slipping the first boot off I tipped, and so did
the chair. My other boot left the chair, kicked my desk, and the
keyboard flew about 8 feet from the computer, along with the coffee and
the ash tray, and all three hit the back wall. The keyboard was not
destroyed, however. It continued to work for another week, then a
cascade effect emanating from the Print Screen key finally caused its
death.

A fair few keyboards in between, I now use an M$ natural, and my RSI has
since cleared up, I'm looking after this keyboard carefully after way
too many accidents\!\!\!

**Update 31-Oct-2002:** Just as this article was going to press, I
spilled water on my natural keyboard and it survived\!\!\!\! I havn't
had it apart yet because I'm scared of the mini Bill Gates that sits
inside its huge posterior. I havn't had any real problems, apart from
once when the lights flickered and the up arrow seems to cease up every
once in a while. Normally while playing MOHAA or AVP2 as a LAN
game\!\!\!\!

## Weakness 2: the floppy drive

In the summer of 1999, I had a grand total of six floppy drives within
four months, each costing between 5 and 10 pounds. The reason they died
was, in three instances I connected the drive the wrong way round.
Because they were cheap drives, the light didn't come on and stay on as
you would expect. No, instead the drive fizzled and smelled bad. One
drive died due to chocolate abuse. (A friend of mine gets chocolate on
everything, and a disk was his victim.) Another drive perished from beer
abuse, and the final drive I can't remember....

