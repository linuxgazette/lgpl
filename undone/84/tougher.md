# Debian APT Part 1: Basic Commands

Autor: Rob Tougher

--- 

## Introduction

I use the [Debian GNU/Linux](http://www.debian.org) operating system. I
use Debian for the following reasons:

* It comes with a lot of [great software](http://packages.debian.org/stable/)
* The software is simple to install

I install Debian software using APT, the Advanced Packaging Tool. With
APT I can install Debian software from either a CD-ROM, an FTP server,
an HTTP server, or my local filesystem. What's even better is that
Debian provides their software archives on [freely-accessible FTP and
HTTP servers](http://www.debian.org/mirror/list). Installation is as
simple as pointing APT at one of these servers and telling it to run.

The purpose of this article is to give you an overview of APT and
describe the basic commands for using it. My hope is that this article
will show you how easy software installation can be using APT.


## Overview

The main concept in APT is the *package*. Every software application
that Debian provides has a corresponding package. A package is a file
with a `*.deb` extension and includes these items:

* a software application
* a description of the application
* a list of dependencies for the application
* the installation scripts for the application
* the application's user documentation

All operations in APT deal with packages. When you want to install a
software application, you tell APT to install that application's
package. When you want to remove a software application, you tell APT to
remove that application's package. Even if you only want information
about the software application, you need to ask APT for the information
using the application's package name.

Another important concept is the *package cache*. The package cache is a
complete list of available packages in the Debian distribution. The
package cache is stored on your local machine, and you are responsible
for keeping it updated. A later section deals with updating your package
cache.

## Using APT

### Initial Setup

Before you can use APT you have to tell it where to look for package
files. Each location you specify is called a *source*. A source can be a
CD-ROM, an HTTP server, an FTP server, or an archive on your hard drive.

APT looks in `/etc/apt/sources.list` for your list of sources. The
following is my `sources.list` file:

```
deb http://security.debian.org/ stable/updates main
deb http://http.us.debian.org/debian stable main contrib non-free
deb cdrom:[Debian GNU/Linux 3.0 r0 _Woody_ - Official i386 Binary-6 (20020718)]/ unstable contrib main non-US/contrib non-US/main
deb cdrom:[Debian GNU/Linux 3.0 r0 _Woody_ - Official i386 Binary-7 (20020718)]/ unstable contrib main non-US/contrib non-US/main
deb cdrom:[Debian GNU/Linux 3.0 r0 _Woody_ - Official i386 Binary-5 (20020718)]/ unstable contrib main non-US/contrib non-US/main
deb cdrom:[Debian GNU/Linux 3.0 r0 _Woody_ - Official i386 Binary-4 (20020718)]/ unstable contrib main non-US/contrib non-US/main
deb cdrom:[Debian GNU/Linux 3.0 r0 _Woody_ - Official i386 Binary-3 (20020718)]/ unstable contrib main non-US/contrib non-US/main
deb cdrom:[Debian GNU/Linux 3.0 r0 _Woody_ - Official i386 Binary-2 (20020718)]/ unstable contrib main non-US/contrib non-US/main
deb cdrom:[Debian GNU/Linux 3.0 r0 _Woody_ - Official i386 Binary-1 (20020718)]/ unstable contrib main non-US/contrib non-US/main
```

Adding HTTP, FTP, and local file archives to `sources.list` is simple -
you can add the entries into the file using a text editor (Debian keeps
a list of mirrors on their [web
site](http://www.debian.org/mirror/list)). To add CD-ROMs you have to
use the `apt-cdrom` command:

```
apt-cdrom add
```

### Updating your local package cache

You are responsible for keeping your local package cache updated. Every
time you want to install or upgrade a software application, you should
update your package cache first. This ensures that you have the latest
information about the software you are installing.

Run this command to update your package cache:

```
apt-get update
```

### Viewing the available packages

The apt-cache utility allows you to search the local package cache for
packages with certain text in it.

For example, you might want to install the Apache HTTP server on your
machine. You could search for the Apache package using the following
command:

```
apt-cache search apache
```

This command returns 119 packages on my machine. Instead of reading
through all of the entries, you could filter the output using `grep`:

```
apt-cache search apache | grep "^apache"
```

This command generates the following:

```
apache-doc - Apache web server docs
apache-dev - Apache web server development kit
apache-ssl - Versatile, high-performance HTTP server with SSL support
apache-common - Support files for all Apache web servers
apache - Versatile, high-performance HTTP server
apache-perl - Versatile, high-performance HTTP server with added Perl support
```

### Viewing the information for a single package

After having searched for a package, you might want to view the
information for that package. The `apt-cache` utility takes a `show`
command, like the following:

```
apt-cache show packagename
```

For example, If you wanted to view the information for the `apache`
package, you would type the following at a command prompt:

```
apt-cache show apache
```

This returns the following:

```
Package: apache
Priority: optional
Section: web
Installed-Size: 748
Maintainer: Matthew Wilcox <willy@debian.org>
Architecture: i386
Version: 1.3.26-0woody1
Replaces: apache-modules
Provides: httpd
Depends: libc6 (>= 2.2.4-4), libdb2 (>= 2:2.7.7.0-7), libexpat1 (>= 1.95.2-6), mime-support, apache-common (>= 1.3.26-0), apache-common (<< 1.3.27-0), perl5 | perl, logrotate (>= 3.5.4-1), dpkg (>> 1.9.0)
Suggests: apache-doc
Conflicts: apache-modules, libapache-mod-perl (<= 1.17-1), jserv (<= 1.1-3)
Filename: pool/main/a/apache/apache_1.3.26-0woody1_i386.deb
Size: 352814
MD5Sum: 728257f5de8d71e0d00701bdca9d452d
Description: Versatile, high-performance HTTP server
 The most popular server in the world, Apache features a modular
 design and supports dynamic selection of extension modules at runtime.
 Some of its strong points are its range of possible customization,
 dynamic adjustment of the number of server processes, and a whole
 range of available modules including many authentication mechanisms,
 server-parsed HTML, server-side includes, access control, CERN httpd
 metafiles emulation, proxy caching, etc.  Apache also supports multiple
 virtual homing.

 Separate Debian packages are available for PHP3, mod_perl, Java
 Servlet support, Apache-SSL, and other common extensions.  More
 information is available at http://www.apache.org/.
Task: web-server
```

### Installing a package

Installing a package is achieved using the `apt-get` utility. The
following will install a package:

```
apt-get install packagename
```

Typing the following at a command prompt would install the Apache HTTP
Server on your machine:

```
apt-get install apache
```

### Removing a package

You can remove packages from your machine with the following command:

```
apt-get remove packagename
```

For example, if you wanted to remove the Apache HTTP Server from your
machine, you would use the following:

```
apt-get remove apache
```

### Keeping your system updated

It's good to keep your system updated with the latest (stable) versions
of software. Doing this using APT is a simple two-step operation. You
have to do only the following:

```
apt-get update
apt-get upgrade
```

The first command updates your local package cache (we saw this in a
previous section). The second command upgrades any packages installed on
your machine that have newer versions available.


## Conclusion

In this article I described the basic usage of APT. You should now be
able to use APT to install Debian software on your machine.


## References

* [Debian documentation](http://www.debian.org/doc/)
* man pages for apt-get, apt-cache, and sources.list

