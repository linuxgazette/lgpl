# Sending and Receiving SMS from your Linux Computer

Autor: Suramya Tomar

---

What is SMS? SMS stands for Short Message Service and is used to send
text messages from one GSM enabled cell phone to another. But sometimes
you might want to send an SMS message using your computer. Now there are
three ways that you can send an SMS from a computer:

1.  Use a SMS gateway service that allows your computer to connect to it
    using an API and send messages. There are a lot of these services
    available on the web but most are not free and require subscriptions
2.  Use a cell-phone/GPRS Modem connected to the computer.
3.  Some service providers allow you to send SMS to a number by sending
    an email to a specifically formated email address.

I choose to use the second option because my provider doesn't support
the last option and I think its cheaper than option one. All you need
for this to work is an extra phone and a cheap phone plan that allows
you to send and receive SMS's. In my case I had an old Sony Ericsson
T610 lying in my desk gathering dust so I got a pre-paid SIM card and I
was ready to proceed.

## Pre-Installation Steps

The first thing you need is to connect your cellphone to your computer.
I recommend using Bluetooth for it because that way there is one less
cable you have to worry about on your desk but you can use a USB cable
to connect if your phone doesn't support Bluetooth.

If you are using Bluetooth to connect then you will need to Pair the
phone and the computer and configure the phone to allow your computer to
connect to it without authorization. The steps for this differ from
phone to phone so you will have to check the manual for the exact steps
to follow.

Once we have the phone configured to auto-accept connections we are
ready to proceed. Now in order to allow your computer to talk to your
phone you will have to install a modem driver for mobile phones on your
computer called gnokii. The steps for this are explained below

## Installing gnokii

To install gnokii on a Debian system run the following command as root:

```sh
apt-get install gnokii
```

This will download and install gnokii on your computer. Most of the
other distributions should also have an installer package available for
download but in-case your distribution doesn't then you can always
compile and install it from source.

To install from source you will need to download the latest version of
gnokii from the project website(
<http://www.gnokii.org/download/gnokii/>).

Once we have the latest version of the source we can install it on our
system. To do that we must first uncompress the archive so issue the
following command:

```sh
tar -zxf gnokii-0.6.27.tar.gz
```

Once tar finishes uncompressing the archive change to the gnokii
directory using the following command:

```sh
cd gnokii-0.6.27/
```

Now if you are connecting your phone to your computer using Bluetooth
you will need to install the libbluetooth-dev library first by issuing
the following command as root:

```sh
apt-get install libbluetooth-dev
```

Once the library is installed you can configure the gnokii installer
using the following command:

```sh
./configure --with-bluetooth=/usr/include/bluetooth
```

If you are not using Bluetooth then you will have to use the following
command instead:

```sh
./configure 
```

Once the configuration is complete and you don’t see any errors compile
gnokii by running the following command:

```sh
make
```

If the compile finishes without errors install gnokii by running the
following command as root:

```sh
make install
```

After gnokii finishes installing you will have to configure it before
you can start using it.

## Configuring gnokii

A sample configuration file for gnokii is provided in the source file
under the *Docs/sample/* subdirectory in the directory where you
extracted the gnokii source. If you installed gnokii using apt-get then
the sample file is installed at */etc/gnokiirc*

The easiest way to configure gnokii is to modify the sample
configuration file so we start by copying the sample configuration to
your home directory using the following command:

```sh
cp gnokii-0.6.27/Docs/sample/gnokiirc ~/.gnokiirc
```

and open it in your favorite text editor for editing. The first thing
you need to do is tell gnokii what phone you are using by setting the
'model' variable according to the matrix below:

* Set model = series40, if you have Nokia non-Symbian Series60 phone
* Set model = gnapplet, if you have Nokia Symbian Series60 prior to
    3rd Edition phone
* Set model = AT, for all other phones.

In my case I set it to AT.

Next, you need to tell gnokii how your phone is connected to the
computer. For this you will have to change the 'connection', 'port' and
'rfcomm\_channel' variables.

If you are using Bluetooth to connect then set the connection variable
to 'bluetooth' and the port variable to your phone's Bluetooth address
as shown below:

```sh
connection = bluetooth
port = 00:1e:3a:7e:a5:1f
```

According to the manual the 'rfcomm\_channel' can be omitted as its
auto-detected but I had to explicitly set it to get gnokii to connect.

Most of the phones use rfcomm\_channel=1 but for my Nokia N95 8GB I had
to set it to 2 while the Sony Ericsson T610 used rfcomm\_channel=1. If
you are not sure then you can use '`sdptool search SP'` or '`sdptool
search DUN'` or '`sdptool browse'` to find the right channel for your
phone.

If you are using USB to connect then the connection name depends on the
type of cable you are using and the phone model. Some values you can try
are 'connection = serial' to use the AT protocol and 'connection =
dku2libusb' to use the FBUS protocol (if supported by cable and phone).

The port name also depends on the type of the cable: some cables create
a /dev/ttyACM0, others create a /dev/ttyUSB0. When using connection =
dku2libusb, port = 1 chooses the first phone, port = 2 the second, and
so on. See /var/log/messages to identify the port the phone connects on.

After you make the changes and save the file you are ready to test the
connection.

## Testing gnokii connection to phone

At a command prompt run the following command:

```sh
gnokii --identify
```

If you have configured everything correctly then you will see an output
listing your phone model like the following:

```sh
suramya@Wyrm:~$ gnokii --identify
GNOKII Version 0.6.9
IMEI         : 35954XXXXXXXXXX
Manufacturer : Nokia
Model        : Nokia N95 8GB
Revision     : V06wk41v46.1
```

If you don't see this output then gnokii wasn't able to connect to your
phone. Make sure that your phone is accepting bluetooth connections, you
have the correct model specified and that you are trying to connect to
the correct address using the correct protocol.

If you still are not able to connect then you should enable the debug
mode in gnokii by setting `'debug = on'` (Without the quotes) in
`~/.gnokiirc`. Looking at the debug output will let you identify the
problem and rectify it.

## Sending an SMS from your computer

Once you have configured gnokii correctly and get the above output you
are ready to send SMS's from your computer. The command to send an SMS
is:

```sh
gnokii --sendsms destination_number
```

This command will take the SMS content from the standard input. For
example the transcript of sending a SMS to 555-123-4567 will look
something like this:

```sh
suramya@Wyrm:~$ gnokii --sendsms  5551234567
GNOKII Version 0.6.9
Please enter SMS text.
End your input with <cr><control-D>:This is a test message

Send succeeded!
suramya@Wyrm:~$
```

gnokii has loads of other functionality. Use gnokii --help to get a list
of parameters and functionality.

Now that you have the connection working you are only limited by your
imagination as to how you use this. I built a gateway that allows me to
send an SMS by sending an email to a specific email address. This
program is described in more detail below.

## Sending SMS using specially formated Emails

To send an SMS when I receive a specifically formated email I needed to
write a program that checks for new emails and then processes them. I
used Perl for this as its great for text processing and I had already
written part of the code for another project (sending/receiving IM's as
email.)

The first thing I needed was a new email account that I could access
using POP3. Since I have my own domain I logged on to my domains admin
section and created a new email address but you can use gmail if you
want.

In my code I assume that the phone \# to which I am sending an SMS to is
in the subject of the email sent and the body of the SMS is the actual
body of the email.

The code to connect and download the email is simple:

```sh
$pop3 = Net::POP3->new($ServerName);                # Connect to email server
   die "Couldn't log on to server" unless $pop3;

$Num_Messages = $pop3->login($UserName, $Password); # Login to the server and get No of messages
  die "Couldn't log on to server" unless $pop3;

$Messages = $pop3->list();                          # Get message list
 
foreach $msg_id (keys(%$Messages))
{
    $MsgContent = $pop3->top($msg_id, 40);
    ProcessEmail(@$MsgContent);
    $pop3->delete ($msg_id);
}
```

In the code above we connect to the email server, download the list of
new emails and then download each email in a loop, process them and then
delete the message.

All the processing work is done in the ProcessEmail(). In this function
we first extract the 'From' name from the email (this is appended to the
SMS so that the recipient knows who sent it).

```sh
my (@lines) = @_;
my $body_start =FALSE;
$sms_body = "";

# Declare local variables
my ($from, $line, $sms_to  );

# Check each line in the header
foreach $line (@lines)
{
if($line =~ m/^From: (.*)/)
{
   # We found the "From" field, so let's get what we need
   $from = $1;
   $from =~ s/"|<.*>//g;
   $from = substr($from, 0, 39);                # This gives us the 'From' Name
}
```

Once we have the 'From' field we extract the subject from the email and
make sure its a phone number (This number can be prefixed with a + for
country codes). If its not a number then we ignore the email and abort.
The code for this is shown below:

```sh
elsif( $line =~ m/^Subject: (.*)/)
{
   # We found the "Subject" field. This contains the No to send the SMS to.

   $sms_to   = $1;
   $sms_to = substr($sms_to, 0, 29);

   if ($sms_to =~ /^[+]?\d+$/ )   # here we check if the subject is a no. If so we proceed.
   {
       print "Got email. Subject is a number. Processing further\n";
   }
   else                           # Otherwise we delete the message and ignore it.
   {
        print "Got email. Subject is NOT a number. Ignoring it. \n";
        return;
   }
}
```

Finally we extract the body from the email. This becomes the body of the
SMS message

```sh
# This is the last line in the email header; after this the body starts
elsif(( $line =~ m/^Envelope-To:/)||($body_start eq TRUE)) 
{ 
   if($body_start ne FALSE)
   {
       $sms_body = $sms_body . $line;
   }

   $body_start=TRUE;
}
```

Now we have all the required components for sending the SMS: Destination
Number, SMS Body and the From address so we are ready to send the SMS.
However since we can only send a max of 160 chars in one message we need
to make sure that the SMS body is broken down into 160 char chunks
before we can send the SMS

```sh
my @stringChunksArray = ($sms_body =~ m/(.{1,160})/gs);
for($i=0;$i<@stringChunksArray;$i++)
{
# Start gnokii and wait for the SMS body
open(GNOKII, "| gnokii --sendsms $sms_to") || die "Error starting gnokii failed: $!\n";
print GNOKII $stringChunksArray[$i];      # Print the SMS body in 160 Char chunks
close(GNOKII);
# This is there so that the phone gets time to reset after each message;
# otherwise the send fails
sleep(10);
}
```

The complete source code for Email2SMS is available for download
[here](misc/tomar/Email2SMS.pl).

## Receiving SMS messages on your computer

The ability to receive messages on your computer depends on the phone
you are using as not all phones are completely supported by gnokii.
According to the project website most Nokia phones from the 3110/3180,
5110/6110, 7110/6210 and 6510/6310 series are supported well.
Unfortunately the AT support in the Symbian OS is very limited so very
basic functionality is supported on these phones. More details on
supported phone models is available at the [project
site](http://wiki.gnokii.org/index.php/Config).

To my sorrow I was unable to read SMS messages from both my Nokia N95
and T610 phones using gnokii. I only managed to get the system to send
SMS messages from these phones.

If your phone is supported then you can read a SMS message on the phone
by issuing the following command:

```sh
gnokii --getsms MemoryType Start [end]
```

Memory type tells gnokii where to read the SMS from. Most phones support
only a limited subset of memory types. To get a list of supported memory
types use the following command:

```sh
gnokii --showsmsfolderstatus
```

Some Possible values are:

* ME: Internal Phone memory
* SM: SIM card
* MT: Combined ME and SIM phonebook
* CB: Currently selected memory

To read all the SMS's on the phones internal memory to the standard
display issue the following command:

```sh
gnokii --getsms ME 1 end
```

Another interesting option for reading SMS is --smsreader which will
wait for incoming messages and saves them into a mailbox under
/tmp/sms/\* (actual filename varies). In this case the messages are
never saved on the phone.

## Conclusions

Now that you have your phone connected to your computer you can have
your computer SMS you when a certain criteria like a particular incoming
email, or when you have an important appointment coming up. You can have
it wait for a message and then initiate a call to a specific number and
play a wave file to the phone. All you are limited by now is your
imagination and the capabilities of your phone.

I am interested in hearing about any projects that you might build based
on this article so if you have an interesting project idea do contact
me.

