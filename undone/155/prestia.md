# Joey's Notes: Using the Automounter

Autor: Joey Prestia

---

In a Windows networked environment, you can go to any machine, log in to
the system, and have access to your home directories and files on the
server. This seems like a convenience until you realize that in doing
this, the server and the client machines are eating up resources by
keeping all network shares hard-mounted at all times. The Linux solution
to this is the autofs daemon known as the automounter.

In the past, Linux networks would have various types of Network File
System (NFS) servers being utilized in different ways: some for network
backup, some exporting users' Network Information Service (NIS) mapped
directories, and some exporting other network resources. One of the
problems was that booting a system that used traditional fstab-based
mounts while the NFS server was down could be a long process: it would
take a long time to come up as it waited for the server to time out.
Similarly, when you have two servers, each mounting directory
hierarchies from the other, and both systems are down, both may hang as
they are brought up (this is called *server-server dependency*). The
automount facility gets around these issues by mounting a directory
hierarchy from another system only when a process tries to access it.

<span id="prestia.html_1_back"></span>[\[1\]](#prestia.html_1) If a
client attempts a hard mount and the server is unavailable, then the
Linux kernel treats the process as it would if a local drive's file
system was unavailable. The user can do nothing until the server becomes
available; the process is in an un-interruptible sleep until the server
is back on line. Often, the process can't even be killed, and a "ps aux"
command will reveal a 'D' state.

\[ For anyone not familiar with "ps" output, 'D' means "Uninterruptible
sleep (usually IO)". For more information, please see the "PROCESS STATE
CODES" section in "man ps". -- Ben \]

Automounting is the process where the mounting and unmounting of file
systems is done automatically by the autofs service. If the filesystem
is unmounted, and a user attempts to access it, it will be automatically
mounted or re-mounted. Using the automounter saves on resources by
mounting a directory only when you need to enter it, then unmounting it
automatically when you don't need it anymore. This is usually done after
a default timeout (usually 300 seconds) that is specified in the
/etc/sysconfig/autofs file, the main configuration file for this
service. The other configuration files are located in the /etc directory
and are called auto.master, auto.misc, auto.net, and auto.smb. The last
two are executable scripts.

Let's say we have some NFS directories and our company's employees are
only trained in use of the GNOME desktop environment; they could use the
automounter if you were to set up the configuration files and copy them
to their machines. They would be able to point and click their way
around, and the remote file systems would do the right thing, invisibly.
Using the automounter is not restricted to the console.

Procedure:

1.  Verify that /etc/sysconfig/autofs options will accommodate your
    particular needs (i.e., browse mode and timeouts)
2.  Modify /etc/auto.master as appropriate
3.  Create any /etc/auto.*file*s that were referenced in
    /etc/auto.master
4.  Start or restart the autofs service
5.  Change into the appropriate directory

## /etc/sysconfig/autofs

Let's check out the configuration files. (**Note:** this guide was
written using RHEL 5.1, so other distributions may differ slightly in
some aspects.) The main configuration file, "/etc/sysconfig/autofs", has
the timeout for mounts defined as 300 seconds (or five minutes) of
inactivity, and the default browse mode set to "no"; this prevents
shares that are not being directly referenced from being automounted.
There are lots of other configurable options for different setups, so it
is worth your time to read through this file just so you know what is in
there. Look particularly closely at any uncommented directives.

```
DEFAULT_TIMEOUT=300
DEFAULT_BROWSE_MODE="no"
```

## /etc/auto.master

'auto.master', contains three uncommented lines:

```
/misc   /etc/auto.misc
/net    -hosts
+auto.master
```

The first line means that the configured devices in auto.misc will be
mounted in the "/misc" directory; the second one means that the exports
from the network hosts will be mounted in "/net". '+auto.master' is a
reference to an external NIS or NIS+ master map. Entries in this file
consist of a mount point followed by the path to a file which defines
the details and the options of what will be mounted where. You can call
the files anything you want, or whatever suits the need. The following
entries would be valid:

```sh
/remote             /etc/auto.somewhere_else
/mnt/nas            /etc/auto.nas
/mnt/backup         /etc/auto.backup
```

We would then create the file that we referenced in the auto.master and
define the mount details in it.

It is also useful to consider mounting the users' remote home
directories in a non-standard location (e.g., "/rhome/username" or
"/export/home/username".) This would be considered a violation of the
FHS (Filesystem Hierarchy Standard), since the mount point is located in
the root of the filesystem; on the other hand, it's only a temporary
directory, created and destroyed by the autofs daemon as necessary. The
advantages are obvious: you get to have access to the content of your
"/home/username" directory as well as the remote one, and you get to
keep the use of the /mnt mount point for other devices or shares.

## /etc/auto.*file*

This is the file that you create for your custom automounts. The
auto.*file*s would have three columns in them:

```
    key      mount options        location or device
```

The "key" is the subdirectory under the mount point that you listed in
the auto.master file. As an example, if in our auto.master we had the
following:

```sh
/misc               /etc/auto.misc
/net                -hosts
/mnt/remote         /etc/auto.remote
+auto.master
```

We would then create, for example, the /etc/auto.remote file with our
desired options:

```sh
# key       mount options       location or device 
records     -rw,soft,intr       server1.example.com:/srv/nfs/medical/records
```

Once we restart the autofs service, changing to the /mnt/remote/records
directory will create the demand for the exported resource
server1.example.com:/srv/nfs/medical/records to be mounted on
/mnt/remote/records. It is important to note that if you use host names
(rather than IPs) and don't have DNS available, you will need an entry
in /etc/hosts that maps the hostname to the IP address.

The following example uses wildcard substitution. Placing an asterisk in
the key field and an ampersand at the end of the location or device path
field will map the end of the location path (directory structure and
files) to whatever is found at the location. In the example below, the
entire remote home directory structure located on
server1.example.com:/home/username would be recreated when the user
attempts to log in. By using this technique, a user could log into any
machine and have their home directory mapped appropriately.

```sh
# key       mount options       location or device 
*       -rw,soft,intr       server1.example.com:/home/username/&
```

## /etc/auto.misc

The /etc/auto.misc file contains a lot of sample configurations for
automounting various types of devices; to use one of these, merely
uncomment a specific line in this file and adjust the appropriate device
accordingly. But just as different machines have different hardware
configurations, different distros may have different setups so a little
customization will probably be necessary.

```sh
# This is an automounter map and it has the following format
# key [ -mount-options-separated-by-comma ] location
# Details may be found in the autofs(5) manpage

cd              -fstype=iso9660,ro,nosuid,nodev :/dev/cdrom

# the following entries are samples to pique your imagination
#linux          -ro,soft,intr           ftp.example.org:/pub/linux
#boot           -fstype=ext2            :/dev/hda1
#floppy         -fstype=auto            :/dev/fd0
#floppy         -fstype=ext2            :/dev/fd0
#e2floppy       -fstype=ext2            :/dev/fd0
#jaz            -fstype=ext2            :/dev/sdc1
#removable      -fstype=ext2            :/dev/hdd
```

Most distros now automatically mount USB devices under the /media
directory and several of the devices listed above are fading away - but
there is still some use for this file. It can be configured in a
multitude of ways, e.g. to automount partitions that you don't need to
have mounted at all times (backup devices are just one example). A good
way to get familiar with this file is to come up with a configuration
scenario and put it into practice. For example, try mounting a USB flash
drive with the automounter and setting up different configurations until
you get familiar with it. Remember to always back up the original file
before making any changes.

Suppose we want to be able to automount the NFS share on the network
server instead of, say, manually typing 
`mount -t nfs 192.168.0.254:/var/ftp/pub/directory/subdirectory /mnt`.) 
In this case, we would just need the hostname of the machine with the exported
filesystem (if there is an entry in DNS) or the IP address of the
machine. The "showmount -e hostname" or "showmount -e IP\_address"
command can be used to discover what directories a host has exported by
NFS. Example: "showmount -e 192.168.0.254" will display the exports for
that host.

## /etc/auto.net

The auto.net file is an executable script which searches for available
NFS shares to mount; the NFS service must be running for the auto.net
script to be able to connect to remote shares. To invoke this ability,
you need to ensure that the autofs and NFS services are running:

```sh
[root@station17 ~]# service autofs status
automount is stopped
[root@station17 ~]# service nfs status
rpc.mountd is stopped
nfsd is stopped
rpc.rquotad is stopped
```

Since they are stopped, we start the service with the "service" command,
and ensure that they stay on through a reboot by using the "chkconfig"
command on Red Hat based systems.

```sh
[root@station17 ~]# service autofs start
Starting automount:                                        [  OK  ]
[root@station17 ~]# chkconfig --level 35 autofs on
[root@station17 ~]# service nfs start
Starting NFS services:                                     [  OK  ]
Starting NFS quotas:                                       [  OK  ]
Starting NFS daemon:                                       [  OK  ]
Starting NFS mountd:                                       [  OK  ]
[root@station17 ~]# chkconfig --level 35 nfs on
[root@station17 ~]#
```

The "chkconfig" arguments just ensure that this service will remain on
through a reboot in runlevels 3 and 5. Now we can see if any network
server exports are available. First, we'll go to /net; assuming that our
server with the NFS export is "server1" and has a DNS entry, we could
execute the following:

```sh
[root@station17 /]# cd /net
[root@station17 net]# cd server1
[root@station17 server1]# ls
data  documents  powervault  rhome  var
[root@station17 server1]#             
```

or, if we know the IP address of the machine, this would work:

```sh
[root@station17 net]# cd 192.168.0.254
[root@station17 192.168.0.254]# ls
data  documents  powervault  remote  var
[root@station17 192.168.0.254]#
```

The directory magically appears - but if we didn't know the machine's
hostname or IP address, we would not be able to find it. The automounter
does not list directories unless they have been accessed, so unless a
user knows what to access they will not be able to blindly find it. This
sure makes mounting NFS shares a lot easier - and you don't have to
remember to manually unmount them\!

As an exercise to see this work on our machine, let's execute the
following commands.

```sh
[root@station17 ~]# echo "/var/www/html 127.0.0.1(sync,rw)" >> /etc/exports
[root@station17 ~]# service nfs restart
Shutting down NFS mountd:                                  [  OK  ]
Shutting down NFS daemon:                                  [  OK  ]
Shutting down NFS quotas:                                  [  OK  ]
Shutting down NFS services:                                [  OK  ]
Starting NFS services:                                     [  OK  ]
Starting NFS quotas:                                       [  OK  ]
Starting NFS daemon:                                       [  OK  ]
Starting NFS mountd:                                       [  OK  ]
[root@station17 ~]# cd /net/localhost
[root@station17 localhost]# ls
var
[root@station17 localhost]# 
```

We've exported our /var/www/html directory as a read/write NFS export to
our loopback IP address just to try this (in case you're not in a
networked environment). Don't forget to remove the line in /etc/exports
that we echoed into the file when you are done checking out the network
mounting capabilities. Do note that typing "mount" at the command line
will not show what is mounted by the automounter: you will have to
execute a "cat /proc/mounts" to see what is currently mounted by the
autofs daemon.

## /etc/auto.smb

The default auto.smb file is also an executable script, and the use of
it may require the Samba servername/username/password, or that a
credentials file be specified for your particular setup. You can also
use the guide included with your Samba documentation, as I did, to
create your own auto.smb file; it follows the same format as the
auto.misc file. If you want to automount Samba shares, you may want to
just go ahead and create your own configuration file customized for your
own needs. Just remember to back up the original file if you're not
going to use the executable.

```sh
# automount points below /smb

# This is an automounter map and it has the following format
# key [ -mount-options-separated-by-comma ] location
# Details may be found in the autofs(5) manpage

# smb-servers
supra_andreas   -fstype=smb,username=andreas,password=foo   ://supra/aheinrich
supra_cspiel    -fstype=smb,username=cspiel                 ://supra/cspiel
phonon_andreas  -fstype=smb,username=andreas                ://phonon/andreas
helium_cspiel   -fstype=smb,username=cspiel                 ://helium/cspiel
```

The automounter will work with NFS exports, Samba, NIS, NIS+, LDAP, and
various other devices, not to mention being scriptable and very
configurable. In this article, I've just touched on the basics very
briefly; don't forget to read the autofs(5) man page, since it has some
interesting concepts. Also, be sure to consult it when setting up
automounting, as well as any other distro-specific documentation when
configuring the automounter to meet your needs.

## Resources

* [The Filesystem Hierarchy Standard](http://www.pathname.com/fhs/)
* [Red Hat
    Documentation](https://www.redhat.com/docs/manuals/enterprise/)
* Michael Jang's book "Red Hat Certified Engineer Linux Study Guide",
    Fifth Edition
* Mark G. Sobell's book "A Practical Guide to Red Hat Linux" Third
    Edition

<span id="prestia.html_1"></span>[\[1\]](#prestia.html_1_back) Taken
from Mark G. Sobell's book "A Practical Guide to Red Hat Linux" Third
Edition, page 691

