# Linux User Caricatures

Autor: Franck Alcidi

---

Due to popular demand, I created a
[Slackware](http://www.slackware.org/) geek caricature as well as a Red
Flag geek caricature. The Slackware character comes across to me as
being the very cool, confident Linux hacker. If you know Slackware, bets
are you know Linux inside and out ;-)

![Slackware geek cartoon](misc/alcidi/slackware_geek.jpg)

 
The [Red Flag](http://www.redflag-linux.com/eindex.html) geek caricature
comes from Asia. Being a Linux distribution developed in China it was
pretty clear cut how this fellow was going to look (well to me anyway).
Lets hope this distribution continues to grow and place a bit of
pressure on MS. I'm sure this particular distro is going to be very
popular amongst our asian buddies.

![Red Flag geek cartoon](misc/alcidi/redflag_geek.jpg)

My previous LG cartoons: [issue72](../issue72/alcidi.html)
[issue73](../issue73/alcidi.html) [issue76](../issue76/alcidi.html)

**Important -** You can view my other artwork and sketches on my **new**
[website](http://www.artsolute.net).

