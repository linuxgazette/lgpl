# Linux Basics

Autor: Paul Anderson

---

I was reading through the Linux Gazette Mailbag column, and noticed a
number of requests for basic info about Linux and the use thereof. In
this doc, I'll cover the basics of logging in, using the shell, and even
some rudamentry shell scripting and various useful utilities. I won't,
however, cover the use of X Windows, as that can be a whole subject unto
itself. Also, I will only be able to cover the Debian distribution as I
don't have RedHat or S.u.S.E or other distributions around here.

Before I begin, I would like to make one point: Linux is *much* more
powerful than any Microsoft product, and with that greater power comes
the responsibility to learn how to use it properly. This is to be
expected, and is much less than painful than many make it out to be.

---

## Logging In

So, you've got Linux installed on your system and you're just about to
begin using it. You're presented with a screen looking like this:

``` 
Debian GNU Linux 1.3 geeky1 tty1

geeky1 login:
```

You probably created a personal account for yourself, and we'll use
that. In these examples, the account is paul, password password. Now, at
the login prompt type in your username, in our example, paul. The screen
will now look like this:

``` 
Debian GNU Linux 1.3 geeky1 tty3

geeky1 login: paul
Password:
```

Now, watch your fingers while you type in the password - it won't be
echoed for security reasons, and watching your fingers will help you
keep from making a mistake. The system will now display the contents of
/etc/motd, and run the file .bash\_profile in your home directory if
you're using the bash shell(most distributions default to using bash).

---

## Using The Shell

The shell is kind of like DOS's command.com, but MUCH more powerful. The
first command we're going to use is the touch command. All it does is
either create an empty file, or update the last modified date on an
already existing file. The shell prompt will probably look like a dollar
sign. First, we want to make a directory to hold our work. At the
prompt, type:

```
mkdir foo
```

Now, we want to use the ls command, which is the equivalent of DOS's dir
command. While I'm at it, if you want more info on using any command
listed here, type:  
man *command*  
Where *command* is the command you want more information on. man is
short for manual page. Type ls, and it will list foo. Now, type:

```
cd foo
```

Okay, you're now in the foo directory. Oh dear, a bit confusing isn't
it? The current directory isn't displayed on the prompt. Let's fix it.
The environment variable PS1 holds the specification for the prompt.
Type:

```
export PS1="\`pwd\`:\`whoami\`:\$ "
```

And you'll notice you're prompt has changed\! Let me explain... export
is used in bash to make the variable so that it is carried to other
shells you run - if you don't export the variable, and run a program,
the program won't know about the variable. It's good practice to use
export whenever you set an evironment variable. Next, you'll notice
`\pwd\`. What's with this `\` stuff, you ask? In bash, the
backslash is used to 'escape' a character - keep the shell from
processing it. Why the backquote? In a shell, it's used to tell the
shell to replace the stuff inside the backquotes with what's printed on
the screen by the program that's inside them. For example, type:

```
echo f`pwd`f
```

The system will print:

```
f/home/paulf
```

The shell evaluated the backquotes, and replaced the text inside them
with the output of the pwd program. Now, if we hadn't escaped the
backquotes - the shell would evaluate them when it shouldn't and the
current directory portion of the prompt would always stay the same. Now,
in bash, \~ refers to your home directory. Go ahead, type:

```
echo ~
```

You'll get something like:

```
/home/paul
```

So, we say that the directory foo you just created is in the path \~/foo
Now, I want you to type:

```
touch bork
```

And then type ls again. Wala\! You've just created an empty file\! Now,
do it again, except this time creating three files - foo.txt, bork.txt
and bar.txt. In DOS, if you wanted to change the extension of all these
to .html, you'd have to do it by hand, right? Linux, however, is much
more powerful. Type:

```sh
for i in `ls`; do mv $i `basename $i .txt`.html; done
```

And do an ls again. They've all changed extension to .html\! You've
actually done some shell programming. You've written what's called a for
loop(for obvious reasons). For every instance reported by ls, the
environment variable $i is set to the entry, and the stuff after do is
run. Right now would be a good time to use man to learn about the
basename command. It strips off the specified extension(in this case,
.txt) and returns the value of $i sans the extension. The backquotes
make certain that the shell ends up seeing it as:

```
mv foo.txt foo.html
```

Amazing, eh?

This brings to an end our basic intro to the shell. Next, I hope to
write an intro to emacs and writing shell scripts.

