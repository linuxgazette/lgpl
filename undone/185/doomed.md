---
title: Doomed to Obscurity
---

* Autor: Pete Trbovich
* Tłumaczył:
* Original Text: https://linuxgazette.net/185/doomed.html

---

*These images are scaled down to minimize horizontal scrolling.*


[![](misc/doomed/0000408.jpg)
Click here to see the full-sized image](misc/doomed/0000408.jpg)

[![](misc/doomed/0000406.jpg)
Click here to see the full-sized image](misc/doomed/0000406.jpg)

[![](misc/doomed/0000413.jpg)
Click here to see the full-sized image](misc/doomed/0000413.jpg)

[![](misc/doomed/0000411.jpg)
Click here to see the full-sized image](misc/doomed/0000411.jpg)

All "Doomed to Obscurity" cartoons are at Pete Trbovich's site,
<http://penguinpetes.com/Doomed_to_Obscurity/>.

