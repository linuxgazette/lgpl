# Taking the Pain Out of Authentication

Autor: Yvo Van Doorn

----

When dealing with authentication, there are a myriad of systems out
there that validate user identity and permissions: shadow passwords,
Kerberos, NIS and LDAP, just to name a few.

In early Unix and (very early) Linux deployments, the applications would
be responsible for directly interacting with whatever security system
that was in place for a given server. When it was just the app scanning
the /etc/passwd file, that wasn't so hard to manage, but once all those
other authentication systems were deployed, then leaving authentication
in the application space quickly became unmanageable.

In 1995, Sun Microsystems came up with a better way: a library that
would handle all of the communication between applications and whatever
authentication system that might be in place. This would take the onus
of authentication off application developers' plates, and prove to be
much easier to expand. If a new authentication system is put into place,
a shared library object can be added to the library, without any changes
to the app. This library is known as the pluggable authentication module
(PAM), and it's a big part of authentication systems used today.

The biggest advantage to PAM is scalability: the capability to add a new
authentication system to any given server without needing to
re-configure (or worse, re-compile) a program is a big deal. That's not
to say there isn't some sort of payment to be made -- nothing is free.
Instead of touching the application, all PAM-aware applications have a
text configuration file that must be edited to compensate for any new
module within the PAM library.

At first, that may seem like more trouble than it's worth, since
managing multiple configuration files could quickly get painful. In
truth, individual PAM configuration files enable each application to use
any authentication protocol they need, even if they're on the same
system. So, one application can use LDAP, while another can rely on
shadow passwords, if that's what you need. Better still, with one
notable exception, switching to a new protocol is as simple as plugging
in the protocol's module to PAM then editing the application(s)
configuration files to make note of the change. The application only
cares what PAM is telling it, not what the actual authentication
solution says.

Another important feature of PAM, which makes it of interest to
authentication providers like Likewise, is that it can be used for more
than just password management. PAMs can be configured to outline session
setup, logging, permissions management, as well as work directly with
Active Directory for authentication.

Modules can be combined as well, thus giving apps a layered approach to
authentication, if needed. Individual PAM configuration files are
comprised of a list, known as a "stack," of all the PAM modules that
will govern access for the application in question. Modules are invoked
in their stack order and any module failure will usually block access
for the app. More lenient policies can be put in place that might
circumvent this all-or-nothing approach.

One example of such a policy might be to allow a user to physically
access a system at any time of the day, but not grant access remotely
during certain times. (Care must be taken, though, to allow for
international travelers who may indeed need to login remotely in the
middle of the server's night.) Another such module could authenticate a
user only if their Bluetooth-enabled device was broadcasting nearby.

Of course, like any system, PAM isn't without its vulnerabilities. A
security hole this past summer would allow a malicious user to own the
permissions of a vulnerable system's shadow password file. With access
to the shadow file, the cracker could easily reset the password for
every account on the machine, including root. At that point, the cracker
can do just about anything to the box, including resetting the shadow
file back to its former state, so that system administrators would be
less likely to notice something was amiss. This vulnerability was very
quickly patched, but it outlines the need to keep a constant eye on any
authentication- or security-based frameworks.

The extreme scalability and flexibility of the PAM framework means it
can fit within almost any security policy an organization might come up
with. If a module or combination of modules doesn't meet specific
security needs, then a new module can be written as a solution. Linux
users, for instance, can visit Andrew Morgan's [The Linux-PAM Module
Writers'
Guide](http://www.kernel.org/pub/linux/libs/pam/Linux-PAM-html/Linux-PAM_MWG.html)
to learn more about creating such solutions.

PAM is a major tool in the security world, and one well worth
understanding, since it can do far more than just handle passwords.

