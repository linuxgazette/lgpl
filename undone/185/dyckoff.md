---
title: Away Mission for April 2011
---

* Autor: Howard Dyckoff
* Tłumaczył:
* Original text: https://linuxgazette.net/185/dyckoff.html

---

There are quite a few April events of interest to the Linux and Open
Source professional. The menu includes hard-core Linux development,
Android, MySQL, system and network security, and cloud computing.

### USENIX NSDI '11

NSDI '11 is actually March 30-April 1 in Boston. It is co-located with
and preceeded by LEET '11, the Large-Scale Exploits and Emergent Threats
seminar.

NSDI '11 will focus on the design, implementation, and practical
evaluation of large-scale networked and distributed systems. It is
primarily a forum for researchers and the papers presented there
represent the leading research on these topics. Of course, it starts
before April 1st. But it is worth noting and, if you are in Boston, you
might catch the end of it.

I attended LEET and NSDI in San Francisco 3 years back and was impressed
with the technical depth and the level of innovative thinking. But I
also have to say that many of the presentations represent advanced grad
student work at leading universities. Part of the conversation is about
the footnotes and several of the presentations are follow-ups to
research presented at earlier NSDI conferences or related events. I can
honestly say that none of the presentations were duds but equally you
couldn't split your attention with email or twitter... unless you were
in that research community.

All of the papers are available after the conference and past papers
from past Symposia are available in the NSDI archive:
<http://www.usenix.org/events/bytopic/nsdi.html>

Here is the link to the abstracts for this year's NSDI:
<http://www.usenix.org/events/nsdi11/tech/techAbstracts.html>

### Multiple Linux Foundation events

The Linux Foundation Collaboration Summit is in its 3rd year. It has
always been a kind of family week for the extended community on the West
Coast, especially as several other major Linux events often occur in the
Boston area.

The Linux Foundation Collaboration Summit is an invitation-only
gathering of folks like core kernel developers, distro maintainers,
system vendors, ISVs, and leaders of community organizations who gather
for plenary sessions and face-to-face workgroup meetings in order to
tackle and solve pressing issues facing Linux. Keynote speakers include
the CTO of Red Hat, Brian Stevens, and the head of Watson Supercomputer
Project, David Ferrucci.

This year, because the merger of the Foundation with the CE Linux Forum
(CELF) in 2010, the Summit is followed by an updated Embedded Linux
Conference at the same San Francisco venue, allowing for down time
between events and lots of socializing. Co-located with the 2011
Embedded Linux Conference is the day and a half Android Builders Summit,
beginning mid-day Wednesday, April 13th and continuing on April 14th
with a full day of content.

The ELC conference, now in its 6th year, has the largest collection of
sessions dedicated exclusively to embedded Linux and embedded Linux
developers. CELF is now a workgroup of the Linux Foundation because the
principals believe that by combining resources they can better enable
the adoption of Linux in the Consumer Electronics industry.

You can use this link to register for both of the embedded Linux events:
<http://events.linuxfoundation.org/events/embedded-linux-conference/register>

Also, Camp KDE is co-located with the Collaboration Summit and actually
precedes it on April 4-5. So the festivities actually run from the 4th
until the 14th.

There will be "Ask the Expert" open discussion forums on several
technical topics and Summit Conference sessions on the following key
topics: Btrfs, SystemTap, KVM Enterprise Development, File and Storage
for Cloud Systems and Device Mapper, Scaling with NFS and Tuning Systems
for Low Latency.

An annual favorite at the Summit is the round table discussion on the
Linux Kernel. Moderated by kernel developer Jon Corbet, the panel will
address the technology, the process and the future of Linux.
Participants will include James Bottomley, Distinguished Engineer at
Novell's SUSE Labs and Linux Kernel maintainer of the SCSI subsystem;
Thomas Gleixner, main author of the hrtimer subsystem; and Andrew
Morton, co-maintainer of the Ext3 filesystem. In previous Summits, this
has been both lively and detailed.

Last year, the Collaboration Summit was where the partnership of Nokia,
Intel, and The Linux Foundation on MeeGo was announced. I understand
that there should be more such announcements this year, especially as
there is now also a mini-Android conference at the end of the 2 week
period.

View the full program here:
<http://events.linuxfoundation.org/events/end-user-summit/schedule>

Interested in the 2010 presentations? Use this link:
<http://events.linuxfoundation.org/archive/2010/collaboration-summit/slides>

### InfoSec World 2011

InfoSec World will be taking place on April 19-21, 2011 in Orlando, FL.
The event features over 70 sessions, dozens of case studies, 9 tracks,
12 in-depth workshops, and 3 co-located summits. The last last of these
summits, the Secure Cloud & Virtualization Summit, will take place on
the Friday after InfoSec. It features a session on VMware Attacks and
How to Prevent Them. This is a serious and professional security
conference.

The opening keynote at InfoSec this year is "Cybersecurity Under Fire:
Government Mis-Steps and Private Sector Opportunities" by Roger W.
Cressey, counterterrorism analysmmmt and former pesidential advisor.

I attended an InfoSec event in 2008 and found the level of presentations
to be excellent. For instance, there was a detailed session on detecting
and exploiting stack overflows and another 2-part session with a deep
dive on Identity Management.

And their "Rock Star" Panel promised and delivered challenging opinions
on the then-hot information security topics. In 2008, the panelists
discussed hardware hacking and included Greg Hoglund, CEO of the now
more famous HBGary, Inc. and founder of www.rootkit.com, and Fyodor
Vaskovich, Creator of Nmap and Insecure.org, among other notables. (This
is actually the pseudonym of Gordon Lyon.)

My problem with prior security events hosted by MISTI is that it had
been very hard to get most of the presentation slides after the event
and nothing was available to link to for perspective attendees who might
want to sample the content - or to readers of our Away Mission column.
This year the management at MISTI is aiming to do better.

Among the sessions presented this year at InfoSec World, these
specifically will be accessible by the public after the conference -
sort of the opposite of RSA's preview mini sessions:

1.  \- Cloud Computing: Risks and Mitigation Strategies- Kevin Haynes
2.  \- Computer Forensics, Fraud Detection and Investigations- Thomas
    Akin
3.  \- Using the Internet as an Investigative Tool- Lance Hawk
4.  \- Social Networking: How to Mitigate Your Organization's Risk-
    Brent Frampton
5.  \- Securing Content in SharePoint: A Road Map- David Totten
6.  \- Cloud and Virtualization Security Threats- Edward Haletky
7.  \- Hacking and Securing the iPhone, iPad and iPod Touch DEMO- Diana
    Kelley
8.  \- Google Hacking: To Infinity and Beyond - A Tool Story DEMO-
    Francis Brow/ Rob Ragan
9.  \- Penetration Testing in a Virtualized Environment DEMO- Tim
    Pierson

For more information about InfoSec World 2011, visit:
<http://www.misti.com/infosecworld>

### MySQL User Conference

After Sun's purchase of MySQL a few years ago, and recently Oracle's
purchase of Sun, the bloom may be coming off the open source flower that
MySQL has been. Many of the original developers are working on the fork
of MySQL that is Maria. And O'Reilly has made it somewhat difficult for
Linux Gazette to attend this and other events that they host. But there
is still a huge user base for MySQL and this is the annual user
conference.

Last year's event seemed a bit more compressed -- fewer tracks, fewer
rooms in the hotel adjacent to the SC Convention Center, fewer vendors
in the expo hall. Also, in a sign of the depressed economy, there was no
breakfast in the mornings. There were coffee and tea islands between the
session rooms, but the signature Euro breakfast at the MySQL conference
was missing, as were many of the Europeans. This is now clearly an
American corporate event.

That being said, there were better conference survey prizes last year --
an iPad or a free pass to MySQL 2011 -- and it still has a community
presence with BOFs from community projects.

Last year, conference organizers added an Open MySQL unconference room.
This was only one space and it seemed to be only lightly used. This year
may be different.

Another interesting change were the slides between sessions promoting
other sessions organized by show sponsors like Pentahoo and Kickfire, as
well as several sponsored by Oracle itself. That seemed a bit heavy
handed... your mileage may vary.

The actual presentations emphasized MySQL tuning and admin, plus
sessions on alternate DBs such as NoSQL, RethinkDB, Drizzle, and MariaDB
(see MariaDB: 20 Slides, 5 Minutes, The Full Monty by Michael Widenius).
Here is the OReilly link for materials from the 2010 MySQL conference:
<http://en.oreilly.com/mysql2010/public/schedule/proceedings>

### CloudSlam '11

CloudSlam '11 is the 3rd annual CloudSlam conference, covering latest
trends and innovations in the world of cloud computing. Conference
panels, workshops, and tutorials will cover topics in cloud computing,
including public sector cloud initiatives, transitioning traditional IT
resources into the cloud, Cloud 2.0, Application Delivery, Private cloud
/ Public cloud, and Greener Data Centers.

This is a hybrid global event: The first day, April 18th, is a physicial
conference, followed by 4 days of virtual conference.

The $99 cost of the regular registration includes access to all Cloud
Slam 2011 live online sessions and downloadable conference proceedings
plus access to Cloud Slam '09 and Cloud Slam '10 downloadable conference
proceedings. But some of the sponsors have sent out discount
registration codes. The $299 VIP rate includes face time the first day
at the Microsoft Silicon Valley campus, should you want that.

Online access to just the 2010 conference proceedings will set you back
$29.95 plus a $1.74 fee for eventbrite.com:
<http://postcloudslam10.eventbrite.com/?ref=etckt>

So attending CloudSlam is a small investment, both in time and cash. We
didn't attend last year but this seems to be a good value at low risk.

-----

**NSDI '11 USENIX Symposium on Networked Systems Design and
Implementation**
Sponsored by USENIX with ACM SIGCOMM and SIGOPS
March 30-April 1, 2011, Boston, MA
<http://www.usenix.org/events/nsdi11/>

**Linux Foundation Collaboration Summit 2011**
April 6-8, Hotel Kabuki, San Francisco, CA
<http://events.linuxfoundation.org/events/collaboration-summit>

**Embedded Linux Conference and Android Builders Summit**
April 11-13, Hotel Kabuki, San Francisco, CA
<http://events.linuxfoundation.org/events/embedded-linux-conference>

**MySQL Conference & Expo**
April 11-14, Santa Clara, CA
<http://en.oreilly.com/mysql2011/>

**Cloud Slam - Virtual Conference**
April 18-22, 2011, On-line
<http://www.cloudslam.org>

**InfoSec World 2011**
April 19-21, 2011, Orlando, FL
<http://www.misti.com/infosecworld>

