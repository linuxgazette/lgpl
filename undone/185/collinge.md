---
title: HelpDex
---

* Autor: Shane Collinge
* Tłumaczył:
* Original text: https://linuxgazette.net/185/collinge.html

---

*These images are scaled down to minimize horizontal scrolling.*

[**Flash problems?**](../124/misc/nottag/flash.html)

[Click here to see the full-sized image](misc/collinge/182superior.swf)

[Click here to see the full-sized image](misc/collinge/183justify.swf)

[Click here to see the full-sized image](misc/collinge/186_9000000.swf)

[Click here to see the full-sized image](misc/collinge/187code.swf)

[Click here to see the full-sized image](misc/collinge/188code.swf)

All HelpDex cartoons are at Shane's web site,
[www.shanecollinge.com](http://www.shanecollinge.com/).

