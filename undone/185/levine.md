# Ubuntu, the cloud OS

Autor: Neil Levine

---

We made a small flurry of announcements recently, all of which were
related to cloud computing. I think it is worthwhile to put some context
around Ubuntu and the cloud and explain a little more about where we are
with this critical strategic strand for our beloved OS.

First of all, the announcements. We announced the release of Ubuntu
Enterprise Cloud on Dell servers. This is a hugely significant advance
in the realm of internal cloud provision. It's essentially formalising a
lot of the bespoke work that Dell has done in huge data centres (based
on a variety of OSes) and making similar technology available for
smaller deployments. We attended the Dell sales summit in Las Vegas and
we were very encouraged to meet with many of the Dell salespeople whose
job it will be to deliver this to their customers. This is a big
company, backing a leading technology and encouraging businesses to
start their investigations of cloud computing in a very real way.

More or less simultaneously, we announced our formal support for the
OpenStack project and the inclusion of their Bexar release in our next
version of Ubuntu, 11.04. This will be in addition to Eucalyptus, it is
worth stating. Eucalyptus is the technology at the core of UEC - and
will be in Ubuntu 11.04 - as it has been since 9.04. Including two
stacks has caused some raised eyebrows but it is not an unusual position
for Ubuntu. While we look to pick one technology for integration into
the platform in order to deliver the best user experience possible, we
also want to make sure that users have access to the best and most up to
date free and open-source software. The increasing speed of innovation
that cloud computing is driving has meant that Ubuntu, with its 6 month
release cadence, is able to deliver the tools and programs that
developers and admins want before any other operating system.

Users will ultimately decide what deployment scenarios each stack best
suits. Eucalyptus certainly has the advantage of maturity right now,
especially for internal cloud deployments. OpenStack, meanwhile,
continue to focus on rapid feature development and, given its heritage,
has appeal to service providers looking to stand up their own public
clouds. Wherever the technology is deployed, be it in the enterprise or
for public clouds, we want Ubuntu to be the underlying infrastructure
for all the scenarios and will continue to direct our platform team to
deliver the most tightly integrated solution possible.

Finally we saw our partner, Autonomic Resources announce UEC is now
available for purchase by Federal US government buyers. This is the
first step on a long road the federal deployment, as anyone familiar
with the governmental buying cycles will realise. But it is a good
example of the built-to-purpose cloud environments that we will see more
of - with the common denominator of Ubuntu at the core of it.

Which actually raises an interesting question - why is it that Ubuntu is
at the heart of cloud computing? Perhaps we ought to look at more
evidence before the theory. In addition to being the OS at the heart of
new cloud infrastructures, we are seeing enormous usage of Ubuntu as the
guest OS on the big public clouds, such as AWS and Rackspace, for
instance. It is probably the most popular OS on those environments and
others - contact your vendor to confirm :-)

So why is this OS that most incumbent vendors would dismiss as fringe,
seeing such popularity in this new(ish) wave of computing? Well there
are a host of technical reasons to do with modularity, footprint, image
maintenance etc. But they are better expressed by others.

I think the reason for Ubuntu's prominence is because it is innovation
made easy. Getting on and doing things on Ubuntu is a friction-free
experience. We meet more and more tech entrepreneurs who tell us how
they have built more than one business on Ubuntu on the cloud. Removing
licence costs and restrictions allows people to get to the market
quickly.

But beyond speed, it is also about reducing risk. With open-source now
firmly established in the IT industry, and with the term open used so
promiscuously, it is easy to forget that the economic benefits of truly
free, open-source software. The combination of cloud computing, where
scale matters, and open source is a natural one and this is why Ubuntu
is the answer for those who need the reassurance that they can both
scale quickly but also avoid vendor lock-in in the long-term.

More specifically, and this brings us back to the announcements, there
are now clear scenarios where users can reach a point where even the
economics of a licence-free software on a public cloud start to break
down. At a certain stage it is simply cheaper to make the hardware
investment to run your own cloud infrastructure. Or there might be
regulatory, cultural or a host of other reasons for wanting cloud-like
efficiencies built on internal servers.

The work we have done with OpenStack and with Eucalyptus means Ubuntu is
an ideal infrastructure on which to build a cloud. This will typically
be for the internal provision of a cloud environment but equally could
be the basis or a new public cloud. It is entirely open as to the type
of guest OS and in all cases continues to support the dominant API of
Amazon EC2, ensuring portability for those writing applications.

And as we have seen, Ubuntu is the ultimate OS to deploy in a cloud and
with which to build a cloud. No-one provides more up-to-date images on
the most popular public cloud platforms. Our work to ensure
compatibility to the most popular standards means that those guests will
run just as well on a UEC cloud however that is deployed - either
internally or for cloud provision externally.

So technology moves markets. Economics does too, only more so. Ubuntu
has come at the right point in our short IT history to ride both waves.
The scale is there, the standards are emerging and the ability to
provide an answer to the choice between running a cloud or running on a
cloud is more fully realised on Ubuntu than on any other OS - open
source or not.


