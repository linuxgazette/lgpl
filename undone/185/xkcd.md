---
title: XKCD
---

* Autor: Randall Munroe
* Tłumaczył:
* Original text: https://linuxgazette.net/185/xkcd.html

---

[![\[cartoon\]](misc/xkcd/beauty.png
"The best hugs are probably from hagfish, which can extrude microscopic filaments that convert a huge volume of water around them to slime in seconds. Instant cozy blanket!
")
Click here to see the full-sized image](misc/xkcd/beauty.png)


[![\[cartoon\]](misc/xkcd/ballmer_peak.png
"Apple uses automated schnapps IVs.
")
Click here to see the full-sized image](misc/xkcd/ballmer_peak.png)


More XKCD cartoons can be found [here](http://xkcd.com).

