# HelpDex

Shane Collinge

---

*These cartoons are scaled down to minimize horizontal scrolling. To see
a panel in all its clarity, click on it.*

[![\[cartoon\]](misc/collinge/cheatcodes.jpg)](misc/collinge/cheatcodes.jpg)  
[![\[cartoon\]](misc/collinge/379sims.jpg)](misc/collinge/379sims.jpg)  
[![\[cartoon\]](misc/collinge/381atmcode.jpg)](misc/collinge/381atmcode.jpg)  
[![\[cartoon\]](misc/collinge/382doohickey.jpg)](misc/collinge/382doohickey.jpg)  
[![\[cartoon\]](misc/collinge/383hashleft.jpg)](misc/collinge/383hashleft.jpg)  
[![\[cartoon\]](misc/collinge/380eminem.jpg)](misc/collinge/380eminem.jpg)  

Recent HelpDex cartoons are at Shane's web site,
[www.shanecollinge.com](http://www.shanecollinge.com/), on the
[Linux](http://www.shanecollinge.com/Linux/) page.

 