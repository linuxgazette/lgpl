
# Linux-Based Voice Recognition

Janine M Lodato

---

Let's look at Linux-based voice recognition software from the
perspective of China. It would behoove Linux computer makers anyway to
begin manufacturing their computers in China, because China offers a
low-cost method of manufacturing and provides them with a large market
for their hardware which can also be exported to other important markets
around the world.

Linux computers have the capacity to accommodate voice recognition
systems, such as IBM ViaVoice. This is especially advantageous to
Chinese speakers because both Mandarin and Cantonese are very complex in
the written form, so documents could be more easily produced through
voice recognition software running on a Linux platform. Using a keyboard
is next to impossible for Chinese languages because so many characters
are involved in typing a document.

Other languages will also benefit from using voice recognition software
for purposes of speed. Hands-busy, eyes-busy professionals can benefit
greatly from voice recognition so they don't have to use a mouse and
keyboard to document their findings. Voice-activated, easily-used
telephone systems will benefit all walks of life. Anyone driving a car
will find voice recognition a much more effective way of manipulating a
vehicle and communicating from the vehicle.

The health-care market alone may justify the Linux based voice
recognition project. Health-care services are the largest expense of the
Group of Ten nations, and it is the fastest growing sector as well.
Health-care workers would benefit from using their voices to document
describing the treatments of patients. Voice recognition allows them a
hands-free environment in which to analyze, treat and write about
particular cases easily and quickly.

Electronically connected medical devices via wireless LAN can benefit:

  - ...Hospital administration staff
      - Improve the usage efficiency of resources
      - Achieve standardized, quality patient management
      - Dramatically reduce data recording (transcription) errors
      - Lower costs
      - Make any room a telemetry room on demand (that is, do laboratory
        measurements in any room regardless of where the central
        equipment is located)
  - ...Medical staff
      - Be empowered with a 24/7 complete set of vital-sign data
      - Have more time for hands-on care
      - See changes in patient status immediately to enable quicker
        responses

In this life sciences field, the simplicity, reliability and low cost of
Linux for servers, tablets, embedded devices and desktops is paramount.
Only about 10% of the documents in the health-care field in the USA are
produced electronically due to the cumbersome and unreliable nature of
the Windows environment. 30% of the cost of health-care is a direct
result of manual creation of the documents and many of the malpractice
cases are also due to the imprecision of transcriptions of manually
scribbled medical records and directives, as anybody who looks at a
prescription can attest.

Obviously, the market for these new technologies exists. What remains is
for a hungry company with aggressive sales people to tap into that
market. Once those sales people get the technology distributed, the
needs of many will be met and a new mass market will open up that
Microsoft isn't filling: assistive technology (AT). Actually, the field
already exists but needs to be expanded to include both physically
disabled and functionally disabled.

Yes, voice recognition offers great promise for the future. However, it
isn't perfect and needs to be improved. One improvement could use lip
reading to bolster its accuracy. Still another is multi-tonal voice
input. Another is directional microphones. Every generation of voice
recognition software will improve as the hardware for Linux gets bigger
and stronger.
