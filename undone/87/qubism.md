# Qubism

Jon "Sir Flakey" Harsem


*These cartoons are scaled down to minimize horizontal scrolling. To see
a panel in all its clarity, click on it.*

[![\[cartoon\]](misc/qubism/qb-remastered.jpg)](misc/qubism/qb-remastered.jpg)  
[![\[cartoon\]](misc/qubism/qb-lotrdm.jpg)](misc/qubism/qb-lotrdm.jpg)  

All Qubism cartoons are
[here](http://www.core.org.au/modules.php?name=Cartoons) at the CORE web
site.

