# Quick-Start Networking

Edgar Howell


## Introduction

Over the holidays I finally had a block of time large enough to work on
a network at home. But getting started is always my biggest problem and
it took a while to understand what had to be done on which machine. In
retrospect it was quite easy to get started.

This article is essentially little more than my notes, taken during the
experience, less false starts. To the best of my knowledge it documents
what I had to do and will be my reference if the need arises to repeat
any of this.

To avoid inflating this unnecessarily and because I'm really just an
experienced newbie, almost nothing is explained. There are references to
some relevant articles but I assume you know how to find the standard
documentation.

To my mind there is no reason for anyone with two or more computers not
to have them networked. My first step was with an Ethernet card for the
PC, a cross-over cable, and a PCMCIA Ethernet card, all for 87.50 euro.
Once that was working, another PCMCIA card (should have known by the
price that it was Windows-only), 8-port switch and 3 3-meter cables cost
67.50 euro. Roughly $160 wasn't bad. And it shouldn't cost much more
than $25 to connect 2 PCs point-to-point.

The current status of this home office network is as follows:

Toshiba 486 500MB/24MB, SuSE 8.0 (kernel 2.4.18-4GB) without X

PC Pentium 166 2x4GB/32MB, SuSE 6.3 (kernel 2.2.13)

Toshiba AMD 4GB/64MB, SuSE 8.0 (kernel 2.4.18-4GB) or Windows 98

By the way, the asymmetry in the following is not due to anything
inherent in networking or the different Linux kernels. Rather, the 486
will one day be my portal to the Internet. It shouldn't be able to do
much of anything other than responding to someone it knows. On the other
hand the other two should have no restrictions.

Other than that, be careful: this is merely intended to get up and
running as quickly as possible. Everything else has been pretty much
ignored. Consider this just a small but important first step. Your next
step has to be the relevant documentation because this is quite
superficial\!

## 1\. Quick-Start - Ethernet

Other than a PCMCIA problem (see below), installing and configuring
Ethernet is rather straight-forward. To keep things simple I started out
with a cross-over cable, i.e. point-to-point, and moved on to a switch
only after everything else was known to work.

Rather than having each machine connect to the network at boot, there
are scripts in /root to run when it is time to connect. Here are the
relevant scripts and files from two of the machines (less comments and
stuff not relevant here):

Toshiba 486

``` 
     /etc/hosts:       127.0.0.1      localhost
                       192.168.0.99   Toshiba486.Lohgo  Lohgo486
                       192.168.0.100  ToshibaAMD.Lohgo  LohgoAMD
                       192.168.0.101  PC.Lohgo          LohgoPC

     /etc/hosts.allow: sshd: 192.168.0.100, 192.168.0.101

     /root/eth-up:     #!/bin/bash
                       /sbin/ifconfig eth0 192.168.0.99 \
                                      broadcast 192.168.0.255 \
                                      netmask 255.255.255.0 up
```

Pentium 166

``` 
     /etc/hosts:       127.0.0.1      localhost         PC
                       192.168.0.99   Toshiba486.Lohgo  Lohgo486
                       192.168.0.100  ToshibaAMD.Lohgo  LohgoAMD
                       192.168.0.101  PC.Lohgo          LohgoPC

     /etc/hosts.allow: sshd:      192.168.0.100
                       portmap:   192.168.0.100
                       lockd:     192.168.0.100
                       rquotad:   192.168.0.100
                       mountd:    192.168.0.100
                       statd:     192.168.0.100

     /root/eth-up:     #!/bin/bash
                       /sbin/insmod rtl8139
                       /sbin/ifconfig eth0 192.168.0.101 \
                                      broadcast 192.168.0.255 \
                                      netmask 255.255.255.0 up
```

The following are the same on all 3 machines:

``` 
     /etc/hosts.deny:  ALL : ALL

     /root/eth-down:   #!/bin/bash
                       /sbin/ifconfig eth0 down

     /root/eth-stat:   #!/bin/bash
                       /sbin/ifconfig eth0; /bin/netstat -r
```

The extra entries for the P166 in /etc/hosts.allow are to support nfs.
And insmod in /root/eth-up is due to the Ethernet card in the PC vs
PCMCIA on the notebooks.

Be aware that SuSE at installation has an option to "re-organize"
/etc/hosts that defaults to CHECK\_ETC\_HOSTS=yes in /etc/rc.config. My
suspicion is that this is what can cause the 192-IP-address to be
replaced by a 127-address for the host itself in /etc/hosts on reboot. I
don't reboot often enough to feel like checking this out. But if you get
an inexplicable inability to access the network, do verify the contents
of this file.

## 2\. Quick-Start - Ssh

Without a doubt this is the most complex of the Linux facilities
described here but is the key to a couple of things that are extremely
useful and it certainly should be set up, for both convenience and
security.

Prerequisites/definitions:

"local" is the machine whose keyboard you want to use

"remote" is the machine whose keyboard you don't want to use

"\<user\>" has been set up on both machines

"\<host\>" is the 3rd column of the entry for the "remote" host in
/etc/hosts on the "local" machine

the entries in /etc/hosts.allow and /etc/hosts.deny on the "remote"
machine permit use of sshd from the "local" machine

use of the mount command does mean playing disk-jockey between the two
machines as appropriate.

the following is based on SuSE 6.3 (2.2.13) and 8.0 (2.4.18-4GB)

This is what you have to do if you don't bother to set ssh up:

    Remote        Local          Comment
    
                  <logon as user also known to remote host>
                  ssh <host>
                                 warning:... SOMETHING NASTY
                  yes            accept it
                  <password>

This is setup:

    Remote        Local          Comment
    
                  <logon as user also known to remote host>
                  /usr/bin/ssh-keygen
                                 accept default: .ssh/identity
                                 no passphrase
                  mount /floppy
                  cp .ssh/identity.pub /floppy/
                  umount /floppy
    
    logon as <the same user>
    mkdir .ssh                   if necessary
    mount /floppy
    cp /floppy/identity.pub .ssh/authorized_keys
    cp /etc/ssh/ssh_host_key.pub /floppy/known_hosts
    umount /floppy
    
                  mount /floppy
                  cp /floppy/known_hosts .ssh/
                  umount /floppy
                  vi .ssh/known_hosts
                                 add <host> at start of line and
                                 remove root@<host> at end

And this is what you have to do to logon after setting things up:

    Remote        Local          Comment
    
                  <logon as user also known to remote host>
                  ssh <host>

Note that the host key is generated as part of system installation (with
SuSE anyhow). And there can be differences in directory structure
(SuSE's kernel 2.2 didn't have 'ssh' under 'etc'). Also note that this
is just intended to get someone unfamiliar with ssh up and running. Do
not blindly follow these steps if you have used ssh before\! In
particular most 'cp's certainly ought to be 'cat ... \>\>'. In the
office at home I don't want a passphrase to begin work on a different
machine, but you might.

## 3\. Quick-Start - Screen

Although it has been mentioned in Linux Gazette several times and I
actually did play with it briefly, the need for screen wasn't at all
obvious to me. Given 6+ vt's and X running on at least two others with
unlimited windows under whatever window manager one has running, it
seemed just another level of complexity.

The need became obvious as the network at home began taking shape. The
rationale behind screen boils down to this: if you start sessions on
remote machines under screen, they remain available to you as long as
the remote machine isn't shut down -- independent of what happens on the
communication link or your local machine. Like one of my PCMCIA Ethernet
cards only works under Windows and I can thus only connect one of the
notebooks to the PC at a time, if the AMD is also running Linux, as it
usually is -- but no need to shut the 486 down, just eject the card, pop
it into the AMD and screen keeps sessions active on the 486 for later
access.

To start screen:

``` 
    screen -R   restart session if available, otherwise start one
```

Within screen (not at all apparent, it hides well) use Ctrl-a followed
by:

``` 
    ?   help
    w   show list of windows
    n   switch to next window
    c   create new window
    d   disconnect
    A   assign title to window
```

## 4\. Quick-Start - File Transfer

If you are using ssh, you can get rid of rsh -- and telnet and ftp as
well for that matter. Here are a couple of alternatives that to me are
more convenient than the lot.

Netcat is a nifty little tool, analogous to cat. You start it to receive
a file on one machine

``` 
    netcat -vv -l -p <port> > <file>
```

and then tell the other machine what to send

``` 
    netcat -vv -w 10 <host> <port> < <file>
or
    tar -czvf - <directory> | netcat -vv -w 10 <host> <port>
```

Use netstat and /etc/services to find an available port. The option "-w
10" tells the sender to terminate the connection after 10 seconds of
inactivity and the option "-vv" lets you verify that the correct number
of bytes was sent and received.

While netcat holds promise for scripts to backup to a different machine
as the network at home gradually takes shape, Midnight Commander has
amazing facilities for the things one simply has to do by hand.

If ssh has been set up properly, the following entered in the command
line makes mc's active panel point to the same user on the "other"
machine -- yes, "\#sh" not "\#ssh", unfortunately

``` 
    cd /#sh:<host>
```

And if the other side has anonymous ftp running, the following should be
fairly self-explanatory

``` 
    cd /#ftp:www.tldp.org/
```

## 5\. Quick-Start - NFS

I played around with nfs and it works but unfortunately my notes are
non-existant (basically just check-marks in the printout of the HOWTO).
As I recall, besides installing the relevant package on client and
server all that was needed was to edit /etc/exports on the PC (server)
as follows:

    /home   192.168.0.100(rw,root_squash,sync,insecure)
    /tmp    192.168.0.100(rw,root_squash,sync,insecure)

See also /etc/hosts.allow under 1. Ethernet, above.

At installation SuSE has a number of options to be selected, many (all?)
of which wind up in /etc/rc.config. Here is an excerpt of those relevant
to nfs:

    START_PORTMAP="yes"
    NFS_SERVER="yes"
    USE_KERNEL_NFSD="yes"
    USE_KERNEL_NFSD_NUMBER="4"
    NFS_SERVER_UGID="no"
    REEXPORT_NFS="no"

On the AMD (client) I added the following to /etc/fstab:

    192.168.0.101:/home /Rhome  nfs noauto,users,sync 0 0
    192.168.0.101:/tmp  /Rtmp   nfs noauto,users,sync 0 0

At that point the mount command works with /Rhome etc. just as well as
/floppy or any other entry in fstab. One minor annoyance is that user
ID's must be the same on all machines using nfs. This was not a problem
for me because, when installing Linux, I create the few users in the
same order.

## 6\. Quick-Start - Samba

Given the difficulty of keeping track of what one is doing under
Windows, particularly with false starts and things that turn out to be
wrong or simply irrelevant, this needs to be taken with a large grain of
salt. It assumes that the driver for the PCMCIA card has been installed,
if relevant. And if the terminology is slightly obscure, that is due to
my translating from the German versions of Windows.

The following is what was necessary to enable logon to the PC from the
AMD under Samba, i.e. from Windows 98 to Linux 2.2.13 (SuSE 6.3). With
appropriate adjustments the same steps worked in the other direction,
i.e. from Windows 95 to Linux 2.4.18-4GB (SuSE 8.0). But note these
differences:

encrypt passwords: 98: yes; 95: no

path to smb.conf: 2.4: /etc/samba; 2.2: /etc

path to smbpasswd: 2.4: /etc/samba; 2.2: /etc

path to netlogon: 2.4: /usr/local/samba; 2.2: /var/lib/samba

    Part 1 - Linux
                                 edit /etc/smb.conf
    [global]
       workgroup = Lohgo
       encrypt passwords = yes
       smb passwd file = /etc/smbpasswd
       password level = 8
       username level = 8
       socket options = TCP_NODELAY
       domain logons = yes
       domain master = yes
       os level = 65
       preferred master = yes
       wins proxy = no
       wins support = yes
       hosts allow = 192.168.0.100 127.
    [homes]
       comment = Home Directories
       read only = no
       browseable = no
    [netlogon]
       comment = Network Logon Service
       path = /usr/local/samba/netlogon
       public = no
       writeable = no
       browseable = no
    [profiles]
       path = /home/%U/profile
       guest ok = yes
       browseable = no
                                 confirm validity, should show no errors
    testparm | less
                                 create user w/password
    smbpasswd -a web
                                 verify user enabled
    smbpasswd -e web
                                 start Samba
    smbd -D
    nmbd -D
                                 at this point from the client -- under
                                 Linux, not Windows -- the following
                                 should give a meaningful response
    smbclient -L LohgoPC
                                 and the following should give you
                                 ftp-like access
    smbclient //LohgoPC/web

    Part 2 - Windows98
    
    control panel | network | configuration
      add | client for microsoft network
      properties
        Windows NT-domain: Lohgo
        quick logon
      add | protocol | microsoft | tcp/ip
      properties | set IP-address
        IP-address:     192.168.000.100
        Subnet address: 255.255.255.000
      primary network logon: client for Microsoft network
    control panel | network | identification
      computer name: LohgoAMD
      workgroup:     Lohgo
      description:   ToshibaAMD.Lohgo
    control panel | passwords | user profiles
      users can customize: both
    reboot
                                 if using PCMCIA the following puts
                                 a symbol on the task bar with which
                                 the PCMCIA card can be removed
    <insert PCMCIA Ethernet card and wait for lights to settle down>
                                 the following works ONLY after TCP/IP
                                 has been set up, shows configuration
    start | run | winipcfg
                                 test connection from within a dos-box
    ping -n 5 192.100.0.101
                                 edit c:\windows\hosts.sam
    127.0.0.1       localhost
    192.168.0.101   lohgopc
                                 edit c:\windows\lmhosts.sam
    192.168.0.101   lohgopc

At this point after booting, Windows will ask you to logon, which you
can either do with a user known to Samba or cancel to use Windows
without the network as before. Now, however, the pop-up window opened by
Ctrl-Esc includes near the bottom a line to logoff that afterwards
provides the same logon prompt as booting. And the entries in the task
bar -- in the home directory, anyhow -- tell you who and where you are,
as in

"Explorer - \<user\> at \<host\>"

where "\<host\>" is the 3rd column of the entry for the Linux machine in
/etc/hosts on the Linux machine.

Symbolic links work quite nicely. The following executed within the home
directory of some user makes a directory -- even on a different
partition -- on the Linux machine available to that user on the Windows
machine:

ln -s /dos/f/pictures pictures

Due to a shortage of resources on the PC and the fact that I have no
real use for Windows anyhow, I use the following scripts to start and
stop the Samba daemons on the PC as needed:

    /root/samba-up:     #!/bin/bash
                        /usr/sbin/smbd    -d3    -l /tmp/sbd.log
                        /usr/sbin/nmbd -D -d0 -o -l /tmp/sbd.log
    
    /root/samba-down:   #!/bin/bash
                        kill -s SIGTERM $(ps aux | grep mbd \
                            | grep -v grep | awk '{print $2}')

Once you have this working, it won't take you 5 minutes to set up a
network printer. Uncomment (or add) the following to smb.conf:

    [printers]
       comment = All Printers
       browseable = no
       printable = yes
       public = no
       read only = yes
       create mode = 0700
       directory = /tmp

And then spend some time with the archaic data entry system on the
Windows machine:

    control panel | printer | new printer
      network printer | search
        network environment | Pc
          hpdj-a4-raw
        manufacturer: HP
        printer:      HP OfficeJet

Shut down and re-start Samba and you're in business.

## 7\. Quick-Start - PCMCIA

To be honest I have no idea whether this is generally applicable or is
specific to SuSE (8.0). And it was only the 2.4 kernel that had problems
with PCMCIA, not 2.2 strangely enough. Also, it has nothing to do with
networking per se. But if you're going to connect a notebook to your
network, you'll probably have to confront the alphabet monster. And a
PCMCIA Ethernet card makes a delightful docking station.

Omitting many details, I initially failed to note an inconsistency with
references to irq 5 and 10 that later led to tons of error messages. But
this was due to having inserted the PCMCIA card before starting the
installation of Linux.

In my case at least, by not inserting the PCMCIA card before starting
installation, there was a reference to only one irq which led to my
doing the following.

After initial boot in /etc/sysconfig/pcmcia add

``` 
    PCMCIA_PCIC="i82365"
    PCMCIA_PCIC_OPTS="irq_list=10"
```

and then run /sbin/SuSEconfig and reboot.

However, installing the PCMCIA software before doing this causes the
notebook to hang irrevocably on boot. The only way to boot is by giving
LILO the parameter NOPCMCIA=yes. Instead, I installed the PCMCIA
software after SuSEconfig and before reboot.

After that, inserting the PCMCIA card produces a couple of beeps and it
works as advertised. Since this is my first personal experience with
Ethernet, I can't comment on alternatives but the D-Link DFE-650TXD
PCMCIA Ethernet card works well, Linux-to-Linux anyhow (a couple of
hours sending stuff over the network before risking the wretched
"Recovery CD-Rom" to make Windows 98 work again) and has lots of LEDs to
let you know what is going on.

Here is the output from /sbin/cardctl config and ident.

CONFIG:

    Socket 0:
      not configured
    Socket 1:
      Vcc 5.0V  Vpp1 0.0V  Vpp2 0.0V
      interface type is "memory and I/O"
      irq 10 [exclusive] [level]
      function 0:
        config base 0x0400
          option 0x60 status 0x00 copy 0x00
        io 0x0300-0x031f [auto]

IDENT:

    Socket 0:
      no product info available
    Socket 1:
      product info: "D-Link", "DFE-650TXD", "Fast Ethernet", "Rev. A1"
      manfid: 0x0149, 0x0230
      function: 6 (network)

## 8\. Further Reading

See also the following articles in the issue of Linux Gazette
indicated:  
36: *Introducing Samba* by John Blair  
39: *Expanding Your Home Network* by J.C. Pollman  
44: *DNS for the Home Network* by J.C. Pollman and Bill Mote  
47: *Backup for the Home Network* by J.C. Pollman and Bill Mote  
48: *SAMBA, Win95, NT and HP Jetdirect* by Eugene Blanchard  
50: *Sharing your Home* by J.C. Pollman and Bill Mote  
57: *Making a Simple Linux Network Including Windows 9x* by Juraj
Sipos  
61: *Using ssh* by Matteo Dell'Omodarme  
64: *ssh suite: sftp, scp and ssh-agent* by Matteo Dell'Omodarme  
67: *Using ssh-agent for SSH1 and OpenSSH* by Jose Nazario  
74: *Play with the Lovely Netcat* by zhaoway

The Linux Gazette Answer Gang Knowledge Base under Network Configuration
has numerous relevant tidbits among which *Routing and Subnetting 101*
is mandatory reading.

And the Linux Focus Index by Subject under System Administration has
several articles well worth looking at, e.g.:  
*Replacing a Windows/NT/2000 server using Linux and Samba* by Sebastian
Sasias  
*Through the Tunnel* by Georges Tarbouriech  
*Samba Configuration* by Eric Seigne  
*Network File System (NFS)* by Frederic Raynal  
*Home Networking, glossary and overview* by Guido Socher

## 9\. A Future without Windows?

Coming from pre-TRS-80 days, I've used DOS, various versions of Windows,
at least 3 releases of OS/2, Coherent, and now 5 releases of SuSE Linux
over at least 5 years. I am convinced that anyone in a position to
"compare and contrast" would agree that at best Windows is unstable
junk. One of my goals for quite some time had been to gain complete
independence from Windows.

But consider: our ISDN phone system has an RS-232 connector with which
it can be programmed via -- yeah, you got it. One of the printers is USB
for the notebook and guess whose drivers are available. Our digital
camera uses smart media and the USB smart media reader... Oh, well, you
get the picture.

I've only had Samba working for a week and actually hadn't even intended
to check it out but everything else worked so well that it seemed worth
a try. And it's so slick that I question whether it would really be
worth my effort to try to find replacement drivers for this legacy
stuff. How many hours, how many experiments, what guarantee of success?
Doesn't it make more sense to boot the notebook under the "silly system"
(I hope Monty Python put that under GPL) and use the Samba connection to
the rest of the network? At least until the last Windows-legacy device
eats it.
