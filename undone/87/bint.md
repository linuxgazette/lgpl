# The Ultimate Editor

Stephen Bint

---

***Somewhere, out there, is a C++ programmer whom destiny has chosen to
be our liberator.***

### The Ultimate Editor's Time Has Come

How can it be that Windows users are spoiled for choice of excellent
text editors, while not one decent one exists for the Linux console?
Linux is a better OS, supported by better programmers who are dedicated
to producing better tools for eachother's benefit. The text editor is
the programmer's most important and frequently used tool. Yet Linux
console editors are rubbish. How can this be?

Those of us who migrate from windows to Linux expect a text editor, at
the very least, to allow selection of text with the mouse and to have
mouse-sensitive menus and dialogs. Of all the editors only mcedit, the
editor built in to the Midnight Commander disk navigator, has these
features. The rest have no dialogs and either no mouse interface or a
very limited, stupid one.

Yet even mcedit has a fatal flaw. If there is anything about its
behaviour you don't like, or a function it lacks which you would like to
add, you will find that reverse-engineering the source to solve that
problem is more difficult than writing your own text editor from
scratch. Unfortunately mcedit is quite basic, so it really needs added
functionality and there is no easy way to add it.

*What is the point of Open Source being open, if it is so complicated
and poorly documented as to be impenetrable to anyone but the author?*

Let's face it, we are all the same. We love writing code and hate
writing docs. Writing slick algorithms is fun but explaining how they
work to newbies is a bore. Yet if someone were to take the trouble to
write an editor with maintenace in mind and build in a simple way to add
C++ functions to menus, it might be the last editor ever written. No one
would bother to write a text editor if one existed, whose behaviour was
easy to change and to which any function could be added.

### Blasphemy

Stallmanist Fundamentalists may say at this point, emacs is extensible.
So it is, but you need to learn a second language to extend it. Besides
that, the basic editor has a crude and confusing user interface which
cannot be improved by adding lisp modules.

Some of us who aspire to use Linux are ordinary people, not software
supermen. It is cruel and unnecessary to tell someone struggling to
learn their first language, that they must simultaneously learn a second
language in order to make their editor work they way they want it to.

It will never do. Emacs isn't a tool. It's an intelligence test. It is
time stupid people fought back against the elitists who are so clever,
they find learning emacs a breeze. Notice that you do not have to learn
how to use mcedit. It does what you expect so there is nothing to learn.

The Ultimate Editor would be what emacs should have been: an extensible
editor with an intuitive mouse-and-menu interface. *\[Editor's note:
emacs was born before mice and pulldown menus were invented.\]* Instead
of complicating the picture with a second language, the extensions would
be written in C++. It would come with a programmer's guide, explaining
how to install your own menu commands and also describing the anatomy of
the source so that you can easily locate the module you are after if you
want to change something about its basic behaviour. It would be a
do-it-yourself editor kit.

### O, Beautiful Tool

If the Ultimate Editor existed, this is what it would be like. You would
download it and build it and find it has the basic functionality of
mcedit. It would have mouse selection, mouse-sensitive menus and a file
open dialog box that allows you to navigate the disk by double-clicking
on directories.

It would have few functions: File Open, File Save, File Save As, Exit,
Cut, Copy, Paste, Delete and Help. At first there would be no search
function, but the README would explain that the source file for the
search function is included and would give simple instructions for how
to add it. The lines to be added to the source would already be there,
but commented out, to make it easy to add the search function.

To add the search function you would have to:

1\. Move its source file to the editor's src directory

2\. Declare the function at the top of main.cc like this:

``` 
   int show_search_dlg();
```

3\. Add a line to main() (actually uncomment a line) like this:

``` 
   ed.add_menu_cmd( show_search_dlg, "Search", "Edit", F2_key, SHIFT_PRESSED );
```

...which installs a command labelled "Search" on the "Edit" menu, which
can be activated directly by pressing Shift-F2.

4\. In the Makefile, add (uncomment) a compile rule for the source file
and add its name to the list of objects to be linked.

5\. Run Make and find that the search function is now on the menu.

Having followed this procedure, even a complete newbie will know how to
write their own menu functions. The editor will be a global variable,
(C++ object) accessible in any source file the user writes, through its
header file. Its member functions will report the states of all its
internal variables, such as cursor position and area selected. The text
array containing the file being edited will be accessible as a member
variable, so that the file can be scanned and modified within the user
function.

### Living Colour

Usually, the logic of colourization is imposed on users. Some editors
offer a dialog to change the colours and to add keywords, but the logic
is dictated by the author.

The Ultimate Editor will offer an easy way for users to write their own
colourization routines. Apart from enabling people to colourize rare and
eccentric languages, this feature will unlock the hidden potential of
colourization.

Think how many ways you could choose to colour source and what an aid to
reverse engineering it could be. Depending on your purpose, you might
want to colour identifiers according to which header file they are
declared in, or whether they are automatic or allocated, or use colours
to indicate their scope. You might choose to have several colouring
schemes installed and switch between them with hot keys.

To make colourizing simple, the Ultimate Editor will store its files in
file arrays which contain two arrays of strings - one for the text and
another for the colours. The file array will keep the sizes of the
strings in these arrays synchronized so that, for every character stored
in the text array, there is always a byte representing its colour at the
same co-ordinates in the colour array.

The editor will always draw on the colour array when it refreshes, so
all the programmer has to do in order to colour a character at certain
co-ordinates, is change the value in the colour array at those same
co-ordinates and refresh the display.

### Ninety Percent Widgets

From the user's point of view, dialog boxes appear to be a small part of
a text editor. From the programmer's perspective, it is the other way
round. The editable fields which appear in dialogs are fully functional
editing windows with a couple of features disabled. So to write the
Ultimate Editor is really to write the Ultimate Widget Library.

A well-written widget library with good docs is more than an accessory
to an extensible editor. If users become familiar with the library in
order to improve the editor, they can use it to produce configuration
dialogs which assist non-experts in configuring other software, by
asking simple questions and writing out their wishes in a config file.

Linuxconf is a very important configuration tool, but it is fading like
a dead language because it is hard to use. Because it is hard to use, it
is hard to get enthusiastic about improving it. Users and programmers
both drift instead towards other, distribution-specific configuration
programs. If linuxconf was rewritten to show mouse-sensitive dialogs
that behave like proper dialogs (like X-windows dialogs), it might grow
to include modules to enable clueless newbies to configure any popular
package.

Do you not agree, that the main obstacle to the popularity of Linux, is
esotericism? I mean, no-one bothers to write software for newbies
because only software experts ever use Linux. The growth of Linux is
being prevented by an elitist Catch-22. If idiot-friendly configuration
programs were not important to the popularity of an OS, would Microsoft
have lavished so much time and money on them?

Rewriting linuxconf with a simple but modern widget library would be the
first step to making what it should be - a project that never ends. It
should be continually growing as more modules are added, until it
becomes the one-stop-shop through which all Linux software can be
configured by children.

### A Little Help

I want this challenge to be open to anyone who knows C++. Because
interfacing with the mouse, keyboard and colour-text screen under Linux
is a low-level nightmare, I have produced an interface library which
makes it as simple under Linux as it is under DOS. I recommend it over
Slang for the purpose of writing an editor for several reasons.

First, the Slang source (including docs and demo programs) zipped is
740k, whereas my library's source zips to 42k. Second, Slang does not
report mouse movement, so a Slang program cannot drag-select with the
mouse. Third, the colouring system in Slang is complicated, but mine
represents the screen as an EGA-style buffer of character/colour byte
pairs.

I wrote my library after an attempt to use Slang myself drove me to the
conclusion that its all-platform capability generated an unacceptable
overhead and took less than full advantage of the potential of the Linux
console. I don't doubt that the author of Slang is a better programmer
than me, but I have produced a library specifically to serve programmers
who want to produce the first adequate editor for the Linux console.

You can download it here: <http://members.lycos.co.uk/ctio/>

And now that interfacing with the console is as simple under Linux as it
ever was under DOS, the obstacle to Linux editors having the same basic
features as DOS editors has been removed. Now anyone who knows C++ can
do something great. To produce the editor and widget library I have
described might change the course of the history of free software, by
rolling out a red carpet to entry-level programmers.

### Invent the Wheel

I am constantly being told that there is no need to reinvent the wheel.
A ship could sail the Atlantic, powered only by my sighs. Let me assure
you, I will march up and down the High Street blowing a trumpet and
proclaiming at the top of my voice, "NO NEED TO REINVENT THE WHEEL\!" on
the day that someone actually produces a ROUND WHEEL.

In theory, any Open Source editor can be hacked and made perfect, but we
are still waiting for a mouse-aware console editor which can be hacked
and improved by programmers with I.Q.s under 170. Without adequate
documentation, Open Source is a Closed Book to ordinary mortals.

### Destiny

What are you, C++ programmer? Someone with the power to build abstract
machines, an inventor that has transcended the limitations of the
material world that crushed the dreams of human inventors of every
generation before this? The citizens of the beautiful city of Free
Software scrape along on square wheels and you could solve their
problem.

If you are sitting on your flabby backside thinking, "Nyaahh. It's not
for me", then who is it for? Not me, I'm homeless. I have had access to
a computer long enough to write the interface library, but now I am
living in a tent and the closest I get to a computer is occasional
internet access at a day centre for the unemployed. That is why it can't
be me. Why can't it be you?

It might be your destiny to be the author of that Ultimate Editor, the
last editor ever written. Perhaps no more than a month after the
importance of free software has been recognised and Stallman's face is
carved on Mount Rushmore, they may have to blow it off with dynamite and
carve yours on there instead.

## Reference

**[Slang](http://www.s-lang.org/)**, by John E. Davis. Slang appears to
have eclipsed curses, as the keyboard/mouse/colour text interface
library most programmers would recommend. If you are dead clever, you
might find a way to use the subset of Slang purely concerned with the
console interface, which is part of the [Midnight
Commander](http://www.ibiblio.org/mc/) source. It is smaller and allows
text selection at the Linux console, while still offering limited
functionality on less capable terminals, even telnet windows\!

**[CTIO](http://members.lycos.co.uk/ctio/)**, by Stephen Bint. By far
the simplest and best console interface library I have ever written.
Only works at the Linux console and DOS, not in rxvt/xterm nor telnet
windows (but it's only 42k). Read about my struggle to write it
[here.](../issue86/bint.html)

**[emacs](http://www.gnu.org/software/emacs/emacs.html)**, by Richard
Stallman. A millstone in the history of free software.

 
