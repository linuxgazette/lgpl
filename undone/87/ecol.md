# Ecol

Javier Malonda

These cartoons were made for es.comp.os.linux (ECOL), the Spanish USENET
newsgroup for Linux. The strips are drawn in Spanish and then translated
to English by the author. Text commentary on this page is by LG Editor
Iron. Your browser has shrunk the images to conform to the horizontal
size limit for LG articles. For better picture quality, click on each
cartoon to see it full size.

[![](misc/ecol/ecol-93-e.png)](misc/ecol/ecol-93-e.png)

[![](misc/ecol/ecol-93.png)](misc/ecol/ecol-93.png)
